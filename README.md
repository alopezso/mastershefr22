# :fire: :fire: :fire: MasterShef Framework :fire: :fire: :fire:

This framework is designed to be compatiable with  ATLAS RootCore base releases. The code is EventLoop based, it takes data and MC in the form of xAODs/DxAODs as input and produces MiniNtuples either locally or on the CERN grid. These can then be used to perform data analysis. 


1. [Quick Start Guide](#1-quick-start-guide)
    - [Setup](#setup)
    - [Compiling](#compiling)
    - [Running locally](#running-locally)
    - [Running multiple analyses](#running-multiple-analyses)
    - [Running systematics](#running-systematics)
    - [Checking the output](#checking-the-output)
2. [Running on the Grid](#2-running-on-the-grid)
    - [Submitting samples to the grid (not complete!)](#submitting-samples-to-the-grid)
    - [Using pandamon](#using-pandamon)
3. [Code Strucutre (not complete!)](#code-structure)
4. [Code Development (not complete!)](#checking-out-creating-a-branch)

----------------------

## 1.  Quick-start guide

Here are some simple instruction on setting up MasterShef and how to run locally on a xAOD to produce a vector ntuple we refer to as a "Mini-Ntuple".


### Setup

First clone the package recursively into a folder called 'GridAnalysis', this could be called whatever you wish. Without the last argument the package will be called `MasterShef`, the problem with this is you'll end up with a confusing folder structure - i.e. `MasterShef/source/MasterShef`. Therefore it is better to name the top directory something else

```
setupATLAS
lsetup git
kinit <user>@CERN.CH
git  clone --recursive https://gitlab.cern.ch/atlas-phys-susy-wg/ThirdGeneration/bbMET/MasterShef.git GridAnalysis
```


__Note:__ if you want to checkout a specific tag or branch you can clone the repo as follows:

```
git  clone --recursive https://gitlab.cern.ch/atlas-phys-susy-wg/ThirdGeneration/bbMET/MasterShef.git GridAnalysis -b <name of branch>
```

This creates the following structure:
```
$ tree -d -L 2 GridAnalysis/
GridAnalysis/
`-- source
    |-- BoostedJetTaggers
    |-- JetSmearing
    `-- MasterShef
```
A source directory containing 3 packages, the main 'MasterShef' code and 2 additional packages (not in athena) that it depends on. More detail on the code structure is given in [Code Structure](#code-structure).


### Compling

```
cd GridAnalysis
mkdir build
cd build
asetup  AnalysisBase,21.2.106
cmake ../source 
make
source x*/setup.sh
cd ..
```

If you want to build with the RestFrames package, used by the tt0L analysis you must do 
```
cmake ../source  -DUSE_RESTFRAMES
```


__Note:__ if you have already built the packages, you can compile from a clean shell with:

```
cd build
asetup
```


### Running locally

To run a simple example locally, download a DAOD locally and run the `sbottom` analysis on 2000 events..

```
mkdir run
cd run
runMasterShef.py --inputLocalFile /atlas/shatlas/DAODS/rel21/p3529/mc16_13TeV.388737.MGPy8EG_A14N_BB_direct_1200_1.deriv.DAOD_SUSY7.e5453_e5984_a875_r9364_r9315_p3529/DAOD_SUSY7.13996612._000001.pool.root.1 --analysis sbottom --maxEvents=2000 --dataSource=2
```

__Note__: This example runs on a single .root file, you can also specify a folder containing multiple root files to run on instead.

__Note__: the argument --dataSource=2 is used here to make sure AF-II calibration is set as we run on this particular file. If you run on a folder with the container name that containing root files, MasterShef can set the dataSource locally based on the name of the folder. 


The final messages you see when running this command is:
```
MasterShef::MasterShef    INFO    CutFlow for analysis sbottom
MasterShef::MasterShef    INFO    CutFlow for Variation 
Cut 0  Metadata Tot  counts 5000 Weighted 5084.57   PU weighted 5084.57
Cut 1  Total in files  counts 2000 Weighted 2032.32   PU weighted 2035.21
Cut 2  GRL  counts 2000 Weighted 2032.32   PU weighted 2035.21
Cut 3  LAr Tile Errors  counts 2000 Weighted 2032.32   PU weighted 2035.21
Cut 4  Trigger  counts 2000 Weighted 2032.32   PU weighted 2035.21
Cut 5  Primary Vertex  counts 2000 Weighted 2032.32   PU weighted 2035.21
Cut 6  Jet Cleaning  counts 1997 Weighted 2029.32   PU weighted 2031.56
Cut 7  clean muons  counts 1993 Weighted 2025.32   PU weighted 2027.46
Cut 8  (sbottom filtering selection)  counts 789 Weighted 804.266   PU weighted 807.126
Cut 9  0.000000>= NbaselineLep >= 0.000000  counts 773 Weighted 787.944   PU weighted 790.996
Cut 10  1000>= NJets >= 2  counts 773 Weighted 787.944   PU weighted 790.996
Cut 11  NBJets>=2  counts 771 Weighted 785.931   PU weighted 789.222
Cut 12  NBJets<=100  counts 771 Weighted 785.931   PU weighted 789.222
Cut 13  MET  counts 737 Weighted 751.294   PU weighted 756.246   <====== dumping ntuple sbottom here 
worker finished:
Real time 0:00:54, CP time 40.880
runMasterShef :: INFO     ::       done with this one

```

This tells you that 5000 events were in the original file and we ran on 2000 of these. Some cuts detailed in the printed cutflwo were applied and then MasterShef has dumped the ntuple after the MET selection, saving 737 events.

We can also see from the output that the following 'fillFuncitions' were called:
```
MasterShef::MasterShef... INFO    User requested to fill fillCommonVariables, added to the list of fills
MasterShef::MasterShef... INFO    User requested to fill fillExtraRecoVariables, added to the list of fills
MasterShef::MasterShef... INFO    User requested to fill fillExtraTruthVariables, added to the list of fills
MasterShef::MasterShef... INFO    User requested to fill fillRCFatJetVariables, added to the list of fills
```
More detail is provided in [Code Structure](#code-structure). The sbottom analysis python configuration file can be modified to dump more or less variables, depending on what the user wants. Also the level in much variables are dumped can be changed (i.e. setting a looser cut).


The following output is created when this is ran, the output root file can be found here:
```
testLocal/data-sbottom/mc16_13TeV.388737.MGPy8EG_A14N_BB_direct_1200_1.deriv.DAOD_SUSY7.e5453_e5984_a875_r9364_r9315_p3529.root
```
This is the so called 'vector' ntuple. A second processing step called ExportTrees can be used locally to add all the kinematic variables you would even want from this vector ntuple to produce a flat mini-ntuple.

### Running multiple analyses

MasterShef is also capable of running multiple analyses. These are comma separated names. The analyses can be defined different: different susy-tools config files (aka different object definitions), different variables to be filled and different points in which to 


The following command with run *both* the sbottom analysis and the sbottom-multib analysis with 200 events on a ttZ(vv) sample:
```
runMasterShef.py --inputLocalFile /atlas/shatlas/DAODS/rel21/p3563/mc16_13TeV.410156.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttZnunu.deriv.DAOD_SUSY1.e5070_e5984_s3126_r10201_r10210_p3596/ --analysis sbottom,sbottom_multib --maxEvents=200 

```

The output now displays the following information about what was dumped to the root files by the code:
```
MasterShef::MasterShef    INFO    CutFlow for analysis sbottom
MasterShef::MasterShef    INFO    CutFlow for Variation 
...
Cut 12  NBJets<=100  counts 24 Weighted 2.63601   PU weighted 1.96833
Cut 13  MET  counts 15 Weighted 2.30649   PU weighted 1.83111   <====== dumping ntuple sbottom here 

 
MasterShef::MasterShef    INFO    CutFlow for analysis sbottom_multib
MasterShef::MasterShef    INFO    CutFlow for Variation 
...
Cut 9  1000>= NJets >= 4  counts 35 Weighted 2.30657   PU weighted 1.78419
Cut 10  SbMb loose selection (0-2 lep, >=2 bjets)  counts 15 Weighted 2.30653   PU weighted 1.8367   <====== dumping ntuple sbottom_multib here 
Cut 11  SbMb tight selection (>=3 bjets || high metsig)  counts 11 Weighted 0.988515   PU weighted 0.595818
```

We can see that different selections were applied. In the ouput folder (__Note__: which can be specified with --outputDirectory) two root files can be found:
```
$ ls testLocal/data-sbottom*/*.root
testLocal/data-sbottom/mc16_13TeV.root  testLocal/data-sbottom_multib/mc16_13TeV.root
```


### Running systematics

Systematic variation (on the kinematics) can be specified in the python files for each analysis. They can also be dumped at a different level to the nominal tree, typically you do not need systematics for all events. These can be turned on with the `--doSyst` flag.

In `/MasterShef/python/analyses/_sbottom.py` we define some systematics like this:
```
if (alg.m_dataSource!=0 and self.doSyst==True):
        alg.m_systNameList.push_back("EG_RESOLUTION_ALL__1down")
        alg.m_systNameList.push_back("EG_RESOLUTION_ALL__1up")
```
These get added when the flag `--doSyst` is selected (and the sample is not data of course!).

Run the above example on the sbottom-multib analysis only, turning on systematics:
```
runMasterShef.py --inputLocalFile /atlas/shatlas/DAODS/rel21/p3563/mc16_13TeV.410156.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttZnunu.deriv.DAOD_SUSY1.e5070_e5984_s3126_r10201_r10210_p3596/ --analysis sbottom_multib --maxEvents=200  --doSyst
```

This gives us the following output...
```
MasterShef::MasterShef    INFO    CutFlow for analysis sbottom_multib
MasterShef::MasterShef    INFO    CutFlow for Variation 
...
Cut 10  SbMb loose selection (0-2 lep, >=2 bjets)  counts 15 Weighted 2.30653   PU weighted 1.8367   <====== dumping ntuple sbottom_multib here 
Cut 11  SbMb tight selection (>=3 bjets || high metsig)  counts 11 Weighted 0.988515   PU weighted 0.595818

MasterShef::MasterShef    INFO    CutFlow for Variation EG_RESOLUTION_ALL__1down
..
Cut 10  SbMb loose selection (0-2 lep, >=2 bjets)  counts 15 Weighted 2.30653   PU weighted 1.8367
Cut 11  SbMb tight selection (>=3 bjets || high metsig)  counts 11 Weighted 0.988515   PU weighted 0.595818   <====== dumping ntuple sbottom_multib here 

MasterShef::MasterShef    INFO    CutFlow for Variation EG_RESOLUTION_ALL__1up
...
Cut 10  SbMb loose selection (0-2 lep, >=2 bjets)  counts 15 Weighted 2.30653   PU weighted 1.8367
Cut 11  SbMb tight selection (>=3 bjets || high metsig)  counts 11 Weighted 0.988515   PU weighted 0.595818   <====== dumping ntuple sbottom_multib here 
...
...
```

As can be seen the 15 events are dumped for the nominal tree after the loose selection. We also fill two additional trees (systematic variation with egamma resolution up/down) but these are dumped after a tighter selection.

These will appear as differently named trees in the output vector ntuple:
```
OBJ: TTree	Nominal	m_tree : 0 at: 0x1b563950
OBJ: TTree	EG_RESOLUTION_ALL__1down	m_tree : 0 at: 0x1b563df0
OBJ: TTree	EG_RESOLUTION_ALL__1up	m_tree : 0 at: 0x1b5642e0

```


### Checking the output

Firstly it is always good to check the output after running MasterShef for WARNING and ERROR messages. 

This is an example, running the `stop0L_truth` analysis on a TRUTH3 sample, piping the output into a file called `run.log`
```
runMasterShef.py --analysis stop0L_truth --doTruth --inputLocalFile /atlas/shatlas/DAODS/TRUTH/SbottomMultib/mc15_13TeV.407345.PhPy8EG_A14_ttbarMET200_300_hdamp258p75_nonallhad.deriv.DAOD_TRUTH3.e6414_e5984_p3555/ --maxEvents=10000 |& tee run.log
```

We then check the log for errors and warnings via `grep`...
```
$ grep -e WARNING -e ERROR run.log 
xAOD::TEvent::retrieve    WARNING Couldn't (non-const) retrieve "DataVector<xAOD::Jet_v1>/AntiKt4TruthJets"
xAOD::TEvent::connectM... WARNING Branch "FileMetaData" not available on input
xAOD::TEvent::retrieve... WARNING Couldn't (const) retrieve "xAOD::FileMetaData_v1/FileMetaData"
MasterShef                ERROR   Failed to execute: "event->retrieveMetaInput(fmd, "FileMetaData")"
xAOD::TEvent::connectB... WARNING Branch "TruthParticles" not available on input
xAOD::TEvent::retrieve    WARNING Couldn't (const) retrieve "DataVector<xAOD::TruthParticle_v1>/TruthParticles"
UnnamedAlgorithm1        WARNING Cannot retrieve TruthParticles containers !
```

For a TRUTH3 sample we expect that there are no `TruthParticle` collection since this is removed in TRUTH3 and also we expect that the FileMetaData is missing as this tends to be for reco samples







## 2. Running on the Grid

This section provides notes on how to run samples on the grid and some tips on bookkeeping.

### Submitting samples to the grid

To submit to the grid we need to provide MasterShef a list of containers to run on. You can prepare these, for example by doing:
```
rucio list-dids mc16_13TeV:mc16_13TeV.*JZ*SUSY1.*p3563 --filter type=container --short | sort -n | tee mysamples.list
```
This will grab all mc16 SUSY1 dijet samples with p-tag p3563 into a list. This list can be edited and you can comment out samples with '#' if you do not wish to submit them.

The MasterShef package also contains many list of common samples that should be kept up-to-date. E.g. for SUSY1 p3563:
```
$ ls GridAnalysis/source/MasterShef/share/sample_lists/Common/SUSY1/mc16a/p3563/*.list -1 
GridAnalysis/source/MasterShef/share/sample_lists/Common/SUSY1/mc16a/p3563/diboson.list
GridAnalysis/source/MasterShef/share/sample_lists/Common/SUSY1/mc16a/p3563/dijet.list
GridAnalysis/source/MasterShef/share/sample_lists/Common/SUSY1/mc16a/p3563/singletop.list
GridAnalysis/source/MasterShef/share/sample_lists/Common/SUSY1/mc16a/p3563/tmplist.list
GridAnalysis/source/MasterShef/share/sample_lists/Common/SUSY1/mc16a/p3563/topEW.list
GridAnalysis/source/MasterShef/share/sample_lists/Common/SUSY1/mc16a/p3563/ttbar.list
GridAnalysis/source/MasterShef/share/sample_lists/Common/SUSY1/mc16a/p3563/W_sherpa_221.list
GridAnalysis/source/MasterShef/share/sample_lists/Common/SUSY1/mc16a/p3563/Z_sherpa_221.list
```

These can be combined into a single file to run on all of them.
```
cat GridAnalysis/source/MasterShef/share/sample_lists/Common/SUSY1/mc16a/p3563/*.list >> mysamples.list
```

To run on the grid (with the stop0L analysis) the basic command is:
```
lsetup panda
runMasterShef.py --analysis stop0L --driver prun --inputContainerList  mysamples.list
```

The grid jobs are given a name in the form of `user.<username>.<mastershef tag>.<date>.<DSID>.<ami-tags>`. The output .root file will appear with the name `user.<username>.<mastershef tag>.<date>.<DSID>.<ami-tags>.<analysis name>.root`

The so called 'MasterShef tag' is built from the Analysis-Base release and the git hash of your last commit hash (`git rev-parse --verify -short HEAD`). The date is also set in the form <Month><Day> (e.g. Aug12). The date can be manually forced with the option `--date <DATE>` to help with bookkeeping.



### Using pandamon

Display information
```
pandamon user.cmacdona 
```

Display failed jobs information (same for broken/finished)
```
pandamon user.cmacdona -i failed
#for more information
pandamon user.cmacdona -i failed -m 
#less information, task name only
pandamon user.cmacdona -i failed -t 
```

List all failed tasks and pipe them to be resubmitted
```
pandamon user.cmacdona -i failed -t | panda-resub-taskid 
```

List my jobs corresponding to a particular tag and date of the production
```
pandamon user.cmacdona*21.2.39-b3f7858*Aug04* 
```

Get a list of input samples corresponding to the jobs that have so far been marked as “done"
```
pandamon user.cmacdona*21.2.39-b3f7858*Aug04* -i done -s IN
```
Get all output files for a particular production
```.
pandamon user.cmacdona*21.2.39-b3f7858*Aug04*  -s OUT 
```
Get only the .root output files (skip hist/)
```
pandamon user.cmacdona*21.2.39-b3f7858*Aug04* -s OUT | grep .root
```


## 3 .Code Strucutre


After building and running the package you will see the following structure..

```
$ tree -d -L 2 .      
.
|-- build
|   |-- BoostedJetTaggers
|   |-- CMakeFiles
|   |-- JetSmearing
|   |-- MasterShef
|   |-- Testing
|   `-- x86_64-slc6-gcc62-opt
|-- run
|   `-- testLocal
`-- source
    |-- BoostedJetTaggers
    |-- JetSmearing
    `-- MasterShef
```

`build` is where all the built .o files and installed python modules are put. This directory can be accessed via `$MasterShef_DIR`. 

`source` is where all the source code is kept and can be accessed via the path `$MasterShef_DIR/../../source`. It contains the main MasterShef code as well as additional packages that we might need to have checked out local, e.g. the JetSmearing package to perform the multi-jet estimatation.

`run` is a custom directory, it could be called whatever you wish. This is a clean directory which you can execute the code from within.



### Structure of MasterShef source code

MasterShef code is structured as follows...

```
$ tree -d -L 1 source/MasterShef/
source/MasterShef/
|-- MasterShef    # contains header files
|-- Root          # contains the main .cxx EL code
|-- cmt           # contains old configuration for RootCore --> redundant now
|-- python        # contains python driving scripts and modules than can be installed
|-- scripts       # contains old script for performing a variety of useful tasks. These are not installed
|-- share         # contains sample lists, pileup files, analysis configurations etc.
`-- util          # contains more old scripts, mot of which are redundant now..
```

Below are some notes and information on the structure of each sub-directory.

#### MasterShef

* contains header files needed by the MasterShef package

```
source/MasterShef/MasterShef/
|-- ClassifyAndCalculateHF.h  #old variables for strong-multi-b-esk HF classification
|-- DataMembersHF.h           #^ same, this should be removed at some point
|-- HFSystDataMembers.h       #^ same again
|-- MasterShef.h              # The main header file for the package
|-- PDGCode.h                 # old code containing PDG codes for JetSmearing R-map construction
|-- TMctLib.h                 # includes for mct tool
|-- mctlib.h                  # ^ same
|-- ntupVar.h                 # The mian header file for ntuple variable names that are written
`-- selection.xml             # configuration for athena/ROOT installation
```

#### Root

* contains the main .cxx code 

```
source/MasterShef/Root/
|-- AdditionalTruthInfo.cxx     # functions for saving additional truth information for a reco sample
|-- ClassifyAndCalculateHF.cxx  # old variables for strong-multi-b-esk HF classification
|-- DataMembersHF.cxx           # ^ same
|-- FatJets.cxx                 # functions for preparing fat-jets as well as reclustering smaller R jets
|-- HFSystDataMembers.cxx       # ^ same as  DataMembersHF.cxx , most likely redundant now
|-- JSS.cxx                     # functions for performing and JetSubStructure, e.g. colour-flow variables
|-- JetSmearing.cxx             # functions for performing JetSmearing
|-- Jets.cxx                    # functions for retrieving baseline and signals AntiKt4 jets and b-jets
|-- Leptons.cxx                 # functions for retrieving Leptons (including hadronic taus)
|-- LinkDef.h                   # Links for building MasterShef ROOT library/dict so can be driven by Python
|-- MET.cxx                     # functions for retrieving MET and MET related quantities
|-- MasterShef.cxx              # Main body of the code for setting up EL and initialising tools
|-- Photons.cxx                 # functions for retrieving photons
|-- Selections.cxx              # functions for selecting and filtering on events to be dumped to output ntuples
|-- TMctLib.cxx                 # mct tool
|-- TreeMaker.cxx               # Main functions for choosing which varaibles are dumped to the output ntuple
|-- TriggerWeights.cxx          # function for loading prescale weights for jet triggers --> redundant now
|-- TruthObjects.cxx            # functions for retrieving jets, leptons, met but at TRUTH level!
`-- mctlib.cxx                  # mct tool
```
#### share

* contains all files that need to be used by MasterShef such as GRLs, pileup files, confs...


```
source/MasterShef/share/
|-- GRL                  # contains GRLs we should be using 
|-- RMap_SysVars         # Unoffical JetSmearing RMap systematic variations
|-- RMaps                # Unoffical JetSmearing RMaps 
|-- confs                # Contains SUSYTools configuration files, e.g. where lepton pTs are defined
|-- other_confs          # Other types of configuration files, such as ones used by TauTools
|-- pileup               # .root piles for doing pileup reweighting --> redundant now that we use autoConfiguration
|-- sample_lists         # list of samples to be ran on for convience
|-- sample_passGRL.py    # simple script for checking if the data samples pass the GRL --> could be moved
|-- systematics          # list of systematics output by SUSYTools that we may/may-not need to run on
|-- top_taggers          # additional configuration for top_taggers (custom 90%)
`-- trigger_lists        # files containing lists of triggers we might want to dump to our ntuples

```

#### python

* contains main driving scripts and modules that we need installing

```
source/MasterShef/python/
|-- __init__.py                 # just makes this a module
|-- analyses                    # definition of analyses class for running 
|   |-- __init__.py             # contains initialisation of "analysis" python class
|   |-- _sbottom.py             # contains definition of sbottom analyses
|   |-- _stop.py                # contains definition of stop analyses
|   `-- old_analyses.txt        # old file of all analyses if we need them re-implementing
|-- pandamonium                 # package from Dan Guest (recursive checkout) for monitoring 
|   |-- README.md               # main README of original package
|   |-- panda-kill-taskid       # ^see package README or section in this README 'Notes for using pandamonium'
|   |-- panda-resub-taskid      # ^see package README or section in this README 'Notes for using pandamonium'
|   |-- panda-shortname         # ^see package README or section in this README 'Notes for using pandamonium'
|   |-- pandamon                # ^see package README or section in this README 'Notes for using pandamonium'
|-- pandatools
|   |-- README.md               # Old scripts for reading json files from files submitted to the grid
|   `-- read_json.py            
`-- runMasterShef.py            # Main MasterShef driver! use `runMasterShef --help` and READMEs 
```






## 4. Code Development
