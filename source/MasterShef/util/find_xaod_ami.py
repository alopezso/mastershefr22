import os,sys,subprocess
import pyAMI
import pyAMI.client
from pyAMI import *

client = pyAMI.client.Client(
  'atlas'
)


out1 = open("aod_list","w")
out2 = open("bad_list","w")


def main():
    ami = client
    for dsname in open(sys.argv[1]):
        dsname = dsname.rstrip().replace("/","")
        if "#" in dsname or len(dsname.split("."))<2:
            continue
        #print "line=",dsname,dsname.split("."),len(dsname.split("."))
        if ":" in dsname:
            dsname=dsname.split(":")[1]
        
        
        aodname=dsname.replace("DAOD_SUSY1","AOD").split("_p")[0]
        dsname=aodname
        
        lcmd = ['GetDatasetInfo','logicalDatasetName=%s' % dsname]
        try:
            result = ami.execute(lcmd,format='dict_object')
        except ValueError:
            continue
        mydict=result.get_rows()[0]
        #print "--------------------------------------"
        print "%15s %s "%(mydict[u'totalEvents'],dsname)
        if(float(mydict[u'totalEvents'])>0):
            out1.write(dsname+"\n")
        else:
            out2.write(dsname+"\n")
        
        #for key in mydict:
        #    print key,mydict[key]
        #print "total size=",mydict[u'totalSize']
        #print "total size (MB)=",float(mydict[u'totalSize'])/1E6
        #print "total size (GB)=",float(mydict[u'totalSize'])/1E9
        #print "nfiles=",mydict[u'nFiles']
        #print "total events=",mydict[u'totalEvents']
        #print "--------------------------------------"

if __name__ == "__main__":
    main() 
