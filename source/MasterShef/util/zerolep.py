#!/usr/bin/env python
isData=False
isAtlfast=True


import os,ROOT,sys
print "import of modules complete..."
ROOT.gROOT.Macro(os.environ['ROOTCOREDIR']+"/scripts/load_packages.C")
print "loading of packages complete"

submitDir="testPython3"
entries=0
triggerlist=""

for i in range(1,len(sys.argv)):
    key=sys.argv[i].split("=")[0]
    val=sys.argv[i].split("=")[1]
    print "processing key",key, "with value=",val

    if key=="entries" or key=="maxevents":
        entries=int(val)
    elif key=="--dir":
        submitDir=val
    elif key=="--trigList" or key=="--triggerlist":
        triggerlist=val  



# Set up the job for xAOD access:
ROOT.xAOD.Init().ignore(
)
# create a new sample handler to describe the data files we use
sh=ROOT.SH.SampleHandler()

# scan for datasets in the given directory
# use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
#inputFilePath = ROOT.gSystem.ExpandPathName ("/atlas1/macdonald/DAODs/p2353/");
#ROOT.SH.ScanDir().sampleDepth(1).samplePattern("DAOD_SUSY1.05555981._000019.pool.root.1").scan(sh, inputFilePath)


#inputFilePath = ROOT.gSystem.ExpandPathName ("/atlas1/macdonald/DAODs/p2375/mc15_13TeV.361460.Sherpa_CT10_Znunu_Pt700_1000_CFilterBVeto.merge.DAOD_SUSY1.e3741_s2608_s2183_r6793_r6264_p2375")

#inputFilePath = ROOT.gSystem.ExpandPathName ("/atlas1/macdonald/DAODs/p2375/mc15_13TeV.410013.PowhegPythiaEvtGen_P2012_Wt_inclusive_top.merge.DAOD_SUSY7.e3753_s2608_s2183_r6630_r6264_p2375")
#inputFilePath = ROOT.gSystem.ExpandPathName ("/atlas1/macdonald/xAODs/mc15_13TeV.387063.MGPy8EG_A14N_BB_direct_800_1.merge.AOD.e3994_a766_a768_r6282/")

inputFilePath = ROOT.gSystem.ExpandPathName ("/atlas1/macdonald/DAODs/p2419/mc15_13TeV.387063.MGPy8EG_A14N_BB_direct_800_1.merge.DAOD_SUSY7.e3994_a766_a768_r6282_p2419/")

inputFilePath = ROOT.gSystem.ExpandPathName ("/atlas1/macdonald/DAODs/p2452/mc15_13TeV.361446.Sherpa_CT10_Znunu_Pt0_70_BFilter.merge.DAOD_SUSY7.e3651_s2586_s2174_r7267_r6282_p2452/")



#inputFilePath = ROOT.gSystem.ExpandPathName ("/atlas1/macdonald/DAODs/p2375/mc15_13TeV.407012.PowhegPythiaEvtGen_P2012CT10_ttbarMET200_hdamp172p5_nonAH.merge.DAOD_SUSY7.e4023_s2608_r6765_r6282_p2375")

#inputFilePath = ROOT.gSystem.ExpandPathName ("/atlas1/macdonald/DAODs/p2411/mc15_13TeV.407012.PowhegPythiaEvtGen_P2012CT10_ttbarMET200_hdamp172p5_nonAH.merge.DAOD_SUSY5.e4023_s2608_r6765_r6282_p2411")

inputFilePath = ROOT.gSystem.ExpandPathName ("/atlas1/macdonald/DATA15/data15_13TeV.00276954.physics_Main.merge.DAOD_SUSY7.f620_m1480_p2425/")

inputFilePath = ROOT.gSystem.ExpandPathName ("/atlas1/macdonald/DAODs/p2470/mc15_13TeV.387063.MGPy8EG_A14N_BB_direct_800_1.merge.DAOD_SUSY7.e3994_a766_a810_r6282_p2470/")



#ROOT.SH.ScanDir().sampleDepth(1).samplePattern("DAOD_SUSY7.05727080._000001.pool.root.1")
ROOT.SH.scanDir(sh, inputFilePath)  
sh.printContent()
#inputFilePath = ROOT.gSystem.ExpandPathName ("/atlas1/macdonald/DATA15/data15_13TeV.00267638.physics_Main.merge.AOD.r6848_p2358/")
#ROOT.SH.ScanDir().sampleDepth(1).samplePattern("AOD.05753864._002123.pool.root.1").scan(sh, inputFilePath)
#ROOT.SH.scanDir(sh, inputFilePath)         





# set the name of the tree in our files
# in the xAOD the TTree containing the EDM containers is "CollectionTree"
sh.setMetaString ("nc_tree", "CollectionTree")

# this is the basic description of our job using sample handler
job=ROOT.EL.Job()
job.sampleHandler (sh)
# for testing purposes, limit to run over the first 500 events
# job.options().setDouble (ROOT.EL.Job.optMaxEvents, 2000)
job.options().setString(ROOT.EL.Job.optXaodAccessMode,ROOT.EL.Job.optXaodAccessMode_branch)

# add our algorithm to the job
alg = ROOT.MasterShef()

# Basic algorithm settings:
if isData:
    alg.m_dataSource=ROOT.ST.Data
else:
    if isAtlfast:
        alg.m_dataSource=ROOT.ST.AtlfastII
    else:
        alg.m_dataSource=ROOT.ST.FullSim

alg.m_EleId="TightLLH"
alg.m_TauId="Tight"
alg.m_EleIsoWP="GradientLoose"
#alg.m_BadJetType="TightBad"
alg.m_jetInputType=ROOT.xAOD.JetInput.EMTopo

alg.m_showerType=0


triggerlist="MasterShef/share/trigger_lists/SUSY7_mylist_test1.txt"
triggerlist=""
# Trigger
alg.m_applyTrigger=True
if triggerlist=="":
    alg.m_triggerList.push_back("HLT_xe70")
#alg.m_triggerList.push_back("HLT_mu26_imedium")
    #alg.m_triggerList.push_back("HLT_e24_tight_iloose")
    #alg.m_triggerNtup.push_back("HLT_mu26_imedium")
    #alg.m_triggerNtup.push_back("HLT_e24_tight_iloose")
else:
    ftrig = open(triggerlist)
    for line in ftrig:
        line=line.rstrip()
        # protect against blank lines or comments
        lst = ["HLT","L1","L2"]
        if any( s in line for s in lst):
            triggername=line
            print "Adding trigger from file list..",triggername
            alg.m_triggerList.push_back(triggername)
            alg.m_triggerNtup.push_back(triggername)


## systematics test
#
#alg.m_systNameList.push_back("JET_BJES_Response__1up")
#alg.m_systNameList.push_back("JET_BJES_Response__1down")
#alg.m_systNameList.push_back("JET_EffectiveNP_1__1up")
#alg.m_systNameList.push_back("JET_EffectiveNP_1__1down")

#alg.m_systNameList.push_back("JET_BJES_Response__1down")
#alg.m_systNameList.push_back("JET_GroupedNP_1__1up")
#alg.m_systNameList.push_back("JET_GroupedNP_1__1down")




alg.m_config="MasterShef/confs/sbottom.conf"


# Cutflow config
#
alg.m_selMods.push_back("initialCuts")
alg.m_selMods.push_back("prepareObjects")

# uncomment if you want to dump an event
alg.m_selMods.push_back("eventDump")
alg.m_eventDump.push_back(21918)

alg.m_selMods.push_back("cleanJets")
alg.m_selMods.push_back("cleanMuons")

# Add the 1L selection with defaults settings
#alg.m_selMods.push_back("Sel2Lor2B")
alg.m_selMods.push_back("SelMET")

alg.m_selMods.push_back("SelJet")

alg.m_selMods.push_back("SelNBJet")

alg.m_selMods.push_back("SelLeadB")


alg.m_SelMET_PtMn=100000

alg.m_SelJet_ptMn.push_back(50000.)
alg.m_SelJet_ptMx.push_back(1.0E08)
# 2nd jet 50<pt<1.0E5
alg.m_SelJet_ptMn.push_back(50000.)
alg.m_SelJet_ptMx.push_back(1.0E08)
# 3rd jet 0<pt<1.0E5
alg.m_SelJet_ptMn.push_back(0.)
alg.m_SelJet_ptMx.push_back(1.0E08)
# 4th jet 0<pt<50
alg.m_SelJet_ptMn.push_back(0.)
alg.m_SelJet_ptMx.push_back(50000.)

alg.m_SelNBJet_nJMn=2
alg.m_SelNBJet_nJMx=2

#alg.m_selMods.push_back("cleanTightJets")

# Define dumper variables (remove comment if want dumper)
alg.m_cutDumpLvl=8

# Add ntuple functionality
# define an output and an ntuple associated to that output
output = ROOT.EL.OutputStream("myOutput")
job.outputAdd(output);
ntuple = ROOT.EL.NTupleSvc("myOutput")
job.algsAdd(ntuple)
# define at which cut level the ntuple is written
alg.m_ntupLevel=9

# later on we'll add some configuration options for our algorithm that go here
job.algsAdd (alg);

# Set the ntuple outpt name
alg.m_outputName = "myOutput"

# make the driver we want to use:
# this one works by running the algorithm directly:
driver=ROOT.EL.DirectDriver()





job.options().setDouble(ROOT.EL.Job.optRemoveSubmitDir,1);
if entries>0:       
    job.options().setDouble(ROOT.EL.Job.optMaxEvents, entries)


driver.submit (job, submitDir)
