#!/usr/bin/env python
isData=False
isAtlfast=False

import os,ROOT
ROOT.gROOT.Macro(os.environ['ROOTCOREDIR']+"/scripts/load_packages.C")

# Set up the job for xAOD access:
ROOT.xAOD.Init().ignore()

# create a new sample handler to describe the data files we use
sh=ROOT.SH.SampleHandler()

# scan for datasets in the given directory
# use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
#inputFilePath = ROOT.gSystem.ExpandPathName ("/atlas1/macdonald/DAODs/p2395/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_SUSY7.e3698_s2608_s2183_r6630_r6264_p2395/");
inputFilePath = ROOT.gSystem.ExpandPathName ("/atlas1/macdonald/DAODs/p2375/mc15_13TeV.361380.Sherpa_CT10_Zee_Pt140_280_BFilter.merge.DAOD_SUSY7.e3651_s2586_s2174_r6853_r6264_p2375/")
#ROOT.SH.ScanDir().sampleDepth(1).samplePattern("DAOD_SUSY1.05555981._000019.pool.root.1").scan(sh, inputFilePath)
ROOT.SH.scanDir(sh, inputFilePath)

# set the name of the tree in our files
# in the xAOD the TTree containing the EDM containers is "CollectionTree"
sh.setMetaString ("nc_tree", "CollectionTree")
sh.printContent()
# this is the basic description of our job using sample handler
job=ROOT.EL.Job()
job.sampleHandler (sh)
# for testing purposes, limit to run over the first 500 events
# job.options().setDouble (ROOT.EL.Job.optMaxEvents, 2000)
job.options().setString(ROOT.EL.Job.optXaodAccessMode,ROOT.EL.Job.optXaodAccessMode_branch)

# add our algorithm to the job
alg = ROOT.MasterShef()

# Basic algorithm settings:
if isData:
    alg.m_dataSource=ROOT.ST.Data
else:
    if isAtlfast:
        alg.m_dataSource=ROOT.ST.AtlfastII
    else:
        alg.m_dataSource=ROOT.ST.FullSim

alg.m_EleId="TightLH"
alg.m_TauId="Tight"
alg.m_EleIsoWP="GradientLoose"
alg.m_jetInputType=ROOT.xAOD.JetInput.EMTopo

# Trigger
alg.m_applyTrigger=True
alg.m_triggerList.push_back("HLT_mu26_imedium")
alg.m_triggerList.push_back("HLT_e24_tight_iloose")
alg.m_triggerNtup.push_back("HLT_mu26_imedium")
alg.m_triggerNtup.push_back("HLT_e24_tight_iloose")

# Cutflow config
#
alg.m_selMods.push_back("initialCuts")
alg.m_selMods.push_back("prepareObjects")

# uncomment if you want to dump an event
# alg.m_selMods.push_back("eventDump")
# alg.m_eventDump.push_back(5818494)

alg.m_selMods.push_back("cleanJets")
alg.m_selMods.push_back("cleanMuons")

# Add the 1L selection with defaults settings
alg.m_selMods.push_back("Sel1L")


alg.m_SelMET_PtMn=80000.
# Add the MET selection with defaults settings
alg.m_selMods.push_back("SelMET")

# Add the Mt selection with defaults settings
alg.m_selMods.push_back("SelMt")



# Add the Jet selection 
alg.m_selMods.push_back("SelJet")

alg.m_SelJet_nJets=4
alg.m_SelJet_isInc=True
# pt ranges for the first 4 jets
# lead jet 50<pt<1.0E5
alg.m_SelJet_ptMn.push_back(50000.)
alg.m_SelJet_ptMx.push_back(1.0E08)
# 2nd jet 50<pt<1.0E5
alg.m_SelJet_ptMn.push_back(50000.)
alg.m_SelJet_ptMx.push_back(1.0E08)
# 3rd jet 0<pt<1.0E5
alg.m_SelJet_ptMn.push_back(0.)
alg.m_SelJet_ptMx.push_back(1.0E08)
# 4th jet 0<pt<50
alg.m_SelJet_ptMn.push_back(0.)
alg.m_SelJet_ptMx.push_back(50000.)


# Add the Jet selection 
alg.m_selMods.push_back("SelNBJet")




# Add the MET selection with defaults settings
alg.m_selMods.push_back("SelMET")



# Add the Jet selection 
#alg.m_selMods.push_back("SelNBJet")
alg.m_SelNBJet_nJMn=2
alg.m_SelNBJet_nJMx=2

# Define dumper variables (remove comment if want dumper)
# alg.m_cutDumpLvl=18

# Add ntuple functionality
# define an output and an ntuple associated to that output
output = ROOT.EL.OutputStream("myOutput")
job.outputAdd(output);
ntuple = ROOT.EL.NTupleSvc("myOutput")
job.algsAdd(ntuple)
# define at which cut level the ntuple is written
alg.m_ntupLevel=9

# later on we'll add some configuration options for our algorithm that go here
job.algsAdd (alg);

# Set the ntuple outpt name
alg.m_outputName = "myOutput"

# make the driver we want to use:
# this one works by running the algorithm directly:
driver=ROOT.EL.DirectDriver()

#process the job using the driver
submitDir="testPython2"
driver.submit (job, submitDir)
