import os,sys,subprocess
import pyAMI
import pyAMI.client
from pyAMI import *

client = pyAMI.client.Client(
  'atlas'
)



def main():
    ami = client
    for dsname in open(sys.argv[1]):
        dsname = dsname.rstrip().replace("/","")
        if "#" in dsname or len(dsname.split("."))<2:
            continue
        #print "line=",dsname,dsname.split("."),len(dsname.split("."))
        if ":" in dsname:
            dsname=dsname.split(":")[1]
        
            
        lcmd = ['GetDatasetInfo','logicalDatasetName=%s' % dsname]
        try:
            result = ami.execute(lcmd,format='dict_object')
        except ValueError:
            continue
        mydict=result.get_rows()[0]
        #print "--------------------------------------"
        #print "%15s %s "%(mydict[u'totalEvents'],dsname)

        temp=dsname.split(".")
        temp=".".join(temp[:len(temp)-1])+".*"
        p = subprocess.Popen(["rucio", "list-dids","--filter", "type=CONTAINER","--short",str(temp)],stdout=subprocess.PIPE)#str(temp)],)#"rucio list-dids ",temp])
        (out,err)=p.communicate()
        p.wait()
        out=out.split("\n")
        

        tag=dsname.split("_r")
        tag=float(tag[len(tag)-1])

        print dsname
        for item in out:
            name=item.split(":")
            if len(name)>1:
                if name[1]!=dsname:
                    newtag=name[1].split("_r")
                    newtag=float(newtag[len(newtag)-1])
                    if newtag>tag:
                        print "# newer tag! \n #",name


            
    

      
        #if(float(mydict[u'totalEvents'])>0):
        #    out1.write(dsname+"\n")
        #else:
        #    out2.write(dsname+"\n")
        
        #for key in mydict:
        #    print key,mydict[key]
        #print "total size=",mydict[u'totalSize']
        #print "total size (MB)=",float(mydict[u'totalSize'])/1E6
        #print "total size (GB)=",float(mydict[u'totalSize'])/1E9
        #print "nfiles=",mydict[u'nFiles']
        #print "total events=",mydict[u'totalEvents']
        #print "--------------------------------------"

if __name__ == "__main__":
    main() 
