#!/usr/bin/env python
isData=False
isAtlfast=False

import os,sys,ROOT

if len(sys.argv)!=2:
    print " incorrect usage! "
    print " ./gridTestRun.py <list_of_grid_samples>"
    exit(1)

ROOT.gROOT.Macro(os.environ['ROOTCOREDIR']+"/scripts/load_packages.C")



# Set up the job for xAOD access:
ROOT.xAOD.Init().ignore()

# create a new sample handler to describe the data files we use
sh=ROOT.SH.SampleHandler()

# scan for datasets in the given directory
# use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
# inputFilePath = ROOT.gSystem.ExpandPathName ("/atlas1/macdonald/DAODs/p2353/");
# ROOT.SH.ScanDir().sampleDepth(1).samplePattern("DAOD_SUSY1.05555981._000019.pool.root.1").scan(sh, inputFilePath)

samples = sys.argv[1]
for sam in open(samples):
    ROOT.SH.scanDQ2(sh , sam)  
sh.printContent()


# set the name of the tree in our files
# in the xAOD the TTree containing the EDM containers is "CollectionTree"
sh.setMetaString ("nc_tree", "CollectionTree")

# this is the basic description of our job using sample handler
job=ROOT.EL.Job()
job.sampleHandler (sh)
# for testing purposes, limit to run over the first 500 events
# job.options().setDouble (ROOT.EL.Job.optMaxEvents, 500)
job.options().setString(ROOT.EL.Job.optXaodAccessMode,ROOT.EL.Job.optXaodAccessMode_branch)

# add our algorithm to the job
alg = ROOT.MasterShef()

# Basic algorithm settings:
if isData:
    alg.m_dataSource=ROOT.ST.Data
else:
    if isAtlfast:
        alg.m_dataSource=ROOT.ST.AtlfastII
    else:
        alg.m_dataSource=ROOT.ST.FullSim

alg.m_EleId="TightLLH"
alg.m_TauId="Tight"
alg.m_EleIsoWP="GradientLoose"
alg.m_jetInputType=ROOT.xAOD.JetInput.EMTopo

# Trigger
alg.m_applyTrigger=True
alg.m_triggerList.push_back("HLT_mu26_imedium")
alg.m_triggerList.push_back("HLT_e24_tight_iloose")

# Cutflow config
#
alg.m_selMods.push_back("initialCuts")
alg.m_selMods.push_back("prepareObjects")
alg.m_selMods.push_back("cleanJets")
alg.m_selMods.push_back("cleanMuons")

# Add the 1L selection with defaults settings
alg.m_selMods.push_back("Sel1L")

# Add the MET selection with defaults settings
alg.m_selMods.push_back("SelMET")

# Add the Mt selection with defaults settings
alg.m_selMods.push_back("SelMt")

# Add ntuple functionality
# define an output and an ntuple associated to that output
output = ROOT.EL.OutputStream("myOutput")
job.outputAdd(output);
ntuple = ROOT.EL.NTupleSvc("myOutput")
job.algsAdd(ntuple)

# later on we'll add some configuration options for our algorithm that go here
job.algsAdd (alg);

# Set the ntuple outpt name
alg.m_outputName = "myOutput"

# make the driver we want to use:
# this one works by running the algorithm directly:
# driver=ROOT.EL.DirectDriver()
driver=ROOT.EL.PrunDriver()
driver.options().setString("nc_outputSampleName", "user.costanzo.test.%in:name[2]%.%in:name[5]%.%in:name[6]%");

#process the job using the driver
submitDir="testPython"
driver.submit (job, submitDir)
