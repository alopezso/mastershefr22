#!/usr/bin/env python
isData=True
isAtlfast=False

import os,ROOT,sys
ROOT.gROOT.Macro(os.environ['ROOTCOREDIR']+"/scripts/load_packages.C")

submitDir="testSUSY11jetsmear"
entries=0
triggerlist=""

for i in range(1,len(sys.argv)):
    key=sys.argv[i].split("=")[0]
    val=sys.argv[i].split("=")[1]
    print "processing key",key, "with value=",val

    if key=="entries" or key=="maxevents":
        entries=int(val)
    elif key=="--dir":
        submitDir=val
    elif key=="--trigList" or key=="--triggerlist":
        triggerlist=val  



# Set up the job for xAOD access:
ROOT.xAOD.Init().ignore()

# create a new sample handler to describe the data files we use
sh=ROOT.SH.SampleHandler()

inputFilePath = ROOT.gSystem.ExpandPathName ("/atlas1/macdonald/DATA15/data15_13TeV.00276262.physics_Main.merge.DAOD_SUSY7.f620_m1480_p2425")

ROOT.SH.scanDir(sh, inputFilePath)  
sh.printContent()





# set the name of the tree in our files
# in the xAOD the TTree containing the EDM containers is "CollectionTree"
sh.setMetaString ("nc_tree", "CollectionTree")

# this is the basic description of our job using sample handler
job=ROOT.EL.Job()
job.sampleHandler (sh)
# for testing purposes, limit to run over the first 500 events
# job.options().setDouble (ROOT.EL.Job.optMaxEvents, 2000)
job.options().setString(ROOT.EL.Job.optXaodAccessMode,ROOT.EL.Job.optXaodAccessMode_branch)

# add our algorithm to the job
alg = ROOT.MasterShef()





# Basic algorithm settings:
if isData:
    alg.m_dataSource=ROOT.ST.Data
else:
    if isAtlfast:
        alg.m_dataSource=ROOT.ST.AtlfastII
    else:
        alg.m_dataSource=ROOT.ST.FullSim

alg.m_EleId="TightLH"
alg.m_TauId="Tight"
alg.m_EleIsoWP="GradientLoose"
#alg.m_BadJetType="TightBad"
alg.m_jetInputType=ROOT.xAOD.JetInput.EMTopo



triggerlist=""#MasterShef/share/trigger_lists/SUSY7_mylist_test1.txt"

# Trigger
alg.m_applyTrigger=False
if triggerlist=="":
    alg.m_triggerList.push_back("HLT_j400");
    alg.m_triggerList.push_back("HLT_j360");
    alg.m_triggerList.push_back("HLT_j320");
    alg.m_triggerList.push_back("HLT_j260");
    alg.m_triggerList.push_back("HLT_j200");
    alg.m_triggerList.push_back("HLT_j175");
    alg.m_triggerList.push_back("HLT_j150");
    alg.m_triggerList.push_back("HLT_j110");
    alg.m_triggerList.push_back("HLT_j85");
    alg.m_triggerList.push_back("HLT_j60");
    alg.m_triggerList.push_back("HLT_j25");
    alg.m_triggerList.push_back("HLT_j15");
    #alg.m_triggerList.push_back("HLT_xe80")
#alg.m_triggerList.push_back("HLT_mu26_imedium")
    #alg.m_triggerList.push_back("HLT_e24_tight_iloose")
    #alg.m_triggerNtup.push_back("HLT_mu26_imedium")
    #alg.m_triggerNtup.push_back("HLT_e24_tight_iloose")
else:
    ftrig = open(triggerlist)
    for line in ftrig:
        line=line.rstrip()
        # protect against blank lines or comments
        lst = ["HLT","L1","L2"]
        if any( s in line for s in lst):
            triggername=line
            print "Adding trigger from file list..",triggername
            alg.m_triggerList.push_back(triggername)
            alg.m_triggerNtup.push_back(triggername)


## systematics test
#
#alg.m_systNameList.push_back("JET_BJES_Response__1up")
#alg.m_systNameList.push_back("JET_BJES_Response__1down")
#alg.m_systNameList.push_back("JET_GroupedNP_1__1up")
#alg.m_systNameList.push_back("JET_GroupedNP_1__1down")




# Cutflow config
#
alg.m_selMods.push_back("initialCuts")
alg.m_selMods.push_back("prepareObjects")

# uncomment if you want to dump an event
# alg.m_selMods.push_back("eventDump")
# alg.m_eventDump.push_back(5818494)

alg.m_selMods.push_back("cleanJets")
alg.m_selMods.push_back("cleanMuons")



alg.m_selMods.push_back("SelNBJet")

alg.m_selMods.push_back("SelJet")


alg.m_selMods.push_back("jetSmearing")
alg.m_METsigSeedCut = 2.7
alg.m_nsmears=1000

#alg.m_selMods.push_back("SelLeadB")


# Add the 1L selection with defaults settings
#alg.m_selMods.push_back("Sel2Lor2B")
#alg.m_selMods.push_back("SelMET")








alg.m_SelJet_ptMn.push_back(50000.)
alg.m_SelJet_ptMx.push_back(1.0E08)
# 2nd jet 50<pt<1.0E5
alg.m_SelJet_ptMn.push_back(50000.)
alg.m_SelJet_ptMx.push_back(1.0E08)
# 3rd jet 0<pt<1.0E5
alg.m_SelJet_ptMn.push_back(0.)
alg.m_SelJet_ptMx.push_back(1.0E08)
# 4th jet 0<pt<50
alg.m_SelJet_ptMn.push_back(0.)
alg.m_SelJet_ptMx.push_back(50000.)

alg.m_SelNBJet_nJMn=2
alg.m_SelNBJet_nJMx=2

#alg.m_selMods.push_back("cleanTightJets")

# Define dumper variables (remove comment if want dumper)
alg.m_cutDumpLvl=15
alg.m_cutDumpLvl_sys=15
#alg.m_cutDumpLvl_smr=15

# Add ntuple functionality
# define an output and an ntuple associated to that output
output = ROOT.EL.OutputStream("myOutput")
job.outputAdd(output);
ntuple = ROOT.EL.NTupleSvc("myOutput")
job.algsAdd(ntuple)
# define at which cut level the ntuple is written
alg.m_ntupLevel=11

# later on we'll add some configuration options for our algorithm that go here
job.algsAdd (alg);

# Set the ntuple outpt name
alg.m_outputName = "myOutput"

# make the driver we want to use:
# this one works by running the algorithm directly:
driver=ROOT.EL.DirectDriver()





job.options().setDouble(ROOT.EL.Job.optRemoveSubmitDir,1);
if entries>0:       
    job.options().setDouble(ROOT.EL.Job.optMaxEvents, entries)


driver.submit (job, submitDir)
