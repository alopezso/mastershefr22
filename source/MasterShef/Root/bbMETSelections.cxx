#include <MasterShef/MasterShef.h>
//#include "CPAnalysisExamples/errorcheck.h"

//static const char* APP_NAME = "MasterShef::bbMET::Selections";
static SG::AuxElement::ConstAccessor<float> acc_GenFiltMET("GenFiltMET"); //!

bool MasterShef :: SelbbMeT(bool init) {
  if (init) {
    m_cuts[m_cutLevel++]="bbMeT loose selection ";
    m_cuts[m_cutLevel++]="bbMeT tight selection (for systematics)";
    Info("SelbbMeT()","Initialising bbMeT analysis Selection selector.... ");
    return true;
  }
  unsigned int nsigEl  = m_signalElectrons->size();
  unsigned int nsigMu  = m_signalMuons->size();
  unsigned int nsigLep  = nsigEl + nsigMu;
  unsigned int nbaseLep = m_baselineElectrons->size()+m_baselineMuons->size(); //6/7 GeV
  unsigned int nbjets   = m_BJets->size();
  unsigned int njets    = m_signalJets->size();
  unsigned int njets35  = m_signalJets35->size();

  double met=m_etmiss_prime/1000.;
  double met_orig=m_etmiss/1000.;
 
  double mll = 0;
  TLorentzVector Z;
  if(nsigEl==2){
    Z=(m_signalElectrons->at(0)->p4() +  m_signalElectrons->at(1)->p4());
    mll=Z.M()/1000.;
  }
  else if(nsigMu==2){
    Z=(m_signalMuons->at(0)->p4() +  m_signalMuons->at(1)->p4());
    mll=Z.M()/1000.;
  }
  


  //save variables for tight and loose soft vertexing
  int nsbv_L = m_goodSBV_Loose->size(); //filtering on loose vertices only. 
  
  //get the leading jet pT
  double pTj1=0;
  double HT=0;
  if(m_signalJets->size()>0){
    pTj1=m_signalJets->at(0)->pt()/1000. ;
    for (const auto& jet : *m_signalJets ) HT+=jet->pt()/1000.;
  }

  //start to build skims for the soft (SRC) selection
  bool passSoftSelection = nsbv_L>=1 &&  met>250  && pTj1>250;
  
  //generic loose sbottom selection
  bool passSbottomSelection = nbjets>=2 && met>250 && pTj1>50;
  
  //met>180 and pTj1>80 are safe for the trigger plateau
  //adding metsigST>7 to reduce events a bit
  bool passDmSelection = nbjets>=2 && njets>=2 && njets<=3 &&  met>180  && m_metsigST>7 && pTj1>80;
  
  //do a looser veto to see if baseline veto is the best thing for the SRC
  bool passSelection0L = (nsigLep == 0  && nbaseLep == 0 &&   ( passSoftSelection || passSbottomSelection || passDmSelection));
  bool passSelection1L = (nsigLep == 1  && nbaseLep == 1 &&  ( passSoftSelection || passSbottomSelection || passDmSelection));
  bool passSelection2L = (nsigLep == 2  && nbaseLep == 2 &&  (nsigMu==2 || nsigEl==2) && ( passSoftSelection || passSbottomSelection || passDmSelection));
  
  //2-4(5) jets
  bool passJetSelection = njets <= 5 && njets35<=4;

  //total selection
  bool passSelection = passJetSelection && ( passSelection0L ||  passSelection1L || passSelection2L);

  //nominal ttbar and single top, met slice overlap
  if(m_isMC){
    int dsid= eventInfo->mcChannelNumber(); 
    // ttbar            tW(DR)            t*W(DR)           tW(DS)         t*W(DS)          
    if(dsid==410470 || dsid==410646 || dsid==410647 || dsid==410654 || dsid==410655 ){
      if(acc_GenFiltMET.isAvailable(*eventInfo)){
	float GenFiltMET = acc_GenFiltMET(*eventInfo)/1000.;
	if(GenFiltMET > 200) passSelection=false;
      }
    }
  }
  
  if(!passSelection){
    failCut();
    return false;
  }
  
  passCut();

  double m_1sbv_loose=-99.;
  double pT_1sbv_loose=-99.;
  if(nsbv_L>0){
    auto v = m_goodSBV_Loose->at(0);
    TLorentzVector totalFourMomentum;
    for (size_t i = 0; i < v->nTrackParticles(); i++) {
      const xAOD::TrackParticle *trk = v->trackParticle(i);
      totalFourMomentum += trk->p4();
    }
    m_1sbv_loose=totalFourMomentum.M()/1000.;
    pT_1sbv_loose=totalFourMomentum.Pt()/1000.;
  }
  //go tighter in the selections for sytematics
  passSoftSelection = passSoftSelection && m_1sbv_loose > 0.6 && pT_1sbv_loose > 3 &&  (nsbv_L>=1 && met>400  && pTj1>400);
  passSbottomSelection = passSbottomSelection && pTj1>100;
  passDmSelection = passDmSelection && pTj1/HT > 0.7 && (njets>=2 && njets<=3);

  passSelection0L = (nsigLep == 0  &&  ( passSoftSelection || passSbottomSelection || passDmSelection));
  passSelection1L = (nsigLep == 1  &&  ( passSoftSelection || passSbottomSelection || passDmSelection));
  passSelection2L = (nsigLep == 2  &&  mll>70 && mll<110 && met_orig<100 && ( passSoftSelection || passSbottomSelection || passDmSelection));
  
  bool passTightSelection = passJetSelection && ( passSelection0L ||  passSelection1L || passSelection2L);
  if(!passTightSelection){
    failCut();
    return false;
  }

  passCut();
  
  return true;
  

}
