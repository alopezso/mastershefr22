#include "MasterShef/DataMembersHF.h"

#include <iostream>


//_________________________________________________________________________
//
DataMembersHF::DataMembersHF():
  EventNumber(0),
  mc_channel_number(0),
  mc_n(0),
  mc_pt(0),
  mc_eta(0),
  mc_phi(0),
  mc_status(0),
  mc_barcode(0),
  mc_pdgId(0),
  mc_charge(0),
  mc_parent_index(0),
  mc_child_index(0),
  
  jet_AntiKt4Truth_n(0),
  jet_AntiKt4Truth_E(0),
  jet_AntiKt4Truth_pt(0),
  jet_AntiKt4Truth_eta(0),
  jet_AntiKt4Truth_phi(0)
{}

//_________________________________________________________________________
//
DataMembersHF::DataMembersHF( const DataMembersHF &dm ){
  EventNumber = dm.EventNumber;
  mc_channel_number = dm.mc_channel_number;
  mc_n = dm.mc_n;
  mc_pt = dm.mc_pt;
  mc_eta = dm.mc_eta;
  mc_phi = dm.mc_phi;
  mc_status = dm.mc_status;
  mc_barcode = dm.mc_barcode;
  mc_pdgId = dm.mc_pdgId;
  mc_charge = dm.mc_charge;
  mc_parent_index = dm.mc_parent_index;
  mc_child_index = dm.mc_child_index;
  
  jet_AntiKt4Truth_n = dm.jet_AntiKt4Truth_n;
  jet_AntiKt4Truth_E = dm.jet_AntiKt4Truth_E;
  jet_AntiKt4Truth_pt = dm.jet_AntiKt4Truth_pt;
  jet_AntiKt4Truth_eta = dm.jet_AntiKt4Truth_eta;
  jet_AntiKt4Truth_phi = dm.jet_AntiKt4Truth_phi;
}

//_________________________________________________________________________
//
DataMembersHF::~DataMembersHF(){
  delete mc_pt;
  delete mc_eta;
  delete mc_phi;
  delete mc_status;
  delete mc_barcode;
  delete mc_pdgId;
  delete mc_charge;
  delete mc_parent_index;
  delete mc_child_index;
  delete jet_AntiKt4Truth_E;
  delete jet_AntiKt4Truth_pt;
  delete jet_AntiKt4Truth_eta;
  delete jet_AntiKt4Truth_phi;
}
