
#include <MasterShef/MasterShef.h>
//#include "CPAnalysisExamples/errorcheck.h"

//c++ memory libaries - e.g. allows us to access std::make_unique to create unique_ptr
#include <memory>

static const char* APP_NAME = "MasterShef";

bool MasterShef :: prepareTruthObjects(bool init){
  if (init) {
    m_doTruth=true;
    return true;
  } 
  
  if(m_debug>0)
    store->print();

  store->clear();

  xAOD::TEvent* event = wk()->xaodEvent();
  const xAOD::EventInfo* eventInfo = 0;
  EL_RETURN_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));


  ATH_MSG_DEBUG("Now going to retrieve muons");
  this->getTruthMuons(m_MuonPt,m_MuonEta);  //saves truth muons to  m_baselineMuonsBeforeOR
  ATH_MSG_DEBUG("Now going to retrieve electrons");
  this->getTruthElectrons(m_ElePt,m_EleEta);//saves truth electrons to m_baselineElectronsBeforeOR
  ATH_MSG_DEBUG("Now going to retrieve jets");
  this->getTruthJets(m_JetPt,m_JetEta); //saves truth jets  to  m_signalJetsBeforeOR
  //! no photons in TRUTH3!!
  if(xStream!="DAOD_TRUTH3"){
    this->getTruthPhotons(m_PhotonPt,m_PhotonEta); //saves truth photons to  m_signalPhotonsBeforeOR 
    ATH_MSG_DEBUG("Now going to retrieve truth b-hadrons");
    if(m_useHFTruthHadrons){
      this->getTruthHFHadrons(); //saves truth b-hadrons to TruthBHadrons in event store (and for c-hadrons)
    }
  }
  else{
    m_usePhotons=false;
  }

  ATH_MSG_DEBUG("Passed getting truth photons?");



  m_signalElectrons = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);
  m_signalMuons = new xAOD::MuonContainer(SG::VIEW_ELEMENTS);
  
  m_signalPhotons = new xAOD::PhotonContainer(SG::VIEW_ELEMENTS); 
  m_signalJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
  m_BJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
  m_signalJets_recl = new xAOD::IParticleContainer(SG::VIEW_ELEMENTS);
  //m_lep_recl = new xAOD::IParticleContainer(SG::VIEW_ELEMENTS);

  TLorentzVector v_jet(0,0,0,0);
  TLorentzVector v_el(0,0,0,0);
  TLorentzVector v_mu(0,0,0,0);
  TLorentzVector v_ph(0,0,0,0);

  for (const auto& jet: *m_signalJetsBeforeOR){

    //check for overlapping jets
    v_jet = jet->p4();
    bool overlap=false;
    for(const auto& el : *m_baselineElectronsBeforeOR){
      v_el=el->p4();
      if(v_jet.DeltaR(v_el) < 0.2){
	overlap=true;
	break;
      }
    }
    if(overlap) continue;
     
    if (m_usePhotons && (m_outputName.find("_ph")!=std::string::npos)){
      for(const auto& ph : *m_signalPhotonsBeforeOR){
	v_ph=ph->p4();
	if(v_jet.DeltaR(v_ph) < 0.4){
	  overlap=true;
	  break;
	}
      }
      if(overlap) continue;
    }
    //jet->auxdata< char >("passOR")=true;
    //passOR = jet->auxdata< char >("passOR");
    if (v_jet.Pt()> m_JetPt && abs(v_jet.Eta())<m_JetEta){
      m_signalJets->push_back(jet);
      m_signalJets_recl->push_back(jet);    
    }
    
    bool bjet =  jet->auxdata< char >("bjet");
    if(bjet)m_BJets->push_back(jet);
  
    
  }
  m_BJets->sort(ptsorter);
  m_signalJets->sort(ptsorter);
    

  ATH_MSG_DEBUG("Storing goodjets");
  CHECK( store->record(m_signalJets,"GoodTruthJets") );

  ATH_MSG_DEBUG("Storing bjets");
  CHECK( store->record(m_BJets,"TruthBJets") );

  //Remove leptons/photons overlapping with jets now
  for(const auto& el : *m_baselineElectronsBeforeOR){
    //el->auxdata< char >("passOR")=false;
    // bool passOR = el->auxdata< char >("passOR");
    v_el = el->p4();
    bool overlap=false;
    for(const auto& jet : *m_signalJets){
      v_jet = jet->p4();
      if( v_el.DeltaR(v_jet) < 0.4 ){
	overlap=true;
	break;
      }
    }
    if(overlap) continue;
    
    //el->auxdata< char >("passOR")=true;
    //passOR = el->auxdata< char >("passOR");
    //apply signal cuts...
    if(v_el.Pt()> m_ElePt && abs(v_el.Eta())<m_EleEta)
      m_signalElectrons->push_back(el);
  }  
  //sort the electrons in Pt
  m_signalElectrons->sort(ptsorter);

  ATH_MSG_DEBUG("Storing electrons");
  CHECK( store->record(m_signalElectrons,"SignalTruthElectrons") );


  ATH_MSG_DEBUG("Are you trying to store photons you mug?"<< m_usePhotons);
  if (m_usePhotons){
    for(const auto& ph : *m_signalPhotonsBeforeOR){
      //ph->auxdata< char >("passOR")=false;
      //bool passOR = ph->auxdata< char >("passOR");
      v_ph = ph->p4();
      bool overlap=false;
      for(const auto& el : *m_signalElectrons){
	v_el = el->p4();
	if( v_ph.DeltaR(v_el) < 0.1 ){
	  overlap=true;
	  break;
	}
      }
      if(overlap) continue;
       
      //ph->auxdata< char >("passOR")=true;
      //passOR = ph->auxdata< char >("passOR");
      //apply signal cuts...
      if(v_ph.Pt()> m_PhotonPt && abs(v_ph.Eta())<m_PhotonEta)   
        m_signalPhotons->push_back(ph);
    }  
  
 
    //sort the photons in Pt 
    m_signalPhotons->sort(ptsorter);
  }
  ATH_MSG_DEBUG("Storing photons");
  CHECK( store->record(m_signalPhotons,"GoodTruthPhotons") );

  for(const auto& mu : *m_baselineMuonsBeforeOR){
         
    v_mu = mu->p4();
    bool overlap=false;
    for(const auto& jet : *m_signalJets){
      v_jet = jet->p4();
      if( v_mu.DeltaR(v_jet) < 0.4 ){
	overlap=true;
	break;
      }
    }
    if(overlap) continue;
 
    //apply signal cuts...
    if(v_mu.Pt()> m_MuonPt && abs(v_mu.Eta())<m_MuonEta)
      m_signalMuons->push_back(mu);
  }  
  //sort the muons in Pt
  m_signalMuons->sort(ptsorter);

  ATH_MSG_DEBUG("Storing muons");
  CHECK( store->record(m_signalMuons,"SignalTruthMuons") );



  //if 1lep --> add lep to m_signalJets_recl     

  if ((m_signalElectrons->size() + m_signalMuons->size())==1 && m_doLepJet){
    TLorentzVector particle;
    if (m_signalElectrons->size()==1) {
      //m_lep_recl->push_back(m_signalElectrons->at(0));
      particle = m_signalElectrons->at(0)->p4();
    } 
  
    else if (m_signalMuons->size()==1) {
      //m_lep_recl->push_back(m_signalMuons->at(0));
      particle = m_signalMuons->at(0)->p4();
    }
    /*
    m_signalJets_recl->push_back(m_lep_recl->at(0));
    */

    //make a fake jet for lepton treatment
    xAOD::Jet *tempJet = new xAOD::Jet();
    tempJet->makePrivateStore();
    tempJet->setJetP4(xAOD::JetFourMom_t(particle.Pt(),particle.Eta(),particle.Phi(),particle.M()));

    m_signalJets->push_back(tempJet);

  }

  m_signalJets->sort(ptsorter);
  m_signalJets_recl->sort(ptsorter);
  CHECK( store->record(m_signalJets_recl,"GoodTruthJets_recl") );


  //_______________________________________________   
  // Get Fat Reclustered jets!!!                 
  //_______________________________________________   
  //                                    

  m_rcjets_kt8 = 0;
  m_rcjets_kt12 = 0;



  ATH_MSG_DEBUG("Shall I do jet reclustering? "<< m_useAntiKtRcJets);
  if(m_useAntiKtRcJets){

    
    m_jetRecTool_kt8->execute();
    m_jetRecTool_kt12->execute();

    CHECK(event->retrieve( m_rcjets_kt8,"MyFatJetsKt8"));
    CHECK(event->retrieve( m_rcjets_kt12,"MyFatJetsKt12" ));

    m_rcjets_kt8->sort(ptsorter);
    m_rcjets_kt12->sort(ptsorter);

    //CHECK( store->record(m_rcjets_kt8,"MyFatJetsKt8_save"+sysName));                                                                                                           
    //CHECK( store->record(m_rcjets_kt12,"MyFatJetsKt12_save"+sysName));                                                                                                           
  }




  m_pxmiss=0;
  m_pymiss=0;
  m_etmiss=0;
  m_sumet=0;

  m_pxmiss_mu=0;
  m_pymiss_mu=0;
  m_etmiss_mu=0;
  m_sumet_mu=0;

  m_pxmiss_NonInt=0;
  m_pymiss_NonInt=0;
  m_etmiss_NonInt=0;
  m_sumet_NonInt=0;

  m_pxmiss_prime=0;
  m_pymiss_prime=0;
  m_etmiss_prime=0;

  TVector2 met_prime;

  const xAOD::MissingETContainer* met_Truth;
  CHECK( event->retrieve( met_Truth, "MET_Truth") );
 
  const xAOD::MissingET* met_Truth_Int = (*met_Truth)["Int"];
  const xAOD::MissingET* met_Truth_NonInt = (*met_Truth)["NonInt"];
  const xAOD::MissingET* met_Truth_Muons = (*met_Truth)["IntMuons"];

  m_pxmiss=met_Truth_Int->mpx();
  m_pymiss=met_Truth_Int->mpy();
  m_etmiss=met_Truth_Int->met();
  m_sumet=met_Truth_Int->sumet();

  m_pxmiss_mu=met_Truth_Muons->mpx();
  m_pymiss_mu=met_Truth_Muons->mpy();
  m_etmiss_mu=met_Truth_Muons->met();
  m_sumet_mu=met_Truth_Muons->sumet();
  
  m_pxmiss+=m_pxmiss_mu;
  m_pymiss+=m_pymiss_mu;
  m_etmiss+=m_etmiss_mu;
  m_sumet+=m_sumet_mu;
  
  

  m_pxmiss_NonInt=met_Truth_NonInt->mpx();
  m_pymiss_NonInt=met_Truth_NonInt->mpy();
  m_etmiss_NonInt=met_Truth_NonInt->met();
  m_sumet_NonInt=met_Truth_NonInt->sumet();

  if (m_signalElectrons->size()==2 && m_signalMuons->size()==0)  {

    met_prime.Set(m_pxmiss+(m_signalElectrons->at(0)->p4()).Px()+(m_signalElectrons->at(1)->p4()).Px(), m_pymiss+(m_signalElectrons->at(0)->p4()).Py()+(m_signalElectrons->at(1)->p4()).Py());

    m_pxmiss_prime = met_prime.Px();
    m_pymiss_prime = met_prime.Py();
    m_etmiss_prime = met_prime.Mod();

  }

  else if (m_signalElectrons->size()==0 && m_signalMuons->size()==2)  {
  
    met_prime.Set(m_pxmiss+(m_signalMuons->at(0)->p4()).Px()+(m_signalMuons->at(1)->p4()).Px(), m_pymiss+(m_signalMuons->at(0)->p4()).Py()+(m_signalMuons->at(1)->p4()).Py());

    m_pxmiss_prime = met_prime.Px();
    m_pymiss_prime = met_prime.Py();
    m_etmiss_prime = met_prime.Mod();
  }

  else {

    m_pxmiss_prime = m_pxmiss;
    m_pymiss_prime = m_pymiss;
    m_etmiss_prime = m_etmiss;
  }


  //__________________________________________
  //
  // we need some code for getting the truth boson 
  // that works for Z+jets and for ttZ 
  //__________________________________________
  // Poor mans version, DONT THINK THIS WORKS IN SHERPA 2.2.1:
  /*
    if ( ( particle->status() == 3 ) && fabs(particle->pdgId())>=11 && fabs(particle->pdgId())<=16 ) 
  {
    if (!foundFirst){
      l1 = particle->p4();
      foundFirst = true;
    } else if(!foundSecond){
      l2 = particle->p4();
      foundSecond = true;
      V = l1 + l2;
    }
  }
  */
  ATH_MSG_DEBUG("Done the MET and that ");

 
  return true;
  }




bool MasterShef :: getTruthMuons(double m_MuonBaselinePt, double m_MuonBaselineEta)
{

  //std::string sysName=m_systNameList.at(m_systNum);
  xAOD::TStore* store = wk()->xaodStore();
  xAOD::TEvent* event = wk()->xaodEvent();

  xAOD::TruthParticleContainer* m_truthMuons = 0 ;
  xAOD::ShallowAuxContainer* m_truthMuonsAux = 0 ;
  const xAOD::TruthParticleContainer* truthMu(0);
  EL_RETURN_CHECK("truthMuons",event->retrieve( truthMu, "TruthMuons" ));
  std::pair<xAOD::TruthParticleContainer*, xAOD::ShallowAuxContainer*> shallowcopy = xAOD::shallowCopyContainer(*truthMu);
  m_truthMuons = shallowcopy.first;
  m_truthMuonsAux = shallowcopy.second;
  bool setLinks = xAOD::setOriginalObjectLink(*truthMu, *m_truthMuons);
  if (!setLinks) {
    std::cout << "warning... faied to set links" << std::endl;
  }
  CHECK(store->record(m_truthMuons,"truthMuons"));
  CHECK(store->record(m_truthMuonsAux,"truthMuonsAux."));
  

  //if this isn't a truth analysis we don't want to overwrite the Containers (which would contain reco objects!!
  //so we break at this point!
  if(!m_doTruth){
    return true;
  }



  //double m_MuonBaselinePt = (*m_objTool->getProperty<double>("MuonBaselinePt"));
  //double m_MuonBaselineEta = (*m_objTool->getProperty<double>("MuonBaselineEta"));

 
  m_baselineMuonsBeforeOR = new xAOD::MuonContainer();
  m_baselineMuonsBeforeORAux= new xAOD::MuonAuxContainer();
  m_baselineMuonsBeforeOR->setStore(m_baselineMuonsBeforeORAux);


  for (const auto& particle: *m_truthMuons) {
    if(!(particle->isMuon() && particle->status()==1))
      continue;

    if(particle->pt() <  m_MuonBaselinePt)
      continue;
    if(fabs(particle->eta()) > m_MuonBaselineEta)
      continue;
    
    //add truth muons to our muon collection
    xAOD::Muon *myMuon = new xAOD::Muon();
    myMuon->makePrivateStore();
    myMuon->setP4(particle->pt(), particle->eta(), particle->phi());
    m_baselineMuonsBeforeOR->push_back(myMuon);
  }
  //record "baseline" decoration
  for (const auto& mu: *m_baselineMuonsBeforeOR){
    mu->auxdata< char >("baseline")=true;
  }
  
  //sort and record to memory
  m_baselineMuonsBeforeOR->sort(ptsorter);
  CHECK(store->record(m_baselineMuonsBeforeOR,"truthBaselinelMuons"));
  CHECK(store->record(m_baselineMuonsBeforeORAux,"truthBaselinelMuonsAux."));

      
  return true;
} 



bool MasterShef :: getTruthElectrons(double m_ElectronBaselinePt,double m_ElectronBaselineEta)
{

  //std::string sysName=m_systNameList.at(m_systNum);
  xAOD::TStore* store = wk()->xaodStore();
  xAOD::TEvent* event = wk()->xaodEvent();

  xAOD::TruthParticleContainer* m_truthElectrons = 0 ;
  xAOD::ShallowAuxContainer* m_truthElectronsAux = 0 ;
  const xAOD::TruthParticleContainer* truthEl(0);
  EL_RETURN_CHECK("truthElectrons",event->retrieve( truthEl, "TruthElectrons" ));
  std::pair<xAOD::TruthParticleContainer*, xAOD::ShallowAuxContainer*> shallowcopy = xAOD::shallowCopyContainer(*truthEl);
  m_truthElectrons = shallowcopy.first;
  m_truthElectronsAux = shallowcopy.second;
  bool setLinks = xAOD::setOriginalObjectLink(*truthEl, *m_truthElectrons);
  if (!setLinks) {
    std::cout << "warning... faied to set links" << std::endl;
  }
  CHECK(store->record(m_truthElectrons,"truthElectrons"));
  CHECK(store->record(m_truthElectronsAux,"truthElectronsAux."));
  

  //if this isn't a truth analysis we don't want to overwrite the Containers (which would contain reco objects!!
  //so we break at this point!
  if(!m_doTruth){
    return true;
  }


 
  m_baselineElectronsBeforeOR = new xAOD::ElectronContainer();
  m_baselineElectronsBeforeORAux= new xAOD::ElectronAuxContainer();
  m_baselineElectronsBeforeOR->setStore( m_baselineElectronsBeforeORAux);



  for (const auto& particle: *m_truthElectrons) {
    if(!(particle->isElectron() && particle->status()==1))
      continue;

    if(particle->pt() <  m_ElectronBaselinePt)
      continue;
    if(fabs(particle->eta()) > m_ElectronBaselineEta)
      continue;
    
    //add truth muons to our muon collection
    xAOD::Electron *myElectron = new xAOD::Electron();
    myElectron->makePrivateStore();
    myElectron->setP4(particle->pt(), particle->eta(), particle->phi(), particle->m());
    m_baselineElectronsBeforeOR->push_back(myElectron);
  }
  //record "baseline" decoration
  for (const auto& el: *m_baselineElectronsBeforeOR){
    el->auxdata< char >("baseline")=true;
  }

  //sort and record to memory
  m_baselineElectronsBeforeOR->sort(ptsorter);
  CHECK(store->record(m_baselineElectronsBeforeOR,"truthBaselinelElectrons"));
  CHECK(store->record(m_baselineElectronsBeforeORAux,"truthBaselinelElectronsAux."));
      
  return true;
}

bool MasterShef :: getTruthPhotons(double m_PhotonBaselinePt,double m_PhotonBaselineEta)
{

  //std::string sysName=m_systNameList.at(m_systNum);
  xAOD::TStore* store = wk()->xaodStore();
  xAOD::TEvent* event = wk()->xaodEvent();
  
  xAOD::TruthParticleContainer* m_truthPhotons = 0 ;
  xAOD::ShallowAuxContainer* m_truthPhotonsAux = 0 ;
  const xAOD::TruthParticleContainer* truthPh(0);
  EL_RETURN_CHECK("truthPhotons",event->retrieve( truthPh, "TruthPhotons" ));
  std::pair<xAOD::TruthParticleContainer*, xAOD::ShallowAuxContainer*> shallowcopy = xAOD::shallowCopyContainer(*truthPh);
  m_truthPhotons = shallowcopy.first;
  m_truthPhotonsAux = shallowcopy.second;
  bool setLinks = xAOD::setOriginalObjectLink(*truthPh, *m_truthPhotons);
  if (!setLinks) {
    std::cout << "warning... faied to set links" << std::endl;
  }
  CHECK(store->record(m_truthPhotons,"truthPhotons"));
  CHECK(store->record(m_truthPhotonsAux,"truthPhotonsAux."));

  
  //if this isn't a truth analysis we don't want to overwrite the Containers (which would contain reco objects!!
  //so we break at this point!
  if(!m_doTruth){
    return true;
  }




  m_signalPhotonsBeforeOR = new xAOD::PhotonContainer();
  m_signalPhotonsBeforeORAux= new xAOD::PhotonAuxContainer();
  m_signalPhotonsBeforeOR->setStore( m_signalPhotonsBeforeORAux);



  for (const auto& particle: *m_truthPhotons) {
    if(!(particle->isPhoton() && particle->status()==1))
      continue;

    if(particle->pt() <  m_PhotonBaselinePt)
      continue;
    if(fabs(particle->eta()) > m_PhotonBaselineEta)
      continue;

    //add truth muons to our muon collection
    xAOD::Photon *myPhoton = new xAOD::Photon();
    myPhoton->makePrivateStore();
    myPhoton->setP4(particle->pt(), particle->eta(), particle->phi(), particle->m());
    m_signalPhotonsBeforeOR->push_back(myPhoton);
  }
  //record "baseline" decoration
  for (const auto& ph: *m_signalPhotonsBeforeOR){
    ph->auxdata< char >("baseline")=true;
  }

  //sort and record to memory
  m_signalPhotonsBeforeOR->sort(ptsorter);
  CHECK(store->record(m_signalPhotonsBeforeOR,"truthBaselinePhotons"));
  CHECK(store->record(m_signalPhotonsBeforeORAux,"truthBaselinePhotonsAux."));

  return true;
}



bool MasterShef :: getTruthTaus(double m_TauPt, double m_TauEta)
{

  //std::string sysName=m_systNameList.at(m_systNum);
  xAOD::TStore* store = wk()->xaodStore();
  xAOD::TEvent* event = wk()->xaodEvent();
  
  xAOD::TruthParticleContainer* m_truthTaus = 0 ;
  xAOD::ShallowAuxContainer* m_truthTausAux = 0 ;
  const xAOD::TruthParticleContainer* truthPh(0);
  EL_RETURN_CHECK("truthTaus",event->retrieve( truthPh, "TruthTaus" ));
  std::pair<xAOD::TruthParticleContainer*, xAOD::ShallowAuxContainer*> shallowcopy = xAOD::shallowCopyContainer(*truthPh);
  m_truthTaus = shallowcopy.first;
  m_truthTausAux = shallowcopy.second;
  bool setLinks = xAOD::setOriginalObjectLink(*truthPh, *m_truthTaus);
  if (!setLinks) {
    std::cout << "warning... faied to set links" << std::endl;
  }
  CHECK(store->record(m_truthTaus,"truthTaus"));
  CHECK(store->record(m_truthTausAux,"truthTausAux."));

  
  //if this isn't a truth analysis we don't want to overwrite the Containers (which would contain reco objects!!
  //so we break at this point!
  if(!m_doTruth){
    //mark the truth objects as baseline if they pass the cuts
    for(const auto &jet: *m_truthTaus){
      jet->auxdata<char>("baseline")=false;
      if(jet->pt() > m_TauPt && fabs(jet->eta())<m_TauEta){
	jet->auxdata<char>("baseline")=true;
      }
    } 
    return true;
  }
  
  //implement code for the truth analysis here??
  
  return true;

}




bool MasterShef :: getTruthJets(double m_JetPt, double m_JetEta)
{

 // std::string sysName=m_systNameList.at(m_systNum);
  xAOD::TStore* store = wk()->xaodStore();
  xAOD::TEvent* event = wk()->xaodEvent();

  xAOD::JetContainer*  m_truthJets = 0 ;
  xAOD::ShallowAuxContainer*     m_truthJetsAux = 0 ;
  const xAOD::JetContainer* truthJET(0);

  std::string truthjets_name = "AntiKt4TruthJets";
  //try instead for later ptags
  bool contains=event->contains<xAOD::JetContainer>(truthjets_name);
  

  if(!event->contains<xAOD::JetContainer>(truthjets_name)){
    truthjets_name="AntiKt4TruthDressedWZJets";
    contains=event->contains<xAOD::JetContainer>(truthjets_name);
    if(!contains){
      Error(APP_NAME,"I dunno what the fuck this is");
      return 0;
    }
  }
  
  EL_RETURN_CHECK("truthJets",event->retrieve( truthJET, truthjets_name));

  std::pair<xAOD::JetContainer*, xAOD::ShallowAuxContainer*> shallowcopy = xAOD::shallowCopyContainer(*truthJET);
  m_truthJets = shallowcopy.first;
  m_truthJetsAux = shallowcopy.second;
  bool setLinks = xAOD::setOriginalObjectLink(*truthJET, *m_truthJets);
  if (!setLinks) {
    std::cout << "warning... faied to set links" << std::endl;
  }


  CHECK(store->record(m_truthJets,"truthJets"));
  CHECK(store->record(m_truthJetsAux,"truthJetsAux."));


  //if this isn't a truth analysis we don't want to overwrite the Containers (which would contain reco objects!!
  //so we break at this point!
  if(!m_doTruth){
    return true;
  }


  
  m_signalJetsBeforeOR = new xAOD::JetContainer();
  m_signalJetsBeforeORAux = new xAOD::JetAuxContainer();
  m_signalJetsBeforeOR->setStore(m_signalJetsBeforeORAux);
  
 // double m_JetPt = 20000;//(*m_objTool->getProperty<double>("JetPt"));
 // double m_JetEta = 2.8;//(*m_objTool->getProperty<double>("JetEta"));


  for (const auto& particle: *m_truthJets) {
   
    if(particle->pt() < m_JetPt)
      continue;
    if(fabs(particle->eta()) > m_JetEta )
      continue;





    //if(particle->isAvailable<ElementLink<DataVector<xAOD::IParticle>>> >("GhostPartons")) {

    // std::vector<ElementLink<DataVector<xAOD::IParticle> > > links = particle->auxdata<std::vector<ElementLink<DataVector<xAOD::IParticle> > > > ("GhostPartons");

    // std::cout << links.size() << std::endl;

    // for(unsigned int i = 0; i<links.size(); i++){
      
    //   link=links[i];

     //   if(link.isValid()){
    // 	std::cout << "found ghost partons" << std::endl;
    // 	//ElementLink<xAOD::TruthParticleContainer> link = mu->auxdata<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink");
    // 	//if(link.isValid())
    // 	//	matched_truth_muon = *link;
    //   }
    //   else{
    // 	std::cout << " no ghost " << std::endl;
    //   }
    // }




    //std::cout << particle->pt() << " " << particle->eta() << std::endl;
    // xAOD::JetConstituentVector constituents = particle->getConstituents();
    // std::cout << "particle " << particle->pt() << " valid?=" << constituents.isValid() << " " << constituents.size() << std::endl;
    // if (constituents.isValid()){
    //   for( auto c : constituents ) {
    // 	const xAOD::IParticle* rawObj = c->rawConstituent();
    // 	//assume the jet is EMTopo, needs a fix for PFlow (PFO)
    // 	//if( rawObj->type() == xAOD::Type::CaloCluster ) {
    // 	std::cout << "here i am " << c->pt() << std::endl;
    //   }
    // }
         
    xAOD::Jet *myJet = new xAOD::Jet(*particle);
    //myJet->makePrivateStore();
    //myJet->setJetP4(xAOD::JetFourMom_t(particle->pt(),particle->eta(),particle->phi(),particle->m()));
    
    myJet->auxdata< char >("bjet") =  particle->auxdata<int>("HadronConeExclExtendedTruthLabelID") == 5;//particle->auxdata<int>("GhostBHadronsFinalCount");
    m_signalJetsBeforeOR->push_back(myJet);

    //particle->auxdata< char >("bjet") =  particle->auxdata<int>("GhostBHadronsFinalCount");

    //m_signalJetsBeforeOR->push_back(particle);
  }
  //decorate all baseline jets as baseline so we can do the OR
  for (const auto& jet: *m_signalJetsBeforeOR){
    jet->auxdata< char >("baseline")=true;
  }



  m_signalJetsBeforeOR->sort(ptsorter);
  CHECK(store->record(m_signalJetsBeforeOR,"truthBaselineJets"));
  CHECK(store->record(m_signalJetsBeforeORAux,"truthBaselineJetsAux."));

  return true;
}


//-------------------------------------
// Function that added neutrinos and muons to a TLV vector
// should be called addNeutMuToVector........... whatever
// 
// * a TLV is passed to this function
// * the truth record obtained from the event is retrieved and looped over
// * particles with the correct status and barcode are added to the TLV if they pass the criteria:
//    * they are within the cone of the truth jet (by default the cone size is 0.4 to be consistent with electrons)
//    * they pass the criteria that they are a muon or a neutrino
//----------------------------------------------------
void MasterShef::addNeutNuToVector(TLorentzVector &full4MomTruth){
  
  if(m_truthParticles == nullptr){
    ATH_MSG_WARNING("You're trying to call addNeutNuToVector but you haven't loaded any truth particles!");
    return;
  }
  //save the original TLorentzVector for adding particles within a cone to this.
  TLorentzVector origTLV = full4MomTruth;
  for (const auto& particle: *m_truthParticles) 
    {
      if ( particle->pt() < 100. ) continue;
      int status = particle->status();
      if (status !=1) continue;
      int bar =  particle->barcode();
      if (bar > 200000) continue;
           
      //check if the truth particle TLV is with a cone (default 0.4) to the original TLV
      bool inCone = origTLV.DeltaR(particle->p4()) < m_ConeSize ;
      bool passCriteria = ((particle->isMuon() || particle->isNeutrino()) && inCone);
      
      //if we pass the criteria of being within the code and is a Muon or a Neutrino, then we add it to the vector
      if (passCriteria){
	full4MomTruth+=particle->p4();
      }
    }//end truthparticle loop
  
}
