#include <MasterShef/MasterShef.h>
#include <xAODPFlow/PFO.h>

//#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"



//#include "CPAnalysisExamples/errorcheck.h"
static const char* APP_NAME = "MasterShef::fill";



static SG::AuxElement::ConstAccessor<float> acc_GenFiltMET("GenFiltMET"); //!
static SG::AuxElement::ConstAccessor<float> acc_GenFiltHT("GenFiltHT"); //!



const std::vector<int>& PDGCode::GetPartonCodes(std::vector<int>& partons)
{
  partons.push_back(d);
  partons.push_back(-d);
  partons.push_back(u);
  partons.push_back(-u);
  partons.push_back(s);
  partons.push_back(-s);
  partons.push_back(c);
  partons.push_back(-c);
  partons.push_back(b);
  partons.push_back(-b);
  partons.push_back(t);
  partons.push_back(-t);
  partons.push_back(g);
  
  return partons;
}



const std::vector<int>& PDGCode::GetNeutrinoCodes(std::vector<int>& nus)
{
  nus.push_back(nu_e);
  nus.push_back(-nu_e);
  nus.push_back(nu_mu);
  nus.push_back(-nu_mu);
  nus.push_back(nu_tau);
  nus.push_back(-nu_tau);
  
  return nus;
}



const std::vector<int>& PDGCode::GetEGammaCodes(std::vector<int>& eGammas)
{
  eGammas.push_back(e_minus);
  eGammas.push_back(-e_minus);
  eGammas.push_back(gamma);
  
  return eGammas;
}




bool MasterShef::prepareTrees(){

  //prepare what to dump!
  m_totFills=0;

  //loop over the user specified tree fill modules
  for(auto name : m_userTreeMods){

    //check that it is a known module!
    //otherwise fail
    //known modules are definied in the MasterShef class constructor
    bool found=(m_allTreeMods.count(name)>0);
    if(!found){
      Error(APP_NAME,"Not found '%s' in list of possible fill functions, exiting...",name.c_str());
      Error(APP_NAME,"Printing all possible tree fill modules..");
      this->printAllPossibleFills();
      return false;
    }
    //if found then add it to the list of tree fillers
    m_funFill[m_totFills++]= m_allTreeMods[name];
    Info(APP_NAME,"User requested to fill %s, added to the list of fills",name.c_str());
  }
  
  Info(APP_NAME,"m_totFills=%i",m_totFills);
  
  
  //make branches for all the selected variables fills
  for (unsigned int jj=0; jj<m_systNameList.size(); jj++) {
    for (int ii = 0; ii<m_totFills; ii++) {
      //init the branches
      ATH_MSG_VERBOSE("On systematic index = "<< jj << " and on selector " << ii );
      bool passInit  = (this->*m_funFill[ii])(true,m_tree.at(jj));
      if(!passInit)return false;
    }
  }
  Info(APP_NAME,"done preparing fills");

  return true;

}




//filling jet constituents
bool  MasterShef::fillJetConstituents(bool init, TTree* tree){
  ATH_MSG_VERBOSE("in jet constituents ");

 
  if(init){
    tree->Branch("jet_tracks_pt",&nt_jet_tracks_pt);
    tree->Branch("jet_tracks_eta",&nt_jet_tracks_eta);
    tree->Branch("jet_tracks_phi",&nt_jet_tracks_phi);
    tree->Branch("jet_tracks_e",&nt_jet_tracks_e);
    return true;
  }

  for(auto item: nt_jet_tracks_pt)item.clear();
  for(auto item: nt_jet_tracks_eta)item.clear();
  for(auto item: nt_jet_tracks_phi)item.clear();
  for(auto item: nt_jet_tracks_e)item.clear();
  
  nt_jet_tracks_pt.clear();
  nt_jet_tracks_eta.clear();
  nt_jet_tracks_phi.clear();
  nt_jet_tracks_e.clear();

 


  for(const auto &jet: *m_signalJets){
    std::vector<const xAOD::IParticle*> jtracks;
    jet->getAssociatedObjects<xAOD::IParticle>( xAOD::JetAttribute::GhostTrack,jtracks);
    std::vector<float> tracks_pt;
    std::vector<float> tracks_eta;
    std::vector<float> tracks_phi;
    std::vector<float> tracks_e;
    

    for(size_t iConst=0; iConst<jtracks.size(); ++iConst) {
      const xAOD::TrackParticle* pTrk = static_cast<const xAOD::TrackParticle*>(jtracks[iConst]);
      if(!pTrk)continue;
      
      tracks_pt.push_back(pTrk->pt());
      tracks_phi.push_back(pTrk->phi());
      tracks_eta.push_back(pTrk->eta());
      tracks_e.push_back(pTrk->e());
    }

    nt_jet_tracks_pt.push_back(tracks_pt);
    nt_jet_tracks_eta.push_back(tracks_eta);
    nt_jet_tracks_phi.push_back(tracks_phi);
    nt_jet_tracks_e.push_back(tracks_e);

  }
  ATH_MSG_VERBOSE("done jet constit");


  return true;
}




//filling jet constituents
bool  MasterShef::fillTruthJetConstituents(bool init, TTree* tree){
  ATH_MSG_VERBOSE("in jet constituents ");

 
  if(init){
    tree->Branch("jet_tracks_pt",&nt_jet_tracks_pt);
    tree->Branch("jet_tracks_eta",&nt_jet_tracks_eta);
    tree->Branch("jet_tracks_phi",&nt_jet_tracks_phi);
    tree->Branch("jet_tracks_e",&nt_jet_tracks_e);
    return true;
  }

  for(auto item: nt_jet_tracks_pt)item.clear();
  for(auto item: nt_jet_tracks_eta)item.clear();
  for(auto item: nt_jet_tracks_phi)item.clear();
  for(auto item: nt_jet_tracks_e)item.clear();
  
  nt_jet_tracks_pt.clear();
  nt_jet_tracks_eta.clear();
  nt_jet_tracks_phi.clear();
  nt_jet_tracks_e.clear();

  for(const auto &jet: *m_signalJets){

    std::vector<const xAOD::IParticle*> jtracks;

    std::vector<float> tracks_pt;
    std::vector<float> tracks_eta;
    std::vector<float> tracks_phi;
    std::vector<float> tracks_e;
    

    
    xAOD::JetConstituentVector constituents = jet->getConstituents();
    
    //std::cout << constituents.size() << std::endl;

    for( auto c : constituents ) {
   
      //std::cout << c->pt() << " " << c->phi() << std::endl;

      tracks_pt.push_back(c->pt());
      tracks_phi.push_back(c->phi());
      tracks_eta.push_back(c->eta());
      tracks_e.push_back(c->e());
    }

    nt_jet_tracks_pt.push_back(tracks_pt);
    nt_jet_tracks_eta.push_back(tracks_eta);
    nt_jet_tracks_phi.push_back(tracks_phi);
    nt_jet_tracks_e.push_back(tracks_e);

  }
  ATH_MSG_VERBOSE("done jet constit");


  return true;
}






//filling jet constituents
bool  MasterShef::fillTrackJetConstituents(bool init, TTree* tree){
  ATH_MSG_VERBOSE("in track jet constituents ");

  //this isn't working yet, so don't use
  std::cout << init << " " << tree->GetEntries() << std::endl;

  if(!m_useTrackJets) return false;
 

  /*
  if(init){
    tree->Branch("trackjet_tracks_pt",&nt_trackjet_tracks_pt);
    tree->Branch("trackjet_tracks_eta",&nt_trackjet_tracks_eta);
    tree->Branch("trackjet_tracks_phi",&nt_trackjet_tracks_phi);
    tree->Branch("trackjet_tracks_e",&nt_trackjet_tracks_e);
    
    return true;
  }
  
  for(auto item: nt_trackjet_tracks_pt)item.clear();
  for(auto item: nt_trackjet_tracks_eta)item.clear();
  for(auto item: nt_trackjet_tracks_phi)item.clear();
  for(auto item: nt_trackjet_tracks_e)item.clear();
  
  nt_trackjet_tracks_pt.clear();
  nt_trackjet_tracks_eta.clear();
  nt_trackjet_tracks_phi.clear();
  nt_trackjet_tracks_e.clear();

 


  for(const auto &jet: *m_signalTrackJets){


    
    xAOD::JetConstituentVector constituents = jet->getConstituents();
    std::cout << "jet " << jet->pt() << " valid?=" << constituents.isValid() << " " << constituents.size() << std::endl;
    //for( auto c : constituents ) {
      //const xAOD::IParticle* rawObj = c->rawConstituent();
      //assume the jet is EMTopo, needs a fix for PFlow (PFO)
      //if( rawObj->type() == xAOD::Type::CaloCluster ) {
      //const xAOD::CaloCluster* cluster = static_cast< const xAOD::CaloCluster* >( rawObj );
      //std::cout << "rawobj " << rawObj->p4().Pt() << " " << rawObj->type()  <<std::endl;
      //particle->charge=cluster->charge();
    // }
    //abort();

    std::vector<const xAOD::IParticle*> jtracks;
    jet->getAssociatedObjects<xAOD::IParticle>( xAOD::JetAttribute::GhostTrack,jtracks);
    std::vector<float> tracks_pt;
    std::vector<float> tracks_eta;
    std::vector<float> tracks_phi;
    std::vector<float> tracks_e;
    
    std::cout << jtracks.size() << std::endl;
    for(size_t iConst=0; iConst<jtracks.size(); ++iConst) {
      const xAOD::TrackParticle* pTrk = static_cast<const xAOD::TrackParticle*>(jtracks[iConst]);
      if(!pTrk)continue;
   
      std::cout << "found track " << pTrk->pt() << std::endl;
      tracks_pt.push_back(pTrk->pt());
      tracks_phi.push_back(pTrk->phi());
      tracks_eta.push_back(pTrk->eta());
      tracks_e.push_back(pTrk->e());
    }
    abort();
    nt_trackjet_tracks_pt.push_back(tracks_pt);
    nt_trackjet_tracks_eta.push_back(tracks_eta);
    nt_trackjet_tracks_phi.push_back(tracks_phi);
    nt_trackjet_tracks_e.push_back(tracks_e);

  }
  ATH_MSG_VERBOSE("done track jet constit");
  */

  return true;
}





bool  MasterShef::fillCommonVariables(bool init, TTree* tree){
  ATH_MSG_VERBOSE("in common variables");

   bool isNominal = true;
  if( std::string(tree->GetName()) != "Nominal" )
    isNominal=false;

  if(init){
    tree->Branch("anaflag", &nt_anaflag);
    tree->Branch("anatruth", &nt_anatruth);
    tree->Branch("realtime", &nt_cputime);
    //for MC the RunNumber is the MCID
    tree->Branch("EventNumber", &nt_eventNumber);
    tree->Branch("RunNumber", &nt_runNumber);
    //mcEventWeight
    tree->Branch("AnalysisWeight",&nt_analysisWeight);
    tree->Branch("BeamspotWeight",&nt_beamspotWeight);

    //new radiation weights
    tree->Branch("nominalWeight", &nt_nominalWeight);
    if(isNominal){
      tree->Branch("muR10_muF05Weight",&nt_muR10_muF05Weight);
      tree->Branch("muR10_muF20Weight",&nt_muR10_muF20Weight);
      tree->Branch("muR05_muF10Weight",&nt_muR05_muF10Weight);
      tree->Branch("muR20_muF10Weight",&nt_muR20_muF10Weight);
      tree->Branch("muR05_muF05Weight",&nt_muR05_muF05Weight);
      tree->Branch("muR20_muF20Weight",&nt_muR20_muF20Weight);
      tree->Branch("Var3cUpWeight",&nt_Var3cUpWeight);
      tree->Branch("Var3cDownWeight",&nt_Var3cDownWeight);
      tree->Branch("ISRmuRfac10_FSRmuRfac20Weight",&nt_ISRmuRfac10_FSRmuRfac20Weight);
      tree->Branch("ISRmuRfac10_FSRmuRfac05Weight",&nt_ISRmuRfac10_FSRmuRfac05Weight);
      
      int dsid= m_isMC ? eventInfo->mcChannelNumber(): eventInfo->runNumber() ;
      bool isSignal = (dsid > 500300 && dsid<500500) || (dsid > 504000);
      //if(isSignal)
      //tree->Branch("PDFUncertaintyWeights",&nt_PDFUncertainty);
    }

    //pileup vars
    tree->Branch("pileupweight",&nt_pileupweight);
    if(isNominal){
      tree->Branch("pileupweightUP",&nt_pileupweightUP);
      tree->Branch("pileupweightDOWN",&nt_pileupweightDOWN);
      tree->Branch("pileupweightHash",&nt_pileupweightHash);
    }

    tree->Branch("passDFCommonJets_eventClean_LooseBad",&nt_passDFCommonJets_eventClean_LooseBad);
   
    // Branches: Jets
    //new to keep track of which type of btagging was used, helps with post-processing step
    //tree->Branch("btagType",&nt_btagType); 
    //tree->Branch("btaggerName",&nt_btaggerName); //if we wanted to save the btagger name
    //jet 4 vectors et. al.
    tree->Branch("nJets",&nt_nJets);
    tree->Branch("jet_pt",&nt_jet_pt);
    tree->Branch("jet_eta",&nt_jet_eta);
    tree->Branch("jet_phi",&nt_jet_phi);
    tree->Branch("jet_e",&nt_jet_e);
    tree->Branch("jet_IsBJet",&nt_jet_IsBJet);
    tree->Branch("jet_IsCJet",&nt_jet_IsCJet);
    tree->Branch("jet_flav",&nt_jet_flav);
    if(m_isMC){
      tree->Branch("jet_truthflav",&nt_jet_truthflav);
      if (m_useHFTruthHadrons) tree->Branch("jet_index_truthbhadron",&nt_jet_index_truthbhadron);
      
      if(m_useMCTruthClassifier){
	tree->Branch("jet_type",&nt_jet_type);
	tree->Branch("jet_origin",&nt_jet_origin);
      }
    }

    if(m_useTrackJets){
      tree->Branch("trackjet_pt",&nt_trackjet_pt);
      tree->Branch("trackjet_eta",&nt_trackjet_eta);
      tree->Branch("trackjet_phi",&nt_trackjet_phi);
      tree->Branch("trackjet_e",&nt_trackjet_e);
      tree->Branch("trackjet_hasVROverlap",&nt_trackjet_hasVROverlap);
      
      tree->Branch("trackjet_isbjet",&nt_trackjet_isbjet);
      tree->Branch("trackjet_passISROR",&nt_trackjet_orISR);
      tree->Branch("trackjet_ntrk",&nt_trackjet_ntrk);
      tree->Branch("trackjet_MV2c10",&nt_trackjet_MV2c10);
      tree->Branch("trackjet_flav",&nt_trackjet_flav);
      tree->Branch("trackjet_passDR",&nt_trackjet_passDR);
      if( m_isMC ){
	tree->Branch("trackjet_truthflav",&nt_trackjet_truthflav);
	tree->Branch("trackjet_truthflavhadexcl",&nt_trackjet_truthflavhadexcl);
	tree->Branch("trackjet_origin",&nt_trackjet_origin);
	tree->Branch("trackjet_type",&nt_trackjet_type);
      if (m_useHFTruthHadrons) tree->Branch("trackjet_index_truthbhadron",&nt_trackjet_index_truthbhadron);
      }
    }
    
    
    // Branches: Electrons
    tree->Branch("nbaselineEl_beforeOR",&nt_nbaselineEl_beforeOR);
    tree->Branch("nbaselineEl",&nt_nbaselineEl);
    tree->Branch("nbaselineEl_lowPt",&nt_nbaselineEl_lowPt);
    tree->Branch("nEl",&nt_nEl);
    tree->Branch("el_pt",&nt_el_pt);
    tree->Branch("el_eta",&nt_el_eta);
    tree->Branch("el_phi",&nt_el_phi);
    tree->Branch("el_e",&nt_el_e); 
    tree->Branch("el_charge",&nt_el_charge); 

    // Branches: Muons
    tree->Branch("nbaselineMu_beforeOR",&nt_nbaselineMu_beforeOR);
    tree->Branch("nbaselineMu",&nt_nbaselineMu);
    tree->Branch("nbaselineMu_lowPt",&nt_nbaselineMu_lowPt);

    tree->Branch("nMu",&nt_nMu);
    tree->Branch("mu_pt",&nt_mu_pt);
    tree->Branch("mu_eta",&nt_mu_eta);
    tree->Branch("mu_phi",&nt_mu_phi);
    tree->Branch("mu_e",&nt_mu_e);
    tree->Branch("mu_charge",&nt_mu_charge); 

    // Branches: photons
    if(m_usePhotons){
      tree->Branch("nbaselinePh",&nt_nbaselinePh);
      tree->Branch("nPh",&nt_nPh);
      tree->Branch("ph_pt",&nt_ph_pt);
      tree->Branch("ph_eta",&nt_ph_eta);
      tree->Branch("ph_phi",&nt_ph_phi);
      tree->Branch("ph_e",&nt_ph_e);
    }      

    // Branches: taus
    if(m_useTaus){
      tree->Branch("nTauBeforeOR", &nt_nbaselineTau);
      tree->Branch("nTau", &nt_nTau);
      tree->Branch("tau_pt", &nt_tau_pt);
      tree->Branch("tau_eta",&nt_tau_eta);
      tree->Branch("tau_phi",&nt_tau_phi);
      tree->Branch("tau_e",  &nt_tau_e);
      
      tree->Branch("tau_bdtJet", &nt_tau_bdtJet);
      tree->Branch("tau_bdtEl", &nt_tau_bdtEl);

      tree->Branch("tau_nH"      ,&nt_tau_nH);
      //tree->Branch("tau_nWTracks",&nt_tau_nWTracks);
      tree->Branch("tau_nTracks" ,&nt_tau_nTracks);
      tree->Branch("tau_nPi0"    ,&nt_tau_nPi0);
      tree->Branch("tau_nCharged",&nt_tau_nCharged);
      tree->Branch("tau_nNeut"   ,&nt_tau_nNeut);
      
      tree->Branch("tau_passOR",&nt_tau_passOR);      
    }
    // //TruthMu set
    // tree->Branch("truthmu_pt",&nt_truthmu_pt);
    // tree->Branch("truthmu_eta",&nt_truthmu_eta);
    // tree->Branch("truthmu_phi",&nt_truthmu_phi);
    // tree->Branch("truthmu_e",&nt_truthmu_e);
    // tree->Branch("truthmu_status",&nt_truthmu_status); 
    // tree->Branch("truthmu_type",&nt_truthmu_type);
    // tree->Branch("truthmu_origin",&nt_truthmu_origin);
    
    // //TruthEl set
    // tree->Branch("truthel_pt",&nt_truthel_pt);
    // tree->Branch("truthel_eta",&nt_truthel_eta);
    // tree->Branch("truthel_phi",&nt_truthel_phi);
    // tree->Branch("truthel_e",&nt_truthel_e);
    // tree->Branch("truthel_status",&nt_truthel_status);
    // tree->Branch("truthel_type",&nt_truthel_type);
    // tree->Branch("truthel_origin",&nt_truthel_origin);
    


    // Branches: MET
    tree->Branch("MET_pt",&nt_MET_pt);
    tree->Branch("MET_phi",&nt_MET_phi);
                                                                                                                                            
    tree->Branch("MET_pt_prime",&nt_MET_pt_prime);
    tree->Branch("MET_phi_prime",&nt_MET_phi_prime);

    tree->Branch("sumet",&nt_sumet);


    tree->Branch("metsig",&nt_metsig);
    tree->Branch("metsig_default",&nt_metsig_default);
    tree->Branch("metsig_binflate",&nt_metsig_binflate);
    tree->Branch("metsig_run1JER",&nt_metsig_run1JER);

    tree->Branch("metsigLepJetResol",&nt_metsigLepJetResol);
    tree->Branch("metsigST",&nt_metsigST);
    tree->Branch("metsigET",&nt_metsigET);
    tree->Branch("metsigHT",&nt_metsigHT);
    tree->Branch("NtrkGlobal",&nt_ntrk_global);

    return true;
  }


  ATH_MSG_VERBOSE("starting to fill reco objects");


  //analysis name
  std::string ana_name(m_outputName);
  nt_anaflag=ana_name;
  //truth flag
  nt_anatruth=0;
  if (m_doTruth) nt_anatruth=1;

  //save the btagging type to our output ntuples
  //nt_btagType = m_btagType;
  //load up the runnumber and eventnumber
  if(m_isMC)
    nt_runNumber=eventInfo->mcChannelNumber();
  else
    nt_runNumber=eventInfo->runNumber();
  
  nt_eventNumber=eventInfo->eventNumber();

  ATH_MSG_VERBOSE("eventNumber= " << nt_eventNumber);


  nt_nominalWeight = 1.0;
  nt_muR05_muF10Weight = 1.0;
  nt_muR20_muF10Weight = 1.0;
  nt_muR10_muF05Weight = 1.0;
  nt_muR10_muF20Weight = 1.0;
  nt_muR05_muF05Weight = 1.0;
  nt_muR20_muF20Weight = 1.0;
  nt_Var3cUpWeight = 1.0;
  nt_Var3cDownWeight = 1.0;
  nt_ISRmuRfac10_FSRmuRfac20Weight = 1.0;
  nt_ISRmuRfac10_FSRmuRfac05Weight = 1.0;
  nt_PDFUncertainty.clear();
 

  /*

  // Uncomment to inspect the systematic uncertainties
  if(m_isMC && m_saveShowerWeights){
    std::cout << " ================  "  << std::endl;
    for(auto weight : m_weightTool->getWeightNames())
      std::cout << "Weight name : " << weight << std::endl;
  }
  */

  if(m_isMC && isNominal && !m_doTruth && m_saveShowerWeights){

    //these are needed by the ttZ renorm and factorisation by 0.5 and 2.0  variations
    //see: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/RareTopFocusGroup#Review_of_systematic_uncertainti
    //The following variations are included in the samples: a). Renormalization scale variations: 
    //2.0 and :0.5; b). Factorization scale variations:  Mur102 and _Muf201/2. 
    //Is recomended to use the largest deviation with respect to the nominal as symmetric uncertainty. = pmg_weights.at(1);
   
    //for(int i=1; i<10; i++){
    //  std::cout << i << " " << pmg_weights.at(i) << std::endl;
    //}

    //For MadGraph
    //1  muR=0.10000E+01 muF=0.20000E+01 
    //2  muR=0.10000E+01 muF=0.50000E+00 
    //3  muR=0.20000E+01 muF=0.10000E+01 
    //4  muR=0.20000E+01 muF=0.20000E+01 
    //5  muR=0.20000E+01 muF=0.50000E+00 
    //6  muR=0.50000E+00 muF=0.10000E+01 
    //7  muR=0.50000E+00 muF=0.20000E+01 
    //8  muR=0.50000E+00 muF=0.50000E+00 
    
    //For PoPy8 and ttbar 
    //1  muR = 1.0, muF = 2.0 
    //2  muR = 1.0, muF = 0.5 
    //3  muR = 2.0, muF = 1.0  !!
    //4  muR = 0.5, muF = 1.0  !!
    //5  muR = 0.5, muF = 0.5  !!
    //6  muR = 2.0, muF = 2.0  !! 
    //7  muR = 2.0, muF = 0.5  !!
    //8  muR = 0.5, muF = 2.0  !! 

    //we only save shower weights for ttbar PoPy8 and MG5_aMcANLO ttZ 
    int dsid=m_isMC ?  eventInfo->mcChannelNumber() :  eventInfo->runNumber();
    //so check if it MG5 (via if it is a ttZ sample)
    bool isMG5 = dsid == 410156 || dsid == 410157 || dsid == 410218 || dsid == 410219 || dsid == 410220; // ttZ samples
    //so check if it Sherpa (via if it is a V+jets sample)
    bool isSherpa = ( dsid >= 364100 && dsid <=364200 ) || dsid == 364222 || dsid ==364223 || ( dsid >= 366001 && dsid <= 366008 ); // V+jets samples
    //so check if it PowPy8 and is SingleTop
    bool isSingleTop = ( dsid >= 411193 && dsid <= 411198) || (dsid >= 411181 && dsid <= 411186) || dsid == 410646 || dsid == 410647; // Wt_DR single-top
    // so check if it is signals from the tcMET analysis
    bool isSignal = (dsid > 500300 && dsid<500500)  || (dsid > 504000) ;
    isSherpa = isSherpa || m_showerType == 3 || m_showerType == 2;
    isMG5 = (isMG5 || m_showerType == 4) && (!isSignal);
    isSingleTop = isSingleTop || dsid == 410644 || dsid == 410645 || dsid == 410658|| dsid == 410659; //single-top s-chan and t-chan     

    std::string muR10_muF20 = isMG5 ? " muR=0.10000E+01 muF=0.20000E+01 " : " muR = 1.0, muF = 2.0 ";
    std::string muR10_muF05 = isMG5 ? " muR=0.10000E+01 muF=0.50000E+00 " : " muR = 1.0, muF = 0.5 ";
    std::string muR20_muF10 = isMG5 ? " muR=0.20000E+01 muF=0.10000E+01 " : " muR = 2.0, muF = 1.0 ";
    std::string muR05_muF10 = isMG5 ? " muR=0.50000E+00 muF=0.10000E+01 " : " muR = 0.5, muF = 1.0 ";
    std::string muR05_muF05 = isMG5 ? " muR=0.50000E+00 muF=0.50000E+00 " : " muR = 0.5, muF = 0.5 ";
    std::string muR20_muF20 = isMG5 ? " muR=0.20000E+01 muF=0.20000E+01 " : " muR = 2.0, muF = 2.0 ";

    auto pmg_weights = m_weightTool->getWeightNames();
    std::string nominal = pmg_weights.at(0);

    if(isSherpa){
      muR10_muF20="MUR1_MUF2_PDF261000";
      muR10_muF05="MUR1_MUF0.5_PDF261000";
      muR20_muF10="MUR2_MUF1_PDF261000";
      muR20_muF20="MUR2_MUF2_PDF261000";
      muR05_muF10="MUR0.5_MUF1_PDF261000";
      muR05_muF05="MUR0.5_MUF0.5_PDF261000";
    }else if (isSingleTop){
      muR10_muF20=" muR = 1.00, muF = 2.00 ";
      muR10_muF05=" muR = 1.00, muF = 0.50 ";
      muR20_muF10=" muR = 2.00, muF = 1.00 ";
      muR20_muF20=" muR = 2.00, muF = 2.00 ";
      muR05_muF10=" muR = 0.50, muF = 1.00 ";
      muR05_muF05=" muR = 0.50, muF = 0.50 ";
    }else if (isSignal){
      muR10_muF20=" MUR1.0_MUF2.0_PDF260000 ";
      muR10_muF05=" MUR1.0_MUF0.5_PDF260000 ";
      muR20_muF10=" MUR2.0_MUF1.0_PDF260000 ";
      muR20_muF20=" MUR2.0_MUF2.0_PDF260000 ";
      muR05_muF10=" MUR0.5_MUF1.0_PDF260000 ";
      muR05_muF05=" MUR0.5_MUF0.5_PDF260000 ";
    }
    if(!isSignal){
      nt_nominalWeight     = m_weightTool->getWeight(nominal);
      nt_muR10_muF20Weight = m_weightTool->getWeight(muR10_muF20);
      nt_muR10_muF05Weight = m_weightTool->getWeight(muR10_muF05);
      nt_muR20_muF10Weight = m_weightTool->getWeight(muR20_muF10);
      nt_muR05_muF10Weight = m_weightTool->getWeight(muR05_muF10);
      nt_muR05_muF05Weight = m_weightTool->getWeight(muR05_muF05);
      nt_muR20_muF20Weight = m_weightTool->getWeight(muR20_muF20);
    }

    nt_Var3cUpWeight = nt_Var3cDownWeight = nt_ISRmuRfac10_FSRmuRfac20Weight = nt_ISRmuRfac10_FSRmuRfac05Weight = 1.0;
    if(pmg_weights.size()>192 && pmg_weights.size()>=199  && !isMG5 && !isSignal){
      std::string Var3cUp  = pmg_weights.at(193);
      std::string Var3cDown =  pmg_weights.at(194);
      std::string ISRmuRfac10_FSRmuRfac20 =  pmg_weights.at(198);
      std::string ISRmuRfac10_FSRmuRfac05 =  pmg_weights.at(199);
      nt_Var3cUpWeight = m_weightTool->getWeight(Var3cUp);
      nt_Var3cDownWeight = m_weightTool->getWeight(Var3cDown);
      nt_ISRmuRfac10_FSRmuRfac20Weight = m_weightTool->getWeight(ISRmuRfac10_FSRmuRfac20);
      nt_ISRmuRfac10_FSRmuRfac05Weight = m_weightTool->getWeight(ISRmuRfac10_FSRmuRfac05);
    }

    /*
    if(isSignal){
      for(std::string weight: pmg_weights){
	if( weight.find("PDF260000") == std::string::npos && weight.find("PDF") != std::string::npos )
	  nt_PDFUncertainty.push_back(m_weightTool->getWeight(weight));
      }
    }
    */

  }


  //AnalysisWeigh!!
  nt_analysisWeight = m_AnalysisWeight;
  nt_beamspotWeight = eventInfo->beamSpotWeight();

  //pileupweight from global variables
  nt_pileupweight=m_pileupweight;
  nt_pileupweightUP=m_pileupweightUP;
  nt_pileupweightDOWN=m_pileupweightDOWN;
  nt_pileupweightHash=m_pileupweightHash;


  //save the DFcommon level jet cleaning flags for loose and tight
  nt_passDFCommonJets_eventClean_LooseBad=1.0;
  if(event->contains<char>("DFCommonJets_eventClean_LooseBad")){
    nt_passDFCommonJets_eventClean_LooseBad=(int)eventInfo->auxdata<char>("DFCommonJets_eventClean_LooseBad");
  }

  nt_jet_pt.clear();
  nt_jet_eta.clear();
  nt_jet_phi.clear();
  nt_jet_e.clear();
  nt_jet_IsBJet.clear();
  nt_jet_IsCJet.clear();
  nt_jet_flav.clear();
  nt_jet_truthflav.clear();
  nt_jet_type.clear();
  nt_jet_origin.clear();
  if (m_useHFTruthHadrons) nt_jet_index_truthbhadron.clear();

  ATH_MSG_VERBOSE("get njets");

  nt_nJets=m_signalJets->size();
 
  ATH_MSG_VERBOSE("get jets="<<nt_nJets);

  //set pointer to truthBHadrons here, only becomes non-null if m_useHFTruthHadrons is true (false by default, unless changed in SUSY.py)
  const xAOD::TruthParticleContainer* truthBHadrons = nullptr;
  //Retrieve the previously found truth B-Hadrons (see getTruthBHadrons)
  if (m_useHFTruthHadrons && m_isMC){
    CHECK(store->retrieve(truthBHadrons, "TruthBHadrons"));
  }
  
  for(const auto &jet: *m_signalJets){
    nt_jet_pt.push_back( jet->pt() );
    nt_jet_eta.push_back( jet->eta() );
    nt_jet_phi.push_back( jet->phi()  );
    nt_jet_e.push_back(  jet->e() );
    
    int isBJet = (int)jet->auxdata< bool >("IsBJet");
    int isCJet = (int)jet->auxdata< bool >("IsCJet");

    int flav = isBJet ? 5 : 0 ;
    //if we're using pseudocontinious btagging, instead save the bin value                                       
    if(m_contWP!=0){
      flav= (int)jet->auxdata< char >("bjet"); // 1 for standard btagging, different bins for pseudo-continuous
    }

    nt_jet_flav.push_back(flav);
    nt_jet_IsBJet.push_back(isBJet);
    nt_jet_IsCJet.push_back(isCJet);

    //truth flavour
    int flavour = -1;
    if(m_isMC && jet->isAvailable<int>("HadronConeExclTruthLabelID") )jet->getAttribute("HadronConeExclTruthLabelID",flavour); // Updated recommendations to use HadronConeExclExtendedTruthLabelID : https://twiki.cern.ch/twiki/bin/view/AtlasProtected/FlavourTaggingLabeling

    nt_jet_truthflav.push_back( flavour );

    std::string type = "";
    std::string origin = "";
    if(m_isMC && m_useMCTruthClassifier){
      auto result = m_MCtruthClassifier->particleTruthClassifier(jet,true);
      ATH_MSG_VERBOSE("Gote result.");
      MCTruthPartClassifier::ParticleDef partDef;
      origin = partDef.sParticleOrigin[result.second];
      ATH_MSG_VERBOSE("Got origin");
      type = partDef.sParticleType[result.first];
      ATH_MSG_VERBOSE("Got type");
      
    }

    nt_jet_type.push_back(type);
    nt_jet_origin.push_back(origin);

    if (m_useHFTruthHadrons && m_isMC){
      int truthBHadronIndex_min = matchTruthHadrons(*truthBHadrons, jet->p4());
      nt_jet_index_truthbhadron.push_back(truthBHadronIndex_min);
    }//if use truth b-hadrons
  }
  ATH_MSG_VERBOSE("Jets were good!");

  nt_trackjet_pt.clear();
  nt_trackjet_eta.clear();
  nt_trackjet_phi.clear();
  nt_trackjet_e.clear();
  nt_trackjet_hasVROverlap=0;
  nt_trackjet_isbjet.clear();
  nt_trackjet_flav.clear();
  nt_trackjet_passDR.clear();
  nt_trackjet_truthflav.clear();
  nt_trackjet_truthflavhadcone.clear();
  nt_trackjet_truthflavhadexcl.clear();
  nt_trackjet_MV2c10.clear();
  nt_trackjet_origin.clear();
  nt_trackjet_type.clear();
  nt_trackjet_index_truthbhadron.clear();
  nt_trackjet_orISR.clear();
  nt_trackjet_ntrk.clear();

  if(m_useTrackJets){


    for(const auto &jet: *m_signalTrackJets){
      nt_trackjet_pt.push_back( jet->pt() );
      nt_trackjet_eta.push_back( jet->eta() );
      nt_trackjet_phi.push_back( jet->phi()  );
      nt_trackjet_e.push_back(  jet->e() );
      nt_trackjet_isbjet.push_back( jet->auxdata< char >("bjet") == true && fabs(jet->eta())<2.5 );
      nt_trackjet_orISR.push_back( jet->auxdata<int>("passISROR") );
      nt_trackjet_ntrk.push_back( jet->numConstituents() );

      if( jet->auxdata<char>("passDRcut") == false )
	nt_trackjet_hasVROverlap=1;

      //dump the flavour (aka if track b-jet)
      int flav= ( (jet->auxdata< char >("bjet") == true ) && fabs(jet->eta())<2.5 ) ? 5 : 0;
      nt_trackjet_flav.push_back(flav);
  
      int passDR = jet->auxdata<char>("passDRcut") == true ? 1 : 0 ;
      nt_trackjet_passDR.push_back( passDR );
      
      //truth flavour using ConeTruthLabelID
      int flavour = -1;
      if(m_isMC)jet->getAttribute("ConeTruthLabelID",flavour);
      nt_trackjet_truthflav.push_back( flavour );

      //truth flavour using ConeTruthLabelID
      int flavourhadcone = -1;
      if(m_isMC)jet->getAttribute("HadronConeExclTruthLabelID",flavourhadcone);
      nt_trackjet_truthflavhadcone.push_back( flavour );

      //This also gives us information about whether a jet is matched to a truth b-hadron - "+5" tells us it is.
      int flavhadexcl = -999;
      jet->getAttribute("HadronConeExclExtendedTruthLabelID",flavhadexcl);
      nt_trackjet_truthflavhadexcl.push_back(flavhadexcl);
      
      double MV2=-999;
      xAOD::BTaggingUtilities::getBTagging(*jet)->MVx_discriminant("MV2c10", MV2);
      ATH_MSG_VERBOSE("TRACKSMV2");
      nt_trackjet_MV2c10.push_back( MV2 );

      std::string type = "";
      std::string origin = "";
      if(m_useMCTruthClassifier){
	auto result = m_MCtruthClassifier->particleTruthClassifier(jet,true);
	ATH_MSG_VERBOSE("Gote result.");
	MCTruthPartClassifier::ParticleDef partDef;
	origin = partDef.sParticleOrigin[result.second];
	ATH_MSG_VERBOSE("Got origin");
	type = partDef.sParticleType[result.first];
	ATH_MSG_VERBOSE("Got type.");
      }
      nt_trackjet_type.push_back(type);
      nt_trackjet_origin.push_back(origin);

      if (m_useHFTruthHadrons && m_isMC){
	int truthBHadronIndex_min = matchTruthHadrons(*truthBHadrons, jet->p4());
	nt_trackjet_index_truthbhadron.push_back(truthBHadronIndex_min);
      }//if use truth b-hadrons
    }
  }

  ATH_MSG_VERBOSE("TrackJets were good!");
  ATH_MSG_VERBOSE("Starting on leptons!");


  // Fill Electrons:
  nt_nEl = m_signalElectrons->size();
  //! HACK FOR NoW
  if(m_doTruth) {
    nt_nbaselineEl = m_signalElectrons->size();
    nt_nbaselineEl_beforeOR = 0;
    nt_nbaselineEl_lowPt =  0;
  }
  else {
    nt_nbaselineEl = m_baselineElectrons->size();
    nt_nbaselineEl_beforeOR = m_baselineElectronsBeforeOR->size();
    nt_nbaselineEl_lowPt =  m_baselineElectrons_lowPt->size();
  }
  
  // clear vectors:
  nt_el_pt.clear();
  nt_el_eta.clear();
  nt_el_phi.clear();
  nt_el_e.clear();
  nt_el_charge.clear();
  ATH_MSG_VERBOSE("cleared electrons!");
   
  for (const auto& electron : *m_signalElectrons) {
    ATH_MSG_VERBOSE("filling electron");
    nt_el_pt.push_back( electron->p4().Pt() );
    nt_el_eta.push_back( electron->p4().Eta() );
    nt_el_phi.push_back( electron->p4().Phi() );
    nt_el_e.push_back(  electron->p4().E() );
    nt_el_charge.push_back(electron->charge());
  }
  ATH_MSG_VERBOSE("Go to muons now!");

  // Fill muons
  nt_nMu = m_signalMuons->size();
  //! hack PLEASE FIX ME!!
  if(m_doTruth){
    nt_nbaselineMu =m_signalMuons->size();
    nt_ncosmicMu=0;
    nt_nbaselineMu_beforeOR = 0;
    nt_nbaselineMu_lowPt =  0;
  }
  else {
    nt_nbaselineMu=m_baselineMuons->size();
    nt_ncosmicMu=m_cosmicMuons->size();
    nt_nbaselineMu_beforeOR = m_baselineMuonsBeforeOR->size();
    nt_nbaselineMu_lowPt =  m_baselineMuons_lowPt->size();
  }

  // clear vectors:
  nt_mu_pt.clear();
  nt_mu_eta.clear();
  nt_mu_phi.clear();
  nt_mu_e.clear();
  nt_mu_charge.clear();
  
  for (const auto& muon : *m_signalMuons) {
    nt_mu_pt.push_back(  muon->p4().Pt()   );
    nt_mu_eta.push_back(  muon->p4().Eta()  );
    nt_mu_phi.push_back(  muon->p4().Phi() );
    nt_mu_e.push_back(   muon->p4().E() );
    nt_mu_charge.push_back(muon->charge());
  }
  ATH_MSG_VERBOSE("Leptons were good");

  if(m_usePhotons){
    nt_nPh = m_signalPhotons->size();
    //! hack PLEASE FIX ME!!
    if(m_doTruth)  nt_nbaselinePh = nt_nPh;
    else nt_nbaselinePh = m_signalPhotons->size();
    nt_ph_pt.clear();
    nt_ph_eta.clear();
    nt_ph_phi.clear();
    nt_ph_e.clear();

  
    for (const auto& photon : *m_signalPhotons){
      nt_ph_pt.push_back(  photon->p4().Pt()   );
      nt_ph_eta.push_back(  photon->p4().Eta()  );
      nt_ph_phi.push_back(  photon->p4().Phi() );
      nt_ph_e.push_back(   photon->p4().E() );
    }
  }
  ATH_MSG_VERBOSE("Photons were good");



  if(m_useTaus){

    nt_tau_pt.clear();
    nt_tau_eta.clear();
    nt_tau_phi.clear();
    nt_tau_e.clear();
    nt_tau_bdtJet.clear();
    nt_tau_bdtEl.clear();
    nt_tau_nH.clear();
    //nt_tau_nWTracks.clear();
    nt_tau_nTracks.clear();
    nt_tau_nPi0.clear();
    nt_tau_nCharged.clear();
    nt_tau_nNeut.clear();
    nt_tau_passOR.clear();
 
    nt_nbaselineTau=m_signalTausBeforeOR->size();
    nt_nTau=m_signalTaus->size();

    // for (const auto& tau : *m_signalTausBeforeOR){
    for (const auto& tau : *m_signalTaus){
      nt_tau_pt.push_back(  tau->p4().Pt()   );
      nt_tau_eta.push_back(  tau->p4().Eta()  );
      nt_tau_phi.push_back(  tau->p4().Phi() );
      nt_tau_e.push_back(   tau->p4().E() );
      
      
      float BDTJetScore= tau->discriminant( xAOD::TauJetParameters::BDTJetScore );
      float BDTEleScore= tau->discriminant( xAOD::TauJetParameters::BDTEleScore );
	
      nt_tau_bdtJet.push_back(BDTJetScore);
      nt_tau_bdtEl.push_back(BDTEleScore);
      
      //extra variables
      int nH=tau->nHadronicPFOs();
      //int nWTracks=tau->nWideTracks();
      int nTracks=tau->nTracks();
      int nPi0s=tau->nPi0PFOs();
      int nCharged=tau->nChargedPFOs();
      int nNeut=tau->nNeutralPFOs();
      
      // Comment out as it crushes on R21. Information is missing from xAODs (SUSY1/5 p3401 tested).
      // for(int i=0; i<nCharged;i++){
      //   xAOD::PFO* pflow_object=(xAOD::PFO*)tau->chargedPFO(i); 
      //   if(pflow_object){
      //     // double charge = pflow_object->charge(); 
      //     // std::cout << "charge " <<  i << " " << charge << std::endl;
      //   }
      // }
      
      
      nt_tau_nH.push_back(nH);
      //nt_tau_nWTracks.push_back(nWTracks);
      nt_tau_nTracks.push_back(nTracks);
      nt_tau_nPi0.push_back(nPi0s);
      nt_tau_nCharged.push_back(nCharged);
      nt_tau_nNeut.push_back(nNeut);
	
      int passOR = tau->auxdata<char>("passOR") == true ? 1 : 0;
      nt_tau_passOR.push_back(passOR);
      
    }
    
    
  }//end of m_useTaus block
      

  ATH_MSG_VERBOSE("taus were good");
  //--------------------------------------------------------------------
  //                    TRUTH LEPTONS (FOR RECO DERIV)
  //--------------------------------------------------------------------

  //---------------------------TRUTH MUONS-----------------------------

//   nt_truthmu_pt.clear();
//   nt_truthmu_eta.clear();
//   nt_truthmu_phi.clear();
//   nt_truthmu_e.clear();
//   nt_truthmu_status.clear();
//   nt_truthmu_origin.clear();
//   nt_truthmu_type.clear();
//   if (m_isMC){
//     const xAOD::TruthParticleContainer* TruthMuons=nullptr;
//     //retrieve the deepcopy container
//     CHECK(evtStore()->retrieve(TruthMuons,"m_TruthMuons"));
    
//     for (const xAOD::TruthParticle* mu: *TruthMuons){
//       if (mu == nullptr || !mu ){
// 	std::cout<<"One empty mu is in the container"<<std::endl;
// 	continue;
//       }
      
//       //      bool TopW_assoc_mu=isFromTopW(mu);

//       //      if (TopW_assoc_mu)
//       //	std::cout<<"I found a mu associated to a W, woohoo"<<std::endl;
      

//       nt_truthmu_pt.push_back(mu->pt());
//       nt_truthmu_eta.push_back(mu->eta());
//       nt_truthmu_phi.push_back(mu->phi());
//       nt_truthmu_e.push_back(mu->e());
//       std::string origin="";
//       std::string type="";
//       //use the truthparticle container to get a truthclassifier
//       if (m_useMCTruthClassifier){
//       	auto result =m_MCtruthClassifier->particleTruthClassifier(mu);
//       	MCTruthPartClassifier::ParticleDef partDef;
// 	type=partDef.sParticleType[result.first];
// 	origin = partDef.sParticleOrigin[result.second];
//       }
//       nt_truthmu_origin.push_back(origin);
//       nt_truthmu_type.push_back(type);
//       nt_truthmu_status.push_back(mu->status());
//     }//end of loop on truth object

//   }
//   //--------------------------TRUTH ELECTRONS-----------------------------

//   nt_truthel_pt.clear();
//   nt_truthel_eta.clear();
//   nt_truthel_phi.clear();
//   nt_truthel_e.clear();
//   nt_truthel_status.clear();
//   nt_truthel_origin.clear();
//   nt_truthel_type.clear();

//   if (m_isMC){
//     const xAOD::TruthParticleContainer* TruthEl=nullptr;
//     //retrieve the deepcopy container
//     CHECK(evtStore()->retrieve(TruthEl,"m_TruthEl"));
    
//     for (const auto el: *TruthEl){
//       if (el == nullptr || !el || el ==NULL){
// 	std::cout<<"One empty el is in the container"<<std::endl;
// 	continue;
//       }
//       //      bool TopW_assoc_el=isFromTopW(el);

//       //      if (TopW_assoc_el)
//       //	std::cout<<"I found an el associated to a W, woohoo"<<std::endl;
      
//       nt_truthel_pt.push_back(el->pt());
//       nt_truthel_eta.push_back(el->eta());
//       nt_truthel_phi.push_back(el->phi());
//       nt_truthel_e.push_back(el->e());

//       std::string origin="";
//       std::string type="";
//       //use the truthparticle container to get a truthclassifier
//       if (m_useMCTruthClassifier){
//       	auto result2 =m_MCtruthClassifier->particleTruthClassifier(el);
//       	MCTruthPartClassifier::ParticleDef partDef1;
// 	origin = partDef1.sParticleOrigin[result2.second];
// 	type=partDef1.sParticleType[result2.first];

//       }
//       nt_truthel_type.push_back(type);
//       nt_truthel_origin.push_back(origin);
//       nt_truthel_status.push_back(el->status());
//     }//end of loop on truth object
//   }
// //end of MC block

//   //--------------------------------TRUTH TAUS-----------------------------

//   nt_truthtau_pt.clear();
//   nt_truthtau_eta.clear();
//   nt_truthtau_phi.clear();
//   nt_truthtau_e.clear();
//   nt_truthtau_status.clear();
//   nt_truthtau_origin.clear();
//   nt_truthtau_type.clear();
//   nt_truthtau_ishadronic.clear();

	     
//   if (m_isMC){
//     const xAOD::TruthParticleContainer* m_TruthTaus=nullptr;
//     //retrieve the deepcopy container
//     CHECK(evtStore()->retrieve(m_TruthTaus,"m_TruthTaus"));
    
//     for (const auto tau: *m_TruthTaus){
//       if (tau == nullptr || !tau ){
// 		   std::cout<<"One empty tau is in the container"<<std::endl;
// 		   continue;
//       }
//       nt_truthtau_pt.push_back(tau->pt());
//       nt_truthtau_eta.push_back(tau->eta());
//       nt_truthtau_phi.push_back(tau->phi());
//       nt_truthtau_e.push_back(tau->e());
      
//       nt_truthtau_ishadronic.push_back((bool)tau->auxdata<char>("IsHadronicTau")); // As tau can only decay either hadronically or leptonically, boolean will check both
      
//       std::string origin="NA";
//       std::string type="NA";
//       // use the truthparticle container to get a truthclassifier
//       if (m_useMCTruthClassifier){
// 	auto result =m_MCtruthClassifier->particleTruthClassifier(tau);
// 	MCTruthPartClassifier::ParticleDef partDef;
// 	origin = partDef.sParticleOrigin[result.second];
// 	type=partDef.sParticleType[result.first];
	
//       }
//       nt_truthtau_origin.push_back(origin);
//       nt_truthtau_type.push_back(type);
//       nt_truthtau_status.push_back(tau->status());
      
//     }
    
//   }// end of MC block for TruthTaus
  
 
  
  ATH_MSG_VERBOSE("Truth Leptons (if defined) were good");
  
  //fill met
  
  TVector2 met;
  met.Set(m_pxmiss,m_pymiss);
  nt_MET_pt=met.Mod();
  nt_MET_phi=met.Phi();

  TVector2 met_prime;
  met_prime.Set(m_pxmiss_prime,m_pymiss_prime);
  nt_MET_pt_prime=met_prime.Mod();
  nt_MET_phi_prime=met_prime.Phi();
  
  nt_sumet=m_sumet;
 
  
  nt_metsig=m_metsig;
  nt_metsig_default=m_metsig_default;
  nt_metsig_binflate=m_metsig_binflate;
  nt_metsig_run1JER=m_metsig_run1JER;

  nt_metsigLepJetResol=m_metsigLepJetResol;
  nt_metsigST=m_metsigST;
  nt_metsigHT=m_metsigHT;
  nt_metsigET=m_metsigET;


  
  ATH_MSG_VERBOSE("Filled MET");

  // fill total number of tracks 

  nt_ntrk_global = m_ntrk_global;

  ATH_MSG_VERBOSE("Filled number of tracks");

  return true;
  
}


bool  MasterShef::fillExtraRecoVariables(bool init, TTree* tree){

  ATH_MSG_VERBOSE("in extra reco variables");
  
  bool isNominal = true;
  if( std::string(tree->GetName()) != "Nominal" )
    isNominal=false;

  // std::string sysName = m_systNameList.at(m_systNum);
  // bool isNominal(true);
  // if (sysName != "") isNominal=false;
   
  if(init){

    tree->Branch("year", &nt_year);

    //variables associated with data
    tree->Branch("lbn",&nt_lbn);  
    tree->Branch("bcid",&nt_bcid);
  
    //event Weights
    tree->Branch("TriggerSF",&nt_TriggerSF);
    tree->Branch("MuonWeightReco",&nt_muonsRecoSF);
    tree->Branch("MuonWeightTrigger",&nt_muonsTrigSF);
    tree->Branch("ElecWeightReco",&nt_electronsRecoSF);
    tree->Branch("ElecWeightTrigger",&nt_electronsTrigSF);
    tree->Branch("LeptonsGlobalTriggerSF",&nt_leptonsGlobalTrigSF);
    if(m_usePhotons)
      tree->Branch("PhotonWeight",&nt_photonSF);
    if(m_useTaus)
      tree->Branch("TauWeight",&nt_tauSF);

    //prescale weights
    //tree->Branch("TriggerWeight",&nt_TriggerWeight);
    tree->Branch("PrescaleWeight",&nt_TriggerWeight);

    tree->Branch("jvtweight",&nt_jvtweight);    
    tree->Branch("btagweight",&nt_btagweight);
    tree->Branch("btagtrkweight",&nt_btagtrkweight);

    if(m_saveExperimentalSyst && isNominal){
      
      
      tree->Branch("MuonWeightTrigTRIGSYSTUP",&nt_muonsTrigSF_TRIGSYSTUP);
      tree->Branch("MuonWeightTrigTRIGSYSTDOWN",&nt_muonsTrigSF_TRIGSYSTDOWN);
      tree->Branch("MuonWeightTrigTRIGSTATUP",&nt_muonsTrigSF_TRIGSTATUP);
      tree->Branch("MuonWeightTrigTRIGSTATDOWN",&nt_muonsTrigSF_TRIGSTATDOWN);
      tree->Branch("MuonWeightTrigTTVASYSTUP",&nt_muonsTrigSF_TTVASYSTUP);
      tree->Branch("MuonWeightTrigTTVASYSTDOWN",&nt_muonsTrigSF_TTVASYSTDOWN);
      tree->Branch("MuonWeightTrigTTVASTATUP",&nt_muonsTrigSF_TTVASTATUP);
      tree->Branch("MuonWeightTrigTTVASTATDOWN",&nt_muonsTrigSF_TTVASTATDOWN);
      tree->Branch("MuonWeightTrigISOSYSTUP",&nt_muonsTrigSF_ISOSYSTUP);
      tree->Branch("MuonWeightTrigISOSYSTDOWN",&nt_muonsTrigSF_ISOSYSTDOWN);
      tree->Branch("MuonWeightTrigISOSTATUP",&nt_muonsTrigSF_ISOSTATUP);
      tree->Branch("MuonWeightTrigISOSTATDOWN",&nt_muonsTrigSF_ISOSTATDOWN);
      tree->Branch("MuonWeightTrigBADMUONSYSUP",&nt_muonsTrigSF_BADMUONSYSUP);
      tree->Branch("MuonWeightTrigBADMUONSYSDOWN",&nt_muonsTrigSF_BADMUONSYSDOWN);
      tree->Branch("MuonWeightTrigBADMUONSTATUP",&nt_muonsTrigSF_BADMUONSTATUP);
      tree->Branch("MuonWeightTrigBADMUONSTATDOWN",&nt_muonsTrigSF_BADMUONSTATDOWN);
      tree->Branch("MuonWeightTrigRECOSYSUP",&nt_muonsTrigSF_RECOSYSUP);
      tree->Branch("MuonWeightTrigRECOSYSDOWN",&nt_muonsTrigSF_RECOSYSDOWN);
      tree->Branch("MuonWeightTrigRECOSTATUP",&nt_muonsTrigSF_RECOSTATUP);
      tree->Branch("MuonWeightTrigRECOSTATDOWN",&nt_muonsTrigSF_RECOSTATDOWN);
      tree->Branch("MuonWeightTrigRECOSYSLOWPTUP",&nt_muonsTrigSF_RECOSYSLOWPTUP);
      tree->Branch("MuonWeightTrigRECOSYSLOWPTDOWN",&nt_muonsTrigSF_RECOSYSLOWPTDOWN);
      tree->Branch("MuonWeightTrigRECOSTATLOWPTUP",&nt_muonsTrigSF_RECOSTATLOWPTUP);
      tree->Branch("MuonWeightTrigRECOSTATLOWPTDOWN",&nt_muonsTrigSF_RECOSTATLOWPTDOWN);

      tree->Branch("MuonWeightTTVASYSTUP",&nt_muonsSF_TTVASYSTUP);
      tree->Branch("MuonWeightTTVASYSTDOWN",&nt_muonsSF_TTVASYSTDOWN);
      tree->Branch("MuonWeightTTVASTATUP",&nt_muonsSF_TTVASTATUP);
      tree->Branch("MuonWeightTTVASTATDOWN",&nt_muonsSF_TTVASTATDOWN);
      tree->Branch("MuonWeightISOSYSTUP",&nt_muonsSF_ISOSYSTUP);
      tree->Branch("MuonWeightISOSYSTDOWN",&nt_muonsSF_ISOSYSTDOWN);
      tree->Branch("MuonWeightISOSTATUP",&nt_muonsSF_ISOSTATUP);
      tree->Branch("MuonWeightISOSTATDOWN",&nt_muonsSF_ISOSTATDOWN);
      tree->Branch("MuonWeightBADMUONSYSUP",&nt_muonsSF_BADMUONSYSUP);
      tree->Branch("MuonWeightBADMUONSYSDOWN",&nt_muonsSF_BADMUONSYSDOWN);
      tree->Branch("MuonWeightBADMUONSTATUP",&nt_muonsSF_BADMUONSTATUP);
      tree->Branch("MuonWeightBADMUONSTATDOWN",&nt_muonsSF_BADMUONSTATDOWN);
      tree->Branch("MuonWeightRECOSYSUP",&nt_muonsSF_RECOSYSUP);
      tree->Branch("MuonWeightRECOSYSDOWN",&nt_muonsSF_RECOSYSDOWN);
      tree->Branch("MuonWeightRECOSTATUP",&nt_muonsSF_RECOSTATUP);
      tree->Branch("MuonWeightRECOSTATDOWN",&nt_muonsSF_RECOSTATDOWN);
      tree->Branch("MuonWeightRECOSYSLOWPTUP",&nt_muonsSF_RECOSYSLOWPTUP);
      tree->Branch("MuonWeightRECOSYSLOWPTDOWN",&nt_muonsSF_RECOSYSLOWPTDOWN);
      tree->Branch("MuonWeightRECOSTATLOWPTUP",&nt_muonsSF_RECOSTATLOWPTUP);
      tree->Branch("MuonWeightRECOSTATLOWPTDOWN",&nt_muonsSF_RECOSTATLOWPTDOWN);
  
      tree->Branch("ElecWeightTrigIDUP",&nt_electronsTrigSF_IDUP);
      tree->Branch("ElecWeightTrigIDDOWN",&nt_electronsTrigSF_IDDOWN);
      tree->Branch("ElecWeightTrigISOUP",&nt_electronsTrigSF_ISOUP);
      tree->Branch("ElecWeightTrigISODOWN",&nt_electronsTrigSF_ISODOWN);
      tree->Branch("ElecWeightTrigRECOUP",&nt_electronsTrigSF_RECOUP);
      tree->Branch("ElecWeightTrigRECODOWN",&nt_electronsTrigSF_RECODOWN);
      tree->Branch("ElecWeightTrigTRIGUP",&nt_electronsTrigSF_TRIGUP);
      tree->Branch("ElecWeightTrigTRIGDOWN",&nt_electronsTrigSF_TRIGDOWN);
      tree->Branch("ElecWeightTrigTRIGEFFUP",&nt_electronsTrigSF_TRIGEFFUP);
      tree->Branch("ElecWeightTrigTRIGEFFDOWN",&nt_electronsTrigSF_TRIGEFFDOWN);
      tree->Branch("ElecWeightTrigCHARGEIDSELUP",&nt_electronsTrigSF_CHARGEIDSELUP);
      tree->Branch("ElecWeightTrigCHARGEIDSELDOWN",&nt_electronsTrigSF_CHARGEIDSELDOWN);

      tree->Branch("ElecWeightRECOUP",&nt_electronsSF_RECOUP);
      tree->Branch("ElecWeightRECODOWN",&nt_electronsSF_RECODOWN);
      tree->Branch("ElecWeightISOUP",&nt_electronsSF_ISOUP);
      tree->Branch("ElecWeightISODOWN",&nt_electronsSF_ISODOWN);
      tree->Branch("ElecWeightIDUP",&nt_electronsSF_IDUP);
      tree->Branch("ElecWeightIDDOWN",&nt_electronsSF_IDDOWN);
      tree->Branch("ElecWeightCHARGEIDSELUP",&nt_electronsSF_CHARGEIDSELUP);
      tree->Branch("ElecWeightCHARGEIDSELDOWN",&nt_electronsSF_CHARGEIDSELDOWN);


      // Trigger Global Tool uncertainties
      tree->Branch("LeptonsGlobalTrigTRIGUP",&nt_leptonsGlobalTrigSF_TRIGUP);
      tree->Branch("LeptonsGlobalTrigTRIGDOWN",&nt_leptonsGlobalTrigSF_TRIGDOWN);
      tree->Branch("LeptonsGlobalTrigTRIGEFFUP",&nt_leptonsGlobalTrigSF_TRIGEFFUP);
      tree->Branch("LeptonsGlobalTrigTRIGEFFDOWN",&nt_leptonsGlobalTrigSF_TRIGEFFDOWN);
      tree->Branch("LeptonsGlobalTrigIDUP",&nt_leptonsGlobalTrigSF_IDUP);
      tree->Branch("LeptonsGlobalTrigIDDOWN",&nt_leptonsGlobalTrigSF_IDDOWN);
      tree->Branch("LeptonsGlobalTrigISOUP",&nt_leptonsGlobalTrigSF_ISOUP);
      tree->Branch("LeptonsGlobalTrigISODOWN",&nt_leptonsGlobalTrigSF_ISODOWN);
      tree->Branch("LeptonsGlobalTrigRECOUP",&nt_leptonsGlobalTrigSF_RECOUP);
      tree->Branch("LeptonsGlobalTrigRECODOWN",&nt_leptonsGlobalTrigSF_RECODOWN);
      tree->Branch("LeptonsGlobalTrigCHARGEIDSELUP",&nt_leptonsGlobalTrigSF_CHARGEIDSELUP);
      tree->Branch("LeptonsGlobalTrigCHARGEIDSELDOWN",&nt_leptonsGlobalTrigSF_CHARGEIDSELDOWN);
      tree->Branch("LeptonsGlobalTrigMuonTRIGSYSTUP",&nt_leptonsGlobalTrigSF_MUONTRIGSYSUP);
      tree->Branch("LeptonsGlobalTrigMuonTRIGSYSTDOWN",&nt_leptonsGlobalTrigSF_MUONTRIGSYSDOWN);
      tree->Branch("LeptonsGlobalTrigMuonTRIGSTATUP",&nt_leptonsGlobalTrigSF_MUONTRIGSTATUP);
      tree->Branch("LeptonsGlobalTrigMuonTRIGSTATDOWN",&nt_leptonsGlobalTrigSF_MUONTRIGSTATDOWN);
      tree->Branch("LeptonsGlobalTrigMuonTTVASYSTUP",&nt_leptonsGlobalTrigMuonSF_TTVASYSTUP);
      tree->Branch("LeptonsGlobalTrigMuonTTVASYSTDOWN",&nt_leptonsGlobalTrigMuonSF_TTVASYSTDOWN);
      tree->Branch("LeptonsGlobalTrigMuonTTVASTATUP",&nt_leptonsGlobalTrigMuonSF_TTVASTATUP);
      tree->Branch("LeptonsGlobalTrigMuonTTVASTATDOWN",&nt_leptonsGlobalTrigMuonSF_TTVASTATDOWN);
      tree->Branch("LeptonsGlobalTrigMuonISOSYSTUP",&nt_leptonsGlobalTrigMuonSF_ISOSYSTUP);
      tree->Branch("LeptonsGlobalTrigMuonISOSYSTDOWN",&nt_leptonsGlobalTrigMuonSF_ISOSYSTDOWN);
      tree->Branch("LeptonsGlobalTrigMuonISOSTATUP",&nt_leptonsGlobalTrigMuonSF_ISOSTATUP);
      tree->Branch("LeptonsGlobalTrigMuonISOSTATDOWN",&nt_leptonsGlobalTrigMuonSF_ISOSTATDOWN);
      tree->Branch("LeptonsGlobalTrigMuonBADMUONSYSUP",&nt_leptonsGlobalTrigMuonSF_BADMUONSYSUP);
      tree->Branch("LeptonsGlobalTrigMuonBADMUONSYSDOWN",&nt_leptonsGlobalTrigMuonSF_BADMUONSYSDOWN);
      tree->Branch("LeptonsGlobalTrigMuonBADMUONSTATUP",&nt_leptonsGlobalTrigMuonSF_BADMUONSTATUP);
      tree->Branch("LeptonsGlobalTrigMuonBADMUONSTATDOWN",&nt_leptonsGlobalTrigMuonSF_BADMUONSTATDOWN);
      tree->Branch("LeptonsGlobalTrigMuonRECOSYSUP",&nt_leptonsGlobalTrigMuonSF_RECOSYSUP);
      tree->Branch("LeptonsGlobalTrigMuonRECOSYSDOWN",&nt_leptonsGlobalTrigMuonSF_RECOSYSDOWN);
      tree->Branch("LeptonsGlobalTrigMuonRECOSTATUP",&nt_leptonsGlobalTrigMuonSF_RECOSTATUP);
      tree->Branch("LeptonsGlobalTrigMuonRECOSTATDOWN",&nt_leptonsGlobalTrigMuonSF_RECOSTATDOWN);
      tree->Branch("LeptonsGlobalTrigMuonRECOSYSLOWPTUP",&nt_leptonsGlobalTrigMuonSF_RECOSYSLOWPTUP);
      tree->Branch("LeptonsGlobalTrigMuonRECOSYSLOWPTDOWN",&nt_leptonsGlobalTrigMuonSF_RECOSYSLOWPTDOWN);
      tree->Branch("LeptonsGlobalTrigMuonRECOSTATLOWPTUP",&nt_leptonsGlobalTrigMuonSF_RECOSTATLOWPTUP);
      tree->Branch("LeptonsGlobalTrigMuonRECOSTATLOWPTDOWN",&nt_leptonsGlobalTrigMuonSF_RECOSTATLOWPTDOWN);


      tree->Branch("PhotonSF_IDUP",&nt_photonSF_IDUP);
      tree->Branch("PhotonSF_IDDOWN",&nt_photonSF_IDDOWN);
      tree->Branch("PhotonSF_TRKISOUP",&nt_photonSF_TRKISOUP);
      tree->Branch("PhotonSF_TRKISODOWN",&nt_photonSF_TRKISODOWN);

      if(m_useTaus){
	tree->Branch("TauWeightTRUEELECTRONELEOLRDOWN",&nt_tauSF_TRUEELECTRON_ELEOLR_TOTAL__1down);
	tree->Branch("TauWeightTRUEELECTRONELEOLRUP",&nt_tauSF_TRUEELECTRON_ELEOLR_TOTAL__1up);
	tree->Branch("TauWeightTRUEHADTAUELEOLRDOWN",&nt_tauSF_TRUEHADTAU_ELEOLR_TOTAL__1down);
	tree->Branch("TauWeightTRUEHADTAUELEOLRUP",&nt_tauSF_TRUEHADTAU_ELEOLR_TOTAL__1up);
	tree->Branch("TauWeightJETID1PRONGSTATSYSTUNCORR2025DOWN",&nt_tauSF_JETID_1PRONGSTATSYSTUNCORR2025__1down);
	tree->Branch("TauWeightJETID1PRONGSTATSYSTUNCORR2025UP",&nt_tauSF_JETID_1PRONGSTATSYSTUNCORR2025__1up);
	tree->Branch("TauWeightJETID1PRONGSTATSYSTUNCORR2530DOWN",&nt_tauSF_JETID_1PRONGSTATSYSTUNCORR2530__1down);
	tree->Branch("TauWeightJETID1PRONGSTATSYSTUNCORR2530UP",&nt_tauSF_JETID_1PRONGSTATSYSTUNCORR2530__1up);
	tree->Branch("TauWeightJETID1PRONGSTATSYSTUNCORR3040DOWN",&nt_tauSF_JETID_1PRONGSTATSYSTUNCORR3040__1down);
	tree->Branch("TauWeightJETID1PRONGSTATSYSTUNCORR3040UP",&nt_tauSF_JETID_1PRONGSTATSYSTUNCORR3040__1up);
	tree->Branch("TauWeightJETID1PRONGSTATSYSTUNCORR40DOWN",&nt_tauSF_JETID_1PRONGSTATSYSTUNCORR40__1down);
	tree->Branch("TauWeightJETID1PRONGSTATSYSTUNCORR40UP",&nt_tauSF_JETID_1PRONGSTATSYSTUNCORR40__1up);
	tree->Branch("TauWeightJETID3PRONGSTATSYSTUNCORR2030DOWN",&nt_tauSF_JETID_3PRONGSTATSYSTUNCORR2030__1down);
	tree->Branch("TauWeightJETID3PRONGSTATSYSTUNCORR2030UP",&nt_tauSF_JETID_3PRONGSTATSYSTUNCORR2030__1up);
	tree->Branch("TauWeightJETID3PRONGSTATSYSTUNCORR30DOWN",&nt_tauSF_JETID_3PRONGSTATSYSTUNCORR30__1down);
	tree->Branch("TauWeightJETID3PRONGSTATSYSTUNCORR30UP",&nt_tauSF_JETID_3PRONGSTATSYSTUNCORR30__1up);
	tree->Branch("TauWeightJETIDHIGHPTDOWN",&nt_tauSF_JETID_HIGHPT__1down);
	tree->Branch("TauWeightJETIDHIGHPTUP",&nt_tauSF_JETID_HIGHPT__1up);
	tree->Branch("TauWeightJETIDSYSTDOWN",&nt_tauSF_JETID_SYST__1down);
	tree->Branch("TauWeightJETIDSYSTUP",&nt_tauSF_JETID_SYST__1up);
	tree->Branch("TauWeightRECOHIGHPTDOWN",&nt_tauSF_RECO_HIGHPT__1down);
	tree->Branch("TauWeightRECOHIGHPTUP",&nt_tauSF_RECO_HIGHPT__1up);
	tree->Branch("TauWeightRECOTOTALDOWN",&nt_tauSF_RECO_TOTAL__1down);
	tree->Branch("TauWeightRECOTOTALUP",&nt_tauSF_RECO_TOTAL__1up);
	// Trigger systematics : Remove them unless some tau trigger is applied 
	// tree->Branch("TauWeightTRIGGER_STATDATA2015DOWN",&nt_tauSF_TRIGGER_STATDATA2015__1down);
	// tree->Branch("TauWeightTRIGGER_STATDATA2015UP",&nt_tauSF_TRIGGER_STATDATA2015__1up);
	// tree->Branch("TauWeightTRIGGER_STATDATA2016DOWN",&nt_tauSF_TRIGGER_STATDATA2016__1down);
	// tree->Branch("TauWeightTRIGGER_STATDATA2016UP",&nt_tauSF_TRIGGER_STATDATA2016__1up);
	// tree->Branch("TauWeightTRIGGER_STATDATA2017DOWN",&nt_tauSF_TRIGGER_STATDATA2017__1down);
	// tree->Branch("TauWeightTRIGGER_STATDATA2017UP",&nt_tauSF_TRIGGER_STATDATA2017__1up);
	// tree->Branch("TauWeightTRIGGER_STATMC2015DOWN",&nt_tauSF_TRIGGER_STATMC2015__1down);
	// tree->Branch("TauWeightTRIGGER_STATMC2015UP",&nt_tauSF_TRIGGER_STATMC2015__1up);
	// tree->Branch("TauWeightTRIGGER_STATMC2016DOWN",&nt_tauSF_TRIGGER_STATMC2016__1down);
	// tree->Branch("TauWeightTRIGGER_STATMC2016UP",&nt_tauSF_TRIGGER_STATMC2016__1up);
	// tree->Branch("TauWeightTRIGGER_STATMC2017DOWN",&nt_tauSF_TRIGGER_STATMC2017__1down);
	// tree->Branch("TauWeightTRIGGER_STATMC2017UP",&nt_tauSF_TRIGGER_STATMC2017__1up);
	// tree->Branch("TauWeightTRIGGER_SYST2015DOWN",&nt_tauSF_TRIGGER_SYST2015__1down);
	// tree->Branch("TauWeightTRIGGER_SYST2015UP",&nt_tauSF_TRIGGER_SYST2015__1up);
	// tree->Branch("TauWeightTRIGGER_SYST2016DOWN",&nt_tauSF_TRIGGER_SYST2016__1down);
	// tree->Branch("TauWeightTRIGGER_SYST2016UP",&nt_tauSF_TRIGGER_SYST2016__1up);
	// tree->Branch("TauWeightTRIGGER_SYST2017DOWN",&nt_tauSF_TRIGGER_SYST2017__1down);
	// tree->Branch("TauWeightTRIGGER_SYST2017UP",&nt_tauSF_TRIGGER_SYST2017__1up);
      }

      tree->Branch("jetfJvtEfficiencyUP",&nt_jetfJvtEfficiencyUP);
      tree->Branch("jetfJvtEfficiencyDOWN",&nt_jetfJvtEfficiencyDOWN);
      tree->Branch("jetJvtEfficiencyUP",&nt_jetJvtEfficiencyUP);
      tree->Branch("jetJvtEfficiencyDOWN",&nt_jetJvtEfficiencyDOWN);

      if(m_isEnvelope){
	tree->Branch("btagweightBUP",&nt_btagweightBUP);
	tree->Branch("btagweightBDOWN",&nt_btagweightBDOWN);
	tree->Branch("btagweightCUP",&nt_btagweightCUP);
	tree->Branch("btagweightCDOWN",&nt_btagweightCDOWN);
	tree->Branch("btagweightLUP",&nt_btagweightLUP);
	tree->Branch("btagweightLDOWN",&nt_btagweightLDOWN);
	tree->Branch("btagweightExUP",&nt_btagweightExUP);
	tree->Branch("btagweightExDOWN",&nt_btagweightExDOWN);
	tree->Branch("btagweightExCUP",&nt_btagweightExCUP);
	tree->Branch("btagweightExCDOWN",&nt_btagweightExDOWN);
      }else{
	tree->Branch("btagweight_B0_UP",&nt_btagweight_B0_UP);
	tree->Branch("btagweight_B0_DOWN",&nt_btagweight_B0_DOWN);
	tree->Branch("btagweight_B1_UP",&nt_btagweight_B1_UP);
	tree->Branch("btagweight_B1_DOWN",&nt_btagweight_B1_DOWN);
	tree->Branch("btagweight_B2_UP",&nt_btagweight_B2_UP);
	tree->Branch("btagweight_B2_DOWN",&nt_btagweight_B2_DOWN);
	tree->Branch("btagweight_B3_UP",&nt_btagweight_B3_UP);
	tree->Branch("btagweight_B3_DOWN",&nt_btagweight_B3_DOWN);
	tree->Branch("btagweight_B4_UP",&nt_btagweight_B4_UP);
	tree->Branch("btagweight_B4_DOWN",&nt_btagweight_B4_DOWN);
	tree->Branch("btagweight_B5_UP",&nt_btagweight_B5_UP);
	tree->Branch("btagweight_B5_DOWN",&nt_btagweight_B5_DOWN);
	tree->Branch("btagweight_B6_UP",&nt_btagweight_B6_UP);
	tree->Branch("btagweight_B6_DOWN",&nt_btagweight_B6_DOWN);
	tree->Branch("btagweight_B7_UP",&nt_btagweight_B7_UP);
	tree->Branch("btagweight_B7_DOWN",&nt_btagweight_B7_DOWN);
	tree->Branch("btagweight_B8_UP",&nt_btagweight_B8_UP);
	tree->Branch("btagweight_B8_DOWN",&nt_btagweight_B8_DOWN);
	tree->Branch("btagweight_C0_UP",&nt_btagweight_C0_UP);
	tree->Branch("btagweight_C0_DOWN",&nt_btagweight_C0_DOWN);
	tree->Branch("btagweight_C1_UP",&nt_btagweight_C1_UP);
	tree->Branch("btagweight_C1_DOWN",&nt_btagweight_C1_DOWN);
	tree->Branch("btagweight_C2_UP",&nt_btagweight_C2_UP);
	tree->Branch("btagweight_C2_DOWN",&nt_btagweight_C2_DOWN);
	tree->Branch("btagweight_C3_UP",&nt_btagweight_C3_UP);
	tree->Branch("btagweight_C3_DOWN",&nt_btagweight_C3_DOWN);
	tree->Branch("btagweight_L0_UP",&nt_btagweight_L0_UP);
	tree->Branch("btagweight_L0_DOWN",&nt_btagweight_L0_DOWN);
	tree->Branch("btagweight_L1_UP",&nt_btagweight_L1_UP);
	tree->Branch("btagweight_L1_DOWN",&nt_btagweight_L1_DOWN);
	tree->Branch("btagweight_L2_UP",&nt_btagweight_L2_UP);
	tree->Branch("btagweight_L2_DOWN",&nt_btagweight_L2_DOWN);
	tree->Branch("btagweight_L3_UP",&nt_btagweight_L3_UP);
	tree->Branch("btagweight_L3_DOWN",&nt_btagweight_L3_DOWN);
	tree->Branch("btagweight_L4_UP",&nt_btagweight_L4_UP);
	tree->Branch("btagweight_L4_DOWN",&nt_btagweight_L4_DOWN);
	tree->Branch("btagweightExUP",&nt_btagweightExUP);
	tree->Branch("btagweightExDOWN",&nt_btagweightExDOWN);
	tree->Branch("btagweightExCUP",&nt_btagweightExCUP);
	tree->Branch("btagweightExCDOWN",&nt_btagweightExDOWN);
      }
      if( m_useTrackJets ){
        tree->Branch("btagtrkweightBUP",&nt_btagtrkweightBUP);
	tree->Branch("btagtrkweightBDOWN",&nt_btagtrkweightBDOWN);
	tree->Branch("btagtrkweightCUP",&nt_btagtrkweightCUP);
	tree->Branch("btagtrkweightCDOWN",&nt_btagtrkweightCDOWN);
	tree->Branch("btagtrkweightLUP",&nt_btagtrkweightLUP);
	tree->Branch("btagtrkweightLDOWN",&nt_btagtrkweightLDOWN);
	tree->Branch("btagtrkweightExUP",&nt_btagtrkweightExUP);
	tree->Branch("btagtrkweightExDOWN",&nt_btagtrkweightExDOWN);
	tree->Branch("btagtrkweightExCUP",&nt_btagtrkweightExCUP);
	tree->Branch("btagtrkweightExCDOWN",&nt_btagtrkweightExDOWN);
	
      }
      
    }

  
    tree->Branch("averageIntPerXing",&nt_averageIntPerXing);
    tree->Branch("actualIntPerXing",&nt_actualIntPerXing);
    tree->Branch("averageIntPerXingCorr",&nt_averageIntPerXingCorr);
    tree->Branch("nVertices",&nt_nVertices);
    tree->Branch("nVertices_PriVrt",&nt_nVertices_PriVtr);


    //hlt leading jet
    //tree->Branch("HLTjet_pt",&nt_hlt_jet);
    //additional reco level jet information
    //tree->Branch("jet_MV2c10",&nt_jet_MV2c10);
    //tree->Branch("jet_DL1",&nt_jet_DL1);
    tree->Branch("jet_btagweight",&nt_jet_btagweight);
    tree->Branch("jet_MV2c10",&nt_jet_MV2c10);
    //tree->Branch("jet_DL1",&nt_jet_DL1);

    tree->Branch("jet_DL1_pu",&nt_jet_DL1_pu);
    tree->Branch("jet_DL1_pc",&nt_jet_DL1_pc);
    tree->Branch("jet_DL1_pb",&nt_jet_DL1_pb);
    tree->Branch("jet_DL1r_pu",&nt_jet_DL1r_pu);
    tree->Branch("jet_DL1r_pc",&nt_jet_DL1r_pc);
    tree->Branch("jet_DL1r_pb",&nt_jet_DL1r_pb);
    tree->Branch("jet_DL1r_cscore",&nt_jet_DL1r_cscore);

    // tree->Branch("jet_isMH85",&nt_isMV2c10H85);
    // tree->Branch("jet_isMH77",&nt_isMV2c10H77);
    // tree->Branch("jet_isMH70",&nt_isMV2c10H70);
    // tree->Branch("jet_isDH85",&nt_isDL1H85);
    // tree->Branch("jet_isDH77",&nt_isDL1H77);
    // tree->Branch("jet_isDH70",&nt_isDL1H70);

    //tree->Branch("jet_MV2c20",&nt_jet_MV2c20);


    // if(m_saveJetCleaningVars){
    //   tree->Branch("jet_chf",&nt_jet_chf);
    //   tree->Branch("jet_jvtxf",&nt_jet_jvtxf);
    //   tree->Branch("jet_BCH_CORR_CELL",&nt_jet_BCH_CORR_CELL);
    //   tree->Branch("jet_emfrac",&nt_jet_emfrac); 
    //   tree->Branch("jet_fmax",&nt_jet_fmax); 
    // }      

    tree->Branch("jet_passTightClean",&nt_jet_passTightClean);
    tree->Branch("jet_passTightCleanDFFlag",&nt_jet_passTightCleanDFFlag);
    tree->Branch("jet_isSimpleTau",&nt_jet_isSimpleTau); 
    tree->Branch("jet_ntracks",&nt_jet_ntracks);
    if(m_isMC){
      tree->Branch("jet_truthflavhadcone",&nt_jet_truthflavhadcone);
      tree->Branch("jet_truthflavhadexcl",&nt_jet_truthflavhadexcl);
    }

    if(m_useJetConstituents){
      tree->Branch("jet_PullX",& nt_jet_PullMag);
      tree->Branch("jet_PullY",& nt_jet_PullPhi);
    }

      
    //extra lepton reco level variables
    tree->Branch("el_SF",&nt_el_SF); 
    tree->Branch("ncosmicMu",&nt_ncosmicMu);
    tree->Branch("mu_SF",&nt_mu_SF);
    tree->Branch("ph_topoetcone20",&nt_ph_topoetcone20);
    tree->Branch("ph_topoetcone40",&nt_ph_topoetcone40);
    tree->Branch("ptcone20",&nt_ph_ptcone20);
    tree->Branch("ptvarcone20",&nt_ph_ptvarcone20);                              
  
    // pass tau veto check
    tree->Branch("passtauveto",&nt_passtauveto);

  
    //additional breakdown of met variables  
    tree->Branch("MET_jet_pt",&nt_MET_jet_pt);
    tree->Branch("MET_jet_phi",&nt_MET_jet_phi);
      
    tree->Branch("MET_mu_pt",&nt_MET_mu_pt);
    tree->Branch("MET_mu_phi",&nt_MET_mu_phi);
      
    tree->Branch("MET_el_pt",&nt_MET_el_pt);
    tree->Branch("MET_el_phi",&nt_MET_el_phi);
      
    tree->Branch("MET_y_pt",&nt_MET_y_pt);
    tree->Branch("MET_y_phi",&nt_MET_y_phi);
      
    tree->Branch("MET_softTrk_pt",&nt_MET_softTrk_pt);
    tree->Branch("MET_softTrk_phi",&nt_MET_softTrk_phi);
      
    
    tree->Branch("MET_track_pt",&nt_trackMET_pt);
    tree->Branch("MET_track_phi",&nt_trackMET_phi);
    
    //tree->Branch("MET_pt_inv",&nt_MET_pt_inv);
    // tree->Branch("MET_phi_inv",&nt_MET_phi_inv);
    
    tree->Branch("MET_NonInt_pt",&nt_MET_NonInt_pt);
    tree->Branch("MET_NonInt_phi",&nt_MET_NonInt_phi);
    if(m_useTaus){
      tree->Branch("MET_tau_pt",&nt_MET_tau_pt);
      tree->Branch("MET_tau_phi",&nt_MET_tau_phi);
    }
  
    tree->Branch("sumet_jet",&nt_sumet_jet);   
    tree->Branch("sumet_el",&nt_sumet_el);  
    tree->Branch("sumet_y",&nt_sumet_y);   
    tree->Branch("sumet_mu",&nt_sumet_mu);   
    tree->Branch("sumet_softTrk",&nt_sumet_softTrk);   
    tree->Branch("sumet_softClu",&nt_sumet_softClu);   
    tree->Branch("sumet_NonInt",&nt_sumet_NonInt);
  
    //tree->Branch("passTSTCleaning",&nt_passTSTCleaning);
    //tree->Branch("cellMET",&nt_cellMET);
    //tree->Branch("mhtMET",&nt_mhtMET);


    //Triggers
   
    tree->Branch("METTrigPassed",&nt_IsMETTrigPassed);
   
    // Branches: Trigger
    for (unsigned int iT=0; iT<m_triggerNtup.size(); iT++) {
      bool* ptrig=&(nt_trigPass[iT]);
      bool* pmatch=&(nt_trigMatch[iT]);
    
      std::string name = m_triggerNtup.at(iT);
      std::string temp = name;
    
      if(name.find("||")!=std::string::npos){
	temp=name.replace(name.find("||"),std::string("||").length(),"OR");
      }
      std::string temp2 = "matched_"+temp;
      tree->Branch(temp.c_str(), ptrig ); 
      tree->Branch(temp2.c_str(), pmatch );

    }
    
    return true;
  }
    
  ATH_MSG_VERBOSE("Doing extra reco variables");


  nt_lbn = eventInfo->lumiBlock();
  nt_bcid = eventInfo->bcid();


  if(m_isMC)
    nt_year = m_objTool->treatAsYear();
  else
    nt_year = 0;
  
  ATH_MSG_VERBOSE("done treat as year");


  nt_averageIntPerXing=eventInfo->averageInteractionsPerCrossing();
  nt_actualIntPerXing=eventInfo->actualInteractionsPerCrossing();

  
  double correctedMu= 0;
  if(!m_isMC)correctedMu=m_objTool->GetCorrectedAverageInteractionsPerCrossing();
  else correctedMu=nt_averageIntPerXing;
  nt_averageIntPerXingCorr=correctedMu;


  nt_nVertices = m_primVertex->size();
  nt_nVertices_PriVtr = 0;
  for ( const auto& vx : *m_primVertex) {
    if (vx->vertexType() == xAOD::VxType::PriVtx) {
      nt_nVertices_PriVtr++ ;
    }
  }


  ATH_MSG_VERBOSE("done pu");


  nt_TriggerWeight=m_TriggerWeight;	
  //nt_TriggerWeight2=m_TriggerWeight2;	
  
  
  nt_muonsTrigSF = 1.0;
  nt_muonsRecoSF = 1.0;

  nt_muonsTrigSF_TRIGSYSTUP=1.0;
  nt_muonsTrigSF_TRIGSYSTDOWN=1.0;
  nt_muonsTrigSF_TRIGSTATUP=1.0;
  nt_muonsTrigSF_TRIGSTATDOWN=1.0;
  nt_muonsTrigSF_TTVASYSTUP=1.0;
  nt_muonsTrigSF_TTVASYSTDOWN=1.0;
  nt_muonsTrigSF_TTVASTATUP=1.0;
  nt_muonsTrigSF_TTVASTATDOWN=1.0;
  nt_muonsTrigSF_ISOSYSTUP=1.0;
  nt_muonsTrigSF_ISOSYSTDOWN=1.0;
  nt_muonsTrigSF_ISOSTATUP=1.0;
  nt_muonsTrigSF_ISOSTATDOWN=1.0;
  nt_muonsTrigSF_BADMUONSYSUP=1.0;
  nt_muonsTrigSF_BADMUONSYSDOWN=1.0;
  nt_muonsTrigSF_BADMUONSTATUP=1.0;
  nt_muonsTrigSF_BADMUONSTATDOWN=1.0;
  nt_muonsTrigSF_RECOSYSUP=1.0;
  nt_muonsTrigSF_RECOSYSDOWN=1.0;
  nt_muonsTrigSF_RECOSTATUP=1.0;
  nt_muonsTrigSF_RECOSTATDOWN=1.0;
  nt_muonsTrigSF_RECOSYSLOWPTUP=1.0;
  nt_muonsTrigSF_RECOSYSLOWPTDOWN=1.0;
  nt_muonsTrigSF_RECOSTATLOWPTUP=1.0;
  nt_muonsTrigSF_RECOSTATLOWPTDOWN=1.0;

  nt_electronsRecoSF = 1.0;
  nt_electronsTrigSF = 1.0;
  nt_electronsTrigSF_IDUP = 1.0;
  nt_electronsTrigSF_IDDOWN = 1.0;
  nt_electronsTrigSF_ISOUP = 1.0;
  nt_electronsTrigSF_ISODOWN = 1.0;
  nt_electronsTrigSF_RECOUP = 1.0;
  nt_electronsTrigSF_RECODOWN = 1.0;
  nt_electronsTrigSF_TRIGUP = 1.0;
  nt_electronsTrigSF_TRIGDOWN = 1.0;
  nt_electronsTrigSF_TRIGEFFUP = 1.0;
  nt_electronsTrigSF_TRIGEFFDOWN = 1.0;
  nt_electronsTrigSF_CHARGEIDSELUP = 1.0;
  nt_electronsTrigSF_CHARGEIDSELDOWN = 1.0;

  nt_muonsSF_TTVASYSTUP=1.0;
  nt_muonsSF_TTVASYSTDOWN=1.0;
  nt_muonsSF_TTVASTATUP=1.0;
  nt_muonsSF_TTVASTATDOWN=1.0;
  nt_muonsSF_ISOSYSTUP=1.0;
  nt_muonsSF_ISOSYSTDOWN=1.0;
  nt_muonsSF_ISOSTATUP=1.0;
  nt_muonsSF_ISOSTATDOWN=1.0;
  nt_muonsSF_BADMUONSYSUP=1.0;
  nt_muonsSF_BADMUONSYSDOWN=1.0;
  nt_muonsSF_BADMUONSTATUP=1.0;
  nt_muonsSF_BADMUONSTATDOWN=1.0;
  nt_muonsSF_RECOSYSUP=1.0;
  nt_muonsSF_RECOSYSDOWN=1.0;
  nt_muonsSF_RECOSTATUP=1.0;
  nt_muonsSF_RECOSTATDOWN=1.0;
  nt_muonsSF_RECOSYSLOWPTUP=1.0;
  nt_muonsSF_RECOSYSLOWPTDOWN=1.0;
  nt_muonsSF_RECOSTATLOWPTUP=1.0;
  nt_muonsSF_RECOSTATLOWPTDOWN=1.0;
  nt_electronsSF_RECOUP=1.0;
  nt_electronsSF_RECODOWN=1.0;
  nt_electronsSF_ISOUP=1.0;
  nt_electronsSF_ISODOWN=1.0;
  nt_electronsSF_IDUP=1.0;
  nt_electronsSF_IDDOWN=1.0;
  nt_electronsSF_CHARGEIDSELUP=1.0;
  nt_electronsSF_CHARGEIDSELDOWN=1.0;


  nt_leptonsGlobalTrigSF=1.0;
  nt_leptonsGlobalTrigSF_MUONTRIGSYSUP=1.0;
  nt_leptonsGlobalTrigSF_MUONTRIGSYSDOWN=1.0;
  nt_leptonsGlobalTrigSF_MUONTRIGSTATUP=1.0;
  nt_leptonsGlobalTrigSF_MUONTRIGSTATDOWN=1.0;
  nt_leptonsGlobalTrigSF_TRIGUP = 1.0;
  nt_leptonsGlobalTrigSF_TRIGDOWN = 1.0;
  nt_leptonsGlobalTrigSF_TRIGEFFUP = 1.0;
  nt_leptonsGlobalTrigSF_TRIGEFFDOWN = 1.0;
  nt_leptonsGlobalTrigSF_IDUP = 1.0;
  nt_leptonsGlobalTrigSF_IDDOWN = 1.0;
  nt_leptonsGlobalTrigSF_ISOUP = 1.0;
  nt_leptonsGlobalTrigSF_ISODOWN = 1.0;
  nt_leptonsGlobalTrigSF_RECOUP = 1.0;
  nt_leptonsGlobalTrigSF_RECODOWN = 1.0;
  nt_leptonsGlobalTrigSF_TRIGUP = 1.0;
  nt_leptonsGlobalTrigSF_TRIGDOWN = 1.0;
  nt_leptonsGlobalTrigSF_TRIGEFFUP = 1.0;
  nt_leptonsGlobalTrigSF_TRIGEFFDOWN = 1.0;
  nt_leptonsGlobalTrigSF_CHARGEIDSELUP = 1.0;
  nt_leptonsGlobalTrigSF_CHARGEIDSELDOWN = 1.0;
  nt_leptonsGlobalTrigMuonSF_TTVASYSTUP=1.0;
  nt_leptonsGlobalTrigMuonSF_TTVASYSTDOWN=1.0;
  nt_leptonsGlobalTrigMuonSF_TTVASTATUP=1.0;
  nt_leptonsGlobalTrigMuonSF_TTVASTATDOWN=1.0;
  nt_leptonsGlobalTrigMuonSF_ISOSYSTUP=1.0;
  nt_leptonsGlobalTrigMuonSF_ISOSYSTDOWN=1.0;
  nt_leptonsGlobalTrigMuonSF_ISOSTATUP=1.0;
  nt_leptonsGlobalTrigMuonSF_ISOSTATDOWN=1.0;
  nt_leptonsGlobalTrigMuonSF_BADMUONSYSUP=1.0;
  nt_leptonsGlobalTrigMuonSF_BADMUONSYSDOWN=1.0;
  nt_leptonsGlobalTrigMuonSF_BADMUONSTATUP=1.0;
  nt_leptonsGlobalTrigMuonSF_BADMUONSTATDOWN=1.0;
  nt_leptonsGlobalTrigMuonSF_RECOSYSUP=1.0;
  nt_leptonsGlobalTrigMuonSF_RECOSYSDOWN=1.0;
  nt_leptonsGlobalTrigMuonSF_RECOSTATUP=1.0;
  nt_leptonsGlobalTrigMuonSF_RECOSTATDOWN=1.0;
  nt_leptonsGlobalTrigMuonSF_RECOSYSLOWPTUP=1.0;
  nt_leptonsGlobalTrigMuonSF_RECOSYSLOWPTDOWN=1.0;
  nt_leptonsGlobalTrigMuonSF_RECOSTATLOWPTUP=1.0;
  nt_leptonsGlobalTrigMuonSF_RECOSTATLOWPTDOWN=1.0;


  nt_photonSF=1.0;
  nt_photonSF_IDUP =1.0;
  nt_photonSF_IDDOWN =1.0;
  nt_photonSF_TRKISOUP =1.0;
  nt_photonSF_TRKISODOWN =1.0;

  nt_tauSF = 1.0;
  nt_tauSF_TRUEELECTRON_ELEOLR_TOTAL__1down = 1.0;
  nt_tauSF_TRUEELECTRON_ELEOLR_TOTAL__1up = 1.0;
  nt_tauSF_TRUEHADTAU_ELEOLR_TOTAL__1down = 1.0;
  nt_tauSF_TRUEHADTAU_ELEOLR_TOTAL__1up = 1.0;
  nt_tauSF_JETID_1PRONGSTATSYSTUNCORR2025__1down = 1.0;
  nt_tauSF_JETID_1PRONGSTATSYSTUNCORR2025__1up = 1.0;
  nt_tauSF_JETID_1PRONGSTATSYSTUNCORR2530__1down = 1.0;
  nt_tauSF_JETID_1PRONGSTATSYSTUNCORR2530__1up = 1.0;
  nt_tauSF_JETID_1PRONGSTATSYSTUNCORR3040__1down = 1.0;
  nt_tauSF_JETID_1PRONGSTATSYSTUNCORR3040__1up = 1.0;
  nt_tauSF_JETID_1PRONGSTATSYSTUNCORR40__1down = 1.0;
  nt_tauSF_JETID_1PRONGSTATSYSTUNCORR40__1up = 1.0;
  nt_tauSF_JETID_3PRONGSTATSYSTUNCORR2030__1down = 1.0;
  nt_tauSF_JETID_3PRONGSTATSYSTUNCORR2030__1up = 1.0;
  nt_tauSF_JETID_3PRONGSTATSYSTUNCORR30__1down = 1.0;
  nt_tauSF_JETID_3PRONGSTATSYSTUNCORR30__1up = 1.0;
  nt_tauSF_JETID_HIGHPT__1down = 1.0;
  nt_tauSF_JETID_HIGHPT__1up = 1.0;
  nt_tauSF_JETID_SYST__1down = 1.0;
  nt_tauSF_JETID_SYST__1up = 1.0;
  nt_tauSF_RECO_HIGHPT__1down = 1.0;
  nt_tauSF_RECO_HIGHPT__1up = 1.0;
  nt_tauSF_RECO_TOTAL__1down = 1.0;
  nt_tauSF_RECO_TOTAL__1up = 1.0;
  // Trigger systematics : Remove them unless some tau trigger is applied 
  // nt_tauSF_TRIGGER_STATDATA2015__1down = 1.0;
  // nt_tauSF_TRIGGER_STATDATA2015__1up = 1.0;
  // nt_tauSF_TRIGGER_STATDATA2016__1down = 1.0;
  // nt_tauSF_TRIGGER_STATDATA2016__1up = 1.0;
  // nt_tauSF_TRIGGER_STATDATA2017__1down = 1.0;
  // nt_tauSF_TRIGGER_STATDATA2017__1up = 1.0;
  // nt_tauSF_TRIGGER_STATMC2015__1down = 1.0;
  // nt_tauSF_TRIGGER_STATMC2015__1up = 1.0;
  // nt_tauSF_TRIGGER_STATMC2016__1down = 1.0;
  // nt_tauSF_TRIGGER_STATMC2016__1up = 1.0;
  // nt_tauSF_TRIGGER_STATMC2017__1down = 1.0;
  // nt_tauSF_TRIGGER_STATMC2017__1up = 1.0;
  // nt_tauSF_TRIGGER_SYST2015__1down = 1.0;
  // nt_tauSF_TRIGGER_SYST2015__1up = 1.0;
  // nt_tauSF_TRIGGER_SYST2016__1down = 1.0;
  // nt_tauSF_TRIGGER_SYST2016__1up = 1.0;
  // nt_tauSF_TRIGGER_SYST2017__1down = 1.0;
  // nt_tauSF_TRIGGER_SYST2017__1up = 1.0;

  nt_jetfJvtEfficiencyUP = 1.0;
  nt_jetfJvtEfficiencyDOWN = 1.0;
  nt_jetJvtEfficiencyUP = 1.0;
  nt_jetJvtEfficiencyDOWN = 1.0;

  nt_btagweightBUP = 1.0;
  nt_btagweightBDOWN = 1.0;
  nt_btagweightCUP = 1.0;
  nt_btagweightCDOWN = 1.0;
  nt_btagweightLUP = 1.0;
  nt_btagweightLDOWN = 1.0;
  nt_btagweightExUP = 1.0;
  nt_btagweightExDOWN = 1.0;
  nt_btagweightExCUP = 1.0;
  nt_btagweightExCDOWN = 1.0;


  nt_btagtrkweightBUP = 1.0;
  nt_btagtrkweightBDOWN = 1.0;
  nt_btagtrkweightCUP = 1.0;
  nt_btagtrkweightCDOWN = 1.0;
  nt_btagtrkweightLUP = 1.0;
  nt_btagtrkweightLDOWN = 1.0;
  nt_btagtrkweightExUP = 1.0;
  nt_btagtrkweightExDOWN = 1.0;
  nt_btagtrkweightExCUP = 1.0;
  nt_btagtrkweightExCDOWN = 1.0;

  nt_btagweight = 1.0;
  nt_btagtrkweight = 1.0;
  nt_jvtweight  = 1.0;


  if(m_isMC){
    ATH_MSG_VERBOSE("start SFS");
    //leptons and photons RECO SFs..
    if(m_usePhotons) nt_photonSF=m_objTool->GetTotalPhotonSF(*m_signalPhotons,true,true);
    ATH_MSG_VERBOSE("photon SFs are good");
    nt_electronsRecoSF=m_objTool->GetTotalElectronSF(*m_signalElectrons,true,true,false,true,""); // booleans: reco, ID, Trigger, Iso
    ATH_MSG_VERBOSE("elec SFs are good");
    nt_muonsRecoSF = m_objTool->GetTotalMuonSF(*m_signalMuons,true,true,""); // booleans: reco, Iso
    ATH_MSG_VERBOSE("muon SFs are good");
    //btag and jvt reco SFs...
    nt_btagweight=m_objTool->BtagSF(m_signalJets);
    ATH_MSG_VERBOSE("btag SF are good");

    if(m_useTrackJets){
      nt_btagtrkweight=m_objTool->BtagSF_trkJet(m_signalTrackJets);
      ATH_MSG_VERBOSE("btag SF are good for track jets.");
    }


    if(m_useTaus)
      nt_tauSF=m_objTool->GetTotalTauSF(*m_signalTaus,true,false); // boolean: idSF -> true , trigSF -> false 
    ATH_MSG_VERBOSE("tau SF are good");

    nt_jvtweight=m_objTool->JVT_SF(m_baselineJetsAfterOR);
    ATH_MSG_VERBOSE("jvt SF are good");

    /*
    //do trigger SF if not SUSY11
    if(xStream!="DAOD_SUSY11" ){
      nt_electronsTrigSF=m_objTool->GetTotalElectronSF(*m_signalElectrons,false,false,true,false,"singleLepton");
      ATH_MSG_VERBOSE("elec Trig SFs are good");
      if (nt_year==2015) nt_muonsTrigSF = m_objTool->GetTotalMuonSF(*m_signalMuons,false,false,"HLT_mu20_iloose_L1MU15_OR_HLT_mu50");
      else nt_muonsTrigSF = m_objTool->GetTotalMuonSF(*m_signalMuons,false,false,"HLT_mu26_ivarmedium_OR_HLT_mu50");
      ATH_MSG_VERBOSE("muon Trig SFs are good");
      nt_leptonsGlobalTrigSF=m_objTool->GetTriggerGlobalEfficiencySF(*m_signalElectrons,*m_signalMuons,"multiLepton");
      ATH_MSG_VERBOSE("muon Trig SFs are good");
    }
    */
    ATH_MSG_VERBOSE("done simple SFs");
    if(isNominal && xStream!="DAOD_SUSY11" && m_saveExperimentalSyst){
      /*
      //muons systematic SFs 
      // double GetTotalMuonSFsys(const xAOD::MuonContainer& muons, const CP::SystematicSet& systConfig, const bool recoSF = true, const bool isoSF = true, const std::string& trigExpr = "HLT_mu20_iloose_L1MU15_OR_HLT_mu50", const bool bmhptSF = true) override final;
      std::string trigExpr = (nt_year==2015) ? "HLT_mu20_iloose_L1MU15_OR_HLT_mu50" : "HLT_mu26_ivarmedium_OR_HLT_mu50";
      nt_muonsTrigSF_TRIGSYSTUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_TrigSystUncertainty__1up"),true,true,trigExpr);
      nt_muonsTrigSF_TRIGSYSTDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_TrigSystUncertainty__1down"),true,true,trigExpr);
      nt_muonsTrigSF_TRIGSTATUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_TrigStatUncertainty__1up"),true,true,trigExpr);
      nt_muonsTrigSF_TRIGSTATDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_TrigStatUncertainty__1down"),true,true,trigExpr);
      nt_muonsTrigSF_TTVASYSTUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_TTVA_SYS__1up"),true,true,trigExpr);
      nt_muonsTrigSF_TTVASYSTDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_TTVA_SYS__1down"),true,true,trigExpr);
      nt_muonsTrigSF_TTVASTATUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_TTVA_STAT__1up"),true,true,trigExpr);
      nt_muonsTrigSF_TTVASTATDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_TTVA_STAT__1down"),true,true,trigExpr);
      nt_muonsTrigSF_ISOSYSTUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_ISO_SYS__1up"),true,true,trigExpr);
      nt_muonsTrigSF_ISOSYSTDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_ISO_SYS__1down"),true,true,trigExpr);
      nt_muonsTrigSF_ISOSTATUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_ISO_STAT__1up"),true,true,trigExpr);
      nt_muonsTrigSF_ISOSTATDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_ISO_STAT__1down"),true,true,trigExpr);
      nt_muonsTrigSF_BADMUONSYSUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_BADMUON_SYS__1up"),true,true,trigExpr);
      nt_muonsTrigSF_BADMUONSYSDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_BADMUON_SYS__1down"),true,true,trigExpr);
      nt_muonsTrigSF_BADMUONSTATUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_BADMUON_STAT__1up"),true,true,trigExpr);
      nt_muonsTrigSF_BADMUONSTATDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_BADMUON_STAT__1down"),true,true,trigExpr);
      nt_muonsTrigSF_RECOSYSUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_SYS__1up"),true,true,trigExpr);
      nt_muonsTrigSF_RECOSYSDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_SYS__1down"),true,true,trigExpr);
      nt_muonsTrigSF_RECOSTATUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_STAT__1up"),true,true,trigExpr);
      nt_muonsTrigSF_RECOSTATDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_STAT__1down"),true,true,trigExpr);
      nt_muonsTrigSF_RECOSYSLOWPTUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_SYS_LOWPT__1up"),true,true,trigExpr);
      nt_muonsTrigSF_RECOSYSLOWPTDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_SYS_LOWPT__1down"),true,true,trigExpr);
      nt_muonsTrigSF_RECOSTATLOWPTUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_STAT_LOWPT__1up"),true,true,trigExpr);
      nt_muonsTrigSF_RECOSTATLOWPTDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_STAT_LOWPT__1down"),true,true,trigExpr);

      nt_muonsSF_TTVASYSTUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_TTVA_SYS__1up"),true,true,"");
      nt_muonsSF_TTVASYSTDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_TTVA_SYS__1down"),true,true,"");
      nt_muonsSF_TTVASTATUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_TTVA_STAT__1up"),true,true,"");
      nt_muonsSF_TTVASTATDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_TTVA_STAT__1down"),true,true,"");
      nt_muonsSF_ISOSYSTUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_ISO_SYS__1up"),true,true,"");
      nt_muonsSF_ISOSYSTDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_ISO_SYS__1down"),true,true,"");
      nt_muonsSF_ISOSTATUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_ISO_STAT__1up"),true,true,"");
      nt_muonsSF_ISOSTATDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_ISO_STAT__1down"),true,true,"");
      nt_muonsSF_BADMUONSYSUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_BADMUON_SYS__1up"),true,true,"");
      nt_muonsSF_BADMUONSYSDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_BADMUON_SYS__1down"),true,true,"");
      nt_muonsSF_BADMUONSTATUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_BADMUON_STAT__1up"),true,true,"");
      nt_muonsSF_BADMUONSTATDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_BADMUON_STAT__1down"),true,true,"");
      nt_muonsSF_RECOSYSUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_SYS__1up"),true,true,"");
      nt_muonsSF_RECOSYSDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_SYS__1down"),true,true,"");
      nt_muonsSF_RECOSTATUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_STAT__1up"),true,true,"");
      nt_muonsSF_RECOSTATDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_STAT__1down"),true,true,"");
      nt_muonsSF_RECOSYSLOWPTUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_SYS_LOWPT__1up"),true,true,"");
      nt_muonsSF_RECOSYSLOWPTDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_SYS_LOWPT__1down"),true,true,"");
      nt_muonsSF_RECOSTATLOWPTUP=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_STAT_LOWPT__1up"),true,true,"");
      nt_muonsSF_RECOSTATLOWPTDOWN=m_objTool->GetTotalMuonSFsys(*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_STAT_LOWPT__1down"),true,true,"");
      
      //electron systematics SFs
      // GetTotalElectronSFsys(const xAOD::ElectronContainer& electrons, const CP::SystematicSet& systConfig, const bool recoSF = true, const bool idSF = true, const bool triggerSF = true, const bool isoSF = true, const std::string& trigExpr = "singleLepton", const bool chfSF = false) // singleLepton == Ele.TriggerSFStringSingle value
      nt_electronsTrigSF_TRIGUP=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up"));
      nt_electronsTrigSF_TRIGDOWN=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down"));
      nt_electronsTrigSF_TRIGEFFUP=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up"));
      nt_electronsTrigSF_TRIGEFFDOWN=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down"));
      nt_electronsTrigSF_RECOUP=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up"));
      nt_electronsTrigSF_RECODOWN=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down"));
      nt_electronsTrigSF_ISOUP=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up"));
      nt_electronsTrigSF_ISODOWN=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down"));
      nt_electronsTrigSF_IDUP=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up"));
      nt_electronsTrigSF_IDDOWN=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down"));  
      nt_electronsTrigSF_CHARGEIDSELUP=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up"));
      nt_electronsTrigSF_CHARGEIDSELDOWN=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down"));

      nt_electronsSF_RECOUP=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up"),true,true,false,true);
      nt_electronsSF_RECODOWN=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down"),true,true,false,true);
      nt_electronsSF_ISOUP=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up"),true,true,false,true);
      nt_electronsSF_ISODOWN=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down"),true,true,false,true);
      nt_electronsSF_IDUP=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up"),true,true,false,true);
      nt_electronsSF_IDDOWN=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down"),true,true,false,true);
      nt_electronsSF_CHARGEIDSELUP=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up"),true,true,false,true);
      nt_electronsSF_CHARGEIDSELDOWN=m_objTool->GetTotalElectronSFsys(*m_signalElectrons,CP::SystematicSet("EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down"),true,true,false,true);

      nt_leptonsGlobalTrigSF_TRIGUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up"));
      nt_leptonsGlobalTrigSF_TRIGDOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down"));
      nt_leptonsGlobalTrigSF_TRIGEFFUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up"));
      nt_leptonsGlobalTrigSF_TRIGEFFDOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down"));
      nt_leptonsGlobalTrigSF_RECOUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up"));
      nt_leptonsGlobalTrigSF_RECODOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down"));
      nt_leptonsGlobalTrigSF_ISOUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up"));
      nt_leptonsGlobalTrigSF_ISODOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down"));
      nt_leptonsGlobalTrigSF_IDUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up"));
      nt_leptonsGlobalTrigSF_IDDOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down"));  
      nt_leptonsGlobalTrigSF_CHARGEIDSELUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up"));
      nt_leptonsGlobalTrigSF_CHARGEIDSELDOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down"));
      nt_leptonsGlobalTrigSF_MUONTRIGSYSUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_TrigSystUncertainty__1up"));
      nt_leptonsGlobalTrigSF_MUONTRIGSYSDOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_TrigSystUncertainty__1down"));
      nt_leptonsGlobalTrigSF_MUONTRIGSTATUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_TrigStatUncertainty__1up"));
      nt_leptonsGlobalTrigSF_MUONTRIGSTATDOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_TrigStatUncertainty__1down"));
      nt_leptonsGlobalTrigMuonSF_TTVASYSTUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_TTVA_SYS__1up"));
      nt_leptonsGlobalTrigMuonSF_TTVASYSTDOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_TTVA_SYS__1down"));
      nt_leptonsGlobalTrigMuonSF_TTVASTATUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_TTVA_STAT__1up"));
      nt_leptonsGlobalTrigMuonSF_TTVASTATDOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_TTVA_STAT__1down"));
      nt_leptonsGlobalTrigMuonSF_ISOSYSTUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_ISO_SYS__1up"));
      nt_leptonsGlobalTrigMuonSF_ISOSYSTDOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_ISO_SYS__1down"));
      nt_leptonsGlobalTrigMuonSF_ISOSTATUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_ISO_STAT__1up"));
      nt_leptonsGlobalTrigMuonSF_ISOSTATDOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_ISO_STAT__1down"));
      nt_leptonsGlobalTrigMuonSF_BADMUONSYSUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_BADMUON_SYS__1up"));
      nt_leptonsGlobalTrigMuonSF_BADMUONSYSDOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_BADMUON_SYS__1down"));
      nt_leptonsGlobalTrigMuonSF_BADMUONSTATUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_BADMUON_STAT__1up"));
      nt_leptonsGlobalTrigMuonSF_BADMUONSTATDOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_BADMUON_STAT__1down"));
      nt_leptonsGlobalTrigMuonSF_RECOSYSUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_SYS__1up"));
      nt_leptonsGlobalTrigMuonSF_RECOSYSDOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_SYS__1down"));
      nt_leptonsGlobalTrigMuonSF_RECOSTATUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_STAT__1up"));
      nt_leptonsGlobalTrigMuonSF_RECOSTATDOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_STAT__1down"));
      nt_leptonsGlobalTrigMuonSF_RECOSYSLOWPTUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_SYS_LOWPT__1up"));
      nt_leptonsGlobalTrigMuonSF_RECOSYSLOWPTDOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_SYS_LOWPT__1down"));
      nt_leptonsGlobalTrigMuonSF_RECOSTATLOWPTUP=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_STAT_LOWPT__1up"));
      nt_leptonsGlobalTrigMuonSF_RECOSTATLOWPTDOWN=m_objTool->GetTriggerGlobalEfficiencySFsys(*m_signalElectrons,*m_signalMuons,CP::SystematicSet("MUON_EFF_RECO_STAT_LOWPT__1down"));

      //photon systematics SFs
      if(m_usePhotons){
        nt_photonSF_IDUP = m_objTool->GetTotalPhotonSFsys(*m_signalPhotons,CP::SystematicSet("PH_EFF_ID_Uncertainty__1up"));
        nt_photonSF_IDDOWN = m_objTool->GetTotalPhotonSFsys(*m_signalPhotons,CP::SystematicSet("PH_EFF_ID_Uncertainty__1down"));
        nt_photonSF_TRKISOUP = m_objTool->GetTotalPhotonSFsys(*m_signalPhotons,CP::SystematicSet("PH_EFF_TRKISO_Uncertainty__1up"));
        nt_photonSF_TRKISODOWN = m_objTool->GetTotalPhotonSFsys(*m_signalPhotons,CP::SystematicSet("PH_EFF_TRKISO_Uncertainty__1down"));
      }

      // tau systematics 
      if( m_useTaus ){
        nt_tauSF_TRUEELECTRON_ELEOLR_TOTAL__1down = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL__1down"),true,false);
        nt_tauSF_TRUEELECTRON_ELEOLR_TOTAL__1up = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL__1up"),true,false);
	nt_tauSF_TRUEHADTAU_ELEOLR_TOTAL__1down = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down"),true,false);
	nt_tauSF_TRUEHADTAU_ELEOLR_TOTAL__1up = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up"),true,false);
	nt_tauSF_JETID_1PRONGSTATSYSTUNCORR2025__1down = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1down"),true,false);
	nt_tauSF_JETID_1PRONGSTATSYSTUNCORR2025__1up = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1up"),true,false);
	nt_tauSF_JETID_1PRONGSTATSYSTUNCORR2530__1down = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1down"),true,false);
	nt_tauSF_JETID_1PRONGSTATSYSTUNCORR2530__1up = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1up"),true,false);
	nt_tauSF_JETID_1PRONGSTATSYSTUNCORR3040__1down = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1down"),true,false);
	nt_tauSF_JETID_1PRONGSTATSYSTUNCORR3040__1up = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1up"),true,false);
	nt_tauSF_JETID_1PRONGSTATSYSTUNCORR40__1down = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1down"),true,false);
	nt_tauSF_JETID_1PRONGSTATSYSTUNCORR40__1up = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1up"),true,false);
	nt_tauSF_JETID_3PRONGSTATSYSTUNCORR2030__1down = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1down"),true,false);
	nt_tauSF_JETID_3PRONGSTATSYSTUNCORR2030__1up = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1up"),true,false);
	nt_tauSF_JETID_3PRONGSTATSYSTUNCORR30__1down = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1down"),true,false);
	nt_tauSF_JETID_3PRONGSTATSYSTUNCORR30__1up = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1up"),true,false);
	nt_tauSF_JETID_HIGHPT__1down = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down"),true,false);
	nt_tauSF_JETID_HIGHPT__1up = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up"),true,false);
	nt_tauSF_JETID_SYST__1down = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_JETID_SYST__1down"),true,false);
	nt_tauSF_JETID_SYST__1up = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_JETID_SYST__1up"),true,false);
	nt_tauSF_RECO_HIGHPT__1down = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down"),true,false);
	nt_tauSF_RECO_HIGHPT__1up = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up"),true,false);
	nt_tauSF_RECO_TOTAL__1down = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down"),true,false);
	nt_tauSF_RECO_TOTAL__1up = m_objTool->GetTotalTauSFsys(*m_signalTaus,CP::SystematicSet("TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up"),true,false);
      }
    */
      //jet systematics SFs
      nt_jetfJvtEfficiencyUP = m_objTool->GetTotalJetSFsys(m_baselineJetsAfterOR,CP::SystematicSet("JET_fJvtEfficiency__1up"));
      nt_jetfJvtEfficiencyDOWN = m_objTool->GetTotalJetSFsys(m_baselineJetsAfterOR,CP::SystematicSet("JET_fJvtEfficiency__1down"));
      nt_jetJvtEfficiencyUP = m_objTool->GetTotalJetSFsys(m_baselineJetsAfterOR,CP::SystematicSet("JET_JvtEfficiency__1up"));
      nt_jetJvtEfficiencyDOWN = m_objTool->GetTotalJetSFsys(m_baselineJetsAfterOR,CP::SystematicSet("JET_JvtEfficiency__1down"));

      // jet flavour tag systematics SFs
      if(m_isEnvelope){
	nt_btagweightBUP =m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_B_systematics__1up")); 
	nt_btagweightBDOWN =m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_B_systematics__1down"));
	nt_btagweightCUP =m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_C_systematics__1up"));
	nt_btagweightCDOWN =m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_C_systematics__1down"));
	nt_btagweightLUP =m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Light_systematics__1up"));
	nt_btagweightLDOWN =m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Light_systematics__1down"));
	nt_btagweightExUP =m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_extrapolation__1up"));
	nt_btagweightExDOWN =m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_extrapolation__1down"));
	nt_btagweightExCUP =m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_extrapolation_from_charm__1up"));
	nt_btagweightExCDOWN =m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_extrapolation_from_charm__1down"));
      }else{
	nt_btagweight_B0_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_0__1down"));
	nt_btagweight_B0_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_0__1up"));
	nt_btagweight_B1_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_1__1down"));
	nt_btagweight_B1_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_1__1up"));
	nt_btagweight_B2_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_2__1down"));
	nt_btagweight_B2_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_2__1up"));
	nt_btagweight_B3_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_3__1down"));
	nt_btagweight_B3_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_3__1up"));
	nt_btagweight_B4_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_4__1down"));
	nt_btagweight_B4_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_4__1up"));
	nt_btagweight_B5_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_5__1down"));
	nt_btagweight_B5_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_5__1up"));
	nt_btagweight_B6_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_6__1down"));
	nt_btagweight_B6_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_6__1up"));
	nt_btagweight_B7_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_7__1down"));
	nt_btagweight_B7_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_7__1up"));
	nt_btagweight_B8_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_8__1down"));
	nt_btagweight_B8_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_B_8__1up"));
	nt_btagweight_C0_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_C_0__1down"));
	nt_btagweight_C0_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_C_0__1up"));
	nt_btagweight_C1_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_C_1__1down"));
	nt_btagweight_C1_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_C_1__1up"));
	nt_btagweight_C2_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_C_2__1down"));
	nt_btagweight_C2_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_C_2__1up"));
	nt_btagweight_C3_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_C_3__1down"));
	nt_btagweight_C3_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_C_3__1up"));
	nt_btagweight_L0_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_Light_0__1down"));
	nt_btagweight_L0_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_Light_0__1up"));
	nt_btagweight_L1_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_Light_1__1down"));
	nt_btagweight_L1_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_Light_1__1up"));
	nt_btagweight_L2_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_Light_2__1down"));
	nt_btagweight_L2_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_Light_2__1up"));
	nt_btagweight_L3_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_Light_3__1down"));
	nt_btagweight_L3_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_Light_3__1up"));
	nt_btagweight_L4_DOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_Light_4__1down"));
	nt_btagweight_L4_UP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_Eigen_Light_4__1up"));
	nt_btagweightExCDOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_extrapolation__1down"));
	nt_btagweightExCUP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_extrapolation__1up"));
	nt_btagweightExCDOWN=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_extrapolation_from_charm__1down"));
	nt_btagweightExCUP=m_objTool->BtagSFsys(m_signalJets,CP::SystematicSet("FT_EFF_extrapolation_from_charm__1up"));
      }

      // track jet systematics SFs
      if(m_useTrackJets){
        nt_btagtrkweightBUP =m_objTool->BtagSFsys_trkJet(m_signalTrackJets,CP::SystematicSet("FT_EFF_B_systematics__1up")); 
	nt_btagtrkweightBDOWN =m_objTool->BtagSFsys_trkJet(m_signalTrackJets,CP::SystematicSet("FT_EFF_B_systematics__1down"));
	nt_btagtrkweightCUP =m_objTool->BtagSFsys_trkJet(m_signalTrackJets,CP::SystematicSet("FT_EFF_C_systematics__1up"));
	nt_btagtrkweightCDOWN =m_objTool->BtagSFsys_trkJet(m_signalTrackJets,CP::SystematicSet("FT_EFF_C_systematics__1down"));
	nt_btagtrkweightLUP =m_objTool->BtagSFsys_trkJet(m_signalTrackJets,CP::SystematicSet("FT_EFF_Light_systematics__1up"));
	nt_btagtrkweightLDOWN =m_objTool->BtagSFsys_trkJet(m_signalTrackJets,CP::SystematicSet("FT_EFF_Light_systematics__1down"));
	nt_btagtrkweightExUP =m_objTool->BtagSFsys_trkJet(m_signalTrackJets,CP::SystematicSet("FT_EFF_extrapolation__1up"));
	nt_btagtrkweightExDOWN =m_objTool->BtagSFsys_trkJet(m_signalTrackJets,CP::SystematicSet("FT_EFF_extrapolation__1down"));
	nt_btagtrkweightExCUP =m_objTool->BtagSFsys_trkJet(m_signalTrackJets,CP::SystematicSet("FT_EFF_extrapolation_from_charm__1up"));
	nt_btagtrkweightExCDOWN =m_objTool->BtagSFsys_trkJet(m_signalTrackJets,CP::SystematicSet("FT_EFF_extrapolation_from_charm__1down"));
      }      
    }
  }

  nt_TriggerSF=nt_muonsTrigSF*nt_electronsTrigSF ;
  ATH_MSG_VERBOSE("done sf variations");


  nt_passtauveto = true;
  nt_jet_btagweight.clear();
  nt_jet_MV2c10.clear();
  nt_jet_DL1.clear();
  nt_isMV2c10H85.clear();
  nt_isMV2c10H77.clear();
  nt_isMV2c10H70.clear();
  nt_isDL1H85.clear();
  nt_isDL1H77.clear();
  nt_isDL1H70.clear();
  nt_jet_chf.clear();
  nt_jet_jvtxf.clear();
  nt_jet_BCH_CORR_CELL.clear();
  nt_jet_emfrac.clear();

  nt_jet_isSimpleTau.clear();
  nt_jet_passTightClean.clear();
  nt_jet_passTightCleanDFFlag.clear();

 
  nt_jet_ntracks.clear();
  nt_jet_truthflavhadexcl.clear();
  nt_jet_truthflavhadcone.clear();
  nt_jet_PullMag.clear();
  nt_jet_PullPhi.clear();
  
	    
  nt_jet_DL1_pu.clear();
  nt_jet_DL1_pc.clear();
  nt_jet_DL1_pb.clear();

  nt_jet_DL1r_pu.clear();
  nt_jet_DL1r_pc.clear();
  nt_jet_DL1r_pb.clear();
  nt_jet_DL1r_cscore.clear();



  for(const auto &jet: *m_signalJets){
    ATH_MSG_VERBOSE("start jet pt=" << jet->pt() << "  size=" << m_signalJets->size());
    
    double MV2=-999.;
    xAOD::BTaggingUtilities::getBTagging(*jet)->MVx_discriminant("MV2c10", MV2);
    nt_jet_MV2c10.push_back( MV2 );
    double btagweight=jet->auxdata<double>("btag_weight");
    nt_jet_btagweight.push_back(btagweight);

    double pu,pc,pb;
    xAOD::BTaggingUtilities::getBTagging(*jet)->pu("DL1",pu);
    xAOD::BTaggingUtilities::getBTagging(*jet)->pc("DL1",pc);
    xAOD::BTaggingUtilities::getBTagging(*jet)->pb("DL1",pb);
	    
    nt_jet_DL1_pu.push_back(pu);
    nt_jet_DL1_pc.push_back(pc);
    nt_jet_DL1_pb.push_back(pb);

    xAOD::BTaggingUtilities::getBTagging(*jet)->pu("DL1r",pu);
    xAOD::BTaggingUtilities::getBTagging(*jet)->pc("DL1r",pc);
    xAOD::BTaggingUtilities::getBTagging(*jet)->pb("DL1r",pb);
    
    nt_jet_DL1r_pu.push_back(pu);
    nt_jet_DL1r_pc.push_back(pc);
    nt_jet_DL1r_pb.push_back(pb);

    nt_jet_DL1r_cscore.push_back(jet->auxdata<double>("CharmScore"));

    int isSimpleTau=0;
    if (!jet->auxdata<char>("bjet") && fabs(jet->eta())<2.5){
      //TAU VETO CODE CURRENTLY!
      std::vector<int> ntrk;
      jet->getAttribute(xAOD::JetAttribute::NumTrkPt500, ntrk);
      if(ntrk.size()>0){
        if (ntrk[0]<=4){
          isSimpleTau=1;
    	    TLorentzVector EtMissVec;	
    	    EtMissVec.SetPxPyPzE(m_pxmiss,m_pymiss,0,m_etmiss);
    	    double dphi = TVector2::Phi_mpi_pi(EtMissVec.Phi() - jet->phi());
          if (fabs(dphi)<(TMath::Pi()/5.0)) nt_passtauveto = false;
        }
      }
    }
    nt_jet_isSimpleTau.push_back(isSimpleTau);


    int passTight = 1;
    std::vector<float> SumTrkPt500vec;
    SumTrkPt500vec = jet->auxdata<std::vector<float>>("SumPtTrkPt500");
    float SumTrkPt500 = -1;
    if (SumTrkPt500vec.size()>0) SumTrkPt500 = SumTrkPt500vec.at(0);
    float chf = SumTrkPt500/jet->pt();
    float fmax = jet->auxdata<float>("FracSamplingMax");
    if (std::fabs(jet->eta())<2.4 && chf/fmax<0.1) passTight = 0;
    nt_jet_passTightClean.push_back(passTight);

    int passTightDFFlag=jet->auxdata<char>("DFCommonJets_jetClean_TightBad");
    nt_jet_passTightCleanDFFlag.push_back(passTightDFFlag);


    //This gives us information about whether a jet is matched to a truth b-hadron - "+5" tells us it is.
    int flavhadcone = -999;
    jet->getAttribute("HadronConeExclTruthLabelID",flavhadcone);
    nt_jet_truthflavhadcone.push_back(flavhadcone);

    //This gives us information about whether a jet is matched to a truth b-hadron - "+5" tells us it is.
    int flavhadexcl = -999;
    jet->getAttribute("HadronConeExclExtendedTruthLabelID",flavhadexcl);
    nt_jet_truthflavhadexcl.push_back(flavhadexcl);
    


    // std::vector<float> jvf;
    // double bchCorrCell = 0;
    // double emfrac = 0;
    // double fmax = 0;


    // if(m_saveJetCleaningVars){
    //   jet->getAttribute("JVF",jvf);
    //   jet->getAttribute("bchCorrCell",bchCorrCell);
    //   jet->getAttribute("EMFrac",emfrac);   
    //   jet->getAttribute("FracSamplingMax",fmax);

    
    //   std::vector<float> sumPtTrkvec;
    //   jet->getAttribute( xAOD::JetAttribute::SumPtTrkPt500, sumPtTrkvec );
    //   float sumpttrk = 0;
    //   if( ! sumPtTrkvec.empty() ) sumpttrk = sumPtTrkvec[0];
    //   float pt = jet->pt();
    //   float chf = sumpttrk/pt;
    //   nt_jet_chf.push_back(chf);

    //   if(jvf.size()>0)
    // 	nt_jet_jvtxf.push_back( jvf[0] );
    //   else
    // 	nt_jet_jvtxf.push_back( 0 );
	
    //   nt_jet_BCH_CORR_CELL.push_back( bchCorrCell );
    //   nt_jet_emfrac.push_back( emfrac );
    // }

    //This gives us information about whether a jet is matched to a truth b-hadron - "+5" tells us it is.

    if ( m_isMC ){
      int flavhadexcl = -999;
      jet->getAttribute("HadronConeExclExtendedTruthLabelID",flavhadexcl);
      nt_jet_truthflavhadexcl.push_back(flavhadexcl);
    }

    std::vector<const xAOD::IParticle*> jtracks;
    jet->getAssociatedObjects<xAOD::IParticle>( xAOD::JetAttribute::GhostTrack,jtracks);
    nt_jet_ntracks.push_back(jtracks.size());



  
    if(m_useJetConstituents){

      TVector2 r_pull(0,0);
      double jeta = jet->rapidity();
      double ref_pt = jet->pt();
    
      for(size_t iConst=0; iConst<jtracks.size(); ++iConst) {
	const xAOD::TrackParticle* pTrk = static_cast<const xAOD::TrackParticle*>(jtracks[iConst]);
	if(!pTrk)
	  continue;
      

	double dphi = TVector2::Phi_mpi_pi(jet->phi() - pTrk->phi() ); 
	double ceta = pTrk->rapidity();
      
	TVector2 r_i(ceta-jeta, dphi);


	r_i *= (pTrk->pt() * r_i.Mod())/ref_pt;
	r_pull += r_i;
      }

    
      nt_jet_PullMag.push_back(r_pull.X());
      nt_jet_PullPhi.push_back(r_pull.Y());
    
    
    
    }
  }

  
  ATH_MSG_VERBOSE("done jets");
  nt_el_SF.clear();
  for (const auto& electron : *m_signalElectrons) {
    bool ismatched=electron->auxdata< bool > ("trig_matched");
    double sf = 1.0;
    if(m_isMC)
      m_objTool->GetSignalElecSF(*electron , true, true, ismatched);
    nt_el_SF.push_back(sf );
  }
  ATH_MSG_VERBOSE("done electrons");

  /*
  double elRECO_SF = 1.0;
  double elTRIG_SF = 1.0;
  
  if(m_isMC){
    elRECO_SF=m_objTool->GetTotalElectronSF(*m_signalElectrons,true,true,false,"");//,true,true,false);
    //SILVIA temporary fix for ele trigger matching
    elTRIG_SF=1;//m_objTool->GetTotalElectronSF(*m_signalElectrons,false,false,true,false,"singleLepton");
  }
  nt_electronsTrigSF = elTRIG_SF;
  nt_electronsRecoSF = elRECO_SF;
  nt_isElTrigMatched = m_isElTrigMatched;

  nt_mu_SF.clear();
  for (const auto& muon : *m_signalMuons) {
    //bool ismatched=muon->auxdata< bool > ("trig_matched");
    double sf = 1.0;
    if(m_isMC)
      CHECK(m_objTool->GetSignalMuonSF(*muon , true, true));
    nt_mu_SF.push_back(  sf );
  }
  double muRECO_SF = 1.0;
  double muTRIG_SF = 1.0;
  if(m_isMC){
    muRECO_SF=m_objTool->GetTotalMuonSF(*m_signalMuons,true,true,"");

    if(nt_year==2015)
      muTRIG_SF=m_objTool->GetTotalMuonSF(*m_signalMuons,false,false,"HLT_mu20_iloose_L1MU15_OR_HLT_mu50");
    else if(nt_year==2016){
      muTRIG_SF=m_objTool->GetTotalMuonSF(*m_signalMuons,false,false,"HLT_mu26_ivarmedium_OR_HLT_mu50");
    }
  }
  nt_muonsTrigSF = muTRIG_SF;
  nt_muonsRecoSF = muRECO_SF;
  */

  
  nt_ph_topoetcone20.clear();
  nt_ph_topoetcone40.clear();
  nt_ph_ptcone20.clear();
  nt_ph_ptvarcone20.clear();

  
  for (const auto &photon: *m_signalPhotons) {
    float topoetcone20= photon->auxdata< float >("topoetcone20"); 
    float topoetcone40= photon->auxdata< float >("topoetcone40"); 
    float ptcone20= photon->auxdata< float >("ptcone20"); 
    float ptvarcone20= -999;
    photon->isolationValue(ptvarcone20,xAOD::Iso::ptvarcone20);
    nt_ph_topoetcone20.push_back(topoetcone20);
    nt_ph_topoetcone40.push_back(topoetcone40);
    nt_ph_ptcone20.push_back(ptcone20);
    nt_ph_ptvarcone20.push_back(ptvarcone20);
  }

  ATH_MSG_VERBOSE("done photons");

  TVector2 met_jet;
  met_jet.Set(m_pxmiss_jet,m_pymiss_jet);
  nt_MET_jet_pt=met_jet.Mod();	
  nt_MET_jet_phi=met_jet.Phi();
 
  TVector2 met_mu;
  met_mu.Set(m_pxmiss_mu,m_pymiss_mu);
  nt_MET_mu_pt=met_mu.Mod();
  nt_MET_mu_phi=met_mu.Phi();

  TVector2 met_el;
  met_el.Set(m_pxmiss_el,m_pymiss_el);
  nt_MET_el_pt=met_el.Mod();
  nt_MET_el_phi=met_el.Phi();
  
  if(m_useTaus){
    TVector2 met_tau;
    met_tau.Set(m_pxmiss_tau,m_pymiss_tau);
    nt_MET_tau_pt=met_tau.Mod();
    nt_MET_tau_phi=met_tau.Phi();
    nt_sumet_tau=m_sumet_tau;
  }
  
  TVector2 met_y;
  met_y.Set(m_pxmiss_y,m_pymiss_y);
  nt_MET_y_pt=met_y.Mod();
  nt_MET_y_phi=met_y.Phi();
  

  TVector2 met_softTrk;
  met_softTrk.Set(m_pxmiss_softTrk,m_pymiss_softTrk);
  nt_MET_softTrk_pt=met_softTrk.Mod();
  nt_MET_softTrk_phi=met_softTrk.Phi();


  TVector2 met_NonInt;
  met_NonInt.Set(m_pxmiss_NonInt,m_pymiss_NonInt);
  nt_MET_NonInt_pt=met_NonInt.Mod();
  nt_MET_NonInt_phi=met_NonInt.Phi();
  nt_sumet_NonInt =  m_sumet_NonInt;

  TVector2 trackmet;
  trackmet.Set(m_trackMET_px,m_trackMET_py);
  nt_trackMET_pt =  trackmet.Mod();
  nt_trackMET_phi = trackmet.Phi();

  nt_cellMET=m_cellMET;
  nt_mhtMET=m_mhtMET;

  nt_sumet_jet =  m_sumet_jet;   
  nt_sumet_el =  m_sumet_el;  
  nt_sumet_y =  m_sumet_y;   
  nt_sumet_mu =  m_sumet_mu;   
  nt_sumet_softTrk =  m_sumet_softTrk;   
  nt_sumet_softClu =  m_sumet_softClu;   

  ATH_MSG_VERBOSE("done met");
  //TRIGGERS!
  nt_IsMETTrigPassed = m_objTool->IsMETTrigPassed(0,true);
  
  for (unsigned int ii=0; ii<m_triggerNtup.size(); ii++) {
    nt_trigPass[ii]=( m_objTool->IsTrigPassed(m_triggerList.at(ii)));
    nt_trigMatch[ii]= m_trigMatch[m_triggerList.at(ii)];
  }
  
  ATH_MSG_VERBOSE("filled extra reco variables");
  return true;
}


bool  MasterShef::fillLepJetVariables(bool init, TTree*tree){

  if ( !m_doLepJet ){
    ATH_MSG_VERBOSE("Option doLepJet is not set to true. Skip lep jet variables.");
    return true;
  }

  if(init){
    tree->Branch("MET_LepJet_pt",&nt_MET_LepJet_pt);
    tree->Branch("MET_LepJet_phi",&nt_MET_LepJet_phi);
    tree->Branch("metsigLepJetST",&nt_metsigLepJetST);
    return true;
  }

  TVector2 met_lepjet;
  met_lepjet.Set(m_px_lepjet_miss,m_py_lepjet_miss);
  nt_MET_LepJet_pt=met_lepjet.Mod();
  nt_MET_LepJet_phi=met_lepjet.Phi();
  nt_metsigLepJetST=m_metsigLepJetST;
  
  ATH_MSG_VERBOSE("filled lep jet variables");
  return true;
}

bool  MasterShef::fillHFClassVariables(bool init, TTree*tree){

  ATH_MSG_VERBOSE("fill HF classification");
 
  if(init){
   
    // HeavyFlavorFilterFlag
    tree->Branch("TopHeavyFlavorFilterFlag",&nt_TopHeavyFlavorFilterFlag);

    //ttbar+HF classification
    tree->Branch("ttbar_class",&nt_ttbar_class);
    tree->Branch("ttbar_class_ext",&nt_ttbar_class_ext);
    tree->Branch("ttbar_class_prompt",&nt_ttbar_class_prompt);
      
    //tree->Branch("jet_wpartons",&nt_jet_wpartons);

    return true;
  }

  ATH_MSG_VERBOSE("Fill truth HF classification information");

  //skip if this is data !
  if(!m_isMC) return true;


  // ttbar+HF classification
  if (m_doTtbarHFClassification){
    HFSystDataMembers *dm_HFsyst = new HFSystDataMembers();
    ClassifyAndCalculateHF classHF(event,"TruthParticles","AntiKt4TruthJets",true);
    classHF.apply(dm_HFsyst, m_ttbarHF, m_ttbarHF_ext, m_ttbarHF_prompt);
  }


  
  static SG::AuxElement::ConstAccessor<int> TopHeavyFlavorFilterFlag_d("TopHeavyFlavorFilterFlag");
  if(!TopHeavyFlavorFilterFlag_d.isAvailable(*eventInfo)) Warning( "fillTruth()", "Cannot retrieve TopHeavyFlavorFilterFlag !" );
  else nt_TopHeavyFlavorFilterFlag = eventInfo->auxdata<int>("TopHeavyFlavorFilterFlag");
  
  // ttbar+HF variables
  nt_ttbar_class = m_ttbarHF;
  nt_ttbar_class_ext = m_ttbarHF_ext;
  nt_ttbar_class_prompt = m_ttbarHF_prompt;


  return true;
}









bool  MasterShef::fillExtraTruthVariables(bool init, TTree* tree){

  ATH_MSG_VERBOSE("fiill extra truth variables");
 
  bool isNominal = true;
  if( std::string(tree->GetName()) != "Nominal" ){
    isNominal=false;
  }
 
  if(init){

    tree->Branch("GenFiltMET",&nt_GenFiltMET);
    tree->Branch("GenFiltHT",&nt_GenFiltHT);

    tree->Branch("NTruthChargedLeptonsFromLQ",&nt_TruthChLeptsFromLQ);    

    if(isNominal){
      tree->Branch("hasMEphoton",&nt_hasMEphoton);
      tree->Branch("hasMEphoton80",&nt_hasMEphoton80);
      tree->Branch("hasMEphotonMCTC",&nt_hasMEphotonMCTC);
      
      //truth vector boson
      tree->Branch("TruthBosonPt",&nt_VbosonPt);
      tree->Branch("TruthBosonEta",&nt_VbosonEta);
      tree->Branch("TruthBosonPhi",&nt_VbosonPhi);
      tree->Branch("TruthBosonE",&nt_VbosonE);
      //tree->Branch("TruthBosonPDGID",&nt_VbosonPDGID);
      
      //other truth
      //tree->Branch("nuMET",&nt_nuMET);     
      tree->Branch("TruthNonInt_pt",&nt_TruthNonInt_pt);
      tree->Branch("TruthNonInt_phi",&nt_TruthNonInt_phi);
      
      
      tree->Branch("tcMeTCategory",&nt_tcMeTcategory);
      tree->Branch("topDecayType",&nt_topDecay);
      tree->Branch("topISRType",&nt_topISR);
      
      //tree->Branch("jet_type",&nt_jet_type);
      //tree->Branch("jet_npartons",&nt_jet_npartons);
      //tree->Branch("jet_wpartons",&nt_jet_wpartons);
    }
    return true;
  }

  ATH_MSG_VERBOSE("Fill extra truth information");

  //skip if this is data !
  if(!m_isMC) return true;

  
  xAOD::TEvent* event = wk()->xaodEvent();
  // get truth vector boson for V+jets
  m_VbosonPt=m_VbosonEta=m_VbosonPhi=m_VbosonE=0;
  m_nuMET=0;
  if(!m_isMC)
    return true;


  // get some truth info for V bosons
  TLorentzVector V;
  TLorentzVector l1;
  TLorentzVector l2;


  const xAOD::EventInfo* eventInfo = 0;
  CHECK(event->retrieve( eventInfo, "EventInfo"));

  const xAOD::IParticle *par = nullptr;
  nt_hasMEphoton = false;
  nt_hasMEphoton80 = false;

  nt_hasMEphotonMCTC = false;

  int pdgId = 22; // look at photons
  
  
      
  for (const auto& particle: *m_truthParticles) {
    if ((eventInfo->mcChannelNumber()==410000 ||
	 eventInfo->mcChannelNumber()==407012 || 
	 (eventInfo->mcChannelNumber()>=410082 && eventInfo->mcChannelNumber()<=410089) || eventInfo->mcChannelNumber()==407320 || eventInfo->mcChannelNumber()==407322) &&
	fabs(particle->pdgId()) == pdgId  && (particle->nParents()==0  || fabs(particle->parent(0)->pdgId()) != pdgId)) 
      {// this particle is a photon
	par = getLastTruthParticle(particle);
	int motherPdgId = -1;
	if ( particle->nParents() > 0) motherPdgId = particle->parent(0)->pdgId();
	
	if(abs(motherPdgId)<100 && particle->barcode() <2e5 && par->p4().Pt()>80e3)
	  nt_hasMEphoton80 = true;
	if(abs(motherPdgId)<100 && particle->barcode() <2e5 && par->p4().Pt()>140e3)
	  nt_hasMEphoton = true;
      }
    
  }  



  for (const auto& particle: *m_truthParticles) {
    if ((eventInfo->mcChannelNumber()==410000 || eventInfo->mcChannelNumber()==407012 || (eventInfo->mcChannelNumber()>=410082 && eventInfo->mcChannelNumber()<=410089) || eventInfo->mcChannelNumber()==407320 || eventInfo->mcChannelNumber()==407322) && fabs(particle->pdgId()) == pdgId && (particle->nParents()==0  || fabs(particle->parent(0)->pdgId()) != pdgId)) {
    
      auto result = m_MCtruthClassifier->particleTruthClassifier(particle);
      //Second in the pair is particle origin
      auto& origin = result.second;
 
      if(origin == MCTruthPartClassifier::PromptPhot)
        nt_hasMEphotonMCTC = true;
    }
  }  


  const xAOD::TruthParticleContainer* m_truthBosons(0);
  bool contains=event->contains<xAOD::TruthParticleContainer>("TruthBoson");
  
  if(contains)
    {
      EL_RETURN_CHECK("truthBosoton",event->retrieve( m_truthBosons, "TruthBoson"));
      for(const auto &boson: *m_truthBosons){
	//find the first Z (sometimes there can be more - because of how generators work)
	if(boson->isZ()){
	  V=boson->p4();
	  break;
	}
	//otherwise find a Gamma for the V boson
	else if (boson->isPhoton()){
	  V=boson->p4();
	  break;
	}

      }
      
    }
  
  nt_VbosonPt=V.Pt()/1000.;
  nt_VbosonEta=V.Eta();
  nt_VbosonPhi=V.Phi();
  nt_VbosonE=V.E()/1000.;

  
   

  const xAOD::MissingETContainer* met_Truth = 0;
  CHECK(event->retrieve(met_Truth,"MET_Truth"));


  if (!met_Truth){
    std::cout << "Could not find MET_Truth container in xAOD" << std::endl;
  }

  nt_TruthNonInt_pt=(*met_Truth)["NonInt"]->met()/1000.;
  nt_TruthNonInt_phi=(*met_Truth)["NonInt"]->phi()/1000.;
  

  float GenFiltMET = 0;
  float GenFiltHT = 0;
  
  if(acc_GenFiltMET.isAvailable(*eventInfo)){
    GenFiltMET = acc_GenFiltMET(*eventInfo);
  }
  if(acc_GenFiltHT.isAvailable(*eventInfo)){
    GenFiltHT = acc_GenFiltHT(*eventInfo);
  }
  
   
 
  nt_GenFiltMET=GenFiltMET/1000.;
  nt_GenFiltHT=GenFiltHT/1000.;
  nt_TruthChLeptsFromLQ=LeptoQuarkDecayInfo();
  nt_tcMeTcategory=TopCharmDecayInfo();
  nt_topDecay=ttbarClassification();
  nt_topISR=ttbarISRClassification();
  return true;
}

bool MasterShef::fillHFHadronTruthVariables(bool init, TTree *tree){

  bool isNominal = true;
  if( std::string(tree->GetName()) != "Nominal" )
    isNominal=false;

  //skip filling if is data
  if(!m_isMC) return true;
  //only fill the truth collection on the nominal branch
  if(!isNominal) return true;

  if (init){
    //initialise the branches
    tree->Branch("TruthBHadron_pt",&nt_truth_bhadron_pt);
    tree->Branch("TruthBHadron_eta",&nt_truth_bhadron_eta);
    tree->Branch("TruthBHadron_phi",&nt_truth_bhadron_phi);
    tree->Branch("TruthBHadron_m",&nt_truth_bhadron_m);
    
    tree->Branch("TruthCHadron_pt",&nt_truth_chadron_pt);
    tree->Branch("TruthCHadron_eta",&nt_truth_chadron_eta);
    tree->Branch("TruthCHadron_phi",&nt_truth_chadron_phi);
    tree->Branch("TruthCHadron_m",&nt_truth_chadron_m);

    tree->Branch("hasTruthBhad",&nt_hasTruthB);
    tree->Branch("hasTruthChad",&nt_hasTruthC);
    tree->Branch("hasNoTruthHad",&nt_hasNoTruth);

    return true;
  }
  
  if (m_isMC){

    //Retrieve the previously found truth B-Hadrons (see getTruthHFHadrons)
    const xAOD::TruthParticleContainer* truthBHadrons = nullptr;
    CHECK(store->retrieve(truthBHadrons, "TruthBHadrons"+m_sysName));
    const xAOD::TruthParticleContainer* truthCHadrons = nullptr;
    CHECK(store->retrieve(truthCHadrons, "TruthCHadrons"+m_sysName));
      
    //Clear ntuple 4-vectors in preparation to fill them for this event
    nt_truth_bhadron_pt.clear();
    nt_truth_bhadron_eta.clear();
    nt_truth_bhadron_phi.clear();
    nt_truth_bhadron_m.clear();

    nt_truth_chadron_pt.clear();
    nt_truth_chadron_eta.clear();
    nt_truth_chadron_phi.clear();
    nt_truth_chadron_m.clear();

    bool hasTruthB=false;
    bool hasTruthC=false;

    //Now loop over the previously found truth B-Hadrons (see getTruthHFHadrons)
    //and then fill the ntuple values
    for (auto* truthBHadron : *truthBHadrons){
      if (truthBHadron){
	nt_truth_bhadron_pt.push_back(truthBHadron->pt());
	nt_truth_bhadron_eta.push_back(truthBHadron->eta());
	nt_truth_bhadron_phi.push_back(truthBHadron->phi());
	nt_truth_bhadron_m.push_back(truthBHadron->m());
	hasTruthB=true;
      }
      else ATH_MSG_WARNING("Invalid pointer to true B hadron in fillHFHadronTruthVariables");
    }
    for (auto* truthChadron : *truthCHadrons){
      if (truthChadron){
	nt_truth_chadron_pt.push_back(truthChadron->pt());
	nt_truth_chadron_eta.push_back(truthChadron->eta());
	nt_truth_chadron_phi.push_back(truthChadron->phi());
	nt_truth_chadron_m.push_back(truthChadron->m());
	hasTruthC=true;
      }
      else ATH_MSG_WARNING("Invalid pointer to true C hadron in fillHFHadronTruthVariables");
    }
    if(hasTruthB==true){
      nt_hasTruthB=true;
      nt_hasTruthC=false;
      nt_hasNoTruth=false;
    }
    if(hasTruthB==false && hasTruthC==true){
      nt_hasTruthB=false;
      nt_hasTruthC=true;
      nt_hasNoTruth=false;
    }
    if(hasTruthB==false && hasTruthC==false){
      nt_hasTruthB=false;
      nt_hasTruthC=false;
      nt_hasNoTruth=true;
    }
    return true;
  }
  else{
    ATH_MSG_DEBUG("not filling because this is data!");
    return true;
  }
}

bool  MasterShef::fillExtraMETVariables(bool init, TTree* tree){

  ATH_MSG_VERBOSE("fiill extra met variables");
  if(init){
    m_useSimpleMET=true;
    tree->Branch("simpleMET_pt",&nt_simpleMET_pt);
    tree->Branch("simpleMET_phi",&nt_simpleMET_phi);
        
    tree->Branch("simpleMETCST_pt",&nt_simpleMETCST_pt);
    tree->Branch("simpleMETCST_phi",&nt_simpleMETCST_phi);
          
    tree->Branch("simpleMET_jet_pt",&nt_simpleMET_jet_pt);
    tree->Branch("simpleMET_jet_phi",&nt_simpleMET_jet_phi);
          
    tree->Branch("simpleMETCST_jet_pt",&nt_simpleMETCST_jet_pt);
    tree->Branch("simpleMETCST_jet_phi",&nt_simpleMETCST_jet_phi);
          
    tree->Branch("simpleMET_mu_pt",&nt_simpleMET_mu_pt);
    tree->Branch("simpleMET_mu_phi",&nt_simpleMET_mu_phi);
          
    tree->Branch("simpleMETCST_mu_pt",&nt_simpleMETCST_mu_pt);
    tree->Branch("simpleMETCST_mu_phi",&nt_simpleMETCST_mu_phi);
          
    tree->Branch("simpleMET_softTrk_pt",&nt_simpleMET_softTrk_pt);
    tree->Branch("simpleMET_softTrk_phi",&nt_simpleMET_softTrk_phi);
          
    tree->Branch("simpleMETCST_softClus_pt",&nt_simpleMETCST_softClus_pt);
    tree->Branch("simpleMETCST_softClus_phi",&nt_simpleMETCST_softClus_phi);
          
    tree->Branch("sumet_simpleTST",&nt_sumet_simpleTST);
    tree->Branch("sumet_simpleTST_jet",&nt_sumet_simpleTST_jet);
    tree->Branch("sumet_simpleTST_mu",&nt_sumet_simpleTST_mu);
    tree->Branch("sumet_simpleTST_softTrk",&nt_sumet_simpleTST_softTrk);
      
    tree->Branch("sumet_simpleCST",&nt_sumet_simpleCST);
    tree->Branch("sumet_simpleCST_jet",&nt_sumet_simpleCST_jet);
    tree->Branch("sumet_simpleCST_mu",&nt_sumet_simpleCST_mu);
    tree->Branch("sumet_simpleCST_softClus",&nt_sumet_simpleCST_softClus);
    return true;
  }


  TVector2 met;
  met.Set(m_pxmiss_simple,m_pxmiss_simple);
  nt_simpleMET_pt=met.Mod();	
  nt_simpleMET_phi=met.Phi();

  
  met.Set(m_pxmiss_simpleCST,m_pxmiss_simpleCST);
  nt_simpleMETCST_pt=met.Mod();	
  nt_simpleMETCST_phi=met.Phi();


  met.Set(m_pxmiss_simple_jet,m_pxmiss_simple_jet);
  nt_simpleMET_jet_pt=met.Mod();	
  nt_simpleMET_jet_phi=met.Phi();

  
  met.Set(m_pxmiss_simpleCST_jet,m_pxmiss_simpleCST_jet);
  nt_simpleMETCST_jet_pt=met.Mod();	
  nt_simpleMETCST_jet_phi=met.Phi();
 
  met.Set(m_pxmiss_simple_mu,m_pxmiss_simple_mu);
  nt_simpleMET_mu_pt=met.Mod();	
  nt_simpleMET_mu_phi=met.Phi();

  
  met.Set(m_pxmiss_simpleCST_mu,m_pxmiss_simpleCST_mu);
  nt_simpleMETCST_mu_pt=met.Mod();	
  nt_simpleMETCST_mu_phi=met.Phi();


  met.Set(m_pxmiss_simple_softTrk,m_pxmiss_simple_softTrk);
  nt_simpleMET_softTrk_pt=met.Mod();	
  nt_simpleMET_softTrk_phi=met.Phi();

  
  met.Set(m_pxmiss_simple_softClus,m_pxmiss_simple_softClus);
  nt_simpleMETCST_softClus_pt=met.Mod();	
  nt_simpleMETCST_softClus_phi=met.Phi();


  nt_sumet_simpleTST=m_simpleTST_sumet;
  nt_sumet_simpleTST_jet=m_simpleTST_sumet_jet;
  nt_sumet_simpleTST_mu= m_simpleTST_sumet_mu;
  nt_sumet_simpleTST_softTrk=m_simpleTST_sumet_softTrk;

  nt_sumet_simpleCST=m_simpleCST_sumet;
  nt_sumet_simpleCST_jet=m_simpleCST_sumet_jet;
  nt_sumet_simpleCST_mu=m_simpleCST_sumet_mu;
  nt_sumet_simpleCST_softClus=m_simpleCST_sumet_softClus;



  return true;

}

bool MasterShef::fillTruthLeptons(bool init,TTree* tree){
  //does what it says on the tin: fills ntuple with truth lepton variables
  ATH_MSG_VERBOSE("in fillTruthLeptons");

  if( std::string(tree->GetName()) != "Nominal" )
    return true;

  if(!m_isMC) return true;
  
  //init
  if(init){        
    if(m_doTruth) return true; // only do this for reco. in truth ntuples, these truth leptons are filled as if they were reco variables
    tree->Branch("truthel_pt",&nt_truthel_pt);
    tree->Branch("truthel_eta",&nt_truthel_eta);
    tree->Branch("truthel_phi",&nt_truthel_phi);
    tree->Branch("truthel_e",&nt_truthel_e);    
    tree->Branch("truthel_origin",&nt_truthel_origin);
    tree->Branch("truthel_type",&nt_truthel_type);

    tree->Branch("truthmu_pt",&nt_truthmu_pt);
    tree->Branch("truthmu_eta",&nt_truthmu_eta);
    tree->Branch("truthmu_phi",&nt_truthmu_phi);
    tree->Branch("truthmu_e",&nt_truthmu_e);
    tree->Branch("truthmu_origin",&nt_truthmu_origin);
    tree->Branch("truthmu_type",&nt_truthmu_type);
    return true;
  }

  
  //now call truth electrons
  this->getTruthLeptonsInRecoNtuples(true,-1.0,-1.0); 
  
  //clear all vectors, and resize them to clear any dynamically associated memory
  nt_truthel_pt.clear();
  nt_truthel_eta.clear();
  nt_truthel_phi.clear();
  nt_truthel_e.clear();
  nt_truthel_origin.clear();
  nt_truthel_type.clear();

  nt_truthel_pt.shrink_to_fit();
  nt_truthel_eta.shrink_to_fit();
  nt_truthel_phi.shrink_to_fit();
  nt_truthel_e.shrink_to_fit();
  nt_truthel_origin.shrink_to_fit();
  nt_truthel_type.shrink_to_fit();

  
  nt_truthmu_pt.clear();
  nt_truthmu_eta.clear();
  nt_truthmu_phi.clear();
  nt_truthmu_e.clear();
  nt_truthmu_origin.clear();
  nt_truthmu_type.clear();


  nt_truthmu_pt.shrink_to_fit();
  nt_truthmu_eta.shrink_to_fit();
  nt_truthmu_phi.shrink_to_fit();
  nt_truthmu_e.shrink_to_fit();
  nt_truthmu_origin.shrink_to_fit();
  nt_truthmu_type.shrink_to_fit();


  //retrieve TruthEl container, then dump variables
  xAOD::TruthParticleContainer* TruthElContainer=nullptr;
  CHECK(evtStore()->retrieve(TruthElContainer,"CustomTruthElectrons"));
  for (auto TruthEl: *TruthElContainer){
    nt_truthel_pt.push_back(TruthEl->pt());
    nt_truthel_eta.push_back(TruthEl->eta());
    nt_truthel_phi.push_back(TruthEl->phi());
    nt_truthel_e.push_back(TruthEl->e());


    unsigned int enum_origin=TruthEl->auxdata<unsigned int>("classifierParticleOrigin");
    unsigned int enum_type=TruthEl->auxdata<unsigned int>("classifierParticleType");

    MCTruthPartClassifier::ParticleDef partDef;
    std::string origin = partDef.sParticleOrigin[enum_origin];
    std::string type = partDef.sParticleType[enum_type];

    
    nt_truthel_origin.push_back(origin);
    nt_truthel_type.push_back(type);
  }



  //retrieve TruthMuon container, then dump variables
  xAOD::TruthParticleContainer* TruthMuonContainer=nullptr;
  CHECK(evtStore()->retrieve(TruthMuonContainer,"FinalTruthMuons"));
  for (auto TruthMuon: *TruthMuonContainer){
    nt_truthmu_pt.push_back(TruthMuon->pt());
    nt_truthmu_eta.push_back(TruthMuon->eta());
    nt_truthmu_phi.push_back(TruthMuon->phi());
    nt_truthmu_e.push_back(TruthMuon->e());

    unsigned int enum_origin=TruthMuon->auxdata<unsigned int>("classifierParticleOrigin");
    unsigned int enum_type=TruthMuon->auxdata<unsigned int>("classifierParticleType");

    MCTruthPartClassifier::ParticleDef partDef;
    std::string origin = partDef.sParticleOrigin[enum_origin];
    std::string type = partDef.sParticleType[enum_type];


    nt_truthmu_origin.push_back(origin);
    nt_truthmu_type.push_back(type);
  }


  return true;
}
 

bool  MasterShef::fillTruthTaus(bool init, TTree* tree){
  
  ATH_MSG_VERBOSE("in fillTruthTaus");
  if(!m_isMC) return true;
 
  if( std::string(tree->GetName()) != "Nominal" )
    return true;

  if(init){
    tree->Branch("truthTau_pt",&nt_truthtau_pt);
    tree->Branch("truthTau_eta",&nt_truthtau_eta);
    tree->Branch("truthTau_phi",&nt_truthtau_phi);
    tree->Branch("truthTau_e",&nt_truthtau_e);
    tree->Branch("truthTau_NTracks",&nt_truthtau_NTracks);
    tree->Branch("truthTau_status",&nt_truthtau_status);
    tree->Branch("truthTau_ishadronic",&nt_truthtau_ishadronic);
    tree->Branch("truthTau_origin",&nt_truthtau_origin);
    tree->Branch("truthTau_type",&nt_truthtau_type);

    return true;
  }


  
  this->getTruthTaus(-1.0,5.);
  

  //--------------------------------TRUTH TAUS-----------------------------

  nt_truthtau_pt.clear();
  nt_truthtau_eta.clear();
  nt_truthtau_phi.clear();
  nt_truthtau_e.clear();
  nt_truthtau_status.clear();
  nt_truthtau_origin.clear();
  nt_truthtau_type.clear();
  nt_truthtau_ishadronic.clear();

  const xAOD::TruthParticleContainer* m_TruthTaus=nullptr;
  //retrieve the deepcopy container we saved in getTruthTaus
  xAOD::TStore* store = wk()->xaodStore();
  CHECK(store->retrieve(m_TruthTaus,"truthTaus"));
  
  for (const auto tau: *m_TruthTaus){
    
    nt_truthtau_pt.push_back(tau->pt());
    nt_truthtau_eta.push_back(tau->eta());
    nt_truthtau_phi.push_back(tau->phi());
    nt_truthtau_e.push_back(tau->e());
    
    // As tau can only decay either hadronically or leptonically, boolean will check both
    nt_truthtau_ishadronic.push_back((bool)tau->auxdata<char>("IsHadronicTau")); 
    
    unsigned int iorigin=tau->auxdata<unsigned int>("classifierParticleOrigin");
    unsigned int itype=tau->auxdata<unsigned int>("classifierParticleType");
    ATH_MSG_VERBOSE("Got origin index of truth tau");
    MCTruthPartClassifier::ParticleDef partDef;
    std::string origin =   partDef.sParticleOrigin[iorigin] ;
    std::string type   = partDef.sParticleType[itype] ;
    ATH_MSG_VERBOSE("Got origin string of truth tau");

    nt_truthJet_origin.push_back(origin);
    nt_truthJet_type.push_back(type);
    
    nt_truthtau_origin.push_back(origin);
    nt_truthtau_type.push_back(type);
    nt_truthtau_status.push_back(tau->status());
    
    
  }// end of loop over truth taus
  



  return true;

}

bool  MasterShef::fillTruthJets(bool init, TTree* tree){
     
  
  ATH_MSG_VERBOSE("in fillTruthJets");
  //block   

  if( std::string(tree->GetName()) != "Nominal" )
    return true;

  std::string ThesysName="";
  if(!init)
     ThesysName= m_systNameList.at(m_systNum);                           
  bool isNominal(true);                                                        
  if (ThesysName != "") isNominal=false;

  if(!isNominal)
    return true;

  if(!m_isMC) return true;

  xAOD::TStore* store = wk()->xaodStore();
  if(init){
    tree->Branch("truthJet_pt",&nt_truthJet_pt);
    tree->Branch("truthJet_eta",&nt_truthJet_eta);
    tree->Branch("truthJet_phi",&nt_truthJet_phi);
    tree->Branch("truthJet_e",&nt_truthJet_e); 
    tree->Branch("truthJet_flav",&nt_truthJet_flav); 
    tree->Branch("truthJet_BdR",&nt_truthJet_BdR);
    tree->Branch("truthJet_CdR",&nt_truthJet_CdR);
    tree->Branch("truthJet_TaudR",&nt_truthJet_TaudR);
    tree->Branch("truthJet_origin",&nt_truthJet_origin);
    tree->Branch("truthJet_type",&nt_truthJet_type);

    ATH_MSG_VERBOSE("initialised branches - event  ");
    return true;
  }
  

  std::string sysName = m_systNameList.at(m_systNum);
  ATH_MSG_VERBOSE("it's not data in event ");
  ATH_MSG_VERBOSE("Fill truthJets too");

  
  //retrive the truth jets from TruthObjects.cxx
  //this stores the truthJets into `truthJets` in the store
  double pt=3000.;
  if (m_isPflowResearch) pt = 10000.;
  double eta= 5.;
  CHECK(this->getTruthJets(pt,eta));
  
  xAOD::JetContainer*  m_truthJets = 0 ;
  CHECK(store->retrieve(m_truthJets,"truthJets"));



  // Implement truth selection here!

  //save truth jets
  nt_truthJet_pt.clear();
  nt_truthJet_eta.clear();
  nt_truthJet_phi.clear();
  nt_truthJet_e.clear();
  nt_truthJet_flav.clear();
  nt_truthJet_BdR.clear();
  nt_truthJet_CdR.clear();
  nt_truthJet_TaudR.clear();
  nt_truthJet_origin.clear();
  nt_truthJet_type.clear();


  

  ATH_MSG_VERBOSE(m_truthJets->size() <<" truth jets in event");
 
  for(const auto &tjet: *m_truthJets){
    ATH_MSG_DEBUG("set up full 4 momenta for the truth het   ");
    TLorentzVector full4MomTruth = tjet->p4();
    ATH_MSG_VERBOSE("Event#: "<<eventInfo->eventNumber()<< " Initial truth P4.E(): "<<full4MomTruth.E()/1000.);

    //add the truth neutrinos and muons to the jet?
    if(m_AddNeutMuTruthJets){
      this->addNeutNuToVector(full4MomTruth);
    }//end adding truth neutrinos flag


    int flavour = -1;
    //tjet->getAttribute("HadronConeExclExtendedTruthLabelID",flavour); // New recommendations here : https://twiki.cern.ch/twiki/bin/view/AtlasProtected/FlavourTaggingLabeling
    tjet->getAttribute("ConeTruthLabelID",flavour);
    float dRTau=-1;
    tjet->getAttribute("TruthLabelDeltaR_T",dRTau);
    float dRChad=-1;
    tjet->getAttribute("TruthLabelDeltaR_C",dRChad);
    float dRBhad=-1;
    tjet->getAttribute("TruthLabelDeltaR_B",dRBhad);
    
   
    
    unsigned int iorigin=0;
    tjet->getAttribute("classifierParticleOrigin",iorigin);
    unsigned int itype=0;
    tjet->getAttribute("classifierParticleType",itype);

    ATH_MSG_VERBOSE("Got origin index of truth jet");
 
    
    MCTruthPartClassifier::ParticleDef partDef;
    std::string origin =   partDef.sParticleOrigin[iorigin] ;
    std::string type   = partDef.sParticleType[itype] ;
 
    ATH_MSG_VERBOSE("Got origin string of truth jet");

  
    nt_truthJet_pt.push_back(  full4MomTruth.Pt() );
    nt_truthJet_eta.push_back(  full4MomTruth.Eta() );
    nt_truthJet_phi.push_back(  full4MomTruth.Phi() );
    nt_truthJet_e.push_back(   full4MomTruth.E() );
    nt_truthJet_flav.push_back( flavour );

    nt_truthJet_TaudR.push_back(dRTau);
    nt_truthJet_CdR.push_back(dRChad);
    nt_truthJet_BdR.push_back(dRBhad);

    nt_truthJet_origin.push_back(origin);
    nt_truthJet_type.push_back(type);
    


  }
 
  return true;
}



bool  MasterShef::fillRMap(bool init, TTree* tree){
     
  
  ATH_MSG_VERBOSE("in the building of RMaps");

  

  xAOD::TStore* store = wk()->xaodStore();
  if(init){

    
    tree->Branch("RMapRecoJet_pt",&nt_RMapRecoJet_pt);
    tree->Branch("RMapRecoJet_eta",&nt_RMapRecoJet_eta);
    tree->Branch("RMapRecoJet_phi",&nt_RMapRecoJet_phi);
    tree->Branch("RMapRecoJet_e",&nt_RMapRecoJet_e);
    tree->Branch("RMapRecoJet_flav",&nt_RMapRecoJet_flav);
    tree->Branch("RMapRecoJet_truthIndex",&nt_RMapRecoJet_truthindex);
    tree->Branch("RMapRecoJet_baseline",&nt_RMapRecoJet_baseline);
    tree->Branch("RMapRecoJet_signal",&nt_RMapRecoJet_signal);
    tree->Branch("RMapRecoJet_passOR",&nt_RMapRecoJet_passOR);

    ATH_MSG_VERBOSE("initialised branches - RMaps  ");
    return true;
  }
  

  if(!m_isMC) return true;
  std::string sysName = m_systNameList.at(m_systNum);
 



  //clear the vectors
  nt_RMapRecoJet_pt.clear();
  nt_RMapRecoJet_eta.clear();
  nt_RMapRecoJet_phi.clear();
  nt_RMapRecoJet_e.clear();
  nt_RMapRecoJet_flav.clear();
  nt_RMapRecoJet_truthindex.clear();
  nt_RMapRecoJet_baseline.clear();
  nt_RMapRecoJet_signal.clear();
  nt_RMapRecoJet_passOR.clear();

  

  //!----------------
  // separate jet container for building the snearing response map
  // these are just all calibrated jets with pT>20 GeV and |eta|<2.8
  if(m_rMapRecoJets->size()==0){
    ATH_MSG_WARNING("NO CALIBRATED JETS, SO SKIPPING TRUTH MAP");
    return true;
  }

  //retreive the truth jets from the store
  xAOD::JetContainer*  m_truthJets = 0 ;
  CHECK(store->retrieve(m_truthJets,"truthJets"));

  if(m_truthJets == nullptr){
    ATH_MSG_ERROR("Something went wrong, you're trying to build the response map but you dont seem to have saved truth jets");
    return false;
  }

  
  //do matching the the truth jets 
  for(const auto& rjet: *m_rMapRecoJets){

    int matched_index=-1; // index of truth jet that is matched to this reco jet
    int nmatched=0; //count how many matches
    int ii=0; //index tracker
    for(const auto &tjet: *m_truthJets){

      bool truthjet_unique=true;
      //loop over the truth jets again, we want to skip truth jets that are too close
      for(const auto &tjet2: *m_truthJets){
	//skip if the two truthjets are the same
	if(tjet2==tjet) continue;
	if(tjet2->p4().DeltaR(tjet->p4()) < m_unique_dRMatching) truthjet_unique=false;
      }//end of 2nd loop over truth jets for uniqueness matching
      
      //skip if the truth jets are not unique
      //aka if anothet truthjet is within dR of 0.6 (by default)
      if(!truthjet_unique){
	ii++;
	continue;
      }
      
      // i.e. no requirement for "baseline" - this picks up pileup jets and more sources of potential mismeasured jets
      if( rjet->p4().DeltaR(tjet->p4()) <  m_TruthReco_dRMatching   ){
	nmatched++;
	if(nmatched==1){
	  matched_index=ii;
	  //std::cout << nt_truthJet_pt[matched_index]/1000. << " " << tjet->p4().Pt()/1000. << std::endl;
	}
	else if(nmatched>1) matched_index=-2; // not exclusively matched, this reco jet is matched to more than one truth jet
      }
      ii++;
    }//end of loop over truth jets
    
    ATH_MSG_VERBOSE("matched index = " << matched_index);


    //skip if no truth jet match found or no unique match found
    if(!(matched_index>=0)) continue;

    
    //check if the reco jet is isolated 
    bool isRecoJetIso=true;
    for(const auto& rjet2: *m_rMapRecoJets){
      if(rjet2==rjet)continue;
      double reco_deltaR = rjet2->p4().DeltaR(rjet->p4());
      if(reco_deltaR < m_unique_dRMatching) {
	ATH_MSG_VERBOSE("Reco jet not isolated!");
	isRecoJetIso = false;
	break;
      } 
    }// end 2nd loop over reco jets

    
    ATH_MSG_VERBOSE("isRecoJetIso = " << isRecoJetIso);

    //if the reco jet is non-isolated, then we also skip this
    if(!isRecoJetIso)continue;
    
    //we have now passed all criteria for building the response map
    //1. we have a unique match of the reco jet to a truth jet
    //2. that truth jet is isolated enough from any other truth jets
    //3. our reco jet is isolated enough from any other reco jets

    
       

    TLorentzVector full4MomReco = rjet->p4();
    
    //now add any reco muons (before OR) to the reco jets, so that we can mimic any HF decays
    if(m_AddRecoMuons && m_baselineMuonsBeforeOR->size() > 0){
      for(const auto &muon : *m_baselineMuonsBeforeOR){
	double dRMuonRecoJet = muon->p4().DeltaR(rjet->p4());
	if(dRMuonRecoJet < m_ConeSize){
	  ATH_MSG_VERBOSE("Found a muon " << dRMuonRecoJet << " pT=" << muon->p4().Pt() <<" will add it to the full4mom if in the cone size " << m_ConeSize);
	  full4MomReco+=muon->p4();
	}
      }//end of loop over baseline muons
    }//end of check if we should add reco muons to the reco jets or not
    ATH_MSG_VERBOSE("added reco muons to reco momenta for event ");
    
   
    //push back the calibrated jet pt/eta/phi/e 
    nt_RMapRecoJet_pt.push_back(  full4MomReco.Pt() );
    nt_RMapRecoJet_eta.push_back(  full4MomReco.Eta() );
    nt_RMapRecoJet_phi.push_back(  full4MomReco.Phi() );
    nt_RMapRecoJet_e.push_back(   full4MomReco.E() ); // write out b-tagged response map branches
      
    //push back matched index
    nt_RMapRecoJet_truthindex.push_back( matched_index ); 
    bool RecoIsBJet = rjet->auxdata<char>("bjet");
    bool isbaseline=rjet->auxdata<char>("baseline");
    bool issignal=rjet->auxdata<char>("signal");
    bool passOR=rjet->auxdata<char>("passOR");
    
    
    nt_RMapRecoJet_flav.push_back(RecoIsBJet);
    nt_RMapRecoJet_baseline.push_back(isbaseline);
    nt_RMapRecoJet_signal.push_back(issignal);
    nt_RMapRecoJet_passOR.push_back(passOR);
  }
  
  ATH_MSG_VERBOSE("out of truth jet loop for event ");
  return true;
}








bool  MasterShef::fillRCFatJetVariables(bool init, TTree *tree){
  ATH_MSG_VERBOSE("in rcjet variables");
  if(init){
    
    if(m_useAntiKtRcJets == false){
      Error(APP_NAME,"you're trying to fill rc fat jets with m_useAntiKtRcJets==false");
      return false;
    }
    
    // Branches : fat jets kt12 (RC)
    tree->Branch("nFatJetsKt12",&nt_nFatJetsKt12);
    tree->Branch("rcjet_kt12_pt",&nt_rcjet_kt12_pt);
    tree->Branch("rcjet_kt12_eta",&nt_rcjet_kt12_eta);
    tree->Branch("rcjet_kt12_phi",&nt_rcjet_kt12_phi);
    tree->Branch("rcjet_kt12_e",&nt_rcjet_kt12_e);
    tree->Branch("rcjet_kt12_flav",&nt_rcjet_kt12_flav);
    tree->Branch("rcjet_kt12_nconst",&nt_rcjet_kt12_nconst);
    if(m_useRCJetSS )
      {
	if(m_useJetConstituents)
	  {
	    tree->Branch("rcjet_kt12_pflowMass",&nt_rcjet_kt12_pflowMass);
	    tree->Branch("rcjet_kt12_trkMass",&nt_rcjet_kt12_trkMass);
	  }
	tree->Branch("rcjet_kt12_massConst",&nt_rcjet_kt12_massConst);
	tree->Branch("rcjet_kt12_Tau1",&nt_rcjet_kt12_Tau1);
	tree->Branch("rcjet_kt12_Tau2",&nt_rcjet_kt12_Tau2);
	tree->Branch("rcjet_kt12_Tau3",&nt_rcjet_kt12_Tau3);
	tree->Branch("rcjet_kt12_Tau21",&nt_rcjet_kt12_Tau21);
	tree->Branch("rcjet_kt12_Tau32",&nt_rcjet_kt12_Tau32);
	tree->Branch("rcjet_kt12_Split12",&nt_rcjet_kt12_Split12);
	tree->Branch("rcjet_kt12_Split23",&nt_rcjet_kt12_Split23);
	tree->Branch("rcjet_kt12_Split34",&nt_rcjet_kt12_Split34);
	tree->Branch("rcjet_kt12_Dip12",&nt_rcjet_kt12_Dip12);
	tree->Branch("rcjet_kt12_Dip13",&nt_rcjet_kt12_Dip13);
	tree->Branch("rcjet_kt12_Dip23",&nt_rcjet_kt12_Dip23);
	tree->Branch("rcjet_kt12_Qw",&nt_rcjet_kt12_Qw);
	tree->Branch("rcjet_kt12_Charge",&nt_rcjet_kt12_Charge);

	tree->Branch("rcjet_kt12_ECF1",&nt_rcjet_kt12_ECF1);	
	tree->Branch("rcjet_kt12_ECF2",&nt_rcjet_kt12_ECF2);
	tree->Branch("rcjet_kt12_ECF3",&nt_rcjet_kt12_ECF3);

	tree->Branch("rcjet_kt12_C2",&nt_rcjet_kt12_C2);
	tree->Branch("rcjet_kt12_D2",&nt_rcjet_kt12_D2);
	
      }

    tree->Branch("nFatJetsKt15",&nt_nFatJetsKt15);
    tree->Branch("rcjet_kt15_pt",&nt_rcjet_kt15_pt);
    tree->Branch("rcjet_kt15_eta",&nt_rcjet_kt15_eta);
    tree->Branch("rcjet_kt15_phi",&nt_rcjet_kt15_phi);
    tree->Branch("rcjet_kt15_e",&nt_rcjet_kt15_e);
    tree->Branch("rcjet_kt15_flav",&nt_rcjet_kt15_flav);
    tree->Branch("rcjet_kt15_nconst",&nt_rcjet_kt15_nconst);

    tree->Branch("nFatJetsKt10_RC",&nt_nFatJetsKt10_RC);
    tree->Branch("rcjet_kt10_pt",&nt_rcjet_kt10_pt);
    tree->Branch("rcjet_kt10_eta",&nt_rcjet_kt10_eta);
    tree->Branch("rcjet_kt10_phi",&nt_rcjet_kt10_phi);
    tree->Branch("rcjet_kt10_e",&nt_rcjet_kt10_e);
    tree->Branch("rcjet_kt10_flav",&nt_rcjet_kt10_flav);
    tree->Branch("rcjet_kt10_nconst",&nt_rcjet_kt10_nconst);

    // Branches : fat jets kt8 (RC)
    tree->Branch("nFatJetsKt8",&nt_nFatJetsKt8);
    tree->Branch("rcjet_kt8_pt",&nt_rcjet_kt8_pt);
    tree->Branch("rcjet_kt8_eta",&nt_rcjet_kt8_eta);
    tree->Branch("rcjet_kt8_phi",&nt_rcjet_kt8_phi);
    tree->Branch("rcjet_kt8_e",&nt_rcjet_kt8_e);
    tree->Branch("rcjet_kt8_flav",&nt_rcjet_kt8_flav);
    tree->Branch("rcjet_kt8_nconst",&nt_rcjet_kt8_nconst);

   
    if(m_useRCJetSS)
      {
	if(m_useJetConstituents)
	  {
	    tree->Branch("rcjet_kt8_pflowMass",&nt_rcjet_kt8_pflowMass);
	    tree->Branch("rcjet_kt8_trkMass",&nt_rcjet_kt8_trkMass);
	  }
	tree->Branch("rcjet_kt8_massConst",&nt_rcjet_kt8_massConst);
	tree->Branch("rcjet_kt8_Tau1",&nt_rcjet_kt8_Tau1);
	tree->Branch("rcjet_kt8_Tau2",&nt_rcjet_kt8_Tau2);
	tree->Branch("rcjet_kt8_Tau3",&nt_rcjet_kt8_Tau3);
	tree->Branch("rcjet_kt8_Tau21",&nt_rcjet_kt8_Tau21);
	tree->Branch("rcjet_kt8_Tau32",&nt_rcjet_kt8_Tau32);
	tree->Branch("rcjet_kt8_Split12",&nt_rcjet_kt8_Split12);
	tree->Branch("rcjet_kt8_Split23",&nt_rcjet_kt8_Split23);
	tree->Branch("rcjet_kt8_Split34",&nt_rcjet_kt8_Split34);
	tree->Branch("rcjet_kt8_Dip12",&nt_rcjet_kt8_Dip12);
	tree->Branch("rcjet_kt8_Dip13",&nt_rcjet_kt8_Dip13);
	tree->Branch("rcjet_kt8_Dip23",&nt_rcjet_kt8_Dip23);
	tree->Branch("rcjet_kt8_Qw",&nt_rcjet_kt8_Qw);
	tree->Branch("rcjet_kt8_Charge",&nt_rcjet_kt8_Charge);


	tree->Branch("rcjet_kt8_ECF1",&nt_rcjet_kt8_ECF1);	
	tree->Branch("rcjet_kt8_ECF2",&nt_rcjet_kt8_ECF2);
	tree->Branch("rcjet_kt8_ECF3",&nt_rcjet_kt8_ECF3);

	tree->Branch("rcjet_kt8_C2",&nt_rcjet_kt8_C2);
	tree->Branch("rcjet_kt8_D2",&nt_rcjet_kt8_D2);
      }

    return true;
  }
  

  
  ATH_MSG_VERBOSE("Filling RC jets");
  //--------------------------------
  //AntiKt 1.5 
  nt_rcjet_kt15_pt.clear();
  nt_rcjet_kt15_eta.clear();
  nt_rcjet_kt15_phi.clear();
  nt_rcjet_kt15_e.clear();
  nt_rcjet_kt15_flav.clear();
  nt_rcjet_kt15_nconst.clear();
  
  //--------------------------------
  //AntiKt 1.2 
  nt_rcjet_kt12_pt.clear();
  nt_rcjet_kt12_eta.clear();
  nt_rcjet_kt12_phi.clear();
  nt_rcjet_kt12_e.clear();
  nt_rcjet_kt12_flav.clear();
  nt_rcjet_kt12_nconst.clear();

  //substructure
  nt_rcjet_kt12_pflowMass.clear();
  nt_rcjet_kt12_trkMass.clear();
  nt_rcjet_kt12_massConst.clear();
  nt_rcjet_kt12_Tau1.clear();
  nt_rcjet_kt12_Tau2.clear();
  nt_rcjet_kt12_Tau3.clear();
  nt_rcjet_kt12_Tau21.clear();
  nt_rcjet_kt12_Tau32.clear();
  nt_rcjet_kt12_Split12.clear();
  nt_rcjet_kt12_Split23.clear();
  nt_rcjet_kt12_Split34.clear();
  nt_rcjet_kt12_Dip12.clear();
  nt_rcjet_kt12_Dip13.clear();
  nt_rcjet_kt12_Dip23.clear();
  nt_rcjet_kt12_Qw.clear();
  nt_rcjet_kt12_Charge.clear();
  nt_rcjet_kt8_ECF1.clear();
  nt_rcjet_kt8_ECF2.clear();
  nt_rcjet_kt8_ECF3.clear();
  nt_rcjet_kt8_C2.clear();
  nt_rcjet_kt8_D2.clear();

  //--------------------------------
  //AntiKt 1.0 
  nt_rcjet_kt10_pt.clear();
  nt_rcjet_kt10_eta.clear();
  nt_rcjet_kt10_phi.clear();
  nt_rcjet_kt10_e.clear();
  nt_rcjet_kt10_flav.clear();
  nt_rcjet_kt10_nconst.clear();
  //--------------------------------
  //AntiKt 8 
  nt_rcjet_kt8_pt.clear();
  nt_rcjet_kt8_eta.clear();
  nt_rcjet_kt8_phi.clear();
  nt_rcjet_kt8_e.clear();
  nt_rcjet_kt8_flav.clear();
  nt_rcjet_kt8_nconst.clear();

  //substructure
  nt_rcjet_kt8_pflowMass.clear();
  nt_rcjet_kt8_trkMass.clear();
  nt_rcjet_kt8_massConst.clear();
  nt_rcjet_kt8_Tau1.clear();
  nt_rcjet_kt8_Tau2.clear();
  nt_rcjet_kt8_Tau3.clear();
  nt_rcjet_kt8_Tau21.clear();
  nt_rcjet_kt8_Tau32.clear();
  nt_rcjet_kt8_Split12.clear();
  nt_rcjet_kt8_Split23.clear();
  nt_rcjet_kt8_Split34.clear();
  nt_rcjet_kt8_Dip12.clear();
  nt_rcjet_kt8_Dip13.clear();
  nt_rcjet_kt8_Dip23.clear();
  nt_rcjet_kt8_Qw.clear();
  nt_rcjet_kt8_Charge.clear();

  nt_rcjet_kt8_ECF1.clear();
  nt_rcjet_kt8_ECF2.clear();
  nt_rcjet_kt8_ECF3.clear();

  nt_rcjet_kt8_C2.clear();
  nt_rcjet_kt8_D2.clear();



  ATH_MSG_VERBOSE("start filling kt15");

  nt_nFatJetsKt15=m_rcjets_kt15->size();
  for (unsigned int i=0;i<m_rcjets_kt15->size();i++) {
    nt_rcjet_kt15_pt.push_back( m_rcjets_kt15->at(i)->pt() );
    nt_rcjet_kt15_eta.push_back( m_rcjets_kt15->at(i)->eta() );
    nt_rcjet_kt15_phi.push_back( m_rcjets_kt15->at(i)->phi()  );
    nt_rcjet_kt15_e.push_back(  m_rcjets_kt15->at(i)->e() );
    nt_rcjet_kt15_flav.push_back(  m_rcjets_kt15->at(i)->auxdata<int>("flav") );
    nt_rcjet_kt15_nconst.push_back(  m_rcjets_kt15->at(i)->numConstituents() );
  }

  ATH_MSG_VERBOSE("start filling kt12");
  nt_nFatJetsKt12=m_rcjets_kt12->size();
  for (unsigned int i=0;i<m_rcjets_kt12->size();i++) {
    nt_rcjet_kt12_pt.push_back( m_rcjets_kt12->at(i)->pt() );
    nt_rcjet_kt12_eta.push_back( m_rcjets_kt12->at(i)->eta() );
    nt_rcjet_kt12_phi.push_back( m_rcjets_kt12->at(i)->phi()  );
    nt_rcjet_kt12_e.push_back(  m_rcjets_kt12->at(i)->e() );
    nt_rcjet_kt12_flav.push_back(  m_rcjets_kt12->at(i)->auxdata<int>("flav") );
    nt_rcjet_kt12_nconst.push_back(  m_rcjets_kt12->at(i)->numConstituents() );


    //fill RC jets SS for only kt12 and kt8
    if(m_useRCJetSS)
      {
	if(m_useJetConstituents)
	  {
	    nt_rcjet_kt12_pflowMass.push_back(m_rcjets_kt12->at(i)->auxdata<float>("JetPFlowMass"));
	    nt_rcjet_kt12_trkMass.push_back(m_rcjets_kt12->at(i)->auxdata<float>("JetTrkMass"));
	  }
	nt_rcjet_kt12_massConst.push_back(m_rcjets_kt12->at(i)->auxdata<float>("JetConstMass"));
	nt_rcjet_kt12_Tau1.push_back(m_rcjets_kt12->at(i)->auxdata<float>("Tau_1"));
	nt_rcjet_kt12_Tau2.push_back(m_rcjets_kt12->at(i)->auxdata<float>("Tau2_wta"));
	nt_rcjet_kt12_Tau3.push_back(m_rcjets_kt12->at(i)->auxdata<float>("Tau3_wta"));
	nt_rcjet_kt12_Tau21.push_back(m_rcjets_kt12->at(i)->auxdata<float>("Tau21_wta"));
	nt_rcjet_kt12_Tau32.push_back(m_rcjets_kt12->at(i)->auxdata<float>("Tau32_wta"));
	nt_rcjet_kt12_Split12.push_back(m_rcjets_kt12->at(i)->auxdata<float>("Split12"));
	nt_rcjet_kt12_Split23.push_back(m_rcjets_kt12->at(i)->auxdata<float>("Split23"));
	nt_rcjet_kt12_Split34.push_back(m_rcjets_kt12->at(i)->auxdata<float>("Split34"));
	nt_rcjet_kt12_Dip12.push_back(m_rcjets_kt12->at(i)->auxdata<float>("Dip12"));
	nt_rcjet_kt12_Dip13.push_back(m_rcjets_kt12->at(i)->auxdata<float>("Dip13"));
	nt_rcjet_kt12_Dip23.push_back(m_rcjets_kt12->at(i)->auxdata<float>("Dip23"));
	nt_rcjet_kt12_Qw.push_back(m_rcjets_kt12->at(i)->auxdata<float>("Qw"));
	nt_rcjet_kt12_Charge.push_back(m_rcjets_kt12->at(i)->auxdata<float>("Charge"));


	float ECF1=m_rcjets_kt12->at(i)->auxdata<float>("ECF1");
	float ECF2=m_rcjets_kt12->at(i)->auxdata<float>("ECF2");
	float ECF3=m_rcjets_kt12->at(i)->auxdata<float>("ECF3");
	
	float D2=-9.0;
	float C2=-9.0;
	
	if (ECF2>0.0){// div 0 check
	  D2=ECF3/ECF2;
	  C2=(ECF1*ECF3)/(pow(ECF2,2.0));
	} 
	nt_rcjet_kt12_ECF1.push_back(ECF1);
	nt_rcjet_kt12_ECF2.push_back(ECF2);
	nt_rcjet_kt12_ECF3.push_back(ECF3);
	nt_rcjet_kt12_D2.push_back(D2);
	nt_rcjet_kt12_C2.push_back(C2);
	
      }
  }
  ATH_MSG_VERBOSE("start filling kt10");
  if(m_useAntiKt10Jets_RC){
    nt_nFatJetsKt10_RC=m_rcjets_kt10->size();

    for (unsigned int i=0;i<m_rcjets_kt10->size();i++) {
      nt_rcjet_kt10_pt.push_back( m_rcjets_kt10->at(i)->pt() );
      nt_rcjet_kt10_eta.push_back( m_rcjets_kt10->at(i)->eta() );
      nt_rcjet_kt10_phi.push_back( m_rcjets_kt10->at(i)->phi()  );
      nt_rcjet_kt10_e.push_back(  m_rcjets_kt10->at(i)->e() );
      nt_rcjet_kt10_flav.push_back(  m_rcjets_kt10->at(i)->auxdata<int>("flav") );
      nt_rcjet_kt10_nconst.push_back(  m_rcjets_kt10->at(i)->numConstituents() );
    }
  }
  ATH_MSG_VERBOSE("start filling kt8");
  nt_nFatJetsKt8=m_rcjets_kt8->size();
  for (unsigned int i=0;i<m_rcjets_kt8->size();i++) {
    nt_rcjet_kt8_pt.push_back( m_rcjets_kt8->at(i)->pt() );
    nt_rcjet_kt8_eta.push_back( m_rcjets_kt8->at(i)->eta() );
    nt_rcjet_kt8_phi.push_back( m_rcjets_kt8->at(i)->phi()  );
    nt_rcjet_kt8_e.push_back(  m_rcjets_kt8->at(i)->e() );
    nt_rcjet_kt8_flav.push_back(  m_rcjets_kt8->at(i)->auxdata<int>("flav") );
    nt_rcjet_kt8_nconst.push_back(  m_rcjets_kt8->at(i)->numConstituents() );

    //fill RC jets SS for only kt12 and kt8
    if(m_useRCJetSS)
      {
	if(m_useJetConstituents)
	  {
	    nt_rcjet_kt8_pflowMass.push_back(m_rcjets_kt8->at(i)->auxdata<float>("JetPFlowMass"));
	    nt_rcjet_kt8_trkMass.push_back(m_rcjets_kt8->at(i)->auxdata<float>("JetTrkMass"));
	  }
	nt_rcjet_kt8_massConst.push_back(m_rcjets_kt8->at(i)->auxdata<float>("JetConstMass"));
	nt_rcjet_kt8_Tau1.push_back(m_rcjets_kt8->at(i)->auxdata<float>("Tau_1"));
	nt_rcjet_kt8_Tau2.push_back(m_rcjets_kt8->at(i)->auxdata<float>("Tau2_wta"));
	nt_rcjet_kt8_Tau3.push_back(m_rcjets_kt8->at(i)->auxdata<float>("Tau3_wta"));
	nt_rcjet_kt8_Tau21.push_back(m_rcjets_kt8->at(i)->auxdata<float>("Tau21_wta"));
	nt_rcjet_kt8_Tau32.push_back(m_rcjets_kt8->at(i)->auxdata<float>("Tau32_wta"));
	nt_rcjet_kt8_Split12.push_back(m_rcjets_kt8->at(i)->auxdata<float>("Split12"));
	nt_rcjet_kt8_Split23.push_back(m_rcjets_kt8->at(i)->auxdata<float>("Split23"));
	nt_rcjet_kt8_Split34.push_back(m_rcjets_kt8->at(i)->auxdata<float>("Split34"));
	nt_rcjet_kt8_Dip12.push_back(m_rcjets_kt8->at(i)->auxdata<float>("Dip12"));
	nt_rcjet_kt8_Dip13.push_back(m_rcjets_kt8->at(i)->auxdata<float>("Dip13"));
	nt_rcjet_kt8_Dip23.push_back(m_rcjets_kt8->at(i)->auxdata<float>("Dip23"));
	nt_rcjet_kt8_Qw.push_back(m_rcjets_kt8->at(i)->auxdata<float>("Qw"));
	nt_rcjet_kt8_Charge.push_back(m_rcjets_kt8->at(i)->auxdata<float>("Charge"));

	float ECF1=m_rcjets_kt8->at(i)->auxdata<float>("ECF1");
	float ECF2=m_rcjets_kt8->at(i)->auxdata<float>("ECF2");
	float ECF3=m_rcjets_kt8->at(i)->auxdata<float>("ECF3");
	
	float D2=-9.0;
	float C2=-9.0;
	
	if (ECF2>0.0){// div 0 check
	  D2=ECF3/ECF2;
	  C2=(ECF1*ECF3)/(pow(ECF2,2.0));
	} 
	nt_rcjet_kt8_ECF1.push_back(ECF1);
	nt_rcjet_kt8_ECF2.push_back(ECF2);
	nt_rcjet_kt8_ECF3.push_back(ECF3);
	nt_rcjet_kt8_D2.push_back(D2);
	nt_rcjet_kt8_C2.push_back(C2);
	
      }
  }
  

  return true;
}







bool  MasterShef::fillFatJetVariables(bool init, TTree *tree){
  ATH_MSG_VERBOSE("in fatjet variables");
  if(init){
    if(m_useAntiKt10Jets_ST == false){
      Error(APP_NAME,"you're trying to fill fat jets from SUSYTools with m_useAntiKt10Jets_ST == false ");
      return false;
    } 

      // Braches : fat jets from DxAOD
    tree->Branch("nFatJetsKt10_ST",&nt_nFatJetsSt);
    tree->Branch("fatjet_kt10_pt",&nt_fatjet_kt10_pt);
    tree->Branch("fatjet_kt10_eta",&nt_fatjet_kt10_eta);
    tree->Branch("fatjet_kt10_phi",&nt_fatjet_kt10_phi);
    tree->Branch("fatjet_kt10_e",&nt_fatjet_kt10_e);
    tree->Branch("fatjet_kt10_w",&nt_fatjet_kt10_wtag);
    tree->Branch("fatjet_kt10_z",&nt_fatjet_kt10_ztag);
    //tree->Branch("fatjet_kt10_w50",&nt_fatjet_kt10_wtag50);
    //tree->Branch("fatjet_kt10_w80",&nt_fatjet_kt10_wtag80);
    //tree->Branch("fatjet_kt10_z50",&nt_fatjet_kt10_ztag50);
    //tree->Branch("fatjet_kt10_z80",&nt_fatjet_kt10_ztag80);
    //ree->Branch("fatjet_kt10_top50",&nt_fatjet_kt10_toptag50);
    //tree->Branch("fatjet_kt10_top80",&nt_fatjet_kt10_toptag80);

    tree->Branch("fatjet_kt10_DNNTop50",&nt_fatjet_kt10_dnntoptag50);
    tree->Branch("fatjet_kt10_DNNTop80",&nt_fatjet_kt10_dnntoptag80);
    tree->Branch("fatjet_kt10_DNNTopTagged",&nt_fatjet_kt10_dnntoptag);
    tree->Branch("topjetweight",&nt_topjetweight);
    
    tree->Branch("fatjet_kt10_nconstituents",&nt_fatjet_kt10_nconstituents);
    tree->Branch("fatjet_kt10_ntrkjets",&nt_fatjet_kt10_ntrkjets);
    tree->Branch("fatjet_kt10_nghostbhad",&nt_fatjet_kt10_nghostbhad);

    if (m_saveFatJetExtraVariables){
    tree->Branch("fatjet_kt10_Split12",&nt_fatjet_kt10_Split12);
    tree->Branch("fatjet_kt10_Split23",&nt_fatjet_kt10_Split23);
    tree->Branch("fatjet_kt10_Split34",&nt_fatjet_kt10_Split34);
    tree->Branch("fatjet_kt10_Qw",&nt_fatjet_kt10_Qw);
    tree->Branch("fatjet_kt10_Tau1",&nt_fatjet_kt10_Tau1);
    tree->Branch("fatjet_kt10_Tau2",&nt_fatjet_kt10_Tau2);
    tree->Branch("fatjet_kt10_Tau3",&nt_fatjet_kt10_Tau3);
    tree->Branch("fatjet_kt10_Tau32",&nt_fatjet_kt10_Tau32);
    tree->Branch("nt_fatjet_kt10_W50res",&nt_fatjet_kt10_W50res);
    tree->Branch("nt_fatjet_kt10_W80res",&nt_fatjet_kt10_W80res);
    tree->Branch("nt_fatjet_kt10_Z50res",&nt_fatjet_kt10_Z50res);
    tree->Branch("nt_fatjet_kt10_Z80res",&nt_fatjet_kt10_Z80res);
    tree->Branch("nt_fatjet_kt10_WLowWMassCut50",&nt_fatjet_kt10_WLowWMassCut50);
    tree->Branch("nt_fatjet_kt10_WLowWMassCut80",&nt_fatjet_kt10_WLowWMassCut80);
    tree->Branch("nt_fatjet_kt10_ZLowWMassCut50",&nt_fatjet_kt10_ZLowWMassCut50);
    tree->Branch("nt_fatjet_kt10_ZLowWMassCut80",&nt_fatjet_kt10_ZLowWMassCut80);
    tree->Branch("nt_fatjet_kt10_WHighWMassCut50",&nt_fatjet_kt10_WHighWMassCut50);
    tree->Branch("nt_fatjet_kt10_WHighWMassCut80",&nt_fatjet_kt10_WHighWMassCut80);
    tree->Branch("nt_fatjet_kt10_ZHighWMassCut50",&nt_fatjet_kt10_ZHighWMassCut50);
    tree->Branch("nt_fatjet_kt10_ZHighWMassCut80",&nt_fatjet_kt10_ZHighWMassCut80);
    tree->Branch("nt_fatjet_kt10_WD2Cut50",&nt_fatjet_kt10_WD2Cut50);
    tree->Branch("nt_fatjet_kt10_WD2Cut80",&nt_fatjet_kt10_WD2Cut80);
    tree->Branch("nt_fatjet_kt10_ZD2Cut50",&nt_fatjet_kt10_ZD2Cut50);
    tree->Branch("nt_fatjet_kt10_ZD2Cut80",&nt_fatjet_kt10_ZD2Cut80);
    tree->Branch("nt_fatjet_kt10_top50res",&nt_fatjet_kt10_top50res);
    tree->Branch("nt_fatjet_kt10_top80res",&nt_fatjet_kt10_top80res);
    tree->Branch("nt_fatjet_kt10_TopTagTau32Cut50",&nt_fatjet_kt10_TopTagTau32Cut50);
    tree->Branch("nt_fatjet_kt10_TopTagTau32Cut80",&nt_fatjet_kt10_TopTagTau32Cut80);
    tree->Branch("nt_fatjet_kt10_TopTagSplit23Cut50",&nt_fatjet_kt10_TopTagSplit23Cut50);
    tree->Branch("nt_fatjet_kt10_TopTagSplit23Cut80",&nt_fatjet_kt10_TopTagSplit23Cut80);
    tree->Branch("fatjet_kt10_BetaG",&nt_fatjet_kt10_BetaG);
    tree->Branch("fatjet_kt10_ECF1",&nt_fatjet_kt10_ECF1);
    tree->Branch("fatjet_kt10_ECF2",&nt_fatjet_kt10_ECF2);
    tree->Branch("fatjet_kt10_ECF3",&nt_fatjet_kt10_ECF3);
    }

    

    tree->Branch("fatjet_kt10_D2",&nt_fatjet_kt10_D2);
    tree->Branch("fatjet_kt10_C2",&nt_fatjet_kt10_C2);

    tree->Branch("fatjet_kt10_origin",&nt_fatjet_kt10_origin);
    
    return true;
  }
  


  ATH_MSG_VERBOSE("Filling fat jets");

  
  
  ATH_MSG_VERBOSE("Filling LargeR jets");

  nt_fatjet_kt10_pt.clear();
  nt_fatjet_kt10_eta.clear();
  nt_fatjet_kt10_phi.clear();
  nt_fatjet_kt10_e.clear();
  nt_fatjet_kt10_nghostbhad.clear();
  nt_fatjet_kt10_ntrkjets.clear();
  nt_fatjet_kt10_dnntoptag50.clear();
  nt_fatjet_kt10_dnntoptag80.clear();
  nt_fatjet_kt10_dnntoptag.clear();
  nt_fatjet_kt10_wtag.clear();
  nt_fatjet_kt10_ztag.clear();
  nt_fatjet_kt10_nconstituents.clear();
  nt_fatjet_kt10_wtag50.clear();
  nt_fatjet_kt10_wtag80.clear();
  nt_fatjet_kt10_ztag50.clear();
  nt_fatjet_kt10_ztag80.clear();
  nt_fatjet_kt10_toptag50.clear();
  nt_fatjet_kt10_toptag80.clear();



  nt_fatjet_kt10_Split12.clear();
  nt_fatjet_kt10_Split23.clear();
  nt_fatjet_kt10_Split34.clear();
  nt_fatjet_kt10_Tau1.clear();
  nt_fatjet_kt10_Tau2.clear();
  nt_fatjet_kt10_Tau3.clear();
  nt_fatjet_kt10_Qw.clear();
  nt_fatjet_kt10_Tau32.clear();
  nt_fatjet_kt10_W50res.clear();
  nt_fatjet_kt10_W80res.clear();
  nt_fatjet_kt10_Z50res.clear();
  nt_fatjet_kt10_Z80res.clear();
  nt_fatjet_kt10_WLowWMassCut50.clear();
  nt_fatjet_kt10_WLowWMassCut80.clear();
  nt_fatjet_kt10_ZLowWMassCut50.clear();
  nt_fatjet_kt10_ZLowWMassCut80.clear();
  nt_fatjet_kt10_WHighWMassCut50.clear();
  nt_fatjet_kt10_WHighWMassCut80.clear();
  nt_fatjet_kt10_ZHighWMassCut50.clear();
  nt_fatjet_kt10_ZHighWMassCut80.clear();
  nt_fatjet_kt10_WD2Cut50.clear();
  nt_fatjet_kt10_WD2Cut80.clear();
  nt_fatjet_kt10_ZD2Cut50.clear();
  nt_fatjet_kt10_ZD2Cut80.clear();
  nt_fatjet_kt10_top50res.clear();
  nt_fatjet_kt10_top80res.clear();
  nt_fatjet_kt10_TopTagTau32Cut50.clear();
  nt_fatjet_kt10_TopTagTau32Cut80.clear();
  nt_fatjet_kt10_TopTagSplit23Cut50.clear();
  nt_fatjet_kt10_TopTagSplit23Cut80.clear();

  nt_fatjet_kt10_ECF1.clear();
  nt_fatjet_kt10_ECF2.clear();
  nt_fatjet_kt10_ECF3.clear();
  nt_fatjet_kt10_C2.clear();
  nt_fatjet_kt10_D2.clear();

  nt_fatjet_kt10_BetaG.clear();
  nt_fatjet_kt10_origin.clear();
  nt_topjetweight = 1.0 ; 


  static SG::AuxElement::Decorator<float>    dec_mcutL ("WZLowWMassCut");
  static SG::AuxElement::Decorator<float>    dec_mcutH ("WZHighWMassCut");
  static SG::AuxElement::Decorator<float>    dec_d2cut ("WZD2Cut");
  static SG::AuxElement::Decorator<float>    dec_tau32 ("TopTagTau32Cut");
  static SG::AuxElement::Decorator<float>    dec_s23 ("TopTagSplit23Cut");

  if( m_useAntiKt10Jets_ST){

    for(const auto& jet : *m_fatjets_copy) {
      if( jet->pt() <200e03 || fabs(jet->eta())> 2.0  )
	continue;

      std::vector<const xAOD::Jet*> trkJets,ghostBHadrons;
      int ntrkjets = 0;
      if (jet->getAssociatedObjects<xAOD::Jet>("GhostAntiKt2TrackJet", trkJets)) {
	ntrkjets = trkJets.size(); 
	//for (const xAOD::Jet* trkJ : trkJets) {
	//if (!trkJ) continue; // if the trackjet is not valid then skip it                                                                            
	//std::cout << trkJ->pt() << std::endl;
	//if (!(trkJ->pt() / 1000. > 10. && fabs(trkJ->eta()) < 2.5)) continue;
	//goodTrkJets.push_back(trkJ);
	//ntrkjets++;
	//}
      }
      //std::sort(goodTrkJets.begin(), goodTrkJets.end(), ptsorter);
      /*
	if(ntrkjets>0){    
	const xAOD::IParticle *trkJ_1 = goodTrkJets.at(0);
	std::cout << trkJ_1 << std::endl;
	std::cout << trkJ_1->p4().Pt() << std::endl;
	std::cout << trkJ_1->e() << std::endl;
	//if (trkJ_1->auxdata< float >("MV2c10") > 0.3706) nbtaggedtrkjets++; // 77% efficiency WP for AntiKt2PV0TrackJets
      
	if (ntrkjets>1){
	const xAOD::IParticle *trkJ_2 = goodTrkJets.at(1);
	if (trkJ_2->auxdata< float >("MV2c10") > 0.3706) nbtaggedtrkjets++;    
      
	if (ntrkjets>2){
	const xAOD::IParticle *trkJ_3 = goodTrkJets.at(2);
	if (trkJ_3->auxdata< float >("MV2c10") > 0.3706) nbtaggedtrkjets++;
	}
	}
      */
      int nghostbhadrons=0;
      if (jet->getAssociatedObjects<xAOD::Jet>("GhostBHadronsFinal", ghostBHadrons)) {
        nghostbhadrons = ghostBHadrons.size();
      }
      
      nt_fatjet_kt10_ntrkjets.push_back(ntrkjets);
      nt_fatjet_kt10_nghostbhad.push_back(nghostbhadrons);
      
      /*
      std::vector<const xAOD::IParticle*> jtracks;
      jet->getAssociatedObjects<xAOD::IParticle>( xAOD::JetAttribute::GhostTrack,jtracks);
      for(size_t iConst=0; iConst<jtracks.size(); ++iConst) {
	const xAOD::TrackParticle* pTrk = static_cast<const xAOD::TrackParticle*>(jtracks[iConst]);
	if(!pTrk)
	  continue;
	
      }
      */
      
      /*
	xAOD::JetConstituentVector cons = jet->getConstituents();
	if (cons.isValid()){
	for(const auto con: cons){
	const xAOD::IParticle* rawObj = con->rawConstituent();
	std::cout << con->pt() << " " << rawObj->type() << std::endl;
	}
	}
      */
      nt_fatjet_kt10_wtag.push_back( jet->pt() >= 200e3 && jet->pt() < 2500e3 && jet->m() >= 40e3 && jet->m() < 600e3 && fabs(jet->eta()) < 2 ?  jet->auxdata<int>("wtagged") : 0 );
      nt_fatjet_kt10_ztag.push_back(jet->pt() >= 200e3 && jet->pt() < 2500e3 && jet->m() >= 40e3 && jet->m() < 600e3 && fabs(jet->eta()) < 2 ?  jet->auxdata<int>("ztagged") : 0 );

      // bool wtag50 = m_smoothedWTagger50->tag( *jet );
      // bool wtag80 = m_smoothedWTagger80->tag( *jet );
      // bool ztag50 = m_smoothedZTagger50->tag( *jet );
      // bool ztag80 = m_smoothedZTagger80->tag( *jet );

      // bool toptag50 = m_smoothedTopTagger50.tag( *jet );       
      // bool toptag80 = m_smoothedTopTagger80.tag( *jet );

      int DNNtoptag50=-99;
      int DNNtoptag80=-99;
      DNNtoptag50=jet->pt() >= 350e3 && jet->pt() < 2500e3 && jet->m() >= 40e3 && fabs(jet->eta()) < 2 ? (int)m_DNNTopTagger50->tag(*jet) : 0; // change bool to int for writing
      DNNtoptag80=jet->pt() >= 350e3 && jet->pt() < 2500e3 && jet->m() >= 40e3 && fabs(jet->eta()) < 2  ? (int)m_DNNTopTagger80->tag(*jet) : 0; // change bool to int for writing
      nt_fatjet_kt10_dnntoptag80.push_back(DNNtoptag80);
      nt_fatjet_kt10_dnntoptag50.push_back(DNNtoptag50);
      nt_fatjet_kt10_dnntoptag.push_back(jet->auxdata<int>("toptagged"));
      nt_fatjet_kt10_nconstituents.push_back(jet->numConstituents());


      if( jet->isAvailable<float>("DNNTaggerTopQuarkInclusive80_SF"))
	nt_topjetweight *= jet->pt() >= 350e3 && jet->pt() < 2500e3 && jet->m() >= 40e3 && fabs(jet->eta()) < 2  ? jet->auxdata<float>("DNNTaggerTopQuarkInclusive80_SF") : 1.0 ;
      else
	ATH_MSG_WARNING("Cannot find the top-tagger scale-factors. SF configured to be DNNTaggerTopQuarkInclusive80.  Please, review your settings");

      //float score = m_DNNTopTagger50->getScore(*jet);
      //if(score < -660 )
      //std::cout << " Score : " << score << " Number of constituents : " << jet->numConstituents() << std::endl;

      /*
	SmoothedWZTagger::Result resW50 = m_smoothedWTagger50->result( *jet, true );
	float WLowWMassCut50 = dec_mcutL(*jet);
	float WHighWMassCut50 = dec_mcutH(*jet);
	float WD2Cut50 = dec_d2cut(*jet);
	int W50res = resW50;

	SmoothedWZTagger::Result resW80 = m_smoothedWTagger80->result( *jet, true );
	float WLowWMassCut80 = dec_mcutL(*jet);
	float WHighWMassCut80 = dec_mcutH(*jet);
	float WD2Cut80 = dec_d2cut(*jet);
	int W80res = resW80;
      
	SmoothedWZTagger::Result resZ50 = m_smoothedZTagger50->result( *jet, true );
	float ZLowWMassCut50 = dec_mcutL(*jet);
	float ZHighWMassCut50 = dec_mcutH(*jet);
	float ZD2Cut50 = dec_d2cut(*jet);
	int Z50res = resZ50;

	SmoothedWZTagger::Result resZ80 = m_smoothedZTagger80->result( *jet, true );       
	float ZLowWMassCut80 = dec_mcutL(*jet);
	float ZHighWMassCut80 = dec_mcutH(*jet);
	float ZD2Cut80 = dec_d2cut(*jet);
	int Z80res = resZ80;

	SmoothedTopTagger::Result res50 = m_smoothedTopTagger50->result( *jet, true );
	float TopTagTau32Cut50 = dec_tau32(*jet);
	float TopTagSplit23Cut50 = dec_s23(*jet);
	int top50res = -1;
	if (res50.allPassed()) top50res = 1;
	else if ( res50.split23Passed() ) top50res = 2;
	else if ( res50.tau32Passed() ) top50res = 3;

	SmoothedTopTagger::Result res80 = m_smoothedTopTagger80->result( *jet, true );
	float TopTagTau32Cut80 = dec_tau32(*jet);
	float TopTagSplit23Cut80 =	dec_s23(*jet);
	int top80res = -1;
	if (res80.allPassed()) top80res = 1;
	else if ( res80.split23Passed() ) top80res = 2;
	else if ( res80.tau32Passed() ) top80res = 3;
	nt_fatjet_kt10_wtag50.push_back(wtag50);
	nt_fatjet_kt10_wtag80.push_back(wtag80);
	nt_fatjet_kt10_ztag50.push_back(ztag50);
	nt_fatjet_kt10_ztag80.push_back(ztag80);   
	
	nt_fatjet_kt10_W50res.push_back(W50res);
	nt_fatjet_kt10_W80res.push_back(W80res);
	nt_fatjet_kt10_Z50res.push_back(Z50res);
	nt_fatjet_kt10_Z80res.push_back(Z80res);
	nt_fatjet_kt10_WLowWMassCut50.push_back(WLowWMassCut50);
	nt_fatjet_kt10_WLowWMassCut80.push_back(WLowWMassCut80);
	nt_fatjet_kt10_ZLowWMassCut50.push_back(ZLowWMassCut50);
	nt_fatjet_kt10_ZLowWMassCut80.push_back(ZLowWMassCut80);
	nt_fatjet_kt10_WHighWMassCut50.push_back(WHighWMassCut50);
	nt_fatjet_kt10_WHighWMassCut80.push_back(WHighWMassCut80);
	nt_fatjet_kt10_ZHighWMassCut50.push_back(ZHighWMassCut50);
	nt_fatjet_kt10_ZHighWMassCut80.push_back(ZHighWMassCut80);
	nt_fatjet_kt10_WD2Cut50.push_back(WD2Cut50);
	nt_fatjet_kt10_WD2Cut80.push_back(WD2Cut80);
	nt_fatjet_kt10_ZD2Cut50.push_back(ZD2Cut50);
	nt_fatjet_kt10_ZD2Cut80.push_back(ZD2Cut80);
     
	nt_fatjet_kt10_toptag50.push_back(toptag50);
	nt_fatjet_kt10_toptag80.push_back(toptag80);
     
	nt_fatjet_kt10_top50res.push_back(top50res);
	nt_fatjet_kt10_top80res.push_back(top80res);
	nt_fatjet_kt10_TopTagTau32Cut50.push_back(TopTagTau32Cut50);
	nt_fatjet_kt10_TopTagTau32Cut80.push_back(TopTagTau32Cut80);
	nt_fatjet_kt10_TopTagSplit23Cut50.push_back(TopTagSplit23Cut50);
	nt_fatjet_kt10_TopTagSplit23Cut80.push_back(TopTagSplit23Cut80);
      */

      nt_fatjet_kt10_pt.push_back( jet->pt() );
      nt_fatjet_kt10_eta.push_back( jet->eta() );
      nt_fatjet_kt10_phi.push_back( jet->phi()  );
      nt_fatjet_kt10_e.push_back(  jet->e() );

    
      nt_fatjet_kt10_Split12.push_back( jet->auxdata< float >("Split12") );   
      nt_fatjet_kt10_Split23.push_back( jet->auxdata< float >("Split23") );
      nt_fatjet_kt10_Split34.push_back( jet->auxdata< float >("Split34") );
      nt_fatjet_kt10_Qw.push_back( jet->auxdata< float >("Qw") );
      nt_fatjet_kt10_Tau1.push_back( jet->auxdata< float >("Tau1_wta") );
      nt_fatjet_kt10_Tau2.push_back( jet->auxdata< float >("Tau2_wta") );
      nt_fatjet_kt10_Tau3.push_back( jet->auxdata< float >("Tau3_wta") );
      float ECF1=0.0;
      float ECF2=0.0;
      float ECF3=0.0;

      ECF1=jet->auxdata<float>("ECF1");
      ECF2=jet->auxdata<float>("ECF2");
      ECF3=jet->auxdata<float>("ECF3");
      
      nt_fatjet_kt10_ECF1.push_back( ECF1);
      nt_fatjet_kt10_ECF2.push_back( ECF2);
      nt_fatjet_kt10_ECF3.push_back( ECF3);
      
      
      float C2=-9.0;
      float D2=-9.0;
      if (ECF2!=0.0){
	C2=(ECF3*ECF1)/(std::pow(ECF2,2));
	D2=ECF3/ECF2;
      }
      nt_fatjet_kt10_D2.push_back(C2);
      nt_fatjet_kt10_C2.push_back(D2);


      float tau32=-9.0;
      //tau2 == 0 exception for the ratio
      if (jet->auxdata<float>("Tau2_wta")!=0){
	tau32=jet->auxdata< float >("Tau3_wta") / jet->auxdata< float >("Tau2_wta");
      }
      nt_fatjet_kt10_Tau32.push_back( tau32 );
    

      //std::cout << jet->auxdata< float >("Tau1_wta") << " " << jet->auxdata< float >("Tau1") <<std::endl;
     
      
      /*
	xAOD::JetConstituentVector constituents = jet->getConstituents();
	//if (constituents.isValid()){
	for( auto c : constituents ) {
	const xAOD::IParticle* rawObj = c->rawConstituent();
	std::cout << c << " " << rawObj << std::endl;
	//assume the jet is EMTopo, needs a fix for PFlow (PFO)
	if( rawObj->type() == xAOD::Type::CaloCluster ) {
	  const xAOD::CaloCluster* cluster = static_cast< const xAOD::CaloCluster* >( rawObj );
	  //particle->charge=cluster->charge();
	}
      }
      */


      /*
      const ElementLink<xAOD::JetContainer>& linkToUngroomed  = jet->getAttribute<ElementLink<xAOD::JetContainer> >("Parent");
      const xAOD::Jet* ungroomed_jet = *linkToUngroomed;
      std::cout << "ungroomed = " << ungroomed_jet << std::endl;

      std::cout << " try nsubjets" << std::endl;
      std::cout << "nconst= " << jet->numConstituents() << std::endl;

      //try a different way to get cons
      static SG::AuxElement::Accessor< std::vector<ElementLink<xAOD::IParticleContainer> > > acc("consitutentLinks");

      const std::vector<ElementLink<xAOD::IParticleContainer> > * constits = &acc(*jet);

      std::cout << "loop over? " << constits->size() << std::endl;
      for(const auto &con: *constits){
	std::cout << con << std::endl;
      }


      */

      //std::cout << std::endl << "#################### event ##################" << std::endl;
      //std::cout << jet->auxdata< float >("Tau1_wta") << " " << jet->auxdata< float >("Tau1") <<std::endl;
      
      //xAOD::Jet *testjet = new xAOD::Jet(*jet);
      //CHECK(nsubjetinessTool->modifyJet(*testjet));
      //std::cout << testjet->auxdata< float >("Tau1_wta") << " " << testjet->auxdata< float >("Tau1") <<std::endl;

      //CHECK(calcJSS(*jet));

      //jet->auxdata< float >("tau1_beta0p5") << " " <<  jet->auxdata< float >("tau1_beta0p5_wta")  << std::endl;
      
      //double beta_3g = 0;
      //double prod1 = (jet->auxdata< float >("tau1_beta0p5") * jet->auxdata< float >("tau2_beta1p0"));
      //if(prod1>0)
      //	beta_3g=jet->auxdata< float >("tau2_beta2p0") / prod1 ;

     
      //nt_fatjet_kt10_BetaG.push_back( beta_3g );



      std::string origin = "";
      if(m_isMC && m_useMCTruthClassifier){
	auto result = m_MCtruthClassifier->particleTruthClassifier(jet,true);
	ATH_MSG_VERBOSE("Gote result.");
	MCTruthPartClassifier::ParticleDef partDef;
	origin = partDef.sParticleOrigin[result.second];
	ATH_MSG_VERBOSE("Got origin");
      }

          
      
      nt_fatjet_kt10_origin.push_back( origin );



    }
    nt_nFatJetsSt=nt_fatjet_kt10_pt.size();
  }//end of check of large R jets!
  return true;
}


bool  MasterShef::fillTrackJetVariables(bool init, TTree *tree){
  ATH_MSG_VERBOSE("in fill track jet variables");
  
  if(init){
    
    if(m_useTrackJets == false){
      Error(APP_NAME,"you're trying to fill track jet variables with m_useTrackJets==false");
      return false;
    }
    
    tree->Branch("trackjet_pt",&nt_trackjet_pt);
    tree->Branch("trackjet_eta",&nt_trackjet_eta);
    tree->Branch("trackjet_phi",&nt_trackjet_phi);
    tree->Branch("trackjet_e",&nt_trackjet_e);
    tree->Branch("trackjet_isbjet",&nt_trackjet_isbjet);
    //tree->Branch("trackjet_passISROR",&nt_trackjet_orISR);
    //tree->Branch("trackjet_ntrk",&nt_trackjet_ntrk);
    tree->Branch("trackjet_MV2c10",&nt_trackjet_MV2c10);
    tree->Branch("trackjet_flav",&nt_trackjet_flav);
    tree->Branch("trackjet_passDR",&nt_trackjet_passDR);
    tree->Branch("trackjet_truthflav",&nt_trackjet_truthflav);
    tree->Branch("trackjet_truthflavhadexcl",&nt_trackjet_truthflavhadexcl);
    tree->Branch("trackjet_origin",&nt_trackjet_origin);
    tree->Branch("trackjet_type",&nt_trackjet_type);
    if (m_useHFTruthHadrons && m_isMC) tree->Branch("trackjet_index_truthbhadron",&nt_trackjet_index_truthbhadron);
       

    return true;
  }

  nt_trackjet_pt.clear();
  nt_trackjet_eta.clear();
  nt_trackjet_phi.clear();
  nt_trackjet_e.clear();
  nt_trackjet_isbjet.clear();
  nt_trackjet_flav.clear();
  nt_trackjet_passDR.clear();
  nt_trackjet_truthflav.clear();
  nt_trackjet_truthflavhadexcl.clear();
  nt_trackjet_MV2c10.clear();
  nt_trackjet_origin.clear();
  nt_trackjet_type.clear();
  nt_trackjet_index_truthbhadron.clear();
  nt_trackjet_orISR.clear();
  nt_trackjet_ntrk.clear();


  const xAOD::TruthParticleContainer* truthBHadrons = nullptr;
  //Retrieve the previously found truth B-Hadrons (see getTruthBHadrons)
  if (m_useHFTruthHadrons && m_isMC){
    CHECK(store->retrieve(truthBHadrons, "TruthBHadrons"));
  }
  
  for(const auto &jet: *m_signalTrackJets){

    nt_trackjet_pt.push_back( jet->pt() );
    nt_trackjet_eta.push_back( jet->eta() );
    nt_trackjet_phi.push_back( jet->phi()  );
    nt_trackjet_e.push_back(  jet->e() );
    nt_trackjet_isbjet.push_back( (int)(jet->auxdata< char >("bjet") == true));
    //nt_trackjet_orISR.push_back( jet->auxdata<int>("passISROR") );
    //nt_trackjet_ntrk.push_back( jet->numConstituents() );
    
        
    //dump the flavour (aka if track b-jet)
    int flav= ( (jet->auxdata< char >("bjet") == true ) && fabs(jet->eta())<2.5 ) ? 5 : 0;
    nt_trackjet_flav.push_back(flav);
    
    int passDR = jet->auxdata<char>("passDRcut") == true ? 1 : 0 ;
    nt_trackjet_passDR.push_back( passDR );
    
    //truth flavour using ConeTruthLabelID
    int flavour = -1;
    if(m_isMC)jet->getAttribute("ConeTruthLabelID",flavour);
    nt_trackjet_truthflav.push_back( flavour );
    
    //This also gives us information about whether a jet is matched to a truth b-hadron - "+5" tells us it is.
    int flavhadexcl = -999;
    if(m_isMC)jet->getAttribute("HadronConeExclExtendedTruthLabelID",flavhadexcl);
    nt_trackjet_truthflavhadexcl.push_back(flavhadexcl);
    
    double MV2=-999;
    xAOD::BTaggingUtilities::getBTagging(*jet)->MVx_discriminant("MV2c10", MV2);
    ATH_MSG_VERBOSE("TRACKSMV2");
    nt_trackjet_MV2c10.push_back( MV2 );
    
    std::string type = "";
    std::string origin = "";
    if(m_useMCTruthClassifier && m_isMC){
      auto result = m_MCtruthClassifier->particleTruthClassifier(jet,true);
      ATH_MSG_VERBOSE("Gote result.");
      MCTruthPartClassifier::ParticleDef partDef;
      origin = partDef.sParticleOrigin[result.second];
      ATH_MSG_VERBOSE("Got origin");
      type = partDef.sParticleType[result.first];
      ATH_MSG_VERBOSE("Got type.");
    }
    nt_trackjet_type.push_back(type);
    nt_trackjet_origin.push_back(origin);
    
    if (m_useHFTruthHadrons && m_isMC){
      int truthBHadronIndex_min = matchTruthHadrons(*truthBHadrons, jet->p4());
      nt_trackjet_index_truthbhadron.push_back(truthBHadronIndex_min);
    }//if use truth b-hadrons
  }
  
  ATH_MSG_VERBOSE("TrackJets were good!");
 


  return true;
}



bool  MasterShef::fillSBVVariables(bool init, TTree *tree){
  ATH_MSG_VERBOSE("in softbtagging fill variables");
  if(init){
    
    if(m_useSBtaggingVertexing == false){
      Error(APP_NAME,"you're trying to fill soft btagging vertexing variables with m_useSBtaggingVertexing==false");
      return false;
    }
    tree->Branch("sbv_loose_Lxy",&nt_sbv_loose_Lxy);
    tree->Branch("sbv_loose_X",&nt_sbv_loose_X);
    tree->Branch("sbv_loose_Y",&nt_sbv_loose_Y);
    tree->Branch("sbv_loose_Z",&nt_sbv_loose_Z);

    tree->Branch("sbv_loose_nTracks", &nt_sbv_loose_nTracks);
    tree->Branch("sbv_loose_Chi2Reduced",&nt_sbv_loose_Chi2Reduced);

    tree->Branch("sbv_loose_m",&nt_sbv_loose_m);
    tree->Branch("sbv_loose_pt",&nt_sbv_loose_pt);
    tree->Branch("sbv_loose_eta",&nt_sbv_loose_eta);
    tree->Branch("sbv_loose_phi",&nt_sbv_loose_phi);

    tree->Branch("nsbv_loose",&nt_nsbv_loose);

    if (m_useHFTruthHadrons){
      tree->Branch("sbv_loose_index_truthbhadron",&nt_sbv_loose_index_truthbhadron);
      tree->Branch("sbv_loose_index_truthchadron",&nt_sbv_loose_index_truthchadron);
    }


    if(m_saveAllSbWPs)
      {
	tree->Branch("sbv_medium_Lxy",&nt_sbv_medium_Lxy);
	tree->Branch("sbv_medium_X",&nt_sbv_medium_X);
	tree->Branch("sbv_medium_Y",&nt_sbv_medium_Y);
	tree->Branch("sbv_medium_Z",&nt_sbv_medium_Z);
      
	tree->Branch("sbv_medium_nTracks", &nt_sbv_medium_nTracks);
	tree->Branch("sbv_medium_Chi2Reduced",&nt_sbv_medium_Chi2Reduced);
      
	tree->Branch("sbv_medium_m",&nt_sbv_medium_m);
	tree->Branch("sbv_medium_pt",&nt_sbv_medium_pt);
	tree->Branch("sbv_medium_eta",&nt_sbv_medium_eta);
	tree->Branch("sbv_medium_phi",&nt_sbv_medium_phi);
      
	tree->Branch("nsbv_medium",&nt_nsbv_medium);
 
	tree->Branch("sbv_tight_Lxy",&nt_sbv_tight_Lxy);
	tree->Branch("sbv_tight_X",&nt_sbv_tight_X);
	tree->Branch("sbv_tight_Y",&nt_sbv_tight_Y);
	tree->Branch("sbv_tight_Z",&nt_sbv_tight_Z);
      
	tree->Branch("sbv_tight_nTracks", &nt_sbv_tight_nTracks);
	tree->Branch("sbv_tight_Chi2Reduced",&nt_sbv_tight_Chi2Reduced);
      
	tree->Branch("sbv_tight_m",&nt_sbv_tight_m);
	tree->Branch("sbv_tight_pt",&nt_sbv_tight_pt);
	tree->Branch("sbv_tight_eta",&nt_sbv_tight_eta);
	tree->Branch("sbv_tight_phi",&nt_sbv_tight_phi);
      
	tree->Branch("nsbv_tight",&nt_nsbv_tight);

	if (m_useHFTruthHadrons){
	  tree->Branch("sbv_tight_index_truthbhadron",&nt_sbv_tight_index_truthbhadron);//mario
	  tree->Branch("sbv_tight_index_truthchadron",&nt_sbv_tight_index_truthchadron);
	  tree->Branch("sbv_medium_index_truthbhadron",&nt_sbv_medium_index_truthbhadron);
	  tree->Branch("sbv_medium_index_truthchadron",&nt_sbv_medium_index_truthchadron);
	} 
      }//end of saving extra WPs
      

    return true;
  }

  //reset the vectors
  nt_sbv_loose_Lxy.clear();
  nt_sbv_loose_X.clear();
  nt_sbv_loose_Y.clear();
  nt_sbv_loose_Z.clear();

  nt_sbv_loose_nTracks.clear();
  nt_sbv_loose_Chi2Reduced.clear();


  nt_sbv_loose_m.clear();
  nt_sbv_loose_pt.clear();
  nt_sbv_loose_eta.clear();
  nt_sbv_loose_phi.clear();

  nt_nsbv_loose = 0;

  nt_sbv_loose_index_truthbhadron.clear();
  nt_sbv_loose_index_truthchadron.clear();
  nt_sbv_medium_index_truthbhadron.clear();
  nt_sbv_medium_index_truthchadron.clear();
  nt_sbv_loose_index_truthbhadron.clear();
  nt_sbv_loose_index_truthchadron.clear();
  
  if(m_saveAllSbWPs)
    {

      nt_sbv_medium_Lxy.clear();
      nt_sbv_medium_X.clear();
      nt_sbv_medium_Y.clear();
      nt_sbv_medium_Z.clear();
      
      nt_sbv_medium_nTracks.clear();
      nt_sbv_medium_Chi2Reduced.clear();
            
      nt_sbv_medium_m.clear();
      nt_sbv_medium_pt.clear();
      nt_sbv_medium_eta.clear();
      nt_sbv_medium_phi.clear();

      nt_nsbv_medium = 0;

      nt_sbv_tight_Lxy.clear();
      nt_sbv_tight_X.clear();
      nt_sbv_tight_Y.clear();
      nt_sbv_tight_Z.clear();
      
      nt_sbv_tight_nTracks.clear();
      nt_sbv_tight_Chi2Reduced.clear();
            
      nt_sbv_tight_m.clear();
      nt_sbv_tight_pt.clear();
      nt_sbv_tight_eta.clear();
      nt_sbv_tight_phi.clear();
      
      nt_nsbv_tight = 0;


    }
  
  //dont both saving sbv info for the 2bjet
  if(m_BJets->size()>=2) return true;  

  const xAOD::TruthParticleContainer* truthBHadrons = nullptr;
  const xAOD::TruthParticleContainer* truthCHadrons = nullptr;

  if (m_useHFTruthHadrons && m_isMC){
    CHECK(store->retrieve(truthBHadrons, "TruthBHadrons"));
    CHECK(store->retrieve(truthCHadrons, "TruthCHadrons"));
  }


  //get some co-oridinates for the primary vertex
  double primVtxX{ 0. }, primVtxY{ 0. }, primVtxZ{ 0. };
  if (m_primVertex->size()>0) {
    primVtxX = m_primVertex->at(0)->x();
    primVtxY = m_primVertex->at(0)->y();
    primVtxZ = m_primVertex->at(0)->z();
  }
  //loop over any vertices found
  for (auto v : *m_goodSBV_Loose) {
    //calculate the Lxy
    double Lxy = sqrt((v->x() - primVtxX) * (v->x() - primVtxX) +
                      (v->y() - primVtxY) * (v->y() - primVtxY));

    TVector3 SVdirection(v->x() - primVtxX,
                         v->y() - primVtxY,
                         v->z() - primVtxZ); //3D vector of the direction of SV with respect to PV. To be used for matching studies.

    //calculate the momenta of the soft b vertex from it's associated tracks 
    TLorentzVector totalFourMomentum;
    for (size_t i = 0; i < v->nTrackParticles(); i++) {
      const xAOD::TrackParticle *trk = v->trackParticle(i);
      totalFourMomentum += trk->p4();
    }
    nt_sbv_loose_Lxy.push_back(Lxy);
    nt_sbv_loose_X.push_back(v->x());
    nt_sbv_loose_Y.push_back(v->y());
    nt_sbv_loose_Z.push_back(v->z());
    nt_sbv_loose_nTracks.push_back(v->nTrackParticles()); 
    nt_sbv_loose_Chi2Reduced.push_back(v->chiSquared()/v->numberDoF());


    nt_sbv_loose_m.push_back(totalFourMomentum.M()/1000.);
    nt_sbv_loose_pt.push_back(totalFourMomentum.Pt()/1000.);
    nt_sbv_loose_eta.push_back(totalFourMomentum.Eta());
    nt_sbv_loose_phi.push_back(totalFourMomentum.Phi());

    //get matching to the bhadrons
    
    if (m_useHFTruthHadrons && m_isMC){
      
      //TLorentzVector p4;

      // int truthBHadronIndex_min = matchTruthHadrons(*truthBHadrons,totalFourMomentum);
      // nt_sbv_index_truthbhadron.push_back(truthBHadronIndex_min);
      // int truthCHadronIndex_min = matchTruthHadrons(*truthCHadrons, totalFourMomentum);
      // nt_sbv_index_truthchadron.push_back(truthCHadronIndex_min);  

      int truthBHadronIndex_min_t = matchTruthHadrons_3D(*truthBHadrons, SVdirection);
      nt_sbv_loose_index_truthbhadron.push_back(truthBHadronIndex_min_t);
      int truthCHadronIndex_min_t = matchTruthHadrons_3D(*truthCHadrons, SVdirection);
      nt_sbv_loose_index_truthchadron.push_back(truthCHadronIndex_min_t);
    }//if use truth b-hadrons



  }
  nt_nsbv_loose = m_goodSBV_Loose->size();


  if(m_saveAllSbWPs)
    {
      nt_sbv_medium_Lxy.clear();
      nt_sbv_medium_X.clear();
      nt_sbv_medium_Y.clear();
      nt_sbv_medium_Z.clear();

      nt_sbv_medium_nTracks.clear();
      nt_sbv_medium_Chi2Reduced.clear();

      nt_sbv_medium_m.clear();
      nt_sbv_medium_pt.clear();
      nt_sbv_medium_eta.clear();
      nt_sbv_medium_phi.clear();

      nt_nsbv_medium = m_goodSBV_Medium->size(); 
     
      for (auto v : *m_goodSBV_Medium) {
	double Lxy = sqrt((v->x() - primVtxX) * (v->x() - primVtxX) +
			  (v->y() - primVtxY) * (v->y() - primVtxY));
	
	TVector3 SVdirection(v->x() - primVtxX,
                             v->y() - primVtxY,
                             v->z() - primVtxZ); //Mario // 3D vector of the direction of SV with respect to PV. To be used for matching studies. 

	//calculate the momenta of the soft b vertex from it's associated tracks
	TLorentzVector totalFourMomentum;
	for (size_t i = 0; i < v->nTrackParticles(); i++) {
	  const xAOD::TrackParticle *trk = v->trackParticle(i);
	  totalFourMomentum += trk->p4();
	}

	nt_sbv_medium_Lxy.push_back(Lxy);
	nt_sbv_medium_X.push_back(v->x());
	nt_sbv_medium_Y.push_back(v->y());
	nt_sbv_medium_Z.push_back(v->z());
	nt_sbv_medium_nTracks.push_back(v->nTrackParticles()); 
	nt_sbv_medium_Chi2Reduced.push_back(v->chiSquared()/v->numberDoF());
	
	nt_sbv_medium_m.push_back(totalFourMomentum.M()/1000.);
	nt_sbv_medium_pt.push_back(totalFourMomentum.Pt()/1000.);
	nt_sbv_medium_eta.push_back(totalFourMomentum.Eta());
	nt_sbv_medium_phi.push_back(totalFourMomentum.Phi());

	if (m_useHFTruthHadrons && m_isMC){
	  int truthBHadronIndex_min_m = matchTruthHadrons_3D(*truthBHadrons, SVdirection);
	  nt_sbv_medium_index_truthbhadron.push_back(truthBHadronIndex_min_m);
	  int truthCHadronIndex_min_m = matchTruthHadrons_3D(*truthCHadrons, SVdirection);
	  nt_sbv_medium_index_truthchadron.push_back(truthCHadronIndex_min_m);
        }

      }

      nt_sbv_tight_Lxy.clear();
      nt_sbv_tight_X.clear();
      nt_sbv_tight_Y.clear();
      nt_sbv_tight_Z.clear();

      nt_sbv_tight_nTracks.clear();
      nt_sbv_tight_Chi2Reduced.clear();

      nt_sbv_tight_m.clear();
      nt_sbv_tight_pt.clear();
      nt_sbv_tight_eta.clear();
      nt_sbv_tight_phi.clear();      
      
      nt_nsbv_tight = m_goodSBV_Tight->size(); 

      for (auto v : *m_goodSBV_Tight) {
	double Lxy = sqrt((v->x() - primVtxX) * (v->x() - primVtxX) +
			  (v->y() - primVtxY) * (v->y() - primVtxY));

	TVector3 SVdirection(v->x() - primVtxX,
                             v->y() - primVtxY,
                             v->z() - primVtxZ);//Mario // 3D vector of the direction of SV with respect to PV. To be used for matching studies.  

	//calculate the momenta of the soft b vertex from it's associated tracks 
	TLorentzVector totalFourMomentum;
	for (size_t i = 0; i < v->nTrackParticles(); i++) {
	  const xAOD::TrackParticle *trk = v->trackParticle(i);
	  totalFourMomentum += trk->p4();
	}

	nt_sbv_tight_Lxy.push_back(Lxy);
	nt_sbv_tight_X.push_back(v->x());
	nt_sbv_tight_Y.push_back(v->y());
	nt_sbv_tight_Z.push_back(v->z());
	nt_sbv_tight_nTracks.push_back(v->nTrackParticles()); 
	nt_sbv_tight_Chi2Reduced.push_back(v->chiSquared()/v->numberDoF());
	
	nt_sbv_tight_m.push_back(totalFourMomentum.M()/1000.);
	nt_sbv_tight_pt.push_back(totalFourMomentum.Pt()/1000.);
	nt_sbv_tight_eta.push_back(totalFourMomentum.Eta());
	nt_sbv_tight_phi.push_back(totalFourMomentum.Phi());

	if (m_useHFTruthHadrons && m_isMC){
	  int truthBHadronIndex_min_l = matchTruthHadrons_3D(*truthBHadrons, SVdirection);
	  nt_sbv_tight_index_truthbhadron.push_back(truthBHadronIndex_min_l);
	  int truthCHadronIndex_min_l = matchTruthHadrons_3D(*truthCHadrons, SVdirection);
	  nt_sbv_tight_index_truthchadron.push_back(truthCHadronIndex_min_l);
	}

      }

      
     

    }
  
  return true;
}

bool  MasterShef::fillSmearingVariables(bool init, TTree *tree){
    
  ATH_MSG_VERBOSE("in smearing variables");
 

  if(init){
    // Branches: smrJets
    tree->Branch("nsmrJets",&nt_nsmrJets);
    tree->Branch("smrjet_index",&nt_smrjet_index);
    //tree->Branch("smrjet_R",&nt_smrjet_R);
    //tree->Branch("smrjet_p4",&nt_smrjet_p4);
    tree->Branch("smrjet_pt",&nt_smrjet_pt);
    tree->Branch("smrjet_eta",&nt_smrjet_eta);
    tree->Branch("smrjet_phi",&nt_smrjet_phi);
    tree->Branch("smrjet_e",&nt_smrjet_e);
    tree->Branch("smrjet_MV2",&nt_smrjet_MV2);
    tree->Branch("smrjet_flav",&nt_smrjet_flav);
    tree->Branch("smrMET_pt",&nt_smrMET_pt);
    tree->Branch("smrMET_phi",&nt_smrMET_phi);
    tree->Branch("smrmetsig",&nt_smrmetsig);

    tree->Branch("tailWeights",&nt_tailWeights);


    //tree->Branch("passSeedSelection",&nt_passSeedSelection);
    //tree->Branch("passNewSeedSelection",&nt_passNewSeedSelection);
    

    //tree->Branch("smrsumet",&nt_smrsumet);
    
    tree->Branch("smrrcjet_kt8_index",&nt_smrrcjet_kt8_index);
    tree->Branch("smrrcjet_kt8_pt",&nt_smrrcjet_kt8_pt);
    tree->Branch("smrrcjet_kt8_eta",&nt_smrrcjet_kt8_eta);
    tree->Branch("smrrcjet_kt8_phi",&nt_smrrcjet_kt8_phi);
    tree->Branch("smrrcjet_kt8_e",&nt_smrrcjet_kt8_e);
    
    tree->Branch("smrrcjet_kt12_index",&nt_smrrcjet_kt12_index);
    tree->Branch("smrrcjet_kt12_pt",&nt_smrrcjet_kt12_pt);
    tree->Branch("smrrcjet_kt12_eta",&nt_smrrcjet_kt12_eta);
    tree->Branch("smrrcjet_kt12_phi",&nt_smrrcjet_kt12_phi);
    tree->Branch("smrrcjet_kt12_e",&nt_smrrcjet_kt12_e);
    /*
      tree->Branch("passSeedSelection",&nt_passSeedSelection);
      //tree->Branch("passHTSeedSelection",&nt_passHTSeedSelection);
      tree->Branch("passSeedSelectionQRT",&nt_passSeedSelectionQRT);
      tree->Branch("passSeedSelectionCU",&nt_passSeedSelectionCU);
    */
    return true;
  }
  

  nt_passSeedSelection=m_passSeedSelection;
  nt_passSeedSelectionQRT=m_passSeedSelectionQRT;
  nt_passSeedSelectionCU=m_passSeedSelectionCU;

  nt_smrjet_index.clear();
  nt_smrjet_R.clear();
  nt_smrjet_pt.clear();
  nt_smrjet_eta.clear();
  nt_smrjet_phi.clear();
  nt_smrjet_e.clear();
  nt_smrjet_MV2.clear();
  nt_smrjet_flav.clear();
  nt_nsmrJets.clear();
  
  nt_smrrcjet_kt8_index.clear();
  nt_smrrcjet_kt8_pt.clear();
  nt_smrrcjet_kt8_eta.clear();
  nt_smrrcjet_kt8_phi.clear();
  nt_smrrcjet_kt8_e.clear();
  
  nt_smrrcjet_kt12_index.clear();
  nt_smrrcjet_kt12_pt.clear();
  nt_smrrcjet_kt12_eta.clear();
  nt_smrrcjet_kt12_phi.clear();
  nt_smrrcjet_kt12_e.clear();
  
  nt_smrMET_pt.clear();
  nt_smrMET_phi.clear();
  //nt_smrsumet.clear();

  nt_smrmetsig.clear();


  nt_tailWeights.clear();

  

  if(!m_doJetSmearing){
    Error(APP_NAME," you are trying to fill smearing ntuples without performing jet smearing??");
    return false;
  }


  for(unsigned int i=0; i < m_smrMET.size(); i++)
    {

      bool isnull =  m_smrMET[i] == nullptr;
      if (isnull)continue;


      xAOD::MissingETContainer::const_iterator smet_it = m_smrMET[i]->find("Final");
      double met = (*smet_it)->met()/1000.;

      //if we want to skim of pseudo-events with met<200
      if(m_doJetSmearingSkim && met<250)continue;


      nt_smrMET_pt.push_back(met);
      double met_phi = (*smet_it)->phi();
      nt_smrMET_phi.push_back(met_phi);
      
      xAOD::JetContainer* allsmrJets = m_smrJets[i];
      
      if(m_ExtraSysMaps || m_TailWeightMaps){

	std::vector<double> TailWeights_thisSmr = m_TailWeights[i];


	for(const auto &tweight: TailWeights_thisSmr){
	  ATH_MSG_VERBOSE("For smear: "<<i+1<<" TailWeight: "<<tweight);
	  nt_tailWeights.push_back(tweight);
	}

	// for future: add in b-jet specific tailweights - requires some trickery to get the branches the same size

      }

      xAOD::JetContainer* goodsmrJets = new xAOD::JetContainer();
      xAOD::JetAuxContainer* goodsmrJets_aux = new xAOD::JetAuxContainer();
      goodsmrJets->setStore( goodsmrJets_aux);
    

      int index =0;
      for(const auto &jet: *allsmrJets){
	bool issmear=jet->auxdata<char>("smearjet");
	double pt = jet->pt()/1000.;
	
	if(pt>20. && issmear){
	  
	  xAOD::Jet *myJet = new xAOD::Jet((*jet));
	  goodsmrJets->push_back(myJet);
 
	  nt_smrjet_pt.push_back( jet->pt() );
	  nt_smrjet_eta.push_back( jet->eta() );
	  nt_smrjet_phi.push_back( jet->phi()  );
	  nt_smrjet_e.push_back(  jet->e() );
	  double weight_mv2c20 = 0;
	  xAOD::BTaggingUtilities::getBTagging(*jet)->MVx_discriminant("MV2c10", weight_mv2c20);
	  nt_smrjet_MV2.push_back( weight_mv2c20 );

	  int flav= ( ( jet->isAvailable<char>("bjet") && jet->auxdata< char >("bjet") == true ) && fabs(jet->eta())<2.5 ) ? 5 : 0;
	  //if we're using pseudocontinious btagging, instead save the bin value
	  if(m_contWP!=0){
	    flav = jet->isAvailable<char>("BtagDefault") && jet->auxdata< int >("BtagDefault");
	  }
	  nt_smrjet_flav.push_back(flav);
	  nt_smrjet_index.push_back(index);
	  index++;
	}
      }
    
      CHECK(store->record(goodsmrJets,"GoodSMRJets"+std::to_string(i)));
      CHECK(store->record(goodsmrJets_aux,"GoodSMRJets"+std::to_string(i)+"Aux."));

      //metsig
      double metsig=0;
      EL_RETURN_CHECK("GetMetSig()",m_objTool->GetMETSig(*m_smrMET[i],
							 metsig,
							 true, // TST
							 true));// doJVT
     
      nt_smrmetsig.push_back(metsig);

    
      //if we're using fat jets.... recluster 
      if(m_useAntiKtRcJets)
	{
	  std::string smr_name = "SMR"+std::to_string(i);
	  m_jetRecTool_smrkt8[smr_name]->execute();
	  m_jetRecTool_smrkt12[smr_name]->execute();
	  

	  CHECK(event->retrieve( m_smrrcjets_kt8,"MySMRFatJetsKt8"+std::to_string(i) ));
	  CHECK(event->retrieve( m_smrrcjets_kt12,"MySMRFatJetsKt12"+std::to_string(i) ));

	  m_smrrcjets_kt8->sort(ptsorter);
	  m_smrrcjets_kt12->sort(ptsorter);

	  int index =0;
	  for(const auto &fatjet: *m_smrrcjets_kt8){
	    nt_smrrcjet_kt8_pt.push_back( fatjet->pt() );
	    nt_smrrcjet_kt8_eta.push_back( fatjet->eta() );
	    nt_smrrcjet_kt8_phi.push_back( fatjet->phi()  );
	    nt_smrrcjet_kt8_e.push_back(  fatjet->e() );
	    nt_smrrcjet_kt8_index.push_back(index);
	    index++;
	  }
	  index=0;
	  for(const auto &fatjet: *m_smrrcjets_kt12){
	    nt_smrrcjet_kt12_pt.push_back( fatjet->pt() );
	    nt_smrrcjet_kt12_eta.push_back( fatjet->eta() );
	    nt_smrrcjet_kt12_phi.push_back( fatjet->phi()  );
	    nt_smrrcjet_kt12_e.push_back(  fatjet->e() );
	    nt_smrrcjet_kt12_index.push_back(index);
	    index++;
	  }
	  
	}
    }



  // xAOD::JetContainer* goodsmrJets = 0;
  // xAOD::JetAuxContainer* goodsmrJets_aux = 0;
  // CHECK(store->retrieve(goodsmrJets,"GoodSMRJets"));
  // CHECK(store->retrieve(goodsmrJets_aux,"GoodSMRJetsAux."));



  // for (unsigned int i=0;i<goodsmrJets->size();i++) {	  
  //   double R =  goodsmrJets->at(i)->pt()/goodsmrJets->at(i)->auxdata< double >("origPt");
  //   nt_smrjet_R.push_back(R);
  //   nt_smrjet_pt.push_back( goodsmrJets->at(i)->pt() );
  //   nt_smrjet_eta.push_back( goodsmrJets->at(i)->eta() );
  //   nt_smrjet_phi.push_back( goodsmrJets->at(i)->phi()  );
  //   nt_smrjet_e.push_back(  goodsmrJets->at(i)->e() );
  //   double weight_mv2c20 = 0;
  //   goodsmrJets->at(i)->btagging()->MVx_discriminant("MV2c10", weight_mv2c20);
  //   nt_smrjet_MV2.push_back( weight_mv2c20 );
  //   if (m_objTool->IsBJet(*(goodsmrJets->at(i)) ) ) 
  //     nt_smrjet_flav.push_back(5);
  //   else
  //     nt_smrjet_flav.push_back(0);
  //   nt_smrjet_index.push_back(i);
  // }
  // nt_nsmrJets.push_back(goodsmrJets->size());
  
  
  // nt_smrMET_pt.push_back(m_smrMetVec.Mod());
  // nt_smrMET_phi.push_back(m_smrMetVec.Phi());
  // nt_smrsumet.push_back(m_SumEtSMR);



  return true;
}
      



bool MasterShef::fillxAODJets(bool init, TTree *tree){

  if(init){
    Info(APP_NAME,"Setup to fill xAOD jets!!");
    Info(APP_NAME,"Current Tree=%llu",tree->GetEntries());
    return true;
  }

  ATH_MSG_VERBOSE("in fillxAODJets");
  std::string sysName = m_systNameList.at(m_systNum);
  bool isNominal(true);
  if (sysName != "") isNominal=false;
  if(m_doxAOD && isNominal)
    {
      ATH_MSG_VERBOSE("Saving Jet Information");
      m_jets_copyaux->setShallowIO(false);
      //dump track information to the output ntuple
      //std::cout << event->dump() << std::endl;
      //store->print();
      //abort();
      //store->print();

      CHECK(event->copy("egammaClusters"));
      CHECK(event->copy("egammaClustersAux."));
      //CHECK(event->copy("egammaTopoSeededClusters"));
      //CHECK(event->copy("egammaTopoSeededClustersAux."));
      CHECK(event->copy("CaloCalTopoClusters"));
      CHECK(event->copy("CaloCalTopoClustersAux."));
      

      //CHECK(event->copy("GSFTrackParticles"));
      //CHECK(event->copy("GSFTrackParticlesAux."));
      CHECK(event->copy("InDetTrackParticles"));
      CHECK(event->copy("InDetTrackParticlesAux."));
      //write our calibrated jets
      CHECK(event->copy("BTagging_AntiKt4EMTopo"));
      CHECK(event->record(m_jets_copy, "STCalibAntiKt4EMPFlowJets"));
      CHECK(event->record(m_jets_copyaux, "STCalibAntiKt4EMPFlowJetsAux."));

      if(m_useAntiKt10Jets_ST){
	CHECK(event->record(m_fatjets_copy, "STCalibAntiKt10LCTopoTrimmedPtFrac5SmallR20Jets"));
	CHECK(event->record(m_fatjets_copyaux, "STCalibAntiKt10LCTopoTrimmedPtFrac5SmallR20JetsAux."));
      }
      event->fill();
      // abort();
    }
  ATH_MSG_VERBOSE("done in fillxAODJets");
  return true;
}

int MasterShef::matchTruthHadrons(const xAOD::TruthParticleContainer& truthHadrons, TLorentzVector thePart){
   //initialise to maximum dR we allow
  double minDR = 0.3;
  int truthHadronIndex_min = -1, truthHadronIndex = 0;

  //Now loop over the previously found truth B-Hadrons (see getTruthHadrons)
  //and then fill the ntuple values
  for (auto* truthHadron : truthHadrons){
    if (truthHadron){
      double dR = truthHadron->p4().DeltaR(thePart);
      if (dR < minDR){
	minDR = dR;
	truthHadronIndex_min = truthHadronIndex;
      }
    }
    else ATH_MSG_ERROR("Invalid pointer to truth b-hadron in jet loop");
    truthHadronIndex++;
  }//truth b-hadron loop      
  return truthHadronIndex_min;
}

int MasterShef::matchTruthHadrons_3D(const xAOD::TruthParticleContainer& truthHadrons, TVector3 SVdirection)
{

  //initialise to maximum dR we allow
                                                                                                                                                                               
  double minDR = 0.3;
  int truthHadronIndex_min = -1, truthHadronIndex = 0;

  //Now loop over the previously found truth B-Hadrons (see getTruthHadrons)                                                                                                                                       
  //and then fill the ntuple values
                                                                                                                                                                                 
  for (auto* truthHadron : truthHadrons){
    if (truthHadron){
      TVector3 THad_3d;
      THad_3d.SetPtEtaPhi(truthHadron->pt(), truthHadron->eta(), truthHadron->phi());
      double dR = THad_3d.DeltaR(SVdirection);
      if (dR < minDR){
        minDR = dR;
        truthHadronIndex_min = truthHadronIndex;
      }
      THad_3d.Clear();
    }
    else ATH_MSG_ERROR("Invalid pointer to truth b-hadron in jet loop");
    truthHadronIndex++;

  }//truth b-hadron loop
  return truthHadronIndex_min;
}
