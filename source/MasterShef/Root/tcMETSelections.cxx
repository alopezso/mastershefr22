#include <MasterShef/MasterShef.h>
//#include "CPAnalysisExamples/errorcheck.h"

static const char* APP_NAME = "MasterShef::tcMETSelections";
static SG::AuxElement::ConstAccessor<float> acc_GenFiltMET("GenFiltMET"); //!                                                                                                                              

bool MasterShef :: SeltcMET(bool init) {
  if (init) {
    m_cuts[m_cutLevel++]="(tcMET loose selection)";
    Info(APP_NAME,"Initialising tcMET analysis Selection selector.... ");
    return true;
  }
  if (m_doTruth){
    passCut();
    return true;
  }

  /// ************************
  /// Definition of the main variables
  /// ************************                                                                                                                                                                                      
  unsigned int nEl=m_signalElectrons->size();
  unsigned int nMu=m_signalMuons->size();
  unsigned int nLep=nEl+nMu;
  unsigned int nB=m_BJets->size();
  unsigned int nJets=m_signalJets->size();
  unsigned int nBaselineEl=m_baselineElectrons->size();
  unsigned int nBaselineMu=m_baselineMuons->size();
  unsigned int nBaselineLep=nBaselineEl+nBaselineMu;

  std::vector<TLorentzVector> leptons;
  leptons.clear();
  std::vector<TLorentzVector> jetLeps;
  jetLeps.clear();
  for( auto electron : *m_signalElectrons ){
    TLorentzVector fourmom ;
    fourmom.SetPtEtaPhiE(electron->pt(),electron->eta(),electron->phi(),electron->e());
    leptons.push_back(fourmom);
    jetLeps.push_back(fourmom);
  }
  for( auto muon : *m_signalMuons ){
    TLorentzVector fourmom ;
    fourmom.SetPtEtaPhiE(muon->pt(),muon->eta(),muon->phi(),muon->e());
    leptons.push_back(fourmom);
    jetLeps.push_back(fourmom);
  }
  for( auto jet : *m_signalJets ){
    TLorentzVector fourmom ;
    fourmom.SetPtEtaPhiE(jet->pt(),jet->eta(),jet->phi(),jet->e());
    jetLeps.push_back(fourmom);
  }

  std::sort(leptons.begin(),leptons.end(),ptfloatsorter);
  std::sort(jetLeps.begin(),jetLeps.end(),ptfloatsorter);


  //nominal ttbar and single top, met slice overlap                                                                                                                                                         
  if(m_isMC){
    int dsid= eventInfo->mcChannelNumber();
    // ttbar            tW(DR)            t*W(DR)           tW(DS)         t*W(DS)
    if(dsid==410470 || dsid==410646 || dsid==410647 || dsid==410654 || dsid==410655 ){
      if(acc_GenFiltMET.isAvailable(*eventInfo)){
        float GenFiltMET = acc_GenFiltMET(*eventInfo)/1000.;
        if(GenFiltMET > 200){
	  failCut();
	  return false;
	}
      }
    }
  }

  /// ************************
  /// Selection cuts
  /// ************************
  
  // Loose selection cut

  // General selection
  if( nBaselineLep == 0 && nLep == 0 && nJets>=2 && m_etmiss/1000.>=250.0 && nB>=1 && m_signalJets->at(0)->pt()/1000. >= 50.)
    {
      passCut();
      return true;
    }
  else if( nBaselineLep == 1 && nLep == 1 &&  nJets>=2 && m_etmiss/1000.>=250.0 && nB>=1 && m_signalJets->at(0)->pt()/1000. >= 50.)
    {
      passCut();
      return true;
    }
  else if( nBaselineLep == 2 && nLep == 2 && m_etmiss_prime/1000. >= 100 && nB >=1 && nJets >=2 ) {
    if( nEl==2||nMu==2  ){ //CRZ                                                                                                             
      
      int charge_1lep=-1;
      int charge_2lep=-1;
      
      if(nMu==2){
        charge_1lep=m_signalMuons->at(0)->charge();
        charge_2lep=m_signalMuons->at(1)->charge();
      } else if (nEl==2){
        charge_1lep=m_signalElectrons->at(0)->charge();
        charge_2lep=m_signalElectrons->at(1)->charge();
      }
      
      if (charge_1lep+charge_2lep==0 ){
        passCut();
        return true;
      }
      
    }
  }
  else if( nBaselineLep == 3 && nLep == 3 &&  nJets>=2 && nB>=1 && m_signalJets->at(0)->pt()/1000. >= 50.) // ttZ
    {
      passCut();
      return true;
    }

  failCut();
  return false;
}
bool MasterShef :: SeltcMETTight(bool init) {
  if (init) {
    m_cuts[m_cutLevel++]="(tcMET tight selection)";
    Info(APP_NAME,"Initialising tcMET analysis Selection selector.... ");
    return true;
  }
  if (m_doTruth){
    passCut();
    return true;
  }

  /// ************************
  /// Definition of the main variables
  /// ************************                                                                                                                                                                                      
  unsigned int nEl=m_signalElectrons->size();
  unsigned int nMu=m_signalMuons->size();
  unsigned int nLep=nEl+nMu;
  unsigned int nB=m_BJets->size();
  unsigned int nC=m_CJets->size();
  unsigned int nJets=m_signalJets->size();
  unsigned int nBaselineEl=m_baselineElectrons->size();
  unsigned int nBaselineMu=m_baselineMuons->size();
  unsigned int nBaselineLep=nBaselineEl+nBaselineMu;

  std::vector<TLorentzVector> leptons;
  leptons.clear();
  std::vector<TLorentzVector> jetLeps;
  jetLeps.clear();
  for( auto electron : *m_signalElectrons ){
    TLorentzVector fourmom ;
    fourmom.SetPtEtaPhiE(electron->pt(),electron->eta(),electron->phi(),electron->e());
    leptons.push_back(fourmom);
    jetLeps.push_back(fourmom);
  }
  for( auto muon : *m_signalMuons ){
    TLorentzVector fourmom ;
    fourmom.SetPtEtaPhiE(muon->pt(),muon->eta(),muon->phi(),muon->e());
    leptons.push_back(fourmom);
    jetLeps.push_back(fourmom);
  }
  for( auto jet : *m_signalJets ){
    TLorentzVector fourmom ;
    fourmom.SetPtEtaPhiE(jet->pt(),jet->eta(),jet->phi(),jet->e());
    jetLeps.push_back(fourmom);
  }

  std::sort(leptons.begin(),leptons.end(),ptfloatsorter);
  std::sort(jetLeps.begin(),jetLeps.end(),ptfloatsorter);


  /// ************************
  /// Selection cuts
  /// ************************

  // Adding charm tagging as the main variable to skim the systematics.
  if( nC >= 1 ){

    double MTcMin = CalcMTcmin();
    double MTbMin = CalcMTbmin();

    if(nBaselineLep == 0 && MTcMin >=100.0 && ( MTbMin >= 200 || m_signalJets->at(0)->pt() > 100.0) ){ // All signal regions require to have a large MTbMin or (SRC) a large pt jet.
      passCut();
      return true;
    }else if( nLep == 1 && ((MTcMin >=200.0 && MTbMin >= 200) || (MTcMin >=100.0 && m_signalJets->at(0)->pt() > 100.0)) ){ // CRst for SRAB or CRs for SRC
      passCut();
      return true;
    }else if( nLep == 2 ){
      TLorentzVector lep1 = nMu == 2 ? m_signalMuons->at(0)->p4() : m_signalElectrons->at(0)->p4() ;
      TLorentzVector lep2 = nMu == 2 ? m_signalMuons->at(1)->p4() : m_signalElectrons->at(1)->p4() ;
      if( (lep1 + lep2).M()/1000. < 110. && (lep1 + lep2).M()/1000. > 70. ){
	// Just cut on Z mass for the 2L.
	passCut();
	return true;
      }
    }else if( nLep == 3 ){
      passCut();
      return true;
    }
    
  }
  
  failCut();
  return false;
}
