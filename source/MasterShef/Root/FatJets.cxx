#include <MasterShef/MasterShef.h>
//#include "CPAnalysisExamples/errorcheck.h"


static const char* APP_NAME = "MasterShef::FatJets";



//decorate rc jets with a b-flavour
// 0 == no b-jets matched
// 5 == 1 b-jet matched
//10 == 2 b-jets matched etc.
void MasterShef::decorateRCFlav(xAOD::JetContainer* rcjets, double radius){
  for(const auto &jet: *rcjets){
    int flav=0;
    for(const auto &bjet: *m_BJets){
      double dR = bjet->p4().DeltaR(jet->p4());
      if(dR<radius){
	flav+=5;
      }
    }
    jet->auxdata<int>("flav")=flav;
  }
}
  	
void MasterShef :: calcConstituentsMass(xAOD::Jet* jet){

  xAOD::JetConstituentVector jetConst=jet->getConstituents();
  int n_cont=0;
  TLorentzVector mass_p4(0,0,0,0);
  for (const xAOD::JetConstituent* constituent: jetConst ){
    if (!constituent)
      continue;
    TLorentzVector tmp_jet;
    tmp_jet.SetPtEtaPhiE(constituent->pt(),constituent->eta(),constituent->phi(),constituent->e());
    mass_p4+=tmp_jet;
    ++n_cont;
  }

  if(n_cont == 0 )
    ATH_MSG_VERBOSE("Jet didn't have any constituent. Mass is zero.");

  jet->auxdata<float>("JetConstMass")=mass_p4.M();

  return;
}
void MasterShef :: calcTrackAssistedMass(xAOD::Jet* jet){

  xAOD::JetConstituentVector jetConst=jet->getConstituents();

  int n_cont=0;
  TLorentzVector mass_p4(0,0,0,0);

  std::vector<const xAOD::JetConstituent*> constituents;

  for (const xAOD::JetConstituent* constituent: jetConst ){
    if (!constituent)
      continue;

    if(constituent->type() == 2 ){

      const xAOD::Jet* subjet=(const xAOD::Jet*)constituent->rawConstituent();
      xAOD::JetConstituentVector jetconst2=subjet->getConstituents();

      for(const xAOD::JetConstituent* jet_cn : jetconst2)
        constituents.push_back(jet_cn);

    }
  }

  double ratio=jet->pt();
  TVector2 total_pt(0,0);

  for (const xAOD::JetConstituent* jet_cn : constituents){
    if( !jet_cn )
      continue;
    if(jet_cn->type() == 4)
      total_pt+=TVector2(jet_cn->px(),jet_cn->py());
  }

  ratio/=total_pt.Mod();

  for (const xAOD::JetConstituent* constituent: constituents ){
    if (!constituent)
      continue;
    TLorentzVector momentum;
    momentum.SetPtEtaPhiE(constituent->pt(),constituent->eta(),constituent->phi(),constituent->e());

    if(constituent->type() != 4)
      mass_p4+=momentum;
    else{
      momentum.SetPtEtaPhiE(constituent->pt()*ratio,constituent->eta(),constituent->phi(),constituent->e()*ratio);
      mass_p4+=momentum;
    }
    ++n_cont;
  }
  if(n_cont == 0 )
    ATH_MSG_VERBOSE("Jet didn't have any constituent. Mass is zero.");

  jet->auxdata<float>("JetTrkMass")=mass_p4.M();

  return;
}
void MasterShef :: calcMassPFlowJet(xAOD::Jet* jet){

  std::vector<const xAOD::JetConstituent*> constituents;

  for (const xAOD::JetConstituent* constituent: jet->getConstituents() ){
    if (!constituent)
      continue;

    if(constituent->type() == 2 ){

      const xAOD::Jet* subjet=(const xAOD::Jet*)constituent->rawConstituent();
      xAOD::JetConstituentVector jetconst2=subjet->getConstituents();

      for(const xAOD::JetConstituent* jet_cn : jetconst2)
        constituents.push_back(jet_cn);

    }
  }

  TFormula tracker_resol("track_resol","0.00036*x*x+1.3*x");
  TFormula calo_resol("calo_resol","0.101*TMath::Sqrt(x)+200+0.0017*x");

  TLorentzVector fourmonvector;
  //  std::vector<TLorentzVector> fourmonvector;

  std::cout << constituents.size() << std::endl;

  for ( auto constituent: constituents){
    if (!constituent || !constituent->rawConstituent())
      continue;

    xAOD::Type::ObjectType const_type = constituent->type();
    if ( const_type != 2 )
      continue;

    const xAOD::PFO* mypfo = dynamic_cast<const xAOD::PFO*>(constituent->rawConstituent());

    if(!mypfo || mypfo == nullptr ){
      std::cout << "Raw constituent cannot be converted to PFO object." <<std::endl;
    }
    if( fabs(mypfo->charge()) > 0.001 ){
      TLorentzVector track = mypfo->track(0) ?  mypfo->track(0)->p4() : TLorentzVector(0,0,0,0);
      TLorentzVector calo = mypfo->cluster(0) ?  mypfo->cluster(0)->p4() : TLorentzVector(0,0,0,0);
      double sigma_track=TMath::Power(tracker_resol.Eval(track.Pt()),-1);
      double sigma_calo=TMath::Power(calo_resol.Eval(calo.E()),-1);
      fourmonvector +=  track*(sigma_track/(sigma_track+sigma_calo))+calo*(sigma_calo/(sigma_track+sigma_calo));
    }else{
      fourmonvector += mypfo->p4();
    }

  }

  jet->auxdata<float>("JetPFlowMass")=fourmonvector.M();


}






// ---------------------------------------------------------------
// Function to prepare baseline fat jets for analysis
// currently no OLR procedure applied to these
// can we b-tag these also in some way?
// ---------------------------------------------------------------
bool MasterShef :: prepareBaselineFatJetsBeforeOR() {

  m_fatjets_copy = 0;
  m_fatjets_copyaux = 0;
  
  if(m_useAntiKt10Jets_ST){// only needed to get in case of using fatjets from SUSYTools, otherwise don't bother retrieving R=1.0 jets.
    //EL_RETURN_CHECK("GetFatJets()",m_objTool->GetFatJets(m_fatjets_copy,m_fatjets_copyaux));
    EL_RETURN_CHECK("GetFatJets()",m_objTool->GetFatJets(m_fatjets_copy,m_fatjets_copyaux,true,"",true));
  }
  
  return true;
}
  

// ---------------------------------------------------------------
// Function to prepare reclustered jets for analysis
// ---------------------------------------------------------------
bool MasterShef :: prepareRCJets() {

  
  //_______________________________________________
  // Get Fat Reclustered jets!!!
  //_______________________________________________
  //

  m_rcjets_kt8 = 0;
  m_rcjets_kt10 = 0;
  m_rcjets_kt12 = 0;
  m_rcjets_kt15 = 0;

  if(m_useAntiKtRcJets){
    m_jetRecTool_kt8->execute();
    m_jetRecTool_kt10->execute();
    m_jetRecTool_kt12->execute();
    m_jetRecTool_kt15->execute();

    CHECK(event->retrieve( m_rcjets_kt8,"MyFatJetsKt8" ));
    CHECK(event->retrieve( m_rcjets_kt10,"MyFatJetsKt10" ));
    CHECK(event->retrieve( m_rcjets_kt12,"MyFatJetsKt12" ));
    CHECK(event->retrieve( m_rcjets_kt15,"MyFatJetsKt15" ));

    decorateRCFlav(m_rcjets_kt15,1.5);
    decorateRCFlav(m_rcjets_kt12,1.2);
    decorateRCFlav(m_rcjets_kt10,1.0);
    decorateRCFlav(m_rcjets_kt8,0.8);


    m_rcjets_kt8->sort(ptsorter);
    m_rcjets_kt10->sort(ptsorter);
    m_rcjets_kt12->sort(ptsorter);
    m_rcjets_kt15->sort(ptsorter);
    

   



    for (xAOD::Jet* jet:*m_rcjets_kt8){

      if(!m_useRCJetSS) continue;
      
      nsubjetinessTool->modifyJet(*jet);
      nsubjetinessRatiosTool->modifyJet(*jet);
      dipTool->modifyJet(*jet);
      splitTool->modifyJet(*jet);
      qwTool->modifyJet(*jet);
      jchargeTool->modifyJet(*jet);
      calcConstituentsMass(jet);
      
      if(m_useJetConstituents)
	{
	  calcTrackAssistedMass(jet);
	  calcMassPFlowJet(jet);
	}
    }
    
    for (xAOD::Jet* jet:*m_rcjets_kt12){
      if(!m_useRCJetSS) continue;

      nsubjetinessTool->modifyJet(*jet);
      nsubjetinessRatiosTool->modifyJet(*jet);
      dipTool->modifyJet(*jet);
      splitTool->modifyJet(*jet);
      qwTool->modifyJet(*jet);
      jchargeTool->modifyJet(*jet);
      calcConstituentsMass(jet);
      
      if(m_useJetConstituents)
	{
	  calcTrackAssistedMass(jet);
	  calcMassPFlowJet(jet);
	}
      
    }
       
  }
  
  return true;
}








bool MasterShef :: GetJetToolRunner(JetToolRunner *& tool, double jetradius, std::string inputcontainer, std::string outputcontainer)
{
 
  
  /*
  Info( APP_NAME,"Setting up JetReclusteringTool for %s",outputcontainer.c_str());

  tool = 0;

  ToolHandleArray<IPseudoJetGetter> hgets;
  ToolHandleArray<IJetExecuteTool> hrecs;

  PseudoJetGetter* plcget = new PseudoJetGetter(("mylcget"+outputcontainer).c_str());
  CHECK(plcget->setProperty("InputContainer", inputcontainer));
  CHECK(plcget->setProperty("OutputContainer", "Reclustered"+outputcontainer));
  CHECK(plcget->setProperty("Label", "Tower"));
  CHECK(plcget->setProperty("SkipNegativeEnergy", true));
  CHECK(plcget->setProperty("GhostScale", 0.0));
  Info( APP_NAME,"finished properties PseudoJetGetter %s",outputcontainer.c_str());
  //CHECK(asg::ToolStore::put(plcget));

  if(m_debug==2)plcget->msg().setLevel(MSG::VERBOSE);

  ToolHandle<IPseudoJetGetter> hlcget(plcget);
  hgets.push_back(hlcget);
 

  Info( APP_NAME,"Setup PseudoJetGetter");

  JetFromPseudojet* pbuild = new JetFromPseudojet(("myjetbuild"+outputcontainer).c_str());
  ToolHandle<IJetFromPseudojet> hbuild(pbuild);
  
  
  if(m_debug==2)pbuild->msg().setLevel(MSG::VERBOSE);

  CHECK(pbuild->initialize());
  
  JetFinder* pfind = new JetFinder(("myjetfind"+outputcontainer).c_str());
  CHECK(pfind->setProperty("JetAlgorithm", "AntiKt"));
  CHECK(pfind->setProperty("JetRadius", jetradius));
  CHECK(pfind->setProperty("PtMin", 15000.0));
  CHECK(pfind->setProperty("GhostArea", 0.00));
  CHECK(pfind->setProperty("RandomOption", 1));
  CHECK(pfind->setProperty("JetBuilder", hbuild));
  ToolHandle<IJetFinder> hfind(pfind);
  CHECK(pfind->initialize());
  
  Info( APP_NAME,"Setup JetFinder");

  JetRecTool* pjrf = new JetRecTool(("myjrfind"+outputcontainer).c_str());
  CHECK(pjrf->setProperty("OutputContainer", outputcontainer));
  CHECK(pjrf->setProperty("PseudoJetGetters", hgets));
  CHECK(pjrf->setProperty("JetFinder", hfind));

  if(m_debug==2)pjrf->msg().setLevel(MSG::VERBOSE);

  CHECK(pjrf->initialize());
  ToolHandle<IJetExecuteTool> hjrf(pjrf);
  hrecs.push_back(pjrf);

 
  tool = new JetToolRunner(("jetrunner"+outputcontainer).c_str());
  CHECK(tool->setProperty("Tools", hrecs));
  if(m_debug==2)tool->msg().setLevel(MSG::VERBOSE);


  Info( APP_NAME,"Initialising JetReclusteringTool(s)");
  CHECK(tool->initialize());
  tool->print();
  Info( APP_NAME,"done.....");

  */
  return true;
} 
