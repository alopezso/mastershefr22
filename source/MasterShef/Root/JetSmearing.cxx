
#include <MasterShef/MasterShef.h>
//#include "CPAnalysisExamples/errorcheck.h"

static const char* APP_NAME = "MasterShef";







bool MasterShef :: jetSmearing(){
  std::vector<std::unique_ptr<SmearData > > smrMc;
  m_jetMCSmearing->DoSmearing(smrMc,*m_jets_copy);
 
 

  std::vector<std::unique_ptr<SmearData > >::iterator firstSmear = smrMc.begin();
  std::vector<std::unique_ptr<SmearData > >::iterator lastSmear = smrMc.end();
  

 

  // xAOD::MissingETContainer *trackMET = new xAOD::MissingETContainer();
  // xAOD::MissingETAuxContainer *trackMETAux = new xAOD::MissingETAuxContainer();
  // trackMET->setStore(trackMETAux);
  // CHECK( store->record(trackMET,"SmrTrackMET" ));
  // CHECK( store->record(trackMETAux,"SmrTrackMETAux") );
  
  
  int nsmear=0;

  m_smrJets.clear();
  m_smrJets.shrink_to_fit();
  m_smrJetsAux.clear();
  m_smrJetsAux.shrink_to_fit();
  
  m_smrMET.clear();
  m_smrMET.shrink_to_fit();

  m_smrMETAux.clear();
  m_smrMETAux.shrink_to_fit();

 
  m_TailWeights.clear();
  m_TailWeights.shrink_to_fit();

  

  for(; firstSmear != lastSmear; ++firstSmear){          
    
    //make sure the pointer isn't unique
    std::move((*firstSmear)->jetContainer);
    std::move((*firstSmear)->jetAuxContainer);

  
    if(m_ExtraSysMaps || m_TailWeightMaps){
      m_TailWeights.push_back((*firstSmear)->TailWeights); // We only ever use the inclusive set of TailWeights

      // (*firstSmear)->TailWeights is a vector of doubles, size of however many sys maps you use
      // therefore need this to be a vector of vectors, first vector size n smear, inner vector size sys maps
      // so total size is nsmear * n_sys_maps, of the order 2000*20 = 40k entries
      
    }

    // CHECK( store->record();
  
    CHECK( store->record((*firstSmear)->jetContainer,"SMRJETS"+std::to_string(nsmear)));
    CHECK( store->record((*firstSmear)->jetAuxContainer,"SMRJETSAux."+std::to_string(nsmear)));
    
    m_smrJets.push_back((*firstSmear)->jetContainer);
    m_smrJetsAux.push_back((*firstSmear)->jetAuxContainer);
  
    //Bool_t test = store->contains<jet_t>("SMRJET");
    //std::cout << "is in store? " << test << std::endl;


    xAOD::MissingETContainer *SMRmetContainer = new xAOD::MissingETContainer;
    xAOD::MissingETAuxContainer *SMRmetContainerAux = new xAOD::MissingETAuxContainer;
    SMRmetContainer->setStore(SMRmetContainerAux);

    CHECK( store->record(SMRmetContainer,"SMRMet"+std::to_string(nsmear) ));
    CHECK( store->record(SMRmetContainerAux,"SMRMetAux."+std::to_string(nsmear) ));


    CHECK( m_objTool->GetMET(*SMRmetContainer ,(*firstSmear)->jetContainer,m_electrons_copy,m_muons_copy,m_photons_copy,0,true));
   
    

    // double SmrEtmiss_Et = 0.;
    // xAOD::MissingETContainer::const_iterator smet_it = SMRmetContainer->find("Final");
    // if (smet_it == SMRmetContainer->end())
    //   {
    // 	Error( APP_NAME, "No 'Final' inside Smeared MET container" );
    //   }
    // else
    //   {
    // 	double SmrEtmiss_Etx = (*smet_it)->mpx();
    // 	double SmrEtmiss_Ety = (*smet_it)->mpy();
    // 	SmrEtmiss_Et = sqrt(SmrEtmiss_Etx*SmrEtmiss_Etx + SmrEtmiss_Ety*SmrEtmiss_Ety);
    // 	std::cout << SmrEtmiss_Et  << " " <<  SMRmetContainer <<  std::endl;
    //   }


    m_smrMET.push_back(SMRmetContainer);
    m_smrMETAux.push_back(SMRmetContainerAux);

    //CHECK( store->remove(("SMRjet"+std::to_string(index)).c_str()));
    //CHECK( store->remove(("SMRjet"+std::to_string(index)+"Aux.").c_str()));
    


    /*
     

    double SmrEtmiss_Etx = 0.;
    double SmrEtmiss_Ety = 0.;
    //double SmrEtmiss_Et = 0.;
    xAOD::MissingETContainer::const_iterator smet_it = m_SMRmetContainer->find("Final");
    if (smet_it == m_SMRmetContainer->end())
      {
    	Error( APP_NAME, "No 'Final' inside Smeared MET container" );
      }
    else
      {
    	SmrEtmiss_Etx = (*smet_it)->mpx();
    	SmrEtmiss_Ety = (*smet_it)->mpy();
    	//SmrEtmiss_Et = sqrt(SmrEtmiss_Etx*SmrEtmiss_Etx + SmrEtmiss_Ety*SmrEtmiss_Ety);
      }
    
    //2 vec met
    m_smrMetVec.Set(SmrEtmiss_Etx, SmrEtmiss_Ety);
    
    //sumEt
    m_SumEtSMR = (*smet_it)->sumet();
    

   

    unsigned int nB = m_BJets->size();
    
    //bool isPlateau=false;
    //if  (pT_1jet>80 && (smrMetVec.Mod()/1000.)>((155*pT_1jet-8000)/(pT_1jet-60)))
    //  isPlateau = 1;
    
  
    if(m_doJetSmearingSkim && (( nB>=2 && (m_smrMetVec.Mod()/1000.) > 250) || (nB>=1 && (m_smrMetVec.Mod()/1000.) > 600) ) )
      {
	//std::cout << m_trackMET_pt << " " << trackMET_pt << std::endl;
    
	xAOD::JetContainer* theContainer = (*firstSmear)->jetContainer;
	xAOD::JetContainer* goodsmrJets = new xAOD::JetContainer();
	xAOD::JetAuxContainer* goodsmrJets_aux = new xAOD::JetAuxContainer();
	goodsmrJets->setStore( goodsmrJets_aux);

	xAOD::JetContainer::iterator firstJet = theContainer->begin();
	xAOD::JetContainer::iterator lastJet  = theContainer->end();
	std::vector<double> R;
	for(; firstJet != lastJet; ++firstJet)
	  {       
	    xAOD::Jet* theJet = (xAOD::Jet*)(*firstJet);
	    if ( theJet->auxdata< char >("smearjet") == true && theJet->pt()>20*1000.  )
	      {
		xAOD::Jet *myJet = new xAOD::Jet((*theJet));
		goodsmrJets->push_back(myJet);
	      }
	  } 

	
	CHECK(store->record(goodsmrJets,"GoodSMRJets"));
	CHECK(store->record(goodsmrJets_aux,"GoodSMRJetsAux."));

	if(m_useAntiKtRcJets)
	  {
	    m_jetRecTool_smrkt8->execute();
	    m_jetRecTool_smrkt12->execute();
	    CHECK(event->retrieve( m_smrrcjets_kt8,"MySMRFatJetsKt8" ));
	    //CHECK(store->remove("ClusterSequence_JetFinder_myjetfind"));
	    CHECK(event->retrieve( m_smrrcjets_kt12,"MySMRFatJetsKt12" ));
	    m_smrrcjets_kt8->sort(ptsorter);
	    m_smrrcjets_kt12->sort(ptsorter);
	    for (unsigned int i=0;i<m_smrrcjets_kt8->size();i++) 
	      {	  
		nt_smrrcjet_kt8_pt.push_back( m_smrrcjets_kt8->at(i)->pt() );
		nt_smrrcjet_kt8_eta.push_back( m_smrrcjets_kt8->at(i)->eta() );
		nt_smrrcjet_kt8_phi.push_back( m_smrrcjets_kt8->at(i)->phi()  );
		nt_smrrcjet_kt8_e.push_back(  m_smrrcjets_kt8->at(i)->e() );
		nt_smrrcjet_kt8_index.push_back(i);
	      }
	    for (unsigned int i=0;i<m_smrrcjets_kt12->size();i++) 
	      {	  
		nt_smrrcjet_kt12_pt.push_back( m_smrrcjets_kt12->at(i)->pt() );
		nt_smrrcjet_kt12_eta.push_back( m_smrrcjets_kt12->at(i)->eta() );
		nt_smrrcjet_kt12_phi.push_back( m_smrrcjets_kt12->at(i)->phi()  );
		nt_smrrcjet_kt12_e.push_back(  m_smrrcjets_kt12->at(i)->e() );
		nt_smrrcjet_kt12_index.push_back(i);
	      }
	  
	    
	    // this is a fucking nightmare
	   
	    
	    CHECK(store->remove("ReclusteredMySMRFatJetsKt8"));
	    CHECK(store->remove("ReclusteredMySMRFatJetsKt12"));
	    
	    CHECK(store->remove("ClusterSequence_JetFinder_myjetfind"));
	    
	    CHECK(store->remove("MySMRFatJetsKt8"));
	    CHECK(store->remove("MySMRFatJetsKt12"));
	    CHECK(store->remove("MySMRFatJetsKt8Aux."));
	    CHECK(store->remove("MySMRFatJetsKt12Aux."));
	  }




	
	CHECK(store->remove("GoodSMRJets"));
	CHECK(store->remove("GoodSMRJetsAux."));
	
	nsmear++;
      }

    

  
    
    CHECK(store->remove("SMRJET"));
    CHECK(store->remove("SMRJETAux."));
    */
    nsmear++;
  }


  

  //if(smrJetVec)
  //  delete smrJetVec;
  //if(smrJetVec_aux)
  //  delete smrJetVec_aux;
  
  return true;
  
}

