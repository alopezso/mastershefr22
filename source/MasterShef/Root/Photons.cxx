#include <MasterShef/MasterShef.h>
//#include "CPAnalysisExamples/errorcheck.h"

//
static const char* APP_NAME = "MasterShef::Photons";



// ---------------------------------------------------------------
// Function to prepare baseline photons for analysis
//
// This is a defintions of all the containers that are loaded here....
// The definitions (pt,eta,id,iso) of these are set in the .conf files!
//

//
// ---------------------------------------------------------------
bool MasterShef :: prepareBaselinePhotonsBeforeOR() {

  
  
  m_photons_copy = 0;
  m_photons_copyaux = 0;
  m_signalPhotonsBeforeOR = new xAOD::PhotonContainer(SG::VIEW_ELEMENTS);
  CHECK(store->record(m_signalPhotonsBeforeOR,"signalPhotonsOR"+m_sysName) );

  if (m_usePhotons){
    EL_RETURN_CHECK("GetPhotons()", m_objTool->GetPhotons(m_photons_copy,m_photons_copyaux));
    for(const auto& ph : *m_photons_copy) {
      if( ph->auxdata< char >("baseline") ){
	m_signalPhotonsBeforeOR->push_back(ph);
      }
    }//ends loop over all photons
  }//ends check on if we are to use photons
    

  ATH_MSG_VERBOSE("Done in prepareBaselinePhotonsBeforeOR");
  ATH_MSG_VERBOSE("use Photons?= "<< m_usePhotons);
  ATH_MSG_VERBOSE("nsignalPhotons before OR= "<< m_signalPhotonsBeforeOR->size());

  return true;

}

//-------------------------------------------------------
// Function to prepare the signal photons
//
//-------------------------------------------------------
bool MasterShef :: preparePhotons() {

  m_signalPhotons = new xAOD::PhotonContainer(SG::VIEW_ELEMENTS);
  m_baselinePhotons = new xAOD::PhotonContainer(SG::VIEW_ELEMENTS);

  //retrieve if we said photons should be used in the OR
  const bool photonsUsedInOR = *(m_objTool->getProperty<bool>("DoPhotonOR"));
 

  //loop over signals photons before the OR
  for(const auto& ph : *m_signalPhotonsBeforeOR) {
    //if photons were not used in the OR then we still want to save the baseline and signal photons
    //so keep this true as default
    bool passOR=true;
    //if they were used in the OR then they'd be decorated as having "passOR"
    //so retrieve this to see if we should push back the photon or not
    if(photonsUsedInOR){
      passOR=ph->auxdata< char >("passOR")==1  ;
    }

    //if baseline and pass the OR then it is a baseline photon
    if( ph->auxdata< char >("baseline")==1 && passOR)
      {
	m_baselinePhotons->push_back(ph);
	
	//check if the photon is also a signal photon
	//likely to have a tighter isolation requirement
	if( ph->auxdata< char >("signal")==1){
	  m_signalPhotons->push_back(ph);
	}
      }//end of the baseline/passOR check
  }//end of loop over all photons before the OR
  m_signalPhotons->sort(ptsorter);
  m_baselinePhotons->sort(ptsorter);
  CHECK( store->record(m_baselinePhotons,"BaseLinePhotons"+m_sysName) );
  CHECK( store->record(m_signalPhotons,"SignalPhotons"+m_sysName) );
  
  return true;

}
