#include <MasterShef/MasterShef.h>
//#include "CPAnalysisExamples/errorcheck.h"


static const char* APP_NAME = "MasterShef::MET";


// ---------------------------------------------------------------
// Function to perform met calculatuions
// we call four 
//
// ---------------------------------------------------------------
bool MasterShef :: prepareMET() {

  CHECK(prepareMETTST());
  CHECK(prepareMETCST());
  CHECK(prepareTrackMET());
  CHECK(prepareSimpleMET());

  return true;
}

// ---------------------------------------------------------------
// Function to retrieve the met and get all met terms/sumet!
// ---------------------------------------------------------------
bool MasterShef :: prepareMETTST() {
 
  //---------------------
  // TST MET
  //---------------------
  // Prepare the classic normal TST MET
  m_metContainer = new xAOD::MissingETContainer();
  m_metContainerAux = new xAOD::MissingETAuxContainer();
  m_metContainer->setStore( m_metContainerAux ); //< Connect the two
  CHECK( store->record(m_metContainer,"GoodMetCont"+m_sysName) );
  CHECK( store->record(m_metContainerAux,"GoodMetContAux"+m_sysName) );

  //std::cout << "Error coming from the MET Orig Container " << std::endl;

  //met is calculated from all objects retrieved by susytools and marked as baseline
  
  EL_RETURN_CHECK("GetMet()", m_objTool->GetMET(*m_metContainer,
						m_jets_copy,
						m_electrons_copy,
						m_muons_copy,
						m_photons_copy,
						m_taus_copy,
						true ));

  ATH_MSG_VERBOSE("Finished GetMet()");   



  //std::cout << "Error not  coming from the MET Container " << std::endl;

  xAOD::MissingETContainer::const_iterator met_it = m_metContainer->find("Final");
  xAOD::MissingETContainer::const_iterator met_it_jet = m_metContainer->find("RefJet");
  xAOD::MissingETContainer::const_iterator met_it_RefEle = m_metContainer->find("RefEle");
  xAOD::MissingETContainer::const_iterator met_it_RefGamma = m_metContainer->find("RefGamma");
  xAOD::MissingETContainer::const_iterator met_it_Muons = m_metContainer->find("Muons");
  xAOD::MissingETContainer::const_iterator met_it_PVSoftTrk = m_metContainer->find("PVSoftTrk");
  xAOD::MissingETContainer::const_iterator met_it_RefTau = m_metContainer->find("RefTau");

  const xAOD::MissingET* mettst=(*m_metContainer)["Final"];
  if(!mettst)Error(APP_NAME,"No 'Final' inside MET container");
  const xAOD::MissingET* mettst_jet=(*m_metContainer)["RefJet"];
  const xAOD::MissingET* mettst_el=(*m_metContainer)["RefEle"];
  const xAOD::MissingET* mettst_mu=(*m_metContainer)["Muons"];   
  const xAOD::MissingET* mettst_gamma=(*m_metContainer)["RefGamma"];   
  const xAOD::MissingET* mettst_PVSoftTrk=(*m_metContainer)["PVSoftTrk"];   
  const xAOD::MissingET* mettst_tau=(*m_metContainer)["RefTau"];   

  m_pxmiss = mettst->mpx();
  m_pymiss = mettst->mpy();
  m_etmiss = mettst->met();

  m_pxmiss_jet = mettst_jet->mpx();
  m_pymiss_jet = mettst_jet->mpy();
  m_etmiss_jet = mettst_jet->met();

  m_pxmiss_mu = mettst_mu ?  mettst_mu->mpx() : -999;
  m_pymiss_mu = mettst_mu ?  mettst_mu->mpy() : -999;
  m_etmiss_mu = mettst_mu ?  mettst_mu->met() : -999;

  m_pxmiss_el = mettst_el ?  mettst_el->mpx() : -999;
  m_pymiss_el = mettst_el ?  mettst_el->mpy() : -999;
  m_etmiss_el = mettst_el ?  mettst_el->met() : -999;
  if(m_usePhotons && mettst_gamma!=nullptr){
    m_pxmiss_y = mettst_gamma ? mettst_gamma->mpx() : -999;
    m_pymiss_y = mettst_gamma ? mettst_gamma->mpy() : -999;
    m_etmiss_y = mettst_gamma ? mettst_gamma->met() : -999;
  }


  if(m_useTaus && mettst_tau!=nullptr){
    m_pxmiss_tau=mettst_tau->mpx();
    m_pymiss_tau=mettst_tau->mpy();
    m_etmiss_tau=mettst_tau->met();
  }


  m_pxmiss_softTrk = mettst_PVSoftTrk->mpx();
  m_pymiss_softTrk = mettst_PVSoftTrk->mpy();
  m_etmiss_softTrk = mettst_PVSoftTrk->met();
  ATH_MSG_VERBOSE("MET IS GOOD");

  m_sumet = mettst->sumet();
  m_sumet_jet = mettst_jet->sumet();
  m_sumet_el = mettst_el ? mettst_el->sumet() : -999.0;
  if(mettst_gamma!=nullptr)
    m_sumet_y = mettst_gamma->sumet();
  m_sumet_mu = mettst_mu ? mettst_mu->sumet() : -999.0;
  m_sumet_softTrk= mettst_PVSoftTrk->sumet();

  ATH_MSG_VERBOSE("sumET IS GOOD");
  return true;
}//end of preapre mettst


//
// Old function for getting METCST, this should be work on in the future
// currently it just returns the soft cluster terms
//
bool MasterShef :: prepareMETCST() {
 
  // Prepare CST MET
  xAOD::MissingETContainer* m_metContainerCST = new xAOD::MissingETContainer();
  xAOD::MissingETAuxContainer* m_metContainerAuxCST = new xAOD::MissingETAuxContainer();
  m_metContainerCST->setStore( m_metContainerAuxCST ); //< Connect the two
  // FIXME. This should be done in GetMet??
  CHECK( store->record(m_metContainerCST,"GoodMetContCST"+m_sysName) );
  CHECK( store->record(m_metContainerAuxCST,"GoodMetContCSTAux"+m_sysName) );

  EL_RETURN_CHECK("GetMet()", m_objTool->GetMET(*m_metContainerCST,
  						 m_jets_copy,
  						 m_electrons_copy,
  						 m_muons_copy,
  						 m_photons_copy,
  						 NULL,
						false,
						false));

  ATH_MSG_VERBOSE("METCST IS GOOD");
  xAOD::MissingETContainer::const_iterator met_it_SoftClus = m_metContainerCST->find("SoftClus");
  
  if (!(met_it_SoftClus == m_metContainerCST->end()))
    {
      m_sumet_softClu= (*met_it_SoftClus)->met();
    }
  
  return true;
  
} //end of prepare metcst


// ---------------------------------------------------------------
// Function to retrieve the met and get all met terms/sumet!
// ---------------------------------------------------------------
bool MasterShef :: prepareTrackMET() {


  xAOD::MissingETContainer *trackMET = new xAOD::MissingETContainer();
  xAOD::MissingETAuxContainer *trackMETAux = new xAOD::MissingETAuxContainer();

  trackMET->setStore(trackMETAux);
  xAOD::MissingETContainer::const_iterator met_it_track;


  m_trackMET_px=0;
  m_trackMET_py=0;

  EL_RETURN_CHECK("GetMet()", m_objTool->GetTrackMET(*trackMET,
                                                     m_jets_copy,
                                                     m_electrons_copy,
                                                     m_muons_copy));

  m_trackMET_px=(*trackMET)["Track"]->mpx();
  m_trackMET_py=(*trackMET)["Track"]->mpy();

  CHECK( store->record(trackMET,"TrackMET"+m_sysName) );
  CHECK( store->record(trackMETAux,"TrackMETAux"+m_sysName) );

  ATH_MSG_VERBOSE("TRACKMET IS GOOD");

  return true;
}//end of prepare mettrack


// ---------------------------------------------------------------
// Function to retrieve some alternative simple versions of MET
// ---------------------------------------------------------------
bool MasterShef :: prepareSimpleMET() {

  //by default this is false, turn on in SUSY.py to load up all these variables
  if(m_useSimpleMET)return true;


  // Prepare Simple TST MET
  m_simpleMETCont = new xAOD::MissingETContainer();
  m_simpleMETContAux = new xAOD::MissingETAuxContainer();
  m_simpleMETCont->setStore( m_simpleMETContAux ); //< Connect the two
  CHECK( store->record(m_simpleMETCont,"SimpleMetCont"+m_sysName) );
  CHECK( store->record(m_simpleMETContAux,"SimpleMetContAux"+m_sysName) );
  
  
  // we say simple met is just calculated with jets, muons and the soft term
  // this avoids issues of lepton OR in the met calculation
  // also turn off jvt cut that can introduce met by accidentally removing a HS jet
  EL_RETURN_CHECK("GetMet()", m_objTool->GetMET(*m_simpleMETCont,
						m_jets_copy,
						NULL, // electrons
						m_muons_copy,
						NULL, // photons
						NULL, // taus
						true, // do TST?
						false // JVT cut
						));
  
  
  // Prepare Simple CST MET
  m_simpleCSTMETCont = new xAOD::MissingETContainer();
  m_simpleCSTMETContAux = new xAOD::MissingETAuxContainer();
  m_simpleCSTMETCont->setStore( m_simpleCSTMETContAux ); //< Connect the two
  CHECK( store->record(m_simpleCSTMETCont,"SimpleCSTMetCont"+m_sysName) );
  CHECK( store->record(m_simpleCSTMETContAux,"SimpleCSTMetContAux"+m_sysName) );
  
  EL_RETURN_CHECK("GetMet()", m_objTool->GetMET(*m_simpleCSTMETCont,
						  m_jets_copy,
						  NULL, // electrons
						  m_muons_copy,
						  NULL, // photons
						  NULL, // taus
						  false, // do TST?
						  false // JVT cut
						  ));


  const xAOD::MissingET* simplemettst=(*m_simpleMETCont)["Final"];
  const xAOD::MissingET* simplemetcst=(*m_simpleCSTMETCont)["Final"];
  
  if(!simplemettst)Error(APP_NAME,"No 'Final' inside simple TST MET container");
  if(!simplemetcst)Error(APP_NAME,"No 'Final' inside simple CST MET container");
  const xAOD::MissingET* simplemettst_jet=(*m_simpleMETCont)["RefJet"];
  const xAOD::MissingET* simplemettst_mu=(*m_simpleMETCont)["Muons"];   
  const xAOD::MissingET* simplemettst_PVSoftTrk=(*m_simpleMETCont)["PVSoftTrk"];   
  
  const xAOD::MissingET* simplemetcst_jet=(*m_simpleCSTMETCont)["RefJet"];
  const xAOD::MissingET* simplemetcst_mu=(*m_simpleCSTMETCont)["Muons"];   
  const xAOD::MissingET* simplemetcst_SoftClus=(*m_simpleCSTMETCont)["SoftClus"];   
  
  
  m_pxmiss_simple = simplemettst->mpx();
  m_pymiss_simple = simplemettst->mpy();
  m_etmiss_simple = simplemettst->met();
  
  m_pxmiss_simpleCST = simplemetcst->mpx();
  m_pymiss_simpleCST = simplemetcst->mpy();
  m_etmiss_simpleCST = simplemetcst->met();
  
  
  m_pxmiss_simple_jet = simplemettst_jet->mpx();
  m_pymiss_simple_jet = simplemettst_jet->mpy();
  m_etmiss_simple_jet = simplemettst_jet->met();
  
  m_pxmiss_simpleCST_jet = simplemetcst_jet->mpx();
  m_pymiss_simpleCST_jet = simplemetcst_jet->mpy();
  m_etmiss_simpleCST_jet = simplemetcst_jet->met();
  
  
  m_pxmiss_simple_mu = simplemettst_mu->mpx();
  m_pymiss_simple_mu = simplemettst_mu->mpy();
  m_etmiss_simple_mu = simplemettst_mu->met();
  
  m_pxmiss_simpleCST_mu = simplemetcst_mu->mpx();
  m_pymiss_simpleCST_mu = simplemetcst_mu->mpy();
  m_etmiss_simpleCST_mu = simplemetcst_mu->met();
  
  m_pxmiss_simple_softTrk = simplemettst_PVSoftTrk->mpx();
  m_pymiss_simple_softTrk = simplemettst_PVSoftTrk->mpy();
  m_etmiss_simple_softTrk = simplemettst_PVSoftTrk->met();
  
  m_pxmiss_simple_softClus = simplemetcst_SoftClus->mpx();
  m_pymiss_simple_softClus = simplemetcst_SoftClus->mpy();
  m_etmiss_simple_softClus = simplemetcst_SoftClus->met();
  
  
  m_simpleTST_sumet = simplemettst->sumet();
  m_simpleTST_sumet_jet = simplemettst_jet->sumet();
  m_simpleTST_sumet_mu = simplemettst_mu->sumet();
  m_simpleTST_sumet_softTrk= simplemettst_PVSoftTrk->sumet();
  
  m_simpleCST_sumet = simplemetcst->sumet();
  m_simpleCST_sumet_jet = simplemetcst_jet->sumet();
  m_simpleCST_sumet_mu = simplemetcst_mu->sumet();
  m_simpleCST_sumet_softClus= simplemetcst_SoftClus->sumet();
  
  return true;

} //done with some simple met




// ---------------------------------------------------------------
// Function to perform complex met calculatuions
// ---------------------------------------------------------------
bool MasterShef :: prepareComplexMET() {

  
  //____________________________________________
  //
  // prepare fake MET
  //____________________________________________

  ATH_MSG_VERBOSE("done single jet trigger matching");   

  xAOD::MissingETContainer *m_met_primeContainer = new xAOD::MissingETContainer();
  xAOD::MissingETAuxContainer *m_met_primeContainerAux = new xAOD::MissingETAuxContainer();
  m_met_primeContainer->setStore( m_met_primeContainerAux );
  CHECK( store->record(m_met_primeContainer,"GoodMet_PrimeCont"+m_sysName) );
  CHECK( store->record(m_met_primeContainerAux,"GoodMet_PrimeContAux"+m_sysName) );

  m_pxmiss_prime=0;
  m_pymiss_prime=0;
  m_etmiss_prime=0;


  ATH_MSG_VERBOSE("going to do fake MET!");
  

  //
  // Here we start to mark particles as invisible in the met calculation
  // this is important for susy searches where we use 2L of photon events to mimic the met
  // by marking Z->ll (Z boson) as invisible then it can be used to mimic Z->vv
  // the same can be applied for a photon 
  //

  const bool photonsUsedInOR = *(m_objTool->getProperty<bool>("DoPhotonOR"));
  
  xAOD::IParticleContainer* invis = 0;
  if (m_signalElectrons->size()==2 && m_signalMuons->size()==0)  {
    invis = dynamic_cast<xAOD::IParticleContainer*>(m_signalElectrons);
  }
  else if (m_signalElectrons->size()==0 && m_signalMuons->size()==2)  {
    invis = dynamic_cast<xAOD::IParticleContainer*>(m_signalMuons);
  }
  else if(m_signalPhotons->size()==1 && photonsUsedInOR)  {
    invis = dynamic_cast<xAOD::IParticleContainer*>(m_signalPhotons);
  }

  //std::cout << "Error coming from the MET Prime Container " << std::endl;
  //____________________________________________
  //
  // build fake MET
  //____________________________________________

  EL_RETURN_CHECK("GetMetPrime()",m_objTool->GetMET(*m_met_primeContainer,
						    m_jets_copy, 
						    m_electrons_copy, 
						    m_muons_copy, 
						    m_photons_copy,
						    NULL,
						    true,
						    true,
						    invis));
   
  xAOD::MissingETContainer::const_iterator met_it = m_met_primeContainer->find("Final");

  m_pxmiss_prime = (*met_it)->mpx();
  m_pymiss_prime = (*met_it)->mpy();
  m_etmiss_prime = sqrt(m_pxmiss_prime*m_pxmiss_prime +  m_pymiss_prime* m_pymiss_prime);


  //std::cout << "Error not coming from the MET Prime Container " << std::endl;

  ATH_MSG_VERBOSE("done MET!");
  
  /*  
  //-------------------------------
  // object based metsignificance that is setup manually
  ATH_MSG_VERBOSE("get variance MET from met sig");

  float avgmu = eventInfo->averageInteractionsPerCrossing();

  CHECK(m_metSignif->varianceMET(m_met_primeContainer, avgmu, "RefJet", "PVSoftTrk","Final"));

  ATH_MSG_VERBOSE("get variables for met sig");
  

  //get the nominal metsignificance
  m_metsigET = m_metSignif->GetMETOverSqrtSumET();
  m_metsigHT = m_metSignif->GetMETOverSqrtHT();
  m_metsig = m_metSignif->GetSignificance();

  //in my hacked version of the tool setting this to false takes the max(data,mc) in jet resolution aka the default
  CHECK(m_metSignif_maxRes->varianceMET(m_met_primeContainer, avgmu, "RefJet", "PVSoftTrk","Final"));
  //the default metsig is the one with the maximum resolution of max(data,MC)
  m_metsig_default=m_metSignif_maxRes->GetSignificance();

  //turn on inflating the b-jet resolution too
  CHECK(m_metSignif_bjet->varianceMET(m_met_primeContainer, avgmu,"RefJet", "PVSoftTrk","Final"));
  m_metsig_binflate=m_metSignif_bjet->GetSignificance();


  CHECK(m_metSignif_run1JER->varianceMET(m_met_primeContainer, avgmu,"RefJet", "PVSoftTrk","Final"));
  m_metsig_run1JER = m_metSignif_run1JER->GetSignificance();

  */
  //-------------------------------
  // object based metsignificance retrieved from SUSYTools

  m_metsigST=0;

  EL_RETURN_CHECK("GetMetSig()",m_objTool->GetMETSig(*m_met_primeContainer,
						     m_metsigST,
						     true, // TST
						     true));// doJVT

  ATH_MSG_VERBOSE("Finished GetMetSig()");     
  ATH_MSG_VERBOSE("complex MET IS GOOD");

  return true;
}

// ---------------------------------------------------------------
// Function to perform  met calculatuions including leptons as jets
// ---------------------------------------------------------------
bool MasterShef :: prepareLepJetMET() {

  //____________________________________________
  //
  // prepare MET where lepton is treated as a jet
  //____________________________________________

  xAOD::MissingETContainer *m_met_lepjetContainer = new xAOD::MissingETContainer();
  xAOD::MissingETAuxContainer *m_met_lepjetContainerAux = new xAOD::MissingETAuxContainer();
  m_met_lepjetContainer->setStore( m_met_lepjetContainerAux );
  CHECK( store->record(m_met_lepjetContainer,"GoodMet_LepJetCont"+m_sysName) );
  CHECK( store->record(m_met_lepjetContainerAux,"GoodMet_LepJetContAux"+m_sysName) );


  //____________________________________________
  //
  // build MET using the signal leptons as jets.
  //____________________________________________

  ConstDataVector<xAOD::ElectronContainer> elecCopy(SG::VIEW_ELEMENTS);
  //  ConstDataVector<xAOD::MuonContainer> muonCopy(SG::VIEW_ELEMENTS);

  for( const auto& electron : *m_electrons_copy ){
    //bool veto = false;
    if (electron->isAvailable<char>("signal") )
      continue;
    if ( electron->auxdata<char>("signal") != 1 )
      elecCopy.push_back(electron);
  }

  // Muons don't form in general a jet with enough energy so that it is safe to substitute them by an overlapping jet. Comment this
  // for( const auto& muon : *m_muons_copy ){
  //   bool veto = false;
  //   if (muon->isAvailable<char>("signal") )
  //     continue;
  //   if ( muon->auxdata<char>("signal") != 1 )
  //     muonCopy.push_back(muon);
  // }
  
  
  EL_RETURN_CHECK("GetMetLepJet()",m_objTool->GetMET(*m_met_lepjetContainer,
						     m_jets_copy,
						     elecCopy.asDataVector(), 
						     m_muons_copy,
						     //						     muonCopy.asDataVector(),
						     m_photons_copy,
						     NULL,
						     true,
						     true
						     ));



  xAOD::MissingETContainer::const_iterator met_lepjet_it = m_met_lepjetContainer->find("Final");

  m_px_lepjet_miss = (*met_lepjet_it)->mpx();
  m_py_lepjet_miss = (*met_lepjet_it)->mpy();

  m_metsigLepJetST=0.;   
  EL_RETURN_CHECK("GetMetSigLepJet()",m_objTool->GetMETSig(*m_met_lepjetContainer,
							   m_metsigLepJetST,
							   true, // TST
							   true));// doJVT
  
  ATH_MSG_VERBOSE("Finished significance using leptons as jets()");   


  /*
  //____________________________________________
  //
  // prepare METSig where lepton is treated as a jet
  //____________________________________________


  xAOD::MissingETContainer *m_met_defaultContainer = nullptr;
  CHECK( evtStore()->retrieve(m_met_defaultContainer,"GoodMetCont"+m_sysName));


  m_metsigLepJetResol = 0.;
  float avgmu = eventInfo->averageInteractionsPerCrossing();
  ATH_CHECK( m_metsigJetResol->varianceMET( m_met_defaultContainer, avgmu, "RefJet", "PVSoftTrk", "Final") );
  m_metsigLepJetResol = m_metsigJetResol->GetSignificance();

  ATH_MSG_VERBOSE("Finished GetMetSigLepJet()");   
  */
  return true;
}
