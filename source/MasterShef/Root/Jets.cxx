#include <MasterShef/MasterShef.h>
//#include "CPAnalysisExamples/errorcheck.h"
 
//new decorator to help with defining bjets when using pseudo-continuous
static SG::AuxElement::Decorator<bool> dec_isBtag("IsBJet");
static SG::AuxElement::Decorator<bool> dec_isCtag("IsCJet");
static SG::AuxElement::Decorator<double> dec_DL1pb("DL1pb");
static SG::AuxElement::Decorator<double> dec_DL1pc("DL1pc");
static SG::AuxElement::Decorator<double> dec_DL1pu("DL1pu");
static SG::AuxElement::Decorator<double> dec_DL1rpb("DL1rpb");
static SG::AuxElement::Decorator<double> dec_DL1rpc("DL1rpc");
static SG::AuxElement::Decorator<double> dec_DL1rpu("DL1rpu");
static SG::AuxElement::Decorator<double> dec_cscore("CharmScore");



static const char* APP_NAME = "MasterShef::Jets";

//!-----------------------------------------------------------------------
//some global variables for inflating the b-jet resolution!
//this is the convolution of two polynomials 
const double p0=70.0496865954;
const double p1=2.14036020126;
const double p2=0.959434710176;
const double p3=0.048699943063;
const double p4=-10.8703629295;
const double p5=14.3160602439;
const double p6=0.990574980208;
const double p7=0.0548759780131;
//----------------------------------------------------------------------


// ---------------------------------------------------------------
// Function to prepare baseline jets for analysis
//
// 
// ---------------------------------------------------------------
bool MasterShef :: prepareBaselineJetsBeforeOR() {
  
 
  //let's get the jets from SUSYTools
  m_jets_copy=0;
  m_jets_copyaux=0;
  EL_RETURN_CHECK("GetJets()",m_objTool->GetJets(m_jets_copy,m_jets_copyaux));
  

  //save a container for signal jets before OR
  m_signalJetsBeforeOR = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
  CHECK( store->record(m_signalJetsBeforeOR,"SignalJetsBeforeOR"+m_sysName) );


  //loop over all jets in the event
  for(const auto& jet : *m_jets_copy) {
    ATH_MSG_VERBOSE("Decorating jet");


    //decorating bjets to be inflate the resolution in the metsig tool
    //set this to default to 0.0, aka no inflation of the resolution
    jet->auxdata<float>("bjetInflate")=0.0;
    //if it's a b-jet then set some amount to inflate the resolution in the metsig tool
   
    
    if(jet->auxdata< bool >("IsBJet") && jet->pt()/1000. > 50 && m_doBJetInflation){
      double x = jet->pt()/1000.;
      float inflate=(p7*(p4/x+p5/sqrt(x)+p6))- (p3*(p0/x+p1/sqrt(x)+p2));
      jet->auxdata<float>("bjetInflate")=inflate; 
    }

    // if this is mc then let's decorate the origin and type of the jet from the mc truth classifier
    // this will let us what particle (Wboson, Top, QCD) the jet originated from and it's type (BJet,CJet..)
    if(m_isMC){
      ATH_MSG_VERBOSE("Going to find the jet type..");
      std::string type = "";
      std::string origin = "";
      if(m_useMCTruthClassifier){
	auto result = m_MCtruthClassifier->particleTruthClassifier(jet,true);
	ATH_MSG_VERBOSE("Gote result.");
	MCTruthPartClassifier::ParticleDef partDef;
	origin = partDef.sParticleOrigin[result.second];
	ATH_MSG_VERBOSE("Got origin");
	type = partDef.sParticleType[result.first];
	ATH_MSG_VERBOSE("Got type.");
      }
      //decorate the aux container with this information so we can retrieve it later
      jet->auxdata<std::string>("type")=type;
      jet->auxdata<std::string>("origin")=origin;
      ATH_MSG_VERBOSE("Jet Origin="<< type);
    }

    //mark all jets to not be smeared at this point
    //when the jet is a signal jet then we'll mark to smear these.
    dec_smeared(*jet) = false;

    //push a new container with signal(baseline) jets before OR
    if ( jet->auxdata< char >("baseline") )
      m_signalJetsBeforeOR->push_back (jet);
  }
  
  //pt sort this new container
  m_signalJetsBeforeOR->sort(ptsorter);
  

  return true;}

 

// ---------------------------------------------------------------
// Function to prepare jets for analysis
// ---------------------------------------------------------------
bool MasterShef :: prepareJets() {


  ATH_MSG_VERBOSE("starting to prepare jets");   

  // Store Jets
  m_badJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
  m_signalJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
  m_signalJets35 = new xAOD::JetContainer(SG::VIEW_ELEMENTS); //create a container of jets with a pt cut 35 GeV
  //m_signalJets_recl = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
  m_BJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
  m_CJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
  // store baseline jets just after OR to pass into JVT SF calculator (no JVT requirement yet--comes with signal definition)
  m_baselineJetsAfterOR = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
 

  //some special code to randomly tag jets as b-tjets
  //randomly tag two light jets as b-jets, data-driven method, be-careful!
  //turn this off for now
  /*
  if(m_randomlyBTagTwoJets){
    //obtain the light jets
    xAOD::JetContainer *m_lightJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
    for(unsigned int ii=0; ii<m_signalJetsBeforeOR->size(); ii++) {
      xAOD::Jet *jet = m_signalJetsBeforeOR->at(ii);
      //skip non-signal
      if(! (jet->auxdata< char >("signal")==1 &&  jet->auxdata< char >("passOR")==1 &&  jet->auxdata< char >("baseline")==1)) continue;
      //skip b-jets
      if((jet->auxdata< char >("bjet")==1) && (fabs(jet->eta() ) < 2.5)) continue;

      //skip jets outside the tracker
      if(fabs(jet->eta() ) > 2.5) continue;

      m_lightJets->push_back(jet);
    }
    
    //only do this on events with two light jets
    if(m_lightJets->size()>1){
      //shuffle the light jets
      std::random_shuffle(m_lightJets->begin(),m_lightJets->end());
      //b-tag a random jet
      m_lightJets->at(0)->auxdata< char >("bjet")=true;
      //b-tag another random jet
      m_lightJets->at(1)->auxdata< char >("bjet")=true;
      
    }

    //since randomly shuffled we can tag the first two as b-jets
    CHECK( store->record(m_lightJets,"SignalLightJets") );
  }
  */

  ATH_MSG_VERBOSE("Start loop on signal jets before OR");   

  for(const auto& jet : *m_signalJetsBeforeOR) {
    ATH_MSG_VERBOSE("in jet loop jets");   
    if ((jet->auxdata< char >("bad")==1 )&& (jet->auxdata< char >("baseline")==1) && (jet->auxdata< char >("passOR")==1) && (jet->auxdata< char >("passJvt")==1) ) {
      m_badJets->push_back( jet );
    }

    // all sorts of complex decoration of jets with different b-tagging
    // this makes things complicated, will turn this off for now
    // commenting out for now incase it ise useful in the future - Calum
    /*
    // Hybrid b-tagging decoration
    if (m_btagSelTool_MV2c10_H85->accept(*jet) && (fabs(jet->eta() ) < 2.5)) jet->auxdata< bool >("isMV2c10H85") = true;
    else jet->auxdata< bool >("isMV2c10H85") = false;
    if (m_btagSelTool_MV2c10_H77->accept(*jet) && (fabs(jet->eta() ) < 2.5)) jet->auxdata< bool >("isMV2c10H77") = true;
    else jet->auxdata< bool >("isMV2c10H77") = false;
    if (m_btagSelTool_MV2c10_H70->accept(*jet) && (fabs(jet->eta() ) < 2.5)) jet->auxdata< bool >("isMV2c10H70") = true;
    else jet->auxdata< bool >("isMV2c10H70") = false;
    if (!m_isPflowResearch){
      if (m_btagSelTool_DL1_H85->accept(*jet) && (fabs(jet->eta() ) < 2.5)) jet->auxdata< bool >("isDL1H85") = true;
      else jet->auxdata< bool >("isDL1H85") = false;
      if (m_btagSelTool_DL1_H77->accept(*jet) && (fabs(jet->eta() ) < 2.5)) jet->auxdata< bool >("isDL1H77") = true;
      else jet->auxdata< bool >("isDL1H77") = false;
      if (m_btagSelTool_DL1_H70->accept(*jet) && (fabs(jet->eta() ) < 2.5)) jet->auxdata< bool >("isDL1H70") = true;
      else jet->auxdata< bool >("isDL1H70") = false;
    }
    
    // MV2c10 & DL1 weight decoration - OP used here doesn't matter
    ANA_CHECK(m_btagSelTool_MV2c10_H77->getTaggerWeight(*jet, jet->auxdata< double >("weight_MV2c10")));
    if (!m_isPflowResearch) ANA_CHECK(m_btagSelTool_DL1_H77->getTaggerWeight(*jet, jet->auxdata< double >("weight_DL1"))); 
    */

    // save baseline jets after OR
    if (jet->auxdata< char >("baseline")==1 && jet->auxdata< char >("passOR")==1) m_baselineJetsAfterOR->push_back(jet);

    if(jet->auxdata< char >("signal")==1 &&  jet->auxdata< char >("passOR")==1 &&  jet->auxdata< char >("baseline")==1) {
      //mark these jets to be smeared by jet smearing
      dec_smeared(*jet) = true;
      //our signal jets
      m_signalJets->push_back(jet);
      //m_signalJets_recl->push_back(jet);
      //b-jets!
      // If continuous b-tagging is used, m_btagBin corresponds to the bin for the WP that has been set on the cmdline, if non-continuous, m_btagBin has defaulted to 1, and >= will pass, cause == 1 is satisfied. Probably not optimal to do it this way, but it does mean it can be all done in one line
      
      double pu,pc,pb;
      
      xAOD::BTaggingUtilities::getBTagging(*jet)->pu("DL1",pu);
      xAOD::BTaggingUtilities::getBTagging(*jet)->pc("DL1",pc);
      xAOD::BTaggingUtilities::getBTagging(*jet)->pb("DL1",pb);
      
      dec_DL1pb(*jet) = pb;
      dec_DL1pc(*jet) = pc;
      dec_DL1pu(*jet) = pu;
      
      xAOD::BTaggingUtilities::getBTagging(*jet)->pu("DL1r",pu);
      xAOD::BTaggingUtilities::getBTagging(*jet)->pc("DL1r",pc);
      xAOD::BTaggingUtilities::getBTagging(*jet)->pb("DL1r",pb);

      dec_DL1rpb(*jet) = pb;
      dec_DL1rpc(*jet) = pc;
      dec_DL1rpu(*jet) = pu;

      double charm_score = pc > 0 && (pu >0 || pb >0) ? TMath::Log(pc/(m_ctagging_fb*pb + (1-m_ctagging_fb)*pu)) : -999;

      dec_cscore(*jet) = charm_score;
      //cast the decoration of char into an int so we can count it
      dec_isBtag(*jet) = ((int)jet->auxdata< char >("bjet") >= m_btagBin) && (fabs(jet->eta())  < 2.5)  ;
      dec_isCtag(*jet) = ((int)jet->auxdata< char >("bjet") < m_btagBin) && (fabs(jet->eta())  < 2.5 && charm_score > m_ctagging_cut)  ;
      //for standard b-tagging m_btagBin is 1
      //for pseudo-continuous the value corresponds to the pseudo-contious bin
      //see MasterShef.cxx L661
      bool isBJet = jet->auxdata< bool >("IsBJet");
      if ( isBJet ) {
	m_BJets->push_back(jet);
      }

      if ( (bool) jet->auxdata<bool>("IsCJet") ) {
	m_CJets->push_back(jet);
      }
      if (jet->pt()>35000){m_signalJets35->push_back(jet);}

    }//end of check on signal/baseline/passOR
  }//end of loop over all signal jets before OLR
    

  ATH_MSG_VERBOSE("Stored jets");   



  // --------------------------------------------------------------------
  //if 1,2lep --> add lep to m_signalJets_recl
  //important bit of code to add leptons to be treated as jets if we wish
  // if (m_doLepJet){
  //   if ((m_signalElectrons->size() + m_signalMuons->size())==1){
  //     TLorentzVector particle;
  //     if (m_signalElectrons->size()==1) {
  // 	particle = m_signalElectrons->at(0)->p4();
  //     }     
  //     else if (m_signalMuons->size()==1) {
  // 	particle = m_signalMuons->at(0)->p4();
  //     }
      
  //     //make a fake jet for lepton treatment
  //     xAOD::Jet *tempJet = new xAOD::Jet();
  //     tempJet->makePrivateStore();
  //     tempJet->setJetP4(xAOD::JetFourMom_t(particle.Pt(),particle.Eta(),particle.Phi(),particle.M()));
      
  //     m_signalJets->push_back(tempJet);
  //   }
  // }

  //pT sort the important ones!
  m_signalJets->sort(ptsorter);
  //m_signalJets_recl->sort(ptsorter);
  m_BJets->sort(ptsorter);
  m_CJets->sort(ptsorter);
  m_signalJets35->sort(ptsorter);
  m_baselineJetsAfterOR->sort(ptsorter);


  CHECK( store->record(m_signalJets,"SignalJets"+m_sysName) );
  //CHECK( store->record(m_signalJets_recl,"SignalJets_recl") );
  CHECK( store->record(m_badJets,"BadJets"+m_sysName) );
  CHECK( store->record(m_BJets,"BJets"+m_sysName) );
  CHECK( store->record(m_CJets,"CJets"+m_sysName) );
  CHECK( store->record(m_signalJets35,"SignalJets35"+m_sysName) );
  CHECK( store->record(m_baselineJetsAfterOR,"BaselineJetsAfterOR"+m_sysName) );
 


  //!----------------
  // separate jet container for doing jetsmearing response map builing
  //
  m_rMapRecoJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
  for (auto j : *m_jets_copy) {  
    //bool isbaseline=j->auxdata<char>("baseline");
    //bool passOR=j->auxdata<char>("passOR");
    ///bool issignal=j->auxdata<char>("signal");
    
    if(j->pt() > 20000.  &&  fabs(j->eta()) < 2.8 ){
      ///if(isbaseline && passOR && issignal){
      m_rMapRecoJets->push_back (j);
      //}
    }
  }
  m_rMapRecoJets->sort(ptsorter);
  CHECK(store->record(m_rMapRecoJets,"RMapRecoJets"+m_sysName));

  
  return true;
}
  

bool MasterShef :: prepareLepJets(){

  m_signalLepJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);

  for( xAOD::Jet* tempJet : *m_signalJets){
    m_signalLepJets->push_back(tempJet);
  } 

  for( xAOD::Electron* electron : *m_signalElectrons){
    TLorentzVector particle=electron->p4();
    //make a fake jet for lepton treatment                                              
    xAOD::Jet *tempJet = new xAOD::Jet();
    tempJet->makePrivateStore();
    tempJet->setJetP4(xAOD::JetFourMom_t(particle.Pt(),particle.Eta(),particle.Phi(),particle.M()));
    m_signalLepJets->push_back(tempJet);
  } 

  for( xAOD::Muon* muon : *m_signalMuons){
    TLorentzVector particle=muon->p4();
    //make a fake jet for lepton treatment                                              
    xAOD::Jet *tempJet = new xAOD::Jet();
    tempJet->makePrivateStore();
    tempJet->setJetP4(xAOD::JetFourMom_t(particle.Pt(),particle.Eta(),particle.Phi(),particle.M()));
    m_signalLepJets->push_back(tempJet);
  } 

  m_signalLepJets->sort(ptsorter);
  CHECK( store->record(m_signalLepJets,"SignalLepJets"+m_sysName) );
  
  return true;
} 
// ---------------------------------------------------------------
// Function to prepare track jets for analysis
// ---------------------------------------------------------------
bool MasterShef :: prepareTrackJets() {
 
  m_trackjetsKt4 = 0;
  m_trackjetsKt4aux = 0;
 
  m_signalTrackJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
  m_signalBTrackJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);

  if(m_useTrackJets)
    {

      // For Stop0L SRD, trk jets overlapping with the highest pt emtopo jet are removed.
      // Adding a flag to each jet.
      TLorentzVector p4_nonbjet_lead;
      for( const auto &topojet: *m_signalJets){
	if( fabs(topojet->eta()) >2.8 )
	  continue;
	p4_nonbjet_lead=topojet->p4();
	if(topojet->auxdata< char >("bjet") == false)
	  break;
      }
      
      EL_RETURN_CHECK("GetTrackJets()",m_objTool->GetTrackJets(m_trackjetsKt4,m_trackjetsKt4aux));


      for(const auto &jet: *m_trackjetsKt4){
	double eta = jet->eta();
	// Save only the signal jets
	if( jet->auxdata<char>("signal") == false  )
	  continue;
	if(p4_nonbjet_lead.DeltaR(jet->p4()) > 0.4)
	  jet->auxdata<int>("passISROR")=1;
	else
	  jet->auxdata<int>("passISROR")=0;

	//skip if out of tracker acceptance or doesnt pass the OR with the ISR jet OR to few consituents 
	if( fabs(eta) > 2.5 || jet->auxdata<int>("passISROR")==0 || jet->numConstituents()<2)
	  continue;

	m_signalTrackJets->push_back(jet);
	if( jet->auxdata< char >("bjet") == true && fabs(eta) < 2.5 )
	  m_signalBTrackJets->push_back(jet);
      }  
    }
  
  m_signalTrackJets->sort(ptsorter);
  m_signalBTrackJets->sort(ptsorter);
  CHECK(store->record(m_signalTrackJets,"MySignalTrackJets"+m_sysName));
  CHECK(store->record(m_signalBTrackJets,"MySignalBTrackJets"+m_sysName));
  return true;
  
}

