#include <MasterShef/MasterShef.h>
//#include "CPAnalysisExamples/errorcheck.h"
#include "xAODMetaData/FileMetaData.h"
#include <PathResolver/PathResolver.h>

static const char* APP_NAME = "MasterShef";


MasterShef :: MasterShef (){

  
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().

  //CP::CorrectionCode::enableFailure();

  //StatusCode::enableFailure();
  //xAOD::TReturnCode::enableFailure();
  //xAOD::TFileAccessTracer::enableDataSubmission( false );
  m_applyTrigger="";
  m_ntupLevel=1;

  //m_systBunch = -1 ; 
  m_baselineElPtCut=10000.;
  m_baselineElEtaCut=2.47;
  m_signalElPtCut=10000.;
  m_baselineMuPtCut=10000.;
  m_baselineMuEtaCut=2.4;
  m_signalMuPtCut=10000.;
  m_signalJetPtCut=35000.;
  m_signalJetEtaCut=2.8;
  m_doTST=true;
  // Make sure nominal uncertainty is included first
  m_systNameList.push_back(""); 
   
  // One lepton selection
  // parameters:
  m_Sel1L_Sgnl=true; // True if using signal leps, False if using baseline
  m_Sel1L_PtMnE=26000.; // minimum electron Pt
  m_Sel1L_PtMxE=1.0E8; // maximum electron Pt
  m_Sel1L_PtMnM=26000.; // minumum muon Pt
  m_Sel1L_PtMxM=1.0E8; // maximum muon Pt

  m_SelnBL_Mn=0;
  m_SelnBL_Mx=1.0E8;


  m_SelHT_Mn=0.;
  m_SelHT_Mx=1.0E08;

  // Two lepton or 2 b's selection:
  // parameters:
  m_SelSbottom_PtMnE=26000.; //  minimum electron Pt
  m_SelSbottom_PtMxE=1.0E8;  //  maximum electron Pt
  m_SelSbottom_PtMnM=26000.; //  minumum muon Pt
  m_SelSbottom_PtMxM=1.0E8;; //  maximum muon Pt
   
  // MET selection
  // parameters:
  m_SelMET_PtMn=0.; // minumum met
  m_SelMET_PtMx=1.0E08; // maximum met


  // mct selection
  // parameters:
  m_SelMct_Mn=0.; // minumum met
  m_SelMct_Mx=1.0E08; // maximum met


  // Mt selection
  // parameters:
  m_SelMt_Mn=30000;  // minimum mt
  m_SelMt_Mx=100000; // maximum mt

  // Jet selection
  // parameters
  m_SelJet_nJMx=100; // Maximum number of selected jets
  m_SelJet_nJMn=2; // Maximum number of selected jets

  // tau selection
  // parameters
  m_SelTauJet_nJMx=100; // Maximum number of selected tau jets
  m_SelTauJet_nJMn=1; // Maximum number of selected tau jets

  // N Bjet selection
  // prameters
  m_SelNBJet_nJMn=2; // Min number of b-jets
  m_SelNBJet_nJMx=100; // Max number of b-jets
  m_SelNBTruthJet_nJMn=1; // Min number of b-jets
  m_SelNBTruthJet_nJMx=100; // Max number of b-jets


  //signal lepton selectors
  m_SelnL_Mn=0; //
  m_SelnL_Mx=1000; //

  // Dumper info
  m_cutDumpLvl=1000.;



  //--------------------------------------------------------------------------
  // map to all possible fill modules
  // users can choose to which ones are called and hence which variables are dumped!
  //--------------------------------------------------------------------------
  m_allTreeMods["fillCommonVariables"]       = &MasterShef::fillCommonVariables;
  m_allTreeMods["fillHFHadronTruthVariables"] = &MasterShef::fillHFHadronTruthVariables;
  m_allTreeMods["fillExtraMETVariables"]     = &MasterShef::fillExtraMETVariables;
  m_allTreeMods["fillExtraRecoVariables"]    = &MasterShef::fillExtraRecoVariables;
  m_allTreeMods["fillLepJetVariables"]       = &MasterShef::fillLepJetVariables;
  m_allTreeMods["fillExtraTruthVariables"]   = &MasterShef::fillExtraTruthVariables;
  m_allTreeMods["fillFatJetVariables"]       = &MasterShef::fillFatJetVariables;
  m_allTreeMods["fillHFClassVariables"]      = &MasterShef::fillHFClassVariables;
  m_allTreeMods["fillJetConstituents"]       = &MasterShef::fillJetConstituents;
  m_allTreeMods["fillRCFatJetVariables"]     = &MasterShef::fillRCFatJetVariables;
  m_allTreeMods["fillSBVVariables"]          = &MasterShef::fillSBVVariables;
  m_allTreeMods["fillTrackJetVariables"]     = &MasterShef::fillTrackJetVariables;
  m_allTreeMods["fillSmearingVariables"]     = &MasterShef::fillSmearingVariables;
  m_allTreeMods["fillTrackJetConstituents"]  = &MasterShef::fillTrackJetConstituents;
  m_allTreeMods["fillTruthJetConstituents"]  = &MasterShef::fillTruthJetConstituents;
  m_allTreeMods["fillTruthJets"]             = &MasterShef::fillTruthJets;
  m_allTreeMods["fillTruthTaus"]             = &MasterShef::fillTruthTaus;
  m_allTreeMods["fillxAODJets"]              = &MasterShef::fillxAODJets;
  m_allTreeMods["fillRMap"]                  = &MasterShef::fillRMap;
  m_allTreeMods["fillTruthLeptons"]   = &MasterShef::fillTruthLeptons;

  //--------------------------------------------------------------------------
  // map to all possible selections  modules
  // users can choose to which ones are called making selection pluggable
  //--------------------------------------------------------------------------
  m_allSelMods["prepareObjects"]      = &MasterShef::prepareObjects;
  m_allSelMods["prepareTruthObjects"] = &MasterShef::prepareTruthObjects;
  m_allSelMods["initialCuts"]	       = &MasterShef::initialCuts;
  m_allSelMods["Sel0Lep"]	       = &MasterShef::Sel0Lep;
  m_allSelMods["Sel1L"]	       = &MasterShef::Sel1L;
  m_allSelMods["Sel1Ph"]	       = &MasterShef::Sel1Ph;
  m_allSelMods["Sel2L"]	       = &MasterShef::Sel2L;
  m_allSelMods["SelBosonBalance"]     = &MasterShef::SelBosonBalance;
  m_allSelMods["SelDiJet"]     = &MasterShef::SelDiJet;
  m_allSelMods["SelDMbb"]	       = &MasterShef::SelDMbb;
  m_allSelMods["SelGammaBalance"]     = &MasterShef::SelGammaBalance;
  m_allSelMods["SelHT"]	       = &MasterShef::SelHT;
  m_allSelMods["SelJet"]	       = &MasterShef::SelJet;
  m_allSelMods["SelJetMET"]	       = &MasterShef::SelJetMET;
  m_allSelMods["SelLeadB"]	       = &MasterShef::SelLeadB;
  m_allSelMods["SelMET"]	       = &MasterShef::SelMET;
  m_allSelMods["SelMct"]	       = &MasterShef::SelMct;
  m_allSelMods["SelMt"]	       = &MasterShef::SelMt;
  m_allSelMods["SelNBJet"]	       = &MasterShef::SelNBJet;
  m_allSelMods["SelNBTruthJet"]       = &MasterShef::SelNBTruthJet;
  m_allSelMods["SelNBaselineLep"]     = &MasterShef::SelNBaselineLep;
  m_allSelMods["SelNL"]	       = &MasterShef::SelNL;
  m_allSelMods["SelOnZ"]	       = &MasterShef::SelOnZ;
  m_allSelMods["SelPseudoEvents"]     = &MasterShef::SelPseudoEvents;
  m_allSelMods["SelPseudoEventsDiJet"]     = &MasterShef::SelPseudoEventsDiJet;
  m_allSelMods["SelSBTriggers"]       = &MasterShef::SelSBTriggers;
  m_allSelMods["SelSJTriggers"]       = &MasterShef::SelSJTriggers; 
  m_allSelMods["SelSbottom"]	       = &MasterShef::SelSbottom;
  m_allSelMods["SelSbottomMultib"]    = &MasterShef::SelSbottomMultib;
  m_allSelMods["SelSbottomTight"]     = &MasterShef::SelSbottomTight;
  m_allSelMods["SelbbMeT"]            = &MasterShef::SelbbMeT;
  m_allSelMods["SelSeedEvents"]       = &MasterShef::SelSeedEvents;
  m_allSelMods["SelStop0L"]	       = &MasterShef::SelStop0L;
  m_allSelMods["SelStop4Body"]	       = &MasterShef::SelStop4Body;
  m_allSelMods["SelStop4BodyTight"]    = &MasterShef::SelStop4BodyTight;
  m_allSelMods["SelTauJet"]	       = &MasterShef::SelTauJet;
  m_allSelMods["SelTruthMET"]	       = &MasterShef::SelTruthMET;
  m_allSelMods["SelttZ"]	       = &MasterShef::SelttZ;
  m_allSelMods["SelttZ2L"]	       = &MasterShef::SelttZ2L;
  m_allSelMods["SelttZ3L"]	       = &MasterShef::SelttZ3L;
  m_allSelMods["cleanJets"]	       = &MasterShef::cleanJets;
  m_allSelMods["cleanMuons"]	       = &MasterShef::cleanMuons;
  m_allSelMods["cleanTightJets"]      = &MasterShef::cleanTightJets;
  m_allSelMods["cosmic"]	       = &MasterShef::cosmic;
  m_allSelMods["eventDump"]	       = &MasterShef::eventDump;
  m_allSelMods["triggerMatching"]     = &MasterShef::triggerMatching;
  m_allSelMods["SelStop0LTight"]      =&MasterShef::SelStop0LTight;
  m_allSelMods["SeltcMET"]	       = &MasterShef::SeltcMET;
  m_allSelMods["SeltcMETTight"]	       = &MasterShef::SeltcMETTight;

  m_trackSelTool = 0;

}



 EL::StatusCode MasterShef :: setupJob (EL::Job& job) {
   Info( APP_NAME,"Entering stupJob");
   // Here you put code that sets up the job on the submission object
   // so that it is ready to work with your algorithm, e.g. you can
   // request the D3PDReader service or add output files.  Any code you
   // put here could instead also go into the submission script.  The
   // sole advantage of putting it here is that it gets automatically
   // activated/deactivated when you add/remove the algorithm from your
   // job, which may or may not be of value to you.

   // let's initialize the algorithm to use the xAODRootAccess package
   job.useXAOD ();
   
   //Info( APP_NAME,"finished useXAOD");
   EL_RETURN_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file

   Info( APP_NAME,"finished stupJob");

   return EL::StatusCode::SUCCESS; 
}




 EL::StatusCode MasterShef :: histInitialize () {
   Info( APP_NAME,"Entering histInitialize");

   // Here you do everything that needs to be done at the very
   // beginning on each worker node, e.g. create histograms and output
   // trees.  This method gets called before any input files are
   // connected.

   //metadata histogram
   //m_MyMetaData = new TH1D("MyMetaData","MyMetaData",100,-0.5,99.5);
   //wk()// ->addOutput(m_MyMetaData);

   //
   // Print the names to check what's here!
  //
   for (unsigned int ii=0; ii<m_systNameList.size(); ii++) {
     std::string Hname=m_systNameList.at(ii);
     Info(APP_NAME,"name[%d] = %s",ii,Hname.c_str());
   }
  
   TFile *outputFile = wk()->getOutputFile(m_outputName);
   outputFile->cd();
   // Cutflow histograms
   for (unsigned int ii=0; ii<m_systNameList.size(); ii++) {
      std::string Hname="H"+m_systNameList.at(ii);
      if (Hname == "H") Hname = "HNominal";
      TH1D* cutFlow = new TH1D(Hname.c_str(),"Cutflow results",100,-0.5,99.5);
      m_cutFlow.push_back(cutFlow);
      if( ( Hname == "HNominal" && m_systBunch <= 0 ) || Hname != "HNominal" )
	m_cutFlow.at(ii)->SetDirectory(outputFile);
      
      //sumWeights(nominal) cutflow
      TH1D* cutFlowWgt = new TH1D((Hname+"_Wgt").c_str(),"Cutflow results",100,-0.5,99.5);
      m_cutFlowWgt.push_back(cutFlowWgt);
      if( (Hname == "HNominal" && m_systBunch <= 0) || Hname != "HNominal"  )
	m_cutFlowWgt.at(ii)->SetDirectory(outputFile);
      
      //sumWeights(muR05_muF10) cutflow
      TH1D* cutFlowWgt_muR05_muF10 = new TH1D((Hname+"_Wgt_muR05_muF10").c_str(),"Cutflow results",100,-0.5,99.5);
      m_cutFlowWgt_muR05_muF10.push_back(cutFlowWgt_muR05_muF10);
      if( (Hname == "HNominal" && m_systBunch <= 0) || Hname != "HNominal"  )
	m_cutFlowWgt_muR05_muF10.at(ii)->SetDirectory(outputFile);
      
      //sumWeights(muR20_muF10) cutflow
      TH1D* cutFlowWgt_muR20_muF10 = new TH1D((Hname+"_Wgt_muR20_muF10").c_str(),"Cutflow results",100,-0.5,99.5);
      m_cutFlowWgt_muR20_muF10.push_back(cutFlowWgt_muR20_muF10);
      if( (Hname == "HNominal" && m_systBunch <= 0) || Hname != "HNominal"  )
	m_cutFlowWgt_muR20_muF10.at(ii)->SetDirectory(outputFile);
      
      //sumWeights(muR10_muF05) cutflow
      TH1D* cutFlowWgt_muR10_muF05 = new TH1D((Hname+"_Wgt_muR10_muF05").c_str(),"Cutflow results",100,-0.5,99.5);
      m_cutFlowWgt_muR10_muF05.push_back(cutFlowWgt_muR10_muF05);
      if( (Hname == "HNominal" && m_systBunch <= 0) || Hname != "HNominal"  )
	m_cutFlowWgt_muR10_muF05.at(ii)->SetDirectory(outputFile);
      
      //sumWeights(muR10_muF20) cutflow
      TH1D* cutFlowWgt_muR10_muF20 = new TH1D((Hname+"_Wgt_muR10_muF20").c_str(),"Cutflow results",100,-0.5,99.5);
      m_cutFlowWgt_muR10_muF20.push_back(cutFlowWgt_muR10_muF20);
      if( (Hname == "HNominal" && m_systBunch <= 0) || Hname != "HNominal"  )
	m_cutFlowWgt_muR10_muF20.at(ii)->SetDirectory(outputFile);
      
      //sumWeights(muR05_muR05) cutflow
      TH1D* cutFlowWgt_muR05_muF05 = new TH1D((Hname+"_Wgt_muR05_muF05").c_str(),"Cutflow results",100,-0.5,99.5);
      m_cutFlowWgt_muR05_muF05.push_back(cutFlowWgt_muR05_muF05);
      if( (Hname == "HNominal" && m_systBunch <= 0) || Hname != "HNominal"  )
	m_cutFlowWgt_muR05_muF05.at(ii)->SetDirectory(outputFile);
    
      //sumWeights(muR20_muF20) cutflow
      TH1D* cutFlowWgt_muR20_muF20 = new TH1D((Hname+"_Wgt_muR20_muF20").c_str(),"Cutflow results",100,-0.5,99.5);
      m_cutFlowWgt_muR20_muF20.push_back(cutFlowWgt_muR20_muF20);
      if( (Hname == "HNominal" && m_systBunch <= 0) || Hname != "HNominal"  )
	m_cutFlowWgt_muR20_muF20.at(ii)->SetDirectory(outputFile);
      
      //sumWeights(ISRmuRfac10_FSRmuRfac20) cutflow
      TH1D* cutFlowWgt_ISRmuRfac10_FSRmuRfac20 = new TH1D((Hname+"_Wgt_ISRmuRfac10_FSRmuRfac20").c_str(),"Cutflow results",100,-0.5,99.5);
      m_cutFlowWgt_ISRmuRfac10_FSRmuRfac20.push_back(cutFlowWgt_ISRmuRfac10_FSRmuRfac20);
      if( (Hname == "HNominal" && m_systBunch <= 0) || Hname != "HNominal"  )
	m_cutFlowWgt_ISRmuRfac10_FSRmuRfac20.at(ii)->SetDirectory(outputFile);
      
      //sumWeights(ISRmuRfac10_FSRmuRfac05) cutflow
      TH1D* cutFlowWgt_ISRmuRfac10_FSRmuRfac05 = new TH1D((Hname+"_Wgt_ISRmuRfac10_FSRmuRfac05").c_str(),"Cutflow results",100,-0.5,99.5);
      m_cutFlowWgt_ISRmuRfac10_FSRmuRfac05.push_back(cutFlowWgt_ISRmuRfac10_FSRmuRfac05);
      if( (Hname == "HNominal" && m_systBunch <= 0) || Hname != "HNominal"  )
	m_cutFlowWgt_ISRmuRfac10_FSRmuRfac05.at(ii)->SetDirectory(outputFile);
    

      TH1D* cutFlowPUWgt = new TH1D((Hname+"_PUWgt").c_str(),"Cutflow results",100,-0.5,99.5);
      m_cutFlowPUWgt.push_back(cutFlowPUWgt);
      if( (Hname == "HNominal" && m_systBunch <= 0) || Hname != "HNominal"  )
	m_cutFlowPUWgt.at(ii)->SetDirectory(outputFile);

   }

   
   // Ntuple stuff
   // get the output file, create a new TTree and connect it to that output
   // define what braches will go in that tree
   
   for (unsigned int ii=0; ii<m_systNameList.size(); ii++) {
      std::string Hname=m_systNameList.at(ii);
      if (Hname == "") Hname = "Nominal";
      TTree* tree = new TTree (Hname.c_str(), "m_tree");
      m_tree.push_back(tree);
      if( Hname == "Nominal" && m_systBunch > 0  )
	continue;
      m_tree.at(ii)->SetDirectory(outputFile);
   }

  
   // Prepare for cuts
   m_cuts.resize(100,"");


   return EL::StatusCode::SUCCESS; 
 }





 EL::StatusCode MasterShef :: fileExecute () {

  Info( APP_NAME,"Entering fileExecute");
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  // Get xAOD event
  event = wk()->xaodEvent();

  // the truth jet collection name might have changed: AntiKt4TruthJets -> WZTrimmed or something
  m_TruthJetsPresent=true;
  xAOD::JetContainer *truthJets(0);
  if(!event->retrieve(truthJets,"AntiKt4TruthJets").isSuccess()){   
    m_TruthJetsPresent=false;
  }

  //retrieve the event info

  //Read the CutBookkeeper container
  const xAOD::CutBookkeeperContainer* completeCBC = 0;
  if (!event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()) {
    Error( APP_NAME, "Failed to retrieve CutBookkeepers from MetaData! Exiting.");
  }

  // new code to retrive the xStream from the fileMetaData
  const xAOD::FileMetaData* fmd = nullptr;
  CHECK(event->retrieveMetaInput(fmd, "FileMetaData"));
  xStream=fmd->auxdata<std::string>("dataType");


  SG::AuxElement::ConstAccessor<std::string> acc_simFlavour("simFlavour");

  std::string simFlav="None";
  if (acc_simFlavour.isAvailable(*fmd)){
    simFlav=acc_simFlavour(*fmd);
  }
  ATH_MSG_INFO("Checking the simFlavour and dataSource ... " << simFlav << " " << m_dataSource);


  const xAOD::CutBookkeeper* allEventsCBK2 = 0;
  const xAOD::CutBookkeeper* allEventsCBK_muR05_muF10 = 0;
  const xAOD::CutBookkeeper* allEventsCBK_muR20_muF10 = 0;
  const xAOD::CutBookkeeper* allEventsCBK_muR10_muF05 = 0;
  const xAOD::CutBookkeeper* allEventsCBK_muR10_muF20 = 0;
  const xAOD::CutBookkeeper* allEventsCBK_muR05_muF05 = 0;
  const xAOD::CutBookkeeper* allEventsCBK_muR20_muF20 = 0;
  const xAOD::CutBookkeeper* allEventsCBK_ISRmuRfac10_FSRmuRfac20 = 0;
  const xAOD::CutBookkeeper* allEventsCBK_ISRmuRfac10_FSRmuRfac05 = 0;
  //const xAOD::CutBookkeeper* allEventsCBK_Var3cUp = 0;

  int maxcycle=-1;
  int maxcycle_muR05_muF10=-1;
  int maxcycle_muR20_muF10=-1;
  int maxcycle_muR10_muF05=-1;
  int maxcycle_muR10_muF20=-1;
  int maxcycle_muR05_muF05=-1;
  int maxcycle_muR20_muF20=-1;
  int maxcycle_ISRmuRfac10_FSRmuRfac20=-1;
  int maxcycle_ISRmuRfac10_FSRmuRfac05=-1;
  //int maxcycle_Var3cUp=-1;
  //let's find the right CBK (latest with StreamAOD input before derivations)
  if (!m_doTruth)  {

    
    //find the CBK for some variations
    for ( auto cbk : *completeCBC ) {
      std::string name = cbk->name();
      if ( name == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxcycle){
        maxcycle = cbk->cycle();
        allEventsCBK2 = cbk;
      }             
      else if((name== "LHE3Weight_muR=05,muF=10" || name == "LHE3Weight_muR=050000E+00muF=010000E+01" || name == "LHE3Weight_MUR05_MUF10_PDF260000") && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxcycle_muR05_muF10){
        maxcycle_muR05_muF10 = cbk->cycle();
        allEventsCBK_muR05_muF10 = cbk;
      }
      else if((name== "LHE3Weight_muR=20,muF=10" || name == "LHE3Weight_muR=020000E+01muF=010000E+01" || name == "LHE3Weight_MUR20_MUF10_PDF260000")  && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxcycle_muR20_muF10){
        maxcycle_muR20_muF10 = cbk->cycle();
	      allEventsCBK_muR20_muF10 = cbk;
      }
      else if((name== "LHE3Weight_muR=10,muF=05" || name == "LHE3Weight_muR=010000E+01muF=050000E+00" || name == "LHE3Weight_MUR10_MUF05_PDF260000") && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxcycle_muR10_muF05){
        maxcycle_muR10_muF05 = cbk->cycle();
        allEventsCBK_muR10_muF05 = cbk;
      }                                                      
      else if((name== "LHE3Weight_muR=10,muF=20" || name == "LHE3Weight_muR=010000E+01muF=020000E+01" || name == "LHE3Weight_MUR10_MUF20_PDF260000") && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxcycle_muR10_muF20){
        maxcycle_muR10_muF20 = cbk->cycle();
        allEventsCBK_muR10_muF20 = cbk;
      }
      else if((name== "LHE3Weight_muR=05,muF=05" || name == "LHE3Weight_muR=050000E+00muF=050000E+00" || name == "LHE3Weight_MUR05_MUF05_PDF260000") && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxcycle_muR05_muF05){
        maxcycle_muR05_muF05 = cbk->cycle();
        allEventsCBK_muR05_muF05 = cbk;
      }
      else if((name== "LHE3Weight_muR=20,muF=20" || name == "LHE3Weight_muR=020000E+01muF=020000E+01" || name == "LHE3Weight_MUR20_MUF20_PDF260000") && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxcycle_muR20_muF20){
        maxcycle_muR20_muF20 = cbk->cycle();
        allEventsCBK_muR20_muF20 = cbk;
      }     
      else if(name== "LHE3Weight_isr:muRfac=10_fsr:muRfac=20" && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxcycle_ISRmuRfac10_FSRmuRfac20){
        maxcycle_ISRmuRfac10_FSRmuRfac20 = cbk->cycle();
        allEventsCBK_ISRmuRfac10_FSRmuRfac20 = cbk;
      }
      else if(name== "LHE3Weight_isr:muRfac=10_fsr:muRfac=05" && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxcycle_ISRmuRfac10_FSRmuRfac05){
        maxcycle_ISRmuRfac10_FSRmuRfac05 = cbk->cycle();
        allEventsCBK_ISRmuRfac10_FSRmuRfac05 = cbk;
      }
      // else if(name== "LHE3Weight_Var3cUp" && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxcycle_Var3cUp){
      // 	maxcycle_Var3cUp = cbk->cycle();
      // 	allEventsCBK_Var3cUp = cbk;
      // }      
    }
     
    //nominal sumOfWeights
    if (allEventsCBK2) {
      uint64_t nEventsProcessed  = allEventsCBK2->nAcceptedEvents();
      double sumOfWeights        = allEventsCBK2->sumOfEventWeights();
      double sumOfWeightsSquared = allEventsCBK2->sumOfEventWeightsSquared();
      Info( APP_NAME, "CutBookkeepersNew Accepted %lu SumWei %f sumWei2 %f ", nEventsProcessed, sumOfWeights, sumOfWeightsSquared);
      for (unsigned int ii=0; ii<m_systNameList.size(); ii++) {
        m_cutFlow.at(ii)->Fill(0.0,(double) nEventsProcessed);
        m_cutFlowWgt.at(ii)->Fill(0.0,sumOfWeights);
      }
    } 
    else 
      { 
	Info( APP_NAME, "No relevent CutBookKeepers found" ); 
      }

    //muR05_muF10 
    if(allEventsCBK_muR05_muF10) {
      uint64_t nEventsProcessed  = allEventsCBK_muR05_muF10->nAcceptedEvents();
      double sumOfWeights        = allEventsCBK_muR05_muF10->sumOfEventWeights();
      double sumOfWeightsSquared = allEventsCBK_muR05_muF10->sumOfEventWeightsSquared();
      Info( APP_NAME, "muR05_muF10 Accepted %lu SumWei %f sumWei2 %f ", nEventsProcessed, sumOfWeights, sumOfWeightsSquared);
      for (unsigned int ii=0; ii<m_systNameList.size(); ii++) {
	m_cutFlowWgt_muR05_muF10.at(ii)->Fill(0.0,sumOfWeights);
      }

    }
    else 
      { 
	Info( APP_NAME, "Was not able to find in  CutBookKeepers muR05_muF10!!" ); 
      }

    //muR20_muF10
    if(allEventsCBK_muR20_muF10) {
      uint64_t nEventsProcessed  = allEventsCBK_muR20_muF10->nAcceptedEvents();
      double sumOfWeights        = allEventsCBK_muR20_muF10->sumOfEventWeights();
      double sumOfWeightsSquared = allEventsCBK_muR20_muF10->sumOfEventWeightsSquared();
      Info( APP_NAME, "muR20_muF10 Accepted %lu SumWei %f sumWei2 %f ", nEventsProcessed, sumOfWeights, sumOfWeightsSquared);
      for (unsigned int ii=0; ii<m_systNameList.size(); ii++) {
	m_cutFlowWgt_muR20_muF10.at(ii)->Fill(0.0,sumOfWeights);
      }

    }
    else 
      { 
	Info( APP_NAME, "Was not able to find in  CutBookKeepers muR20_muF10!!" ); 
      }

    
    //muR10_muF05 
    if(allEventsCBK_muR10_muF05) {
      uint64_t nEventsProcessed  = allEventsCBK_muR10_muF05->nAcceptedEvents();
      double sumOfWeights        = allEventsCBK_muR10_muF05->sumOfEventWeights();
      double sumOfWeightsSquared = allEventsCBK_muR10_muF05->sumOfEventWeightsSquared();
      Info( APP_NAME, "muR10_muF05 Accepted %lu SumWei %f sumWei2 %f ", nEventsProcessed, sumOfWeights, sumOfWeightsSquared);
      for (unsigned int ii=0; ii<m_systNameList.size(); ii++) {
	m_cutFlowWgt_muR10_muF05.at(ii)->Fill(0.0,sumOfWeights);
      }

    }
    else 
      { 
	Info( APP_NAME, "Was not able to find in  CutBookKeepers muR10_muF05!!" ); 
      }

    //muR10_muF20 
    if(allEventsCBK_muR10_muF20) {
      uint64_t nEventsProcessed  = allEventsCBK_muR10_muF20->nAcceptedEvents();
      double sumOfWeights        = allEventsCBK_muR10_muF20->sumOfEventWeights();
      double sumOfWeightsSquared = allEventsCBK_muR10_muF20->sumOfEventWeightsSquared();
      Info( APP_NAME, "muR10_muF20 Accepted %lu SumWei %f sumWei2 %f ", nEventsProcessed, sumOfWeights, sumOfWeightsSquared);
      for (unsigned int ii=0; ii<m_systNameList.size(); ii++) {
	m_cutFlowWgt_muR10_muF20.at(ii)->Fill(0.0,sumOfWeights);
      }

    }
    else 
      { 
	Info( APP_NAME, "Was not able to find in  CutBookKeepers muR10_muF20!!" ); 
      }


    //muR05_muF05 for RadHi ttbar sample
    if(allEventsCBK_muR05_muF05) {
      uint64_t nEventsProcessed  = allEventsCBK_muR05_muF05->nAcceptedEvents();
      double sumOfWeights        = allEventsCBK_muR05_muF05->sumOfEventWeights();
      double sumOfWeightsSquared = allEventsCBK_muR05_muF05->sumOfEventWeightsSquared();
      Info( APP_NAME, "muR05_muF05 Accepted %lu SumWei %f sumWei2 %f ", nEventsProcessed, sumOfWeights, sumOfWeightsSquared);
      for (unsigned int ii=0; ii<m_systNameList.size(); ii++) {
	m_cutFlowWgt_muR05_muF05.at(ii)->Fill(0.0,sumOfWeights);
      }

    }
    else 
      { 
	Info( APP_NAME, "Was not able to find in  CutBookKeepers muR05_muF05!!" ); 
      }

    //muR20_muF20 for RadLow ttbar sample
    if(allEventsCBK_muR20_muF20) {
      uint64_t nEventsProcessed  = allEventsCBK_muR20_muF20->nAcceptedEvents();
      double sumOfWeights        = allEventsCBK_muR20_muF20->sumOfEventWeights();
      double sumOfWeightsSquared = allEventsCBK_muR20_muF20->sumOfEventWeightsSquared();
      Info( APP_NAME, "muR20_muF20 Accepted %lu SumWei %f sumWei2 %f ", nEventsProcessed, sumOfWeights, sumOfWeightsSquared);
      for (unsigned int ii=0; ii<m_systNameList.size(); ii++) {
	m_cutFlowWgt_muR20_muF20.at(ii)->Fill(0.0,sumOfWeights);
      }

    }
    else 
      { 
	Info( APP_NAME, "Was not able to find in  CutBookKeepers muR20_muF20!!" ); 
      }

    if(allEventsCBK_ISRmuRfac10_FSRmuRfac20) {
      uint64_t nEventsProcessed  = allEventsCBK_ISRmuRfac10_FSRmuRfac20->nAcceptedEvents();
      double sumOfWeights        = allEventsCBK_ISRmuRfac10_FSRmuRfac20->sumOfEventWeights();
      double sumOfWeightsSquared = allEventsCBK_ISRmuRfac10_FSRmuRfac20->sumOfEventWeightsSquared();
      Info( APP_NAME, "ISRmuRfac10_FSRmuRfac20 Accepted %lu SumWei %f sumWei2 %f ", nEventsProcessed, sumOfWeights, sumOfWeightsSquared);
      for (unsigned int ii=0; ii<m_systNameList.size(); ii++) {
  m_cutFlowWgt_ISRmuRfac10_FSRmuRfac20.at(ii)->Fill(0.0,sumOfWeights);
      }

    }
    else 
      { 
  Info( APP_NAME, "Was not able to find in  CutBookKeepers ISRmuRfac10_FSRmuRfac20!!" ); 
      }

    if(allEventsCBK_ISRmuRfac10_FSRmuRfac05) {
      uint64_t nEventsProcessed  = allEventsCBK_ISRmuRfac10_FSRmuRfac05->nAcceptedEvents();
      double sumOfWeights        = allEventsCBK_ISRmuRfac10_FSRmuRfac05->sumOfEventWeights();
      double sumOfWeightsSquared = allEventsCBK_ISRmuRfac10_FSRmuRfac05->sumOfEventWeightsSquared();
      Info( APP_NAME, "ISRmuRfac10_FSRmuRfac05 Accepted %lu SumWei %f sumWei2 %f ", nEventsProcessed, sumOfWeights, sumOfWeightsSquared);
      for (unsigned int ii=0; ii<m_systNameList.size(); ii++) {
  m_cutFlowWgt_ISRmuRfac10_FSRmuRfac05.at(ii)->Fill(0.0,sumOfWeights);
      }

    }
    else 
      { 
  Info( APP_NAME, "Was not able to find in  CutBookKeepers ISRmuRfac10_FSRmuRfac05!!" ); 
      }

    //Var3cUp and Var3cDown are left out because they are almost identical to the nominal
    // if(allEventsCBK_Var3cUp) {
    //   uint64_t nEventsProcessed  = allEventsCBK_Var3cUp->nAcceptedEvents();
    //   double sumOfWeights        = allEventsCBK_Var3cUp->sumOfEventWeights();
    //   double sumOfWeightsSquared = allEventsCBK_Var3cUp->sumOfEventWeightsSquared();
    //   Info( APP_NAME, "Var3cUp Accepted %lu SumWei %f sumWei2 %f ", nEventsProcessed, sumOfWeights, sumOfWeightsSquared);
    // }
    // else 
    //   { 
    // 	Info( APP_NAME, "Was not able to find in  CutBookKeepers Var3cUp!!" ); 
    //   }


  }
  else {
    //force truth3 here to stop truth photons being loaded
    xStream="DAOD_TRUTH3";
    try{
      TTree*  m_ctree;
      m_ctree = dynamic_cast<TTree*> (wk()->inputFile()->Get("CollectionTree"));
      uint64_t nEventsProcessed      = m_ctree->GetEntries();
      double sumOfWeights    = 0;//m_ctree->GetEntries();

      //for(unsigned int ii=0; ii< nEventsProcessed; ii++){/
      //   std::cout << ii << std::endl;
      // }

      //double sumOfWeightsSquared   = m_ctree->GetEntries();
      for (unsigned int ii=0; ii<m_systNameList.size(); ii++) {
        m_cutFlow.at(ii)->Fill(0.0,(double) nEventsProcessed);
        m_cutFlowWgt.at(ii)->Fill(0.0,sumOfWeights);
      }
    }
    catch(...){
    // pass
    }
  }

  /*
  delete allEventsCBK2;
  delete allEventsCBK_muR05_muF10;
  delete allEventsCBK_muR20_muF10;
  delete allEventsCBK_muR10_muF05;
  delete allEventsCBK_muR10_muF20;
  delete allEventsCBK_muR05_muF05;
  delete allEventsCBK_muR20_muF20;
  delete allEventsCBK_ISRmuRfac10_FSRmuRfac20;
  delete allEventsCBK_ISRmuRfac10_FSRmuRfac05;
  */
  return EL::StatusCode::SUCCESS; 
 }


 EL::StatusCode MasterShef :: changeInput (bool firstFile) {
   Info( APP_NAME,"Entering changeInput, first file?=%i",firstFile);
   // Here you do everything you need to do when we change input files,
   // e.g. resetting branch addresses on trees.  If you are using
   // D3PDReader or a similar service this method is not needed.
   return EL::StatusCode::SUCCESS; }



 EL::StatusCode MasterShef :: initialize () {
   // Here you do everything that you need to do after the first input
   // file has been connected and before the first event is processed,
   // e.g. create additional histograms based on which variables are
   // available in the input files.  You can also create all of your
   // histograms and trees in here, but be aware that this method
   // doesn't get called if no events are processed.  So any objects
   // you create here won't be available in the output if you have no
   // input events.

   //retrieve the path
   m_path=gSystem->ExpandPathName(m_basePath.c_str());

   Info("initialize()", "path=%s",m_path.c_str());
 

   if( !m_useAntiKt10Jets_ST && m_saveFatJetExtraVariables){
     ATH_MSG_WARNING("Pay attention, you cannot ask to save extra fatjet variables and have m_useAntiKt10Jets_ST turned off. Turning m_saveFatJetExtraVariables off.");
     m_saveFatJetExtraVariables=false;
   }


   // Get xAOD event
   event = wk()->xaodEvent();

   if(m_doxAOD){
     TFile* xaod_file = wk()->getOutputFile(m_outputName);
     CHECK(event->writeTo(xaod_file));
   }


   
   m_cutLevel=1; //reserve 0 for metadata
   m_cuts[0]="Metadata Tot";


   //retrieve the event info
   eventInfo = 0;
   EL_RETURN_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));

   m_isMC = false;
   if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION )){
     m_isMC= true;
   }


   std::map<int, int> continuousBTagBin;
   continuousBTagBin.insert(std::make_pair(60, 5));
   continuousBTagBin.insert(std::make_pair(70, 4));
   continuousBTagBin.insert(std::make_pair(77, 3));
   continuousBTagBin.insert(std::make_pair(85, 2));
   continuousBTagBin.insert(std::make_pair(100, 1));
   
   m_btagBin = 1; // This is defaulted to this value, as it shouldn't be used unless we're using continuous, if we are using continuous, it'll change to the correct bin for the continuous. Otherwise it will never be used.
   if (m_contWP != 0){
     for (auto itr = continuousBTagBin.find(m_contWP); itr != continuousBTagBin.end(); itr++){
       if (itr->first == m_contWP){
	 m_btagBin = itr->second;
       }
     }
   }
   

// Cheatsheet for continuous b-tagging:
// returns 5 if between 60% and 0%
// returns 4 if between 70% and 60%
// returns 3 if between 77% and 70%
// returns 2 if between 85% and 77%
// returns 1 if between 100% and 85%
// returns 0 if smaller than -1e4-> should never happen
// return -1 if bigger than 1e4 or not in b-tagging acceptance



   // as a check, let's see the number of events in our xAOD
   Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

 

   // count number of events
   m_eventCounter = 0;
   // This is the function where the cuts are prepared
   CHECK(prepareCuts());
  
 
   // let's no bother if there's no entries
   if( event->getEntries() == 0 )
     return EL::StatusCode::SUCCESS;



   // Print out our configuration
   Info("initialize()", "List of parameter configuration");
   Info("initialize()","Applying trigger =%s", m_applyTrigger.c_str());
   Info("initialize()","Baseline El Pt Cut=%f",m_baselineElPtCut);
   Info("initialize()","Baseline El eta Cut=%f",m_baselineElEtaCut);
   Info("initialize()","Signal El Pt Cut=%f",m_signalElPtCut);
   Info("initialize()","Baseline Mu Pt Cut=%f",m_baselineMuPtCut);
   Info("initialize()","Baseline Mu eta Cut=%f",m_baselineMuEtaCut);
   Info("initialize()","Baseline Jet Pt Cut=%f",m_signalJetPtCut);
   Info("initialize()","Baseline Jet eta cut=%f",m_signalJetEtaCut);
   Info("initialize()","MET do TST = %i",m_doTST);
   Info("initialize()","usePhotons = %i",m_usePhotons);
   Info("initialize()","showerType = %i",m_showerType);
   Info("initialize()","Ntuple written at level = %i",m_ntupLevel);

   // Loop over cut initializers
   //Info("CutInit()","");
   for (int ii = 0; ii<m_totCuts; ii++) {
     (this->*m_funExec[ii])(true);
     Info("initialize()","cutLevel = %d ",ii);
   }
   //Info("CutInit()","");
   for (unsigned int ii=0; ii< m_triggerList.size(); ii++)
      Info("initialize()","Trigger selection %u = %s",ii,m_triggerList.at(ii).c_str());


   //initialise cpu clock
   clock = new TStopwatch();

   //mctlib
   mcttool = new TMctLib();
  
   CHECK(GetJetToolRunner(m_jetRecTool_kt15,1.5,"SignalJets","MyFatJetsKt15"));
   Info(APP_NAME,"Setup Kt12 jet tool");
   CHECK(GetJetToolRunner(m_jetRecTool_kt12,1.2,"SignalJets","MyFatJetsKt12"));
   Info(APP_NAME,"Setup Kt12 jet tool");
   CHECK(GetJetToolRunner(m_jetRecTool_kt10,1.0,"SignalJets","MyFatJetsKt10"));
   Info(APP_NAME,"Setup Kt12 jet tool");
   CHECK(GetJetToolRunner(m_jetRecTool_kt8,0.8,"SignalJets","MyFatJetsKt8"));
   Info(APP_NAME,"Setup Kt8 jet tool");
   

   // CHECK(GetJetToolRunner(m_jetRecTool_smrkt12,1.2,"GoodSMRJets","MySMRFatJetsKt12"));
   // Info(APP_NAME,"Setup Kt12 jet tool");
   // CHECK(GetJetToolRunner(m_jetRecTool_smrkt8,0.8,"GoodSMRJets","MySMRFatJetsKt8"));
   // Info(APP_NAME,"Setup Kt8 jet tool");
   

   m_jetPseudoRetTool = new JetPseudojetRetriever("PseudoRetriever");
   CHECK( m_jetPseudoRetTool->initialize());


   m_jetConstRetTool = new JetConstituentsRetriever("ConstRetriever");
   ToolHandle<IJetPseudojetRetriever> m_hpjr(m_jetPseudoRetTool);  
   
   CHECK( m_jetConstRetTool->setProperty("PseudojetRetriever",m_hpjr));
   CHECK( m_jetConstRetTool->initialize());
   
   if(m_doJetSmearing){

     //need to set up a reclustering too for each smear!
     for(unsigned int i=0; i<m_nsmears; i++){
       std::string smr_name = "SMR"+std::to_string(i);
       
       JetToolRunner* smr_rctool_kt12;
       JetToolRunner* smr_rctool_kt8;
       
       CHECK(GetJetToolRunner(smr_rctool_kt12,1.2,"GoodSMRJets"+std::to_string(i),"MySMRFatJetsKt12"+std::to_string(i)));
       CHECK(GetJetToolRunner(smr_rctool_kt8, 0.8,"GoodSMRJets"+std::to_string(i),"MySMRFatJetsKt8" +std::to_string(i)));
       
       m_jetRecTool_smrkt12[smr_name]=smr_rctool_kt12;
       m_jetRecTool_smrkt8[smr_name]=smr_rctool_kt8;


    }
     

   }

   // Track selection tool 
   
   m_trackSelTool = new InDet::InDetTrackSelectionTool("TrackSelTool");
   CHECK( m_trackSelTool->setProperty("CutLevel","LoosePrimary")); 
   CHECK( m_trackSelTool->setProperty("maxZ0SinTheta", 3)); 
   CHECK( m_trackSelTool->setProperty("OutputLevel", MSG::INFO)); 
   CHECK( m_trackSelTool->initialize());      

   // MC truth classifier!
   m_MCtruthClassifier = new MCTruthClassifier("MCTruthClassifier");
   CHECK( m_MCtruthClassifier->initialize());


   PDGCode::GetNeutrinoCodes(m_pdgCodes); /// function which gets the nu codes in PhysicsTools
   m_pdgCodes.push_back(PDGCode::mu_minus);
   m_pdgCodes.push_back(-PDGCode::mu_minus);
   //  m_pdgCodes.push_back(15); //tau jets are vetoed
   // m_pdgCodes.push_back(-15);
   m_pdgCodes.push_back(11);//electron 
   m_pdgCodes.push_back(-11);
   // m_pdgCodes.push_back(22);// photon

   if(m_doTruth ){
     CHECK(initializeTruthTools());
   }
   else{
     CHECK(initializeTools());
   }

   // This is the function where the functions to fill trees are prepared
   CHECK(prepareTrees());
   
   /*
   //Skim branches for all the trees                                                                                                                                                             
   for (unsigned int jj=0; jj<m_systNameList.size(); jj++) {
     for (int ii = 0; ii<m_totFills; ii++) {
       SkimTreeBranches(m_tree.at(jj),m_skimPolicy);
     }
   }
   */

   // initialize cut dumpers
   ///if (m_cutDumpLvl<1000.) 
   //   m_cutDumpStr.open("cutDump_"+std::to_string(m_cutDumpLvl)+".txt");

   // check for TruthParticles containers
   const xAOD::TruthParticleContainer* check_truthparticles(0);
   m_doTtbarHFClassification = true;
   if(!event->retrieve(check_truthparticles,"TruthElectrons").isSuccess()){
    ANA_MSG_WARNING("Cannot retrieve TruthParticles containers !" );
    m_doTtbarHFClassification = false;
   }

   return EL::StatusCode::SUCCESS; 

 }


EL::StatusCode MasterShef :: initializeTools () {
  
  std::vector<std::string> confFiles;
  std::vector<std::string> lcalcFiles;
  
  int periodNumber=eventInfo->runNumber();
  bool ismc16a = (periodNumber == 284500);
  bool ismc16d = (periodNumber == 300000); //same as mc16c samples
  bool ismc16e = (periodNumber == 310000); 
  ATH_MSG_INFO("PERIOD NUMBER = " <<  periodNumber << ", ismc16a = " << ismc16a << ", ismc16d = " << ismc16d << ", ismc16e = " << ismc16e);

   // LUMICALC
   // only for data 2015+16 should go here
  if( ismc16a ){
     lcalcFiles.push_back(PathResolverFindCalibFile("GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root"));//2015
     lcalcFiles.push_back(PathResolverFindCalibFile("GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root"));//2016
   }
   else if( ismc16d ){
     lcalcFiles.push_back(PathResolverFindCalibFile("GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root")); //2017
   }
   else if( ismc16e){
     lcalcFiles.push_back(PathResolverFindCalibFile("GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"));
   }


   // Initialise SUSYTools nominal version
   m_objTool = std::make_unique<ST::SUSYObjDef_xAOD>("SUSYObjDef_xAOD");
   Info(APP_NAME, " ABOUT TO INITIALIZE SUSYTOOLS "); // bool isData=false;
   // Properties coming from the python config

   std::vector<std::string> configFiles;
   configFiles.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/share/DSID410xxx/pileup_mc20e_dsid410470_FS.root");
   //configFiles.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/share/DSID437xxx/pileup_mc16a_dsid437213_AFII.root");
   //configFiles.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/share/DSID437xxx/pileup_mc16d_dsid437213_AFII.root");
   //configFiles.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/share/DSID437xxx/pileup_mc16e_dsid437213_AFII.root");
   //configFiles.push_back("/afs/cern.ch/work/a/alopezso/private/tcMET/MasterShef/run/400000.NTUP.500_325.pool.root");
   //configFiles.push_back("/afs/cern.ch/work/j/janders/public/For_Rosa/NTUP_PILEUP.999000.root");

   Info(APP_NAME, " Starting autoconfig with dataSource= %d", m_dataSource );
   CHECK(m_objTool->setProperty("DataSource",m_dataSource) ) ;
   CHECK(m_objTool->setProperty("ConfigFile",m_config) ) ;
   CHECK(m_objTool->setProperty("ShowerType",m_showerType));
   //CHECK(m_objTool->setProperty("JESNuisanceParameterSet",1) );
   CHECK(m_objTool->setProperty("AutoconfigurePRWTool",true) );
   CHECK(m_objTool->setProperty( "PRWLumiCalcFiles", lcalcFiles));
   //CHECK(m_objTool->setProperty( "mcChannel", eventInfo->mcChannelNumber()));

   //std::cout << "MC Channel number : " << eventInfo->mcChannelNumber() <<std::endl;

   // Turn this on for test on locally produced samples
   //CHECK(m_objTool->setProperty("AutoconfigurePRWTool",false) );
   //CHECK(m_objTool->setProperty( "PRWConfigFiles", configFiles));
   //CHECK(m_objTool->setProperty("mcCampaign","mc16e") ) ;


   if( m_objTool->initialize() != StatusCode::SUCCESS){
     Error( APP_NAME, "Cannot intialize SUSYObjDef_xAOD..." );
     Error( APP_NAME, "Exiting... " );
     exit(-1);
   }
   else{
     Info( APP_NAME, "SUSYObjDef_xAOD initialized... " );
   }
   //CHECK( m_metSignif.setProperty("ApplyBias",true));
   //------------------------------------------------------------
   //default metsignificance tool with our special configuration
   /*
   m_metSignif.setTypeAndName("met::METSignificance/metSignifDefault");
   CHECK( m_metSignif.setProperty("SoftTermParam",met::TSTParam));
   CHECK( m_metSignif.setProperty("TreatPUJets", true) );
   CHECK( m_metSignif.setProperty("DoPhiReso", true));
   CHECK( m_metSignif.setProperty("IsDataJet",true));
   CHECK( m_metSignif.setProperty("JetEtaThr",2.8));
   CHECK( m_metSignif.retrieve() );
   //CHECK(m_metSignif->initialize());


   //metsignificance tool + without isDataJet so that the max resolution of (Data,MC) is taken
   m_metSignif_maxRes.setTypeAndName("met::METSignificance/metSignifMaxRes");
   CHECK( m_metSignif_maxRes.setProperty("SoftTermParam",met::TSTParam));
   CHECK( m_metSignif_maxRes.setProperty("TreatPUJets", true) );
   CHECK( m_metSignif_maxRes.setProperty("DoPhiReso", true));
   CHECK( m_metSignif_maxRes.setProperty("IsDataJet",false));
   //CHECK( m_metSignif_maxRes.setProperty("JetEtaThr",2.8)); //add in an eta restriction temporarily 
   CHECK( m_metSignif_maxRes.retrieve() );
   //CHECK(m_metSignif_maxRes->initialize());


   //metsignificance tool + with bjet resolution inflateion
   m_metSignif_bjet.setTypeAndName("met::METSignificance/metSignifBjet");
   CHECK( m_metSignif_bjet.setProperty("SoftTermParam",met::TSTParam));
   CHECK( m_metSignif_bjet.setProperty("TreatPUJets", true) );
   CHECK( m_metSignif_bjet.setProperty("DoPhiReso", true));
   CHECK( m_metSignif_bjet.setProperty("IsDataJet",false));
   CHECK( m_metSignif_bjet.setProperty("JetResoAux","bjetInflate"));
   CHECK( m_metSignif_bjet.retrieve() );
   //CHECK( m_metSignif_bjet->initialize());


   //alternative metsignificance tool with run1 JER tool
   m_metSignif_run1JER.setTypeAndName("met::METSignificance/metSignifRun1JER");
   CHECK( m_metSignif_run1JER.setProperty("SoftTermParam",met::TSTParam));
   CHECK( m_metSignif_run1JER.setProperty("TreatPUJets", true) );
   CHECK( m_metSignif_run1JER.setProperty("DoPhiReso", true));
   CHECK( m_metSignif_run1JER.setProperty("DoRun1JER",true));
   CHECK( m_metSignif_run1JER.setProperty("IsDataJet",true));
   CHECK( m_metSignif_run1JER.retrieve() );
   //CHECK( m_metSignif_run1JER->initialize());

   //alternative metsignificance tool using the jet resolution to leptons
   m_metsigJetResol.setTypeAndName("met::METSignificance/metsigJetResol");
   CHECK( m_metsigJetResol.setProperty("SoftTermParam",met::TSTParam));
   CHECK( m_metsigJetResol.setProperty("TreatPUJets", true) );
   CHECK( m_metsigJetResol.setProperty("DoPhiReso", false));
   CHECK( m_metsigJetResol.setProperty("JetCollection", "AntiKt4EMTopo"));
   CHECK( m_metsigJetResol.setProperty("DoJerForEMu", true ));
   CHECK( m_metsigJetResol.retrieve() );
   //CHECK( m_metsigJetResol->initialize());
      
   
   ATH_MSG_VERBOSE("done setting up metsig tools");
   */

   m_prescaleTool = std::make_unique<JetSmearing::PreScaleTool>("PreScaleTool");
   std::string nametemp = m_path+"/data/JetSmearing/PreScales/prescale_histos_combined_2015-2018_v102-pro22-04.root";
   CHECK(m_prescaleTool->setProperty("HistoPath",nametemp));
   CHECK(m_prescaleTool->initialize());
   
   nsubjetinessTool = new NSubjettinessTool("SubjetinessTool");
   CHECK(nsubjetinessTool->initialize());
   nsubjetinessRatiosTool = new NSubjettinessRatiosTool("SubjetinessRatiosTool");
   CHECK(nsubjetinessRatiosTool->initialize());
   dipTool = new DipolarityTool("DipolarityTool");
   CHECK(dipTool->initialize());
   splitTool = new KTSplittingScaleTool("KTSplittingScaleTool");
   CHECK(splitTool->initialize());
   qwTool = new QwTool("QwTool");
   CHECK(qwTool->initialize());
   jchargeTool = new JetChargeTool("jetChargeTool");
   CHECK(jchargeTool->initialize());
   
   // m_smoothedWTagger50 = nullptr;
   // m_smoothedWTagger50 = std::unique_ptr<SmoothedWZTagger>( new SmoothedWZTagger( "SmoothWTagger50" ) );
   // CHECK(m_smoothedWTagger50->setProperty( "ConfigFile",   "SmoothedWZTaggers/SmoothedContainedWTagger_AntiKt10TrackCaloClusterTrimmed_MaxSignificance_3Var_MC16d_20190410.dat"));
   // CHECK(m_smoothedWTagger50->initialize());
     
   // m_smoothedWTagger80 = nullptr;
   // m_smoothedWTagger80 = std::unique_ptr<SmoothedWZTagger>( new SmoothedWZTagger( "SmoothWTagger80" ) );
   // CHECK(m_smoothedWTagger80->setProperty( "ConfigFile",   "SmoothedWZTaggers/SmoothedContainedWTagger_AntiKt10TrackCaloClusterTrimmed_MaxSignificance_3Var_MC16d_20190410.dat"));
   // CHECK(m_smoothedWTagger80->initialize());
     
   // m_smoothedZTagger50 = nullptr;
   // m_smoothedZTagger50 = std::unique_ptr<SmoothedWZTagger>( new SmoothedWZTagger( "SmoothZTagger50" ) );
   // CHECK(m_smoothedZTagger80->setProperty( "ConfigFile",   "SmoothedWZTaggers/SmoothedContainedZTagger_AntiKt10TrackCaloClusterTrimmed_MaxSignificance_3Var_MC16d_20190410.dat"));
   // CHECK(m_smoothedZTagger50->initialize());
     
   // m_smoothedZTagger80 = nullptr;
   // m_smoothedZTagger80 = std::unique_ptr<SmoothedWZTagger>( new SmoothedWZTagger( "SmoothZTagger80" ) );
   // CHECK(m_smoothedZTagger80->setProperty( "ConfigFile",   "SmoothedWZTaggers/SmoothedContainedZTagger_AntiKt10TrackCaloClusterTrimmed_MaxSignificance_3Var_MC16d_20190410.dat"));
   // CHECK(m_smoothedZTagger80->initialize());
     
   // m_smoothedTopTagger50 = nullptr;
   // m_smoothedTopTagger50 = std::unique_ptr<SmoothedTopTagger>( new SmoothedTopTagger( "SmoothTopTagger50" ) );
   // CHECK(m_smoothedTopTagger50->setProperty( "ConfigFile",   "SmoothedTopTaggers/SmoothedTopTagger_AntiKt10LCTopoTrimmed_MassTau32FixedSignalEfficiency90_MC15c_20170519.dat"));
   // CHECK(m_smoothedTopTagger50->initialize());

     
   // m_smoothedTopTagger80 = nullptr;
   // m_smoothedTopTagger80 = std::unique_ptr<SmoothedTopTagger>( new SmoothedTopTagger( "SmoothTopTagger80" ) );
   // CHECK(m_smoothedTopTagger80->setProperty( "ConfigFile",   "SmoothedTopTaggers/SmoothedTopTagger_AntiKt10LCTopoTrimmed_Tau32Split23FixedSignalEfficiency80_MC15c_20161209.dat"));
   // CHECK(m_smoothedTopTagger80->initialize());

   // causing a crash, see https://gitlab.cern.ch/Shef-Analysis/MasterShef/issues/96
   m_DNNTopTagger80=std::unique_ptr<JSSWTopTaggerDNN>(new JSSWTopTaggerDNN("DNNTopTagger80"));
   CHECK(m_DNNTopTagger80->setProperty("ConfigFile", "JSSDNNTagger_AntiKt10LCTopoTrimmed_TopQuarkInclusive_MC16_20201216_80Eff.dat"));
   CHECK(m_DNNTopTagger80->setProperty("CalibArea", "JSSWTopTaggerDNN/Rel21/"));
   CHECK(m_DNNTopTagger80->setProperty("IsMC", m_isMC ? true:false));
   CHECK(m_DNNTopTagger80->setProperty("TruthBosonContainerName","TruthBoson"));
   CHECK(m_DNNTopTagger80->setProperty("TruthTopQuarkContainerName","TruthTop"));
   CHECK(m_DNNTopTagger80->setProperty("OutputLevel", MSG::ERROR));
   CHECK(m_DNNTopTagger80->initialize());
   
   m_DNNTopTagger50=std::unique_ptr<JSSWTopTaggerDNN>(new JSSWTopTaggerDNN("DNNTopTagger50"));
   CHECK(m_DNNTopTagger50->setProperty("ConfigFile", "JSSDNNTagger_AntiKt10LCTopoTrimmed_TopQuarkInclusive_MC16_20201216_50Eff.dat"));
   CHECK(m_DNNTopTagger50->setProperty("CalibArea", "JSSWTopTaggerDNN/Rel21/"));
   CHECK(m_DNNTopTagger50->setProperty("IsMC", m_isMC ? true:false));
   CHECK(m_DNNTopTagger50->setProperty("TruthBosonContainerName","TruthBoson"));
   CHECK(m_DNNTopTagger50->setProperty("TruthTopQuarkContainerName","TruthTop"));
   CHECK(m_DNNTopTagger50->setProperty("OutputLevel", MSG::ERROR));
   CHECK(m_DNNTopTagger50->initialize());


   	 
   //grl tool
   m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
  
   std::vector<std::string> vecStringGRL;


   //2015 GRL
   vecStringGRL.push_back(PathResolverFindCalibFile("GoodRunsLists/data15_13TeV/20170619/physics_25ns_21.0.19.xml"));
   //2016 GRL
   vecStringGRL.push_back(PathResolverFindCalibFile("GoodRunsLists/data16_13TeV/20180129/physics_25ns_21.0.19.xml"));
   //2017 GRL
   vecStringGRL.push_back(PathResolverFindCalibFile("GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.xml"));
   //current 2018 GRL
   vecStringGRL.push_back(PathResolverFindCalibFile("GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.xml"));
     
   CHECK( m_grl->setProperty("GoodRunsListVec", vecStringGRL) );
   CHECK( m_grl->setProperty("PassThrough", false) ); // if true (default) will ignore result of GRL and will just pass all events
   CHECK( m_grl->initialize());

   // Initialize b-tagging selection tools and retrieve fix OP cut values.
   TEnv rEnv;
   std::string conf_name = m_path+"/data/"+m_config;
   int success = rEnv.ReadFile(gSystem->ExpandPathName(conf_name.c_str()), kEnvAll);
   if(success != 0){
     Error(APP_NAME,"Uh-oh!!!  CONFIG FILE FOR RETRIEVING VALUES FOR TRUTH NOT FOUND!  Please try again..");
     return EL::StatusCode::FAILURE;
   }
   //find the btagWP used, if not found - call it Unknown
   m_btagType = rEnv.GetValue("Btag.WP","Unknown");
   //find the btagWP used, if not found - use the default, thus SFEigen
   std::string b_systematicStrategy = rEnv.GetValue("Btag.SystStrategy","SFEigen");
   if(b_systematicStrategy == "Envelope")
     m_isEnvelope=true;

   double sumOfWeightsAlt=0;
   if(m_isMC)
     {
       CHECK(m_objTool->ApplyPRWTool());

       int channel = eventInfo->mcChannelNumber();
       sumOfWeightsAlt = m_objTool->GetSumOfWeights(channel);
     }
 
   

   for (unsigned int ii=0; ii<m_systNameList.size(); ii++) {
     m_cutFlowPUWgt.at(ii)->Fill(0.0,sumOfWeightsAlt);
   }


   /// PMGTool Weights
   if(m_isMC){
     int dsid= eventInfo->mcChannelNumber();

     m_saveShowerWeights= (dsid > 500300  && dsid < 500500 ) || // signals for the tcMET analysis
       dsid == 410470 || dsid == 410471 || // ttbar powheg pythia8
       dsid == 407345 || dsid == 407346 || dsid == 407347 || //ttbar powheg pythia8 met filtered samples
       dsid == 410480 || dsid == 410481 || //ttbar powheg pythia8 radiation samples
       dsid == 410156 || dsid == 410157 || dsid == 410218 || dsid == 410219 || dsid == 410220 || // ttZ samples
       ( dsid >= 364100 && dsid <=364200 ) || dsid == 364222 || dsid ==364223 || ( dsid >= 366001 && dsid <= 366008 ) || // V+jets samples
       ( dsid >= 411193 && dsid <= 411198) || (dsid >= 411181 && dsid <= 411186) || dsid == 410646 || dsid == 410647 || // Wt_DR single-top 
       dsid == 410644 || dsid == 410645 || dsid == 410658|| dsid == 410659; //single-top s-chan and t-chan

     m_saveShowerWeights = false;

     if(m_saveShowerWeights){
       m_weightTool.setTypeAndName("PMGTools::PMGTruthWeightTool/PMGTruthWeightTool");
       //CHECK(m_weightTool.setProperty("HFFilter", TString(rEnv.GetValue("PRW.autoconfigPRWHFFilter","")).ReplaceAll(" ","").Data() ));
       CHECK(m_weightTool.retrieve());
     }
   }

   // jet smearing!
   m_jetMCSmearing = std::make_unique<JetSmearing::JetMCSmearingTool>("JetMCSmearing");

   CHECK(m_jetMCSmearing->setProperty("NumberOfSmearedEvents", m_nsmears));
   // set properties here 
   if (m_TailWeightMaps || m_ExtraSysMaps ){ // if you are using tail weights set to true                                     
     CHECK(m_jetMCSmearing->setProperty("UseTailWeights",true));
   }
   else CHECK(m_jetMCSmearing->setProperty("UseTailWeights",false));
   CHECK(m_jetMCSmearing->setProperty("DoGaussianCoreSmearing",false));
   CHECK(m_jetMCSmearing->setProperty("GaussianCoreSmearingType","none"));
   CHECK(m_jetMCSmearing->setProperty("DoPhiSmearing", m_doJetSmearingPhiCorrections)); 
   CHECK(m_jetMCSmearing->initialize());
   

   bool is2017or2018 = !m_isMC && eventInfo->runNumber()>=311481;
   
   int  year = m_objTool->treatAsYear();

   if(m_doJetSmearingPhiCorrections){
     std::string m_corrFile_name=gSystem->ExpandPathName((m_path+"/data/JetSmearing/Moriond19/dijet_balance_Medium_metsig_M8_corrections.root").c_str());
     TFile* corrFile = new TFile(m_corrFile_name.c_str());
     Info("initialize()","Check the correction file for jetsmearing!");
     corrFile->ls();


     std::string corrname_data="dPhi/Data1516";
     std::string corrname_pseudo="dPhi/PseudoData1516";
     
     if(year==2017){
       corrname_data="dPhi/Data17";
       corrname_pseudo="dPhi/PseudoData17";
     }
     else if(year==2018){
       corrname_data="dPhi/Data18";
       corrname_pseudo="dPhi/PseudoData18";
     }
     
     Info("initialize()","Looking up corrections from %s and %s ",corrname_data.c_str(), corrname_pseudo.c_str());

     TF1* cdata=(TF1*)corrFile->Get(corrname_data.c_str());
     TF1* cpseudo=(TF1*)corrFile->Get(corrname_pseudo.c_str());
     m_jetMCSmearing->SetPhiCorrections(cdata,cpseudo);
   }


   //hard code these for now
   m_JS_map_name="responseEJES";
   m_JS_bvetomap_file="/data/JetSmearing/Moriond19/R_map2019_bveto_EJES_p3627_r9364.root";
   m_JS_bmap_file="/data/JetSmearing/Moriond19/R_map2019_btag_EJES_p3627_r9364.root";
   
  
   //switch to mc16d derived response maps for 2017/2018 data as their mu profiles are similar
   if(is2017or2018){
     m_JS_bvetomap_file="/data/JetSmearing/Moriond19/R_map2019_bveto_EJES_p3627_r10201.root";
     m_JS_bmap_file="/data/JetSmearing/Moriond19/R_map2019_btag_EJES_p3627_r10201.root";
   
   }
  
  

   std::string tempname = gSystem->ExpandPathName((m_path+m_JS_bvetomap_file).c_str());
   std::string tempnameB = gSystem->ExpandPathName((m_path+m_JS_bmap_file).c_str());


   TFile* LFile = new TFile(tempname.c_str());
   TFile* BFile = new TFile(tempnameB.c_str());
   Info("initialize()","Getting File (L-jets) = %s Getting File (b-jets) = %s",tempname.c_str(),tempnameB.c_str());
   

   TH2F* tempMap = (TH2F*)LFile->Get(m_JS_map_name.c_str());
   TH2F* myMap = (TH2F*)tempMap->Clone("map");
   TH2F* tempMapB = (TH2F*)BFile->Get(m_JS_map_name.c_str());
   TH2F* myMapB = (TH2F*)tempMapB->Clone("Bmap");
   Info("initialize()","Getting Response map (L-jets) = %s  Getting (b-jets) is = %s",tempname.c_str(),tempnameB.c_str());
   m_jetMCSmearing->SetResponseMaps(myMap,myMapB);


   // add jet smearing extra sys maps / tailweight maps in here!
   // copied over from QCDBkg - gone from histogram based stuff to ntuples now

   if(m_TailWeightMaps){ // This adds the systematic tail weighting response shapes you need extra histograms for this        

     // Only use this once you've got the "final, corrected" set of response maps with the high/low/optimal histograms in :)
     //////////////////////////Systematics code ////////////////                                                              
     std::string tempnameR = "responseEJES_p2666_doublegaus_high"; // should probably not hard code these                     
     TH2F* tempMapR = (TH2F*)LFile->Get(tempnameR.c_str());
     TH2F* myMapHigh = (TH2F*)tempMapR->Clone("Rmap_high");

     TH2F* tempMapRB = (TH2F*)BFile->Get(tempnameR.c_str());
     TH2F* myMapHighB = (TH2F*)tempMapRB->Clone("Rmap_high_Bjet");
     ATH_MSG_DEBUG("Adding the high tail Rmap");
     m_jetMCSmearing->AddSystematicResponseMaps(myMapHigh,myMapHighB);
     ATH_MSG_DEBUG("Finished high tail Rmap");
     //// low tail ////                                                                                                       
     tempnameR = "responseEJES_p2666_doublegaus_low";
     tempMapR = (TH2F*)LFile->Get(tempnameR.c_str());
     TH2F* myMapLow = (TH2F*)tempMapR->Clone("Rmap_low");

     tempMapRB = (TH2F*)BFile->Get(tempnameR.c_str());
     TH2F* myMapLowB = (TH2F*)tempMapRB->Clone("Rmap_low_Bjet");
     ATH_MSG_DEBUG("Adding the Low tail Rmap");
     m_jetMCSmearing->AddSystematicResponseMaps(myMapLow,myMapLowB);
     ATH_MSG_DEBUG("Finished low tail Rmap");
     ////////////////////////////////////////////////////////                                                                 
   }

   if ( m_ExtraSysMaps ){ /// Mercedes tailWeighting need M_hists before we can do this                                       
     //      TFile Input, InputB;                                                                                             
     //     TFile *ofile = wk()->getOutputFile ("ExtTailMaps"); // Needed so the clones are generated in the output file which does not disappear
     TFile *ofile = wk()->getOutputFile (m_outputName); // Needed so the clones are generated in the output file which does not disappear
     //      ANA_CHECK(m_event->writeTo(ofile));
     
     // If not the clones will crash the code when you try to access the response projections                                 
     for ( double s = 0.1; s < 1.81 ; s+=0.05 ) {
       std::stringstream st;
       ATH_MSG_DEBUG("S: "<<s);
       if (s < 1.0001 && s>0.9999) {
	 st<<"1.0";
       }
       else st << s;
       ATH_MSG_DEBUG("st: "<<st.str());
       // File Names                                                                                                          
       //// Put these Rmaps in $ROOTCORE DATA                                                                                 
       std::string TempF = "$ROOTCOREBIN/data/MasterShef/RMap_SysVars/R_map2016_bveto_OP77_EJES_p2666_doublegaus_"+ st.str() + "_Jan2017.root";
       std::string TempF_B ="$ROOTCOREBIN/data/MasterShef/RMap_SysVars/R_map2016_btag_OP77_EJES_p2666_doublegaus_"+ st.str() + "_Jan2017.root";
       ATH_MSG_DEBUG("File names = "<<TempF);
       std::string TempName = "responseEJES_p2666_doublegaus_"+ st.str();
       
       //Open files                                                                                                           
       TFile* Input = new TFile (TempF.c_str());
       TFile* InputB = new TFile (TempF_B.c_str());
       //GetHists                                                                                                             
       if (Input->IsZombie()) {
	 delete Input;
	 ATH_MSG_DEBUG("Error in file opening: "<<TempF);
	 continue;
   
       }
       if (InputB->IsZombie()){
	 delete InputB;
	 ATH_MSG_DEBUG("Error in file opening: "<<TempF_B);
	 continue;
       }
       ATH_MSG_DEBUG("Getting Hist: "<<TempName);
       Input->cd();
       TH2F* tempMap = (TH2F*) Input->Get(TempName.c_str());
       std::string RName = "Rmap_bveto_"+st.str();
       ATH_MSG_DEBUG("Got Rmap!");
       TH2F* myMapk = (TH2F*)tempMap->Clone(RName.c_str());
       //B-tag map                                                                                                            
       InputB->cd(); // myMap K clone gets removed when input file is closed                                                  
       TH2F* tempMap_B = (TH2F*)InputB->Get(TempName.c_str());
       std::string RName_B = "Rmap_btag_"+st.str();
       TH2F* myMapk_B = (TH2F*)tempMap_B->Clone(RName_B.c_str());
       
       // CD to output hist file so the clones made in the Jet Smearing tool don't lose their pointers after the input files  
       // closed and deleted.                                                                                                 
       ofile->cd();
       ATH_MSG_DEBUG("Adding Rmap: "<<st.str());
       m_jetMCSmearing->AddSystematicResponseMaps(myMapk,myMapk_B);
       ATH_MSG_DEBUG("Finished Rmap: "<<st.str());
       // Add value to vector // keeps the order                                                                              
       m_TailSysVals.push_back(s); // Push back the names of the histograms you have created so you can fill and create the hists
       st.str("");
       st.clear();
       Input->Close();
       InputB->Close();
       delete Input; // You don't want 10-20 input TFiles kicking about in the code when they are not needed                  
       delete InputB;
     }
   }
   



   // Get list of systematics
   //  if (!m_doTruth)
   // {
   Info( APP_NAME, "Possible list of systematics variation given below:" );
   const std::vector<ST::SystInfo>& infoList = m_objTool->getSystInfoList() ;
   for(const auto& sysInfo : infoList  ){
     const CP::SystematicSet& sysSet =sysInfo.systset ;
     Info( APP_NAME, "Found systematic %s, affects kinematics %i",sysSet.name().c_str(), sysInfo.affectsKinematics );
   }
    
   
   
   // Create the reduced vector of the syst are going to use
   ST::SystInfo nom_sysInfo;
   for (unsigned int iS=0; iS<m_systNameList.size(); iS++) {
     for(const auto& sysInfo : infoList ){
       const CP::SystematicSet& sysSet =sysInfo.systset ;
       if(m_systNameList.at(iS)==sysSet.name() ) {
	 m_systList.push_back(sysInfo);
	 if(m_systNameList.at(iS)=="")
	   nom_sysInfo = sysInfo;
	 break;
       }
       
     }
         
     if (m_systList.size()<=iS) {
       Error( APP_NAME, "Cannot initialize systematics %s ",m_systNameList.at(iS).c_str()  );
       Error( APP_NAME, "Exiting... " );
       exit(-1);
     }
     
   }

   
   if(m_systList.size()!=m_systNameList.size()){
     Error(APP_NAME,"Uh-oh!!! something went wrong because m_systList.size()!=m_systNameList.size(), check how you are adding systematics and alternative config files...");
     return EL::StatusCode::FAILURE;
   }
   
   
   
   Info( APP_NAME, "List of systematics variation currently in use:" );
   for(const auto& sysInfo : m_systList){
     Info( APP_NAME, "Found systematic %s",sysInfo.systset.name().c_str() );
   }

   Info(APP_NAME, "Finished with initialise" );


   return EL::StatusCode::SUCCESS; 
}


EL::StatusCode MasterShef :: initializeTruthTools () {

   //retrieve values in job option        
   TEnv rEnv;
   int success = -1;       
   std::string conf_name ="";
   //
   conf_name = m_path+"/data/"+m_config;;
   success = rEnv.ReadFile(gSystem->ExpandPathName(conf_name.c_str()), kEnvAll);
   if(success != 0){
     Error(APP_NAME,"Uh-oh!!!  CONFIG FILE FOR RETRIEVING VALUES FOR TRUTH NOT FOUND!  Please try again..");
     return EL::StatusCode::FAILURE;
   }

   m_ElePt   = rEnv.GetValue("Ele.Et", 20000.);
   m_EleEta  = rEnv.GetValue("Ele.Eta", 2.47);
   m_MuonPt   = rEnv.GetValue("Muon.Pt", 20000.);
   m_MuonEta  = rEnv.GetValue("Muon.Eta", 2.7);
   m_PhotonPt   = rEnv.GetValue("Photon.Pt", 25000.);
   m_PhotonEta  = rEnv.GetValue("Photon.Eta", 2.37);
   //For pflow R&D we want to lower the jet pt cut to 10 GeV
   if (!m_isPflowResearch) m_JetPt = rEnv.GetValue("Jet.Pt", 20000.);
   else  m_JetPt = rEnv.GetValue("Jet.Pt", 10000.);
   m_JetEta = rEnv.GetValue("Jet.Eta", 2.8);

   return EL::StatusCode::SUCCESS; 
}



// function to start to prepare the objects
bool MasterShef :: prepareObjects(bool init) {
 
  if (init) return true; // Nothing to do...

  //let's check the store and clear it as we're preparing new objects
  if(m_debug>0)store->print();
  store->clear();
  
  //get the systname variation of recording containers
  m_sysName=m_systNameList.at(m_systNum);
  
  //get the event so we can retrieve objects in this method
  //we can use this function to retrieve the truth particles 
  xAOD::TEvent* event = wk()->xaodEvent();
    
  //------------------------- Truth Paricles ----------------------------
  //retrieve all truth particles if we want to use these at some later point
  if(m_isMC){
    m_truthParticles = 0 ;
    m_truthParticlesAux = 0 ;
    const xAOD::TruthParticleContainer* truth(0);
    if(event->retrieve( truth, "TruthElectrons" ).isSuccess()){
      std::pair<xAOD::TruthParticleContainer*, xAOD::ShallowAuxContainer*> shallowcopy = xAOD::shallowCopyContainer(*truth);
      m_truthParticles = shallowcopy.first;
      m_truthParticlesAux = shallowcopy.second;
      bool setLinks = xAOD::setOriginalObjectLink(*truth, *m_truthParticles);
      if (!setLinks) {
	std::cout << "warning... faied to set links" << std::endl;
      }
      CHECK(store->record(m_truthParticles,"truthParticles"+m_sysName));
      CHECK(store->record(m_truthParticlesAux,"truthParticlesAux."+m_sysName));
    }

    //retrieve truth bhadrons if requested
    if (m_useHFTruthHadrons){
      this->getTruthHFHadrons();  
      
    }
  }
  //-----------------------------------------------------------------


  //
  // Primary vertex
  //
  //get the primary vertices
  m_primVertex = 0;
  EL_RETURN_CHECK("execute",event->retrieve( m_primVertex, "PrimaryVertices"));



  //------------------------- Baseline Objects ----------------------------
  // get objects before performing an overlap removal procedure
  //firstly retrieve all leptons, defined in Leptons.cxx
  CHECK(prepareBaselineLeptonsBeforeOR());
  //now retrieve all photons (if requested), defined in Photons.cxx
  CHECK(prepareBaselinePhotonsBeforeOR());
  //now retrieve all AntiKt4 jets, defined in Jets.cxx
  CHECK(prepareBaselineJetsBeforeOR());
  //also retrieve all AntiKt10 jets, defined in FatJets.cxx
  CHECK(prepareBaselineFatJetsBeforeOR());

  //if turned on, then retrieve the soft btagging vertex
  if(m_useSBtaggingVertexing){
    CHECK(prepareSBV());
  }

  //now retrieve met based on currently loaded objects (not fatjets) and all associated variables in MET.cxx 
 //met is calculated with all baseline objects
  CHECK(prepareMET());
  //-----------------------------------------------------------------

 

  //-------------------- Overlap Removal --------------------------
  // Do the overlap removal procedure!!
  xAOD::PhotonContainer* gammaTERM = NULL;
  xAOD::TauJetContainer* taujetTERM = NULL;
  
  //if we've used photons these should be included in the OR
  //but retrieve if we said photons should be used in the OR
  //it's possible to save photons but also not include them in the MET or the OLR
  const bool photonsUsedInOR = *(m_objTool->getProperty<bool>("DoPhotonOR"));
  if(m_usePhotons && photonsUsedInOR){
    gammaTERM=m_signalPhotonsBeforeOR;
     CHECK(store->record(gammaTERM,"ORPhotonForMET"+m_sysName));
  }
  
  //if we've used taujets these should also be included in the OR
  //same for using taus in the OR
   const bool tausUsedInOR = *(m_objTool->getProperty<bool>("DoTauOR"));
   if(m_useTaus && tausUsedInOR){
     taujetTERM=m_signalTausBeforeOR;
     CHECK(store->record(taujetTERM,"ORTauForMET"+m_sysName));
   }

  CHECK( m_objTool->OverlapRemoval(m_baselineElectronsBeforeOR, m_baselineMuonsBeforeOR, m_signalJetsBeforeOR, 
				   gammaTERM,
				   taujetTERM) );
  //-----------------------------------------------------------------
  
  
  //------------------------- Signal Objects ----------------------------


  //signal+baseline leptons defined in Leptons.cxx
  CHECK(prepareLeptons());
  //signal+baseline photons, defined in Photons.cxx
  CHECK(preparePhotons());
  //signal+baseline AntiKt4 jets, including b-jets, defined in Jets.cxx
  CHECK(prepareJets());
  // //prepare jet collection with leptons inside
  // if( m_doLepJet )
  //   CHECK(prepareLepJets());
  //prepare track jets 
  CHECK(prepareTrackJets());
  //prepare fat+rc jets
  //CHECK(prepareFatJets());
  CHECK(prepareRCJets());

  //calculate more complicated met variables such as metsignificance and invisible/fake met
  CHECK(prepareComplexMET());

  if(m_doLepJet)
    CHECK(prepareLepJetMET());
     
  for (unsigned int ii=0; ii<m_triggerList.size(); ii++) {
    bool matched = false;
    if(xStream=="DAOD_SUSY11" || xStream=="DAOD_EXOT14" ){
      // skip all trigger matching for SUSY11 & EXOT14 as missing relevent info
      matched=true;
      ATH_MSG_VERBOSE("skipping lepton trigger matching because we're all SUSY11 & SUSY14 aren't we");   
    }
    else if( m_triggerList.at(ii).find("HLT_e")!=std::string::npos) { 
      // Electron triggers. Recommendations in :
      // https://twiki.cern.ch/twiki/bin/view/Atlas/TrigEgammaRecommendations2015

      for (unsigned int j=0;j< m_signalElectrons->size();j++) {
        if (m_triggerList.at(ii).find("HLT_e26")!=std::string::npos) { 
          if (m_signalElectrons->at(j)->pt()/1000 < 26+1) continue; // Single Lep. Trig.: Offline electron pt = Online electron pt +1 GeV
        }
        if (m_triggerList.at(ii).find("HLT_e24")!=std::string::npos) { 
          if (m_signalElectrons->at(j)->pt()/1000 < 24+1) continue; // Single Lep. Trig.: Offline electron pt = Online electron pt +1 GeV
        }
        if (m_triggerList.at(ii).find("HLT_e60")!=std::string::npos) { 
          if (m_signalElectrons->at(j)->pt()/1000 < 60+1) continue; // Single Lep. Trig.: Offline electron pt = Online electron pt +1 GeV
        }
        if (m_triggerList.at(ii).find("HLT_e120")!=std::string::npos) { 
          if (m_signalElectrons->at(j)->pt()/1000 < 120+1) continue; // Single Lep. Trig.: Offline electron pt = Online electron pt +1 GeV
        }
        if (m_triggerList.at(ii).find("HLT_e140")!=std::string::npos) { 
          if (m_signalElectrons->at(j)->pt()/1000 < 140+1) continue;
        }

	// Lowest unprescaled single electron triggers 

	/*
        if (m_objTool->treatAsYear()==2015 && // 2015 conditions
	    ( m_triggerList.at(ii).find("HLT_e24_lhmedium_L1EM20VH")!=std::string::npos
	      ||  m_triggerList.at(ii).find("HLT_e60_lhmedium")!=std::string::npos
	      ||  m_triggerList.at(ii).find("HLT_e120_lhloose")!=std::string::npos)) 
	  { 
            matched = m_objTool->IsTrigMatched((*m_signalElectrons)[j],m_triggerList.at(ii));
            m_signalElectrons->at(j)->auxdata<bool>("trig_matched") = matched;
	  }
	else if ( m_objTool->treatAsYear()>= 2016 && m_objTool->treatAsYear()<= 2018 && // 2016 - 2018 conditions 
		  ( m_triggerList.at(ii).find("HLT_e26_lhtight_nod0_ivarloose") != std::string::npos
		    || m_triggerList.at(ii).find("HLT_e60_lhmedium_nod0") != std::string::npos
		    || m_triggerList.at(ii).find("HLT_e140_lhloose_nod0") != std::string::npos
		    )) 
	  { 
	    matched = m_objTool->IsTrigMatched((*m_signalElectrons)[j],m_triggerList.at(ii));
	    m_signalElectrons->at(j)->auxdata<bool>("trig_matched") = matched;
	  }
        if(matched)
	  break;
	*/
      }
    } else if( m_triggerList.at(ii).find("HLT_mu")!=std::string::npos) { 
      // Muon trigger: Recommendations in 
      // https://twiki.cern.ch/twiki/bin/view/Atlas/MuonTriggerPhysicsRecommendationsRel212017#Recommended_triggers

      for (unsigned int j=0;j< m_signalMuons->size();j++) {
        if (m_triggerList.at(ii).find("HLT_mu26")!=std::string::npos) { 
          if (m_signalMuons->at(j)->pt()/1000 < 26*1.05) continue; // 5% for the plateu
        }
        if (m_triggerList.at(ii).find("HLT_mu20")!=std::string::npos) { 
          if (m_signalMuons->at(j)->pt()/1000 < 20*1.05) continue;// 5% for the plateu
        }
        if (m_triggerList.at(ii).find("HLT_mu50")!=std::string::npos) { 
          if (m_signalMuons->at(j)->pt()/1000 < 50*1.05) continue; // 5% for the plateu
        }


	/*
	// Lowest unprescaled single muon triggers 
        if (m_objTool->treatAsYear()==2015 && m_triggerList.at(ii).find("HLT_mu20_iloose_L1MU15")!=std::string::npos) { // 2015 conditions 
	  matched = m_objTool->IsTrigMatched((*m_signalMuons)[j],m_triggerList.at(ii));
	  m_signalMuons->at(j)->auxdata<bool>("trig_matched") = matched;
        }
        else if (m_objTool->treatAsYear()>= 2016 && m_objTool->treatAsYear()<= 2018 && m_triggerList.at(ii).find("HLT_mu26_ivarmedium" ) !=std::string::npos) { // 2016 - 2018 conditions
            matched = m_objTool->IsTrigMatched((*m_signalMuons)[j],m_triggerList.at(ii));
            m_signalMuons->at(j)->auxdata<bool>("trig_matched") = matched;
        }
	else if ( m_triggerList.at(ii).find("HLT_mu50")!=std::string::npos) { // available in all years 
	  matched = m_objTool->IsTrigMatched((*m_signalMuons)[j],m_triggerList.at(ii));
          m_signalMuons->at(j)->auxdata<bool>("trig_matched") = matched;
	}
        
        if(matched) break;
	*/
      }
    } else if(m_triggerList.at(ii).find("HLT_g")!=std::string::npos) {
      if(m_usePhotons) {
        for (unsigned int j=0;j< m_signalPhotons->size();j++) {
          if(m_signalPhotons->at(j)->pt()/1000 > 26) {
            matched = m_objTool->IsTrigMatched((*m_signalPhotons)[j],m_triggerList.at(ii));
          }
          if(matched)
	    break;
        }
      }
      else matched=true;
    }
    else matched=true;

    m_trigMatch[m_triggerList.at(ii)]=matched;
  }

  return true;
}







EL::StatusCode MasterShef :: execute () {
   // Here you do everything that needs to be done on every single
   // events, e.g. read input variables, apply cuts, and fill
   // histograms and trees.  This is where most of your actual analysis
   // code will go.
   // Get xAOD event
   event = wk()->xaodEvent();
   store = wk()->xaodStore();

   eventInfo = 0;
   EL_RETURN_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));
   
   clock->Start(true);


   //int ntotal = wk()->inputFileNumEntries();

   // print every 100 events, so we know where we are:
   if( (m_eventCounter % 1000) ==0 ) Info("execute()", " \033[34m %s \033[0m Event number = %i  ",
            m_outputName.c_str(), m_eventCounter  );
   m_eventCounter++;



   //----------------------------
   // Event information
   //---------------------------
   const xAOD::EventInfo* eventInfo = 0;
   EL_RETURN_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));
   ATH_MSG_VERBOSE("event number==" << eventInfo->eventNumber());


   // loop over systematics
   unsigned int sysList_size =  m_doTruth ? 1 : m_systList.size();
   
   ATH_MSG_VERBOSE("sys list size: "<<sysList_size);
   
   for (m_systNum=0; m_systNum<sysList_size; m_systNum++) {

     ATH_MSG_VERBOSE("inside sys list");
     
     //_______________________________________________
     //
     //switch between different susytools instances if custom alternative config files exist
     //________________________________________________
     if (!m_doTruth) {
       
       if(m_isMC)CHECK(m_objTool->ApplyPRWTool());
       ATH_MSG_VERBOSE("applied prw tool");       
       //_____________________________________________
       // this could be moved out this loop, but should be set for the nominalTool and the m_susyTools
       if(m_debug==1)
	 m_objTool->msg().setLevel(MSG::DEBUG);
       if(m_debug==2){
	 this->msg().setLevel(MSG::VERBOSE);
	 m_objTool->msg().setLevel(MSG::VERBOSE);
       }
       else
	 m_objTool->msg().setLevel(MSG::WARNING);
       
     }

     if(m_debug==2){
       this->msg().setLevel(MSG::VERBOSE);
     }
     
     ATH_MSG_VERBOSE("set susytools message level");       
     
     m_cutLevel=1.; // reserve zero for the metadata total

     // check if the event is data or MC
     // (many tools are applied either to data or MC)
     m_AnalysisWeight=1.0;
     m_pileupweight = 1.0;
     m_pileupweightUP = 1.0;
     m_pileupweightDOWN = 1.0;
     m_pileupweightHash = 1.0; 
     m_TriggerWeight = 1.0;
     m_TriggerSF = 1.0;

     // check if the event is MC

     if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
       //m_isMC = true; // can do something with this later
       ATH_MSG_VERBOSE("file is MC");       
       m_AnalysisWeight=eventInfo->mcEventWeight();
       if (!m_doTruth) {
	 m_pileupweight = m_objTool->GetPileupWeight();
	 m_pileupweightHash = m_objTool->GetPileupWeightHash();
	 ATH_MSG_VERBOSE("got PU weights");       
	 if (m_objTool->resetSystematics() != StatusCode::SUCCESS){
	   Error(APP_NAME, "Cannot reset SUSYTools systematics" );
	   exit(-2);
	 }
	 
	 if( m_objTool->applySystematicVariation( CP::SystematicSet("PRW_DATASF__1up") ) == StatusCode::SUCCESS){
	   m_pileupweightUP = m_objTool->GetPileupWeight();
	 }
	 
	 if (m_objTool->resetSystematics() != StatusCode::SUCCESS){
	   Error(APP_NAME, "Cannot reset SUSYTools systematics" );
	   exit(-2);
	 }
	 
	 if( m_objTool->applySystematicVariation( CP::SystematicSet("PRW_DATASF__1down") ) == StatusCode::SUCCESS){
	   m_pileupweightDOWN = m_objTool->GetPileupWeight();
	 }
	 if (m_objTool->resetSystematics() != StatusCode::SUCCESS){
	   Error(APP_NAME, "Cannot reset SUSYTools systematics" );
	   exit(-2);
	 }
       }
     }
     else if(m_useTriggerWeight){
       int runnumber=eventInfo->runNumber();
       nt_hlt_jet = 0.;
       std::string single_jet_triggers [16] = {"HLT_j400","HLT_j380","HLT_j360","HLT_j320","HLT_j300","HLT_j260","HLT_j200","HLT_j175",
					       "HLT_j150","HLT_j110","HLT_j100","HLT_j85","HLT_j60","HLT_j55","HLT_j25","HLT_j15"};
       //std::string single_jet_triggers [14] = {"HLT_j400","HLT_j380","HLT_j360","HLT_j320","HLT_j300","HLT_j260","HLT_j175",
       //						  "HLT_j150","HLT_j110","HLT_j85","HLT_j60","HLT_j55","HLT_j25","HLT_j15"};
      
       std::vector<std::pair<std::string,bool > > triggersPass;
       for(const std::string &sjt : single_jet_triggers)
	 {
	   // use susytools to test if each single jet trigger has fired
	   triggersPass.push_back( std::make_pair (sjt, m_objTool->IsTrigPassed(sjt.c_str())));
	 }
      
       // HLT Jet
       const xAOD::JetContainer * hlt_jet = 0;
       TString hlt_name="HLT_xAOD__JetContainer_a4tcemsubjesFS";
       if(m_objTool->treatAsYear() >= 2017) hlt_name="HLT_xAOD__JetContainer_a4tcemsubjesISFS";


       if( ! event->retrieve( hlt_jet, hlt_name.Data()).isSuccess() )
	 Error("execute()", Form("failed to retrieve %s", hlt_name.Data()));
      

       //double HLTjetPt=0;
       if(hlt_jet->size()>0){
	 m_TriggerWeight = m_prescaleTool->getTriggerPrescale(triggersPass,*(hlt_jet->at(0)),runnumber);	
	 //HLTjetPt=hlt_jet->at(0)->pt()/1000.;

       }
     
       /*
	 const Trig::ChainGroup* chainGroup = m_objTool->GetTrigChainGroup("HLT_j.*");
	 std::map<int, std::string> PresenTriggers;
	 bool anypass=false;
	 for (auto &trig : chainGroup->getListOfTriggers()) {
	 std::string thisTrig = trig;
	 if (thisTrig.length() < 9) {
	 //// Extract threshold
	 std::string str_thres = thisTrig.substr(5);
	 PresenTriggers[atoi(str_thres.c_str())] = thisTrig;
	 }
	 if(m_objTool->IsTrigPassed(trig.c_str())){
	 anypass=true;
	 std::cout << "==== " << trig << " PASSED!" << std::endl;
	 }
	 }
    
	 std::vector<std::string> single_jet_triggers2;
	 std::vector<double> single_jet_theshold;
	 for (std::map<int, std::string>::iterator it = PresenTriggers.begin(); it != PresenTriggers.end(); it++) {
	 single_jet_triggers2.push_back(it->second);
	 single_jet_theshold.push_back(int(it->first));
	 }
    
	 //// Determine highest threshold trigger, which could have fired
	 int i_highest_threshold = -1;
	 for (int i  = single_jet_theshold.size()-1; i >= 0; i--) {
	 if (float(single_jet_theshold.at(i)) <= HLTjetPt) {
	 i_highest_threshold = i;
	 break;
	 }
	 }


	 double combined_PS=1.;
	 for (int i = 0; i <= i_highest_threshold; i++) {
	 const Trig::ChainGroup* chainGroup = m_objTool->GetTrigChainGroup(single_jet_triggers2.at(i));
	 for (auto &trig : chainGroup->getListOfTriggers()) {
	 auto cg = m_objTool->GetTrigChainGroup(trig);
	 std::string thisTrig = trig;
	 double ps = cg->getPrescale();
	 if (ps > 0) {
	 combined_PS *= (1. - 1./ps);
	 }
	 }
	 }
	 if(i_highest_threshold>-1 && anypass){
	 m_TriggerWeight2= 1. / (1. - combined_PS);
           
	 std::cout << "=== end ==== " << m_TriggerWeight2 << " " << m_TriggerWeight  << "    " << single_jet_triggers2.at(i_highest_threshold) << " " << HLTjetPt  << std::endl;
	 }
       */

   
     }//end of if is use trigger weight


     
     ATH_MSG_VERBOSE("before getting correctedaverageinteractionspercrossing");       
     //if(!m_doTruth) m_averageIntPerXing = m_objTool->GetCorrectedAverageInteractionsPerCrossing();
     ATH_MSG_VERBOSE("got em :)");       
     
     if (!m_doTruth) {
       //Apply systematic variations:                                             
       if( m_objTool->applySystematicVariation(m_systList.at(m_systNum).systset) != StatusCode::SUCCESS) {
	 Error(APP_NAME,"CANNOT INITIALISE SYSTEMATIC");
       }
       ATH_MSG_VERBOSE("init systematics");       
     }
     
     // Enter a count per event
     ATH_MSG_VERBOSE("before printing total");
     passCut();
     std::string sysName="Nominal";
     if(!m_doTruth )
       m_systNameList.at(m_systNum);

     // Loop over cut functions
     for (int ii = 0; ii<m_totCuts; ii++) {
       bool success=(this->*m_funExec[ii])(false);
       //Info("loop()","cut = %d ; success=%i",ii,success);
       if ( !success) break;
     }
     
     // For nominal variation the copies are recorded in the store and deleted
     // For systematic variations, we do not record then as we can't change the name
     //     so we have to clean up
   
     if (!m_doTruth )
       {
         if (m_objTool->resetSystematics() != StatusCode::SUCCESS){ 
           Error(APP_NAME, "Cannot reset SUSYTools systematics" );
	   exit(-2);
         }
       } 
     
   
   }

   ATH_MSG_VERBOSE("end of execute() for event number==" << eventInfo->eventNumber());
   return EL::StatusCode::SUCCESS; }



 EL::StatusCode MasterShef :: postExecute () {
   // Here you do everything that needs to be done after the main event
   // processing.  This is typically very rare, particularly in user
   // code.  It is mainly used in implementing the NTupleSvc.

   return EL::StatusCode::SUCCESS; }



 EL::StatusCode MasterShef :: finalize () {
   // This method is the mirror image of initialize(), meaning it gets
   // called after the last event has been processed on the worker node
   // and allows you to finish up any objects you created in
   // initialize() before they are written to disk.  This is actually
   // fairly rare, since this happens separately for each worker node.
   // Most of the time you want to do your post-processing on the
   // submission node after all your histogram outputs have been
   // merged.  This is different from histFinalize() in that it only
   // gets called on worker nodes that processed input events.

   // Get xAOD event // xAOD::TEvent* event = wk()->xaodEvent();

   if(m_doxAOD){
     xAOD::TEvent* event = wk()->xaodEvent();
     TFile* xaod_file = wk()->getOutputFile(m_outputName);
     CHECK(event->finishWritingTo(xaod_file));
   }


 
   TFile *outputFile = wk()->getOutputFile(m_outputName);


   Info(APP_NAME,"\n \n \n =========== Finalising the output files ===========");
   outputFile->ls();

   return EL::StatusCode::SUCCESS; }



 EL::StatusCode MasterShef :: histFinalize () {
   // This method is the mirror image of histInitialize(), meaning it
   // gets called after the last event has been processed on the worker
   // node and allows you to finish up any objects you created in
   // histInitialize() before they are written to disk.  This is
   // actually fairly rare, since this happens separately for each
   // worker node.  Most of the time you want to do your
   // post-processing on the submission node after all your histogram
   // outputs have been merged.  This is different from finalize() in
   // that it gets called on all worker nodes regardless of whether
   // they processed input events.

   // Print CutFlow from the histogram:
   // m_cutFlow->Print();
   std::cout << "\n \n " << std::endl;
   Info(APP_NAME,("CutFlow for analysis "+m_outputName).c_str());
   int iterations=0;
   for (unsigned int iS=0; iS<m_systNameList.size(); iS++) {
     if ( (m_systNameList.at(iS) == "Nominal" || m_systNameList.at(iS) == "") && m_systBunch > 0 )
       continue;
       
     Info(APP_NAME,"CutFlow for Variation %s",m_systNameList.at(iS).c_str() );
     if(m_cutFlow.size()==0)
       break;

     if(!m_cutFlow.at(iS))
       continue;
      

     int nMax=m_cutFlow.at(iS)->GetNbinsX();
     //hack for truth sumOfWeights so the correct number is used in ExportTrees
     if((m_doTruth) && nMax>1){

       m_cutFlowWgt.at(iS)->SetBinContent(1,m_cutFlowWgt.at(iS)->GetBinContent(2));

     }

     for (int ii=0; ii<nMax; ii++){
       iterations++;
       std::string cutlevel="";
       std::string useColor="";
  
       if(m_cuts.size()==0)
	 break;
  
       if(ii==m_ntupLevel && iS==0){
	 useColor="\033[1;32m";
	 cutlevel="   <====== dumping ntuple "+m_outputName+" here \033[0m" ;
       }
       else if(ii==m_ntupLevel_sys && iS>0){
	 useColor="\033[1;33m";
	 cutlevel="   <====== dumping ntuple "+m_outputName+" here \033[0m" ;
       }
    

       if (m_cuts.at(ii) != "")
	 std::cout << useColor <<  "Cut " << ii << "  " << m_cuts.at(ii) << "  counts " << m_cutFlow.at(iS)->GetBinContent(ii+1) <<
	   " Weighted " << m_cutFlowWgt.at(iS)->GetBinContent(ii+1) <<
	   "   PU weighted " << m_cutFlowPUWgt.at(iS)->GetBinContent(ii+1) << cutlevel <<  std::endl;
     }
     
      
   }

   // Removing the histograms from memory
   //for( auto histo: m_cutFlowPUWgt ){
   //  delete histo;
   //}

   //if(iterations>0)
   return EL::StatusCode::SUCCESS; 
   //else
   //  return EL::StatusCode::FAILURE;
 }



// eventDump
// parameters:
// m_eventDump: The vector of events to dump
bool MasterShef :: eventDump(bool init)
{
   if (init) {
     Info("eventDump()","Initialising eventDump selector.... ");
     for (unsigned int ii=0; ii<m_eventDump.size() ;  ii++)
          Info("eventDump()","  Will dump event %i",m_eventDump.at(ii));
     return true;
   }
  
   xAOD::TEvent* event = wk()->xaodEvent();
   const xAOD::EventInfo* eventInfo = 0;
   CHECK(event->retrieve( eventInfo, "EventInfo") );
   int en = eventInfo->eventNumber();
   if (std::find(m_eventDump.begin(), m_eventDump.end(), en)!=m_eventDump.end() )   {
     Info("eventDump()"," ");
     Info("eventDump()","  Dumping event %i",en);
     Info("Dumping information for variation %s",m_systNameList.at(m_systNum).c_str() );
     
     
     const xAOD::MissingETContainer* met_Truth = 0;
     CHECK(event->retrieve(met_Truth,"MET_Truth"));
     double met_NonInt =(*met_Truth)["NonInt"]->met()/1000.;
     double met_Int =(*met_Truth)["Int"]->met()/1000.;

      Info("eventDump()","MET::  reco = %f ; Truth NonInt = %f ; Truth Int = %f",m_etmiss/1000.,met_NonInt,met_Int);

      Info("eventDump()"," ");
      unsigned int nj=m_signalJets->size();
      Info("eventDump()","  Number of jets %i (after OR)",nj); 
      for (unsigned int i=0;i<m_signalJets->size();i++) {
          Info("eventDump()","  eta_jet[J%i]=%f, pt_jet[J%i]=%f ",i,m_signalJets->at(i)->eta(),i, m_signalJets->at(i)->pt()/1000.);
          bool btagged=false;
          double weight_mv2c20(0.);
	  xAOD::BTaggingUtilities::getBTagging(*m_signalJets->at(i))->MVx_discriminant("MV2c20", weight_mv2c20);
          if ( m_objTool->IsBJet(*(m_signalJets->at(i)) ) && fabs( m_signalJets->at(i)->eta() ) <2.5 ) btagged=true;
          Info("eventDump()"," Jet J%i btagged=%i, weight = %f",i,btagged,weight_mv2c20);
      }
      unsigned int nmu = m_baselineMuons->size();
      Info("eventDump()","  Number of baseline muons (after OR) %i",nmu);
      for (unsigned int i=0;i<nmu;i++) {
          Info("eventDump()","  eta_mu[M%i]=%f, pt_mu[M%i]=%f ",i,m_baselineMuons->at(i)->eta(),i, m_baselineMuons->at(i)->pt()/1000.);
      }
      unsigned int nel = m_baselineElectrons->size();
      Info("eventDump()","  Number of baseline electrons (after OR) %i",nel);
      for (unsigned int i=0;i<nel;i++) {
          Info("eventDump()","  eta_el[M%i]=%f, pt_el[M%i]=%f ",i,m_baselineElectrons->at(i)->eta(),i, m_baselineElectrons->at(i)->pt()/1000.);
      }
      Info("eventDump()"," ");
      
    }

    
    
    return true;
}


void MasterShef::fillTree(int jj){
  
  for (int ii = 0; ii<m_totFills; ii++) {
    //fill the branches
    ATH_MSG_VERBOSE("filling tree " << jj << " for syst variation " << ii << "\t This is " << m_systNameList.at(jj) << " treename=" << m_tree.at(jj)->GetName() << " \t " << m_userTreeMods.at(ii));
    ATH_MSG_VERBOSE("Started filling variable branch = "<< ii << "  " << m_userTreeMods.at(ii));
    (this->*m_funFill[ii])(false,m_tree.at(jj));
    ATH_MSG_VERBOSE("Done filling variable branch = "<< ii);
  }

  TFile *outputFile = wk()->getOutputFile(m_outputName);
  outputFile->cd();
  
  ATH_MSG_VERBOSE("Fill tree " << jj << " name=" << m_tree.at(jj)->GetName() << " entries before fill =" << m_tree.at(jj)->GetEntries());
  m_tree.at(jj)->Fill();
  ATH_MSG_VERBOSE("tree now has entries = " << m_tree.at(jj)->GetEntries() << " \n ");
  
}


