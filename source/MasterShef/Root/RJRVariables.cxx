#include "MasterShef/MasterShef.h"

#ifndef USE_RESTFRAMES
//do nothing
#else
#include "RestFrames/RestFrames.hh"

struct part_RJR{
  TLorentzVector fourmom;
  int isbjet;
};
bool ptsort_rjr(part_RJR part1,part_RJR part2){
  return part1.fourmom.Pt() > part2.fourmom.Pt();
}

using namespace std;
using namespace RestFrames;
void MasterShef::GetRJRVariables(){

  std::vector<part_RJR> jets;

 
  for(xAOD::Jet* jet : *m_signalJets){
   part_RJR jet_rjr;
    jet_rjr.fourmom.SetPtEtaPhiM(jet->pt()/1000,0.0,jet->phi(),jet->m()/1000);
    if( jet->auxdata< bool >("IsBJet")  && fabs(jet->eta()) < 2.5 )
      jet_rjr.isbjet=1;
    else
      jet_rjr.isbjet=0;
    jets.push_back(jet_rjr);
  }

 /// Add leptons
  for(xAOD::Electron* electron : *m_signalElectrons){
    part_RJR jet_rjr;
    jet_rjr.fourmom.SetPtEtaPhiM(electron->pt()/1000,0.0,electron->phi(),electron->m()/1000);
    jet_rjr.isbjet=0;
    jets.push_back(jet_rjr);
  }
  for(xAOD::Muon* muon : *m_signalMuons){
    part_RJR jet_rjr;
    jet_rjr.fourmom.SetPtEtaPhiM(muon->pt()/1000,0.0,muon->phi(),muon->m()/1000);
    jet_rjr.isbjet=0;
    jets.push_back(jet_rjr);
  }


 /// Sorting the jet (+lepton) collection
 std::sort(jets.begin(),jets.end(),ptsort_rjr);
 if(jets.size()==0)
   return;
 
  // RestFrames stuff                                                                                                                                                     

  ////////////// Tree set-up /////////////////                                                                                                                           
  LabRecoFrame*        LAB;
  DecayRecoFrame*      CM;
  DecayRecoFrame*      S;
  VisibleRecoFrame*    ISR;
  VisibleRecoFrame*    V;
  InvisibleRecoFrame*  I;
  InvisibleGroup*      INV;
  SetMassInvJigsaw*    InvMass;
  CombinatoricGroup*   VIS;
  MinMassesCombJigsaw* SplitVis;
  
  LAB = new LabRecoFrame("LAB", "lab");
  CM = new DecayRecoFrame("CM", "CM");
  S = new DecayRecoFrame("S", "S");
  ISR = new VisibleRecoFrame("ISR", "ISR");
  V = new VisibleRecoFrame("V", "Vis");
  I = new InvisibleRecoFrame("I", "Inv");
  
  LAB->SetChildFrame(*CM);
  CM->AddChildFrame(*ISR);
  CM->AddChildFrame(*S);
  S->AddChildFrame(*V);
  S->AddChildFrame(*I);
  
  LAB->InitializeTree();
  ////////////// Tree set-up /////////////////                                                                                     
  ////////////// Jigsaw rules set-up /////////////////                                                                                                                    
  INV = new InvisibleGroup("INV", "Invisible System");
  INV->AddFrame(*I);
  
  VIS = new CombinatoricGroup("VIS", "Visible Objects");
  VIS->AddFrame(*ISR);
  VIS->SetNElementsForFrame(*ISR, 1, false);
  VIS->AddFrame(*V);
  VIS->SetNElementsForFrame(*V, 0, false);
  
  // set the invisible system mass to zero                                                                                                                                
  InvMass = new SetMassInvJigsaw("InvMass", "Invisible system mass Jigsaw");
  INV->AddJigsaw(*InvMass);
  
  // define the rule for partitioning objects between "ISR" and "V"                                                                                                       
  SplitVis = new MinMassesCombJigsaw("SplitVis", "Minimize M_{ISR} and M_{S} Jigsaw");
  VIS->AddJigsaw(*SplitVis);
  // "0" group (ISR)                                                        
  
  SplitVis->AddFrame(*ISR, 0);
  // "1" group (V + I)                                                                                                                                                    
  SplitVis->AddFrame(*V, 1);
  SplitVis->AddFrame(*I, 1);
  
  LAB->InitializeAnalysis();
  
  

  m_PTISR = -999;
  m_RISR = -999;
  m_MS = -999;
  m_MV = -999;
  m_dphiISRI = -999;
  m_pTbV1 = -999;
  m_NbV = -999;
  m_NjV = -999;
  m_pTjV4 = -999;
  

  ////////////// Jigsaw rules set-up /////////////////
  
  // analyze event in RestFrames tree
  LAB->ClearEvent();
  vector<RFKey> jetID;
  TVector3 MET_vec(m_pxmiss/1000.,m_pymiss/1000.,0);

  for (part_RJR sigjet: jets){
    jetID.push_back(VIS->AddLabFrameFourVector(sigjet.fourmom));
  }

  INV->SetLabFrameThreeVector(MET_vec);
  if(!LAB->AnalyzeEvent()) ATH_MSG_WARNING("Something went very wrong here");
    
  // Compressed variables from tree
  m_NjV = 0;
  m_NbV = 0;
  m_NjISR = 0;
  m_NbISR = 0;
  //    m_pTjV1 = 0.;
  //    m_pTjV2 = 0.;
  //    m_pTjV3 = 0.;
  m_pTjV4 = 0.;
  //    m_pTjV5 = 0.;
  //    m_pTjV6 = 0.;
  m_pTbV1 = 0.;
  //    m_pTbV2 = 0.;
  //for (int i = 0; i < int(inputJets.GetEntries()); i++) {
  //auto jet = static_cast<Stop0LUtils::Jet*>(inputJets.At(i));
  
  int ijet=0;

  // Pay attention. Jets should then be added to jetID before the electrons or the muons.
  for( part_RJR Jet : jets ){  //jet loop   
    if (VIS->GetFrame(jetID[ijet]) == *V) { // sparticle group
      m_NjV++;
      if (m_NjV == 4)
	m_pTjV4 = Jet.fourmom.Pt();

      //      if (Jet->auxdata<char>("bjet")==true && fabs(Jet->eta())<2.5){ 	
      if (Jet.isbjet==1 ){ 	
	m_NbV++;
	if (m_NbV == 1)
	  m_pTbV1 = Jet.fourmom.Pt();
      }    
    } else {
      m_NjISR++;
      if (Jet.isbjet==1 )
	m_NbISR++;
      //      if (Jet->auxdata<char>("bjet")==true && fabs(Jet->eta())<2.5)
    }
    ijet++;
  }
  
  
    // need at least one jet associated with sparticle-side of event
  if (m_NjV < 1) {
    m_PTISR = 0.;
    m_RISR = 0.;
    //      m_cosS = 0.;
    m_MS = 0.;
    m_MV = 0.;
    //      m_MISR = 0.;
    //      m_dphiCMI = 0.;
    m_dphiISRI = 0.;
  } else {
    
    TVector3 vP_ISR = ISR->GetFourVector(*CM).Vect();
    TVector3 vP_I   = I->GetFourVector(*CM).Vect();
    
    m_PTISR = vP_ISR.Mag();
    m_RISR = fabs(vP_I.Dot(vP_ISR.Unit())) / m_PTISR;
    //      m_cosS = S->GetCosDecayAngle();
    m_MS = S->GetMass();
    m_MV = V->GetMass();
    //      m_MISR = ISR->GetMass();
    //      m_dphiCMI = acos(-1.)-fabs(CM->GetDeltaPhiBoostVisible());
    m_dphiISRI = fabs(vP_ISR.DeltaPhi(vP_I));
  }
  
  delete LAB;
  delete CM;
  delete S;
  delete ISR;
  delete V;
  delete I;
  delete INV;
  delete InvMass;
  delete VIS;
  delete SplitVis;
  


}
#endif
