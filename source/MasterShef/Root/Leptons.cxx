#include <MasterShef/MasterShef.h>
//#include "CPAnalysisExamples/errorcheck.h"

#include "TauAnalysisTools/TauSelectionTool.h"
#include "TauAnalysisTools/TauEfficiencyCorrectionsTool.h"
#include "TauAnalysisTools/TauSmearingTool.h"
#include "TauAnalysisTools/TauTruthMatchingTool.h" 
//#include "TauAnalysisTools/TauOverlappingElectronLLHDecorator.h"
#include "tauRecTools/TauWPDecorator.h"
//


static const char* APP_NAME = "MasterShef::Leptons";




// ---------------------------------------------------------------
// Function to prepare baseline leptons for analysis
//
// This is a defintions of all the containers that are loaded here....
// The definitions (pt,eta,id,iso) of these are set in the .conf files!
//
// m_electrons_copy(aux): all electrons retrieved via susytools + aux container 
// m_muons_copy(aux): all muons retrieved via susytools + aux container  
// m_taus_copy(aux): all hadronic taus retrieved via susytools + aux container
//
// m_baselineElectronsBeforeOR : all baseline electrons before any overlap removal procedure
// m_baselineMuonsBeforeOR : all baseline muons before any overlap removal procedure
// m_signalTausBeforeOR: all baseline(signal) taus before any overlap removal procedure
//                       currently baseline and signal taus have the same definition 
//
// ---------------------------------------------------------------
bool MasterShef :: prepareBaselineLeptonsBeforeOR() {
  
 

  //set null pointers for the blank containers to be used to retrieve leptons
  m_electrons_copy=NULL;
  m_electrons_copyaux=NULL;
  
  m_muons_copy=NULL;
  m_muons_copyaux=NULL;

  m_taus_copy=NULL;
  m_taus_copy_aux=NULL;

  //maybe you might not want to retrieve electrons/muons at all (unlikely, but configurable)
  //retrieve electrons, by default this is true
  if(m_useElectrons){
    //setup new containers to track baseline leptons before OR
    m_baselineElectronsBeforeOR = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);
    //record these to the TStore
    CHECK(store->record(m_baselineElectronsBeforeOR,"BaselineElectronsBeforeOR"+m_sysName) );

    EL_RETURN_CHECK("GetElectrons()", m_objTool->GetElectrons(m_electrons_copy,m_electrons_copyaux));
    //make a new container for electrons retrieved that susytools marks as baseline! 
    //this makes things easier, i.e. to check the numbers retrieved.
    for(const auto& el : *m_electrons_copy) {
      if ( el->auxdata< char >("baseline") ){
	m_baselineElectronsBeforeOR->push_back (el);
      }
    }//ends loop over retrieved leptons
    //sort the containers by pT
    m_baselineElectronsBeforeOR->sort(ptsorter);

  }//ends if statement to retrieve electrons

  /*
  /// WE need this to make the code work in Rel 21.2.168. Remove when done
  xAOD::TauJetContainer* taus_nominal(0);
  xAOD::ShallowAuxContainer* taus_nominal_aux(0);                                                                                                                                            
  EL_RETURN_CHECK("GetTauJets()",m_objTool->GetTaus(taus_nominal,taus_nominal_aux));
  // record to manage memory                                                                                                                                                                    
  ANA_CHECK(store->record(taus_nominal, "DummyTaus"+m_sysName));

  */


  //retrieve muons, by default this is true
  if(m_useMuons){
    //setup new containers to track baseline leptons before OR
    m_baselineMuonsBeforeOR = new xAOD::MuonContainer(SG::VIEW_ELEMENTS);
    //record these to the TStore
    CHECK(store->record(m_baselineMuonsBeforeOR,"BaselineMuonsBeforeOR"+m_sysName) );
    
    EL_RETURN_CHECK("GetMuons()",m_objTool->GetMuons(m_muons_copy,m_muons_copyaux));
    for(const auto& mu : *m_muons_copy) {
      if ( mu->auxdata< char >("baseline") ){
	m_baselineMuonsBeforeOR->push_back (mu);
      }
    }//ends loop over all muons
    //sort the containers by pT
    m_baselineMuonsBeforeOR->sort(ptsorter);
  }//ends if statement to retrieve muons
  


  if(m_useTaus){
    //setup new containers to track baseline leptons before OR
    m_signalTausBeforeOR = new xAOD::TauJetContainer(SG::VIEW_ELEMENTS);
    //record these to the TStore
    CHECK(store->record(m_signalTausBeforeOR,"SignalTausBeforeOR"+m_sysName) );
    EL_RETURN_CHECK("GetTauJets()",m_objTool->GetTaus(m_taus_copy,m_taus_copy_aux));
    for(const auto& tau : *m_taus_copy) {
      if( tau->auxdata< char >("baseline") ) {
	m_signalTausBeforeOR->push_back(tau);
      }
    }//ends loop over tau jet containers    }
    //sort the containers by pT
    m_signalTausBeforeOR->sort(ptsorter);
  }//ends if use taus

  ATH_MSG_VERBOSE("Done in prepareBaselineLeptonsBeforeOR");
  ATH_MSG_VERBOSE("use Electrons?= "<< m_useElectrons);
  ATH_MSG_VERBOSE("use Muons?= "<< m_useMuons);
  ATH_MSG_VERBOSE("use Taus?= "<< m_useTaus);

  ATH_MSG_VERBOSE("nbaselineElectrons before OR= "<< m_baselineElectronsBeforeOR->size());
  ATH_MSG_VERBOSE("nbaselineMuons before OR= "<< m_baselineMuonsBeforeOR->size());
  ATH_MSG_VERBOSE("nsignalTaus before OR= "<< m_signalTausBeforeOR->size());


  return true;

}

//--------------------------------------------------
// Function that will prepare baseline and signal leptons
// this function is called after the overlap removal procedure
//
bool MasterShef :: prepareLeptons() {

  //-----------------------------------------------------------------
  // Store electrons
  if (m_useElectrons){
    m_baselineElectrons = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);
    m_baselineElectrons_lowPt = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);
    m_signalElectrons = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);
    CHECK( store->record(m_baselineElectrons,"BaselineElectrons"+m_sysName) );
    CHECK( store->record(m_baselineElectrons_lowPt,"BaselineElectrons_lowPt"+m_sysName) );
    CHECK( store->record(m_signalElectrons,"SignalElectrons"+m_sysName) );
    
    for(const auto& el : *m_baselineElectronsBeforeOR) {
      //if it passes OR then it is a "low pt" electron - this means the new low pt threshold is used
      if ( el->auxdata< char >("passOR") ==1){
	m_baselineElectrons_lowPt->push_back( el);
	//if it passes the OR, and has Pt > 7 GeV, then it is a baseline electron passing the OR
	if (el->pt() > 7000)
	  {
	    m_baselineElectrons->push_back( el);
	    //if it is also decorated as a signal electrons 
	    if ( el->auxdata< char >("signal") ==1 )
	      {
		m_signalElectrons->push_back( el);
	      }
	  }//pt threshold applied
      }//end check on if the electron passes the OR
    }//end loop over signal electrons before OR
    
    m_baselineElectrons_lowPt->sort(ptsorter);
    m_baselineElectrons->sort(ptsorter);
    m_signalElectrons->sort(ptsorter);
    
    ATH_MSG_VERBOSE("Stored baseline and signal Electrons after overlap removal");   
  }
  //-----------------------------------------------------------------


  //-----------------------------------------------------------------

  if (m_useMuons){
  // Store baseline muons, cosmic+bad muons and signal muons
  m_baselineMuons = new xAOD::MuonContainer(SG::VIEW_ELEMENTS);
  m_baselineMuons_lowPt = new xAOD::MuonContainer(SG::VIEW_ELEMENTS);
  m_cosmicMuons = new xAOD::MuonContainer(SG::VIEW_ELEMENTS);
  m_badMuons = new xAOD::MuonContainer(SG::VIEW_ELEMENTS);
  m_signalMuons = new xAOD::MuonContainer(SG::VIEW_ELEMENTS);
  CHECK( store->record(m_baselineMuons,"BaselineMuons"+m_sysName) );
  CHECK( store->record(m_baselineMuons_lowPt,"BaselineMuons_lowPt"+m_sysName) );
  CHECK( store->record(m_cosmicMuons,"CosmicMuons"+m_sysName) );
  CHECK( store->record(m_badMuons,"BadMuons"+m_sysName) );
  CHECK( store->record(m_signalMuons,"SignalMuons"+m_sysName) );
  
  //loop over all baseline muons before the overlap removal
  for(const auto& mu : *m_baselineMuonsBeforeOR) 
    {
      //we count how many muons are bad
      //these are not skiped when defining baseline and signal muon
      //instead we tend to veto entirely on these events
  
      if ( mu->auxdata< char >("bad") ==1 && mu->pt() > 6000)
	{
	  m_badMuons->push_back( mu);
	}
      
      //if the muon passes the OR then it is a "low pt" muon - this means the new low pt threshold is used
      if ( mu->auxdata< char >("passOR") ==1 ){
	m_baselineMuons_lowPt->push_back( mu);
	//and is not a cosmic muon then it's a baseline muon
	//in either case it must have pt > 6 GeV
	if (mu->pt() > 6000){
	  if ((mu->auxdata< char >("cosmic") ==1) )
	    {
	      m_cosmicMuons->push_back( mu);
	    }
	  else //it's a baseline muon
	    {
	      m_baselineMuons->push_back( mu);
	      //is it a signal muon?
	      if ( mu->auxdata< char >("signal") ==1 ){
		m_signalMuons->push_back( mu);
	      }
	  }//end of else it's a baseline muon
	}//require pt > 6 GeV
      }//end of if it passes the OR
    }//end of loop over all leptons

  //pT sort the important containers
  m_baselineMuons->sort(ptsorter);
  m_signalMuons->sort(ptsorter);
  }
  //-----------------------------------------------------------------



  //-----------------------------------------------------------------
  if(m_useTaus){

    const bool tausUsedInOR = *(m_objTool->getProperty<bool>("DoTauOR"));
    m_signalTaus = new xAOD::TauJetContainer(SG::VIEW_ELEMENTS);
    for(const auto& tau : *m_signalTausBeforeOR) {
      bool passOR=true;
      //check if we've used taus in the OR
      //if so, then these will be decorated as "passOR" or not
      if(tausUsedInOR){
	passOR=tau->auxdata< char >("passOR")==1;
	
	if( tau->auxdata< char >("baseline")==1 && passOR)
	  {
	    m_signalTaus->push_back(tau);
	  }
      }
    }//loop over taus before OR
    
    m_signalTaus->sort(ptsorter);
    CHECK( store->record(m_signalTaus,"SignalTaus"+m_sysName));
    //-----------------------------------------------------------------
  }
  

  
  return true;


}


