
#include <MasterShef/MasterShef.h> 
  
double MasterShef :: GetTriggerWeight(int runnumber)
{

 
  
  std::vector<std::string> listtrigs;
  
  
 
  listtrigs.push_back("HLT_j400");
  listtrigs.push_back("HLT_j360");
  listtrigs.push_back("HLT_j320");
  listtrigs.push_back("HLT_j260");
  listtrigs.push_back("HLT_j200");
  listtrigs.push_back("HLT_j175");
  listtrigs.push_back("HLT_j150");
  listtrigs.push_back("HLT_j110");
  listtrigs.push_back("HLT_j85");
  listtrigs.push_back("HLT_j60");
  listtrigs.push_back("HLT_j25");
  listtrigs.push_back("HLT_j15");

  double trigweight = 0;
  
  int njgood= m_signalJets->size();
    
    

  for( unsigned int jj=0 ; jj<listtrigs.size();jj++){

    std::string trigger = (std::string)listtrigs[jj];

    if( m_objTool->IsTrigPassed(trigger.c_str())){
	      

	double GeV=1000;
          
 
	if(njgood==0)
	  break;
	  
         		
	if (trigger=="HLT_j400" && (*m_signalJets)[0]->pt() > 430*GeV){
	  ////passtrigger = true;//passl1j12;
	  if (runnumber == 276262) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276329) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276336) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276416) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276511) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276689) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276778) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276790) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276952) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276954) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 278880) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 278912) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 278968) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 279169) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 279259) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 279279) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 279284) trigweight = 1.000 ; // for  
	  else if (runnumber == 279345) trigweight = 1.000 ; // for  
	  else if (runnumber == 279515) trigweight = 1.000 ; // for  
	  else if (runnumber == 279598) trigweight = 1.000 ; // for  
	  else if (runnumber == 279685) trigweight = 1.000 ; // for  
	  else if (runnumber == 279764) trigweight = 1.000 ; // for  
	  else if (runnumber == 279813) trigweight = 1.000 ; // for  
	  else if (runnumber == 279867) trigweight = 1.000 ; // for  
	  else if (runnumber == 279928) trigweight = 1.000 ; // for  
	  else if (runnumber == 279932) trigweight = 1.000 ; // for  
	  else if (runnumber == 279984) trigweight = 1.000 ; // for  
	  else if (runnumber == 280231) trigweight = 1.000 ; // for  
	  else if (runnumber == 280319) trigweight = 1.000 ; // for  
	  else if (runnumber == 280368) trigweight = 1.000 ; // for  
	  else if (runnumber == 280423) trigweight = 1.000 ; // for  
	  else if (runnumber == 280464) trigweight = 1.000 ; // for  
	  else if (runnumber == 280500) trigweight = 1.000 ; // for  
	  else if (runnumber == 280520) trigweight = 1.000 ; // for  
	  else if (runnumber == 280614) trigweight = 1.000 ; // for  
	  else if (runnumber == 280673) trigweight = 1.000 ; // for  
	  else if (runnumber == 280753) trigweight = 1.000 ; // for  
	  else if (runnumber == 280853) trigweight = 1.000 ; // for  
	  else if (runnumber == 280862) trigweight = 1.000 ; // for  
	  else if (runnumber == 280950) trigweight = 1.000 ; // for  
	  else if (runnumber == 280977) trigweight = 1.000 ; // for  
	  else if (runnumber == 281070) trigweight = 1.000 ; // for  
	  else if (runnumber == 281074) trigweight = 1.000 ; // for  
	  else if (runnumber == 281075) trigweight = 1.000 ; // for  
	  else if (runnumber == 281317) trigweight = 1.000 ; // for  
	  else if (runnumber == 281385) trigweight = 1.000 ; // for  
	  else if (runnumber == 281411) trigweight = 1.000 ; // for  
	  else if (runnumber == 282625) trigweight = 1.000 ; // for  
	  else if (runnumber == 282631) trigweight = 1.000 ; // for  
	  else if (runnumber == 282712) trigweight = 1.000 ; // for  
	  else if (runnumber == 282992) trigweight = 1.000 ; // for  
	  else if (runnumber == 283074) trigweight = 1.000 ; // for  
	  else if (runnumber == 283155) trigweight = 1.000 ; // for  
	  else if (runnumber == 283270) trigweight = 1.000 ; // for  
	  else if (runnumber == 283429) trigweight = 1.000 ; // for  
	  else if (runnumber == 283608) trigweight = 1.000 ; // for  
	  else if (runnumber == 283780) trigweight = 1.000 ; // for  
	  else if (runnumber == 284006) trigweight = 1.000 ; // for  
	  else if (runnumber == 284154) trigweight = 1.000 ; // for  
	  else std::cout << "Warning No runnumber found for this trigger!: " << trigger << "  Prescale set to 1" << std::endl; 
	  break;
	}// J400
	
	else if (trigger=="HLT_j360" && (*m_signalJets)[0]->pt() > 390*GeV && (*m_signalJets)[0]->pt() <= 430*GeV){
	  //passtrigger = true;//passl1j12;
	  if (runnumber == 276262) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276329) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276336) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276416) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276511) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276689) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276778) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276790) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276952) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 276954) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 278880) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 278912) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 278968) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 279169) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 279259) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 279279) trigweight = 1.000 ; // for D1 
	  else if (runnumber == 279284) trigweight = 1.000 ; // for  
	  else if (runnumber == 279345) trigweight = 1.000 ; // for  
	  else if (runnumber == 279515) trigweight = 1.000 ; // for  
	  else if (runnumber == 279598) trigweight = 1.000 ; // for  
	  else if (runnumber == 279685) trigweight = 1.000 ; // for  
	  else if (runnumber == 279764) trigweight = 1.000 ; // for  
	  else if (runnumber == 279813) trigweight = 1.000 ; // for  
	  else if (runnumber == 279867) trigweight = 1.000 ; // for  
	  else if (runnumber == 279928) trigweight = 1.000 ; // for  
	  else if (runnumber == 279932) trigweight = 1.000 ; // for  
	  else if (runnumber == 279984) trigweight = 1.000 ; // for  
	  else if (runnumber == 280231) trigweight = 1.000 ; // for  
	  else if (runnumber == 280319) trigweight = 1.000 ; // for  
	  else if (runnumber == 280368) trigweight = 1.000 ; // for  
	  else if (runnumber == 280423) trigweight = 1.000 ; // for  
	  else if (runnumber == 280464) trigweight = 1.000 ; // for  
	  else if (runnumber == 280500) trigweight = 1.000 ; // for  
	  else if (runnumber == 280520) trigweight = 1.000 ; // for  
	  else if (runnumber == 280614) trigweight = 1.000 ; // for  
	  else if (runnumber == 280673) trigweight = 1.000 ; // for  
	  else if (runnumber == 280753) trigweight = 1.000 ; // for  
	  else if (runnumber == 280853) trigweight = 1.000 ; // for  
	  else if (runnumber == 280862) trigweight = 1.000 ; // for  
	  else if (runnumber == 280950) trigweight = 1.000 ; // for  
	  else if (runnumber == 280977) trigweight = 1.000 ; // for  
	  else if (runnumber == 281070) trigweight = 1.000 ; // for  
	  else if (runnumber == 281074) trigweight = 1.000 ; // for  
	  else if (runnumber == 281075) trigweight = 1.000 ; // for  
	  else if (runnumber == 281317) trigweight = 1.000 ; // for  
	  else if (runnumber == 281385) trigweight = 1.000 ; // for  
	  else if (runnumber == 281411) trigweight = 1.000 ; // for  
	  else if (runnumber == 282625) trigweight = 1.000 ; // for  
	  else if (runnumber == 282631) trigweight = 1.000 ; // for  
	  else if (runnumber == 282712) trigweight = 1.000 ; // for  
	  else if (runnumber == 282992) trigweight = 1.000 ; // for  
	  else if (runnumber == 283074) trigweight = 1.000 ; // for  
	  else if (runnumber == 283155) trigweight = 1.000 ; // for  
	  else if (runnumber == 283270) trigweight = 1.000 ; // for  
	  else if (runnumber == 283429) trigweight = 1.000 ; // for  
	  else if (runnumber == 283608) trigweight = 1.000 ; // for  
	  else if (runnumber == 283780) trigweight = 1.000 ; // for  
	  else if (runnumber == 284006) trigweight = 1.000 ; // for  
	  else if (runnumber == 284154) trigweight = 1.000 ; // for  

	  else std::cout << "Warning No runnumber found for this trigger!: " << trigger << "  Prescale set to 1" << std::endl; 
	  break;
	}//J360
	
	else if (trigger=="HLT_j320" && (*m_signalJets)[0]->pt() > 350*GeV && (*m_signalJets)[0]->pt() <= 390*GeV){
	  //passtrigger = true;//passl1j12;
	  if (runnumber == 276262) trigweight = 2.353 ; // for D1 
	  else if (runnumber == 276329) trigweight = 7.479 ; // for D1 
	  else if (runnumber == 276336) trigweight = 7.000 ; // for D1 
	  else if (runnumber == 276416) trigweight = 7.000 ; // for D1 
	  else if (runnumber == 276511) trigweight = 8.921 ; // for D1 
	  else if (runnumber == 276689) trigweight = 11.600 ; // for D1 
	  else if (runnumber == 276778) trigweight = 7.334 ; // for D1 
	  else if (runnumber == 276790) trigweight = 5.485 ; // for D1 
	  else if (runnumber == 276952) trigweight = 6.963 ; // for D1 
	  else if (runnumber == 276954) trigweight = 5.737 ; // for D1 
	  else if (runnumber == 278880) trigweight = 5.362 ; // for D1 
	  else if (runnumber == 278912) trigweight = 6.170 ; // for D1 
	  else if (runnumber == 278968) trigweight = 9.260 ; // for D1 
	  else if (runnumber == 279169) trigweight = 6.383 ; // for D1 
	  else if (runnumber == 279259) trigweight = 9.874 ; // for D1 
	  else if (runnumber == 279279) trigweight = 9.994 ; // for D1 
	  else if (runnumber == 279284) trigweight = 8.061 ; // for  
	  else if (runnumber == 279345) trigweight = 10.126 ; // for  
	  else if (runnumber == 279515) trigweight = 27.000 ; // for  
	  else if (runnumber == 279598) trigweight = 12.448 ; // for  
	  else if (runnumber == 279685) trigweight = 11.392 ; // for  
	  else if (runnumber == 279764) trigweight = 20.233 ; // for  
	  else if (runnumber == 279813) trigweight = 10.413 ; // for  
	  else if (runnumber == 279867) trigweight = 15.253 ; // for  
	  else if (runnumber == 279928) trigweight = 27.000 ; // for  
	  else if (runnumber == 279932) trigweight = 13.125 ; // for  
	  else if (runnumber == 279984) trigweight = 15.348 ; // for  
	  else if (runnumber == 280231) trigweight = 14.825 ; // for  
	  else if (runnumber == 280319) trigweight = 17.331 ; // for  
	  else if (runnumber == 280368) trigweight = 22.956 ; // for  
	  else if (runnumber == 280423) trigweight = 18.788 ; // for  
	  else if (runnumber == 280464) trigweight = 20.532 ; // for  
	  else if (runnumber == 280500) trigweight = 15.000 ; // for  
	  else if (runnumber == 280520) trigweight = 14.922 ; // for  
	  else if (runnumber == 280614) trigweight = 19.469 ; // for  
	  else if (runnumber == 280673) trigweight = 17.686 ; // for  
	  else if (runnumber == 280753) trigweight = 21.797 ; // for  
	  else if (runnumber == 280853) trigweight = 18.688 ; // for  
	  else if (runnumber == 280862) trigweight = 17.535 ; // for  
	  else if (runnumber == 280950) trigweight = 16.903 ; // for  
	  else if (runnumber == 280977) trigweight = 21.562 ; // for  
	  else if (runnumber == 281070) trigweight = 25.270 ; // for  
	  else if (runnumber == 281074) trigweight = 21.482 ; // for  
	  else if (runnumber == 281075) trigweight = 30.100 ; // for  
	  else if (runnumber == 281317) trigweight = 27.156 ; // for  
	  else if (runnumber == 281385) trigweight = 20.542 ; // for  
	  else if (runnumber == 281411) trigweight = 27.685 ; // for  
	  else if (runnumber == 282625) trigweight = 9.605 ; // for  
	  else if (runnumber == 282631) trigweight = 20.811 ; // for  
	  else if (runnumber == 282712) trigweight = 24.480 ; // for  
	  else if (runnumber == 282992) trigweight = 22.237 ; // for  
	  else if (runnumber == 283074) trigweight = 24.695 ; // for  
	  else if (runnumber == 283155) trigweight = 25.335 ; // for  
	  else if (runnumber == 283270) trigweight = 27.797 ; // for  
	  else if (runnumber == 283429) trigweight = 22.483 ; // for  
	  else if (runnumber == 283608) trigweight = 18.000 ; // for  
	  else if (runnumber == 283780) trigweight = 22.638 ; // for  
	  else if (runnumber == 284006) trigweight = 29.798 ; // for  
	  else if (runnumber == 284154) trigweight = 21.316 ; // for  

	  else std::cout << "Warning No runnumber found for this trigger!: " << trigger << "  Prescale set to 1" << std::endl; 
	  break;
	} // J320
	
	else if (trigger=="HLT_j260" && (*m_signalJets)[0]->pt() > 280*GeV && (*m_signalJets)[0]->pt() <= 350*GeV){
	  //passtrigger = true;//passl1j12;
	  if (runnumber == 276262) trigweight = 7.070 ; // for D1 
	  else if (runnumber == 276329) trigweight = 22.248 ; // for D1 
	  else if (runnumber == 276336) trigweight = 20.800 ; // for D1 
	  else if (runnumber == 276416) trigweight = 20.200 ; // for D1 
	  else if (runnumber == 276511) trigweight = 25.765 ; // for D1 
	  else if (runnumber == 276689) trigweight = 33.600 ; // for D1 
	  else if (runnumber == 276778) trigweight = 21.123 ; // for D1 
	  else if (runnumber == 276790) trigweight = 15.679 ; // for D1 
	  else if (runnumber == 276952) trigweight = 19.897 ; // for D1 
	  else if (runnumber == 276954) trigweight = 16.406 ; // for D1 
	  else if (runnumber == 278880) trigweight = 15.019 ; // for D1 
	  else if (runnumber == 278912) trigweight = 17.236 ; // for D1 
	  else if (runnumber == 278968) trigweight = 26.030 ; // for D1 
	  else if (runnumber == 279169) trigweight = 17.789 ; // for D1 
	  else if (runnumber == 279259) trigweight = 27.555 ; // for D1 
	  else if (runnumber == 279279) trigweight = 27.867 ; // for D1 
	  else if (runnumber == 279284) trigweight = 22.469 ; // for  
	  else if (runnumber == 279345) trigweight = 28.251 ; // for  
	  else if (runnumber == 279515) trigweight = 75.300 ; // for  
	  else if (runnumber == 279598) trigweight = 34.744 ; // for  
	  else if (runnumber == 279685) trigweight = 31.786 ; // for  
	  else if (runnumber == 279764) trigweight = 56.433 ; // for  
	  else if (runnumber == 279813) trigweight = 29.046 ; // for  
	  else if (runnumber == 279867) trigweight = 42.505 ; // for  
	  else if (runnumber == 279928) trigweight = 75.300 ; // for  
	  else if (runnumber == 279932) trigweight = 36.599 ; // for  
	  else if (runnumber == 279984) trigweight = 42.772 ; // for  
	  else if (runnumber == 280231) trigweight = 41.334 ; // for  
	  else if (runnumber == 280319) trigweight = 48.321 ; // for  
	  else if (runnumber == 280368) trigweight = 64.055 ; // for  
	  else if (runnumber == 280423) trigweight = 52.396 ; // for  
	  else if (runnumber == 280464) trigweight = 57.270 ; // for  
	  else if (runnumber == 280500) trigweight = 41.800 ; // for  
	  else if (runnumber == 280520) trigweight = 41.584 ; // for  
	  else if (runnumber == 280614) trigweight = 54.309 ; // for  
	  else if (runnumber == 280673) trigweight = 49.316 ; // for  
	  else if (runnumber == 280753) trigweight = 60.811 ; // for  
	  else if (runnumber == 280853) trigweight = 52.123 ; // for  
	  else if (runnumber == 280862) trigweight = 48.893 ; // for  
	  else if (runnumber == 280950) trigweight = 47.125 ; // for  
	  else if (runnumber == 280977) trigweight = 60.171 ; // for  
	  else if (runnumber == 281070) trigweight = 70.489 ; // for  
	  else if (runnumber == 281074) trigweight = 59.944 ; // for  
	  else if (runnumber == 281075) trigweight = 83.700 ; // for  
	  else if (runnumber == 281317) trigweight = 75.724 ; // for  
	  else if (runnumber == 281385) trigweight = 57.317 ; // for  
	  else if (runnumber == 281411) trigweight = 77.292 ; // for  
	  else if (runnumber == 282625) trigweight = 26.814 ; // for  
	  else if (runnumber == 282631) trigweight = 58.929 ; // for  
	  else if (runnumber == 282712) trigweight = 68.768 ; // for  
	  else if (runnumber == 282992) trigweight = 62.686 ; // for  
	  else if (runnumber == 283074) trigweight = 69.267 ; // for  
	  else if (runnumber == 283155) trigweight = 71.077 ; // for  
	  else if (runnumber == 283270) trigweight = 43.457 ; // for  
	  else if (runnumber == 283429) trigweight = 63.268 ; // for  
	  else if (runnumber == 283608) trigweight = 50.500 ; // for  
	  else if (runnumber == 283780) trigweight = 63.700 ; // for  
	  else if (runnumber == 284006) trigweight = 83.638 ; // for  
	  else if (runnumber == 284154) trigweight = 60.254 ; // for  

	  else std::cout << "Warning No runnumber found for this trigger!: " << trigger << "  Prescale set to 1" << std::endl; 
	  break;
	} //J260
	
	else if (trigger=="HLT_j200" && (*m_signalJets)[0]->pt() > 220*GeV && (*m_signalJets)[0]->pt() <= 280*GeV){
	  //passtrigger = true;//passl1j12;
	  if (runnumber == 276262) trigweight = 26.391 ; // for D1 
	  else if (runnumber == 276329) trigweight = 82.790 ; // for D1 
	  else if (runnumber == 276336) trigweight = 77.400 ; // for D1 
	  else if (runnumber == 276416) trigweight = 77.400 ; // for D1 
	  else if (runnumber == 276511) trigweight = 98.754 ; // for D1 
	  else if (runnumber == 276689) trigweight = 129.000 ; // for D1 
	  else if (runnumber == 276778) trigweight = 80.924 ; // for D1 
	  else if (runnumber == 276790) trigweight = 60.311 ; // for D1 
	  else if (runnumber == 276952) trigweight = 76.471 ; // for D1 
	  else if (runnumber == 276954) trigweight = 63.090 ; // for D1 
	  else if (runnumber == 278880) trigweight = 52.241 ; // for D1 
	  else if (runnumber == 278912) trigweight = 60.046 ; // for D1 
	  else if (runnumber == 278968) trigweight = 90.311 ; // for D1 
	  else if (runnumber == 279169) trigweight = 62.107 ; // for D1 
	  else if (runnumber == 279259) trigweight = 96.212 ; // for D1 
	  else if (runnumber == 279279) trigweight = 97.133 ; // for D1 
	  else if (runnumber == 279284) trigweight = 78.373 ; // for  
	  else if (runnumber == 279345) trigweight = 98.564 ; // for  
	  else if (runnumber == 279515) trigweight = 262.800 ; // for  
	  else if (runnumber == 279598) trigweight = 121.158 ; // for  
	  else if (runnumber == 279685) trigweight = 110.903 ; // for  
	  else if (runnumber == 279764) trigweight = 196.929 ; // for  
	  else if (runnumber == 279813) trigweight = 101.345 ; // for  
	  else if (runnumber == 279867) trigweight = 148.459 ; // for  
	  else if (runnumber == 279928) trigweight = 262.799 ; // for  
	  else if (runnumber == 279932) trigweight = 127.714 ; // for  
	  else if (runnumber == 279984) trigweight = 149.386 ; // for  
	  else if (runnumber == 280231) trigweight = 144.296 ; // for  
	  else if (runnumber == 280319) trigweight = 168.684 ; // for  
	  else if (runnumber == 280368) trigweight = 223.441 ; // for  
	  else if (runnumber == 280423) trigweight = 182.866 ; // for  
	  else if (runnumber == 280464) trigweight = 199.843 ; // for  
	  else if (runnumber == 280500) trigweight = 146.000 ; // for  
	  else if (runnumber == 280520) trigweight = 145.239 ; // for  
	  else if (runnumber == 280614) trigweight = 189.500 ; // for  
	  else if (runnumber == 280673) trigweight = 172.140 ; // for  
	  else if (runnumber == 280753) trigweight = 212.134 ; // for  
	  else if (runnumber == 280853) trigweight = 181.894 ; // for  
	  else if (runnumber == 280862) trigweight = 170.671 ; // for  
	  else if (runnumber == 280950) trigweight = 164.523 ; // for  
	  else if (runnumber == 280977) trigweight = 209.868 ; // for  
	  else if (runnumber == 281070) trigweight = 245.798 ; // for  
	  else if (runnumber == 281074) trigweight = 209.080 ; // for  
	  else if (runnumber == 281075) trigweight = 292.001 ; // for  
	  else if (runnumber == 281317) trigweight = 264.275 ; // for  
	  else if (runnumber == 281385) trigweight = 199.947 ; // for  
	  else if (runnumber == 281411) trigweight = 269.622 ; // for  
	  else if (runnumber == 282625) trigweight = 93.650 ; // for  
	  else if (runnumber == 282631) trigweight = 198.898 ; // for  
	  else if (runnumber == 282712) trigweight = 232.067 ; // for  
	  else if (runnumber == 282992) trigweight = 211.596 ; // for  
	  else if (runnumber == 283074) trigweight = 233.779 ; // for  
	  else if (runnumber == 283155) trigweight = 239.835 ; // for  
	  else if (runnumber == 283270) trigweight = 278.064 ; // for  
	  else if (runnumber == 283429) trigweight = 213.504 ; // for  
	  else if (runnumber == 283608) trigweight = 170.400 ; // for  
	  else if (runnumber == 283780) trigweight = 214.995 ; // for  
	  else if (runnumber == 284006) trigweight = 282.100 ; // for  
	  else if (runnumber == 284154) trigweight = 204.152 ; // for  

	  else std::cout << "Warning No runnumber found for this trigger!: " << trigger << "  Prescale set to 1" << std::endl; 
	  break;
	}//J200

	else if (trigger=="HLT_j150" && (*m_signalJets)[0]->pt() > 170*GeV && (*m_signalJets)[0]->pt() <= 220*GeV){
	  //passtrigger = true;//passl1j12;
	  if (runnumber == 276262) trigweight = 96.965 ; // for D1 
	  else if (runnumber == 276329) trigweight = 305.867 ; // for D1 
	  else if (runnumber == 276336) trigweight = 286.200 ; // for D1 
	  else if (runnumber == 276416) trigweight = 286.200 ; // for D1 
	  else if (runnumber == 276511) trigweight = 365.290 ; // for D1 
	  else if (runnumber == 276689) trigweight = 476.999 ; // for D1 
	  else if (runnumber == 276778) trigweight = 299.510 ; // for D1 
	  else if (runnumber == 276790) trigweight = 223.015 ; // for D1 
	  else if (runnumber == 276952) trigweight = 282.777 ; // for D1 
	  else if (runnumber == 276954) trigweight = 233.339 ; // for D1 
	  else if (runnumber == 278880) trigweight = 193.668 ; // for D1 
	  else if (runnumber == 278912) trigweight = 222.113 ; // for D1 
	  else if (runnumber == 278968) trigweight = 333.996 ; // for D1 
	  else if (runnumber == 279169) trigweight = 229.690 ; // for D1 
	  else if (runnumber == 279259) trigweight = 355.055 ; // for D1 
	  else if (runnumber == 279279) trigweight = 360.100 ; // for D1 
	  else if (runnumber == 279284) trigweight = 290.344 ; // for  
	  else if (runnumber == 279345) trigweight = 364.493 ; // for  
	  else if (runnumber == 279515) trigweight = 972.003 ; // for  
	  else if (runnumber == 279598) trigweight = 448.235 ; // for  
	  else if (runnumber == 279685) trigweight = 409.890 ; // for  
	  else if (runnumber == 279764) trigweight = 728.372 ; // for  
	  else if (runnumber == 279813) trigweight = 374.733 ; // for  
	  else if (runnumber == 279867) trigweight = 549.094 ; // for  
	  else if (runnumber == 279928) trigweight = 972.001 ; // for  
	  else if (runnumber == 279932) trigweight = 472.446 ; // for  
	  else if (runnumber == 279984) trigweight = 552.528 ; // for  
	  else if (runnumber == 280231) trigweight = 533.695 ; // for  
	  else if (runnumber == 280319) trigweight = 623.899 ; // for  
	  else if (runnumber == 280368) trigweight = 826.422 ; // for  
	  else if (runnumber == 280423) trigweight = 676.354 ; // for  
	  else if (runnumber == 280464) trigweight = 739.146 ; // for  
	  else if (runnumber == 280500) trigweight = 540.001 ; // for  
	  else if (runnumber == 280520) trigweight = 537.187 ; // for  
	  else if (runnumber == 280614) trigweight = 700.891 ; // for  
	  else if (runnumber == 280673) trigweight = 636.680 ; // for  
	  else if (runnumber == 280753) trigweight = 784.607 ; // for  
	  else if (runnumber == 280853) trigweight = 672.762 ; // for  
	  else if (runnumber == 280862) trigweight = 631.248 ; // for  
	  else if (runnumber == 280950) trigweight = 608.509 ; // for  
	  else if (runnumber == 280977) trigweight = 776.225 ; // for  
	  else if (runnumber == 281070) trigweight = 909.118 ; // for  
	  else if (runnumber == 281074) trigweight = 773.308 ; // for  
	  else if (runnumber == 281075) trigweight = 1080.001 ; // for  
	  else if (runnumber == 281317) trigweight = 977.455 ; // for  
	  else if (runnumber == 281385) trigweight = 739.526 ; // for  
	  else if (runnumber == 281411) trigweight = 997.228 ; // for  
	  else if (runnumber == 282625) trigweight = 345.188 ; // for  
	  else if (runnumber == 282631) trigweight = 720.859 ; // for  
	  else if (runnumber == 282712) trigweight = 843.166 ; // for  
	  else if (runnumber == 282992) trigweight = 767.714 ; // for  
	  else if (runnumber == 283074) trigweight = 849.546 ; // for  
	  else if (runnumber == 283155) trigweight = 871.874 ; // for  
	  else if (runnumber == 283270) trigweight = 997.300 ; // for  
	  else if (runnumber == 283429) trigweight = 775.526 ; // for  
	  else if (runnumber == 283608) trigweight = 620.400 ; // for  
	  else if (runnumber == 283780) trigweight = 780.733 ; // for  
	  else if (runnumber == 284006) trigweight = 1027.714 ; // for  
	  else if (runnumber == 284154) trigweight = 739.762 ; // for  

	  else std::cout << "Warning No runnumber found for this trigger!: " << trigger << "  Prescale set to 1" << std::endl; 
	  break;
	}//J150

	else if (trigger=="HLT_j110" && (*m_signalJets)[0]->pt() > 130*GeV && (*m_signalJets)[0]->pt() <= 170*GeV){
	  //passtrigger = true;//passl1j12;
	  if (runnumber == 276262) trigweight = 325.124 ; // for D1 
	  else if (runnumber == 276329) trigweight = 1020.299 ; // for D1 
	  else if (runnumber == 276336) trigweight = 954.000 ; // for D1 
	  else if (runnumber == 276416) trigweight = 949.500 ; // for D1 
	  else if (runnumber == 276511) trigweight = 1212.327 ; // for D1 
	  else if (runnumber == 276689) trigweight = 1582.497 ; // for D1 
	  else if (runnumber == 276778) trigweight = 993.621 ; // for D1 
	  else if (runnumber == 276790) trigweight = 740.065 ; // for D1 
	  else if (runnumber == 276952) trigweight = 938.155 ; // for D1 
	  else if (runnumber == 276954) trigweight = 774.204 ; // for D1 
	  else if (runnumber == 278880) trigweight = 717.483 ; // for D1 
	  else if (runnumber == 278912) trigweight = 823.825 ; // for D1 
	  else if (runnumber == 278968) trigweight = 1238.435 ; // for D1 
	  else if (runnumber == 279169) trigweight = 853.559 ; // for D1 
	  else if (runnumber == 279259) trigweight = 1322.389 ; // for D1 
	  else if (runnumber == 279279) trigweight = 1337.314 ; // for D1 
	  else if (runnumber == 279284) trigweight = 1077.725 ; // for  
	  else if (runnumber == 279345) trigweight = 1355.751 ; // for  
	  else if (runnumber == 279515) trigweight = 3615.696 ; // for  
	  else if (runnumber == 279598) trigweight = 1666.034 ; // for  
	  else if (runnumber == 279685) trigweight = 1525.802 ; // for  
	  else if (runnumber == 279764) trigweight = 2709.524 ; // for  
	  else if (runnumber == 279813) trigweight = 1393.102 ; // for  
	  else if (runnumber == 279867) trigweight = 2040.239 ; // for  
	  else if (runnumber == 279928) trigweight = 3615.683 ; // for  
	  else if (runnumber == 279932) trigweight = 1756.306 ; // for  
	  else if (runnumber == 279984) trigweight = 2052.873 ; // for  
	  else if (runnumber == 280231) trigweight = 1983.756 ; // for  
	  else if (runnumber == 280319) trigweight = 2316.753 ; // for  
	  else if (runnumber == 280368) trigweight = 3072.370 ; // for  
	  else if (runnumber == 280423) trigweight = 2510.320 ; // for  
	  else if (runnumber == 280464) trigweight = 2744.201 ; // for  
	  else if (runnumber == 280500) trigweight = 2006.408 ; // for  
	  else if (runnumber == 280520) trigweight = 1996.085 ; // for  
	  else if (runnumber == 280614) trigweight = 2601.382 ; // for  
	  else if (runnumber == 280673) trigweight = 2363.767 ; // for  
	  else if (runnumber == 280753) trigweight = 2914.713 ; // for  
	  else if (runnumber == 280853) trigweight = 2496.229 ; // for  
	  else if (runnumber == 280862) trigweight = 2343.453 ; // for  
	  else if (runnumber == 280950) trigweight = 2259.403 ; // for  
	  else if (runnumber == 280977) trigweight = 2884.418 ; // for  
	  else if (runnumber == 281070) trigweight = 3385.074 ; // for  
	  else if (runnumber == 281074) trigweight = 2873.286 ; // for  
	  else if (runnumber == 281075) trigweight = 4014.373 ; // for  
	  else if (runnumber == 281317) trigweight = 3635.856 ; // for  
	  else if (runnumber == 281385) trigweight = 2746.704 ; // for  
	  else if (runnumber == 281411) trigweight = 3705.808 ; // for  
	  else if (runnumber == 282625) trigweight = 1287.774 ; // for  
	  else if (runnumber == 282631) trigweight = 2720.579 ; // for  
	  else if (runnumber == 282712) trigweight = 3181.844 ; // for  
	  else if (runnumber == 282992) trigweight = 2899.493 ; // for  
	  else if (runnumber == 283074) trigweight = 3207.896 ; // for  
	  else if (runnumber == 283155) trigweight = 3289.049 ; // for  
	  else if (runnumber == 283270) trigweight = 3737.180 ; // for  
	  else if (runnumber == 283429) trigweight = 2926.149 ; // for  
	  else if (runnumber == 283608) trigweight = 2339.104 ; // for  
	  else if (runnumber == 283780) trigweight = 2947.359 ; // for  
	  else if (runnumber == 284006) trigweight = 3865.194 ; // for  
	  else if (runnumber == 284154) trigweight = 2791.249 ; // for  

	  else std::cout << "Warning No runnumber found for this trigger!: " << trigger << "  Prescale set to 1" << std::endl; 
	  break;
	}//J110
	
	else if (trigger=="HLT_j60" && (*m_signalJets)[0]->pt() > 80*GeV && (*m_signalJets)[0]->pt() <= 130*GeV){
	  //passtrigger = true;//passl1j12;
	  if (runnumber == 276262) trigweight = 3793.540 ; // for D1 
	  else if (runnumber == 276329) trigweight = 11930.356 ; // for D1 
	  else if (runnumber == 276336) trigweight = 11159.151 ; // for D1 
	  else if (runnumber == 276416) trigweight = 11219.681 ; // for D1 
	  else if (runnumber == 276511) trigweight = 14332.858 ; // for D1 
	  else if (runnumber == 276689) trigweight = 18723.974 ; // for D1 
	  else if (runnumber == 276778) trigweight = 11743.171 ; // for D1 
	  else if (runnumber == 276790) trigweight = 8748.997 ; // for D1 
	  else if (runnumber == 276952) trigweight = 11092.119 ; // for D1 
	  else if (runnumber == 276954) trigweight = 9152.233 ; // for D1 
	  else if (runnumber == 278880) trigweight = 8347.290 ; // for D1 
	  else if (runnumber == 278912) trigweight = 9587.457 ; // for D1 
	  else if (runnumber == 278968) trigweight = 14414.527 ; // for D1 
	  else if (runnumber == 279169) trigweight = 9933.187 ; // for D1 
	  else if (runnumber == 279259) trigweight = 15366.956 ; // for D1 
	  else if (runnumber == 279279) trigweight = 15556.614 ; // for D1 
	  else if (runnumber == 279284) trigweight = 12544.360 ; // for  
	  else if (runnumber == 279345) trigweight = 15762.902 ; // for  
	  else if (runnumber == 279515) trigweight = 42032.051 ; // for  
	  else if (runnumber == 279598) trigweight = 19391.603 ; // for  
	  else if (runnumber == 279685) trigweight = 17736.189 ; // for  
	  else if (runnumber == 279764) trigweight = 31499.109 ; // for  
	  else if (runnumber == 279813) trigweight = 16212.950 ; // for  
	  else if (runnumber == 279867) trigweight = 23740.601 ; // for  
	  else if (runnumber == 279928) trigweight = 42031.925 ; // for  
	  else if (runnumber == 279932) trigweight = 20435.402 ; // for  
	  else if (runnumber == 279984) trigweight = 23889.260 ; // for  
	  else if (runnumber == 280231) trigweight = 23079.120 ; // for  
	  else if (runnumber == 280319) trigweight = 26977.959 ; // for  
	  else if (runnumber == 280368) trigweight = 35724.394 ; // for  
	  else if (runnumber == 280423) trigweight = 29252.752 ; // for  
	  else if (runnumber == 280464) trigweight = 31965.688 ; // for  
	  else if (runnumber == 280500) trigweight = 23347.271 ; // for  
	  else if (runnumber == 280520) trigweight = 23225.910 ; // for  
	  else if (runnumber == 280614) trigweight = 30310.211 ; // for  
	  else if (runnumber == 280673) trigweight = 27531.596 ; // for  
	  else if (runnumber == 280753) trigweight = 33915.408 ; // for  
	  else if (runnumber == 280853) trigweight = 29100.672 ; // for  
	  else if (runnumber == 280862) trigweight = 27297.955 ; // for  
	  else if (runnumber == 280950) trigweight = 26313.817 ; // for  
	  else if (runnumber == 280977) trigweight = 33550.337 ; // for  
	  else if (runnumber == 281070) trigweight = 39306.794 ; // for  
	  else if (runnumber == 281074) trigweight = 33423.746 ; // for  
	  else if (runnumber == 281075) trigweight = 46709.849 ; // for  
	  else if (runnumber == 281317) trigweight = 42268.366 ; // for  
	  else if (runnumber == 281385) trigweight = 31971.101 ; // for  
	  else if (runnumber == 281411) trigweight = 43120.927 ; // for  
	  else if (runnumber == 282625) trigweight = 14945.123 ; // for  
	  else if (runnumber == 282631) trigweight = 30979.070 ; // for  
	  else if (runnumber == 282712) trigweight = 36144.050 ; // for  
	  else if (runnumber == 282992) trigweight = 32953.840 ; // for  
	  else if (runnumber == 283074) trigweight = 36408.434 ; // for  
	  else if (runnumber == 283155) trigweight = 37354.606 ; // for  
	  else if (runnumber == 283270) trigweight = 42397.170 ; // for  
	  else if (runnumber == 283429) trigweight = 33252.427 ; // for  
	  else if (runnumber == 283608) trigweight = 26535.464 ; // for  
	  else if (runnumber == 283780) trigweight = 33483.276 ; // for  
	  else if (runnumber == 284006) trigweight = 43931.379 ; // for  
	  else if (runnumber == 284154) trigweight = 31779.595 ; // for  

	  else std::cout << "Warning No runnumber found for this trigger!: " << trigger << "  Prescale set to 1" << std::endl;
	  break;
	}//J60

	else if (trigger=="HLT_j25" && (*m_signalJets)[0]->pt() > 45*GeV && (*m_signalJets)[0]->pt() <= 80*GeV ) {
	  //passtrigger = true;//passl1j12;
	  if (runnumber == 276262) trigweight = 171911.959 ; // for D1 
	  else if (runnumber == 276329) trigweight = 373504.849 ; // for D1 
	  else if (runnumber == 276336) trigweight = 373505.529 ; // for D1 
	  else if (runnumber == 276416) trigweight = 375537.116 ; // for D1 
	  else if (runnumber == 276511) trigweight = 375539.230 ; // for D1 
	  else if (runnumber == 276689) trigweight = 375537.188 ; // for D1 
	  else if (runnumber == 276778) trigweight = 555894.688 ; // for D1 
	  else if (runnumber == 276790) trigweight = 555894.452 ; // for D1 
	  else if (runnumber == 276952) trigweight = 555209.289 ; // for D1 
	  else if (runnumber == 276954) trigweight = 555897.477 ; // for D1 
	  else if (runnumber == 278880) trigweight = 798726.433 ; // for D1 
	  else if (runnumber == 278912) trigweight = 798724.604 ; // for D1 
	  else if (runnumber == 278968) trigweight = 1295115.550 ; // for D1 
	  else if (runnumber == 279169) trigweight = 1306632.290 ; // for D1 
	  else if (runnumber == 279259) trigweight = 1306632.033 ; // for D1 
	  else if (runnumber == 279279) trigweight = 1306625.838 ; // for D1 
	  else if (runnumber == 279284) trigweight = 1306633.419 ; // for  
	  else if (runnumber == 279345) trigweight = 1563509.755 ; // for  
	  else if (runnumber == 279515) trigweight = 1818680.408 ; // for  
	  else if (runnumber == 279598) trigweight = 1818678.940 ; // for  
	  else if (runnumber == 279685) trigweight = 1818676.466 ; // for  
	  else if (runnumber == 279764) trigweight = 2094832.730 ; // for  
	  else if (runnumber == 279813) trigweight = 1818677.987 ; // for  
	  else if (runnumber == 279867) trigweight = 2094849.714 ; // for  
	  else if (runnumber == 279928) trigweight = 2094833.074 ; // for  
	  else if (runnumber == 279932) trigweight = 2094844.063 ; // for  
	  else if (runnumber == 279984) trigweight = 2094840.799 ; // for  
	  else if (runnumber == 280231) trigweight = 2094842.072 ; // for  
	  else if (runnumber == 280319) trigweight = 2331471.953 ; // for  
	  else if (runnumber == 280368) trigweight = 2331472.050 ; // for  
	  else if (runnumber == 280423) trigweight = 2331473.405 ; // for  
	  else if (runnumber == 280464) trigweight = 2331475.901 ; // for  
	  else if (runnumber == 280500) trigweight = 1569894.526 ; // for  
	  else if (runnumber == 280520) trigweight = 1569895.278 ; // for  
	  else if (runnumber == 280614) trigweight = 2331472.871 ; // for  
	  else if (runnumber == 280673) trigweight = 2590016.468 ; // for  
	  else if (runnumber == 280753) trigweight = 2590021.015 ; // for  
	  else if (runnumber == 280853) trigweight = 2590025.315 ; // for  
	  else if (runnumber == 280862) trigweight = 2590031.085 ; // for  
	  else if (runnumber == 280950) trigweight = 2590031.362 ; // for  
	  else if (runnumber == 280977) trigweight = 2590022.618 ; // for  
	  else if (runnumber == 281070) trigweight = 2844588.763 ; // for  
	  else if (runnumber == 281074) trigweight = 2844594.795 ; // for  
	  else if (runnumber == 281075) trigweight = 2844595.616 ; // for  
	  else if (runnumber == 281317) trigweight = 3227326.858 ; // for  
	  else if (runnumber == 281385) trigweight = 3227322.237 ; // for  
	  else if (runnumber == 281411) trigweight = 3227333.588 ; // for  
	  else if (runnumber == 282625) trigweight = 1049739.204 ; // for  
	  else if (runnumber == 282631) trigweight = 2760984.778 ; // for  
	  else if (runnumber == 282712) trigweight = 3135809.356 ; // for  
	  else if (runnumber == 282992) trigweight = 3135827.961 ; // for  
	  else if (runnumber == 283074) trigweight = 782937.537 ; // for  
	  else if (runnumber == 283155) trigweight = 782936.683 ; // for  
	  else if (runnumber == 283270) trigweight = 1050551.728 ; // for  
	  else if (runnumber == 283429) trigweight = 876424.430 ; // for  
	  else if (runnumber == 283608) trigweight = 876420.984 ; // for  
	  else if (runnumber == 283780) trigweight = 964062.968 ; // for  
	  else if (runnumber == 284006) trigweight = 964064.115 ; // for  
	  else if (runnumber == 284154) trigweight = 254500.105 ; // for  

	  else std::cout << "Warning No runnumber found for this trigger!: " << trigger << "  Prescale set to 1" << std::endl; 
	  break;
	}// J25
	
	else if (trigger=="HLT_j15" && (*m_signalJets)[0]->pt() <= 45*GeV ) {
	  //passtrigger = true;//passl1j12;
	  if (runnumber == 276262) trigweight = 740519.251 ; // for D1 
	  else if (runnumber == 276329) trigweight = 1609255.789 ; // for D1 
	  else if (runnumber == 276336) trigweight = 1609255.561 ; // for D1 
	  else if (runnumber == 276416) trigweight = 1611633.146 ; // for D1 
	  else if (runnumber == 276511) trigweight = 1611630.431 ; // for D1 
	  else if (runnumber == 276689) trigweight = 1611626.269 ; // for D1 
	  else if (runnumber == 276778) trigweight = 2385634.479 ; // for D1 
	  else if (runnumber == 276790) trigweight = 2385629.574 ; // for D1 
	  else if (runnumber == 276952) trigweight = 2382697.547 ; // for D1 
	  else if (runnumber == 276954) trigweight = 2385635.055 ; // for D1 
	  else if (runnumber == 278880) trigweight = 2919461.834 ; // for D1 
	  else if (runnumber == 278912) trigweight = 2919460.631 ; // for D1 
	  else if (runnumber == 278968) trigweight = 4733833.430 ; // for D1 
	  else if (runnumber == 279169) trigweight = 4785497.739 ; // for D1 
	  else if (runnumber == 279259) trigweight = 4785505.134 ; // for D1 
	  else if (runnumber == 279279) trigweight = 4785488.755 ; // for D1 
	  else if (runnumber == 279284) trigweight = 4785499.305 ; // for  
	  else if (runnumber == 279345) trigweight = 5726303.146 ; // for  
	  else if (runnumber == 279515) trigweight = 6663893.970 ; // for  
	  else if (runnumber == 279598) trigweight = 6663901.151 ; // for  
	  else if (runnumber == 279685) trigweight = 6663864.759 ; // for  
	  else if (runnumber == 279764) trigweight = 7677120.129 ; // for  
	  else if (runnumber == 279813) trigweight = 6663897.934 ; // for  
	  else if (runnumber == 279867) trigweight = 7677135.089 ; // for  
	  else if (runnumber == 279928) trigweight = 7677085.741 ; // for  
	  else if (runnumber == 279932) trigweight = 7677129.531 ; // for  
	  else if (runnumber == 279984) trigweight = 7677127.060 ; // for  
	  else if (runnumber == 280231) trigweight = 7677092.166 ; // for  
	  else if (runnumber == 280319) trigweight = 8544322.731 ; // for  
	  else if (runnumber == 280368) trigweight = 8544354.168 ; // for  
	  else if (runnumber == 280423) trigweight = 8544329.069 ; // for  
	  else if (runnumber == 280464) trigweight = 8544341.165 ; // for  
	  else if (runnumber == 280500) trigweight = 5753307.376 ; // for  
	  else if (runnumber == 280520) trigweight = 5753318.801 ; // for  
	  else if (runnumber == 280614) trigweight = 8544343.663 ; // for  
	  else if (runnumber == 280673) trigweight = 9491825.011 ; // for  
	  else if (runnumber == 280753) trigweight = 9491852.104 ; // for  
	  else if (runnumber == 280853) trigweight = 9491852.257 ; // for  
	  else if (runnumber == 280862) trigweight = 9491910.656 ; // for  
	  else if (runnumber == 280950) trigweight = 9491839.066 ; // for  
	  else if (runnumber == 280977) trigweight = 9491875.323 ; // for  
	  else if (runnumber == 281070) trigweight = 10424796.945 ; // for  
	  else if (runnumber == 281074) trigweight = 10424796.761 ; // for  
	  else if (runnumber == 281075) trigweight = 10424818.235 ; // for  
	  else if (runnumber == 281317) trigweight = 11827420.747 ; // for  
	  else if (runnumber == 281385) trigweight = 11827413.708 ; // for  
	  else if (runnumber == 281411) trigweight = 11827505.633 ; // for  
	  else if (runnumber == 282625) trigweight = 3844632.768 ; // for  
	  else if (runnumber == 282631) trigweight = 10402940.272 ; // for  
	  else if (runnumber == 282712) trigweight = 11817255.326 ; // for  
	  else if (runnumber == 282992) trigweight = 11817307.631 ; // for  
	  else if (runnumber == 283074) trigweight = 2954826.173 ; // for  
	  else if (runnumber == 283155) trigweight = 2954817.334 ; // for  
	  else if (runnumber == 283270) trigweight = 3964398.754 ; // for  
	  else if (runnumber == 283429) trigweight = 3307642.789 ; // for  
	  else if (runnumber == 283608) trigweight = 3307640.403 ; // for  
	  else if (runnumber == 283780) trigweight = 3638391.998 ; // for  
	  else if (runnumber == 284006) trigweight = 3638412.556 ; // for  
	  else if (runnumber == 284154) trigweight = 960489.787 ; // for  

	  else std::cout << "Warning No runnumber found for this trigger!: " << trigger << "  Prescale set to 1" << std::endl; 
	  break;
	}
    }
  }
    
  listtrigs.clear();
  listtrigs.shrink_to_fit();
  
  return trigweight;
}

    
   
      // 	  TString trigger = listtrigs[j];
      // 	  if(j==0)
      // 	    triggerOR+=trigger;
      // 	  else
      // 	    triggerOR+=" || " + trigger;
      // 	  TriggerWeights[trigger] = m_PRWtool->GetDataWeight(eventInfo->runNumber(),trigger);//,eventInfo->averageInteractionsPerCrossing()*1.09);
      // 	  //std::cout << trigger << " Trigger weight=" << TriggerWeights[trigger] << std::endl;
      // 	}
      // std::cout << triggerOR << std::endl;
      // std::cout << "OR weight = " << m_PRWtool->GetDataWeight(eventInfo->runNumber(),triggerOR,eventInfo->averageInteractionsPerCrossing()) << std::endl;
      
   
      // }
  
