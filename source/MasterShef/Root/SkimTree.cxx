#include "MasterShef/MasterShef.h"
//#include "CPAnalysisExamples/errorcheck.h"

static const char* APP_NAME = "MasterShef::Skim";


void MasterShef::SkimTreeBranches(TTree* tree,std::string skimPolicy){

  DefineSkimPolicies();
  if(skimPolicy == "" )
    return;
  if( m_treeSkim.find(skimPolicy) == m_treeSkim.end() ){
    Info("SkimTreeBranches()",Form("Cannot find skim strategy %s. The output tree won't be skimmed.",skimPolicy));
    return;
  }

  for( TString branch : m_treeSkim[skimPolicy] ) {

    TBranch* br = tree->GetBranch(branch);
    if( br != nullptr ){
      tree->SetBranchStatus(branch,0);
      tree->GetListOfBranches()->Remove(br);
    }
  }
  return;
}


void MasterShef::DefineSkimPolicies(){

  m_treeSkim["tcMET"]={ "metsig_default","metsig_binflate","metsig_run1JER","hasMEphoton","hasMEphoton80","hasMEphotonMCTC"};
}
