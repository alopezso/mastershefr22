#include <MasterShef/MasterShef.h>
//#include "CPAnalysisExamples/errorcheck.h"
#include "xAODTracking/TrackParticleContainer.h"



static const char* APP_NAME = "MasterShef::SBV";

// ---------------------------------------------------------------
// Function to prepare secondary vertices for soft b-tagging
//
// 
// ---------------------------------------------------------------
bool MasterShef :: prepareSBV() {

  ATH_MSG_DEBUG("inside SBV");

  //default is tight
  TString sbvname="SoftBVrtClusterTool_Loose_Vertices";

  //try and find the tight, if not present then drop to using the old name
  if(!event->contains<xAOD::VertexContainer>(sbvname.Data())){
    Error(APP_NAME,"Cannot find SoftBTagger_Vertices in the AOD!");
    return false;
  }
  
  //do the standard ones first
  m_softBVertices_Loose = 0;
  ATH_CHECK(evtStore()->retrieve(m_softBVertices_Loose,  sbvname.Data()));
   
  // Count the number of tracks and store the number for later processing
  const xAOD::TrackParticleContainer* tracks(nullptr);
   
   ATH_MSG_DEBUG( " retrieve TrackParticles " );
   ATH_CHECK( evtStore()->retrieve( tracks, "InDetTrackParticles") );
 
   unsigned int l_ntracks = 0;
 
   for (const xAOD::TrackParticle *track : *tracks)
     {            
       // recommended track quality cuts (configure to LoosePrimary in job options)
       if (!m_trackSelTool->accept( *track, m_primVertex->at(0)) ) continue;
       ++l_ntracks;
     }
 
   m_ntrk_global = l_ntracks;
   
   //record the good sbvs 
   m_goodSBV_Loose = new xAOD::VertexContainer();
   m_goodSBV_Looseaux = new xAOD::VertexAuxContainer();
   m_goodSBV_Loose->setStore( m_goodSBV_Looseaux);
   
   //loop over any vertices retrieved from the DxAOD
   for (auto v : *m_softBVertices_Loose) {
     xAOD::Vertex *temp = new xAOD::Vertex();
     *temp = *v;
     m_goodSBV_Loose->push_back(temp);
     //copy over the aux data n that
   }
   
   if(m_goodSBV_Loose->size()>0){
     ATH_MSG_VERBOSE("found at least one sbv, size=" << m_goodSBV_Loose->size());
     for (auto v : *m_goodSBV_Loose) { 
       ATH_MSG_VERBOSE("check x position at " << v->x());
     }
     
   }

   //record to the tstore
   CHECK(store->record(m_goodSBV_Loose,"GoodSoftBtaggedVerticesLoose"));
   CHECK(store->record(m_goodSBV_Looseaux,"GoodSoftBtaggedVerticesLooseAux"));
   
   
   if(m_saveAllSbWPs)
     {
       sbvname="SoftBVrtClusterTool_Medium_Vertices";
       m_softBVertices_Medium = 0;
       ATH_CHECK(evtStore()->retrieve(m_softBVertices_Medium,  sbvname.Data()));
       
       
       m_goodSBV_Medium = new xAOD::VertexContainer();
       m_goodSBV_Mediumaux = new xAOD::VertexAuxContainer();
       m_goodSBV_Medium->setStore( m_goodSBV_Mediumaux);
       
       for (auto v : *m_softBVertices_Medium) {
	 xAOD::Vertex *temp = new xAOD::Vertex();
	 *temp = *v;
	 m_goodSBV_Medium->push_back(temp);
       }
       //record to the tstore
       CHECK(store->record(m_goodSBV_Medium,"GoodSoftBtaggedVerticesMedium"));
       CHECK(store->record(m_goodSBV_Mediumaux,"GoodSoftBtaggedVerticesMediumAux"));
       
       //do loose 
       sbvname="SoftBVrtClusterTool_Tight_Vertices";
       m_softBVertices_Tight = 0;
       ATH_CHECK(evtStore()->retrieve(m_softBVertices_Tight,  sbvname.Data()));
       
       
       m_goodSBV_Tight = new xAOD::VertexContainer();
       m_goodSBV_Tightaux = new xAOD::VertexAuxContainer();
       m_goodSBV_Tight->setStore( m_goodSBV_Tightaux);

       for (auto v : *m_softBVertices_Tight) {
	 xAOD::Vertex *temp = new xAOD::Vertex();
	 *temp = *v;
	 m_goodSBV_Tight->push_back(temp);
       }
       //record to the tstore
       CHECK(store->record(m_goodSBV_Tight,"GoodSoftBtaggedVerticesTight"));
       CHECK(store->record(m_goodSBV_Tightaux,"GoodSoftBtaggedVerticesTightAux"));
  
     }


   return true;
}


 

 



