#include "MasterShef/ClassifyAndCalculateHF.h"
#include "TVector3.h"
#include <iostream>
#include <stdlib.h>
#include <cmath>


#define HADPTCUT 5000
#define JETPTCUT 15000
#define JETETACUT 2.5
#define DRCONE   0.4



//-----------------------------------------------//

ClassifyAndCalculateHF::ClassifyAndCalculateHF(xAOD::TEvent*& event, std::string truthcollName,std::string jetcollName, bool calculateVariables):m_event(event),m_truthcollName(truthcollName),m_jetcollName(jetcollName),m_calculateVariables(calculateVariables),m_ext_code(-9999),m_jet_trueflav(0),m_jet_count(0),m_jet_id(0),m_jet_pt(0),m_jet_eta(0),m_jet_phi(0),m_jet_m(0)
{

}

ClassifyAndCalculateHF::~ClassifyAndCalculateHF(){

}

//--------//


void ClassifyAndCalculateHF::apply(HFSystDataMembers *HFSystMembers, int &code, int &ext_code, int &prompt_code){

  int HF_Classification=ClassifyEvent(false);
  int HF_Classification_Prompt=ClassifyEvent(true);

  //std::cout<<"HF CLASSIFICATION after Classify"<<HF_Classification<<std::endl;
  ext_code = HF_Classification;
  prompt_code = HF_Classification_Prompt;

  HFSystMembers -> HF_Classification=HF_Classification;

  if(fabs(ext_code)>=100){
    code = 1;
  }
  else if(fabs(ext_code)==0){
    code = 0;
  }
  else{
    code =-1;
  }

  if(m_calculateVariables){

    std::map<std::string,float> Variables_map= CalculateVariables();

    HFSystMembers->q1_pt= Variables_map["q1_pt"];
    HFSystMembers->q1_eta= Variables_map["q1_eta"];
    HFSystMembers->qq_pt= Variables_map["qq_pt"];
    HFSystMembers->qq_dr= Variables_map["qq_dr"];

  }

}

int ClassifyAndCalculateHF::ClassifyEvent(bool doPrompt){

  initEvent();

  flagJets();
  m_ext_code=countJets(doPrompt);

  return m_ext_code;
}

void ClassifyAndCalculateHF::initEvent(){

  //Checks TruthJet container exists
  const xAOD::JetContainer* truthJets(nullptr);
  if(!m_event->retrieve(truthJets,m_jetcollName).isSuccess()){
    Warning( "ClassifyAndCalculateHF::InitEvent()", "Cannot retrieve TruthJets containers !" );
    return;
  }


  int jet_n=truthJets->size();

  ///initialization of data members///

  m_jet_trueflav.clear();
  m_jet_count.clear();
  m_jet_id.clear();
  m_jet_pt.clear();
  m_jet_eta.clear();
  m_jet_phi.clear();
  m_jet_m.clear();
  m_jet_trueflav.resize(jet_n,0);
  m_jet_count.resize(jet_n,0);
  m_jet_id.resize(jet_n,0);

  for(auto jptr : *truthJets){

    m_jet_pt.push_back(jptr->pt());
    m_jet_eta.push_back(jptr->eta());
    m_jet_phi.push_back(jptr->phi());
    m_jet_m.push_back(jptr->m());
  }


}




void ClassifyAndCalculateHF::flagJets(){


  //Checks TruthParticle container exists
  const xAOD::TruthParticleContainer* truth_particles(0);
  if(!m_event->retrieve(truth_particles,m_truthcollName).isSuccess()){
    Warning( "ClassifyAndCalculateHF::flagJets()", "Cannot retrieve TruthParticles containers !" );
    return;
  }




 ///matches hadrons with jets///

  m_FinalHadrons.clear();
  m_HadronJets.clear();


  int flav;


  for(const xAOD::TruthParticle* part: *truth_particles) {//loop truth

    int OriginFlag = part->isAvailable<int>("TopHadronOriginFlag")?part->auxdata< int >("TopHadronOriginFlag"):-1;

    if( OriginFlag==6 ) continue; //only hadrons flagged with origin at derivation level


    m_FinalHadrons[ part->index() ]=OriginFlag;


    flav = hadronType( part->pdgId() );

    TVector3 hadron, jet;
    hadron.SetPtEtaPhi(part->pt(),part->eta(),part->phi());

    float mindr=99;
    int bestjet=-1;
    //match closest jet in DR


    for(uint jcount=0;jcount<m_jet_pt.size();jcount++){

      jet.SetPtEtaPhi(m_jet_pt.at(jcount),m_jet_eta.at(jcount),m_jet_phi.at(jcount));

      if(jet.DeltaR(hadron) < DRCONE && jet.DeltaR(hadron) < mindr ){
	mindr = jet.DeltaR(hadron);
	bestjet = jcount;
      }

    }//loop over jets



    if(bestjet!=-1){

      m_HadronJets[ part->index() ]=bestjet;

      if((m_jet_id.at(bestjet)==0 || (OriginFlag>0 && OriginFlag>m_jet_id.at(bestjet)) || (OriginFlag<0 && m_jet_id.at(bestjet)<0 && OriginFlag<m_jet_id.at(bestjet)) ) ){

	m_jet_id.at(bestjet) = OriginFlag;

      }

      if(flav > m_jet_trueflav.at(bestjet) && part->pt() >= HADPTCUT){

	m_jet_trueflav.at(bestjet) = flav;
	m_jet_count.at(bestjet) = 1;

      }
      else if(flav == m_jet_trueflav.at(bestjet)){

	m_jet_count.at(bestjet) += 1;
      }

    }//jet chosen
  }//loop over hadrons

}




int ClassifyAndCalculateHF::countJets(bool doPrompt){

  ///count how many hadrons are in a jet///

  int b=0, B=0, c=0, C=0;
  int b_prompt=0, B_prompt=0, c_prompt=0, C_prompt=0;
  for(unsigned int i=0;i<m_jet_id.size();i++){
    if(m_jet_pt.at(i) < JETPTCUT || fabs(m_jet_eta.at(i)) > JETETACUT) continue;
    ///count just additional HF for btype <3 while for c-type <1 and >-3///
    if(m_jet_trueflav.at(i)==5 && m_jet_id.at(i) < 3){
      if(m_jet_count.at(i) > 1){
	B++;
      }
      else{
	b++;
      }
    }
    if(m_jet_trueflav.at(i)==4 && (m_jet_id.at(i)==0 || m_jet_id.at(i)==-1 || m_jet_id.at(i)==-2)){
      if(m_jet_count.at(i) > 1){
	C++;
      }
      else{
	c++;
      }
    }

    if(m_jet_trueflav.at(i)==5 && m_jet_id.at(i)==0){

      if(m_jet_count.at(i) > 1){
	B_prompt++;
      }
      else{
	b_prompt++;
      }
    }
    if(m_jet_trueflav.at(i)==4 && m_jet_id.at(i)==0){
      if(m_jet_count.at(i) > 1){
	C_prompt++;
      }
      else{
	c_prompt++;
      }
    }


  }

  int ext_code = 1000*b+100*B+10*c+1*C;

  int prompt_code=1000*b_prompt+100*B_prompt+10*c_prompt+1*C_prompt;

  ///MPI and FSR categories///

  if(prompt_code==0 && ext_code!=0){
    ext_code=0;
    for(uint ii=0; ii< m_jet_id.size(); ii++){
      if(m_jet_pt.at(ii) < JETPTCUT || fabs(m_jet_eta.at(ii)) > JETETACUT) continue;
      if(m_jet_id.at(ii)==1 && m_jet_trueflav.at(ii)==5){ //b MPI
	ext_code -= 1000;
      } else if(m_jet_id.at(ii)==2 && m_jet_trueflav.at(ii)==5){ //b FSR
	ext_code -= 100;
      } else if(m_jet_id.at(ii)==-1 && m_jet_trueflav.at(ii)==4){ //c MPI
	ext_code -= 10;
      } else if(m_jet_id.at(ii)==-2 && m_jet_trueflav.at(ii)==4) { //c FSR
	ext_code -= 1;
      }
    }
  }

  if(doPrompt==false) return ext_code;
  if(doPrompt==true) return prompt_code;

  return 0;
}






int ClassifyAndCalculateHF::hadronType(int pdgid){

  ///method to transform hadron pdg id to parton flavor///

  int rest1(abs(pdgid%1000));
  int rest2(abs(pdgid%10000));

  if ( rest2 >= 5000 && rest2 < 6000 ) return 5;
  if( rest1 >= 500 && rest1 < 600 ) return 5;

  if ( rest2 >= 4000 && rest2 < 5000 ) return 4;
  if( rest1 >= 400 && rest1 < 500 ) return 4;

  return 0;

}






std::map<std::string,float> ClassifyAndCalculateHF::CalculateVariables(){

  m_VariablesForReweighting.clear();

  m_VariablesForReweighting["q1_pt"]=-99;
  m_VariablesForReweighting["q1_eta"]=-99;
  m_VariablesForReweighting["q1_phi"]=-99;
  m_VariablesForReweighting["q1_m"]=-99;
  m_VariablesForReweighting["q2_pt"]=-99;
  m_VariablesForReweighting["q2_eta"]=-99;
  m_VariablesForReweighting["q2_phi"]=-99;
  m_VariablesForReweighting["q2_m"]=-99;

  m_VariablesForReweighting["qq_pt"]=-99;
  m_VariablesForReweighting["qq_ht"]= -99;
  m_VariablesForReweighting["qq_dr"]=-99;
  m_VariablesForReweighting["qq_m"]= -99;



  int i1 =-1, i2=-1;
  for(uint j=0; j< m_jet_pt.size() && i2<0; j++){
    if(m_jet_pt.at(j) < JETPTCUT || fabs(m_jet_eta.at(j)) > 2.5) continue;
    if((m_ext_code >= 100 && m_jet_trueflav.at(j)==5 && m_jet_id.at(j)<3) ||
       (m_ext_code < 100 && m_jet_trueflav.at(j)==4 && (m_jet_id.at(j) ==0 || m_jet_id.at(j) ==-1 || m_jet_id.at(j) ==-2 )) ){
      if(i1==-1)
	i1 = j;
      else{
	i2 = j;
	break;
      }
    }
  }
  TLorentzVector v1, v2;
  if(i1!=-1){
    v1.SetPtEtaPhiM(m_jet_pt.at(i1), m_jet_eta.at(i1), m_jet_phi.at(i1), m_jet_m.at(i1));


    m_VariablesForReweighting["q1_pt"]=v1.Pt();
    m_VariablesForReweighting["q1_eta"]=v1.Eta();
    m_VariablesForReweighting["q1_phi"]=v1.Phi();
    m_VariablesForReweighting["q1_m"]=v1.M();


    if(i2!=-1){
      v2.SetPtEtaPhiM(m_jet_pt.at(i2), m_jet_eta.at(i2), m_jet_phi.at(i2), m_jet_m.at(i2));


      m_VariablesForReweighting["q2_pt"]=v2.Pt();
      m_VariablesForReweighting["q2_eta"]=v2.Eta();
      m_VariablesForReweighting["q2_phi"]=v2.Phi();
      m_VariablesForReweighting["q2_m"]=v2.M();

      m_VariablesForReweighting["qq_pt"]=(v1+v2).Pt();
      m_VariablesForReweighting["qq_ht"]= v1.Pt()+v2.Pt();
      m_VariablesForReweighting["qq_dr"]=v1.DeltaR(v2);
      m_VariablesForReweighting["qq_m"]= (v1+v2).M();


    }

  }


  return m_VariablesForReweighting;

}
