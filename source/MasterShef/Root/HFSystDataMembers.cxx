#include "MasterShef/HFSystDataMembers.h"

#include <iostream>


//_________________________________________________________________________
//
HFSystDataMembers::HFSystDataMembers():
  HF_Classification(-9),
  q1_pt(-99),
  q1_eta(-99),
  qq_pt(-99),
  qq_dr(-99),
  top_pt(-99),
  ttbar_pt(-99)
{}

//_________________________________________________________________________
//
HFSystDataMembers::HFSystDataMembers( const HFSystDataMembers &dm ){
  
  HF_Classification = dm.HF_Classification;
  q1_pt = dm.q1_pt;
  q1_eta = dm.q1_eta;
  qq_pt = dm.qq_pt;
  qq_dr = dm.qq_dr;
  top_pt = dm.top_pt;
  ttbar_pt = dm.ttbar_pt;

}

//_________________________________________________________________________
//
HFSystDataMembers::~HFSystDataMembers(){
  
}
