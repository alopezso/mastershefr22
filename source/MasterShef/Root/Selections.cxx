#include <MasterShef/MasterShef.h>
//#include "CPAnalysisExamples/errorcheck.h"

static const char* APP_NAME = "MasterShef::Selections";
static SG::AuxElement::ConstAccessor<float> acc_GenFiltMET("GenFiltMET"); //!

bool MasterShef::prepareCuts() {

  //prepare what selections to use
  m_totCuts=0;
  //loop over the user specified tree fill modules
  for(auto name : m_userSelMods){

    //check that it is a knownselection
    //otherwise fail
    //known selections are definied in the MasterShef class constructor
    bool found=(m_allSelMods.count(name)>0);
    if(!found){
      Error(APP_NAME,"Not found '%s' in list of possible fill functions, exiting...",name.c_str());
      Error(APP_NAME,"Printing all possible tree fill modules..");
      this->printAllPossibleSelections();
      return false;
    }
    //if found then add it to the list of tree fillers
    m_funExec[m_totCuts++]= m_allSelMods[name];
    Info(APP_NAME,"User requested to use selection %s, added to the list of selections",name.c_str());
  }
  
  Info(APP_NAME,"m_totCuts=%i",m_totCuts);
   
  Info(APP_NAME,"done preparing cuts!");

  return true;

}



 bool MasterShef :: initialCuts(bool init) {

   // initialise cutflow names
   if (init) {
      m_cuts[m_cutLevel++]="Total in files";
      m_cuts[m_cutLevel++]="GRL";
      m_cuts[m_cutLevel++]="LAr Tile Errors";
      m_cuts[m_cutLevel++]="Trigger";
      m_cuts[m_cutLevel++]="Primary Vertex";
      return true;
   }

   //skip all this for truth analysis                         
   if(m_doTruth){
     //passCut(); //all  *NOT NEEDED!*
     passCut(); //grl 
     passCut(); //Errors  
     passCut(); //triggers 
     passCut(); //PrimaryVertex 
     return true;
   }
   
   //total number in file

   // Here we do the preliminary cuts. All this can be done
   // before processing the event with SUSYTools
   // GRL, TileTrip, LAr Error
   //
   xAOD::TEvent* event = wk()->xaodEvent();
   const xAOD::EventInfo* eventInfo = 0;
   CHECK(event->retrieve( eventInfo, "EventInfo") );


   if (m_isMC) {
     //GRL
     passCut();
   } 
   else {
     //GRL
     bool passGRL=m_grl->passRunLB(*eventInfo);
     if(!passGRL){
       failCut();
       return false;
     }
     passCut();
   }
     
   
   // LAr,TileCal,SCT and Incomplete event errors 
   bool detError=false;
  
   if ((eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error ) || (eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error ) || (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) ) || (eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error ) )
     {
       detError=true;
     }
   
   
   if(!(m_objTool->GetPrimVtx()))
     detError=true;
      
   
   if(detError){
     failCut();
     return false;
   }
  
   passCut();

   //
   // Trigger
   //
   /*
   if(m_applyTrigger!="") {
     
     bool passTrig=false;
     
     //bool passElTrig=false;
     //bool passMuTrig=false;
     
     if(m_applyTrigger=="IsMETTrigPassed"){
       passTrig=m_objTool->IsMETTrigPassed();
     }
     // TRICK for 2lep cutflow !!!
     else if (m_applyTrigger=="IsLEPTrigPassed" && m_objTool->treatAsYear()==2015){
       passTrig=(m_objTool->IsTrigPassed("HLT_e24_lhmedium_L1EM20VH" )|| m_objTool->IsTrigPassed("HLT_e120_lhloose") || m_objTool->IsTrigPassed("HLT_e60_lhmedium") || m_objTool->IsTrigPassed("HLT_mu50") || m_objTool->IsTrigPassed("HLT_mu20_iloose_L1MU15"));
     }
     else if (m_applyTrigger=="IsLEPTrigPassed" && m_objTool->treatAsYear()==2016){
       passTrig=(m_objTool->IsTrigPassed("HLT_e26_lhtight_nod0_ivarloose") || m_objTool->IsTrigPassed("HLT_e140_lhloose_nod0") || m_objTool->IsTrigPassed("HLT_e60_lhmedium_nod0") || m_objTool->IsTrigPassed("HLT_mu50") || m_objTool->IsTrigPassed("HLT_mu26_ivarmedium"));
     }     
     // -------------------------
     else if(m_applyTrigger=="stop0Ltrigs"){
       //OREGON CODE BLOCKS
       bool passMETTrigger=m_objTool->IsMETTrigPassed(true); // runnumber not needed, but isj400 needed - seems to be for 2017
       //check trigger matching later on (ExportTrees), but want to check 1L trigger is fired at all
       bool pass1Ltrigger=false;
       if(m_objTool->treatAsYear()==2015)
	 pass1Ltrigger=(m_objTool->IsTrigPassed("HLT_e24_lhmedium_L1EM20VH" )|| m_objTool->IsTrigPassed("HLT_e120_lhloose") || m_objTool->IsTrigPassed("HLT_e60_lhmedium") || m_objTool->IsTrigPassed("HLT_mu50") || m_objTool->IsTrigPassed("HLT_mu20_iloose_L1MU15"));

       if(m_objTool->treatAsYear()==2016)     
	 pass1Ltrigger=(m_objTool->IsTrigPassed("HLT_e26_lhtight_nod0_ivarloose") || m_objTool->IsTrigPassed("HLT_e140_lhloose_nod0") || m_objTool->IsTrigPassed("HLT_e60_lhmedium_nod0") || m_objTool->IsTrigPassed("HLT_mu50") || m_objTool->IsTrigPassed("HLT_mu26_ivarmedium"));
       passTrig=(passMETTrigger || pass1Ltrigger);
              

     }

     else{
       passTrig=m_objTool->IsTrigPassed(m_applyTrigger);
     }
     
     if (passTrig)
       passCut();
     else {
       failCut();
       return false;
     }

   } 
   else {
     // No trigger
     passCut();
   }
   */
   passCut();
   //
   // Primary vertex
   //
   //get the primary vertices
   m_primVertex = 0;
   EL_RETURN_CHECK("execute",event->retrieve( m_primVertex, "PrimaryVertices"));
   
 
   if (m_primVertex)
     m_numVtx = m_primVertex->size();
   else
     m_numVtx = 0;


   if (m_numVtx>0) {
     passCut();
   } 
   else {
     failCut();
     return false;
   }  


   // initial stuff passed
   return true; 
 }




 bool MasterShef :: triggerMatching(bool init){
   if(init) {
      m_cuts[m_cutLevel++]="Leptons trigger matched";
      return true;
   }


   bool fail = true;
   for (unsigned int ii=0; ii<m_triggerList.size(); ii++) {
     if(m_objTool->IsTrigPassed(m_triggerList.at(ii)) &&  m_trigMatch[m_triggerList.at(ii)]){
       fail=false;
       break;
     }

   }


   if(fail){
     failCut();
     return false;
   }

   passCut();


   return true;

 }




// Jet Cleaning selection 
// parameters: 
// None 
bool MasterShef :: cleanJets(bool init) {
   if(init) {
     Info("cleanJets()","Initialising cleanJets.... ");
     m_cuts[m_cutLevel++]="Jet Cleaning";
     return true;
   }
   
   ATH_MSG_VERBOSE("inside cleanJets");   


   if(m_isPflow){//if we are pflow, then use event cleaning
    //if is pflow do event level jet cleaning instead of jet level
     //https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HowToCleanJets2017
     bool isclean=eventInfo->auxdata<char>("DFCommonJets_eventClean_LooseBad");
     

     if(!isclean){
       failCut();
       ATH_MSG_VERBOSE("some bad jets about!");   
       return false;
     }

     if( !m_objTool->IsPFlowCrackVetoCleaning(m_baselineElectronsBeforeOR,m_signalPhotonsBeforeOR) && m_objTool->treatAsYear() < 2017 ){
       failCut();
       ATH_MSG_VERBOSE("PFlow e/gamma MET crack veto failed. Refer to : https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HowToCleanJetsR21#EGamma_Crack_Electron_topocluste ");
       return false;
     } 
     
     
     if( !m_objTool->IsPFlowCrackVetoCleaning(m_baselineElectronsBeforeOR,m_signalPhotonsBeforeOR) && m_objTool->treatAsYear() < 2017 ){
       failCut();
       ATH_MSG_VERBOSE("PFlow e/gamma MET crack veto failed. Refer to : https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HowToCleanJetsR21#EGamma_Crack_Electron_topocluste ");
       return false;
     }

   }//otherwise use the event level susytools cleanining
   else if (m_badJets->size()!=0) {
     failCut();
     ATH_MSG_VERBOSE("some bad jets about!");   
     return false;
   }
 


   ATH_MSG_VERBOSE("jets are clean ;)");   
   passCut();
  return true; 
}






bool MasterShef :: cleanTightJets(bool init) {
   if(init) {
     Info("cleanTightJets()","Initialising cleanJets.... ");
     m_cuts[m_cutLevel++]="TightJet Cleaning";
     return true;
   }
   int nbadJetsTight = 0;
   for(const auto& jet : *m_signalJets) 
     {
       bool isbad=false;
       double eta = jet->eta();
       double pt = jet->pt();
       double fem = jet->getAttribute<double>(xAOD::JetAttribute::EMFrac);
       std::vector<float> sumPtTrkvec;
       jet->getAttribute( xAOD::JetAttribute::SumPtTrkPt500, sumPtTrkvec );
       double sumpttrk = 0;
       if( ! sumPtTrkvec.empty() ) sumpttrk = sumPtTrkvec[0];
       double fch = sumpttrk/pt;
       if(fabs(eta)<2.4)
   {
     
     //chf veto
     if(fabs(eta)<2 && pt>100*1000)
       {
         if(fch<0.02) isbad=true;
         if(fch<0.05 && fem>0.9) isbad=true;
       }
   }
       if(isbad)nbadJetsTight++;
     }
   // TightJet cleaning
  if (nbadJetsTight>0) {
    failCut();
    return false;
  }
  passCut();
  return true; 
}




// Cosmic selection 
// parameters: 
// None 
bool MasterShef :: cosmic(bool init) {
  // Initialise cutflow names
  if(init) {
     Info("cosmic()","Initialising cosmic.... ");
     m_cuts[m_cutLevel++]="cosmic";
     return true;
  }
  // Cosmic muons
  if (m_cosmicMuons->size() >0) {
        failCut();
        return false;
  }
  passCut();
  return true; 
}

// Clean muon selection 
// parameters: 
// None 
bool MasterShef :: cleanMuons(bool init) {
  // Initialise cutflow names
  if(init) {
     Info("cleanMuons()","Initialising cleanMuons.... ");
     m_cuts[m_cutLevel++]="clean muons";
     return true;
  }
  // Bad muons
  if (m_badMuons->size() >0) {
    failCut();
    return false;
  }
  
  passCut();
  return true; 
}




//baseline lepton selector
bool MasterShef :: SelNBaselineLep(bool init) {
  // Initialise cutflow names
  if(init) {
     Info("cleanMuons()","Initialising nbaseline lepton selector.... ");
     m_cuts[m_cutLevel++]=std::to_string(m_SelnBL_Mx)+">= NbaselineLep >= "+std::to_string(m_SelnBL_Mn);
     return true;
  }
 
  unsigned int nbLep=m_baselineElectrons->size()+m_baselineMuons->size();

  if (nbLep<m_SelnBL_Mn || nbLep>m_SelnBL_Mx){
    failCut();
    return false;
  }
  passCut();
  return true; 
}


bool MasterShef :: Sel0Lep(bool init) {
  // Initialise cutflow names
  if(init) {
     Info("cleanMuons()","Initialising 0 lepton selector.... ");
     m_cuts[m_cutLevel++]="0 Signal Lep";
     return true;
  }
  
  unsigned int nLep=m_signalElectrons->size()+m_signalMuons->size();
  
  if (nLep>0){
    failCut();
    return false;
  }
  passCut();
  return true; 
}







bool MasterShef :: Sel1Ph(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="nphotons=1";
     //m_cuts[m_cutLevel++]="pass HLT_g120_loose";
     //m_cuts[m_cutLevel++]="HLT_g120_loose Trigger Matched";
     m_cuts[m_cutLevel++]="pT_1ph>130";
     return true;
   }
   int NPhotons=m_signalPhotons->size();
   
   if(NPhotons!=1)
     {
       failCut();
       return false;
     }
   passCut();

   // bool passed =  m_objTool->IsTrigPassed("HLT_g120_loose");
   // if(!passed){
   //   failCut();
   //   return false;
   // }
   // passCut();

   // bool matched = m_objTool->IsTrigMatched((*m_signalPhotons)[0],"HLT_g120_loose");
   // if(!matched){
   //   failCut();
   //   return false;
   // }
   // passCut();

   double pT_1ph = m_signalPhotons->at(0)->pt()/1000.;
   if(pT_1ph<130){
     failCut();
     return false;
   }
   passCut();
     
     


   return true;
}
   



bool MasterShef :: Sel1L(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="NbaselineLeptons==1";
     m_cuts[m_cutLevel++]="NLeptons==1";
     m_cuts[m_cutLevel++]="PT ele/Muon";

     Info("Sel1L()","Initialising One lepton selector.... ");
     Info("Sel1L()","  Use signal leptons=%i",m_Sel1L_Sgnl);
     Info("Sel1L()","  Min Electron Pt=%f",m_Sel1L_PtMnE);
     Info("Sel1L()","  Max Electron Pt=%f",m_Sel1L_PtMxE);
     Info("Sel1L()","  Min Muon Pt=%f",m_Sel1L_PtMnM);
     Info("Sel1L()","  Max Muon Pt=%f",m_Sel1L_PtMxM);
     return true;
   }
   int NLep=m_signalElectrons->size()+m_signalMuons->size();
   int NbLep=m_baselineElectrons->size()+m_baselineMuons->size();
   
   if(!(/*NLep==1*/ NbLep==1) ) {
       failCut();
       return false;
   }
   passCut();

   if(!(NLep==1) ) {
       failCut();
       return false;
   }
   passCut();

   bool passPt=false;
   if (m_signalElectrons->size()>0) {
      double elPt=m_signalElectrons->at(0)->pt();
      if (elPt > m_Sel1L_PtMnE && elPt<m_Sel1L_PtMxE)
          passPt=true;
   }
   if (m_signalMuons->size()>0) {
      double muPt=m_signalMuons->at(0)->pt();
      if (muPt > m_Sel1L_PtMnM && muPt<m_Sel1L_PtMxM)
          passPt=true;
   }
   if (!passPt) {
       failCut();
       return false;
   }
   passCut();
   return true; 
}





bool MasterShef :: Sel2L(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="(2 sig Lep SF )==1";
     m_cuts[m_cutLevel++]="(pT1lep > 26";
     m_cuts[m_cutLevel++]="(2 baseline SF )==1";
     Info("Sel2L()","Initialising Two lepton  selector.... ");
     return true;
   }
   
 
   unsigned int nEl=m_signalElectrons->size();
   unsigned int nMu=m_signalMuons->size();
   

   if (!((nEl==0 && nMu==2) || (nEl==2 && nMu==0))){
     failCut();
     return false;
   }
   passCut();
   
   // if(!((nEl==2 && nMu==0) || (nEl==0 && nMu==2)))
   //   {
   //     failCut();
   //     return false;
   //   }
   // passCut();
       
   if(nEl==2){
     if(m_signalElectrons->at(0)->pt()<26*1000){
       failCut();
       return false;
     }
   }
   else if(nMu==2){
     if(m_signalMuons->at(0)->pt()<26*1000){
       failCut();
       return false;
     } 
   }
   passCut();


   nEl=m_baselineElectrons->size();
   nMu=m_baselineMuons->size();
   
   if(!((nEl==2 && nMu==0) || (nEl==0 && nMu==2)))
     {
       failCut();
       return false;
     }
   passCut();






   return true;
}





bool MasterShef :: SelNL(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="(nLep )>="+std::to_string(m_SelnL_Mn); 
     m_cuts[m_cutLevel++]="(nLep )<="+std::to_string(m_SelnL_Mx); 
     Info("SelNL()","Initialising N lepton  selector.... ");
     return true;
   }
   
 
   int nEl=m_signalElectrons->size();
   int nMu=m_signalMuons->size();
   

   if (nEl+nMu < m_SelnL_Mn){
     failCut();
     return false;
   }
   passCut();
   
   

   if (nEl+nMu > m_SelnL_Mx){
     failCut();
     return false;
   }
   passCut();



   return true;
}








bool MasterShef :: SelDMbb(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="(DMbb)";
     Info("SelDMbb()","Initialising sbottom analysis Selection selector.... ");
     return true;
   }
   
   unsigned int nB = m_BJets->size();
   
   if(nB>=2 || (m_etmiss/1000.)>600. ){
     passCut();
     return true;
   }
   
   failCut();
   return false;
}



bool MasterShef :: SelSeedEvents(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="prescale weight > 0";
     m_cuts[m_cutLevel++]="(met-8)/sqrt(sumet) < 0.3  ";//|| metsig(object based) < 0.5 ";
     Info("SelSbottom()","Initialising seed event Selection selector.... ");
     return true;
   }

    
   if(!m_isMC && m_useTriggerWeight && m_TriggerWeight==0){
     failCut();
     return false;
   }
   
   passCut();

   //old seed selection loose
   double M=8.;
   //unsigned int nB= m_BJets->size();
   //m_passSeedSelection =  true;
   //m_passSeedSelection =((( (m_etmiss/1000.)-M) / pow((m_sumet/1000.),0.5  ))  < (0.6+0.2*nB) ); //((m_etmiss/1000.) < 70.) ;
   m_passSeedSelection =((( (m_etmiss/1000.)-M) / pow((m_sumet/1000.),0.5  ))  < 0.3 );
   
   //new object based metsig - loose 
   //m_passNewSeedSelection = m_metsigST < 0.5;

   
   if( !(m_passSeedSelection)){// || m_passNewSeedSelection) ){
     failCut();
     return false;
   }

  passCut();
  return true;
}

bool MasterShef :: SelPseudoEvents(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="perform JetSmearing";
     //m_cuts[m_cutLevel++]="check Nsmears";
     m_cuts[m_cutLevel++]="any smrMET>200";
     Info("SelSbottom()","Initialising seed event Selection selector.... ");
     return true;
   }

   //fail if we weren't suppose to be doing jetsmearing
   if(!m_doJetSmearing){
     failCut();
     return false;
   }

   //fail if not at least one signal jet
   if(m_signalJets->size()==0){
     failCut();
     return false;
   }

   
   //check nsmears
   this->jetSmearing();
   passCut(); // ntuple dumps here on level 12

   
   
   int ngood_smears=0;
   for(unsigned int i=0; i < m_smrMET.size(); i++)
     {
       xAOD::MissingETContainer::const_iterator smet_it = m_smrMET[i]->find("Final");
       double met = (*smet_it)->met()/1000.;
       
       if(met>200){
	 ngood_smears++;
       }
     }
 
   if(ngood_smears==0){
     failCut();
     return false;
   }
   
   passCut();
   return true;
}






bool MasterShef :: SelSbottom(bool init) {
  if (init) {
    m_cuts[m_cutLevel++]="(sbottom filtering selection)";
    Info("SelSbottom()","Initialising sbottom analysis Selection selector.... ");
    return true;
  }
  
  unsigned int nLep=0;
  
  for (unsigned int i=0;i<m_signalElectrons->size();i++) {
    //double elPt=m_signalElectrons->at(i)->pt();
    //if (elPt > m_SelSbottom_PtMnE && elPt<m_SelSbottom_PtMxE)
    nLep++;
  }
  
  for (unsigned int i=0;i<m_signalMuons->size();i++) {
    //double muPt=m_signalMuons->at(i)->pt();
    //if (muPt > m_SelSbottom_PtMnM && muPt<m_SelSbottom_PtMxM) 
    nLep++;     
  }

  unsigned int nB = m_BJets->size();

  unsigned int nphotons = m_signalPhotons->size();
  double pT_ph = 0;
  if(nphotons>=1) pT_ph = m_signalPhotons->at(0)->p4().Pt()/1000.;


     
  bool zeroLepExp=(nLep==0 && nB>=2 && (m_etmiss/1000.)>200.);
  bool oneLepExp =(nLep==1 && nB>=1 && (m_etmiss/1000.)>100.);
  bool twoLepExp =(nLep>=2 && (  (nB>=2 && (m_etmiss_prime/1000.)>100.) || ((m_etmiss_prime/1000.)>250.)) );
  bool onePhExp  =(nphotons>=1 && pT_ph>130 );

  if(onePhExp || zeroLepExp || oneLepExp || twoLepExp ){
    passCut();
    return true;
  }
   
  failCut();
  return false;
}

bool MasterShef :: SelSbottomMultib(bool init) {
  if (init) {
    m_cuts[m_cutLevel++]="SbMb loose selection (0-2 lep, >=2 bjets)";
    m_cuts[m_cutLevel++]="SbMb tight selection (>=3 bjets || high metsig)";
    Info("SelSbottomMultib()","Initialising sbottom multib analysis Selection selector.... ");
    return true;
  }
  unsigned int nsigLep = m_signalElectrons->size()+m_signalMuons->size();
  unsigned int nbaseLep = m_baselineElectrons->size()+m_baselineMuons->size();
  unsigned int nbjets = m_BJets->size();
  /*
  unsigned int nb_MH77 = 0;
  unsigned int nb_DF77 = 0;
  unsigned int nb_DH77 = 0;

  for(const auto &jet: *m_signalJets){
    double weight_DL1 = jet->auxdata< double >("weight_DL1");
    int isMV2c10H77 = (jet->auxdata< bool >("isMV2c10H77") == true ) ? 1 : 0;
    int isDL1H77 = (jet->auxdata< bool >("isDL1H77") == true ) ? 1 : 0;

    if (isMV2c10H77) nb_MH77++;
    if (isDL1H77) nb_DH77++;
    if (weight_DL1 > m_cutval_DL1F77 && fabs(jet->eta()) < 2.5) nb_DF77++;
  }

  bool passSelection = (nbaseLep == 0 || nsigLep == 1 || nsigLep == 2) && (nb_MF77 > 2 || nb_MH77 > 2 || nb_DF77 > 2 || nb_DH77 > 2);
  */// turn off this multi-tag selection

  bool passSelection = (nbaseLep == 0 || nsigLep == 1 || nsigLep == 2) && nbjets >= 2;
  
  bool passTightSelection = nbjets>=3 || m_metsigST>20 || m_metsigET>20 || m_metsigHT >20;


  if(!passSelection){
    failCut();
    return false;
  }
  
  passCut();

  if(!passTightSelection){
    failCut();
    return false;
  }

  passCut();

  return true;
  

}


bool MasterShef :: SelttZ2L(bool init) {
 if (init) {
    m_cuts[m_cutLevel++]="(ttZ 2L filtering selection)";
    Info("SelttZ()","Initialising ttZ 2L analysis Selection selector.... ");
    return true;
  }
  unsigned int nLep=m_signalElectrons->size()+m_signalMuons->size();
  unsigned int nB = m_BJets->size();
  unsigned int nJ = m_signalJets->size();

  bool passSelection = (nLep>=2  && nJ>=4 && nB>=2);

  if(passSelection){
    passCut();
    return true;
  }

  failCut();
  return false;
}


bool MasterShef :: SelttZ3L(bool init) {
  if (init) {
    m_cuts[m_cutLevel++]="(ttZ 3L filtering selection)";
    Info("SelttZ()","Initialising ttZ 3L analysis Selection selector.... ");
    return true;
  }
  unsigned int nLep=m_signalElectrons->size()+m_signalMuons->size();
  unsigned int nB = m_BJets->size();
  //unsigned int nJ = m_signalJets->size();

  bool passSelection = (nLep>=3  &&  nB>=1);

  if(passSelection){
    passCut();
    return true;
  }

  failCut();
  return false;
}


bool MasterShef :: SelttZ(bool init) {
  if (init) {
    m_cuts[m_cutLevel++]="(ttZ filtering selection)";
    Info("SelttZ()","Initialising ttZ  analysis Selection selector.... ");
    return true;
  }
  unsigned int nLep=m_signalElectrons->size()+m_signalMuons->size();
  unsigned int nB = m_BJets->size();
  //unsigned int nJ = m_signalJets->size();

  unsigned int nTau = m_signalTausBeforeOR->size();
  double met = m_etmiss/1000.;

  unsigned int nB_85 = 0;
  for(const auto &jet: *m_signalJets){
    double MV2=-999;
    xAOD::BTaggingUtilities::getBTagging(*jet)->MVx_discriminant("MV2c10", MV2);  
    if(MV2>0.1758475)nB_85++;
  }

  //require at least 2 77% WP b-jets and 4 in total b-jets
  bool pass4bSelection = nB>=2 && nB_85>=4;
  //require at least 2 b-jets and 2 taus
  bool pass2b2tauSelection = nB>=2 && nTau>=2;
  //require 0L,2b and met>200
  bool pass0LSelection = nLep==0 && nB>=2 && met>200;
  //require 1L,2b and met>100
  bool pass1LSelection = nLep==1 && nB>=2 && met>100;
  //require >=2 leptons and 2-bjets
  bool pass2LSelection = nLep>=2 && nB>=2;
  
  

  bool passSelection = (pass4bSelection || pass2b2tauSelection || pass0LSelection || pass1LSelection ||  pass2LSelection );

  if(passSelection){
    passCut();
    return true;
  }

  failCut();
  return false;
}




bool MasterShef :: SelSbottomTight(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="(sbottom tight filtering selection)";
     Info("SelSbottom()","Initialising sbottom analysis Selection selector.... ");
     return true;
   }
   unsigned int nLep=0;

   for (unsigned int i=0;i<m_signalElectrons->size();i++) {
     double elPt=m_signalElectrons->at(i)->pt();
     if (elPt > 20)
       nLep++;
   }
   for (unsigned int i=0;i<m_signalMuons->size();i++) {
     double muPt=m_signalMuons->at(i)->pt();
     if (muPt > 20)
       nLep++;     
   }

   unsigned int nB = m_BJets->size();
   unsigned int njets = m_signalJets->size();
    
  
   
 
   double mctjj=0;
   if(m_signalJets->size()>=2)
     {
       mctjj=mcttool->mct(m_signalJets->at(0)->p4(),m_signalJets->at(1)->p4());
     }

   
   double MTj1=0;
   double MTj2=0;
   double MTj3=0;
   double MTj4=0;
   double MTjmin=0;
   double pT_1jet=0;
   if(njets>=2)
     {
       TVector2 met_fake(m_pxmiss/1000.,m_pymiss/1000.);
       TLorentzVector j1 = m_signalJets->at(0)->p4()*0.001;
       TLorentzVector j2 = m_signalJets->at(1)->p4()*0.001;
       
       pT_1jet=j1.Pt();
          
       MTj1 = sqrt(2*(met_fake.Mod())*j1.Et() - 2*((met_fake.Px())*j1.Px() + (met_fake.Py())*j1.Py()));
       MTj2 = sqrt(2*(met_fake.Mod())*j2.Et() - 2*((met_fake.Px())*j2.Px() + (met_fake.Py())*j2.Py()));
       MTjmin=std::min(MTj1,MTj2);
       


       if(njets>=3)
   {
     TLorentzVector j3 = m_signalJets->at(2)->p4();

     MTj3 = sqrt(2*(met_fake.Mod())*j3.Et() - 2*((met_fake.Px())*j3.Px() + (met_fake.Py())*j3.Py()));
     MTjmin=std::min(MTj1,std::min(MTj2,MTj3));
     
     if(njets>=4)
       {
         TLorentzVector j4 = m_signalJets->at(3)->p4();
         MTj4 = sqrt(2*(met_fake.Mod())*j4.Et() - 2*((met_fake.Px())*j4.Px() + (met_fake.Py())*j4.Py()));
         MTjmin=std::min(std::min(MTj1,MTj4),std::min(MTj2,MTj3));
       }
   }
     }
  

   bool leadb1 = m_objTool->IsBJet(*(m_signalJets->at(0)));

   //unsigned int nphotons = m_signalPhotons->size();
   //double pT_ph = 0;
   //if(nphotons>=1)
   //  pT_ph = m_signalPhotons->at(0)->p4().Pt()/1000.;


   bool pass1Lfilter=(nLep==1 && ( nB==1 || nB==2) && (m_etmiss/1000.)>100.  &&  (mctjj>150 || (pT_1jet>500 && !leadb1) || MTjmin>200 ) );
   bool pass0Lfilter=(nLep==0 && nB==2 && (m_etmiss/1000.)>250.);
   bool pass2Lfilter=(nLep==2 && nB==2);
          

   if( pass0Lfilter || pass1Lfilter || pass2Lfilter){
     passCut();
     return true;
   }
   
   failCut();
   return false;
}
double MasterShef :: CalcMTbmin(){
  
  double MTbmin=0;
  double dPhi_bjet=0;
  double dPhi_min=99;
  unsigned int ref_min=0;
  unsigned int nB=m_BJets->size();
  TVector2 MetVec;
  MetVec.Set(m_pxmiss/1000.,m_pymiss/1000.);
  if(nB>0){
    for (unsigned int k=0; k<nB;k++){       
      dPhi_bjet=TVector2::Phi_mpi_pi(m_BJets->at(k)->p4().Phi()-MetVec.Phi());       
      if(fabs(dPhi_bjet)<fabs(dPhi_min)){ 	
	dPhi_min=dPhi_bjet; 	
	ref_min=k;       
      }     
    }     
    double MET=m_etmiss/1000.;     
    MTbmin=sqrt(2.*(MET)*((m_BJets->at(ref_min)->p4().Pt()/1000.))*( 1 - TMath::Cos(dPhi_min)));
  }
  return MTbmin;
}

double MasterShef :: CalcMTcmin(){

  double MTcmin=0;
  double dPhi_bjet=0;
  double dPhi_min=99;
  unsigned int ref_min=0;
  unsigned int nC=m_CJets->size();
  TVector2 MetVec;
  MetVec.Set(m_pxmiss/1000.,m_pymiss/1000.);
  if(nC>0){
    for (unsigned int k=0; k<nC;k++){
      dPhi_bjet=TVector2::Phi_mpi_pi(m_CJets->at(k)->p4().Phi()-MetVec.Phi());
      if(fabs(dPhi_bjet)<fabs(dPhi_min)){
	dPhi_min=dPhi_bjet;
        ref_min=k;
      }
    }
    double MET=m_etmiss/1000.;
    MTcmin=sqrt(2.*(MET)*((m_CJets->at(ref_min)->p4().Pt()/1000.))*( 1 - TMath::Cos(dPhi_min)));
  }
  return MTcmin;
}

double MasterShef:: CalcLeadingKt12FatJetMass(){
   double m_1fatjet_kt12=0;
   if(m_rcjets_kt12!=nullptr){ // null pointer check
     if(m_rcjets_kt12->size()>0){ // need 0 fatjet check     
       m_1fatjet_kt12=m_rcjets_kt12->at(0)->m()/1000.; 
     }
   }
  return m_1fatjet_kt12;
}

bool MasterShef :: SelStop0L(bool init) {
  if (init) {
    m_cuts[m_cutLevel++]="(stop0L loose selection)";
    Info("SelStop0L()","Initialising stop0L analysis Selection selector.... ");
    return true;
  }
  if (m_doTruth){
    passCut();
    return true;    
  }

  /// ************************
  /// Definition of the main variables
  /// ************************


  unsigned int nEl=m_signalElectrons->size();
  unsigned int nMu=m_signalMuons->size();
  unsigned int nLep=nEl+nMu;
  unsigned int nB=m_BJets->size();
  unsigned int nJets=m_signalJets->size();
  unsigned int nBaselineEl=m_baselineElectrons->size();
  unsigned int nBaselineMu=m_baselineMuons->size();
  unsigned int nBaselineLep=nBaselineEl+nBaselineMu;

  std::vector<TLorentzVector> leptons;
  leptons.clear();
  std::vector<TLorentzVector> jetLeps;
  jetLeps.clear();
  for( auto electron : *m_signalElectrons ){
    TLorentzVector fourmom ;
    fourmom.SetPtEtaPhiE(electron->pt(),electron->eta(),electron->phi(),electron->e());
    leptons.push_back(fourmom);
    jetLeps.push_back(fourmom);
  }
  for( auto muon : *m_signalMuons ){
    TLorentzVector fourmom ;
    fourmom.SetPtEtaPhiE(muon->pt(),muon->eta(),muon->phi(),muon->e());
    leptons.push_back(fourmom);
    jetLeps.push_back(fourmom);
  }
  for( auto jet : *m_signalJets ){
    TLorentzVector fourmom ;
    fourmom.SetPtEtaPhiE(jet->pt(),jet->eta(),jet->phi(),jet->e());
    jetLeps.push_back(fourmom);
  }

  std::sort(leptons.begin(),leptons.end(),ptfloatsorter);
  std::sort(jetLeps.begin(),jetLeps.end(),ptfloatsorter);

  double MTbmin=CalcMTbmin();
  double m_1fatjet_kt12=CalcLeadingKt12FatJetMass();
 
  #ifdef USE_RESTFRAMES
  GetRJRVariables();
  #endif
 
  /// ************************
  /// Selection cuts
  /// ************************

  const xAOD::EventInfo* eventInfo = 0;
  CHECK(event->retrieve( eventInfo, "EventInfo"));

  // 0L block (should cover all regions)
  if(nLep==0 && nBaselineLep==0 && nJets>=4 && nB>=1 && m_etmiss/1000.>=250
     && jetLeps[1].Pt()/1000. >= 80 && jetLeps[3].Pt()/1000. >= 40){
    //SRA/B target cuts
    if(nB>=2 && m_1fatjet_kt12>120 && MTbmin>50){
      passCut();
      return true;
    }
    //SRC target cuts
    if(nB>=2 && m_NjV>=4 && m_NbV>=1 && m_pTbV1>=40 && m_pTjV4>=40)
      {	
	passCut();
	return true;	
      }
  }
  // 1L selection branch  
  else if(nLep==1 && nBaselineLep==1 && m_etmiss/1000.>=250  && nB>=1  && nJets >=3 && jetLeps[1].Pt()/1000. >= 80 && jetLeps[3].Pt()/1000. >= 40){ 
      
    //CRTop
    TVector2 MetVec;
    MetVec.Set(m_pxmiss/1000.,m_pymiss/1000.);    
    double mTLepMet= sqrt(2*(MetVec.Mod()*(leptons[0].Pt()/1000.)) - 2*((MetVec.Px()*leptons[0].Px()/1000.) + (MetVec.Py()*leptons[0].Py()/1000.)) );
    
    if(mTLepMet>120){
      failCut();
      return false;
    }
    
    if(MTbmin>=150 && mTLepMet<=100){
      passCut();
      return true;
    }else if(nB>=1 && leptons[0].Pt()/1000.>=20 && m_NjV>=3 && m_NbV>=2 && mTLepMet<=100){ // CRTopC
      passCut();
      return true;
    }else if( nB>=2 && MTbmin>=150 && m_1fatjet_kt12>=120 ){ // CRTopAB  requires loose lepton pT. Tighter requirements 
      passCut();
      return true;
    }
    
  }
  //2L branch
  else if(nLep==2 && nBaselineLep==2 && nJets>=4 && m_etmiss/1000.<70 && m_etmiss_prime/1000.>100 && (nEl==2||nMu==2) ){ //CRZ
      
    int charge_1lep=-1; 
    int charge_2lep=-1; 
     
    if(nMu==2){
      charge_1lep=m_signalMuons->at(0)->charge();
      charge_2lep=m_signalMuons->at(1)->charge();
    } else if (nEl==2){
      charge_1lep=m_signalElectrons->at(0)->charge();
      charge_2lep=m_signalElectrons->at(1)->charge();
    }

    if(charge_1lep+charge_2lep==0 && (leptons[0].Pt()/1000.>=20. && leptons[1].Pt()/1000. >=20.)){
      passCut();
      return true;
    }
  }    
  // 3l branch: ttZ
  else if(nLep==3 && nBaselineLep==3 && 
	  leptons[0].Pt()/1000. >= 20. && leptons[1].Pt()/1000. >=20.  && leptons[2].Pt()/1000. >= 20.
	  && nJets>=2 && nB>=2 ){
    passCut();
    return true;
  }

  
  failCut();
  return false;
}
bool MasterShef::SelStop0LTight(bool init){
  if(init){
    m_cuts[m_cutLevel++]="(stop0L tight selection)";
    Info("SelStop0LTight()","Initialising stop0L tight analysis Selection selector.... ");
    return true;
  }
  
  if (m_doTruth){
    passCut();
    return true;
  }

  /// ************************
  /// Definition of the main variables
  /// ************************
  
  unsigned int nEl=m_signalElectrons->size();
  unsigned int nMu=m_signalMuons->size();
  unsigned int nLep=nEl+nMu;
  unsigned int nB=m_BJets->size();

  unsigned int nBaselineEl=m_baselineElectrons->size();
  unsigned int nBaselineMu=m_baselineMuons->size();
  unsigned int nBaselineLep=nBaselineEl+nBaselineMu;
  double MTbmin=CalcMTbmin();
  double m_1fatjet_kt12=CalcLeadingKt12FatJetMass();

  std::vector<TLorentzVector> leptons;
  std::vector<TLorentzVector> jetLeps;
  for( auto electron : *m_signalElectrons ){
    TLorentzVector fourmom ;
    fourmom.SetPtEtaPhiE(electron->pt(),electron->eta(),electron->phi(),electron->e());
    leptons.push_back(fourmom);
    jetLeps.push_back(fourmom);
  }
  for( auto muon : *m_signalMuons ){
    TLorentzVector fourmom ;
    fourmom.SetPtEtaPhiE(muon->pt(),muon->eta(),muon->phi(),muon->e());
    leptons.push_back(fourmom);
    jetLeps.push_back(fourmom);
  }
  for( auto jet : *m_signalJets ){
    TLorentzVector fourmom ;
    fourmom.SetPtEtaPhiE(jet->pt(),jet->eta(),jet->phi(),jet->e());
    jetLeps.push_back(fourmom);
  }

  std::sort(leptons.begin(),leptons.end(),ptfloatsorter);
  std::sort(jetLeps.begin(),jetLeps.end(),ptfloatsorter);

  #ifdef USE_RESTFRAMES
  GetRJRVariables();
  #endif

  if(nLep==0 && nBaselineLep==0){ // 0 Lepton selection 
    if(nB>=2 && MTbmin>=120 && m_1fatjet_kt12>=120){ // additional cuts only for SRA/B-tight
      passCut();
      return true;
    }
    //SRC tight cut

    if(m_PTISR>=400 && m_MS>=300 && m_NjV>=4 && m_NbV>=2 && m_pTbV1>=40 && m_pTjV4>=40){        
      passCut();
      return true;
    }
  } else if (nLep==1 && nBaselineLep==1){ // 1 Lepton selection

    if(MTbmin >= 200 && nB==1){ // CRW,CRST
      passCut();
      return true;
    }
    if(MTbmin >= 150 && nB>=2){ // CRST
      passCut();
      return true;
    }
    if(MTbmin >= 150 && m_1fatjet_kt12>=120 && nB>=2){ // CRttB
      passCut();
      return true;
    }
    // CRTopC selections only
    if( m_NjV >=4 && m_NbV>=1 && m_pTbV1>=40 && m_pTjV4>=40 && m_PTISR>=300 && m_MS>=300 ){
      passCut();
      return true;
    }

  } else if (nLep==2 && nBaselineLep==2 && (nEl == 2 || nMu == 2 ) ){ // 2 Lepton selection 
    TLorentzVector lep1 = nMu == 2 ? m_signalMuons->at(0)->p4() : m_signalElectrons->at(0)->p4() ;
    TLorentzVector lep2 = nMu == 2 ? m_signalMuons->at(1)->p4() : m_signalElectrons->at(1)->p4() ;

    if( (lep1 + lep2).M()/1000. < 105. && (lep1 + lep2).M()/1000. > 80. && (lep1.Pt()/1000. >= 20. && lep2.Pt()/1000. >= 20.)){
      // Just cut on Z mass for the 2L. 
      passCut();
      return true; 
    }
  } else if (nLep==3 && nBaselineLep==3){ // 3 Lepton selection 
    passCut();
    return true;
  }

  failCut();
  return false;
}




bool MasterShef :: SelStop4Body(bool init) {
  if (init) {
    m_cuts[m_cutLevel++]="(stop 4 body selection)";
    Info("SelStop0L4Body()","Initialising stop4body analysis Selection selector.... ");
    return true;
  }
  if (m_doTruth){
    passCut();
    return true;    
  }


  /// ************************
  /// Definition of the main variables
  /// ************************

  unsigned int nEl=m_signalElectrons->size();
  unsigned int nMu=m_signalMuons->size();
  unsigned int nLep=nEl+nMu;
  unsigned int nB=m_BJets->size();
  unsigned int nBaselineEl=m_baselineElectrons->size();
  unsigned int nBaselineMu=m_baselineMuons->size();
  unsigned int nBaselineLep=nBaselineEl+nBaselineMu;

  int nJets=m_signalJets->size();
  int nBTrackJets=0;

  float pT_1trkjet = -999;
  float pT_1btrkjet = -999;
  TLorentzVector lepton;
  TLorentzVector jets;


  // Ensure that we use the track jets that don't overlap 
  // with the ISR jet and that have a meaningful number of constituents

  for (const auto& jet : *m_signalTrackJets ){
    if (jet->auxdata<int>("passISROR") == 1 && jet->numConstituents() > 1){
      pT_1trkjet=jet->pt();
      break;
    }
  }
  for (const auto& jet : *m_signalBTrackJets ){
    if( jet->auxdata<int>("passISROR") == 1 && jet->numConstituents() > 1 ){
      pT_1btrkjet=jet->pt(); 
      break;
    }
  }


  for (const auto& jet : *m_signalBTrackJets ){
    if( jet->auxdata<int>("passISROR") == 1 && jet->numConstituents() > 1 ){
      ++nBTrackJets;
    }
  }


  // Leptons
  std::vector<TLorentzVector> leptons;    
  for( auto electron : *m_signalElectrons ){
    TLorentzVector fourmom ;
    fourmom.SetPtEtaPhiE(electron->pt(),electron->eta(),electron->phi(),electron->e());
    leptons.push_back(fourmom);
  }
  for( auto muon : *m_signalMuons ){
    TLorentzVector fourmom ;
    fourmom.SetPtEtaPhiE(muon->pt(),muon->eta(),muon->phi(),muon->e());
    leptons.push_back(fourmom);
  }

  // Calculate important variables for 1L/2L regions
  double m_ll=0.0;
  double mTLepMet=-999;

  if(nLep==1){
    TVector2 MetVec;
    MetVec.Set(m_pxmiss/1000,m_pymiss/1000);
    mTLepMet=sqrt(2*(MetVec.Mod()*(leptons[0].Pt()/1000)) - 2*((MetVec.Px()*leptons[0].Px()/1000) + (MetVec.Py()*leptons[0].Py()/1000)) );
  }else if(nLep==2){    
    m_ll=(leptons[0]+leptons[1]).M()/1000; 
  }


  /// ************************
  /// Selection cuts
  /// ************************

  if(nJets < 1 ){
    failCut();
    return false;
  }

  // Add the VR overlap
  // 0L SR
  if (nLep==0 && nBaselineLep==0 && m_signalJets->at(0)->pt()/1000. >= 250 && m_etmiss/1000. >=250 ){
    //SRD0
    if( nB == 0 && nBTrackJets >=1 && pT_1btrkjet/1000. <= 70. ){
      passCut();
      return true;
    }
    //SRD1
    if( nB == 1 && nBTrackJets >=1 && pT_1trkjet/1000. <= 60 ){      
      passCut();
      return true;      
    }
    //SRD2
    if( nB >= 2 && m_BJets->at(0)->pt()/1000. <= 200){
      passCut();
      return true;
    }
  }
  // 1L control regions and validation
  else if (nLep == 1 && nBaselineLep==1 && m_signalJets->at(0)->pt()/1000. > 250 && m_etmiss/1000. >=250 
	    && mTLepMet < 130 
	    ){ 
    //CRD0 : VRW0
    if( nB == 0 && nBTrackJets >=1 && pT_1btrkjet/1000. < 70. ){
      passCut();
      return true;
    }
    //CRD1 : CRDttbar1, CRDsingletop,CRW, VRW1, VRst1
    if( nB == 1 && nBTrackJets >=1 && pT_1trkjet/1000. < 60 ){
      passCut();
      return true;
    }
    //CRD2 : CRDttbar2
    if( nB >= 2 && m_BJets->at(0)->pt()/1000. < 200 && m_signalJets->at(0)->pt()/1000. > 250 && m_etmiss/1000. >=250){
      passCut();
      return true;
    }
  }
  // 2L regions: CRZ
  else if(nLep==2 && nBaselineLep == 2 && (nEl == 2 || nMu == 2 )  
	  && m_signalJets->at(0)->pt()/1000. >= 100. && (nEl==2||nMu==2) && m_etmiss_prime/1000. >= 150. 
	  && m_etmiss/1000.<=70 && fabs(m_ll-91.03) < 11 && leptons[0].Pt()/1000 >= 27 // && leptons[1].Pt()/1000 >= 20
	  ){

    //CRD0 : CRDZ0
    if( nB == 0 && nBTrackJets >=1 && pT_1btrkjet/1000. <= 70.  
	&& m_signalJets->at(0)->pt()/1000. >= 200. && m_etmiss_prime/1000. >= 200 ){
      passCut();
      return true;
    }
    //CRD1 : CRDZ1
    if( nB == 1 && nBTrackJets >=1 && pT_1trkjet/1000. <= 60  &&  m_signalJets->at(0)->pt()/1000. >= 175.){
      passCut();
      return true;
    }
    //CRD2 : CRDZ2
    if( nB >= 2  && m_BJets->at(0)->pt()/1000. <= 200 && m_signalJets->at(0)->pt()/1000. >= 250 && m_etmiss_prime/1000. >= 200){
      passCut();
      return true;
    }
  }
  failCut();
  return false;
}

double MasterShef::Calc_4b_PTISR(bool IncludeLepton){
  // function to calculate the 4body PTISR or PTISR_prime. note this is different from the RJR definition... 
  // function also allows calculation of ISR_Prime
  double PTISR=0.0;
  unsigned int nEl=m_signalElectrons->size();
  unsigned int nMu=m_signalMuons->size();
  if (!IncludeLepton && m_signalJets->size()>0){
    for (xAOD::Jet* jet: *m_signalJets){
      if(jet->auxdata<char>("bjet")==false && fabs(jet->eta())<2.5){
	PTISR=jet->pt()/1000.;
	break;
      }
    }    
  }
  else if (IncludeLepton && m_signalJets->size()>0){ // if nJets =0, don't bother, should never be the case though    
    // calc PTISR_PRIME
    std::vector<TLorentzVector> JetCollection;
    // init empty
    JetCollection.clear();
    // since we're only looking for non-btagged jets, skim out bjets in the collection, then add the leptons
    for (xAOD::Jet* jet: *m_signalJets){
      if(jet->auxdata<char>("bjet")==false && fabs(jet->eta())<2.5){
	TLorentzVector tlv=(jet->p4());
	JetCollection.push_back(tlv);
      }      
    }
    if((nEl+nMu)>=2){
      return 0.0;
    }
    
    if(nEl==1){
      TLorentzVector tlv=(m_signalElectrons->at(0)->p4());
      JetCollection.push_back(tlv);
    }
    else if(nMu==1){
      TLorentzVector tlv=(m_signalMuons->at(0)->p4());
      JetCollection.push_back(tlv);
    }
    //sort the jet collection
    struct{
      bool operator()(TLorentzVector v1, TLorentzVector v2){
	return v1.Pt()> v2.Pt();
      }
    }vectorsorter;
    std::sort(JetCollection.begin(),JetCollection.end(),vectorsorter);
    //PTISR_PRIME
    PTISR=JetCollection[0].Pt()/1000.;
  }
  
  return PTISR;
}


bool MasterShef::SelStop4BodyTight(bool init){
  if(init){
    m_cuts[m_cutLevel++]="(Stop 0L 4body tight)";
    Info("SelStop0L4BodyTight()","Initialising stop4body tight analysis Selection selector.... ");
    return true;
  }

  if(m_doTruth){
    passCut();
    return true;
  }
  
  // unsigned int nEl=m_signalElectrons->size();
  // unsigned int nBaselineEl=m_baselineElectrons->size();
  // unsigned int nMu=m_signalMuons->size();
  // unsigned int nBaselineMu=m_baselineMuons->size();
  // unsigned int nBaselineLep=nBaselineEl+nBaselineMu;
  // unsigned int nLep=nEl+nMu;
  // unsigned int nB=m_BJets->size();
  // unsigned int nJets=m_signalJets->size();
  // int nBTrackJets=m_signalBTrackJets->size();
  // unsigned int nTrackJets=m_signalTrackJets->size();
  // double pT_ISR=0.0;
  // //define ISR as leading non btagged ISR jet
  // pT_ISR=Calc_4b_PTISR();
  // double pt_1jet=0.0;
  // if(nJets>0)
  //   pt_1jet=m_signalJets->at(0)->pt()/1000.;

  
  
  // double pt_1trackjet=0.0;
  // if(nTrackJets>0)
  //   pt_1trackjet=m_signalTrackJets->at(0)->pt()/1000.;  
  // double pt_1btrackjet=0.;
  // if (nBTrackJets>0){
  //   pt_1btrackjet=m_signalBTrackJets->at(0)->pt()/1000.;
  // }

  //get met variables

  // TVector2 METVEC;
  // METVEC.Set(m_pxmiss/1000.,m_pymiss/1000.);
  // TVector2 trkMET;
  // trkMET.Set(m_trackMET_px/1000,m_trackMET_py/1000);	
  
  //double dphimettrack=fabs(METVEC.DeltaPhi(trkMET));
  
  //  double trackmet=trkMET.Mod();

//   //SRD
//   if (nLep==0 && nBaselineLep==0 && MET>250 && pT_ISR>250.){ 
//   // emacs seems to throw indentation error, not sure why...
//     if(nB==0 && nBTrackJets>0){ // SRD1
//       if (pt_1btrackjet<100){	
// 	passCut();
// 	return true;
//       }
//     }
//     else if(nB==1){ //SRD2      
//       if(pt_1trackjet< 100 && nBTrackJets>0 && pt_1trackjet<100){	
// 	passCut();
// 	return true;
//       }
//     }
//     else if (nB>=2){ // SRD3
//       if(m_BJets->at(0)->pt()/1000. < 200){
// 	passCut();
// 	return true;
//       }
//     }
//   }
// // emacs seems to throw indentation error, not sure why...
// // CRTop
//   if(nLep==1 && nBaselineLep==1){
//   //   
//     if (MET>250 && pT_ISR>250 && trackmet>30 && dphimettrack< TMath::Pi()/3.){
//       TLorentzVector lepton;
//       TVector2 MetVec(m_pxmiss/1000.,m_pymiss/1000.);
//       if(nMu==1)
// 	lepton=m_signalMuons->at(0)->p4();    
//       else if (nEl==1)
// 	lepton=m_signalElectrons->at(0)->p4();
      
//       double mTLepMet=sqrt(2*(MetVec.Mod()*(lepton.Pt()/1000)) - 2*((MetVec.Px()*lepton.Px()/1000) + (MetVec.Py()*lepton.Py()/1000)) );
//       if(mTLepMet<120 && mTLepMet>20 && pT_ISR>250){
// 	if (nB==1){ // CRT2
// 	  // require existence of track jet
	  
// 	  if(nBTrackJets>0 && pt_1trackjet<50 ){
// 	    passCut();
// 	    return true;
// 	  }
// 	}
// 	else if (nB>=2){//CRT3
// 	  double pt_1_b_leadEMTopo=m_BJets->at(0)->pt()/1000.;
// 	  if(pt_1_b_leadEMTopo<175.){
// 	    passCut();
// 	    return true;
// 	  }       
// 	}
//       }
//     }
    
//  }
//   if (nLep==2 && nBaselineLep==2){
//     //CRZ
//     int sumcharge=-9;
//     double mll=0.0;
//     if (nMu==2){
//       sumcharge=m_signalMuons->at(0)->charge()+m_signalMuons->at(1)->charge();
//       mll=(m_signalMuons->at(0)->p4() + m_signalMuons->at(1)->p4()).M();          
//     }
//     else if (nEl==2){
//       sumcharge=m_signalElectrons->at(0)->charge()+m_signalElectrons->at(1)->charge();
//       mll=(m_signalElectrons->at(0)->p4() + m_signalElectrons->at(1)->p4()).M(); 
//     }
//     double METPrime=m_etmiss_prime/1000.;
//     if(mll<101. && mll>81. && sumcharge==0 && METPrime>150. && pT_ISR>150. && MET<70.){
//       passCut();
//       return true;
    

//   }
//  }


 passCut();
 return true;

 // failCut();
 // return false;
}

bool MasterShef :: SelGammaBalance(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="(GammaBalance filtering)";
     Info("SelGammaBalance()","Initialising gammaBalance analysis Selection selector.... ");
     if(!m_usePhotons){
       Error("SelGammaBalance()","Trying to run GammaBalance analysis with no photons!!");
       return false;
     }
     return true;
   }
 
   unsigned int nJ = m_signalJets->size();
  

   unsigned int nphotons = m_signalPhotons->size();
 

   

   if(!(nphotons==1 && nJ==1)){
     failCut();
     return false;
   }
   double dphi = TVector2::Phi_mpi_pi(m_signalPhotons->at(0)->p4().Phi() - m_signalJets->at(0)->p4().Phi());
   
   if(dphi<2.5)
     {
       failCut();
       return false;
     }
   

   passCut();
   return true;
}


bool MasterShef :: SelBosonBalance(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="(BosonBalance filtering)";
     Info("SelBosonBalance()","Initialising gammaBalance analysis Selection selector.... ");
     if(!m_usePhotons){
       Error("SelBosonBalance()","Trying to run GammaBalance analysis with no photons!!");
       return false;
     }
     return true;
   }
 
   unsigned int nJ = m_signalJets->size();
  

   unsigned int nphotons = m_signalPhotons->size();
 
   unsigned int nEl=m_signalElectrons->size();
   unsigned int nMu=m_signalMuons->size();
   
   double mll = 0;
      
   TLorentzVector Z;
   if(nEl==2){
     Z=(m_signalElectrons->at(0)->p4() +  m_signalElectrons->at(1)->p4());
     mll=Z.M()/1000.;
   }
   else if(nMu==2){
     Z=(m_signalMuons->at(0)->p4() +  m_signalMuons->at(1)->p4());
     mll=Z.M()/1000.;
   }
  
   unsigned int nZ = 0;
 
   if((mll>80 && mll<110)){
     nZ=1;
   }
  


   if(nphotons==1){
     if(!(nphotons==1 && nJ==1)){
       failCut();
       return false;
     }
     double dphi = TVector2::Phi_mpi_pi(m_signalPhotons->at(0)->p4().Phi() - m_signalJets->at(0)->p4().Phi());
     
     if(dphi<2.5)
       {
   failCut();
   return false;
       }
   }
   else if(nZ==1){
     if(!(nZ==1 && nJ==1)){
       failCut();
       return false;
     }
     double dphi = TVector2::Phi_mpi_pi(Z.Phi() - m_signalJets->at(0)->p4().Phi());
     
     if(dphi<2.5)
       {
   failCut();
   return false;
       }
   }
   else{
     failCut();
     return false;
   }
   

   passCut();
   return true;
}


bool passDiJetSelection(xAOD::JetContainer* m_jets){

  int nJ = 0;
  for(const auto &jet: *m_jets){
    if(jet->auxdata<char>("smearjet") == true && jet->pt()/1000. > 20 ) nJ++;
  }
  
  if(nJ<2 || nJ>3){
    return false;
  }
   
  //get jets relatively back-to-back 
  double dphi = fabs(TVector2::Phi_mpi_pi(m_jets->at(0)->p4().Phi() - m_jets->at(1)->p4().Phi()));
  if((TMath::Pi() - dphi)>0.25)
    {
      return false;
    }

  if(m_jets->at(0)->pt()/1000. <150){
    return false;
  }
  if(m_jets->at(1)->pt()/1000. <50){
    return false;
  }

   
  //if more than 2 jets, put some requirements on the 3rd jet
  if(nJ>2){
    double e3=m_jets->at(2)->e();
    double e1=m_jets->at(0)->e();
    double e2=m_jets->at(1)->e();
     
    //fail if the energy of the 3rd jet is greater than the 1st and 2nd
    if(e3>e1 || e3>e2){
      return false;
    }
     
    double p1=m_jets->at(0)->pt();
    double p2=m_jets->at(1)->pt();
    double p3=m_jets->at(2)->pt();

    double av= 0.5 * ( p1 + p2);

    if(p3/av > 0.1){
      return false;
    }
     
  }

  return true;
}



bool MasterShef :: SelDiJet(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="DiJet Balance filtering:: HLT_j* triggers fired ";
     m_cuts[m_cutLevel++]="DiJet Balance filtering:: dPhi>2.0 && E(j3)<E(j1),E(j2) and  pT(j3)/<pT> < 0.1";
     //m_cuts[m_cutLevel++]="DiJet Balance filtering:: E(j3)<E(j1),E(j2) and  pT(j3)/<pT> < 0.1";
     Info("SelDiJet()","Initialising dijet analysis Selection selector.... ");
     return true;
   }
 
   if(m_TriggerWeight<1.0){
     //if this condition is met then the events arent triggers on with the single jet triggers
     failCut();
     return false;
   }

   passCut();


   if(!passDiJetSelection(m_signalJets)){
     failCut();
     return false;
   }


   passCut();
   return true;
}




bool MasterShef :: SelPseudoEventsDiJet(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="perform JetSmearing";
     m_cuts[m_cutLevel++]="any smear events pass dijet analysis ";
     Info("SelSbottom()","Initialising seed event Selection selector.... ");
     return true;
   }

  
   //fail if we weren't suppose to be doing jetsmearing
   if(!m_doJetSmearing){
     failCut();
     return false;
   }

   //fail if not at least one signal jet
   if(m_signalJets->size()==0){
     failCut();
     return false;
   }

   
   //check nsmears
   this->jetSmearing();
   passCut();


   
   int ngood_smears=0;
   
   //loop over the vector of jet containers
   for(unsigned int i=0; i < m_smrJets.size(); i++)
     {
       
       //get the jet collection for the smear
       xAOD::JetContainer* smrJets = m_smrJets[i];

       //check if any of these smeared jets pass the dijet selection
       if(passDiJetSelection(smrJets)) ngood_smears++;
      
     }//end of loop over all smeared containers
   
   if(ngood_smears==0){
     failCut();
     return false;
   }

   passCut();
	
   return true;
   
}






//On Z selection
// 2 leptons 106>mll>86
bool MasterShef :: SelOnZ(bool init) {
  if (init) {
    m_cuts[m_cutLevel++]="106>mll>86";
    m_cuts[m_cutLevel++]="OS leptons";
    Info("SelSbottom()","Initialising On Z selector.... ");
    return true;
  }
  unsigned int nEl=m_signalElectrons->size();
  unsigned int nMu=m_signalMuons->size();
  

 
  double mll = 0;
  int charge1 = 0;
  int charge2 = 0;
  
  if(nEl==2){
    mll = (m_signalElectrons->at(0)->p4() +  m_signalElectrons->at(1)->p4()).M()/1000.;
    charge1 = m_signalElectrons->at(0)->charge();
    charge2 = m_signalElectrons->at(1)->charge();
  
  }
  else if(nMu==2){
    mll = (m_signalMuons->at(0)->p4() +  m_signalMuons->at(1)->p4()).M()/1000.;
    charge1 = m_signalMuons->at(0)->charge();
    charge2 = m_signalMuons->at(1)->charge();
  
  }
  

  if( !(mll>86 && mll<106)){
    failCut();
    return false;
  }

  passCut();

  if( charge1==charge2){
    failCut();
    return false;
  }
  
  
  
  
  passCut();
  return true;
  
  

}





// Missing ET selection 
// parameters: 
// m_SelMct_PtMn : Minimum mct required 
// m_SelMct_PtMx : Maximum mct required 
bool MasterShef :: SelMct(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="mct";
     Info("SelMct()","Initialising mct selector.... ");
     Info("SelMct()","  Min mct =%f",m_SelMct_Mn);
     Info("SelMct()","  Max mct =%f",m_SelMct_Mx);
     return true;
   }

   double mctjj=0;
   if(m_signalJets->size()>=2)
     {
       mctjj=mcttool->mct(m_signalJets->at(0)->p4(),m_signalJets->at(1)->p4());
     }

   if (mctjj < m_SelMct_Mn || mctjj > m_SelMct_Mx) {
     failCut();
     return false;
   }
   passCut();
   return true;
}


// Missing ET selection 
// parameters: 
// m_SelMET_PtMn : Minimum Etmiss required 
// m_SelMET_PtMx : Maximum Etmiss required 
bool MasterShef :: SelMET(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="MET";
     Info("SelMET()","Initialising MissingET selector.... ");
     Info("SelMET()","  Min MET Pt=%f",m_SelMET_PtMn);
     Info("SelMET()","  Max MET Pt=%f",m_SelMET_PtMx);
     return true;
   }
   
   
   if (m_etmiss_prime < m_SelMET_PtMn || m_etmiss_prime > m_SelMET_PtMx) {
     failCut();
     return false;
   }
   passCut();
   return true; 
}


bool MasterShef :: SelTruthMET(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="True MET";
     Info("SelTruthMET()","Initialising True MissingET selector.... ");
     return true;
   }
   const xAOD::MissingETContainer* met_Truth = 0;
   CHECK(event->retrieve(met_Truth,"MET_Truth"));
   double met_NonInt =(*met_Truth)["NonInt"]->met()/1000.;
      
   if (!(met_NonInt < 20 && (m_etmiss/1000.)>200 )){
     failCut();
     return false;
   }

   const xAOD::EventInfo* eventInfo = 0;
   CHECK(event->retrieve( eventInfo, "EventInfo") );
   unsigned int en = eventInfo->eventNumber();
   m_eventDump.push_back(en);
   eventDump(false);


   passCut();
   return true; 
}



bool MasterShef :: SelJetMET(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="JetMET";
     Info("SelJetMET()","Initialising JETMissingET selector.... ");
     Info("SelJetMET()","  Min MET Pt=%f",m_SelMET_PtMn);
     Info("SelJetMET()","  Max MET Pt=%f",m_SelMET_PtMx);
     return true;
   }
   
   double ptj1 = 0.;
   for(const auto &jet: *m_signalJets){
     ptj1 += jet->p4().Pt();
     break;
   }


   TVector2 etmiss2D (m_pxmiss,m_pymiss);
   TVector2 etmissfix2D = etmiss2D;
   
   
   unsigned int nEl=m_signalElectrons->size();
   unsigned int nMu=m_signalMuons->size();
 

   unsigned int nbaselineLep=m_baselineElectrons->size() + m_baselineMuons->size();
   //m_signalMuons->at(i)->pt()
   if(nEl==2 && nMu==0 && nbaselineLep==2)
     {
       etmissfix2D.Set(etmiss2D.Px()+m_signalElectrons->at(0)->p4().Px()+m_signalElectrons->at(1)->p4().Px(),etmiss2D.Py()+m_signalElectrons->at(0)->p4().Py()+m_signalElectrons->at(1)->p4().Py());
     }
   else if(nEl==0 && nMu==2 && nbaselineLep==2)
     {
       etmissfix2D.Set(etmiss2D.Px()+m_signalMuons->at(0)->p4().Px()+m_signalMuons->at(1)->p4().Px(),etmiss2D.Py()+m_signalMuons->at(0)->p4().Py()+m_signalMuons->at(1)->p4().Py());
     }

   double m_etmiss_corr = etmiss2D.Mod();//etmissfix2D.Mod();
   


   if (m_etmiss_corr < m_SelMET_PtMn || m_etmiss_corr > m_SelMET_PtMx) {
     
     if(ptj1 < 400*1000.){
       failCut();
       return false;
     }
   }
   passCut();
   return true; 
}


// HT selection 
// parameters: 
// m_SelHT_PtMn : Minimum HT required 
// m_SelHT_PtMx : Maximum HT required 
bool MasterShef :: SelHT(bool init) {
   if (init) {
     m_cuts[m_cutLevel++]="HT";
     Info("SelHT()","Initialising HT selector.... ");
     Info("SelHT()","  Min HT Pt=%f",m_SelHT_Mn);
     Info("SelHT()","  Max HT Pt=%f",m_SelHT_Mx);
     return true;
   }
   
   double HT = 0.;
   for(const auto &jet: *m_signalJets){
     HT += jet->p4().Pt();
   }
 
   if (HT<m_SelHT_Mn || HT>m_SelHT_Mx) {
      failCut();
      return false;
   }
   
   passCut();
   return true; 
}



// Mt selection 
// parameters: 
// m_SelMt_Mn : Minimum Mt 
// m_SelMt_Mx : Maximum Mt 
bool MasterShef :: SelMt(bool init) 
{
   if (init) {
     m_cuts[m_cutLevel++]="Mt range";
     Info("SelMt()","Initialising MissingET selector.... ");
     Info("SelMt()","  Min Mt %f",m_SelMt_Mn);
     Info("SelMt()","  Max Mt %f",m_SelMt_Mx);
     return true;
   }
   double mt=calcMt();
   if (mt<m_SelMt_Mn || mt>m_SelMt_Mx) {
      failCut();
      return false;
   }
   passCut();
   return true; 
}



bool MasterShef :: SelSBTriggers(bool init)
{
   if (init) {
     Info("SelSBTriggerset()","Initialising single jet trigger selection.... ");
     m_cuts[m_cutLevel++]="Leading b-jet";
     m_cuts[m_cutLevel++]="pass b-jet triggers";
     return true;
   }
   if(m_signalJets->size()==0){
     failCut();
     return false;
   }
   if (! m_objTool->IsBJet(*(m_signalJets->at(0))) ){
     failCut();
     return false;
   }

   passCut();


   std::string whichtrig="";
   bool passTrig=false;
   for (unsigned int ii =0; ii<m_triggerList.size(); ii++){
     if( m_triggerList.at(ii).find("btight")!=std::string::npos || m_triggerList.at(ii).find("bmedium")!=std::string::npos || m_triggerList.at(ii).find("bloose")!=std::string::npos){
       if (m_objTool->IsTrigPassed(m_triggerList.at(ii)))
   {
     passTrig=true;
     whichtrig=m_triggerList.at(ii);
     break;
   }
       
     }
   }
   
   if (!passTrig){
     failCut();
     return false;
   }

   passCut();



   return true;
}




bool MasterShef :: SelSJTriggers(bool init)
{
   if (init) {
     Info("SelSJTriggerset()","Initialising single jet trigger selection.... ");
     m_cuts[m_cutLevel++]="Skim of single jet triggers";
     return true;
   }


   std::string whichtrig="";
   bool passTrig=false;
   for (unsigned int ii =0; ii<m_triggerList.size(); ii++){
     if( m_triggerList.at(ii).find("HLT_j")!=std::string::npos){
       if (m_objTool->IsTrigPassed(m_triggerList.at(ii)))
   {
     passTrig=true;
     whichtrig=m_triggerList.at(ii);
     break;
   }
       
     }
   }
   
   if (!passTrig){
     failCut();
     return false;
   }

   
   passCut();
   return true;
}

// jet selection
// parameters:
// m_SelJet_nJMn: Min num of jets
// m_SelJet_nJMx: Max num  jets
// m_SelJet_ptMn: Min Pt of jet
// m_SelJet_ptMx: Max Pt of jet


bool MasterShef :: SelJet(bool init)
{
   if (init) {
     Info("SelJet()","Initialising Jet selector.... ");
   
     if(m_SelJet_ptMn.size() != m_SelJet_ptMx.size() )
     {
       Error("SelJet()","misconfiguration of seljet vectors m_SelJet_ptMn.size() != m_SelJet_ptMx.size()");
       return false;
     }

     m_cuts[m_cutLevel++]=std::to_string(m_SelJet_nJMx)+">= NJets >= "+std::to_string(m_SelJet_nJMn);
     Info("SelJet()","  NJet Min %i and Max %i",m_SelJet_nJMn,m_SelJet_nJMx);
     
     for (unsigned int ii=0; ii<m_SelJet_ptMn.size(); ii++) {
       m_cuts[m_cutLevel++]="jet Pt sel "+std::to_string(ii);
       Info("SelJet()","  Jet %i Min Pt %f -- Max Pt %f",ii,m_SelJet_ptMn.at(ii),m_SelJet_ptMx.at(ii));
     }
     //m_cuts[m_cutLevel++]="jet1 Pt>50";
     //m_cuts[m_cutLevel++]="jet2 Pt>50";
     //m_cuts[m_cutLevel++]="jet4 Pt<50";

     return true;
   }

 
   //Number of jets:
   unsigned int nj_good=m_signalJets->size();
   

   if (nj_good < m_SelJet_nJMn || nj_good > m_SelJet_nJMx)
     {
       failCut();
       return false;
     }
   passCut();
   
   unsigned int npc=m_SelJet_ptMn.size();
   for (unsigned int ii=0; ii<npc; ii++) {
     if (ii>=nj_good) {
       passCut();
     } 
     else { 
       float jetpt=m_signalJets->at(ii)->pt();
       if (jetpt < m_SelJet_ptMn.at(ii) || jetpt > m_SelJet_ptMx.at(ii) ) {
   failCut();
   return false;
       } 
       passCut();
       
     }
   }

   return true;
}

bool MasterShef :: SelTauJet(bool init)
{
   if (init) {
     Info("SelTauJet()","Initialising TauJet selector.... ");
   
     if(m_SelTauJet_ptMn.size() != m_SelTauJet_ptMx.size() )
     {
       Error("SelTauJet()","misconfiguration of seljet vectors m_SelTauJet_ptMn.size() != m_SelTauJet_ptMx.size()");
       return false;
     }

     m_cuts[m_cutLevel++]=" NTauJets(BeforeOR) >= "+std::to_string(m_SelTauJet_nJMn);
     m_cuts[m_cutLevel++]=std::to_string(m_SelTauJet_nJMx)+">= NTauJets >= "+std::to_string(m_SelTauJet_nJMn);
     Info("SelTauJet()","  NTauJet Min %i and Max %i",m_SelTauJet_nJMn,m_SelTauJet_nJMx);
     
     for (unsigned int ii=0; ii<m_SelTauJet_ptMn.size(); ii++) {
       m_cuts[m_cutLevel++]="jet Pt sel "+std::to_string(ii);
       Info("SelTauJet()","  TauJet %i Min Pt %f -- Max Pt %f",ii,m_SelTauJet_ptMn.at(ii),m_SelTauJet_ptMx.at(ii));
     }
     //m_cuts[m_cutLevel++]="jet1 Pt>50";
     //m_cuts[m_cutLevel++]="jet2 Pt>50";
     //m_cuts[m_cutLevel++]="jet4 Pt<50";

     return true;
   }

 

   //Number of jets:
   unsigned int nj_good_beforeOR=m_signalTausBeforeOR->size();
   if (nj_good_beforeOR < m_SelTauJet_nJMn)
     {
       failCut();
       return false;
     }
   passCut();


   //Number of jets:
   unsigned int nj_good=m_signalTaus->size();
   if (nj_good < m_SelTauJet_nJMn || nj_good > m_SelTauJet_nJMx)
     {
       failCut();
       return false;
     }
   passCut();
   
   unsigned int npc=m_SelTauJet_ptMn.size();
   for (unsigned int ii=0; ii<npc; ii++) {
     if (ii>=nj_good) {
       passCut();
     } 
     else { 
       float jetpt=m_signalTaus->at(ii)->pt();
       if (jetpt < m_SelTauJet_ptMn.at(ii) || jetpt > m_SelTauJet_ptMx.at(ii) ) {
   failCut();
   return false;
       } 
       passCut();
       
     }
   }

   return true;
}

bool MasterShef :: SelLeadB(bool init)
{
   if (init) {
     Info("SelLeadB()","Initialising NBJet selector.... ");
     m_cuts[m_cutLevel++]="1st leading bjet";
     m_cuts[m_cutLevel++]="2nd leading bjet";
     return true;
   }
   if(m_BJets->size()<2){
     failCut();
     return false;
   }
   
   if (fabs((m_BJets->at(0)->p4().Pt()-m_signalJets->at(0)->p4().Pt()))>0.0001){
     failCut();
     return false;
   }
   passCut();
   if (fabs(m_BJets->at(1)->p4().Pt()-m_signalJets->at(1)->p4().Pt())>0.0001){
     failCut();
     return false;
   }
    passCut();
    return true;
}


// n bjet selection
// parameters:   
// m_SelNBJet_nJMn: Min num of jets
// m_SelNBJet_nJMx: Max num  jets
bool MasterShef :: SelNBJet(bool init)
{
   if (init) {
     Info("SelNBJet()","Initialising NBJet selector.... ");
     m_cuts[m_cutLevel++]="NBJets>="+std::to_string(m_SelNBJet_nJMn);
     //m_cuts[m_cutLevel++]="NBJets=="+std::to_string(m_SelNBJet_nJMx);
     m_cuts[m_cutLevel++]="NBJets<="+std::to_string(m_SelNBJet_nJMx);
     //for (unsigned int ii=0; ii<m_SelNBJet_nJMn ;  ii++) 
     //   m_cuts[m_cutLevel++]="NBJets>"+std::to_string(ii);
     Info("SelNBJet()","  NBJet Min %i and Max %i",m_SelNBJet_nJMn,m_SelNBJet_nJMx);            
     return true;
   }
   unsigned int nB=0;
   nB = m_BJets->size();
  


   if(nB<m_SelNBJet_nJMn)
     {
      
       failCut();
       return false;
     }
   passCut();

   if(nB>m_SelNBJet_nJMx) 
     {
       failCut();
       return false;
     }

   passCut();


  return true;
}
bool MasterShef :: SelNBTruthJet(bool init)
{
   if (init) {
     Info("SelNBJet()","Initialising NBJet selector.... ");
     m_cuts[m_cutLevel++]="NBTruthJets>="+std::to_string(m_SelNBTruthJet_nJMn);
     m_cuts[m_cutLevel++]="NBTruthJets<="+std::to_string(m_SelNBTruthJet_nJMx);
     Info("SelNBTruthJet()","  NBJet Min %i and Max %i",m_SelNBTruthJet_nJMn,m_SelNBTruthJet_nJMx);            
     return true;
   }
   if(!m_isMC){
     passCut();
     passCut();
     return true;
   }
       
   unsigned int nB=0;
   for (unsigned int i=0;i<m_signalJets->size();i++) {
     int flavour = -1;
     m_signalJets->at(i)->getAttribute("ConeTruthLabelID",flavour);
     if(flavour==5)
       nB++;
   }
    
  
   if(nB<m_SelNBTruthJet_nJMn)
     {
       failCut();
       return false;
     }

   
   passCut();
   if(nB>m_SelNBTruthJet_nJMx) {
     failCut();
     return false;
   }
   passCut();


    return true;
}



// 
// Calculate Mt using the leading signal leptons and MET 
// Returns -1 if no signal leptons are present

double MasterShef::calcMt() {
   int NLep=m_signalElectrons->size()+m_signalMuons->size();
   if (NLep==0)
       return -1.0;

   double elPt=-1.0;
   double muPt=-1.0;

   if (m_signalElectrons->size()>0)
      elPt=m_signalElectrons->at(0)->pt();

   if (m_signalMuons->size()>0)
      muPt=m_signalMuons->at(0)->pt();

   TLorentzVector pLep;
   if (elPt>muPt) {
     pLep.SetX(m_signalElectrons->at(0)->p4().X() );
     pLep.SetY(m_signalElectrons->at(0)->p4().Y() );
     pLep.SetE(m_signalElectrons->at(0)->pt() );
   } else {
     pLep.SetX(m_signalMuons->at(0)->p4().X() );
     pLep.SetY(m_signalMuons->at(0)->p4().Y() );
     pLep.SetE(m_signalMuons->at(0)->pt() );
   }
   
   TLorentzVector pMET;
   pMET.SetX(m_pxmiss);
   pMET.SetY(m_pymiss);
   pMET.SetE(m_etmiss);

  return (pLep+pMET).M();

}



void MasterShef::passCut() {
  // If the cut is passed we increment the number
  // and fill the histogram
  m_cutFlow.at(m_systNum)->Fill(m_cutLevel);
  m_cutFlowWgt.at(m_systNum)->Fill(m_cutLevel,m_AnalysisWeight);
  double weightFull=1.0;
  if(!m_doTruth )weightFull=m_pileupweight*m_AnalysisWeight;//*m_btagweight*m_TriggerSF;
  m_cutFlowPUWgt.at(m_systNum)->Fill(m_cutLevel,weightFull);
    

  //now do the filling of trees
  bool doFill = false;
    //if is the nominal variation, check if we tell the code to fill (dump the event) here
  if( m_systNum == 0 ){
    doFill=m_cutLevel == m_ntupLevel;
  }
  else{ //the systematics have a different cut level you can supply
    doFill=m_cutLevel == m_ntupLevel_sys;
  }

  if(doFill){
    this->fillTree(m_systNum);
  }
  
   
  // Check if we need to dump
  
  /*
  if (m_cutDumpLvl == int(m_cutLevel+0.1) ) {
    // Get xAOD event
    xAOD::TEvent* event = wk()->xaodEvent();
    const xAOD::EventInfo* eventInfo = 0;
    if (event->retrieve( eventInfo, "EventInfo")  != StatusCode::SUCCESS) {
       std::cout << "This should not happen! " << std::endl;
       exit (-1);
    }
    int en = eventInfo->eventNumber();
    m_cutDumpStr << en << std::endl;
    }*/
  // Do lots of other stuff
  m_cutLevel++; // Finally increment the cut level
  return; 
}


void MasterShef::failCut() {
  // If the cut is not passed we increment the number
  // Do lots of other stuff
  m_cutLevel++; // Finally increment the cut level
  return; 
}

