#include <MasterShef/MasterShef.h>
//#include "CPAnalysisExamples/errorcheck.h"
static const char* APP_NAME = "MasterShef";




const xAOD::TruthParticle* MasterShef::getLastTruthParticle(const xAOD::TruthParticle* particle){ 
  auto pdgId = particle->pdgId();

  if (particle->nChildren() >= 1 && particle->child(0) &&  particle->child(0)->pdgId() == pdgId) //allow for t->t gluon
    return getLastTruthParticle(particle->child(0));

  return particle;
}


bool dRsort(const xAOD::IParticle* truth, const xAOD::IParticle* i, const xAOD::IParticle* j )
{
  double dRi = i->p4().DeltaR(truth->p4());
  double dRj = j->p4().DeltaR(truth->p4());
  return (dRi < dRj);
}


// double MasterShef :: z0sinTheta(const xAOD::TruthParticle* j ){
//   const xAOD::Vertex* pv = m_objTool->GetPrimVtx();
//   double primvertex_z = pv ? pv->z() : 0;
//   const xAOD::TrackParticle* track =  j->primaryTrackParticle();
  
//   return (track->z0() + track->vz() - primvertex_z) * TMath::Sin(j->p4().Theta());
  
// }



bool  MasterShef :: getAncestors(const xAOD::TruthParticle* p,std::vector< const xAOD::TruthParticle* >& ancestors) {
  ancestors.clear();
  if (!(p->hasProdVtx())) return 0;
  const xAOD::TruthVertex* pvtx = p->prodVtx();
  if (!pvtx) return 0;
  if (pvtx->nIncomingParticles() == 0) return 0;
  const std::vector< ElementLink< xAOD::TruthParticleContainer > >& inPart = pvtx->incomingParticleLinks();
  for (int k=0; k<(int)inPart.size(); ++k) {
    if (!(inPart[k]).isValid()) continue;
    const xAOD::TruthParticle* kid = *(inPart[k]);
    ancestors.push_back(kid);
  }
  
  int nstart = 0;
  int nstop = ancestors.size();
  
  while (nstop > nstart) {
    for (int i=nstart; i<nstop; ++i) {
      const xAOD::TruthParticle* pp = ancestors[i];
      if (!(pp->hasProdVtx())) continue;
      const xAOD::TruthVertex* vpp = pp->prodVtx();
      if (!vpp) continue;
      if (vpp->nIncomingParticles() == 0) continue;
      const std::vector< ElementLink< xAOD::TruthParticleContainer > >&inPart2 = vpp->incomingParticleLinks();
      for (int k=0; k<(int)inPart2.size(); ++k) {
	if (!(inPart2[k]).isValid()) continue;
	const xAOD::TruthParticle* kpp = *(inPart2[k]);
	bool isIn = false;
	for (int kk=0; kk<(int)ancestors.size(); ++kk) {
	  if (kpp==ancestors[kk]) isIn = true;
	}
	if (!isIn) ancestors.push_back(kpp);
      }
    }
    nstart = nstop;
    nstop = ancestors.size();
  }
  
  return nstop;
}



bool MasterShef :: isFromTopW(const xAOD::TruthParticle* particle)
{
  auto result = m_MCtruthClassifier->particleTruthClassifier(particle);
  //Second in the pair is particle origin
  auto& origin = result.second;
  if (origin == MCTruthPartClassifier::WBoson || origin == MCTruthPartClassifier::top)
    {
      return true;
    }
  else if (origin == MCTruthPartClassifier::NonDefined)
    {
      //MCTruthPartClassifier::ParticleDef particleDef;
      //Failed to classify. Most likely the link to the top is broken. Give it a second try by look at the ancestors
      auto& type = result.first;
      std::vector<const xAOD::TruthParticle*> ancestors;
      
      getAncestors(particle, ancestors);
      
      if (type == MCTruthPartClassifier::Neutrino)
	{
	  if (ancestors[0]->isW())
	    {
	      return true;
	    }
	  else if (ancestors[0]->isTau())
	    {
	      auto tauresult = m_MCtruthClassifier->particleTruthClassifier(ancestors[0]);
	      if (tauresult.second == MCTruthPartClassifier::WBoson)
		{
		  return true;
		}
	      else
		{
		  return false;
		}
	    }
	}
      return false;
    }
  else
    {
      return false;
    }
}






bool MasterShef::getTruthHFHadrons(){

  ATH_MSG_VERBOSE("inside of getTruthBHadrons");
  const xAOD::TruthParticleContainer* truthParticles = nullptr;
  ATH_CHECK(evtStore()->retrieve(truthParticles, "TruthParticles"));

  auto bHadronTruthParticles = std::make_unique<xAOD::TruthParticleContainer>();
  auto bHadronTruthParticlesAux = std::make_unique<xAOD::AuxContainerBase>();
  bHadronTruthParticles->setStore(bHadronTruthParticlesAux.get());//connect auxilary store to main container

  auto cHadronTruthParticles = std::make_unique<xAOD::TruthParticleContainer>();
  auto cHadronTruthParticlesAux = std::make_unique<xAOD::AuxContainerBase>();
  cHadronTruthParticles->setStore(cHadronTruthParticlesAux.get());//connect auxilary store to main container

  //Code logic from G. Ungaro
  for (auto thisTruthParticle : *truthParticles){

    if (!thisTruthParticle){
      ATH_MSG_ERROR("Invalid pointer to truth particle in getTruthBHadrons");
      continue;
    }

    if (thisTruthParticle->hasDecayVtx() && thisTruthParticle->isBottomHadron()){
      bool hasSameChild = false;
      for(unsigned int i = 0; i < thisTruthParticle->decayVtx()->nOutgoingParticles(); ++i){
	if (thisTruthParticle->decayVtx()->outgoingParticle(i) && thisTruthParticle->decayVtx()->outgoingParticle(i)->isBottomHadron()){
	  hasSameChild=true;
	  break;
	}
      }
      if(!hasSameChild){
	 xAOD::TruthParticle *truthBHadron = new xAOD::TruthParticle();
	 bHadronTruthParticles->push_back(truthBHadron);//truthBHadron acquires the bHadronTruthParticles auxstore
	 *truthBHadron = *thisTruthParticle;//copies auxdata from one auxstore to the other
      }
    }
    if (thisTruthParticle->hasDecayVtx() && thisTruthParticle->isCharmHadron()){
      bool hasSameChild = false;
      for(unsigned int i = 0; i < thisTruthParticle->decayVtx()->nOutgoingParticles(); ++i){
	if (thisTruthParticle->decayVtx()->outgoingParticle(i) && thisTruthParticle->decayVtx()->outgoingParticle(i)->isCharmHadron()){
	  hasSameChild=true;
	  break;
	}
      }
      if(!hasSameChild){
	 xAOD::TruthParticle *truthCHadron = new xAOD::TruthParticle();
	 cHadronTruthParticles->push_back(truthCHadron);//truthBHadron acquires the bHadronTruthParticles auxstore
	 *truthCHadron = *thisTruthParticle;//copies auxdata from one auxstore to the other
      }
    }

  }

  //record container and aux container into store, whilst releasing the memory from the unique_ptrs.
  CHECK(store->record(bHadronTruthParticles.release(),"TruthBHadrons"+m_sysName));
  CHECK(store->record(bHadronTruthParticlesAux.release(),"TruthBHadrons"+m_sysName+"Aux."));
  
  CHECK(store->record(cHadronTruthParticles.release(),"TruthCHadrons"+m_sysName));
  CHECK(store->record(cHadronTruthParticlesAux.release(),"TruthCHadrons"+m_sysName+"Aux."));
  
  return true;
}

bool MasterShef::getTruthLeptonsInRecoNtuples(bool fillTruthLeptonsInReco,double ele_pT_thresh, double mu_pT_thresh){
  //does what it says on the tin: Gets truth leptons with a pT threshold cut
  //threshold truth pT values to skim container by

  if(!m_isMC){
    //if data, skip obviously
    return true;
  }
  
  if(!fillTruthLeptonsInReco){
    ATH_MSG_VERBOSE("Not filling truth lepton container");
    return true;      
  }

  

  //Get the variables we want from the container
 
  //init the containers to be deep copied into
  auto CopyTruthElectronContainer=std::make_unique<xAOD::TruthParticleContainer>();
  auto CopyTruthElectronContainerAux=std::make_unique<xAOD::AuxContainerBase>();
  CopyTruthElectronContainer->setStore(CopyTruthElectronContainerAux.get());


  auto CopyTruthMuonContainer=std::make_unique<xAOD::TruthParticleContainer>();
  auto CopyTruthMuonContainerAux=std::make_unique<xAOD::AuxContainerBase>();
  CopyTruthMuonContainer->setStore(CopyTruthMuonContainerAux.get());
  
  //Retrieve from objtool
  const xAOD::TruthParticleContainer* TruthElectrons = nullptr;  
  
  CHECK(evtStore()->retrieve(TruthElectrons,"TruthElectrons"));


  // DEEP COPY ROUTINE FOR TRUTH ELECTRONS
  
  for (auto TruthEl : *TruthElectrons){
    //check that what we pick up actually is an electron, if not skip
    if(!(TruthEl->isElectron() && TruthEl->status()==1))
      continue;
    double pt= TruthEl->pt()/1000.;
    //double eta=TruthEl->eta();
    if(pt< ele_pT_thresh  || fabs(TruthEl->pdgId()) != 11 ) //or fabs(eta)<ele_eta_thresh)
      continue;   
  
    xAOD::TruthParticle* CopyEl=new xAOD::TruthParticle();
    CopyTruthElectronContainer->push_back(CopyEl);
    *CopyEl=*TruthEl; // connect auxstores
  
  }
  // HAND OFF TO TEVENTSTORE FOR DELETION/PRESERVATION  
  CHECK(evtStore()->record(CopyTruthElectronContainer.release(),"CustomTruthElectrons"+m_sysName));
  CHECK(evtStore()->record(CopyTruthElectronContainerAux.release(),"CustomTruthElectrons"+m_sysName+"Aux"));
 
  // Now do the same thing as above, but for muons instead 
  //Retrieve from objtool
  const xAOD::TruthParticleContainer* TruthMuons = nullptr;  
  CHECK(evtStore()->retrieve(TruthMuons,"TruthMuons"));

  //DEEP COPY FOR MUONS
  for (auto TruthMu : *TruthMuons){
    //check that what we pick up actually is a muon, if not skip
    if(!(TruthMu->isMuon() && fabs(TruthMu->pdgId()) == 13  &&  TruthMu->status()==1))
      continue;
    double pt= TruthMu->pt()/1000.;
    //double eta=TruthMu->eta();
    if(pt< mu_pT_thresh)// or fabs(eta)<mu_eta_thresh)
      continue;   
   
    xAOD::TruthParticle* CopyMu=new xAOD::TruthParticle();
 
    CopyTruthMuonContainer->push_back(CopyMu);
    *CopyMu=*TruthMu; // connect auxstores
      
  }
  CHECK(evtStore()->record(CopyTruthMuonContainer.release(),"FinalTruthMuons"+m_sysName));
  CHECK(evtStore()->record(CopyTruthMuonContainerAux.release(),"FinalTruthMuons"+m_sysName+"Aux"));

  return true;





}


int MasterShef::LeptoQuarkDecayInfo(){
  int nTruthChLeptsFromLQ=0;
  xAOD::TEvent *m_event = wk()->xaodEvent();
  const xAOD::TruthParticleContainer* m_truthParts;
   if (m_event->retrieve(m_truthParts, "TruthParticles").isSuccess()) {
    if ( m_truthParts->size()>0) {
      for (unsigned int tI=0; tI< m_truthParts->size(); ++tI) {
	const xAOD::TruthParticle* tPart = (*m_truthParts)[tI];
	// Select truth charged leptons from container
	int TruthPDGID=tPart->absPdgId();
	if (TruthPDGID==11 || TruthPDGID==13 || TruthPDGID==15) {
	  for (unsigned int parentIndex=0; parentIndex<tPart->nParents(); parentIndex++) {
	    if (tPart->parent(parentIndex)->absPdgId()==42 || tPart->parent(parentIndex)->absPdgId()==43)
	      nTruthChLeptsFromLQ++;
	  }
	}
      } // End loop over truth particle container
    }
  }
  return nTruthChLeptsFromLQ;
}


std::string MasterShef :: wbosonChildren(const xAOD::TruthParticle* top){
  const xAOD::TruthParticle* wboson = top->child(0)->absPdgId() == 24 ? top->child(0) : top->child(1);
  std::string output = "";
  int count = 0;
  while( wboson->nChildren() == 1 && count <= 10) {
    wboson = wboson->child(0);
    ++count;
  }

  while( (wboson->nChildren() == 2 && count <= 10 ) && (wboson->child(0)->absPdgId() == 22 || wboson->child(0)->absPdgId() == 24 ) ){
    wboson = wboson->child(0)->absPdgId() == 24 ? wboson->child(0) : wboson->child(1);
    ++count;
	
  }
  for(unsigned int i = 0; i < wboson->nChildren() ; ++i){
    //std::cout << " W-boson decay (" << i << ") " << wboson->child(i)->absPdgId() << std::endl;
    switch(wboson->child(i)->absPdgId()){
    case 11:
      output+="el";
      break;
    case 13:
      output+="mu";
      break;
    case 15:
      output+="tau";
      break;
    case 12:
      output+="nuel";
	break;
    case 14:
      output+="numu";
      break;
    case 16:
      output+="nutau";
      break;
    default:
      output+="";
      break;
    }
    if(wboson->child(i)->absPdgId() < 6)
      output+="j";
  }
  if( output == "nuelel" ) output = "elnuel";
  if( output == "numumu" ) output = "munumu";
  if( output == "nutautau" ) output = "taunutau";
  
  return output;
}

int MasterShef :: ttbarClassification(){
  // Function to determine if ther e

  xAOD::TEvent *m_event = wk()->xaodEvent();
  const xAOD::TruthParticleContainer* m_truthParts;
  if (m_event->retrieve(m_truthParts, "TruthParticles").isFailure()){
      ATH_MSG_WARNING("TruthParticles container is missing in this sample.");
      return -1;
  }
  if ( m_truthParts->size() == 0){
    ATH_MSG_WARNING("TruthParticles container is empty in this sample.");
    return -1;
  }

  const xAOD::TruthParticle* top = nullptr;
  const xAOD::TruthParticle* antitop = nullptr;
  for (unsigned int tI=0; tI< m_truthParts->size(); ++tI) {
    const xAOD::TruthParticle* tPart = (*m_truthParts)[tI];
    if (!tPart)
      continue;
    if( tPart->pdgId() == 6 && tPart->nChildren() == 2)
      top = tPart;
    else if( tPart->pdgId() == -6  && tPart->nChildren() == 2)
      antitop = tPart;
  }
  
  // Get the top decays
  std::string top_decay;
  std::string antitop_decay;
  if ( top ) top_decay = wbosonChildren(top);
  if ( antitop ) antitop_decay = wbosonChildren(antitop);

  //std::cout << "Top decay : " << top_decay << " | AntiTop decay : "<< antitop_decay << std::endl;

  //Dilepton decays
  if ( top_decay == "elnuel" && antitop_decay == "elnuel" ) return 0;
  if ( ( top_decay == "munumu" && antitop_decay == "elnuel" ) || ( antitop_decay == "munumu" && top_decay == "elnuel" )) return 1;
  if ( ( top_decay == "taunutau" && antitop_decay == "elnuel" ) || ( antitop_decay == "taunutau" && top_decay == "elnuel" )) return 2;
  if ( top_decay == "munumu" && antitop_decay == "munumu" ) return 3;
  if ( ( top_decay == "munumu" && antitop_decay == "taunutau" ) || ( antitop_decay == "munumu" && top_decay == "taunutau" )) return 4;
  if ( top_decay == "taunutau" && antitop_decay == "taunutau" ) return 5;
  //Single-lepton decays
  if ( (top_decay == "elnuel" && antitop_decay == "jj") || (top_decay == "jj" && antitop_decay == "elnuel") ) return 6;
  if ( (top_decay == "munumu" && antitop_decay == "jj") || (top_decay == "jj" && antitop_decay == "munumu") ) return 7;
  if ( (top_decay == "taunutau" && antitop_decay == "jj") || (top_decay == "jj" && antitop_decay == "taunutau") ) return 8;
  // All-hadronic decays 
  if ( top_decay == "jj" && antitop_decay == "jj" ) return 9;

  return -1;
}
std::string MasterShef :: ttbarISRClassification(){

  // Function to classify ttbar as ttbar+b, ttbar+c, ttbar+light for background composition.
  // Gluons can be radiated as ISR or FSR, despite what the function name says.

  std::string output = "" ; 
  xAOD::TEvent *m_event = wk()->xaodEvent();
  const xAOD::TruthParticleContainer* m_truthParts;
  if (m_event->retrieve(m_truthParts, "TruthParticles").isFailure()){
    ATH_MSG_WARNING("TruthParticles container is missing in this sample.");
    return "";
  }
  if ( m_truthParts->size() == 0){
    ATH_MSG_WARNING("TruthParticles container is empty in this sample.");
    return "";
  }
  
  // Select only gluons, since they are the only radiation that can appear from the ttbar quarks

  xAOD::TruthParticleContainer* gluons = new xAOD::TruthParticleContainer();
  auto gluonsAux = new xAOD::AuxContainerBase();
  gluons->setStore(gluonsAux); //< Connect the two
  for (unsigned int tI=0; tI< m_truthParts->size(); ++tI) {
    xAOD::TruthParticle* tPart = new xAOD::TruthParticle();
    if (!m_truthParts->at(tI) || m_truthParts->at(tI) == nullptr )
      continue;
    if ( m_truthParts->at(tI)->pt() < 100.0 ) // Not interested in 0 MeV dummy particles
      continue;
    if( m_truthParts->at(tI)->status()  < 20 && m_truthParts->at(tI)->status() >= 11) // In Pythia, this status corresponds to beam particles. Not interested in incoming gluons. Only ISR/FSR
      continue;
    if( m_truthParts->at(tI)->absPdgId() == 21 && m_truthParts->at(tI)->nChildren() == 2){
      gluons->push_back(tPart);
      *tPart = *m_truthParts->at(tI);
    }
  }

  if( gluons->size() == 0){
    ATH_MSG_VERBOSE("No additional gluons in this event.");
    return "LO";
  }

  // Sort the gluons according to the pt
  gluons->sort(ptsorter);

  
  for (unsigned int tI = 0; tI < gluons->size() ; ++tI){
    xAOD::TruthParticle* gluon  = (*gluons)[tI];
    if( tI >= 2)
      break;
    for(unsigned int childIndex=0; childIndex<gluon->nChildren(); childIndex++) {
      if ( !gluon->child(childIndex) || gluon->child(childIndex)==nullptr )
        continue;
      switch(gluon->child(childIndex)->absPdgId()){
      case 6:
	output += "t";
      break;
      case 5:
	output += "b";
      break;
      case 4:
	output += "c";
      break;
      default:
	output += "l";
      break;
      }
    }
  }
  return output;

}

int MasterShef::TopCharmDecayInfo(){
  int category=-1; // categories-> 0 == tc+MET; 1 == cc+MET; 2 == tt+MET; -1 == "Unknown"

  xAOD::TEvent *m_event = wk()->xaodEvent();
  const xAOD::TruthParticleContainer* m_truthParts;
  if (m_event->retrieve(m_truthParts, "TruthParticles").isFailure()){
      ATH_MSG_WARNING("TruthParticles container is missing in this sample.");
      return category;
  }
  if ( m_truthParts->size() == 0){
    ATH_MSG_WARNING("TruthParticles container is empty in this sample.");
    return category;
  }
  int nTops = 0;
  int nCharms = 0;
  
  for (unsigned int tI=0; tI< m_truthParts->size(); ++tI) {
    const xAOD::TruthParticle* tPart = (*m_truthParts)[tI];

    int TruthPDGID=tPart->absPdgId();
    // Select stops: PDGID 1000006
    if (TruthPDGID != 1000006 )
      continue;
    if ( fabs(tPart->status()) != 62  )
      continue;
    for (unsigned int childIndex=0; childIndex<tPart->nChildren(); childIndex++) {
      if ( !tPart->child(childIndex) || tPart->child(childIndex)==nullptr )
	continue;
      if (tPart->child(childIndex)->absPdgId() == 4 ){
	nCharms += 1;
      }
      if (tPart->child(childIndex)->absPdgId() == 6 ){
	nTops += 1;
      }
    }
  }
  // Check where does the category fall.
  if(nTops==1 && nCharms == 1)
    category=0;
  else if(nTops==0 && nCharms == 2)
    category=1;
  else if(nTops==2 && nCharms == 0)
    category=2;

  return category;
}
