#include <MasterShef/MasterShef.h>
#include "JetSubStructureUtils/Nsubjettiness.h"
#include "JetSubStructureUtils/Dipolarity.h"
#include "JetSubStructureUtils/EnergyCorrelator.h"

bool MasterShef::calcJSS(xAOD::Jet &jet){
  
  
  JetSubStructureUtils::Dipolarity dip12(1,2,false,0.3);
  jet.auxdata<float>("Dip12")=dip12.result(jet);
  
  
  fastjet::contrib::WTA_KT_Axes wta_kt_axes;
  fastjet::contrib::KT_Axes kt_axes;

  fastjet::contrib::NormalizedCutoffMeasure measure_0p5(0.5, jet.getSizeParameter(), 1000000);
  fastjet::contrib::NormalizedCutoffMeasure measure_1p0(1.0, jet.getSizeParameter(), 1000000);
  fastjet::contrib::NormalizedCutoffMeasure measure_2p0(2.0, jet.getSizeParameter(), 1000000);


  JetSubStructureUtils::Nsubjettiness tau1_beta0p5    (1,kt_axes    ,measure_0p5);
  JetSubStructureUtils::Nsubjettiness tau1_beta0p5_wta(1,wta_kt_axes,measure_0p5);
  JetSubStructureUtils::Nsubjettiness tau1_beta2p0_wta(1,wta_kt_axes,measure_2p0);

  
  JetSubStructureUtils::Nsubjettiness tau2_beta0p5    (2,kt_axes    ,measure_0p5);
  JetSubStructureUtils::Nsubjettiness tau2_beta0p5_wta(2,wta_kt_axes,measure_0p5);
  JetSubStructureUtils::Nsubjettiness tau2_beta1p0    (2,kt_axes    ,measure_1p0);
  JetSubStructureUtils::Nsubjettiness tau2_beta2p0_wta(2,wta_kt_axes,measure_2p0);
  JetSubStructureUtils::Nsubjettiness tau2_beta2p0    (2,kt_axes    ,measure_2p0);
  
  JetSubStructureUtils::Nsubjettiness tau3_beta0p5    (2,kt_axes    ,measure_0p5);
  JetSubStructureUtils::Nsubjettiness tau3_beta0p5_wta(3,wta_kt_axes,measure_0p5);
  JetSubStructureUtils::Nsubjettiness tau3_beta2p0_wta(3,wta_kt_axes,measure_2p0);
  float m_Beta=0.5;

  JetSubStructureUtils::EnergyCorrelator ECF1(1, m_Beta, JetSubStructureUtils::EnergyCorrelator::pt_R);
  JetSubStructureUtils::EnergyCorrelator ECF2(2, m_Beta, JetSubStructureUtils::EnergyCorrelator::pt_R);
  JetSubStructureUtils::EnergyCorrelator ECF3(3, m_Beta, JetSubStructureUtils::EnergyCorrelator::pt_R);


  jet.auxdata<float>("tau1_beta0p5")=tau1_beta0p5.result(jet);
  jet.auxdata<float>("tau1_beta0p5_wta")=tau1_beta0p5_wta.result(jet);
  jet.auxdata<float>("tau1_beta2p0_wta")=tau1_beta2p0_wta.result(jet);

  jet.auxdata<float>("tau2_beta0p5")=tau2_beta0p5.result(jet);
  jet.auxdata<float>("tau2_beta0p5_wta")=tau2_beta0p5_wta.result(jet);
  jet.auxdata<float>("tau2_beta1p0")    =tau2_beta1p0.result(jet);
  jet.auxdata<float>("tau2_beta2p0_wta")=tau2_beta2p0_wta.result(jet);
  jet.auxdata<float>("tau2_beta2p0")    =tau2_beta2p0.result(jet);

  jet.auxdata<float>("tau3_beta0p5")=tau3_beta0p5.result(jet);
  jet.auxdata<float>("tau3_beta0p5_wta")=tau3_beta0p5_wta.result(jet);
  jet.auxdata<float>("tau3_beta2p0_wta")=tau3_beta2p0_wta.result(jet);
  jet.auxdata<float>("ECF1_beta0p5")=ECF1.result(jet);
  jet.auxdata<float>("ECF2_beta0p5")=ECF2.result(jet);
  jet.auxdata<float>("ECF3_beta0p5")=ECF3.result(jet);

  return true;
}

 
std::vector<fastjet::PseudoJet> MasterShef::jetConVecToPseudojet ( xAOD::JetConstituentVector input)
{
  std::vector<fastjet::PseudoJet> constituents;
    
  for ( const auto* con : input){
    fastjet::PseudoJet p(0,0,0,0);
    float pt = con->pt();
    float y = con->rapidity();
    float phi = con->phi();
    float m = con->m();
    if (y != y) {
      continue; // null vectors cause NaNs
    } else {
      p.reset_PtYPhiM(pt, y, phi, m);
      constituents.push_back(p);
    }
  }
    
  return constituents;
}



double MasterShef::getWparton(const xAOD::Jet *theJet, std::vector<const xAOD::TruthParticle*> partons){
  TLorentzVector jet = theJet->p4();

  double HT_part=0;
  double sum_pt_R=0;
  for(auto part : partons){
    if(!part)continue;
      
    TLorentzVector par=part->p4();
    HT_part+=par.Pt();
    
    sum_pt_R+= par.Pt()*par.DeltaR(jet);
  }
  
  double retval=0;
  if(HT_part>0)
    retval=sum_pt_R/HT_part;

  return retval;

}



