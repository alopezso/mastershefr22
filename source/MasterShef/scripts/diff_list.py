import sys

with open(sys.argv[1]) as f:
    content1=f.readlines()
content1 = [x.rstrip() for x in content1]
with open(sys.argv[2]) as f:
    content2=f.readlines()
content2 = [x.rstrip() for x in content2]

missing_from_2=set(content1)-set(content2)
missing_from_1=set(content2)-set(content1)
all_missing = missing_from_2.union(missing_from_1)

for item in sorted(missing_from_1):#sorted(all_missing):
    print item
