import sys,os,argparse,glob,gc,ROOT,collections,array
from ROOT import gROOT,gSystem,PyConfig,TH1,TH2
c1=ROOT.TCanvas("c1")

#nominal=ROOT.TFile("/data/fracchia/local_ntuples/MasterShef/Zmumu_cutflow.root") # this is the Nominal input file (from SUSY.py)
#nominalfix=ROOT.TFile("/data/fracchia/local_ntuples/ExportTree/Zmumu_cutflow.root") # this is the fixed Nominal input file from ExportTrees
              
nominal=ROOT.TFile("/home/fracchia/SUSY_CODE/testLocal/data-stop0L/mc15_13TeV.root") # this is the Nominal input file (from SUSY.py)                                            
nominalfix=ROOT.TFile("/home/fracchia/SUSY_CODE/testExport/mc15_13TeV.root") # this is the fixed Nominal input file from ExportTrees                                     

HN=nominal.Get("HNominal") # get the HNominal hist from the Nominal input
NF=nominalfix.Get("NominalFixed") # get the NominalFixed branch from the fixed input file

variables=open('CalibratedVariables.txt','w') # open the variables file
variables.write(str(NF.GetListOfBranches())) # prints a list of the branches/variables in the NominalFixed file
variables.close

#Variables for the cutflow (from variables.txt) and the value of the cut
pT_80_80_40_40='pT_2jet>80 && pT_4jet>40'
nsigLep1='(nEl+nMu)==1 && nbaselineLep==1'
nsigLep2='((nEl==2 && nMu==0) || (nEl==0 && nMu==2)) && nbaselineLep==2'
matching='((nEl==2 && nMu==0 && pass1etriggers) || (nEl==0 && nMu==2 && pass1mutriggers)) && nbaselineLep==2'
dphimin2='dphimin2_orig>0.4'
num_bjets='num_bjets>=1'
met250='eT_miss_orig>250'
metlow50='eT_miss_orig<50'
MTlepmet='MT_orig>30 && MT_orig<120'
MTbmin='MTbmin_orig>100'
m0antikt12='m_1fatjet_kt12>70'
dRblmin='dRlbmin<1.5'
OSlep='(charge_1l+charge_2l)==0'
mll='mll>86 && mll<96'
metlep70='eT_miss>70'
num_bjets2='num_bjets>=2'
mettrack='eT_miss_track>30'
dphitrack='dphi_track<TMath::Pi()/3'
lepveto='nbaselineLep==0'
NbV='NbV>=1'
NjV='NjV>=5'
MS='MS>300'
dphiISRI='dphiISRI>3.0'
PTISR='PTISR>400'
pTjV4='pTjV4>50'
RISR='RISR>0.25 && RISR<0.4'

#ttbar
#CutNames=[nsigLep1,pT_80_80_40_40,num_bjets,met250,dphimin2,MTlepmet,MTbmin,m0antikt12,dRblmin] # make a list of the cuts
#zmumu
#CutNames=[nsigLep2,matching,metlow50,OSlep,pT_80_80_40_40,mll,metlep70,num_bjets2] # make a list of the cuts                                                          
CutNames=[lepveto,pT_80_80_40_40,num_bjets,met250,dphimin2,mettrack,dphitrack,num_bjets2,NbV,NjV,MS,dphiISRI,PTISR,pTjV4,RISR]


ExportedCuts=[] # create an array for the cuts you wish to apply

for i in range (0,len(CutNames)): # loop over the variables you are cutting on
    if i==0:
        ExportedCuts.append(CutNames[i]) # for the first case the cut is just your first cut (met100)
    else:
        addcut=str(ExportedCuts[i-1])+'&&'+str(CutNames[i]) # for each subsequent cut you need to add all previous cuts
        ExportedCuts.append(addcut)   
print ExportedCuts


CF=ROOT.TH1F("CutFlow",";cut;entries",19,0,19) # set up your cutflow histogram
CF.Fill(0,HN.GetBinContent(1)) # for nominal cuts just read the bin content from the HNominal histogram
CF.Fill(1,HN.GetBinContent(2))
CF.Fill(2,HN.GetBinContent(3))
CF.Fill(3,HN.GetBinContent(4))
CF.Fill(4,HN.GetBinContent(5))
CF.Fill(5,HN.GetBinContent(6))
CF.Fill(6,HN.GetBinContent(7)) 
CF.Fill(7,HN.GetBinContent(8)) # end of nominal cuts
CF.Fill(8,HN.GetBinContent(9))
j=9 # try not to hardcode this, but it'll do for now (you're filling CF bins starting from 7)
for i in range (0,len(CutNames)):    
    CF.Fill(j,NF.GetEntries(ExportedCuts[i])) # fill a new bin for each new cut you add 
    j+=1
cutvalue_pairs=[('Total from bookkeeper:',CF.GetBinContent(1)), # create a dictionary of cuts and entries after the cut
      ('Total:',CF.GetBinContent(2)),
      ('GRL:',CF.GetBinContent(3)),
      ('LAr and Tile error:',CF.GetBinContent(4)),
      ('IsMetTriggerPassed:',CF.GetBinContent(5)),
      ('Primary Vertex >= 1',CF.GetBinContent(6)),
      ('Jet Cleaning:',CF.GetBinContent(7)),
      ('Cosmic Muon:',CF.GetBinContent(8)),
      ('Clean Muon:',CF.GetBinContent(9)),
      ('1 lepton:',CF.GetBinContent(10)),
      ('jet pT 80,80,40,40:',CF.GetBinContent(11)),
      ('num_bjets>=1:',CF.GetBinContent(12)),
      ('MET > 250:',CF.GetBinContent(13)),
      ('minDphi2:',CF.GetBinContent(14)),
      ('30<MT<120:',CF.GetBinContent(15)),
      ('MTbmin>100:',CF.GetBinContent(16)),
      ('mAkT120>70:',CF.GetBinContent(17)),
      ('dR(b,l)_min<1.5:',CF.GetBinContent(18)),
      ('1:',CF.GetBinContent(19)),
      ('2:',CF.GetBinContent(20)),
      ('3:',CF.GetBinContent(21)),
      ('4:',CF.GetBinContent(22)),
      ('5:',CF.GetBinContent(23)),
      ('6:',CF.GetBinContent(24))
      ]
cutValues=collections.OrderedDict(cutvalue_pairs) # maintain the order that you filled the dict

ofilecsv=open('FinalValues.csv','w') # open your output file (.csv)
ofiletxt=open('FinalValues.txt','w') # open your output file (.txt)
for cut in cutValues:
    print cut,cutValues[cut]
    ofilecsv.write('%s \t %d\n' % (cut,cutValues[cut])) # print cut name and entries for each cut, write to file
    ofiletxt.write('%s \t %d\n' % (cut,cutValues[cut])) # print cut name and entries for each cut, write to file
ofilecsv.close
ofiletxt.close


CF.Draw("hist") # draw cutflow histogram
c1.SaveAs("CutFlowHist.pdf")
c1.SaveAs("CutFlowHist.eps")
print 'press any key to exit'
raw_input() # wait for user input to close
