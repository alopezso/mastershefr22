import ROOT, sys


var="jet_pt[0]/1000."
a=600
var="MET_pt[0]/1000."
a=200



f = ROOT.TFile(sys.argv[1])
t = f.Get("Nominal")
#ROOT.gStyle.SetOptStat(0000)
c = ROOT.TCanvas("c","c",800,600)
c.SetLogy()
h = ROOT.TH1F("h","h",200,0,a)
t.Draw(var+">>h","TriggerWeight2*(1)","goff")
h.SetLineColor(1)
h.SetMarkerColor(1)
h.Draw("ep")
h.SetTitle("")
h.SetYTitle("Nevents")
h.SetXTitle("Leading Jet p_{T} [GeV]")
h.SetXTitle("E_{T}^{miss} [GeV]")


#seed
seed = ROOT.TH1F("seed","seed",200,0,a)
t.Draw(var+">>seed","TriggerWeight2*( (MET_pt/1000. - 8)/sqrt(sumet/100.) < 0.2)","goff")
seed.SetLineColor(222)
seed.SetMarkerColor(222)
seed.Draw("ep same")
#seed
seed2 = ROOT.TH1F("seed2","seed2",200,0,a)
#t.SetAlias("HT","Sum$(jet_pt)")
#t.SetAlias("met_avPt","(MET_pt/1000.)/(HT/nJets)")
#t.Scan("met_avPt:MET_pt:Sum$(jet_pt):nJets:jet_pt[0]:jet_pt[1]")

t.Draw(var+">>seed2","TriggerWeight2*( (MET_pt/1000.-8)/sqrt(sumet/100.) < 0.2 && (MET_pt/1000.)/((Sum$(jet_pt)/1000.)/nJets) <0.1)","goff")




seed2.SetLineColor(225)
seed2.SetMarkerColor(225)
seed2.Draw("ep same")

print seed.GetEntries(),seed2.GetEntries()


leg = ROOT.TLegend(0.3,0.65,0.7,0.8)
leg.SetBorderSize(0)
leg.AddEntry(h,"All Events","epl")
leg.AddEntry(seed,"All Seed Events (metsig)","epl")
leg.AddEntry(seed2,"All Seed Events (metsig + met/avPt)","epl")
leg.Draw()

raw_input()
