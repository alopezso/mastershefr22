import sys,os,argparse,glob,gc,ROOT,collections,array
from ROOT import gROOT,gSystem,PyConfig,TH1,TH2
c1=ROOT.TCanvas("c1")

nominal=ROOT.TFile("Nominal.root") # this is the Nominal input file (from SUSY.py)
nominalfix=ROOT.TFile("NominalFixed.root") # this is the fixed Nominal input file from ExportTrees

HN=nominal.Get("HNominal") # get the HNominal hist from the Nominal input
NF=nominalfix.Get("NominalFixed") # get the NominalFixed branch from the fixed input file

variables=open('CalibratedVariables.txt','w') # open the variables file
variables.write(str(NF.GetListOfBranches())) # prints a list of the branches/variables in the NominalFixed file
variables.close

#Variables for the cutflow (from variables.txt) and the value of the cut
met100='eT_miss_orig>100'
njgood35='nj_good35 <=4 && nj_good35>=2'
pT_1j_g50='pT_1jet>50'
pT_2j_g50='pT_2jet>50'
pT_4j_l50='pT_4jet<50'
nsigLep0='(nEl+nMu)==0'
nbaseLep0='nbaselineLep==0'
dphimin4='dphimin4_orig>0.4'
metmeff2j='(eT_miss_orig/meff2j)>0.25'
num_bjets='num_bjets==2'
lbj='(pT_1jet==pT_1bjet) && (pT_2jet==pT_2bjet)'
met250='eT_miss_orig>250'
mbb='mbb>200'
mct2b='mct2b>100'
CutNames=[met100,njgood35,pT_1j_g50,pT_2j_g50,pT_4j_l50,nsigLep0,nbaseLep0,dphimin4,metmeff2j,num_bjets,lbj,met250,mbb,mct2b] # make a list of the cuts
ExportedCuts=[] # create an array for the cuts you wish to apply

for i in range (0,len(CutNames)): # loop over the variables you are cutting on
    if i==0:
        ExportedCuts.append(CutNames[i]) # for the first case the cut is just your first cut (met100)
    else:
        addcut=str(ExportedCuts[i-1])+'&&'+str(CutNames[i]) # for each subsequent cut you need to add all previous cuts
        ExportedCuts.append(addcut)   

CF=ROOT.TH1F("CutFlow",";cut;entries",22,0,22) # set up your cutflow histogram
CF.Fill(0,HN.GetBinContent(1)) # for nominal cuts just read the bin content from the HNominal histogram
CF.Fill(1,HN.GetBinContent(2))
CF.Fill(2,HN.GetBinContent(4))
CF.Fill(3,HN.GetBinContent(5))
CF.Fill(4,HN.GetBinContent(6))
CF.Fill(5,HN.GetBinContent(7))
CF.Fill(6,HN.GetBinContent(8)) # end of nominal cuts

j=7 # try not to hardcode this, but it'll do for now (you're filling CF bins starting from 7)
for i in range (0,len(CutNames)):
    CF.Fill(j,NF.GetEntries(ExportedCuts[i])) # fill a new bin for each new cut you add 
    j+=1
cutvalue_pairs=[('Total from bookkeeper:',CF.GetBinContent(1)), # create a dictionary of cuts and entries after the cut
      ('Total:',CF.GetBinContent(2)),
      ('LAr and Tile error:',CF.GetBinContent(3)),
      ('IsMetTriggerPassed:',CF.GetBinContent(4)),
      ('Primary Vertex >= 1',CF.GetBinContent(5)),
      ('Jet Cleaning:',CF.GetBinContent(6)),
      ('Clean Muon:',CF.GetBinContent(7)),
      ('MET>100:',CF.GetBinContent(8)),
      ('4>= nj_good35>=2:',CF.GetBinContent(9)),
      ('pT_1jet > 50:',CF.GetBinContent(10)),
      ('pT_2jet > 50:',CF.GetBinContent(11)),
      ('pT_4jet<50:',CF.GetBinContent(12)),
      ('nsignalLep==0:',CF.GetBinContent(13)),
      ('nbaselineLep==0:',CF.GetBinContent(14)),
      ('dphimin4>0.4:',CF.GetBinContent(15)),
      ('met/meff2j > 0.25:',CF.GetBinContent(16)),
      ('num_bjets==2:',CF.GetBinContent(17)),
      ('leading b-jets:',CF.GetBinContent(18)),
      ('MET > 250:',CF.GetBinContent(19)),
      ('mbb>200:',CF.GetBinContent(20)),
      ('mct2b>100:',CF.GetBinContent(21))
      ]
cutValues=collections.OrderedDict(cutvalue_pairs) # maintain the order that you filled the dict

ofilecsv=open('FinalValues.csv','w') # open your output file (.csv)
ofiletxt=open('FinalValues.txt','w') # open your output file (.txt)
for cut in cutValues:
    print cut,cutValues[cut]
    ofilecsv.write('%s \t %d\n' % (cut,cutValues[cut])) # print cut name and entries for each cut, write to file
    ofiletxt.write('%s \t %d\n' % (cut,cutValues[cut])) # print cut name and entries for each cut, write to file
ofilecsv.close
ofiletxt.close


CF.Draw("hist") # draw cutflow histogram
c1.SaveAs("CutFlowHist.pdf")
c1.SaveAs("CutFlowHist.eps")
print 'press any key to exit'
raw_input() # wait for user input to close
