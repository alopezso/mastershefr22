import ROOT,sys

##
## Set the region to perform the cutflow for
##
region="SRA"
#region="Stop0L"


##
## Variable name lookup
##
m_namelookup={
    "met"     :"eT_miss_orig",
    "njets"   :"nj_good35",
    "pTj1"    :"pT_1jet",
    "pTj2"    :"pT_2jet",
    "pTj4"    :"pT_4jet",
    "nsigLep" :"(nEl+nMu)",
    "nbaseLep":"nbaselineLep",
    "dphimin" :"dphimin4",
    "meff2j"  :"meff2j",
    "metmeff" :"eT_miss_orig/meff2j",
    "nbjets"  :"num_bjets",
    "leadb1"  :"pT_1bjet==pT_1jet",
    "leadb2"  :"pT_2bjet==pT_2jet",
    "mbb"     :"mbb",
    "mct2b"   :"mct2b",
    "dphimin2":"dphimin2",
    "dphimettrack":"dphi_track_orig",
    "mettrack":"eT_miss_track",
    "mAkT120":"m_1fatjet_kt12",
    "mAkT121":"m_2fatjet_kt12",
    "mAkT080":"m_1fatjet_kt8",
    "MTbmin":"MTbmin_orig",
    "tauveto":"passtauveto",
    "dRbb":"dRb1b2",
    "MT2":"MT2Chi2"
    }


##
## Define the Cuts
##
m_cuts={}
m_cuts["SRA"]=[
    ("nLep=0"             ,m_namelookup["nsigLep"]+"==0 && "+m_namelookup["nbaseLep"]+"==0"),
    ("jet pT 80,80,40,40" ,m_namelookup["pTj2"]+">80 && "+m_namelookup["pTj4"]+">40"),
    ("nbjets>=1"        ,m_namelookup["nbjets"]+">=1"),
    ("met>250"         ,m_namelookup["met"]+">250"),
    ("dphimin2>0.4"     ,m_namelookup["dphimin2"]+">0.4"),
    ("mettrack>30"     ,m_namelookup["mettrack"]+">30"),
    ("dphimettrack<Pi/3"     ,m_namelookup["dphimettrack"]+"<(TMath::Pi()/3.)"),
    ("nbjets>=2"     ,m_namelookup["nbjets"]+">=2"),
    ("mAkT120>120"     ,m_namelookup["mAkT120"]+">120"),
    ("mAkT121>120"     ,m_namelookup["mAkT121"]+">120"),
    ("mAkT08>80"     ,m_namelookup["mAkT080"]+">60"),
    ("MTbmin>200"     ,m_namelookup["MTbmin"]+">200"),
    ("tauveto"     ,m_namelookup["tauveto"]+"==1"),
    ("met>450"         ,m_namelookup["met"]+">450"),
    ("dRbb>1"     ,m_namelookup["dRbb"]+">1"),
    ("MT2>200"     ,m_namelookup["MT2"]+">200")
    #("njets =2,3,4"    ,m_namelookup["njets"]+">=2 && "+m_namelookup["njets"]+"<=4"),
    #("pTj1>50"         ,m_namelookup["pTj1"]+">50"),
    #("pTj2>50"         ,m_namelookup["pTj2"]+">50"),
    #("pTj4<50"         ,m_namelookup["pTj4"]+"<50"),
    #("nsignalLep=0"    ,m_namelookup["nsigLep"]+"==0"),
    #("nbaselineLep=0"  ,m_namelookup["nbaseLep"]+"==0"),
    #("dphimin>0.4"     ,m_namelookup["dphimin"]+">0.4"),
    #("met/meff2j>0.25" ,m_namelookup["metmeff"]+">0.25"),
    #("nbjets=2"        ,m_namelookup["nbjets"]+"==2"),
    #("Leading bjets"   ,m_namelookup["leadb1"]+"&&"+m_namelookup["leadb2"]),
    #("met>250"         ,m_namelookup["met"]+">250"),
    #("mbb>200"         ,m_namelookup["mbb"]+">200"),
    #("mct2b>100"       ,m_namelookup["mct2b"]+">100")
    ]    


m_cuts["Stop0L"]=[
    ("nbaselineLep=0"           ,"nbaselineLep==0"),
    ("Signal jet (80,80,40,40)" ,"pT_1jet>80 && pT_2jet>80 && pT_3jet>40 && pT_4jet>40"),
    ("OneBtags"                 ,"num_bjets>0"),
    ("MET>250"                  ,"eT_miss_orig>250")
]




##
## get input file from command line 
##
ch = ROOT.TChain("NominalFixed")
ch.Add(sys.argv[1])

##
## Set the weights
##
m_weights=["1"]#,"ElecWeight","MuonWeight","jvtweight","btagweight","pileupweight"]



##
## Do the loop over the cuts printing out the name, raw and weighted 
##
cutstring=""
print "{0:>20} {1:>15} {2:>15}".format("Name","Raw", "Weighted")
for cut in m_cuts[region]:
    name=cut[0]
    cut=cut[1]
    cut="("+cut+")"
    cutstring+="*"+cut

    expression="*".join(m_weights)+cutstring
    #print expression

    h = ROOT.TH1F("h","h",1,0,2)
    ch.Draw("1>>h",expression,"goff")
    print "{0:20s} {1:15.0f} {2:15.3f}".format(name,h.GetEntries(), h.Integral())
    del h
