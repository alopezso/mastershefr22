import sys,os


try:
    buildDir=os.environ["ACMBUILDDIR"]
except KeyError: 
   print "Please setup the release in the build dir before trying this!"
   sys.exit(1)    


import argparse

parser = argparse.ArgumentParser(description='make cluster jobs for brute force running on a missing sample')
parser.add_argument('-f', '--inputContainer', action='store', dest='container',
                    required=True,
                    help='give a container that is to be ran on')
parser.add_argument('-o','--outDir', action='store', dest='outDir',
                    required=True,
                    help='output directory for the files to appear',
                    )
parser.add_argument('--useScratch',action='store_true', dest='useScratch',
                    help='switch out the primary disk to use scratch disk and copy over',
                    )
parser.add_argument("--proxy", dest="proxy_file",help="an alternative location of your proxy", default=None)

options=parser.parse_args()




#setup a logger
import logging
debug_level=logging.INFO
logging.basicConfig(level=debug_level,
                    format='%(name)-12s :: %(levelname)-8s ::       %(message)s')
msg=logging.getLogger('make_cluster_jobs ')



import subprocess
container_output=subprocess.check_output(["rucio","list-files" ,"%s"%(options.container)]).rstrip()    

list_of_files=[]
for line in container_output.split('\n'):
    if "mc16_13TeV:" not in line:continue
    sam=line.split("|")[1]
    sam=sam.strip()
    list_of_files.append(sam)
    
msg.info("Got list of files from container %s"%(options.container))

outDir=options.outDir


user=os.environ.get('USER')
#check if a proxy file exists
proxy_file="/home/%s/.globus/myproxy.tmp"%(user)
if options.proxy_file is not None:
    msg.info("you supplied the proxy file =%s from the command line"%(options.proxy_file))
    proxy_file=options.proxy_file

if not os.path.exists(proxy_file):
    msg.error("warning proxy_file=%s does not exist, please create it or supply a different one with --proxy <file> option"%(proxy_file))
    exit(0)


msg.info("Going to use a temporary proxy found =%s, now checking..."%(proxy_file))

#now check the proxy
import subprocess
proxy_check_output=subprocess.check_output(["voms-proxy-info","--file" ,"%s"%(proxy_file)]).rstrip()    

msg.info(proxy_check_output)

for line in proxy_check_output.split("\n"):
    if "timeleft" in line:
        time=line.rstrip().split("  :")[1].replace(" ","")
        if time == "00:00:00":
            msg.error("proxy has expired!!")
            exit(0)




top=r'''
#!/bin/bash
unset GLITE_ENV_SET 
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
export X509_USER_PROXY=%s
export DQ2_LOCAL_SITE_ID=ROAMING 
lsetup fax
lsetup python 
lsetup rucio --quiet -w 
source /cvmfs/grid.cern.ch/emi3ui-latest/etc/profile.d/setup-ui-example.sh

'''%(proxy_file)




for i,runfile in enumerate(list_of_files):
    a=open("msjob_"+str(i)+".sh","w")
    
    a.write(top)
    #setup the code
    a.write("cd %s; acmSetup; acm compile \n"%(buildDir))
    
    #setup the working directories
    workDir="%s/Job%d/"%(outDir,i)
    runDir="%srun/"%(workDir)
    samDir="%ssample/"%(workDir)
    
    #go to the location to download the sample
    a.write("mkdir -p %s ; mkdir -p %s \n"%(runDir,samDir))
    a.write("cd %s; rucio get %s; ls \n"%(samDir,runfile))
    
    #go to the run dir
    a.write("cd %s \n"%(runDir))
            
    a.write("python %s/../run/SUSY.py --analysis=sbottom_multib_fixed --dataSource=1 --localFile=%s --submitDir=%s \n"%(buildDir,samDir,"output"))
   
    a.close()
    os.system("chmod u+x msjob_"+str(i)+".sh")
    msg.info("done msjob %d"%(i))
    #os.system("condor_qsub -N Znunu_"+str(i)+" `pwd`/job_"+str(i)+".sh")

    break
    
    
