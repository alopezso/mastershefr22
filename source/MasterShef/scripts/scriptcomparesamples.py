import os
file_submitted = "/home/macdonald/SUSY_Nov_2016/runNov09.txt"
file_wanted = "/home/macdonald/SUSY_Nov_2016/MasterShef/share/sample_lists/mc_request_susy7.txt"

list_submitted = []
list_wanted = []
list_missing = []

with open(file_wanted, "r") as fw:
    for line in fw:
        if line.strip() and "#" not in line:
                list_wanted.append(line.rstrip().split("mc15_13TeV.")[1])
with open(file_submitted, "r") as fs:
    for line in fs:
        if line.strip() and "#" not in line:
            list_submitted.append(line.rstrip().split("mc15_13TeV.")[1])
for item_wanted in list_wanted:
    item_found = 0
    for item_submitted in list_submitted:
        if item_wanted.split(".")[0] == item_submitted.split(".")[0]:
            item_found = 1
            break
    if item_found == 0:
        list_missing.append("mc15_13TeV."+item_wanted)
print "##### %i FILES HAVE NOT BEEN SUBMITTED #####" % len(list_missing)
for item_missing in list_missing:
    #print item_missing
    os.system("rucio list-dids "+item_missing.replace("AOD","DAOD_SUSY1")+"* --filter type=container --short")
