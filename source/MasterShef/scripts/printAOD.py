import os,sys
file_wanted = sys.argv[1]

with open(file_wanted, "r") as fw:
    for line in fw:
        line=line.rstrip()
        if "#" in line:
            continue
        
        name=line.split("_p")[0].replace("DAOD_SUSY1","AOD").replace("DAOD_SUSY7","AOD")

        os.system("rucio list-dids "+name+"* --filter type=container --short")
