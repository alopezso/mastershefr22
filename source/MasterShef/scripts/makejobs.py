import sys,os


for i,line in enumerate(open(sys.argv[1])):
    a=open("job_"+str(i)+".sh","w")
    runfile=line.rstrip().split(",")[0]

    a.write("#!/bin/bash \n")
    a.write("unset GLITE_ENV_SET \n")
    a.write("echo $(hostname) \n")
    a.write("export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase \n")
    a.write("source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh \n")
    a.write("export X509_USER_PROXY=/home/macdonald/.globus/myproxy.tmp \n")
    a.write("export DQ2_LOCAL_SITE_ID=ROAMING \n")
    a.write("lsetup fax \n")
    a.write("lsetup rucio --quiet \n")
    a.write("source /cvmfs/grid.cern.ch/emi3ui-latest/etc/profile.d/setup-ui-example.sh \n")
    

    a.write("mkdir /scratch/macdonald/SUSY_Dec_2016_"+str(i)+"/ \n")
    a.write("cd  /scratch/macdonald/SUSY_Dec_2016_"+str(i)+"/ \n")
    a.write("lsetup \"rcsetup Base,2.4.24\"  \n")
    a.write("cp -r /home/macdonald/SUSY_Dec_2016/MasterShef/ . \n")
    a.write("cp -r /home/macdonald/SUSY_Dec_2016/JetSmearing/ . \n")
    a.write("cp -r /home/macdonald/SUSY_Dec_2016/SUSYTools/ . \n")
    a.write("ls \n")
    a.write("rc find_packages \n")
    a.write("rc compile \n")
    a.write("mkdir temp \n")
    a.write("cd temp \n")
    a.write("rucio list-dids "+runfile+"\n")
    a.write("rucio get "+runfile+"\n")
    a.write("ls \n")
    a.write("cd  /scratch/macdonald/SUSY_Dec_2016_"+str(i)+"/ \n")
    a.write("ls \n")
    a.write("python MasterShef/Run/SUSY.py --analysis=sbottom_ph --dataSource=1 --localFile=temp/ --submitDir=job_"+str(i)+" \n")
    a.write("mv job_"+str(i)+" /atlas/macdonald/Znunu_missing/ \n") 
    a.write("rm -rf temp/ \n")
    a.close()
    os.system("chmod u+x job_"+str(i)+".sh")
    os.system("condor_qsub -N Znunu_"+str(i)+" `pwd`/job_"+str(i)+".sh")
    
    
    
