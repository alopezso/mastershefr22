import os,sys
import argparse
#import rucio.client

def getDataSetOriginalName(dataset,scope,tag):
    orig_dataset=dataset.replace(scope,"")
    tag=tag.replace(scope+".","")
    if not tag.endswith("."):
        tag+="."
    
    typeSample="mc16_13TeV"
    isData=False

    if "period" in dataset and "grp15" in dataset:
        typeSample="data15_13TeV"
        isData=True
    elif "period" in dataset and "grp16" in dataset:
        typeSample="data16_13TeV"
        isData=True
    elif "period" in dataset and "grp17" in dataset:
        typeSample="data17_13TeV"
        isData=True
    elif "period" in dataset and "grp18" in dataset:
        typeSample="data18_13TeV"
        isData=True

    if str(dataset.split(tag)[-1].split('.')[0]) in range(0,20):
        dataset.replace('.'+dataset.split(tag)[-1].split('.')[0]+'.','.')
    dsid=dataset.split(tag)[-1].split(".")[0]
    derivation=dataset.split(tag)[-1].split(".")[1]
    prodtags=dataset.split(tag)[-1].split(".")[2]
    stags=prodtags.split("_")
    single_tags=[]
    if dsid.startswith("00") or dsid.startswith("period"):
        isData=True
    for stag in stags:
        if any( stag.startswith(x) for x in ["e","s312","r10201","a875","r9364","r10724","p399"]):
            single_tags.append(stag)

    if not isData:
        orig_dataset=os.popen("rucio list-dids %s:%s*%s*%s.*%s --filter type=container --short"%(typeSample,typeSample,dsid,derivation,"_".join(single_tags))).read()
        print("rucio list-dids %s:%s*%s*%s.*%s --filter type=container --short"%(typeSample,typeSample,dsid,derivation,"_".join(single_tags)))
    elif isData and typeSample != "mc16_13TeV":
        orig_dataset=os.popen("rucio list-dids %s:%s*%s*%s.*%s --filter type=container --short"%(typeSample,typeSample,dsid,derivation,"_".join(single_tags))).read()
    elif isData and typeSample == "mc16_13TeV":
        orig_dataset=os.popen("rucio list-dids %s:%s*%s*%s.*%s --filter type=container --short"%("data15_13TeV","data15_13TeV",dsid,derivation,"_".join(single_tags))).read()
        orig_dataset+="\n"
        orig_dataset+=os.popen("rucio list-dids %s:%s*%s*%s.*%s --filter type=container --short"%("data16_13TeV","data16_13TeV",dsid,derivation,"_".join(single_tags))).read()
        orig_dataset+="\n"
        orig_dataset+=os.popen("rucio list-dids %s:%s*%s*%s.*%s --filter type=container --short"%("data17_13TeV","data17_13TeV",dsid,derivation,"_".join(single_tags))).read()
        orig_dataset+="\n"
        orig_dataset+=os.popen("rucio list-dids %s:%s*%s*%s.*%s --filter type=container --short"%("data18_13TeV","data18_13TeV",dsid,derivation,"_".join(single_tags))).read()
    return orig_dataset.split(":")[-1].split("\n")[0]+".root"



parser=argparse.ArgumentParser("Create linkks for datasets in RSE")
parser.add_argument("-r","--rse",dest="rse",action="store",help="RSE")
parser.add_argument("-t","--tag",dest="tag",action="store",help="MasterShef tag")
parser.add_argument("-t2","--tag2",dest="tag2",action="store",default="",help="Second tag")
parser.add_argument("-o","--output",dest="output",action="store",default="/eos/user/a/alopezso/testIsolationFTAG/",help="eos folder")
args=parser.parse_args()

tags=args.tag.split(",")

listofDatasets={}

for tag in tags:
    files=[]
    if args.tag2 != "":
        files=os.popen("rucio list-datasets-rse %s | grep \"%s\" | grep \"%s\" "%(args.rse,tag,args.tag2)).readlines()
    else:
        files=os.popen("rucio list-datasets-rse %s | grep \"%s\""%(args.rse,tag)).readlines()
    for dataset in files:
        if not tag in listofDatasets.keys():
            listofDatasets[tag]=[]
        listofDatasets[tag].append(dataset)


#rucio_client = rucio.client.didclient.DIDClient()

for tag,datasets in listofDatasets.items():
    
    for dataset in datasets:
        scope=dataset.split(':')[0]
        name_filter=dataset.split(':')[-1]

        if name_filter == "":
            print("Do not recognize the dataset identifier. It should have a form of <scope>:<dataset>. Exiting")
            sys.exit(0)
        if scope == "" or scope == dataset :
            print("Do not recognize the dataset identifier. It should have a form of <scope>:<dataset>. Exiting")
            sys.exit(0)
            
        datasetname=getDataSetOriginalName(dataset,scope,tag)
        current_dir=os.getcwd()
        os.chdir(current_dir)
        os.system("mkdir -p %s/%s"%(args.output,datasetname))
        os.chdir(args.output+"/"+datasetname)
        print("Linking dataset %s to %s"%(dataset,args.output+"/"+datasetname))
        os.system("rucio list-file-replicas --rse %s --link /eos/:/eos/  %s "%(args.rse,dataset))
        
