import sys,json


infile=sys.argv[1]

with open(infile) as f:
    data=json.load(f)

broken_list=[]
for item in data:
    for dataset in item["datasets"]:
        cont=dataset["containername"]
        if cont not in broken_list:broken_list.append(cont)

for item in broken_list:
    print(item)
