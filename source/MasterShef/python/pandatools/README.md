# How to get a list of broken jobs!


Firstly you should setup a cookie so that you can access the bigpanda pages from the terminal. This should last for a few days.
```
ssh <user>@lxplus.cern.ch "cern-get-sso-cookie -u https://bigpanda.cern.ch/ -o bigpanda.cookie.txt;
```

Now you can run a curl command to extract a json file for the samples from your favourite page. Here is an example of how I get a json file of my broken jobs with the wildcard for my production `cmacdona*21.2.32*Jun11*`:
```
ssh cmacdona@lxplus.cern.ch 'curl -b ~/bigpanda.cookie.txt -H '"'"'Accept: application/json'"'"' -H '"'"'Content-Type: application/json'"'"' "https://bigpanda.cern.ch/tasks/?taskname=*cmacdona*21.2.32*Jun11*&status=broken" | tee my_broken_jobs.json'
```

Now I run the script in this directory to print out the broken jobs for me:
```
$ python read_json.py my_broken_jobs.json | tee broken.list
mc16_13TeV:mc16_13TeV.343367.aMcAtNloPythia8EvtGen_A14_NNPDF23_NNPDF30ME_ttH125_allhad.deriv.DAOD_SUSY1.e4706_e5984_s3126_r10201_r10210_p3401
data17_13TeV:data17_13TeV.periodK.physics_Main.PhysCont.DAOD_SUSY1.grp17_v01_p3372
...
```

And now I have a list of broken jobs that I can resubmit.




