import os



def bbMeT_truth(self,alg):
    alg.m_config="MasterShef/confs/bbMeT/Nominal.conf" 
   
    #let's select at least 2 jets
    alg.m_userSelMods.push_back("SelJet")
    alg.m_SelJet_nJMn=1
    alg.m_SelJet_nJMx=1000

    #alg.printAllPossibleSelections()
    alg.m_useMCTruthClassifier=False
    
    
    alg.m_ntupLevel=6
    
    return alg
    

#nominal class for bbMET
def bbMeT(self,alg):
    
    alg.m_useAntiKtRcJets=False
    #alg.m_userTreeMods.push_back("fillRCFatJetVariables")
    
    alg.m_useTaus=False
    
    
    alg.m_useMCTruthClassifier=True
    alg.m_userTreeMods.push_back("fillTruthJets")
    alg.m_AddNeutMuTruthJets=False
    

    #----------- soft btagging ----------------------------------
    #turn on soft btagging
    alg.m_useSBtaggingVertexing=True
    #make sure loose/medium/tight working points are saved
    #turn this off as we only need the tight version
    alg.m_saveAllSbWPs=False
    #make sure track jets are now not saved
    alg.m_useTrackJets=False
    #dump these to the ntuples
    #alg.m_userTreeMods.push_back("fillTrackJetVariables")
    alg.m_userTreeMods.push_back("fillSBVVariables")
   

    #turn on the filling of HF truth hadrons to be matched!
    alg.m_useHFTruthHadrons=True
    alg.m_userTreeMods.push_back("fillHFHadronTruthVariables");
    #-----------------------------------------------------------

    #alg.m_config="MasterShef/confs/bbMeT/Nominal.conf" 
    alg.m_config="MasterShef/confs/bbMeT/PFlow_Tight.conf" 
    alg.m_isPflow=True
    #since we use continuous btagging, force the standard bjets in the skimming to be 77%
    alg.m_contWP=77
    
       
    #sel 150 GeV MET
    alg.m_userSelMods.push_back("SelMET")
    alg.m_SelMET_PtMn=180000.
    
    #sel 4 jets
    alg.m_userSelMods.push_back("SelJet")
    alg.m_SelJet_nJMn=2
    alg.m_SelJet_nJMx=1000

    #sel 3 b-jets
    #alg.m_userSelMods.push_back("SelNBJet")
    #alg.m_SelNBJet_nJMn=1
    #alg.m_SelNBJet_nJMx=1000

    

    #use a custom complex filter
    # currently:    b-jets >= 2 && ( 0 baseLep || 1 sigLep || 2 sigLep )
    alg.m_userSelMods.push_back("SelbbMeT")
    

    # # sel 0 signal leptons
    # alg.m_userSelMods.push_back("Sel0Lep")
    
    # #sel 0 baseline leptons
    alg.m_userSelMods.push_back("SelNBaselineLep")
    alg.m_SelnBL_Mx=0

    
    
    if (alg.m_dataSource!=0 and self.doSyst==True):

        alg.m_systNameList.push_back("EG_RESOLUTION_ALL__1down")
        alg.m_systNameList.push_back("EG_RESOLUTION_ALL__1up")
        if alg.m_dataSource==2:
            alg.m_systNameList.push_back("EG_SCALE_AF2__1down")
            alg.m_systNameList.push_back("EG_SCALE_AF2__1up")
        alg.m_systNameList.push_back("EG_SCALE_ALL__1down")
        alg.m_systNameList.push_back("EG_SCALE_ALL__1up")


        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1up")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1down")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1up")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1down")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1up")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1down")

        alg.m_systNameList.push_back("JET_GroupedNP_1__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_1__1down")
        alg.m_systNameList.push_back("JET_GroupedNP_2__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_2__1down")
        alg.m_systNameList.push_back("JET_GroupedNP_3__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_3__1down")
        alg.m_systNameList.push_back("JET_Flavor_Response__1up")
        alg.m_systNameList.push_back("JET_Flavor_Response__1down")

        if alg.m_dataSource==2:
            alg.m_systNameList.push_back("JET_JER_DataVsMC_AFII__1up")
            alg.m_systNameList.push_back("JET_JER_DataVsMC_AFII__1down")
            alg.m_systNameList.push_back("JET_RelativeNonClosure_AFII__1up")
            alg.m_systNameList.push_back("JET_RelativeNonClosure_AFII__1down")

        #JER, turning off _1down as JER is one-sided, so why bother saving both
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_1__1up")
        #        alg.m_systNameList.push_back("JET_JER_EffectiveNP_1__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_2__1up")
        #        alg.m_systNameList.push_back("JET_JER_EffectiveNP_2__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_3__1up")
        #        alg.m_systNameList.push_back("JET_JER_EffectiveNP_3__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_4__1up")
        #        alg.m_systNameList.push_back("JET_JER_EffectiveNP_4__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_5__1up")
        #        alg.m_systNameList.push_back("JET_JER_EffectiveNP_5__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_6__1up")
        #        alg.m_systNameList.push_back("JET_JER_EffectiveNP_6__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_7restTerm__1up")
        #        alg.m_systNameList.push_back("JET_JER_EffectiveNP_7restTerm__1down")


        alg.m_systNameList.push_back("MET_SoftTrk_ResoPara")
        alg.m_systNameList.push_back("MET_SoftTrk_ResoPerp")
        alg.m_systNameList.push_back("MET_SoftTrk_ScaleDown")
        alg.m_systNameList.push_back("MET_SoftTrk_ScaleUp")
                
        alg.m_systNameList.push_back("MUON_ID__1down")
        alg.m_systNameList.push_back("MUON_ID__1up")
        alg.m_systNameList.push_back("MUON_MS__1down")
        alg.m_systNameList.push_back("MUON_MS__1up")
        alg.m_systNameList.push_back("MUON_SAGITTA_RESBIAS__1down")
        alg.m_systNameList.push_back("MUON_SAGITTA_RESBIAS__1up")
        alg.m_systNameList.push_back("MUON_SAGITTA_RHO__1down")
        alg.m_systNameList.push_back("MUON_SAGITTA_RHO__1up")
        alg.m_systNameList.push_back("MUON_SCALE__1down")
        alg.m_systNameList.push_back("MUON_SCALE__1up")
            
       
    alg.m_ntupLevel=10
    alg.m_ntupLevel_sys=11
    return alg

#clone but set the default susytools conf to run over
def bbMeT_STDefault(self,alg):
    alg=self.bbMeT(alg)
    alg.m_config="MasterShef/confs/bbMeT/SUSYTools_Default_21_2_64.conf"
    alg.m_ntupLevel=10
    return alg



#seperate class with deep learning b-tagging 
def bbMeT_DL1(self,alg):
    #clone the nominal bbMet analysis
    alg=self.bbMeT(alg)
    #switch the configuration to be one with DL btagging
    alg.m_config="MasterShef/confs/bbMeT/DL1_btag.conf" 
    return alg

#seperate class with hybrid b-tagging
#current set with the 60% WP .....
#Btag.Tagger: MV2c10  
#Btag.WP: HybBEff_60    
def bbMeT_Hybrid(self,alg):
    #clone the nominal bbMet analysis
    alg=self.bbMeT(alg)
    #switch the configuration to be one with hybrid btagging
    alg.m_config="MasterShef/confs/bbMeT/Hybrid_btag.conf" 
    return alg



#seperate class with PFlow jets
def bbMeT_PFlow(self,alg):
    #clone the nominal bbMet analysis
    alg=self.bbMeT(alg)
    #switch the configuration to be one with pflow jets
    alg.m_config="MasterShef/confs/bbMeT/PFlow.conf" 
    alg.m_isPflow=True
    alg.m_ntupLevel=11
    return alg

#seperate class with PFlow jets.JVT WP = Loose.
def bbMeT_PFlow_Loose(self,alg):
    #clone the nominal bbMet analysis
    alg=self.bbMeT(alg)
    #switch the configuration to be one with pflow jets
    alg.m_config="MasterShef/confs/bbMeT/PFlow_Loose.conf" 
    alg.m_isPflow=True
    alg.m_ntupLevel=11
    return alg

#seperate class with PFlow jets. JVT WP = Medium.
def bbMeT_PFlow_Medium(self,alg):
    #clone the nominal bbMet analysis
    alg=self.bbMeT(alg)
    #switch the configuration to be one with pflow jets
    alg.m_config="MasterShef/confs/bbMeT/PFlow_Medium.conf" 
    alg.m_isPflow=True
    alg.m_ntupLevel=10
    return alg


#seperate class with PFlow jetsJVT WP = Tight.
def bbMeT_PFlow_Tight(self,alg):
    #clone the nominal bbMet analysis
    alg=self.bbMeT(alg)
    #switch the configuration to be one with pflow jets
    alg.m_config="MasterShef/confs/bbMeT/PFlow_Tight.conf" 
    alg.m_isPflow=True
    alg.m_ntupLevel=10
    return alg

#seperate class inflating the bjet reso in metsig
def bbMeT_bRES(self,alg):
    #clone the nominal bbMet analysis
    alg=self.bbMeT(alg)
    #inflate bjet reso
    alg.m_doBJetInflation=True
    return alg



