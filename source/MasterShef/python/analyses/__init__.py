
#def getMethods(:
#        self.method_list = [func for func in dir(self) if callable(getattr(self, func)) and not func.startswith("__")]
    

class Analyses():
    

    #import all sbottom analyses
    from ._sbottom import sbottom,sbottom_multib,sbottom_PFlowResearch,sbottom_multib_combi,sbottom_multib_combi_JES2,sbottom_multib_combi_JES3,sbottom_multib_smr,sbottom_multib_smr_noPhi,sbottom_multib_truth
    #import all stop analyses
    from ._stop import stop0L,stop0L_altJESOne,stop0L_altJESTwo,stop0L_photons,stop0L_taus,stop0L_pflow,stop0L_truth,stop0L_4body,stop0L_cutflow,stop0L_BFilter,stop0L_CVetoBVeto,stop0L_CFilterBVeto,stop0L_4body_BFilter,stop0L_4body_CVetoBVeto,stop0L_4body_CFilterBVeto,stop0L_comp,stop0L_altJESOne_BFilter,stop0L_altJESOne_CFilterBVeto,stop0L_altJESOne_CVetoBVeto,stop0L_4body_altJESOne,stop0L_4body_altJESOne_BFilter,stop0L_4body_altJESOne_CVetoBVeto,stop0L_4body_altJESOne_CFilterBVeto
    #import all bbMeT analyses
    from ._bbMeT import bbMeT,bbMeT_STDefault,bbMeT_truth,bbMeT_DL1,bbMeT_Hybrid,bbMeT_PFlow,bbMeT_bRES,bbMeT_PFlow_Loose,bbMeT_PFlow_Medium,bbMeT_PFlow_Tight
    #import all tcMeT analyses
    from ._tcMET import tcMET,tcMET_BFilter,tcMET_CVetoBVeto,tcMET_CFilterBVeto
    #import jet smearing rmap builders
    from ._JetSmearing import JSrmap,JSrmap_Rc0p6,JSrmap_Rc0p3,JSrmap_Rc0p3_pflow,JSrmap_Rc0p1
    #import jet smearing analyses
    from ._JetSmearing import JSnom,DiJetBalance,SMRDiJetBalance


    #save a list of the methods (analyses) we have loaded
    method_list=[mod for mod in dir() if not mod.startswith("__")]
    

    #define global variables for the analyses
    #triggerList=None
    doSyst=False

        
    #def setTriggerList(self,value):
    #    self.triggerList=value
    def setdoSyst(self,value):
        self.doSyst=value
    def setSystBunch(self,value):
        self.m_systBunch=value
    def getAllAnalyses(self):
        return sorted(self.method_list)
