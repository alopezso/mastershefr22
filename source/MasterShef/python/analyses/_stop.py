def stop0L_truth(self,alg):
    alg.m_config="MasterShef/confs/stop0LR21.conf"

    alg.m_doTruth=True
    #let's select at least 2 jets
    alg.m_userSelMods.push_back("SelJet")
    alg.m_SelJet_nJMn=2
    alg.m_SelJet_nJMx=1000

    #alg.printAllPossibleSelections()

    #select at least 1 b-jets
    alg.m_userSelMods.push_back("SelNBJet")
    alg.m_SelNBJet_nJMn=1
    alg.m_SelNBJet_nJMx=1000

    #selects >=1 bjets, >=3 leptons
    # alg.m_userSelMods.push_back("SelttZ3L")
    

    #alg.m_useAntiKtRcJets=True
    #alg.m_userTreeMods.push_back("fillRCFatJetVariables") 
    #alg.m_userTreeMods.push_back("fillTruthJetConstituents") 
    
    alg.m_useMCTruthClassifier=False
    
    alg.m_ntupLevel=7
    
    return alg
    


def stop0L(self,alg):
    alg.m_config="MasterShef/confs/stop0LR21.conf"
    # alg.m_config="MasterShef/confs/bbMeT/SUSYTools_Default_21_2_62.conf"

    # Only in the case of 1 lepton, treat the lepton as a jet.
    # WARNING : Skimming in MasterShef::SelStop0L uses nJets>=4 assuming leptons are
    # included in the signalJet collection. If you turn off this flag, make sure that
    # you don't need to change the 1Lep skimming.

    ## Trigger flags 
    # alg.m_useMETTrigj400CHECK=False


    ## Objects to be saved
    alg.m_doLepJet=True
    ## Filling the variables including leptons in jets
    if alg.m_doLepJet:
        alg.m_userTreeMods.push_back("fillLepJetVariables") 

    alg.m_useMCTruthClassifier=False
    # include Taus
    alg.m_useTaus=False
    alg.m_usePhotons=False
    #use track jets
    alg.m_useTrackJets=False

    #use fat jets in this analysis
    alg.m_useAntiKtRcJets=True
    #turn on substructure for RCJets
    alg.m_useRCJetSS=False
    alg.m_userTreeMods.push_back("fillRCFatJetVariables")

    alg.m_useAntiKt10Jets_RC=True
    alg.m_useAntiKt10Jets_ST=False
    
    ## Selection variables

    #make sure the fatjet variables are filled in the branches
    #alg.m_userTreeMods.push_back("fillFatJetVariables")
    
    #veto on cosmic muons in this analysis
    #alg.m_userSelMods.push_back("cosmic")
  
    #select at least 2 jets
    alg.m_userSelMods.push_back("SelJet")
    alg.m_SelJet_nJMn=1
    alg.m_SelJet_nJMx=1000


    #select at least 1 b-jets
    #        alg.m_userSelMods.push_back("SelNBJet")
    #        alg.m_SelNBJet_nJMn=1
    #alg.m_SelNBJet_nJMx=1000
    
   
    #fill some extra truth level information 
    #alg.m_AddRecoMuons=False
    alg.m_AddNeutMuTruthJets=False
    

    alg.m_userTreeMods.push_back("fillTruthJets") # separate function to fill truthJets(+neut, +muons)
#    alg.m_userTreeMods.push_back("fillTruthTaus") 
    alg.m_userTreeMods.push_back("fillTruthLeptons")

    alg.m_userSelMods.push_back("SelStop0L")
    alg.m_userSelMods.push_back("SelStop0LTight")
    #alg.m_SelMET_PtMn=200000.
    #alg.m_userSelMods.push_back("SelMET")    
    #alg.m_userTreeMods.push_back("fillJetConstituents")


    ## Systematics

    if (alg.m_dataSource!=0 and self.doSyst==True):
        
        ## Lepton systematics
        alg.m_systNameList.push_back("EG_RESOLUTION_ALL__1down")
        alg.m_systNameList.push_back("EG_RESOLUTION_ALL__1up")
        alg.m_systNameList.push_back("EG_SCALE_AF2__1down")
        alg.m_systNameList.push_back("EG_SCALE_AF2__1up")
        alg.m_systNameList.push_back("EG_SCALE_ALL__1down")
        alg.m_systNameList.push_back("EG_SCALE_ALL__1up")
        alg.m_systNameList.push_back("MUON_ID__1down")
        alg.m_systNameList.push_back("MUON_ID__1up")
        alg.m_systNameList.push_back("MUON_MS__1down")
        alg.m_systNameList.push_back("MUON_MS__1up")
        alg.m_systNameList.push_back("MUON_SAGITTA_RESBIAS__1down")
        alg.m_systNameList.push_back("MUON_SAGITTA_RESBIAS__1up")
        alg.m_systNameList.push_back("MUON_SAGITTA_RHO__1down")
        alg.m_systNameList.push_back("MUON_SAGITTA_RHO__1up")
        alg.m_systNameList.push_back("MUON_SCALE__1down")
        alg.m_systNameList.push_back("MUON_SCALE__1up")
        # alg.m_systNameList.push_back("TAUS_TRUEHADTAU_SME_TES_DETECTOR__1down")
        # alg.m_systNameList.push_back("TAUS_TRUEHADTAU_SME_TES_DETECTOR__1up")
        # alg.m_systNameList.push_back("TAUS_TRUEHADTAU_SME_TES_INSITU__1down")
        # alg.m_systNameList.push_back("TAUS_TRUEHADTAU_SME_TES_INSITU__1up")
        # alg.m_systNameList.push_back("TAUS_TRUEHADTAU_SME_TES_MODEL__1down")
        # alg.m_systNameList.push_back("TAUS_TRUEHADTAU_SME_TES_MODEL__1up")


        ## Jet systematics 

        # alg.m_systNameList.push_back("JET_JER_SINGLE_NP__1up") Removed with summer 2018 recommendations.         
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure__1up")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure__1down")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1up")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1down")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1up")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1down")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1up")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1down")


        # Using the strong reduction scheme (JES) + simple JER
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure__1up")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure__1down")

        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1up")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1down")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1up")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1down")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1up")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1down")
        alg.m_systNameList.push_back("JET_GroupedNP_1__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_1__1down")
        alg.m_systNameList.push_back("JET_GroupedNP_2__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_2__1down")
        alg.m_systNameList.push_back("JET_GroupedNP_3__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_3__1down")
        alg.m_systNameList.push_back("JET_Flavor_Response__1up")
        alg.m_systNameList.push_back("JET_Flavor_Response__1down")
        if alg.m_dataSource==2:
            alg.m_systNameList.push_back("JET_RelativeNonClosure_AFII__1up")
            alg.m_systNameList.push_back("JET_RelativeNonClosure_AFII__1down")
            alg.m_systNameList.push_back("JET_JER_DataVsMC_AFII__1up")
        #        alg.m_systNameList.push_back("JET_JER_DataVsMC_AFII__1down")
        else:
            alg.m_systNameList.push_back("JET_JER_DataVsMC_MC16__1up")
    #        alg.m_systNameList.push_back("JET_JER_DataVsMC_MC16__1down")

        alg.m_systNameList.push_back("JET_JER_EffectiveNP_1__1up")
        #        alg.m_systNameList.push_back("JET_JER_EffectiveNP_1__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_2__1up")
#        alg.m_systNameList.push_back("JET_JER_EffectiveNP_2__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_3__1up")
#        alg.m_systNameList.push_back("JET_JER_EffectiveNP_3__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_4__1up")
#        alg.m_systNameList.push_back("JET_JER_EffectiveNP_4__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_5__1up")
#        alg.m_systNameList.push_back("JET_JER_EffectiveNP_5__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_6__1up")
#        alg.m_systNameList.push_back("JET_JER_EffectiveNP_6__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_7restTerm__1up")
#        alg.m_systNameList.push_back("JET_JER_EffectiveNP_7restTerm__1down")

        ###### Using the CategoryReduction scheme (JES) + full JER
        # alg.m_systNameList.push_back("JET_EffectiveNP_Detector1__1up")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Detector1__1down")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Detector2__1up")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Detector2__1down")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Mixed1__1up")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Mixed1__1down")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Mixed2__1up")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Mixed2__1down")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Mixed3__1up")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Mixed3__1down")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Modelling1__1up")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Modelling1__1down")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Modelling2__1up")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Modelling2__1down")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Modelling3__1up")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Modelling3__1down")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Modelling4__1up")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Modelling4__1down")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Statistical1__1up")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Statistical1__1down")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Statistical2__1up")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Statistical2__1down")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Statistical3__1up")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Statistical3__1down")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Statistical4__1up")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Statistical4__1down")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Statistical5__1up")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Statistical5__1down")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Statistical6__1up")
        # alg.m_systNameList.push_back("JET_EffectiveNP_Statistical6__1down")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_Modelling__1up")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_Modelling__1down")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1up")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1down")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1up")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1down")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1up")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1down")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_TotalStat__1up")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_TotalStat__1down")
        # alg.m_systNameList.push_back("JET_Flavor_Composition__1up")
        # alg.m_systNameList.push_back("JET_Flavor_Composition__1down")
        # alg.m_systNameList.push_back("JET_Flavor_Response__1up")
        # alg.m_systNameList.push_back("JET_Flavor_Response__1down")
        # if alg.m_dataSource == 2:
        #     alg.m_systNameList.push_back("JET_JER_DataVsMC_AFII__1up")
        #     alg.m_systNameList.push_back("JET_JER_DataVsMC_AFII__1down")
        # else:
        #     alg.m_systNameList.push_back("JET_JER_DataVsMC_MC16__1up")
        #     alg.m_systNameList.push_back("JET_JER_DataVsMC_MC16__1down")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_1__1up")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_1__1down")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_2__1up")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_2__1down")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_3__1up")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_3__1down")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_4__1up")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_4__1down")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_5__1up")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_5__1down")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_6__1up")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_6__1down")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_7__1up")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_7__1down")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_8__1up")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_8__1down")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_9__1up")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_9__1down")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_10__1up")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_10__1down")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_11__1up")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_11__1down")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_12restTerm__1up")
        # alg.m_systNameList.push_back("JET_JER_EffectiveNP_12restTerm__1down")


        ## MET systematics

        alg.m_systNameList.push_back("MET_SoftTrk_ResoPara")
        alg.m_systNameList.push_back("MET_SoftTrk_ResoPerp")
        alg.m_systNameList.push_back("MET_SoftTrk_ScaleDown")
        alg.m_systNameList.push_back("MET_SoftTrk_ScaleUp")

    alg.m_ntupLevel=9
    alg.m_ntupLevel_sys=10
    

    return alg

def stop0L_altJESOne(self,alg):
    alg=self.stop0L(alg)
    alg.m_config="MasterShef/confs/stop0LR21.altJESOne.conf"
    alg.m_systNameList.clear()
    if (alg.m_dataSource!=0 and self.doSyst==True):
        alg.m_systNameList.push_back("JET_GroupedNP_1__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_1__1down")
        alg.m_systNameList.push_back("JET_GroupedNP_2__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_2__1down")
        alg.m_systNameList.push_back("JET_GroupedNP_3__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_3__1down")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1up")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1down")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1up")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1down")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1up")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1down")

    return alg
# next 3 cfgs are needed because MC16e Zvv is annoying with this PRW Filter... Really hope for a new derivation to fix this issue, but alas no.
def stop0L_altJESOne_BFilter(self,alg):
    alg=self.stop0L_altJESOne(alg)
    alg.m_config="MasterShef/confs/tt0L_altJES/stop0LR21.altJESOne.BFilter.conf"
    return alg

def stop0L_altJESOne_CVetoBVeto(self,alg):
    alg=self.stop0L_altJESOne(alg)
    alg.m_config="MasterShef/confs/tt0L_altJES/stop0LR21.altJESOne.CVetoBVeto.conf"
    return alg

def stop0L_altJESOne_CFilterBVeto(self,alg):
    alg=self.stop0L_altJESOne(alg)
    alg.m_config="MasterShef/confs/tt0L_altJES/stop0LR21.altJESOne.CFilterBVeto.conf"
    return alg


def stop0L_altJESTwo(self,alg):
    alg=self.stop0L(alg)
    alg.m_config="MasterShef/confs/stop0LR21.altJESTwo.conf"
    alg.m_systNameList.clear()
    if (alg.m_dataSource!=0 and self.doSyst==True):
        alg.m_systNameList.push_back("JET_GroupedNP_1__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_1__1down")
        alg.m_systNameList.push_back("JET_GroupedNP_2__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_2__1down")
        alg.m_systNameList.push_back("JET_GroupedNP_3__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_3__1down")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1up")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1down")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1up")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1down")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1up")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1down")

    return alg
        
    

def stop0L_comp(self,alg):
    alg=self.stop0L(alg)
    alg.m_config="MasterShef/confs/stop0LR21_comp.conf"
    alg.m_userTreeMods.push_back("fillTruthTaus") 
    alg.m_userTreeMods.push_back("fillTruthLeptons")
    alg.m_ntupLevel=8
    return alg

def stop0L_BFilter(self,alg):
    alg=self.stop0L(alg)
    alg.m_config="MasterShef/confs/stop0LR21.BFilter.conf"
    return alg

def stop0L_CVetoBVeto(self,alg):
    alg=self.stop0L(alg)
    alg.m_config="MasterShef/confs/stop0LR1.CVetoBVeto.conf"
    return alg

def stop0L_CFilterBVeto(self,alg):
    alg=self.stop0L(alg)
    alg.m_config="MasterShef/confs/stop0LR21.CFilterBVeto.conf"
    return alg

 
def stop0L_4body(self,alg):
    alg=self.stop0L(alg)
    alg.m_useTrackJets=True
    for i in range(alg.m_userSelMods.size()):
        if alg.m_userSelMods[i] == "SelStop0L":
            alg.m_userSelMods[i] = "SelStop4Body"
        elif alg.m_userSelMods[i] == "SelStop0LTight":
            alg.m_userSelMods[i]="SelStop4BodyTight"
    #use fat jets in this analysis                                                                                                          
    alg.m_SelJet_nJMn=1
                                                                        
    alg.m_useAntiKtRcJets=True
    alg.m_useRCJetSS=False
    index=0
    alg.m_SelJet_nJMn=1
    alg.m_SelJet_nJMx=1000

    return alg

def stop0L_4body_BFilter(self,alg):
    alg=self.stop0L_BFilter(alg)
    alg.m_useTrackJets=True
    for i in range(alg.m_userSelMods.size()):
        if alg.m_userSelMods[i] == "SelStop0L":
            alg.m_userSelMods[i] = "SelStop4Body"
        elif alg.m_userSelMods[i] == "SelStop0LTight":
            alg.m_userSelMods[i]="SelStop4BodyTight"
    #use fat jets in this analysis                                                                                                          
    alg.m_SelJet_nJMn=1
                                                                        
    alg.m_useAntiKtRcJets=True
    alg.m_useRCJetSS=False
    index=0
    alg.m_SelJet_nJMn=1
    alg.m_SelJet_nJMx=1000
    return alg

def stop0L_4body_CVetoBVeto(self,alg):
    alg=self.stop0L_CVetoBVeto(alg)
    alg.m_useTrackJets=True
    for i in range(alg.m_userSelMods.size()):
        if alg.m_userSelMods[i] == "SelStop0L":
            alg.m_userSelMods[i] = "SelStop4Body"
        elif alg.m_userSelMods[i] == "SelStop0LTight":
            alg.m_userSelMods[i]="SelStop4BodyTight"
    #use fat jets in this analysis                                                                                                          
    alg.m_SelJet_nJMn=1
                                                                        
    alg.m_useAntiKtRcJets=True
    alg.m_useRCJetSS=False
    index=0
    alg.m_SelJet_nJMn=1
    alg.m_SelJet_nJMx=1000
    return alg

def stop0L_4body_CFilterBVeto(self,alg):
    alg=self.stop0L_CFilterBVeto(alg)
    alg.m_useTrackJets=True
    for i in range(alg.m_userSelMods.size()):
        if alg.m_userSelMods[i] == "SelStop0L":
            alg.m_userSelMods[i] = "SelStop4Body"
        elif alg.m_userSelMods[i] == "SelStop0LTight":
            alg.m_userSelMods[i]="SelStop4BodyTight"
    #use fat jets in this analysis                                                                                                          
    alg.m_SelJet_nJMn=1
                                                                        
    alg.m_useAntiKtRcJets=True
    alg.m_useRCJetSS=False
    index=0
    alg.m_SelJet_nJMn=1
    alg.m_SelJet_nJMx=1000
    return alg

def stop0L_4body_altJESOne(self,alg):
    alg=self.stop0L_4body(alg)
    alg.m_config="MasterShef/confs/tt0L_altJES/stop0LR21.altJESOne.conf"
    alg.m_systNameList.clear()
    if (alg.m_dataSource!=0 and self.doSyst==True):
        alg.m_systNameList.push_back("JET_GroupedNP_1__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_1__1down")
        alg.m_systNameList.push_back("JET_GroupedNP_2__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_2__1down")
        alg.m_systNameList.push_back("JET_GroupedNP_3__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_3__1down")
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1up")                                                                                                                                                     
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1down")                                                                                                                                                   
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1up")                                                                                                                                                    
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1down")                                                                                                                                                  
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1up")                                                                                                                                                    
        # alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1down")   
    return alg

def stop0L_4body_altJESOne_BFilter(self,alg):
    alg=self.stop0L_4body_altJESOne(alg)
    alg.m_config="MasterShef/confs/tt0L_altJES/stop0LR21.altJESOne.BFilter.conf"
    return alg

def stop0L_4body_altJESOne_CVetoBVeto(self,alg):
    alg=self.stop0L_4body_altJESOne(alg)
    alg.m_config="Mastershef/confs/tt0L_altJES/stop0LR21.altJESOne.CVetoBVeto.conf"
    return alg

def stop0L_4body_altJESOne_CFilterBVeto(self,alg):
    alg=self.stop0L_4body_altJESOne(alg)
    alg.m_config="Mastershef/confs/tt0L_altJES/stop0LR21.altJESOne.CFilterBVeto.conf"
    return alg

def stop0L_photons(self,alg):
    alg.m_config="MasterShef/confs/stop0LR21_photons.conf"
    alg=self.stop0L(alg)
    alg.m_usePhotons=True
    alg.m_useMCTruthClassifier=True      
    return alg

    
def stop0L_taus(self,alg):
    alg=self.stop0L(alg)
    alg.m_useTaus=True
    alg.m_useMCTruthClassifier=True      
    return alg

def stop0L_pflow(self,alg):
    alg=self.stop0L(alg)
    alg.m_config="MasterShef/confs/stop0LR21_pflow.conf" 
    alg.m_isPflowResearch=True
    return alg


def stop0L_cutflow(self,alg):
    alg=self.stop0L(alg)
    alg.m_config="MasterShef/confs/stop0LR21.conf"
    alg.m_applyTrigger="stop0Ltrigs"
    alg.m_ntupLevel=8
    alg.m_ntupLevel_sys=8
    return alg



def stop0L_smr(self,alg):
        
    alg.m_useTaus=False
    alg.m_config="MasterShef/confs/stop0LR21.conf"    

    #sel 4 jets
    alg.m_userSelMods.push_back("SelJet")
    alg.m_SelJet_nJMn=4
    alg.m_SelJet_nJMx=1000

    #sel 3 b-jets
    alg.m_userSelMods.push_back("SelNBJet")
    alg.m_SelNBJet_nJMn=2
    alg.m_SelNBJet_nJMx=1000

    #sel 0 baseline leptons
    alg.m_userSelMods.push_back("SelNBaselineLep")
    alg.m_SelnBL_Mx=0
    alg.m_userSelMods.push_back("SelSeedEvents")
    alg.m_userSelMods.push_back("SelPseudoEvents")


    alg.m_nsmears=1000
    alg.m_doJetSmearing=True
    alg.m_doJetSmearingPhiCorrections=True
    alg.m_doJetSmearingSkim=True
    alg.m_useTriggerWeight=True
    alg.m_userTreeMods.push_back("fillSmearingVariables")

    #make sure reclustered rcjets are there
    alg.m_useAntiKtRcJets=True
    #save rc jets for the seed too
    alg.m_userTreeMods.push_back("fillRCFatJetVariables")    

    alg.m_ntupLevel=15
    alg.m_ntupLevel_sys=15
    return alg

