import ROOT

def JSnom(self,alg):
    alg.m_config="MasterShef/confs/JetSmearing/SUSYTools_Default.conf"
    
    alg.m_userSelMods.push_back("SelNBaselineLep")
    alg.m_SelnBL_Mn=0
    alg.m_SelnBL_Mx=0
    
    
    alg.m_userSelMods.push_back("SelJet")
    alg.m_SelJet_nJMn=0
    alg.m_SelJet_nJMx=10000
    alg.m_SelJet_ptMn.push_back(20000.)
    alg.m_SelJet_ptMx.push_back(1.0E08)
      
    alg.m_ntupLevel=6

  

    return alg



def SMRDiJetBalance(self,alg):

    alg.m_config="MasterShef/confs/JetSmearing/SUSYTools_Default.conf"
    alg.m_useTriggerWeight=True

   
    #select 0 baseline leptons
    alg.m_userSelMods.push_back("SelNBaselineLep")
    alg.m_SelnBL_Mn=0
    alg.m_SelnBL_Mx=0
    
    #select 2-4 jets to be smeared (since we might smear down 4 jets to 3)
    alg.m_userSelMods.push_back("SelJet")
    alg.m_SelJet_nJMn=2
    alg.m_SelJet_nJMx=4


    alg.m_userSelMods.push_back("SelSeedEvents")
    alg.m_userSelMods.push_back("SelPseudoEventsDiJet")


    alg.m_nsmears=100
    alg.m_doJetSmearing=True
    alg.m_doJetSmearingPhiCorrections=True
    alg.m_userTreeMods.push_back("fillSmearingVariables")

    #alg.m_userSelMods.push_back("SelDiJet")
 
    alg.m_ntupLevel=13


        
    return alg


def DiJetBalance(self,alg):

    alg.m_config="MasterShef/confs/JetSmearing/SUSYTools_Default.conf"
    alg.m_useTriggerWeight=True

    #select 0 baseline leptons
    alg.m_userSelMods.push_back("SelNBaselineLep")
    alg.m_SelnBL_Mn=0
    alg.m_SelnBL_Mx=0
    
    #select 2-3 jets
    alg.m_userSelMods.push_back("SelJet")
    alg.m_SelJet_nJMn=2
    alg.m_SelJet_nJMx=3
    #leading jet of pT>100 GeV
    alg.m_SelJet_ptMn.push_back(100000.)
    alg.m_SelJet_ptMx.push_back(1.0E08)
    
    #sub-leading jet of pT>50 GeV
    alg.m_SelJet_ptMn.push_back(50000.)
    alg.m_SelJet_ptMx.push_back(1.0E08)
    
    alg.m_userSelMods.push_back("SelDiJet")

    alg.m_ntupLevel=13
        
    return alg

    


def JSrmap(self,alg):
    alg.m_config="MasterShef/confs/JetSmearing/SUSYTools_Default.conf"
    alg.m_triggerListFile=""
    
    #set the matching criteria for truth-reco
    alg.m_TruthReco_dRMatching=0.3
    #set the uniqueness criteria for both reco and truth jets
    alg.m_unique_dRMatching=0.6
    
    alg.m_AddNeutMuTruthJets=True
    #alg.m_AddNeutMuTruthJets=False
    alg.m_AddRecoMuons=True
    alg.m_ConeSize=0.4

    #remove our normal default fills, we dont need them
    alg.m_userTreeMods.clear()
    #erase(alg.m_userTreeMods.begin(),alg.m_userTreeMods.begin()+3)

    ##make sure we will fill truth jets and reco jets for the rmap 
    alg.m_userTreeMods.push_back("fillTruthJets")
    alg.m_userTreeMods.push_back("fillRMap")
    #alg.m_userTreeMods.push_back("fillExtraMETVariables")
    

    ## remove the cleaning
    #alg.m_userSelMods.clear()
    #alg.m_userSelMods.shrink_to_fit()
    #alg.m_userSelMods.push_back("initialCuts")
    #alg.m_userSelMods.push_back("prepareObjects")
    
    
    #alg.m_userSelMods.push_back("SelNBaselineLep")
    #alg.m_SelnBL_Mn=0
    #alg.m_SelnBL_Mx=0
    
    
    # if options.doJetSmearing==True:
    #     alg.m_userSelMods.push_back("SelSeedEvents")
    #     alg.m_userSelMods.push_back("SelPseudoEvents")
    #     alg.m_ntupLevel=12
    #     return alg
    
    alg.m_userSelMods.push_back("SelJet")
    alg.m_SelJet_nJMn=1
    alg.m_SelJet_nJMx=10000
    alg.m_SelJet_ptMn.push_back(20000.)
    alg.m_SelJet_ptMx.push_back(1.0E08)
    
    
  
    alg.m_ntupLevel=8
    
    return alg
    
#define an analysis for RMaps with a matching cone size of 0.6
#when adding truth neutrinos and muons
def JSrmap_Rc0p6(self,alg):
    alg=self.JSrmap(alg)
    alg.m_TruthReco_dRMatching=0.6
    return alg


#define an analysis for RMaps with a matching cone size of 0.3
#when adding truth neutrinos and muons
def JSrmap_Rc0p3(self,alg):
    alg=self.JSrmap(alg)
    alg.m_TruthReco_dRMatching=0.3
    return alg

def JSrmap_Rc0p3_pflow(self,alg):
    alg=self.JSrmap(alg)
    alg.m_TruthReco_dRMatching=0.3
    alg.m_config="MasterShef/confs/JetSmearing/SUSYTools_Default_Pflow.conf"
    return alg



#define an analysis for RMaps with a matching cone size of 0.1
#when adding truth neutrinos and muons
def JSrmap_Rc0p1(self,alg):
    alg=self.JSrmap(alg)
    alg.m_TruthReco_dRMatching=0.1
    return alg
