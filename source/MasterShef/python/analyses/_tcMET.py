def tcMET(self,alg):
    alg.m_config="MasterShef/confs/tcMET_21-2-100.conf"
    #alg.m_config="MasterShef/confs/SUSYTools_Default.conf"
    # Only in the case of 1 lepton, treat the lepton as a jet.
    # WARNING : Skimming in MasterShef::SelStop0L uses nJets>=4 assuming leptons are
    # included in the signalJet collection. If you turn off this flag, make sure that
    # you don't need to change the 1Lep skimming.

    ## Trigger flags 
    # alg.m_useMETTrigj400CHECK=False

    ## Objects to be saved
    alg.m_doLepJet=False
    ## Filling the variables including leptons in jets
    if alg.m_doLepJet:
        alg.m_userTreeMods.push_back("fillLepJetVariables") 

    alg.m_useMCTruthClassifier=False
    # include Taus
    alg.m_useTaus=False
    alg.m_usePhotons=False
    #use track jets
    alg.m_useTrackJets=False
    alg.m_useElectrons = True ## Only for trigger samples by now
    alg.m_useMuons = True ## Only for trigger samples by now

    #use fat jets in this analysis
    alg.m_useAntiKtRcJets=False
    #turn on substructure for RCJets
    alg.m_useRCJetSS=False
    #alg.m_userTreeMods.push_back("fillRCFatJetVariables")

    alg.m_useAntiKt10Jets_RC=False
    alg.m_useAntiKt10Jets_ST=True
    alg.m_saveFatJetExtraVariables=True
        
    ## Selection variables

    #make sure the fatjet variables are filled in the branches
    alg.m_userTreeMods.push_back("fillFatJetVariables")
    
    #veto on cosmic muons in this analysis
    #alg.m_userSelMods.push_back("cosmic")
  
    #select at least 2 jets
    alg.m_userSelMods.push_back("SelJet")
    alg.m_SelJet_nJMn=1
    alg.m_SelJet_nJMx=1000


    #select at least 1 b-jets
    #        alg.m_userSelMods.push_back("SelNBJet")
    #        alg.m_SelNBJet_nJMn=1
    #alg.m_SelNBJet_nJMx=1000
    
   
    #fill some extra truth level information 
    #alg.m_AddRecoMuons=False
    alg.m_AddNeutMuTruthJets=False
    alg.m_skimPolicy="tcMET"

    alg.m_userTreeMods.push_back("fillTruthJets") # separate function to fill truthJets(+neut, +muons)
    alg.m_userTreeMods.push_back("fillTruthTaus") 
    alg.m_userTreeMods.push_back("fillTruthLeptons")

    alg.m_userSelMods.push_back("SeltcMET")
    alg.m_userSelMods.push_back("SeltcMETTight") # Tight selection to skim systematic trees.


    ## c-tagging settings while waiting some CDI
    alg.m_ctagging_fb = 0.28
    alg.m_ctagging_cut = 1.315


    ## Systematics

    ElectronSystematics = [
        "EG_RESOLUTION_ALL__1down",
        "EG_RESOLUTION_ALL__1up",
        "EG_SCALE_AF2__1down",
        "EG_SCALE_AF2__1up",
        "EG_SCALE_ALL__1down",
        "EG_SCALE_ALL__1up",
    ] 
    
    MuonSystematics = [ 
        "MUON_ID__1down",
        "MUON_ID__1up",
        "MUON_MS__1down",
        "MUON_MS__1up",
        "MUON_SAGITTA_RESBIAS__1down",
        "MUON_SAGITTA_RESBIAS__1up",
        "MUON_SAGITTA_RHO__1down",
        "MUON_SAGITTA_RHO__1up",
        "MUON_SCALE__1down",
        "MUON_SCALE__1up",
    ]

    TauSystematics=[
        "TAUS_TRUEHADTAU_SME_TES_DETECTOR__1down",
        "TAUS_TRUEHADTAU_SME_TES_DETECTOR__1up",
        "TAUS_TRUEHADTAU_SME_TES_INSITU__1down",
        "TAUS_TRUEHADTAU_SME_TES_INSITU__1up",
        "TAUS_TRUEHADTAU_SME_TES_MODEL__1down",
        "TAUS_TRUEHADTAU_SME_TES_MODEL__1up",
    ]

    JetSimpleJERSystematics = [
        "JET_JER_EffectiveNP_1__1up",
        "JET_JER_EffectiveNP_2__1up",
        "JET_JER_EffectiveNP_3__1up",
        "JET_JER_EffectiveNP_4__1up",
        "JET_JER_EffectiveNP_5__1up",
        "JET_JER_EffectiveNP_6__1up",
        "JET_JER_EffectiveNP_7restTerm__1up",
    ]

    JetFullJERSystematics = [
        "JET_JER_EffectiveNP_1__1up",
        "JET_JER_EffectiveNP_2__1up",
        "JET_JER_EffectiveNP_3__1up",
        "JET_JER_EffectiveNP_4__1up",
        "JET_JER_EffectiveNP_5__1up",
        "JET_JER_EffectiveNP_6__1up",
        "JET_JER_EffectiveNP_7restTerm__1up",
        "JET_JER_EffectiveNP_7__1up",
        "JET_JER_EffectiveNP_8__1up",
        "JET_JER_EffectiveNP_9__1up",
        "JET_JER_EffectiveNP_10__1up",
        "JET_JER_EffectiveNP_11__1up",
        "JET_JER_EffectiveNP_12restTerm__1up"
    ]
    JetPDSmearSystematics = [
        "JET_JER_EffectiveNP_1__2up",
        "JET_JER_EffectiveNP_2__2up",
        "JET_JER_EffectiveNP_3__2up",
        "JET_JER_EffectiveNP_4__2up",
        "JET_JER_EffectiveNP_5__2up",
        "JET_JER_EffectiveNP_6__2up",
        "JET_JER_EffectiveNP_7__2up",
        "JET_JER_EffectiveNP_8__2up",
        "JET_JER_EffectiveNP_9__2up",
        "JET_JER_EffectiveNP_10__2up",
        "JET_JER_EffectiveNP_11__2up",
        "JET_JER_EffectiveNP_12restTerm__2up",
    ]

    JetStrongRedJESSystematics = [
        #"JET_EtaIntercalibration_NonClosure__1up",
        #"JET_EtaIntercalibration_NonClosure__1down",
        "JET_EtaIntercalibration_Modelling__1up",
        "JET_EtaIntercalibration_Modelling__1down",
        "JET_EtaIntercalibration_NonClosure_highE__1up",
        "JET_EtaIntercalibration_NonClosure_highE__1down",
        "JET_EtaIntercalibration_NonClosure_negEta__1up",
        "JET_EtaIntercalibration_NonClosure_negEta__1down",
        "JET_EtaIntercalibration_NonClosure_posEta__1up",
        "JET_EtaIntercalibration_NonClosure_posEta__1down",
        "JET_EtaIntercalibration_NonClosure_2018data__1up",
        "JET_EtaIntercalibration_NonClosure_2018data__1down",
        "JET_GroupedNP_1__1up",
        "JET_GroupedNP_1__1down",
        "JET_GroupedNP_2__1up",
        "JET_GroupedNP_2__1down",
        "JET_GroupedNP_3__1up",
        "JET_GroupedNP_3__1down",
        "JET_Flavor_Response__1up",
        "JET_Flavor_Response__1down",
        "JET_Flavor_Composition__1up",
        "JET_Flavor_Composition__1down",
    ]


    JetCategoryJESSystematics = [
        "JET_EffectiveNP_Detector1__1up",
        "JET_EffectiveNP_Detector1__1down",
        "JET_EffectiveNP_Detector2__1up",
        "JET_EffectiveNP_Detector2__1down",
        "JET_EffectiveNP_Mixed1__1up",
        "JET_EffectiveNP_Mixed1__1down",
        "JET_EffectiveNP_Mixed2__1up",
        "JET_EffectiveNP_Mixed2__1down",
        "JET_EffectiveNP_Mixed3__1up",
        "JET_EffectiveNP_Mixed3__1down",
        "JET_EffectiveNP_Modelling1__1up",
        "JET_EffectiveNP_Modelling1__1down",
        "JET_EffectiveNP_Modelling2__1up",
        "JET_EffectiveNP_Modelling2__1down",
        "JET_EffectiveNP_Modelling3__1up",
        "JET_EffectiveNP_Modelling3__1down",
        "JET_EffectiveNP_Modelling4__1up",
        "JET_EffectiveNP_Modelling4__1down",
        "JET_EffectiveNP_Statistical1__1up",
        "JET_EffectiveNP_Statistical1__1down",
        "JET_EffectiveNP_Statistical2__1up",
        "JET_EffectiveNP_Statistical2__1down",
        "JET_EffectiveNP_Statistical3__1up",
        "JET_EffectiveNP_Statistical3__1down",
        "JET_EffectiveNP_Statistical4__1up",
        "JET_EffectiveNP_Statistical4__1down",
        "JET_EffectiveNP_Statistical5__1up",
        "JET_EffectiveNP_Statistical5__1down",
        "JET_EffectiveNP_Statistical6__1up",
        "JET_EffectiveNP_Statistical6__1down",
        "JET_EtaIntercalibration_Modelling__1up",
        "JET_EtaIntercalibration_Modelling__1down",
        "JET_EtaIntercalibration_NonClosure_highE__1up",
        "JET_EtaIntercalibration_NonClosure_highE__1down",
        "JET_EtaIntercalibration_NonClosure_negEta__1up",
        "JET_EtaIntercalibration_NonClosure_negEta__1down",
        "JET_EtaIntercalibration_NonClosure_posEta__1up",
        "JET_EtaIntercalibration_NonClosure_posEta__1down",
        "JET_EtaIntercalibration_TotalStat__1up",
        "JET_EtaIntercalibration_TotalStat__1down",
        "JET_EtaIntercalibration_NonClosure_2018data__1up",
        "JET_EtaIntercalibration_NonClosure_2018data__1down",
        "JET_Flavor_Composition__1up",
        "JET_Flavor_Composition__1down",
        "JET_Flavor_Response__1up",
        "JET_Flavor_Response__1down",
    ]

    MetSystematics = [
        "MET_SoftTrk_ResoPara",
        "MET_SoftTrk_ResoPerp",
        "MET_SoftTrk_Scale__1down",
        "MET_SoftTrk_Scale__1up",
    ]

    LargeRSystematics = [
        "JET_EtaIntercalibration_R10_TotalStat__1up",
        "JET_EtaIntercalibration_R10_TotalStat__1down",
        #"JET_EtaIntercalibration_Modelling", ## Correlated with small-R
        #"JET_Flavor_Composition", ## Correlated with small-R
        #"JET_Flavor_Response", ## Correlated with small-R
        "JET_EffectiveNP_R10_1__1up",
        "JET_EffectiveNP_R10_2__1up",
        "JET_EffectiveNP_R10_3__1up",
        "JET_EffectiveNP_R10_4__1up",
        "JET_EffectiveNP_R10_5__1up",
        "JET_EffectiveNP_R10_6restTerm__1up",
        #"JET_Rtrk_Baseline_mass__1up",
        #"JET_Rtrk_Baseline_mass__1down",
        #"JET_Rtrk_Baseline_pT__1up",
        #"JET_Rtrk_Baseline_pT__1down",
        #"JET_Rtrk_Closure_mass__1up",
        #"JET_Rtrk_Closure_mass__1down",
        #"JET_Rtrk_Closure_pT__1up",
        #"JET_Rtrk_Closure_pT__1down",
        #"JET_Rtrk_Modelling_mass__1up",
        #"JET_Rtrk_Modelling_mass__1down",
        #"JET_Rtrk_Modelling_pT__1up",
        #"JET_Rtrk_Modelling_pT__1down",
        #"JET_Rtrk_TotalStat_mass__1up",
        #"JET_Rtrk_TotalStat_mass__1down",
        #"JET_Rtrk_TotalStat_pT__1up",
        #"JET_Rtrk_TotalStat_pT__1down",
        #"JET_Rtrk_Tracking_mass__1up",
        #"JET_Rtrk_Tracking_mass__1down",
        #"JET_Rtrk_Tracking_pT__1up",
        #"JET_Rtrk_Tracking_pT__1down",
        "JET_SingleParticle_HighPt__1up",
        "JET_SingleParticle_HighPt__1down",
        "JET_LargeR_TopologyUncertainty_V__1up",
        "JET_LargeR_TopologyUncertainty_V__1down",
        "JET_LargeR_TopologyUncertainty_top__1up",
        "JET_LargeR_TopologyUncertainty_top__1down",
        #"JET_EtaIntercalibration_NonClosure_2018data__1up",
        #"JET_EtaIntercalibration_NonClosure_2018data__1down",
        "JET_CombMass_Baseline__1up",
        "JET_CombMass_Baseline__1down",
        "JET_CombMass_Modelling__1up",
        "JET_CombMass_Modelling__1down",
        "JET_CombMass_Tracking1__1up",
        "JET_CombMass_Tracking1__1down",
        "JET_CombMass_Tracking2__1up",
        "JET_CombMass_Tracking2__1down",
        "JET_CombMass_Tracking3__1up",
        "JET_CombMass_Tracking3__1down",
        "JET_CombMass_TotalStat__1up",
        "JET_CombMass_TotalStat__1down",
        "JET_MassRes_Top_comb__1up",
        "JET_MassRes_Top_comb__1down",
        "JET_MassRes_WZ_comb__1up",
        "JET_MassRes_WZ_comb__1down",
        "JET_MassRes_Hbb_comb__1up",
        "JET_MassRes_Hbb_comb__1down",
    ]
    LargeRJetConsolidated = [
        "JET_EtaIntercalibration_R10_TotalStat__1up",
        "JET_EtaIntercalibration_R10_TotalStat__1down",
        #"JET_EtaIntercalibration_Modelling", ## Correlated with small-R
        #"JET_Flavor_Composition", ## Correlated with small-R
        #"JET_Flavor_Response", ## Correlated with small-R
        "JET_EffectiveNP_R10_1__1up",
        "JET_EffectiveNP_R10_2__1up",
        "JET_EffectiveNP_R10_3__1up",
        "JET_EffectiveNP_R10_4__1up",
        "JET_EffectiveNP_R10_5__1up",
        "JET_EffectiveNP_R10_6restTerm__1up",
        "JET_EffectiveNP_R10_Mixed1__1up",
        "JET_EffectiveNP_R10_Mixed1__1down",
        "JET_EffectiveNP_R10_Modelling1__1up",
        "JET_EffectiveNP_R10_Modelling1__1down",
        "JET_JMS_FF_AllOthers__1up",
        "JET_JMS_FF_AllOthers__1down",
        "JET_JMS_FF_InterpolationDifference__1up",
        "JET_JMS_FF_InterpolationDifference__1down",
        "JET_JMS_FF_LargerSample__1up",
        "JET_JMS_FF_LargerSample__1down",
        "JET_JMS_FF_MatrixElement__1up",
        "JET_JMS_FF_MatrixElement__1down",
        "JET_JMS_FF_PartonShower__1up",
        "JET_JMS_FF_PartonShower__1down",
        "JET_JMS_FF_Shape__1up",
        "JET_JMS_FF_Shape__1down",
        "JET_JMS_FF_Stat__1up",
        "JET_JMS_FF_Stat__1down",
        "JET_JMS_Rtrk_Generator__1up",
        "JET_JMS_Rtrk_Generator__1down",
        "JET_JMS_Rtrk_Generator_InterpolationDifference__1up",
        "JET_JMS_Rtrk_Generator_InterpolationDifference__1down",
        "JET_JMS_Rtrk_InterpolationDifference__1up",
        "JET_JMS_Rtrk_InterpolationDifference__1down",
        "JET_JMS_Rtrk_Stat1__1up",
        "JET_JMS_Rtrk_Stat1__1down",
        "JET_JMS_Rtrk_Stat2__1up",
        "JET_JMS_Rtrk_Stat2__1down",
        "JET_JMS_Rtrk_Stat3__1up",
        "JET_JMS_Rtrk_Stat3__1down",
        "JET_JMS_Rtrk_Stat4__1up",
        "JET_JMS_Rtrk_Stat4__1down",
        "JET_JMS_Rtrk_Stat5__1up",
        "JET_JMS_Rtrk_Stat5__1down",
        "JET_JMS_Rtrk_Stat6__1up",
        "JET_JMS_Rtrk_Stat6__1down",
        "JET_JMS_Rtrk_Tracking__1up",
        "JET_JMS_Rtrk_Tracking__1down",
        "JET_JMS_Topology_QCD__1up",
        "JET_JMS_Topology_QCD__1down",
    ]
    TopTagSystematics = [
        "JET_JetTagSF_Dijet_Modelling__1up",
        "JET_JetTagSF_Dijet_Modelling__1down",
        "JET_JetTagSF_Gammajet_Modelling__1up",
        "JET_JetTagSF_Gammajet_Modelling__1down",
        "JET_JetTagSF_Hadronisation__1up",
        "JET_JetTagSF_Hadronisation__1down",
        "JET_JetTagSF_MatrixElement__1up",
        "JET_JetTagSF_MatrixElement__1down",
        "JET_JetTagSF_Radiation__1up",
        "JET_JetTagSF_Radiation__1down",
        "JET_TopTagInclusive_SigEff80_BGSF_Dijet_Stat__1up",
        "JET_TopTagInclusive_SigEff80_BGSF_Dijet_Stat__1down",
        "JET_TopTagInclusive_SigEff80_BGSF_Gammajet_Stat__1up",
        "JET_TopTagInclusive_SigEff80_BGSF_Gammajet_Stat__1down",
        "JET_TopTagInclusive_SigEff80_BGSF_Propagated_AllOthers__1up",
        "JET_TopTagInclusive_SigEff80_BGSF_Propagated_AllOthers__1down",
        "JET_TopTagInclusive_SigEff80_SigSF_BinVariation__1up",
        "JET_TopTagInclusive_SigEff80_SigSF_BinVariation__1down",
        "JET_TopTagInclusive_SigEff80_SigSF_ExtrapolationPt__1up",
        "JET_TopTagInclusive_SigEff80_SigSF_ExtrapolationPt__1down",
        "JET_TopTagInclusive_SigEff80_SigSF_Propagated_AllOthers__1up",
        "JET_TopTagInclusive_SigEff80_SigSF_Propagated_AllOthers__1down",
        "JET_TopTagInclusive_SigEff80_SigSF_Statistics__1up",
        "JET_TopTagInclusive_SigEff80_SigSF_Statistics__1down",
        "JET_TopTagInclusive_SigEff80_TagEffUnc_GlobalBackground__1up",
        "JET_TopTagInclusive_SigEff80_TagEffUnc_GlobalBackground__1down",
        "JET_TopTagInclusive_SigEff80_TagEffUnc_GlobalOther__1up",
        "JET_TopTagInclusive_SigEff80_TagEffUnc_GlobalOther__1down",
        "JET_TopTagInclusive_SigEff80_TagEffUnc_GlobalSignal__1up",
        "JET_TopTagInclusive_SigEff80_TagEffUnc_GlobalSignal__1down",
    ]


    if alg.m_dataSource==2:
        JetCategoryJESSystematics.extend(["JET_PunchThrough_AFII__1up","JET_PunchThrough_AFII__1down","JET_RelativeNonClosure_AFII__1up","JET_RelativeNonClosure_AFII__1down"])
        JetSimpleJERSystematics.extend(["JET_JER_DataVsMC_AFII__1up","JET_JER_DataVsMC_AFII__1down"])
        JetFullJERSystematics.extend(["JET_JER_DataVsMC_AFII__1up","JET_JER_DataVsMC_AFII__1down"])
        JetPDSmearSystematics.extend(["JET_JER_DataVsMC_AFII__2up","JET_JER_DataVsMC_AFII__2down"])
    else:
        JetCategoryJESSystematics.extend(["JET_PunchThrough_MC16__1up","JET_PunchThrough_MC16__1down"])
        JetSimpleJERSystematics.extend(["JET_JER_DataVsMC_MC16__1up","JET_JER_DataVsMC_MC16__1down"])
        JetFullJERSystematics.extend(["JET_JER_DataVsMC_MC16__1up","JET_JER_DataVsMC_MC16__1down"])
        JetPDSmearSystematics.extend(["JET_JER_DataVsMC_MC16__2up","JET_JER_DataVsMC_MC16__2down"])

    ListOfSystematics = ElectronSystematics + MuonSystematics + JetStrongRedJESSystematics + LargeRSystematics + MetSystematics + JetSimpleJERSystematics
    ListOfSystematics = TopTagSystematics + LargeRJetConsolidated
    #+ TopTagSystematics
    CategorisedSystematics = {}
    NumberSystDivisions = 9 
    SystPerDivision=int(len(ListOfSystematics)/NumberSystDivisions)

    print( " List of systematics has a total of %d objects. Dividing into %d chunks for processing "%(len(ListOfSystematics),NumberSystDivisions) ) 

    # Categorizing the systematics list
    count = 0 
    for i in range(0, len(ListOfSystematics), SystPerDivision):  
        CategorisedSystematics[count] = ListOfSystematics[i:i + SystPerDivision]
        count += 1 
    NumberSystDivisions = len(CategorisedSystematics.keys())

    alg.m_systdivisions = NumberSystDivisions

    print(" Requesting systematic category %d"%alg.m_systBunch)
    ## Adding systematics to the output list in case systematics are requested
    if (alg.m_dataSource!=0 and self.doSyst==True):

        if alg.m_systBunch == -1:
            for syst in ListOfSystematics:
                alg.m_systNameList.push_back(syst)
        elif alg.m_systBunch < -1:
            print(" Systematic categories start at -1 (all systematics included) or are in %s. Exiting."%CategorisedSystematics.keys() )
            exit(-1)
        elif alg.m_systBunch >= NumberSystDivisions:
            print(" Requesting a set of systematics that is beyond the number of categories defined %s. Exiting."%CategorisedSystematics.keys())
            exit(-1)
        else:
            for syst in CategorisedSystematics[alg.m_systBunch]:
                alg.m_systNameList.push_back(syst)

    alg.m_ntupLevel=9
    alg.m_ntupLevel_sys=10
    

    return alg

def tcMET_BFilter(self,alg):
    alg=self.tcMET(alg)
    alg.m_config="MasterShef/confs/tcMET_21-2-100.BFilter.conf"
    return alg

def tcMET_CVetoBVeto(self,alg):
    alg=self.tcMET(alg)
    alg.m_config="MasterShef/confs/tcMET_21-2-100.CVetoBVeto.conf"
    return alg

def tcMET_CFilterBVeto(self,alg):
    alg=self.tcMET(alg)
    alg.m_config="MasterShef/confs/tcMET_21-2-100.CFilterBVeto.conf"
    return alg

 
