import os
#----------------------------------------------------
#class for the nominal sbottom analysis
#----------------------------------------------------
def sbottom(self,alg):

    alg.m_saveExperimentalSyst=False
   
    #change the trigger list
    #alg.m_triggerListFile=os.environ[alg.m_basePath[1:]]+"/data/MasterShef/trigger_lists/simple_triggers.txt"
    #turn off saving triggers. The passMETTrigger variable will still be saved however!
    alg.m_triggerListFile=""
    #location of the SUSYTools config file
    alg.m_config="MasterShef/confs/sbottom.conf"

    #add a complex custom selection on 
    alg.m_userSelMods.push_back("SelSbottom") #level 8
    
    #add a selection on 0 baseline leptons
    alg.m_SelnBL_Mn=0
    alg.m_SelnBL_Mx=0
    alg.m_userSelMods.push_back("SelNBaselineLep") #level 9
    
    #add a selection on the number of jets
    alg.m_SelJet_nJMn=2
    alg.m_SelJet_nJMx=1000
    alg.m_userSelMods.push_back("SelJet") #level 10

    #add a selection on the number of bjets
    alg.m_SelNBJet_nJMn=2
    alg.m_userSelMods.push_back("SelNBJet") #level 11-12
    
    #add a selection on MET
    alg.m_SelMET_PtMn=250000.
    alg.m_userSelMods.push_back("SelMET") #level 13
    
    #set the level to number the nominal tree at the SelSbottom
    #this is cut level 8 in this case
    alg.m_ntupLevel=13
    
    #likewise for the systematic trees. These could be dumped at a tighter selection
    alg.m_ntupLevel_sys=13


    #turn of using RCjets
    alg.m_useAntiKtRcJets=True
    #turn on RCjets sub-structure
    m_useRCJetSS=True
    alg.m_userTreeMods.push_back("fillRCFatJetVariables")


    return alg




def sbottom_multib_truth(self,alg):
        
    alg.m_doTruth=True
    alg.m_config="MasterShef/confs/sbottom_multib_combi.conf" 
    

    alg.m_config="MasterShef/confs/sbottom_multib_combi.conf" 
    
    #sel 250 GeV MET
    alg.m_userSelMods.push_back("SelMET")
    alg.m_SelMET_PtMn=250000.
    
    #sel 4 jets
    alg.m_userSelMods.push_back("SelJet")
    alg.m_SelJet_nJMn=4
    alg.m_SelJet_nJMx=1000



    #sel 3 b-jets
    alg.m_userSelMods.push_back("SelNBJet")
    alg.m_SelNBJet_nJMn=3



    alg.m_ntupLevel=7
    
    return alg
    




def sbottom_multib_smr(self,alg):
        
    alg.m_useTaus=False
    alg.m_config="MasterShef/confs/sbottom_multib_combi.conf" 
    
    #sel 4 jets
    alg.m_userSelMods.push_back("SelJet")
    alg.m_SelJet_nJMn=4
    alg.m_SelJet_nJMx=1000

    #sel 3 b-jets
    alg.m_userSelMods.push_back("SelNBJet")
    alg.m_SelNBJet_nJMn=3
    alg.m_SelNBJet_nJMx=1000

    #sel 0 baseline leptons
    alg.m_userSelMods.push_back("SelNBaselineLep")
    alg.m_SelnBL_Mx=0
    alg.m_userSelMods.push_back("SelSeedEvents")
    alg.m_userSelMods.push_back("SelPseudoEvents")


    alg.m_nsmears=1000
    alg.m_doJetSmearing=True
    alg.m_doJetSmearingPhiCorrections=True
    alg.m_doJetSmearingSkim=True
    alg.m_useTriggerWeight=True
    alg.m_userTreeMods.push_back("fillSmearingVariables")



    alg.m_ntupLevel=15
    alg.m_ntupLevel_sys=15
    return alg

def sbottom_multib_smr_noPhi(self,alg):
    alg=self.sbottom_multib_smr(alg)
    alg.m_doJetSmearingPhiCorrections=False
    return alg

def sbottom_multib(self,alg):
    alg.m_useAntiKtRcJets=False
    alg.m_useAntiKt10Jets=False
    alg.m_userTreeMods.push_back("fillExtraRecoVariables")
    # alg.m_userTreeMods.push_back("fillFatJetVariables")
    
    alg.m_useTaus=False
    
    alg.m_useMCTruthClassifier=True
    alg.m_userTreeMods.push_back("fillTruthJets")
    alg.m_AddNeutMuTruthJets=True
    
    alg.m_config="MasterShef/confs/sbottom_multib_fixed.conf" 
    
    #turn on an option to do the b-jet resolution inflation
    alg.m_doBJetInflation=True

    #sel 250 GeV MET
    alg.m_userSelMods.push_back("SelMET")
    alg.m_SelMET_PtMn=250000.
    
    #sel 4 jets
    alg.m_userSelMods.push_back("SelJet")
    alg.m_SelJet_nJMn=4
    alg.m_SelJet_nJMx=1000

    #use a custom complex filter
    # currently two step filter:
    # loose selection (0-2 lep, >=2 bjets)
    # tight selection (>=3 bjets || high metsig)
    alg.m_userSelMods.push_back("SelSbottomMultib")
    
    #sel 3 b-jets
    # alg.m_userSelMods.push_back("SelNBJet")
    # alg.m_SelNBJet_nJMn=3
    # alg.m_SelNBJet_nJMx=1000

    # # sel 0 signal leptons
    # alg.m_userSelMods.push_back("Sel0Lep")
    
    # #sel 0 baseline leptons
    # alg.m_userSelMods.push_back("SelNBaselineLep")
    # alg.m_SelnBL_Mx=0
    
    if (alg.m_dataSource!=0 and self.doSyst==True):

        alg.m_systNameList.push_back("EG_RESOLUTION_ALL__1down")
        alg.m_systNameList.push_back("EG_RESOLUTION_ALL__1up")
        alg.m_systNameList.push_back("EG_SCALE_AF2__1down")
        alg.m_systNameList.push_back("EG_SCALE_AF2__1up")
        alg.m_systNameList.push_back("EG_SCALE_ALL__1down")
        alg.m_systNameList.push_back("EG_SCALE_ALL__1up")
        
        alg.m_systNameList.push_back("MUON_ID__1down")
        alg.m_systNameList.push_back("MUON_ID__1up")
        alg.m_systNameList.push_back("MUON_MS__1down")
        alg.m_systNameList.push_back("MUON_MS__1up")
        alg.m_systNameList.push_back("MUON_SAGITTA_RESBIAS__1down")
        alg.m_systNameList.push_back("MUON_SAGITTA_RESBIAS__1up")
        alg.m_systNameList.push_back("MUON_SAGITTA_RHO__1down")
        alg.m_systNameList.push_back("MUON_SAGITTA_RHO__1up")
        alg.m_systNameList.push_back("MUON_SCALE__1down")
        alg.m_systNameList.push_back("MUON_SCALE__1up")
        
        alg.m_systNameList.push_back("MET_SoftTrk_ResoPara")
        alg.m_systNameList.push_back("MET_SoftTrk_ResoPerp")
        alg.m_systNameList.push_back("MET_SoftTrk_ScaleDown")
        alg.m_systNameList.push_back("MET_SoftTrk_ScaleUp")
        
        alg.m_systNameList.push_back("JET_GroupedNP_1__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_1__1down")
        alg.m_systNameList.push_back("JET_GroupedNP_2__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_2__1down")
        alg.m_systNameList.push_back("JET_GroupedNP_3__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_3__1down")
        
        alg.m_systNameList.push_back("JET_JER_DataVsMC__1up")
        alg.m_systNameList.push_back("JET_JER_DataVsMC__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_1__1up")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_1__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_2__1up")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_2__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_3__1up")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_3__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_4__1up")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_4__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_5__1up")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_5__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_6__1up")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_6__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_7restTerm__1up")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_7restTerm__1down")
        
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1up")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1down")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1up")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1down")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1up")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1down")
        alg.m_systNameList.push_back("JET_Flavor_Response__1up")
        alg.m_systNameList.push_back("JET_Flavor_Response__1down")
            
       
    alg.m_ntupLevel=10
    alg.m_ntupLevel_sys=11
    return alg


def sbottom_multib_combi(self,alg):
    alg.m_useAntiKtRcJets=False
    alg.m_useAntiKt10Jets=False
    alg.m_userTreeMods.push_back("fillExtraRecoVariables")
    # alg.m_userTreeMods.push_back("fillFatJetVariables")
    alg.m_useTaus=False
    alg.m_useMCTruthClassifier=True
    alg.m_userTreeMods.push_back("fillTruthJets")
    alg.m_AddNeutMuTruthJets=True
    
    alg.m_config="MasterShef/confs/sbottom_multib_combi.conf" 
    
    #sel 250 GeV MET
    alg.m_userSelMods.push_back("SelMET")
    alg.m_SelMET_PtMn=250000.
    
    #sel 4 jets
    alg.m_userSelMods.push_back("SelJet")
    alg.m_SelJet_nJMn=4
    alg.m_SelJet_nJMx=1000

    #use a custom complex filter
    # currently two step filter:
    # loose selection (0-2 lep, >=2 bjets)
    # tight selection (>=3 bjets || high metsig)
    alg.m_userSelMods.push_back("SelSbottomMultib")

    if (alg.m_dataSource!=0 and self.doSyst==True):

        alg.m_systNameList.push_back("EG_RESOLUTION_ALL__1down")
        alg.m_systNameList.push_back("EG_RESOLUTION_ALL__1up")
        alg.m_systNameList.push_back("EG_SCALE_AF2__1down")
        alg.m_systNameList.push_back("EG_SCALE_AF2__1up")
        alg.m_systNameList.push_back("EG_SCALE_ALL__1down")
        alg.m_systNameList.push_back("EG_SCALE_ALL__1up")
        
        alg.m_systNameList.push_back("MUON_ID__1down")
        alg.m_systNameList.push_back("MUON_ID__1up")
        alg.m_systNameList.push_back("MUON_MS__1down")
        alg.m_systNameList.push_back("MUON_MS__1up")
        alg.m_systNameList.push_back("MUON_SAGITTA_RESBIAS__1down")
        alg.m_systNameList.push_back("MUON_SAGITTA_RESBIAS__1up")
        alg.m_systNameList.push_back("MUON_SAGITTA_RHO__1down")
        alg.m_systNameList.push_back("MUON_SAGITTA_RHO__1up")
        alg.m_systNameList.push_back("MUON_SCALE__1down")
        alg.m_systNameList.push_back("MUON_SCALE__1up")
        
        alg.m_systNameList.push_back("MET_SoftTrk_ResoPara")
        alg.m_systNameList.push_back("MET_SoftTrk_ResoPerp")
        alg.m_systNameList.push_back("MET_SoftTrk_ScaleDown")
        alg.m_systNameList.push_back("MET_SoftTrk_ScaleUp")
        
        alg.m_systNameList.push_back("JET_GroupedNP_1__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_1__1down")
        alg.m_systNameList.push_back("JET_GroupedNP_2__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_2__1down")
        alg.m_systNameList.push_back("JET_GroupedNP_3__1up")
        alg.m_systNameList.push_back("JET_GroupedNP_3__1down")
       
        alg.m_systNameList.push_back("JET_JER_DataVsMC__1up")
        alg.m_systNameList.push_back("JET_JER_DataVsMC__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_1__1up")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_1__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_2__1up")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_2__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_3__1up")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_3__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_4__1up")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_4__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_5__1up")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_5__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_6__1up")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_6__1down")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_7restTerm__1up")
        alg.m_systNameList.push_back("JET_JER_EffectiveNP_7restTerm__1down")
        
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1up")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_highE__1down")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1up")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_negEta__1down")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1up")
        alg.m_systNameList.push_back("JET_EtaIntercalibration_NonClosure_posEta__1down")
        alg.m_systNameList.push_back("JET_Flavor_Response__1up")
        alg.m_systNameList.push_back("JET_Flavor_Response__1down")
    
    alg.m_ntupLevel=10
    alg.m_ntupLevel_sys=11
    return alg


def sbottom_multib_combi_JES2(self,alg):
    #clone the combi
    alg=self.sbottom_multib_combi(alg)
    alg.m_config="MasterShef/confs/sbottom_multib_combi_JES2.conf"
    
    alg.m_systNameList.clear()
    alg.m_systNameList.push_back("JET_GroupedNP_1__1up")
    alg.m_systNameList.push_back("JET_GroupedNP_1__1down")
    alg.m_systNameList.push_back("JET_GroupedNP_2__1up")
    alg.m_systNameList.push_back("JET_GroupedNP_2__1down")
    alg.m_systNameList.push_back("JET_GroupedNP_3__1up")
    alg.m_systNameList.push_back("JET_GroupedNP_3__1down")

    alg.m_ntupLevel=11
    
    return alg

def sbottom_multib_combi_JES3(self,alg):
    #clone the combi
    alg=self.sbottom_multib_combi(alg)
    alg.m_config="MasterShef/confs/sbottom_multib_combi_JES3.conf"
    
    alg.m_systNameList.clear()
    alg.m_systNameList.push_back("JET_GroupedNP_1__1up")
    alg.m_systNameList.push_back("JET_GroupedNP_1__1down")
    alg.m_systNameList.push_back("JET_GroupedNP_2__1up")
    alg.m_systNameList.push_back("JET_GroupedNP_2__1down")
    alg.m_systNameList.push_back("JET_GroupedNP_3__1up")
    alg.m_systNameList.push_back("JET_GroupedNP_3__1down")
    
    alg.m_ntupLevel=11

    return alg


#-------------------------------------------------------
# define some alternative classes for different research
# or selections, e.g. photons for y+jets method
#-------------------------------------------------------
def sbottom_PFlowResearch(self,alg):
    #clone the standard sbottom selection
    alg=self.sbottom(alg)

    #change the config file
    alg.m_config="MasterShef/confs/sbottom_PFlowResearch.conf"

    #change the ntuple output level
    alg.m_ntupLevel=8

    #enable a custom flag
    alg.m_isPflowResearch=True
    
    #If adding truth jets, do NOT add truth neutrinos and muons into those jets
    alg.m_AddNeutMuTruthJets=False
    
    #Add truth jets to output
    alg.m_userTreeMods.push_back("fillTruthJets")
    
    #Add track jets
    alg.m_useTrackJets=True  
    
    #add B-Hadron truth 4-vectors
    alg.m_useHFTruthHadrons=True
    alg.m_userTreeMods.push_back("fillBHadronTruthVariables");
    alg.m_useMCTruthClassifier=True
    return alg
