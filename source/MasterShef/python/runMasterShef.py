#!/usr/bin/env python
 
import os,ROOT,sys,glob
from optparse import OptionParser
from copy import deepcopy
import pdb



#setting up message logging 
import logging
#debug_level=logging.ERROR
debug_level=logging.INFO
logging.basicConfig(level=debug_level,
                    format='%(name)-12s :: %(levelname)-8s ::       %(message)s')

msg=logging.getLogger('runMasterShef')
msg.info('Starting to run '+str(__file__))


#basePath="$WorkDir_DIR"
basePath="$MasterShef_DIR"
basePath="$REL22VALIDATION_DIR"

#work directory (build/<environ>/)   
workDir=os.environ[basePath[1:]]
msg.info("Found working directory '%s'"%(workDir))


#get the date in the form Month-Day e.g. Aug01
from datetime import datetime
mydate = datetime.now()
date=mydate.strftime("%b")+mydate.strftime('%d')


parser = OptionParser()
parser.add_option("--driver", help="force which driver to run, you might want to run on a cluster instead of the grid", choices=("direct","xrootd","prun","condor"), default="direct")
parser.add_option("--analysis", help="give a comma separated list of analyses in which you wish to run", default=None)
parser.add_option("--dataSource", help="force setting the data source for a file,  data=0, FullSim=1, AF-II=2. By default the dataSource will be worked out from the sample name ", type="int", default=None)
parser.add_option("--continuousWP", help="Force setting which working point you want for the continuous b-tagging, default to 0 in case using non continuous. Options 85, 77, 70, 60, 0", type="int", default=None)
parser.add_option("--date", help="submission date", default=date)
parser.add_option("--debug", help="warning=0, debug=1, verbose=2", type="int", default=0)
parser.add_option("--doJetSmearing", help="do jet smearing ntuple output", action="store_true", default=False)
parser.add_option("--doSyst", help="Do systemtic variations" ,action="store_true",default=False)
parser.add_option("--doTruth", help="do a truth level analysis",action="store_true", default=False)
parser.add_option("--doxAOD", help="save xAOD ntuple?",type="int", default=0)
parser.add_option("--dry_run", help="Do a dry run first" ,action="store_true",default=False)
parser.add_option("--exclude", help="comma separated list of sites to exclude", type="str", default=None)
parser.add_option("--filtTrigger", help="filter on a trigger?",type="str", default="")
parser.add_option("--forceCERN", help="force running on CERN_PROD sites where /afs/ is still installed", action="store_true", default=False)
parser.add_option("--mergeOutput", help="Force merging output on the grid", action="store_true", default=False)
parser.add_option("--inputContainerList", help="supply a list of containers that you want to submit to the grid or a cluster", default=None)
parser.add_option("--inputLocalFile",help="select a local directory containing an input file",default=None)
parser.add_option("--isAOD", help="tell master shef if the file is an AOD or not true/false",action="store_true", default=False)
parser.add_option("--maxEvents", type=int, help="maximum number of events to run on", default=0)
parser.add_option("--nGBPerJob",help="select the number of GBs per grid Job",default="5")
parser.add_option("--nFilesPerJob",help="select the files per grid job",default=None)
parser.add_option("--nGridJobs",help="number of jobs to run on the grid",default=None)
parser.add_option("--nGridFiles",help="number of files to run on the grid",default=None)
parser.add_option("--outputDirectory",help="name of the output directory when running tests",default="testLocal")
#MCtoMC SFs were bugged for the sbottom_multib, so by default they are turned off. Aka we set shower type=0
parser.add_option("--useDefaultMCtoMCSFs", help="turn off MCtoMC SFs for b-tagging" ,action="store_true",default=False)
parser.add_option("--triggerList",help="select input trigger list", default=workDir+"/data/MasterShef/trigger_lists/trigger_run3.txt")
#parser.add_option("--triggerList",help="select input trigger list", default=workDir+"/data/MasterShef/trigger_lists/triggers_3G_Mar2017.txt")
parser.add_option("--PRWFromTop",help="Configure PRW from Top Group data files instead of SUSYTools AutoConfig", default=False)
parser.add_option("--groupProduction", help="Use group production VO auth instead of user",default=False, action="store_true")
parser.add_option("--systBunch", help="Category of systematics to run on",default=-1,type="int" , dest="systBunch",action="store")
parser.add_option("--allSystBunches", help="Flag to define several algorithms in the same job to run in all the systematic bunches.",dest="allSystBunches",action="store_true")


(options, args) = parser.parse_args()

#set the default EL driver to be direct (local)
m_driver=options.driver
m_PRWFromTop=options.PRWFromTop
m_GroupAuth=options.groupProduction
if (m_GroupAuth==True):
    msg. info("You're running a group production, hopefully you have permission to do this")

# do  some checks on the setup.
# you need to run with either a local file or a list of containers to run on the grid
# if we are running on the grid with the --inputContainerList then we need to switch to the prun driver
if options.inputContainerList is None and options.inputLocalFile is None:
    msg.error("you didn't run --inputContainerList <list> or --inputLocalFile <path>, so I dunno what to do... exiting...")
    sys.exit(1)

if options.inputContainerList is not None:
    #if we have supplied a list of files then we check with driver has been selected
    if m_driver=="direct":
        msg.error("direct driver not yet implemented for a list of containers! Run with --driver=prun or --driver=condor. exiting...")
        sys.exit(1)
    msg.info("You're going to run in a list of containers with the driver '%s' ... I hope this is ok... doing it anyway.."%(m_driver))
        

if options.inputLocalFile is not None:
    #check options with a local file
    if m_driver=="prun":
        msg.error("you don't want to run a local input file on the grid!! try with '--driver direct' or '--driver condor' instead")
        sys.exit(1)
    if m_driver=="condor":
        msg.error("running a downloaded file on the condor isnt implemented yet, it will be soon.... exiting")
        sys.exit(1)
    

    

msg.info("")
for item in sorted(options.__dict__.items()):
    msg.info("Loaded %s"%(str(item)))
msg.info("")


#release the release
baseRelease=os.environ["ROOTCOREDIR"].split("AnalysisBase/")[1].split("/")[0]
msg.info("Running on AnalyisBase '%s'"%(baseRelease))


#check if this is rel21 or no
isR21 = int(os.environ["ROOTCORE_RELEASE_SERIES"])>=25

if isR21 is False:
    msg.error("rel20.7 no longer supported, fuck off")
    sys.exit(1)


#retrieve the has commit on the repo
#otherwise if it's a clone of the code use 'local' as the name
#this will become the so called 'tag' when submitting to the grid
import subprocess
try:
    hashcommit=subprocess.check_output(["git","rev-parse","--verify","--short","HEAD"]).rstrip()
except subprocess.CalledProcessError: 
    hashcommit="local"


#tag for grid submission is the release plus has commit
tag="%s-%s"%(baseRelease,hashcommit)
msg.info("Tag to be used if submitted to the grid '%s'"%(tag))



#setup a class for all analyses
from MasterShef.analyses import Analyses
_analyses=Analyses()
#set the default trigger list
#_analyses.setTriggerList(options.triggerList)
_analyses.setdoSyst(options.doSyst)
_analyses.setSystBunch(options.systBunch)

#make sure an analysis was supplied
if options.analysis is None:
    msg.error("")
    msg.error("You did no supply an analysis to run on!!!")
    msg.error("Printing a list of available analyses...")
    msg.error(str(_analyses.getAllAnalyses()))
    msg.error("")
    msg.error("you can supply a single analysis or a comma separated list of them, e.g. --analysis sbottom,stop0L")
    sys.exit(1)

list_of_analyses=options.analysis.split(",")
#check if the analysis is loaded
for ana in list_of_analyses:
    if not  ana in _analyses.getAllAnalyses():
        msg.error("Unknown analysis named '%s' "%(ana))
        msg.error("Printing a list of available analyses...")
        msg.error(str(_analyses.getAllAnalyses()))
        sys.exit(1)
 
#done with setup, now main() will be called
msg.info("setup and checks now complete")





#simply python method for adding triggers to the MasterShef alg
#converts m_triggerListFile ( a file containing a list of triggers)
#into vector elements in mastershef for dumping 
#...could be a method with mastershef
#... but easier with python
def addTriggers(theAlg):
    theList=theAlg.m_triggerListFile
    if theList=="":return 
    ftrig = open(theList)
    for line in ftrig:
        line=line.rstrip()
        # protect against blank lines or comments
        if "#" in line:continue
        
        lst = ["HLT","L1","L2"]
        if any( s in line for s in lst):
            triggername=line
            msg.info("Adding trigger from file list.. %s"%(triggername))
            theAlg.m_triggerList.push_back(triggername)
            theAlg.m_triggerNtup.push_back(triggername)
        


def runJob(job,sh,driver,dataSource,showerType=0,submitDir=options.outputDirectory):
    #tell the job about the sample handler containing info about the files/containers
    job.sampleHandler (sh)
    

    #set up a default algorithm to be used by all analyses
    ROOT.gROOT.SetBatch()
    alg = ROOT.MasterShef()
        

    #alg.printAllPossibleSelections()
    #alg.printAllPossibleFills()
    
    alg.m_basePath=basePath
    alg.m_triggerListFile=options.triggerList

    # save xAOD jets in ntup?
    alg.m_doxAOD=options.doxAOD
   
    # filter on a trigger? 
    # this part needs some work
    alg.m_applyTrigger=options.filtTrigger
   
    # tell MasterShef about the showerType
    alg.m_showerType=showerType
    # tell MasterShef about the dataSource
    alg.m_dataSource=dataSource
    # turn on debugging?
    alg.m_debug=options.debug
    
    alg.m_systBunch = options.systBunch
    # setup the initialCuts, this should be common and forced for all analyses
    # this includes primary vertex checks, event cleaning etc..
    alg.m_userSelMods.push_back("initialCuts")

    #fill common variables for all analyses
    #these are the basics, jet 4-vectors, lepton 4-vectors, met, event-weights etc....
    alg.m_userTreeMods.push_back("fillCommonVariables")
    
    #split between perform truth analyses and reco analyses
    if options.doTruth:
        #tell the algorithm this is a truth analysis
        alg.m_doTruth=True
        # perhaps not needed
        alg.m_dataSource=0 
        #prepare truth objects as this is a truth analysis
        alg.m_userSelMods.push_back("prepareTruthObjects")
    else:
       
        #prepare reco objects
        alg.m_userSelMods.push_back("prepareObjects")
        #clean reco objects!
        alg.m_userSelMods.push_back("cleanJets")
        #Calum - hmm need to see if we should apply a cosmic veto still
        #currently by default we just remove cosmic leptons from collections
        #alg.m_userSelMods.push_back("cosmic")
        alg.m_userSelMods.push_back("cleanMuons")


        #these are more complex variables needed by the reco analysis
        #things like systematic event weights, lbn, bcid, jet varaibles
        alg.m_userTreeMods.push_back("fillExtraRecoVariables")
        
         #additional truth info that is needed in the reco analysis, but not needed in the truth analysis
         #alg.m_userTreeMods.push_back("fillExtraTruthVariables")

    msg.info("Finished setting up the default analysis")
    ##
    ## Finished setting up the default algorithm, now let's setup all analyses
    ##
    for m_analysis in list_of_analyses:
        #clone the default alg
        msg.info("Cloning the the default analysis for %s"%(m_analysis))
       
       
        new_alg = alg.Clone()
        new_alg.m_outputName = m_analysis
        alg.SetName("MasterShef::"+m_analysis)
        #add analysis options to the alg
        addAnalysisOptions = getattr(_analyses,m_analysis,new_alg)
        new_alg=addAnalysisOptions(new_alg)
        
        #force telling MasterShef about the continuous b-tagging WP
        contWP=options.continuousWP
        if contWP != None:
            msg.info("Forcing the continuous btagging WP to be something else!")
            new_alg.m_contWP=int(contWP) #cast it to an int
      
        addTriggers(new_alg)
        
        msg.info("")
        msg.info("Printing loaded selections for  analysis '%s'"%(m_analysis)) 
        new_alg.printSelections()
        msg.info("Printing loaded functions for variable filling for  analysis '%s'"%(m_analysis))
        new_alg.printFills()
        
        #add new outputs and ntuples for the algo!
        #output = ROOT.EL.OutputStream(m_analysis)
        #ntuple = ROOT.EL.NTupleSvc(m_analysis)
        #job.outputAdd(output);
        #job.algsAdd(ntuple)
        #job.algsAdd(new_alg);
        #msg.info("done setting up analysis '%s' with dataSource='%d' "%(m_analysis,new_alg.m_dataSource))
        
        if not options.doSyst or (options.doSyst and not options.allSystBunches):
            #add new outputs and ntuples for the algo!
            output = ROOT.EL.OutputStream(m_analysis)
            ntuple = ROOT.EL.NTupleSvc(m_analysis)
            job.outputAdd(output);
            job.algsAdd(ntuple)
            job.algsAdd(new_alg);
            msg.info("done setting up analysis '%s' with dataSource='%d' "%(m_analysis,new_alg.m_dataSource))
        else:
            for ialg in range(new_alg.m_systdivisions):
                new_alg_syst = new_alg.Clone()
                _analyses.setSystBunch(ialg)
                addAnalysisOptions = getattr(_analyses,m_analysis,new_alg_syst)
                new_alg_syst=addAnalysisOptions(new_alg_syst)
                new_alg_syst.m_outputName = m_analysis+"."+str(ialg)
                new_alg_syst.SetName("MasterShef::"+m_analysis+"."+str(ialg))
                #add new outputs and ntuples for the algo!
                output = ROOT.EL.OutputStream(m_analysis+"."+str(ialg))
                ntuple = ROOT.EL.NTupleSvc(m_analysis+"."+str(ialg))
                job.outputAdd(output)
                job.algsAdd(ntuple)
                job.algsAdd(new_alg_syst)
                msg.info("done setting up analysis '%s' with dataSource='%d' for systematic bunch %d"%(m_analysis,new_alg.m_dataSource,ialg))
        
#        msg.info("done setting up analysis '%s' with dataSource='%d' "%(m_analysis,new_alg.m_dataSource))
          
    job.options().setString(ROOT.EL.Job.optXaodAccessMode,ROOT.EL.Job.optXaodAccessMode_class)
    if options.isAOD:
        job.options().setString(ROOT.EL.Job.optXaodAccessMode,ROOT.EL.Job.optXaodAccessMode_athena)
        alg.m_is_p2411=False


    #job.options().setBool(ROOT.EL.Job.optMemFailOnLeak,True)

    job.options().setDouble(ROOT.EL.Job.optRemoveSubmitDir,1);
    if options.maxEvents>0:
        msg.info("Running on maximum number of events=%d"%(options.maxEvents))
        job.options().setDouble(ROOT.EL.Job.optMaxEvents, options.maxEvents)
  
    
    sh.printContent()
    msg.info("Submitting the job.. ")
    if  options.dry_run:
        msg.info("...nah not really this is a dry run...")
    else:
        driver.submitOnly(job, submitDir)
 
    msg.info("done with this one")



def getLastTag(sam):
    return sam.split(".")[-1].split("_")[-1]





def getDataSource(sam):


    if options.dataSource != None and (m_driver!="direct" and m_driver!="xrootd"):
        msg.warning("you're trying to run on the grid but are forcing a dataSource '%d'"%(options.dataSource))
        msg.warning("not safe.. haven't implemented this yet, or why you would want to do it...so exiting")
        sys.exit(0)
        

    if options.dataSource is None:

        if any(x in sam for x in  ["data15","data16","data17","data18"]):
            return ROOT.ST.ISUSYObjDef_xAODTool.Data
        elif sam.split(".")[-1].find("_a")>0:
            return ROOT.ST.ISUSYObjDef_xAODTool.AtlfastII 
        elif (sam.find("_s")>0 or sam.find("TRUTH")>0):
            return ROOT.ST.ISUSYObjDef_xAODTool.FullSim 
        else:
            msg.error("unknown container %s, scan't get the dataSource !! exiting..."%(sam))
            msg.error("you could try forcing the dataSource with --dataSource argument")
            sys.exit(0)
        

    elif options.dataSource==0:
        return ROOT.ST.ISUSYObjDef_xAODTool.Data
    elif options.dataSource==1:
        return ROOT.ST.ISUSYObjDef_xAODTool.FullSim
    elif  options.dataSource==2:
        return ROOT.ST.ISUSYObjDef_xAODTool.AtlfastII
    else:
        msg.error("incorrect datasource selected! set 0/1/2")
        sys.exit(0)


def arrangeSamples( sam,shower,dictionary):
        
    dataSource=getDataSource(sam)
    if dataSource==0:
        dictionary["data"].append(sam)
    elif dataSource==1:
        dictionary["full"][shower].append(sam)
    elif dataSource==2:
        dictionary["fast"][shower].append(sam)
    else:
        msg.error("unknown container %s !! exiting..."%(sam))
        sys.exit(0)



def getDSName(name):
    return name.split(".")[2]


def getShowerTypeFromName(name):
    name=getDSName(name)
    if options.useDefaultMCtoMCSFs == True:
        return "PhPy8"

    #SUSYTools mentions only Powheg+Herwig7 but we select other cases as aMcAtNlo+Herwigpp too...
    if any( x in name for x in ["Herwig","HW7"]):
        return "Herwig7" 
    elif "Sherpa_21" in name: 
        return "Sherpa21"
    elif any( x in name for x in ["Sherpa_228","Sh_228"]): 
        return "Sherpa228"
    elif any( x in name for x in ["Sherpa_22","Sh_22"]): 
        return "Sherpa22"
    elif any( x in name for x in ["MGPy8EG","aMcAtNloPy8","aMcAtNloPythia8","MadGraphPythia8"]) : 
        return "MadGraphPy8"
    else: 
        return "PhPy8"


#function to prepare all samples to be submitted
def prepareSamplesGrid(theList,submitDir=options.outputDirectory,source=0,shower=0,group_auth=False):
    
    #setup sample handler
    sh=ROOT.SH.SampleHandler()

    check=[]
    for sam in theList:
        if sam not in check:
            ROOT.SH.scanRucio(sh , sam)  
            check.append(sam)
        else:
            msg.warning("Ahh crap you duplicated a sample.... ")
            msg.warning("So I am skipping '%s'"%(sam))
            msg.warning("becareful next time....")
            #sys.exit(1)

    sh.printContent()
    sh.setMetaString ("nc_tree", "CollectionTree")
    sh.setMetaString ("nc_cmtConfig",os.getenv("AnalysisBase_PLATFORM"))
    if group_auth:
        sh.setMetaDouble("nc_official",1)
        sh.setMetaString("nc_voms","atlas:/atlas/phys-susy/Role=production")
    #setup the job
    job=ROOT.EL.Job()
    #setup the driver
    driver=ROOT.EL.PrunDriver()
    if group_auth:
        driver.options().setString("nc_outputSampleName","group.phys-susy."+tag+"."+options.date+"ttMET0L"+".%in:name[2]%.%in:name[5]%.%in:name[6]%")
    else:
#        driver.options().setString("nc_outputSampleName","group."+tag+"."+options.date+".%in:name[2]%.%in:name[5]%.%in:name[6]%") 
        driver.options().setString("nc_outputSampleName","user.%nickname%."+tag+"."+options.date+".%in:name[2]%.%in:name[5]%.%in:name[6]%");

        #driver.options().setString("nc_memory","000")

    ## hack!! for submitting jobs my own username, needst o be fixed
 #   driver.options().setString("nc_outputSampleName","user.%nickname%."+tag+".%in:name[4]%.%in:name[7]%.%in:name[8]%");
    
    if options.mergeOutput:
         driver.options().setString( "nc_mergeOutput", "true" )
    else:
         driver.options().setString( "nc_mergeOutput", "false" )

    if options.forceCERN:
        driver.options().setString(ROOT.EL.Job.optGridSite,"CERN_PROD")

    #separate with commas
    if options.exclude != None:
        driver.options().setString(ROOT.EL.Job.optGridExcludedSite, options.exclude)


    if options.nFilesPerJob is not None:
        driver.options().setString(ROOT.EL.Job.optGridNFilesPerJob, str(options.nFilesPerJob))
    #else:
    #    driver.options().setString(ROOT.EL.Job.optGridNGBPerJob, options.nGBPerJob);
    
    if options.nGridJobs is not None:
        driver.options().setString(ROOT.EL.Job.optGridNJobs, str(options.nGridJobs))
    if options.nGridFiles is not None:
        driver.options().setString(ROOT.EL.Job.optGridNFiles, str(options.nGridFiles))
    
    msg.info("about to run job with source=%d, showerType=%d, submitDir=%s"%(source,shower,submitDir))
    
    #now run the job !
    runJob(job,sh,driver,source,shower,submitDir)
                

def prepareSampleCluster(sam,submitDir,source,shower):
    sh=ROOT.SH.SampleHandler() 

       
    job=ROOT.EL.Job()
    driver=ROOT.EL.CondorDriver()
    certificate_path = os.path.expandvars('$X509_USER_PROXY')
    certificate_newpath = os.path.join(os.getcwd(), os.path.split(certificate_path)[1])

    input_path=sam
    if sam.startswith("mc16") or sam.startswith("mc20") or sam.startswith("data"):
        if ":" in sam: temp=sam.split(":")[1]
        ptag=getLastTag(temp)
        input_path="/atlas/shatlas/DAODS/rel21/%s/"%(ptag)

    import shutil
    shutil.copyfile(certificate_path, certificate_newpath)
    driver.shellInit = r"""export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${{ATLAS_LOCAL_ROOT_BASE}}/user/atlasLocalSetup.sh
export X509_USER_PROXY={0}; echo $X509_USER_PROXY
localSetupEmi
lsetup "rucio -w"
mkdir -p {1}
rucio get {2} --dir={1} 
""".format(certificate_newpath,input_path,sam)
    job.options().setString(ROOT.EL.Job.optCondorConf, 'use_X509userproxy = TRUE')


    if ":" in sam:sam=sam.split(":")[1]
    input_sam_local="%s/%s"%(input_path,sam)
    ROOT.SH.ScanDir().scan(sh, input_sam_local)       
    sh.printContent()
    sh.setMetaString ("nc_tree", "CollectionTree")
    sh.setMetaString ("nc_cmtConfig",os.getenv("AnalysisBase_PLATFORM"))

    runJob(job,sh,driver,source,shower,submitDir)
    

#split the list into separate condor jobs
def prepareSamplesCluster(theList,submitDir,source,shower):
    for sam in theList:
        prepareSampleCluster(sam,submitDir,source,shower)

def prepareSamples(theList,submitDir=options.outputDirectory,source=0,shower=0):
    if len(theList)<1:return

    if m_driver=="prun":
        prepareSamplesGrid(theList,submitDir,source,shower,m_GroupAuth)
    if m_driver=="condor":
        msg.error("running on samples with condor isn't finalised yet! hopefully will be soon")
        sys.exit(1)
        #prepareSamplesCluster(theList,submitDir,source,shower)
    
    

                


#main function for running the samples on the grid
def runBatch():
    #firstly, retrieve the list on input containers to run on
    sampleslist = options.inputContainerList
    
    #split up samples based on if they are data/full-sim/fast-sim
    #their shower type and also based on if 
    
    import collections
    dictShowerTypes=collections.OrderedDict([("PhPy8",[]),("Herwig7",[]),("Sherpa21",[]),("Sherpa22",[]),("MadGraphPy8",[]),("Sherpa228",[])])

    dictSamples={"data":[],"full":deepcopy(dictShowerTypes),"fast":deepcopy(dictShowerTypes)}


    #loop over the samples in the input file
    for sam in open(sampleslist):
        sam=sam.rstrip()
        #skip commented out lines and blank lines
        if "#" in sam: continue
        if sam=="": continue
        
        shower = getShowerTypeFromName(sam)
        arrangeSamples(sam, shower, dictSamples)

    
        
    #prepare data samples
    msg.info("Now preparing data samples....")
    prepareSamples(dictSamples["data"],
                   submitDir="%s_data"%(tag),
                   source=ROOT.ST.ISUSYObjDef_xAODTool.Data)
 
    #prepare full and fast sim samples

    for key,source in [("full",ROOT.ST.ISUSYObjDef_xAODTool.FullSim),("fast",ROOT.ST.ISUSYObjDef_xAODTool.AtlfastII)]:
        msg.info("Now preparing %s sim samples...."%(key))
        
    
        for showerType,sname in enumerate(dictSamples[key]):
            #skip if no samples with this shower type
            if len(dictSamples[key][sname])<1:continue
            
            msg.info("Found and now preparing %s sim samples with shower type %d (%s)...."%(key,showerType,sname))
            prepareSamples(dictSamples[key][sname],
                           submitDir="%s_%s_%s"%(tag,key,sname),
                           source=source,
                           shower=showerType)

   
    msg.info("Done!!")
    msg.info("You submitted these jobs to the grid with tag=%s please tag MasterShef as this and now push to the online repo, cheers lad.."%(tag))   





def runLocal():
        #get the input file path                                                                                                                                                                              
    inputFilePath = options.inputLocalFile
    ROOT.gSystem.ExpandPathName(inputFilePath)
    #setup sample handler                                                                                                                                                                                 
    sh=ROOT.SH.SampleHandler()

    shower="PhPy8"
    if "/" in inputFilePath:
        inputFile=inputFilePath.split("/")[-2]
        if "." in inputFile and len(inputFile.split(".")) >2 :
            shower=getShowerTypeFromName(inputFile)
        if shower=="PhPy8":
            inputFile=inputFilePath.split("/")[-1]
            if "." in inputFile and len(inputFile.split(".")) > 2 :
                shower=getShowerTypeFromName(inputFile)
    else:
        inputFile=inputFilePath
        if "." in inputFile and len(inputFile.split(".")) > 2 :
            shower=getShowerTypeFromName(inputFile)

    dictShowerTypes={"PhPy8":0, "Herwig7":1,"Sherpa21":2,"Sherpa22":3,"MadGraphPy8":4}

    
    if m_driver == 'xrootd':
        server, path = inputFilePath.replace('root://','').split('//')
        path=os.path.join('/',path)
        sh_list = ROOT.SH.DiskListXRD(server, path, True)
        ROOT.SH.ScanDir().scan(sh, sh_list)

    #if this is a file, the setup to run on this only
    elif os.path.isfile(inputFilePath):
        name=inputFilePath.rsplit("/",1)
        pattern=str(name[1]+"*")
        name=str(name[0])
        ROOT.SH.ScanDir().filePattern(pattern).scan(sh,name)
    #maybe it is a wildcard of input files
    #could change this to nargs argument in OptParser
    elif "*" in inputFilePath:
        name=inputFilePath.rsplit("/",1)
        pattern=str(name[1]+"*")
        name=str(name[0])
        ROOT.SH.ScanDir().filePattern(pattern).scan(sh,name)
    #if it's a directort then fine root files here to submit
    elif os.path.isdir(inputFilePath):
        ROOT.SH.ScanDir().scan(sh, inputFilePath)
                        
    else:
        try:
            name=inputFilePath.rsplit("/",1)
            input_sample=ROOT.SH.SampleLocal("myTest")
            input_sample.add(inputFilePath)
            sh.add(input_sample)
        except:
            msg.error("urmmm what is this???...  %s  ... not a file or a directory!"%(inputFilePath))
            sys.exit(1)

       
    
    sh.printContent()
    sh.setMetaString ("nc_tree", "CollectionTree")
    sh.setMetaString ("nc_cmtConfig",os.getenv("AnalysisBase_PLATFORM"))


    dataSource=getDataSource(inputFilePath)

    job=ROOT.EL.Job()
    driver=ROOT.EL.DirectDriver()
    runJob(job,sh,driver,dataSource,showerType=dictShowerTypes[shower],submitDir=options.outputDirectory)
    


#
# Start the main preparation script
#
def main():
    ROOT.gROOT.Macro(os.environ['ROOTCOREDIR']+"/scripts/load_packages.C")
    # Set up the job for xAOD access:
    ROOT.xAOD.Init().ignore()
    #select the driver
    if m_driver=="prun" or m_driver=="condor": runBatch()
    else : runLocal()
                
  

        
 
if __name__ == "__main__":
    main()



        






        
  
