import os,sys,subprocess
#import pyAMI
#import pyAMI.client
#from pyAMI import *
import ROOT

#client = pyAMI.client.Client(
#  'atlas'
#)
#ami = client



py_version=int(sys.version_info[0])+float(sys.version_info[1])*0.1 
if py_version < 2.7:
    raise "Must be using at least Python 2.7"


import argparse
#Parse arguments from the command line
parser = argparse.ArgumentParser(description='prep_download.py a script for downloading MasterShef outputs from the grid')
parser.add_argument("-e", dest="expr",help="regrex search expression for the jobd", default=None)#,required=True)
parser.add_argument("-f", dest="in_file",help='alternatively use a file with a list of samples to be downloaded',default=None)
parser.add_argument("-n", dest="nfiles_per_job",help='number of files per download job',default=50)
parser.add_argument("--proxy", dest="proxy_file",help="an alternative location of your proxy", default=None)
parser.add_argument("--scratch", dest="use_scratch",help="an alternative location of your proxy",action="store_true")
parser.add_argument("--update", action="store_true", dest="update",help="update the samples")
parser.add_argument("--sort",  action="store_true", dest="sort",help="sort the samples based on size")
parser.add_argument("-l", dest="loc",help="location of where to download to", default="/atlas/shatlas/NTUP_SUSY/")
args=par=parser.parse_args() 



nfiles_per_job=int(args.nfiles_per_job)


loc=args.loc
if args.use_scratch:
    temp_loc=loc.replace("/atlas/","/scratch/")
else:
    temp_loc=loc




blacklist=[]
#blacklist=["UKI-SCOTGRID-ECDF_SCRATCHDISK","AGLT2_SCRATCHDISK","UKI-NORTHGRID-LANCS-HEP_SCRATCHDISK"]#CA-VICTORIA-WESTGRID-T2_SCRATCHDISK"]#CA-MCGILL-CLUMEQ-T2_SCRATCHDISK"]#,UKI-LT2-QMUL_SCRATCHDISK","SARA-MATRIX_SCRATCHDISK","SWT2_CPB_SCRATCHDISK","UNIBE-LHEP_SCRATCHDISK"]
#blacklist=["MWT2_UC_SCRATCHDISK"]
#blacklist=["SARA-MATRIX_SCRATCHDISK"]
#blacklist=["GOEGRID_SCRATCHDISK"]

#do the setup
user=os.environ.get('USER')

#setup a logger
import logging
debug_level=logging.INFO
logging.basicConfig(level=debug_level,
                    format='%(name)-12s :: %(levelname)-8s ::       %(message)s')

msg=logging.getLogger('prep_download ')
msg.info("Hello %s "%(user))

#check there's an input
if args.expr is None and args.in_file is None:
    msg.error("neither a search expression was supplied (-e) nor an in file (-f) ! Please do one")
    exit(0)




samples_aods=".samples_aods.list"
if os.path.isfile(samples_aods) and args.update:
    msg.info("updating the file %s"%(samples_aods))
    os.system("rm -rf %s "%(samples_aods))
if not os.path.isfile(samples_aods):
    msg.info("creating file %s for the first time"%(samples_aods))
    os.system("touch %s"%(samples_aods))
    msg.info("gettig all mc15 evgen")
    os.system("rucio list-dids mc15_13TeV:mc15_13TeV.*evgen.EVNT* --filter type=container --short >> %s"%(samples_aods))
    msg.info("gettig all mc16")
    os.system("rucio list-dids mc16_13TeV:mc16_13TeV.*merge*.AOD.* --filter type=container --short >> %s"%(samples_aods))
    msg.info("getting all data15")
    os.system("rucio list-dids data15_13TeV:data15_13TeV.*merge*.AOD.* --filter type=container --short >> %s"%(samples_aods))
    msg.info("getting all data16")
    os.system("rucio list-dids data16_13TeV:data16_13TeV.*merge*.AOD.* --filter type=container --short >> %s"%(samples_aods))
    msg.info("getting all data17")
    os.system("rucio list-dids data17_13TeV:data17_13TeV.*merge*.AOD.* --filter type=container --short >> %s"%(samples_aods))
    msg.info("done with getting aods")


def getLookUpDict():
    with open(samples_aods) as f:
        content = f.readlines()

    retval={}
    for x in content:
        x=x.strip()
        temp=x.strip().split(".")

        container=temp[0].split(":")[1]
        dsid=temp[1]
        name=temp[2]

        if 'data' in container:
            if 'physics_Main' not in name: continue
            
        retval[dsid]=[container,name]
        
    return retval

lookup=getLookUpDict()


def getFileList(fname):
    with open(fname) as f:
        content = f.readlines()

    content = [x.strip() for x in content] 
    return iter(sorted(content))

def getTagDate(sam):
    temp=sam
    if ":" in sam:temp=sam.split(":")[1]
    temp=temp.split(".")[2:6]
    return ".".join(temp[:-1]),temp[-1]
    


def getOriginalName(sam):
    temp=sam.split(".")[-4:]
    dsid=temp[0]
    deri=temp[1]
    tags=".".join(temp[2:])
    
    try:
        cont,name=lookup[dsid]
    except KeyError:
        if "period" not in dsid:
            msg.error("unknown dsid=%s try running --update or check your list!"%(dsid))
            #exit(0)

        cont="data15_13TeV"
        if "grp16" in tags:cont="data16_13TeV"
        if "grp17" in tags:cont="data17_13TeV"
        if "grp18" in tags:cont="data18_13TeV"
        name="data"
        



    sam_name=".".join([cont,dsid,name,tags])
    return sam_name
    



#check if a proxy file exists
proxy_file="/home/%s/.globus/myproxy.tmp"%(user)


if args.proxy_file is not None:
    msg.info("you supplied the proxy file =%s from the command line"%(args.proxy_file))
    proxy_file=args.proxy_file

if not os.path.exists(proxy_file):
    msg.error("warning proxy_file=%s does not exist, please create it or supply a different one with --proxy <file> option"%(proxy_file))
    exit(0)


msg.info("Going to use a temporary proxy found =%s, now checking..."%(proxy_file))

#now check the proxy
import subprocess
proxy_check_output=subprocess.check_output(["voms-proxy-info","--file" ,"%s"%(proxy_file)]).rstrip()    

msg.info(proxy_check_output)

for line in proxy_check_output.split("\n"):
    if "timeleft" in line:
        time=line.rstrip().split("  :")[1].replace(" ","")
        if time == "00:00:00":
            msg.error("proxy has expired!!")
            exit(0)





if args.expr is not None:
    msg.info("Using rucio go get samples with expression =%s"%(args.expr))
    samples=iter(sorted(subprocess.check_output(["rucio","list-dids","%s"%(args.expr),"--filter","type=container","--short"]).rstrip().splitlines()))
else:
    samples=getFileList(args.in_file)



if args.sort:
    msg.info("Going to sort the samples based on their size")

    samples_list_size=[]
    #convert the iterator back to a list
    samples=list(samples)

    #loop over the samples
    for sample in samples:
        #get information about their size
        output=subprocess.check_output(["rucio","list-files","%s"%(sample)]).splitlines()
        temp=output[-1].split(": ")[1]
        
        msg.info("got %s with size %s"%(sample,temp))

        #retrive the size and convert to bytes
        size,denom=temp.split(" ")
        if "kB" in denom:
            size=float(size)*1000.
        if "MB" in denom:
            size=float(size)*1000.*1000.
        
        #store a list of tuples with the size of the sample
        samples_list_size.append((size,sample))

    #order the list of size, with the largest first
    samples_list_size.sort(key=lambda x: x[0],reverse=True)

    #remake the sample list based on the size
    temp=[]
    for size,sam in samples_list_size:
        temp.append(sam)

        
    samples=[]
    #split the size into ever other piece so the large files are split over each job
    for i in range(0,nfiles_per_job):
        piece=temp[i::nfiles_per_job]
        samples+=piece

    #conver back to iterator
    samples=iter(temp)
                                                          



top=r'''
unset GLITE_ENV_SET 
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
export X509_USER_PROXY=%s
export DQ2_LOCAL_SITE_ID=ROAMING 
lsetup fax
lsetup python 
lsetup "rucio -w"  --quiet 
source /cvmfs/grid.cern.ch/emi3ui-latest/etc/profile.d/setup-ui-example.sh

'''%(proxy_file)

i=0
done = False
while not done:

    fname="download_"+str(i)+".sh"
    a=open(fname,"w")
    a.write(top)
    
    count = 0
    
    while (count<=nfiles_per_job):

        try:
            sample=samples.next()
        except StopIteration:
            msg.info("finished with samples")
            done = True
            break

        
        msg.info("samples=%s"%(sample))
        orig_sample=getOriginalName(sample)
        tag,date=getTagDate(sample)


        #tag=""
        #date=""
        msg.info("name=%s"%(orig_sample))
        msg.info("")
       
        a.write("mkdir -p "+temp_loc+tag+"/"+date+"/"+orig_sample+"\n")
        a.write("cd "+temp_loc+tag+"/"+date+"\n")
                 
        command=r'''pwd; rucio -v -v get --ndownloader=4  '''
        if len(blacklist)>0:
            command+=r''' --rse '(tier>-1)'''
            for site in blacklist:
                command+=r'''\\'''+site
            command+="' "
        command+= sample+" --dir="+orig_sample+" \n"

        a.write(command)
        count = count+1

    if count == 0 :
        msg.error("No files found, check your expression!")
        sys.exit(1)
        
    a.write("mkdir -p "+loc+tag+"/"+date+" \n ")
    a.write("mv "+temp_loc+tag+"/"+date+"/*  "+loc+tag+"/"+date+"/ \n ")

    msg.info("done with job=%s"%(fname))
    a.close()

    os.system("chmod u+x "+fname)
    i = i+1
    

    
