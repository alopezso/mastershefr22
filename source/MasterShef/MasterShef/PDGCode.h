
#ifndef _INC_PDG_CODE_
#define _INC_PDG_CODE_

#include <vector>


class PDGCode
{
 public:

  PDGCode()  {}
  ~PDGCode()  {}

  enum NUMBERING_SCHEME {

    Unknown = 0,

    // quarks

    d = 1, u, s, c, b, t,

    // leptons

    e_minus = 11, nu_e, mu_minus, nu_mu, tau_minus, nu_tau,

    // gauge bosons

    g = 21, gamma, Z0, W_plus
  };

  static const std::vector<int>& GetPartonCodes(std::vector<int>& partons);
  static const std::vector<int>& GetNeutrinoCodes(std::vector<int>& nus);
  static const std::vector<int>& GetEGammaCodes(std::vector<int>& eGammas);
};

#endif
