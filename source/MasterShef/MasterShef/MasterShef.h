
#ifndef MasterShef_MasterShef_H
#define MasterShef_MasterShef_H

#include "MasterShef/TMctLib.h"
#include "MasterShef/PDGCode.h"

#include <EventLoop/Algorithm.h>
#include <EventLoop/Job.h>
#include "xAODRootAccess/tools/TFileAccessTracer.h"
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>


#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"

// SUSYTools include:
#include "SUSYTools/SUSYObjDef_xAOD.h"
//#include "CPAnalysisExamples/errorcheck.h"

#include <EventLoop/Job.h> 
#include "xAODRootAccess/tools/TFileAccessTracer.h"

#include <EventLoop/StatusCode.h> 
#include <EventLoop/Worker.h> 

// Infrastructure include(s): 
#include "xAODRootAccess/Init.h" 
#include "xAODRootAccess/TEvent.h" 
#include "xAODRootAccess/tools/Message.h"
#include <TFile.h>

// EDM include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/PhotonAuxContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODBTagging/BTaggingUtilities.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODBase/IParticle.h"
#include "xAODBase/IParticleContainer.h"

#include "ElectronPhotonShowerShapeFudgeTool/ElectronPhotonShowerShapeFudgeTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"

#include "TauAnalysisTools/TauTruthMatchingTool.h"
#include "TauAnalysisTools/TauSelectionTool.h"
#include "TauAnalysisTools/TauSmearingTool.h"
#include "TauAnalysisTools/TauTruthMatchingTool.h"
#include "TauAnalysisTools/Enums.h"
#include "xAODTau/TauJet.h"
#include "xAODTau/TauJetContainer.h"

#include "xAODMissingET/MissingET.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODJet/JetTypes.h"
#include <SampleHandler/MetaFields.h>


#include "AsgTools/AsgTool.h"
#include "AsgTools/IAsgTool.h"
#include "AsgMessaging/AsgMessaging.h"


#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "GoodRunsLists/TGoodRunsList.h"
#include "GoodRunsLists/TGoodRunsListReader.h"

#include "AthLinks/ElementLink.h"


// ASG includes: 
#include "AsgTools/AsgTool.h" 
#include "AsgTools/IAsgTool.h" 
#include "AsgMessaging/AsgMessaging.h"

// Metadata includes
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

// Systematics includes
#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicRegistry.h"
//#include "PATInterfaces/SystematicCode.h"


#include "PileupReweighting/PileupReweightingTool.h"

#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"


#include "PMGTools/PMGSherpa22VJetsWeightTool.h"
#include "PMGTools/PMGTruthWeightTool.h"

#include "JetSmearing/JetMCSmearingTool.h"
#include "JetSmearing/PreScaleTool.h"
#include "JetSmearing/SmearData.h"
#include "JetSmearing/IJetMCSmearingTool.h"
#include "JetSmearing/JetMCSmearingParameters.h"


#include "MCTruthClassifier/MCTruthClassifier.h"
//#include "MCTruthClassifier/MCTruthClassifierDefs.h"


#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
// Systematics includes
#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicRegistry.h"
//#include "PATInterfaces/SystematicCode.h"


// jet reclustering
//#include "JetRec/PseudoJetGetter.h"
#include "JetRec/JetFromPseudojet.h"
#include "JetRec/JetFinder.h"
#include "JetRec/JetSplitter.h"
#include "JetRec/JetRecTool.h"
#include "JetRec/JetDumper.h"
#include "JetRec/JetToolRunner.h"
#include "JetRec/JetConstituentsRetriever.h"
#include "JetRec/JetPseudojetRetriever.h"

// track selection

#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

// METUtilities includes
//#include "METUtilities/METSignificance.h" 

// boosted tagger
//#include "BoostedJetTaggers/IJSSTagger.h"
#include "BoostedJetTaggers/SmoothedWZTagger.h"
//#include "BoostedJetTaggers/SmoothedTopTagger.h"
#include "BoostedJetTaggers/JSSWTopTaggerDNN.h"
//#include "BoostedJetTaggers/JSSWTopTaggerBDT.h"
//#include "JetSubStructureMomentTools/NSubjettinessTool.h"
#include "JetSubStructureUtils/Nsubjettiness.h"
#include "JetSubStructureMomentTools/NSubjettinessTool.h"
#include "JetSubStructureMomentTools/NSubjettinessRatiosTool.h"
#include "JetSubStructureMomentTools/QwTool.h"
#include "JetSubStructureMomentTools/JetChargeTool.h"
#include "JetSubStructureMomentTools/DipolarityTool.h"
#include "JetSubStructureMomentTools/KTSplittingScaleTool.h"


//ttbar+HF classification
#include "MasterShef/ClassifyAndCalculateHF.h"
#include "MasterShef/HFSystDataMembers.h"

// ROOT includes
#include <TH1D.h>
#include <TFormula.h>
#include <TTree.h>
#include <TStopwatch.h>
#include <TSystem.h>
#include <TFile.h>


//#include "CPAnalysisExamples/errorcheck.h"
#include "PathResolver/PathResolver.h"


#include "ElectronPhotonShowerShapeFudgeTool/ElectronPhotonShowerShapeFudgeTool.h"
#include "TrigConfxAOD/xAODConfigTool.h" 
#include "TrigDecisionTool/TrigDecisionTool.h"

#ifdef USE_RESTFRAMES
#include "RestFrames/RestFrames.hh"
#endif

static SG::AuxElement::Decorator<char> dec_smeared("smearjet"); //!


// rel 22 addition                                                                                                                                                                                        
#define CHECK( ARG )                                     \
  do {                                                   \
  const bool result = ARG;                             \
  if( ! result ) {                                     \
  ::Error( APP_NAME, "Failed to execute: \"%s\"",    \
	   #ARG );                                   \
  return 1;                                          \
  }                                                    \
  } while( false )



/// Helper macro for checking xAOD::TReturnCode return values 
#define EL_RETURN_CHECK( CONTEXT, EXP )  \
   do { \
      if( ! EXP.isSuccess() ) { \
         Error( CONTEXT, \
                XAOD_MESSAGE( "Failed to execute: %s" ), \
                #EXP );  \
         return EL::StatusCode::FAILURE;  \
      } \
   } while( false )



inline bool ptsorter( const xAOD::IParticle* j1, const xAOD::IParticle* j2 ) {
  return ( j1->pt() > j2->pt() ); }

inline bool ptfloatsorter( TLorentzVector j1,  TLorentzVector j2 ) {
  return ( j1.Pt() > j2.Pt() );
}


class DeltaRSort {
public:
  DeltaRSort(const xAOD::IParticle* obj) : m_obj(obj){} ;

  bool operator() (const xAOD::IParticle* o1, const xAOD::IParticle* o2) const {
    return (o1 && o2) ? ( o1->p4().DeltaR(m_obj->p4()) < o2->p4().DeltaR(m_obj->p4()) ) : (o1 != 0);
  }
private:
  const xAOD::IParticle* m_obj;
};


class MasterShef : public EL::Algorithm
{

private:
  
  std::vector<TH1D* > m_cutFlow; //!
  std::vector<TH1D* > m_cutFlowWgt; //!
  std::vector<TH1D* > m_cutFlowWgt_muR05_muF10; //!
  std::vector<TH1D* > m_cutFlowWgt_muR20_muF10; //!
  std::vector<TH1D* > m_cutFlowWgt_muR10_muF05; //!
  std::vector<TH1D* > m_cutFlowWgt_muR10_muF20; //!
  std::vector<TH1D* > m_cutFlowWgt_muR05_muF05; //!
  std::vector<TH1D* > m_cutFlowWgt_muR20_muF20; //!
  std::vector<TH1D* > m_cutFlowWgt_ISRmuRfac10_FSRmuRfac20; //!
  std::vector<TH1D* > m_cutFlowWgt_ISRmuRfac10_FSRmuRfac05; //!
  std::vector<TH1D* > m_cutFlowPUWgt; //!
  
  TH1D* htime; //!
  TH1D* hmeantime; //!

  std::map<TString, TH1D*> m_HjetSmearing; //!

  std::vector<std::string> m_cuts; //!
  double m_AnalysisWeight,m_pileupweight,m_pileupweightUP,m_pileupweightDOWN,m_TriggerWeight,m_TriggerWeight2;//!
  double m_averageIntPerXing;//!

  bool m_TruthJetsPresent; //!

  double m_jvtweight,m_jvtweightUP,m_jvtweightDOWN,m_btagweight,m_btagweightBUP,m_btagweightBDOWN,m_btagweightCUP,m_btagweightCDOWN; //!
  double m_btagweightLUP,m_btagweightLDOWN,m_btagweightExUP,m_btagweightExDOWN,m_btagweightExCUP,m_btagweightExCDOWN; //!


  double m_TriggerSF; //!
  double m_muonsTrigSF; //!
  double m_electronsTrigSF; //!

  ULong64_t m_pileupweightHash; //!
  std::map<TString,float> TriggerWeights; //!
  std::vector<TString> sjtriggers; //!
  TString xStream;  //!
  //truth variables
  double m_VbosonPt,m_VbosonEta,m_VbosonPhi,m_VbosonE; //!
  double m_nuMET,m_nuMETv2,m_nuMETv3;//!
  bool m_passSeedSelection = false;
  bool m_passNewSeedSelection = false;
  bool m_passHTSeedSelection = false;
  bool m_passETSeedSelection = false;
  bool m_passETSeedSelectionv2 = false;
  bool m_passSeedSelectionQRT = false;
  bool m_passSeedSelectionCU = false; 

  bool m_isEnvelope=false;
 
  int m_JESNuisanceParameterSet=1;

  // ttbar+HF classification
  int m_ttbarHF = -999;
  int m_ttbarHF_ext = -999; 
  int m_ttbarHF_prompt = -999;

  double m_isElTrigMatched, m_isMuTrigMatched;//!


  std::vector<int> m_pdgCodes; 
  #include "ntupVar.h"

  std::string m_path = "";  

  //event info
  const xAOD::EventInfo* eventInfo;//!


 //
  // These are containers that we are using 
  // throughout the code
  //
  //met
  xAOD::MissingETContainer* m_metContainer; //!
  xAOD::MissingETAuxContainer* m_metContainerAux; //!

  std::vector<xAOD::MissingETContainer*> m_smrMET; //!
  std::vector<xAOD::MissingETAuxContainer*> m_smrMETAux;//!!

  std::vector<xAOD::JetContainer*> m_smrJets; //
  std::vector<xAOD::ShallowAuxContainer*> m_smrJetsAux; //


  // simple met
  xAOD::MissingETContainer* m_simpleMETCont = new xAOD::MissingETContainer(); //!
  xAOD::MissingETAuxContainer* m_simpleMETContAux = new xAOD::MissingETAuxContainer(); //!


  xAOD::MissingETContainer* m_simpleCSTMETCont; //!
  xAOD::MissingETAuxContainer* m_simpleCSTMETContAux; //!


  xAOD::TStore* store; //!
  xAOD::TEvent* event; //!

  int m_ntrk_global; //!


  double  m_metsigET; //!
  double  m_metsigHT; //!
  double  m_metsig; //!
  double  m_metsig_default; //!
  double  m_metsig_binflate; //!
  double  m_metsig_run1JER; //!
  double  m_metsigST; //!
  double  m_metsigLepJetST; //!
  double  m_metsigLepJetResol;//!

  double m_pxmiss ; //!
  double m_pymiss ; //!
  double m_etmiss ; //!

  double m_px_lepjet_miss ; //!
  double m_py_lepjet_miss ; //!

  double m_pxmiss_jet ; //!
  double m_pymiss_jet ; //!
  double m_etmiss_jet ; //!

  double m_pxmiss_mu ; //!
  double m_pymiss_mu ; //!
  double m_etmiss_mu ; //!

  double m_pxmiss_el ; //!
  double m_pymiss_el ; //!
  double m_etmiss_el ; //!

  double m_pxmiss_y ; //!
  double m_pymiss_y ; //!
  double m_etmiss_y ; //!

  double m_pxmiss_softTrk ; //!
  double m_pymiss_softTrk ; //!
  double m_etmiss_softTrk ; //!

  double m_pxmiss_NonInt ; //! 
  double m_pymiss_NonInt ; //!
  double m_etmiss_NonInt ; //!  

  double m_pxmiss_tau; //!
  double m_pymiss_tau; //!
  double m_etmiss_tau; //!

  // fake met!
  double m_pxmiss_prime; //! 
  double m_pymiss_prime; //!
  double m_etmiss_prime; //!
   



 // simple met variables!

  double m_pxmiss_simple; //!
  double m_pymiss_simple; //!
  double m_etmiss_simple; //!

  double m_pxmiss_simpleCST; //!
  double m_pymiss_simpleCST; //!
  double m_etmiss_simpleCST; //!

  double m_pxmiss_simple_jet; //!
  double m_pymiss_simple_jet; //!
  double m_etmiss_simple_jet; //!

  double m_pxmiss_simpleCST_jet; //!
  double m_pymiss_simpleCST_jet; //!
  double m_etmiss_simpleCST_jet; //!

  double m_pxmiss_simple_mu; //!
  double m_pymiss_simple_mu; //!
  double m_etmiss_simple_mu; //!
  
  double m_pxmiss_simpleCST_mu; //!
  double m_pymiss_simpleCST_mu; //!
  double m_etmiss_simpleCST_mu; //!
  
  double m_pxmiss_simple_softTrk; //!
  double m_pymiss_simple_softTrk; //!
  double m_etmiss_simple_softTrk; //!
  
  double m_pxmiss_simple_softClus; //!
  double m_pymiss_simple_softClus; //!
  double m_etmiss_simple_softClus; //!

  double m_simpleTST_sumet; //! 
  double m_simpleTST_sumet_jet; //! 
  double m_simpleTST_sumet_mu; //! 
  double m_simpleTST_sumet_softTrk; //!   

  double m_simpleCST_sumet; //! 
  double m_simpleCST_sumet_jet; //! 
  double m_simpleCST_sumet_mu; //! 
  double m_simpleCST_sumet_softClus; //! 




  double m_trackMET_px; //!
  double m_trackMET_py; //!
  double m_trackMET_pt; //!
  
  double m_pxmiss_inv ; //!
  double m_pymiss_inv ; //!
  double m_etmiss_inv; //!
  double m_sumet;   //!
  double m_sumet_jet;   //!
  double m_sumet_el;   //!
  double m_sumet_y;   //!
  double m_sumet_mu;   //!
  double m_sumet_tau;//!
  double m_sumet_softTrk;   //!
  double m_sumet_softClu;   //!
  double m_sumet_NonInt;   //!

  double m_cellMET; //!
  double m_mhtMET; //!
  

  TVector2 m_smrMetVec; //!
  double m_SumEtSMR; //!

  //RJR variables for stop0L selection:
  

  double m_PTISR,m_RISR,m_MS,m_MV,m_dphiISRI,m_pTbV1,m_NbV,m_NjV,m_pTjV4;
  double m_NbISR,m_NjISR;
 

  //electrons
  xAOD::ElectronContainer* m_electrons_copy; //!
  xAOD::ShallowAuxContainer* m_electrons_copyaux; //!
  
  xAOD::ElectronContainer* m_baselineElectronsBeforeOR; //! 
  xAOD::ElectronAuxContainer* m_baselineElectronsBeforeORAux; //!
  
  xAOD::ElectronContainer* m_signalElectronsBeforeOR; //!

  xAOD::ElectronContainer* m_baselineElectrons; //!
  xAOD::ElectronContainer* m_baselineElectrons_lowPt; //!
  xAOD::ElectronContainer* m_signalElectrons; //!

  //muons
  xAOD::MuonContainer* m_muons_copy; //!
  xAOD::ShallowAuxContainer* m_muons_copyaux; //!
  xAOD::MuonContainer* m_muonsTR; //!
  xAOD::ShallowAuxContainer* m_muonsTRaux; //!
  
  xAOD::MuonContainer* m_baselineMuonsBeforeOR; //!
  xAOD::MuonAuxContainer* m_baselineMuonsBeforeORAux; //!
  xAOD::MuonContainer* m_signalMuonsBeforeOR; //!
  xAOD::MuonContainer* m_cosmicMuons; //!
  xAOD::MuonContainer* m_badMuons;   //!
  xAOD::MuonContainer* m_baselineMuons; //!
  xAOD::MuonContainer* m_baselineMuons_lowPt; //!
  xAOD::MuonContainer* m_signalMuons; //!
  xAOD::MuonContainer* m_baseMuonsTR; //!


  //truthjets
  xAOD::JetContainer* m_truthjets; //! 
  xAOD::ShallowAuxContainer* m_truthjetsAux; //! 
  
  xAOD::JetContainer* m_recoJetsTR; //!

  //jets
  xAOD::JetContainer* m_jets_nominal; //!
  xAOD::ShallowAuxContainer* m_jets_nominalAux; //!
  xAOD::JetContainer* m_jets_copy; //!
  xAOD::ShallowAuxContainer* m_jets_copyaux; //!
  xAOD::JetContainer* m_fatjets_copy; //!
  xAOD::ShallowAuxContainer* m_fatjets_copyaux; //!
  xAOD::JetContainer* m_signalJetsBeforeOR; //!
  xAOD::JetAuxContainer* m_signalJetsBeforeORAux; //!  
  xAOD::JetContainer* m_signalJets; //!
  xAOD::JetContainer* m_signalLepJets; //!
  xAOD::JetContainer* m_signalJets35; //!
  xAOD::JetContainer* m_baselineJetsAfterOR; //!
  xAOD::JetAuxContainer* m_baselineJetsAfterORAux; //! 

  xAOD::JetContainer* m_rMapRecoJets; //!

  xAOD::IParticleContainer* m_signalJets_recl; //!  
  xAOD::IParticleContainer* m_lep_recl; //!
  xAOD::JetAuxContainer* m_signalJetsAux; //!
  xAOD::JetContainer* m_signalAntiKt4TruthJets; //!
  xAOD::JetContainer* m_BJets; //!
  xAOD::JetContainer* m_CJets; //!
  xAOD::JetContainer* m_BJetsFlat; //!
  xAOD::JetContainer* m_nonBJets; //!
  xAOD::JetContainer* m_badJets; //!
 
  //save a global variable to track which type of btagging we're using 
  std::string m_btagType; //!

  //track jets

  std::string m_trackjet_cont="AntiKt2PV0TrackJets";
  xAOD::JetContainer* m_trackjetsKt2; //!
  xAOD::ShallowAuxContainer* m_trackjetsKt2aux; //!
  
  xAOD::JetContainer* m_trackjetsKt4; //!
  xAOD::ShallowAuxContainer* m_trackjetsKt4aux; //!
     
  xAOD::JetContainer* m_signalTrackJets; //!
  xAOD::JetContainer* m_signalBTrackJets; //!

  
  //soft btagging vertices
  const xAOD::VertexContainer *m_softBVertices_Tight; //!
  const xAOD::VertexContainer *m_softBVertices_Medium; //!
  const xAOD::VertexContainer *m_softBVertices_Loose; //!
  xAOD::VertexContainer *m_goodSBV_Tight; //!
  xAOD::VertexContainer *m_goodSBV_Medium; //!
  xAOD::VertexContainer *m_goodSBV_Loose; //!
  xAOD::VertexAuxContainer *m_goodSBV_Tightaux; //!
  xAOD::VertexAuxContainer *m_goodSBV_Mediumaux; //!
  xAOD::VertexAuxContainer *m_goodSBV_Looseaux; //!
  


  const xAOD::VertexContainer* m_primVertex; //!


  // Fat jets
  xAOD::JetContainer* m_rcjets_kt15; //!
  xAOD::ShallowAuxContainer* m_rcjets_kt15aux; //!
  xAOD::JetContainer* m_rcjets_kt12; //!
  xAOD::ShallowAuxContainer* m_rcjets_kt12aux; //!
  xAOD::JetContainer* m_rcjets_kt10; //!
  xAOD::ShallowAuxContainer* m_rcjets_kt10aux; //!
  xAOD::JetContainer* m_rcjets_kt8; //!
  xAOD::ShallowAuxContainer* m_rcjets_kt8aux; //!

  xAOD::JetContainer* m_smrrcjets_kt12; //!
  xAOD::JetContainer* m_smrrcjets_kt8; //!


  //photons
  xAOD::PhotonContainer* m_photons_copy; //!
  xAOD::ShallowAuxContainer* m_photons_copyaux; //!
  xAOD::PhotonContainer* m_signalPhotonsBeforeOR; //!
  xAOD::PhotonAuxContainer* m_signalPhotonsBeforeORAux; //!
  xAOD::PhotonContainer* m_signalPhotons; //!
  xAOD::PhotonContainer* m_baselinePhotons; //!


  //taus
  xAOD::TauJetContainer* m_taus_copy; //!
  xAOD::ShallowAuxContainer* m_taus_copy_aux; //!
  xAOD::TauJetContainer* m_signalTausBeforeOR; //!
  xAOD::ShallowAuxContainer* m_signalTausBeforeORaux; //!
  xAOD::TauJetContainer* m_signalTaus; //!

  xAOD::TauJetContainer* m_AODTaus; //!
  xAOD::ShallowAuxContainer* m_AODTausAux; //!

  //truth objects
  xAOD::TruthParticleContainer* m_truthParticles; //!
  xAOD::ShallowAuxContainer* m_truthParticlesAux; //!
  xAOD::TruthParticleContainer* m_truthMuons; //!
  xAOD::TruthParticleAuxContainer* m_truthMuonsAux; //!
  


  // Systematic variations:
  std::vector<ST::SystInfo> m_systList; //!
  unsigned int m_systNum; //!
  unsigned int m_smrNum; //!
  
public:
  std::vector<std::string> m_systNameList; 

public:
  typedef  bool (MasterShef::*xAODMemFn)(bool); // Needed for the python configuration
  typedef  bool (MasterShef::*xAODMemFill)(bool,TTree*); // Needed for the python configuration

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  int m_eventCounter; //!
  double m_cutLevel; //!
  
  int m_systBunch = -1;
  int m_systdivisions = 5;

  double m_ctagging_fb = 0.28;
  double m_ctagging_cut = 1.315;

  std::string m_basePath = "";

  TStopwatch* clock; //!

  std::unique_ptr<JetSmearing::PreScaleTool> m_prescaleTool; //!
  std::unique_ptr<JetSmearing::JetMCSmearingTool> m_jetMCSmearing; //!

  //the main lad
  std::unique_ptr<ST::SUSYObjDef_xAOD> m_objTool; //! 
  
  asg::AnaToolHandle<PMGTools::IPMGTruthWeightTool> m_weightTool; //!

  //  std::vector<ST::SUSYObjDef_xAOD*> m_susyTools; //!

  /*
  asg::AnaToolHandle<IMETSignificance> m_metSignif; //!
  asg::AnaToolHandle<IMETSignificance> m_metSignif_maxRes; //!
  asg::AnaToolHandle<IMETSignificance> m_metSignif_bjet; //!
  asg::AnaToolHandle<IMETSignificance> m_metSignif_run1JER; //!
  asg::AnaToolHandle<IMETSignificance> m_metsigJetResol; //!
  */


  NSubjettinessTool *nsubjetinessTool; //!
  NSubjettinessRatiosTool *nsubjetinessRatiosTool; //!
  QwTool *qwTool; //!
  JetChargeTool *jchargeTool; //!
  DipolarityTool *dipTool; //!
  KTSplittingScaleTool *splitTool; //!

  std::unique_ptr<SmoothedWZTagger> m_smoothedWTagger50;    //!  
  std::unique_ptr<SmoothedWZTagger> m_smoothedWTagger80;    //! 
  std::unique_ptr<SmoothedWZTagger> m_smoothedZTagger50;    //!  
  std::unique_ptr<SmoothedWZTagger> m_smoothedZTagger80;    //! 
  //std::unique_ptr<SmoothedTopTagger> m_smoothedTopTagger50;    //!   
  //std::unique_ptr<SmoothedTopTagger> m_smoothedTopTagger80;    //! 
  
  std::unique_ptr<JSSWTopTaggerDNN> m_DNNTopTagger50;    //! 
  std::unique_ptr<JSSWTopTaggerDNN> m_DNNTopTagger80;    //! 
    

  GoodRunsListSelectionTool *m_grl; //!
  CP::PileupReweightingTool *m_PRWtool; //!

  TMctLib* mcttool; //!



  //TauDecay* taudec; //!
  TVector* m_constcut; //!
  double m_cutval_MV2c10F85 = -99;
  double m_cutval_DL1F85 = -99;
  double m_cutval_DL1F77 = -99;
  TString m_cutname_MV2c10F85 = "MV2c10/AntiKt4EMTopoJets/FixedCutBEff_85/cutvalue";
  TString m_cutname_DL1F85 = "DL1/AntiKt4EMTopoJets/FixedCutBEff_85/cutvalue";
  TString m_cutname_DL1F77 = "DL1/AntiKt4EMTopoJets/FixedCutBEff_77/cutvalue";
  BTaggingSelectionTool *m_btagSelTool_MV2c10_H85; //!
  BTaggingSelectionTool *m_btagSelTool_MV2c10_H77; //!
  BTaggingSelectionTool *m_btagSelTool_MV2c10_H70; //!
  BTaggingSelectionTool *m_btagSelTool_DL1_H85; //!
  BTaggingSelectionTool *m_btagSelTool_DL1_H77; //!
  BTaggingSelectionTool *m_btagSelTool_DL1_H70; //!
 
  


  double m_ElePt   = 0;
  double m_EleEta  = 0;
  double m_MuonPt   = 0;
  double m_MuonEta  = 0;
  double m_PhotonPt   = 0;
  double m_PhotonEta  = 0;
  double m_JetPt = 0;
  double m_JetEta = 0;


   // This are initialised in the python to be passed to C++
  ST::ISUSYObjDef_xAODTool::DataSource m_dataSource; 
   xAOD::JetInput::Type m_jetInputType; 
   std::string m_EleId; 
   std::string m_TauId;    
   std::string m_EleIsoWP;
   std::string m_BadJetType = "LooseBad";

   //default the continuous working point to zero, so normal btagging is not affected
   int m_contWP = 0;
   //also to make sure normal btagging is not affected
   int m_btagBin = 1;

   bool m_useSimpleMET = false;

   bool m_usePhotons = true;
   bool m_useElectrons = true; 
   bool m_useMuons = true; 

   bool m_useTaus = false; 
   bool m_useFatJets = false; 
   bool m_useTrackJets = false;
   bool m_useSBtaggingVertexing = false;

   bool m_saveAllSbWPs = false;
   bool m_doBJetInflation=false;

   //if this is true, then we find truth b-hadrons and add them to track jets
   //You must also enable the treemode "fillBHadronTruthVariables" to get useful ntuples.
   bool m_useHFTruthHadrons = false;
  
   bool m_useAntiKtRcJets = false; 
   bool m_useRCJetSS = false;
   bool m_useJetConstituents = false;
   
   bool m_useAntiKt10Jets_RC = false;  // reclustered R=1.0 "Fat jets"
   bool m_useAntiKt10Jets_ST=false; // Large R (R=1.0) jets from container.
   
   bool m_saveExperimentalSyst = true;
   bool m_saveJetCleaningVars = false;
   bool m_saveFatJetExtraVariables=false;

   bool m_doLepJet = false;

   bool m_useMCTruthClassifier=false;

   bool m_doTruth = false;
   bool m_Jets = true;
   bool m_dumpTruthJets = false;
   bool m_doxAOD = true;
   bool m_noPU = false;
   bool m_PRWFromTop = false;
   bool m_doTtbarHFClassification = true;
  

   //   bool m_useTausFromAOD=false;
   //Enable m_isPFlowResearch, via SUSY.py, if you would like to use the additional features useful for doing R&D studies with pflow jets
   //Note this does NOT enable pflow jets or do anything specific to pflow jets only
   bool m_isPflowResearch=false;
   bool m_isPflow = false;
  

   bool m_AddNeutMuTruthJets = false;
   bool m_AddRecoMuons = true;
   double m_TruthReco_dRMatching = 0.3;
   double m_unique_dRMatching = 0.6;
   double m_ConeSize=0.4;
   bool m_useTriggerWeight = false;
   int m_debug = 0;
   bool ADebugging = false;
   bool m_ExtraSysMaps=false;
   bool m_TailWeightMaps=false;

   bool m_randomlyBTagTwoJets=false;


   std::string m_config = "MasterShef/sbottom.conf";
   std::vector<std::string>  m_sysconfig;
   std::vector<std::string>  m_sysconfig_name;
   
   std::string m_BtagWP = "FixedCutBEff_77";
   std::string m_BtagWP_OR = "FixedCutBEff_80";

   int m_showerType = 0 ;

   // Trigger
   std::string m_applyTrigger="";
   
   std::string m_triggerListFile;
   std::vector<std::string> m_triggerList;
   std::vector<std::string> m_triggerNtup;
   std::map<std::string,bool> m_trigMatch;
   

   // Object definition criteria
   // Electrons
   double m_baselineElPtCut;
   double m_baselineElEtaCut;
   double m_signalElPtCut;
   // Muons
   double m_baselineMuPtCut;
   double m_baselineMuEtaCut;
   double m_signalMuPtCut;

   // Jets
   double m_signalJetPtCut;
   double m_signalJetEtaCut;
   
   // MET
   bool m_doTST;     

   // Python configurator
   //map of all possible selection modules
   std::map<std::string,xAODMemFn> m_allSelMods;//!
   //vector of selection modules to use
   std::vector<std::string> m_userSelMods; 
   xAODMemFn m_funExec[100];  //! 
   int m_totCuts; //!


   //Python control of which variables to dump
   
   //map of all possible tree fill modules
   std::map<std::string,xAODMemFill> m_allTreeMods;//!
   //vector of tree fill modules to use
   std::vector<std::string> m_userTreeMods;   
   xAODMemFill m_funFill[100];  //! 
   int m_totFills; //!


   // map of the branches that are general but not needed in an analysis.
   std::map<std::string,std::vector<TString> > m_treeSkim; //!
   void SkimTreeBranches(TTree* tree,std::string skimPolicy);
   void DefineSkimPolicies();
   std::string m_skimPolicy;

   //print loaded fills
   void printFills(){for(auto str: m_userTreeMods) std::cout << str << std::endl;}
   void printSelections(){for(auto str: m_userSelMods) std::cout << str << std::endl;}

   void printAllPossibleFills(){for(auto str: m_allTreeMods) std::cout << str.first << std::endl;}
   void printAllPossibleSelections(){for(auto str: m_allSelMods) std::cout << str.first << std::endl;}







   //
   // These are variables that would go in the ntuple
   int m_numVtx; //!
   bool m_isMC;  //!
   bool m_saveShowerWeights = false;
   bool m_is_p2411 = true; 
   bool m_is_exot14 = false; 


   // defining the output file name and tree that we will put in the output ntuple, 
   // also the one branch that will be in that tree 
   std::string m_outputName;
   std::string m_sysName;
   int m_ntupLevel; 
   int m_ntupLevel_sys; 
   
   std::vector<TTree*> m_tree; //!
   std::vector<TTree*> m_RMaptree; //!
   TTree* m_MyMetaData; //!
 

   // These are needed by the event dumpers
   //std::ofstream m_cutDumpStr; //!
   int m_cutDumpLvl;

   // the eventDump is like a selector
   std::vector<unsigned int> m_eventDump;
   bool eventDump(bool init=false); 

  // this is a standard constructor
  MasterShef ();

  // Prepare cuts
  bool prepareCuts();
  //branch filling functions
  bool prepareTrees();


  
  bool fillCommonVariables(bool init, TTree*tree);
  bool fillExtraRecoVariables(bool init, TTree*tree);
  bool fillExtraTruthVariables(bool init, TTree*tree);
  // Fill LepJet varialbes
  bool fillLepJetVariables(bool init, TTree*tree);
  //Fill truth b-hadron 4-vectors
  bool fillHFHadronTruthVariables(bool init, TTree *tree);
  bool fillHFClassVariables(bool init, TTree*tree);
  bool fillExtraMETVariables(bool init, TTree*tree);
  bool fillTruthJets(bool init, TTree*tree);
  bool fillTruthTaus(bool init, TTree*tree);
  bool fillTruthLeptons(bool init, TTree*tree);
  //build an Rmap for jetsmearing
  bool fillRMap(bool init, TTree*tree);


  bool fillTruthJetConstituents(bool init, TTree*tree);
 
  bool fillJetConstituents(bool init, TTree*tree);
  bool fillTrackJetConstituents(bool init, TTree*tree);

  bool fillFatJetVariables(bool init, TTree*tree);
  bool fillRCFatJetVariables(bool init, TTree*tree); 
  bool fillSBVVariables(bool init, TTree*tree); 
  bool fillTrackJetVariables(bool init, TTree*tree); 
  bool fillSmearingVariables(bool init, TTree*tree);

  bool fillxAODJets(bool init, TTree*tree);


  // method to estimate the mass of the jet with its constituents. 
                                                                                                                                                                             
  void decorateRCFlav(xAOD::JetContainer* jets,double radius);
  void calcConstituentsMass(xAOD::Jet* jet);
  void calcTrackAssistedMass(xAOD::Jet* jet);
  void calcMassPFlowJet(xAOD::Jet* jet);
  #ifdef USE_RESTFRAMES
  void GetRJRVariables();
  #endif
  
  // These are functions called by execute
  //
  bool initialCuts(bool init=false);
  bool prepareObjects(bool init=false);

  //methods for preparing baseline objects before any OR procedure
  bool prepareBaselineJetsBeforeOR();
  bool prepareBaselineFatJetsBeforeOR();
  bool prepareBaselineLeptonsBeforeOR(); 
  bool prepareBaselinePhotonsBeforeOR(); 
  
  //methods for preparing signal objects
  bool prepareLeptons();
  bool preparePhotons();
  bool prepareJets();
  bool prepareLepJets();
  bool prepareTrackJets();
  //bool prepareFatJets(); //actual fat jets, no OR used yet so not yet defined
  bool prepareRCJets(); //reclustered antikt4 jets

  //soft btagging vertices
  bool prepareSBV();

  //prepare the met separately 
  bool prepareMET();
  bool prepareMETTST();
  bool prepareMETCST();
  bool prepareTrackMET();
  bool prepareSimpleMET();
  bool prepareComplexMET();
  bool prepareLepJetMET();

  bool prepareTruthObjects(bool init=false);

  // These are the cleaning selections
  bool cleanJets(bool init=false);  // Jet cleaning. No parameters
  bool cleanTightJets(bool init=false);  // TightJet cleaning. No parameters
  bool cleanMuons(bool init=false); // Muon cleaning. No parameters
  bool cosmic(bool init=false);     // cosmic selection. No parameters
  
  bool triggerMatching(bool init=false);     // cosmic selection. No parameters


  // One lepton selection
  // parameters:
  bool     m_Sel1L_Sgnl; //  True if using signal leps, False if using baseline
  double   m_Sel1L_PtMnE; //  minimum electron Pt
  double   m_Sel1L_PtMxE; //  maximum electron Pt
  double   m_Sel1L_PtMnM; //  minumum muon Pt
  double   m_Sel1L_PtMxM; //  maximum muon Pt
  bool  Sel1L(bool init=false);


  double   m_SelnBL_Mn; //  minumum # of baseline leptons
  double   m_SelnBL_Mx; //  maximum # of baseline leptons
  bool  SelNBaselineLep(bool init=false);
  bool  Sel0Lep(bool init=false);



  bool Sel1Ph(bool init=false);

  // Two leptons or two b selection
  // parameters:
  double   m_SelSbottom_PtMnE; //  minimum electron Pt  
  double   m_SelSbottom_PtMxE; //  maximum electron Pt
  double   m_SelSbottom_PtMnM; //  minumum muon Pt
  double   m_SelSbottom_PtMxM; //  maximum muon Pt
  bool  SelSbottom(bool init=false);
  bool  SelSbottomMultib(bool init=false);
  bool  SelbbMeT(bool init=false);  
  bool  SelttZ3L(bool init=false);
  bool  SelttZ2L(bool init=false);
  bool  SelttZ(bool init=false);
  bool  SelDMbb(bool init=false);
  bool  SelSbottomTight(bool init=false);
  double CalcMTbmin();
  double CalcMTcmin();
  double CalcLeadingKt12FatJetMass();
  bool  SelStop0L(bool init=false);
  bool  SelStop0LTight(bool init=false);
  bool  SelStop4Body(bool init=false);
  double Calc_4b_PTISR(bool IncludeLepton=false);
  bool  SelStop4BodyTight(bool init=false);
  bool  SelGammaBalance(bool init=false);
  bool  SelBosonBalance(bool init=false);
  bool  SelDiJet(bool init=false);
  bool  Sel2L(bool init=false);
  bool  SelSeedEvents(bool init=false);
  bool  SelPseudoEvents(bool init=false);
  bool  SelPseudoEventsDiJet(bool init=false);



  bool  SelNL(bool init=false);
  int   m_SelnL_Mn; //  minumum # of leptons
  int   m_SelnL_Mx; //  maximum # of leptons



  // Missing ET selection
  double m_SelMET_PtMn; // minimum met
  double m_SelMET_PtMx; // maximum met
  bool SelMET(bool init=false);
  bool SelTruthMET(bool init=false);
  bool SelJetMET(bool init=false);


  // HT selection
  double m_SelHT_Mn; // minimum met
  double m_SelHT_Mx; // maximum met
  bool SelHT(bool init=false);

  bool SelOnZ(bool init=false);

  // mct selection
  double m_SelMct_Mn; // minimum mct
  double m_SelMct_Mx; // maximum mct
  bool SelMct(bool init=false);



  // Mt selection
  double m_SelMt_Mn; // minimum mt
  double m_SelMt_Mx; // maximum mt
  bool SelMt(bool init=false);

  // jet selection
  unsigned int   m_SelJet_nJMn; // Min number of jets
  unsigned int   m_SelJet_nJMx; // Max number of jets
  std::vector<float> m_SelJet_ptMn; // Min Pt of jet
  std::vector<float> m_SelJet_ptMx; // Max Pt of jet
  bool SelJet(bool init=false);

  // taujet selection
  unsigned int   m_SelTauJet_nJMn; // Min number of taujets
  unsigned int   m_SelTauJet_nJMx; // Max number of taujets
  std::vector<float> m_SelTauJet_ptMn; // Min Pt of taujet
  std::vector<float> m_SelTauJet_ptMx; // Max Pt of taujet
  bool SelTauJet(bool init=false);




  bool SelSJTriggers(bool init=false);
  bool SelSBTriggers(bool init=false);


  // Number of bjets selection
  unsigned int   m_SelNBJet_nJMn; // Min number of b-jets
  unsigned int   m_SelNBJet_nJMx; // Max number of b-jets
  bool SelNBJet(bool init=false);   
  bool SelLeadB(bool init=false);   
  // Number of truth bjets selection
  unsigned int   m_SelNBTruthJet_nJMn; // Min number of truth b-jets
  unsigned int   m_SelNBTruthJet_nJMx; // Max number of truth b-jets
  bool SelNBTruthJet(bool init=false);   
  
  
  // jet smearing selection
  bool jetSmearing();
  bool rMapAnalyses();
  bool passSeedSelection();
  bool m_doJetSmearing = false;
  bool m_doJetSmearingSkim = false;
  bool m_doJetSmearingPhiCorrections=false;

  std::string m_JS_bmap_file = "/data/JetSmearing/MC15/R_map2016_btag_OP77_EJES_p2666.root";
  std::string m_JS_bvetomap_file = "/data/JetSmearing/MC15/R_map2016_bveto_OP77_EJES_p2666.root";
  std::string m_JS_map_name = "responseEJES_p2666";


  double m_METsigSeedCut = 0.7; 
  unsigned int m_nsmears = 100;
  std::vector<double> m_TailSysVals; //!
  std::vector<std::vector<double>> m_TailWeights; //!


  xAOD::MissingET* seed_met; //!
  xAOD::MissingET* smr_met; //!
  xAOD::MissingET* data_met; //!


  //strings for triggermatching
  std::string m_el_trigmatch_trigger;
  std::string m_mu_trigmatch_trigger;
  std::string m_muonsTrigSF_trigger;

  //string for muontrigsf


  // tcMET block 

  bool SeltcMET(bool init=false);
  bool SeltcMETTight(bool init=false);
  bool retrieveCharmTaggers();


  // Functions to calculate global quantities
  double calcMt();
  
  double getWparton(const xAOD::Jet *theJet, std::vector<const xAOD::TruthParticle*> partons);

  std::vector<fastjet::PseudoJet> jetConVecToPseudojet ( xAOD::JetConstituentVector input);


  bool calcJSS(xAOD::Jet &jet);


  // Methods to deal with the cuts and their bookkeeping
  void passCut();
  void failCut();
  void fillTree(int jj=0);
  

  double GetTriggerWeight(int runnumber);

  JetToolRunner* m_jetRecTool_kt15; //!
  JetToolRunner* m_jetRecTool_kt12; //!
  JetToolRunner* m_jetRecTool_kt10; //!
  JetToolRunner* m_jetRecTool_kt8; //!

  //JetToolRunner* m_jetRecTool_smrkt12; //!
  //JetToolRunner* m_jetRecTool_smrkt8; //!


  //consistuent retriever
  JetConstituentsRetriever* m_jetConstRetTool; //!
  JetPseudojetRetriever* m_jetPseudoRetTool; //!

  std::map<std::string, JetToolRunner*> m_jetRecTool_smrkt12; //!
  std::map<std::string, JetToolRunner*> m_jetRecTool_smrkt8; //!

  // track selection tool 

  InDet::InDetTrackSelectionTool * m_trackSelTool; //!

  //method to recluster jets
  bool GetJetToolRunner(JetToolRunner *& tool, double jetradius, std::string inputcontainer, std::string outputcontainer); //!

  // method to get truth information
  bool getTruthElectrons(double m_ElectronBaselinePt,double m_ElectronBaselineEta); //!
  bool getTruthMuons(double m_MuonBaselinePt, double m_MuonBaselineEta); //!  
  bool getTruthPhotons(double m_PhotonBaselinePt,double m_PhotonBaselineEta); //! 
  bool getTruthJets(double m_JetPt, double m_JetEta); //! 
  bool getTruthLeptonsInRecoNtuples(bool fillTruthLeptonsInReco=false,double ele_pT_thresh=-1.0,double mu_pT_thresh=-1.0); //!
  void addNeutNuToVector(TLorentzVector &full4MomTruth);//!

  int LeptoQuarkDecayInfo();
  int TopCharmDecayInfo(); // Adding stop quark decay information
  int ttbarClassification();
  std::string ttbarISRClassification();
  std::string wbosonChildren(const xAOD::TruthParticle* top);

  bool getTruthTaus(double m_TauPt, double m_TauEta); //! 
  bool getTruthHFHadrons();//!
  int matchTruthHadrons(const xAOD::TruthParticleContainer& truthHadrons, TLorentzVector thePart); //!
  int matchTruthHadrons_3D(const xAOD::TruthParticleContainer& truthHadrons, TVector3 SVdirection); //Mario 
  bool getTMuons(); //!
  bool isFromTopW(const xAOD::TruthParticle* particle); //!
  bool  getAncestors(const xAOD::TruthParticle* p,std::vector< const xAOD::TruthParticle* >& ancestors); //!
  const xAOD::TruthParticle* getLastTruthParticle(const xAOD::TruthParticle* particle); //!
   
  double z0sinTheta(const xAOD::TruthParticle* particle); //!
  MCTruthClassifier *m_MCtruthClassifier; //!
  
  // Now fill the ntuple
  EL::StatusCode fillNtup();
 
  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode initializeTools ();
  virtual EL::StatusCode initializeTruthTools ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();




  // this is needed to distribute the algorithm to the workers
  ClassDef(MasterShef, 1);
};

#endif
