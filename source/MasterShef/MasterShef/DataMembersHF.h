#ifndef MasterShef_DataMembersHF_H
#define MasterShef_DataMembersHF_H


#include <vector>
#include <string>

/** @class DataMemberHF
 ** to be used with HF Classification Tool
 ** independantly of the framework used
 */

class DataMembersHF{
  
 public:

  DataMembersHF();
  DataMembersHF( const DataMembersHF &dm );
  virtual ~DataMembersHF();

  //Declaration of needed variables as public data members
  int EventNumber;
  int mc_channel_number;

  int mc_n;
  std::vector<float>   *mc_pt;
  std::vector<float>   *mc_eta;
  std::vector<float>   *mc_phi;  
  std::vector<int>     *mc_status;
  std::vector<int>     *mc_barcode;
  std::vector<int>     *mc_pdgId;
  std::vector<float>   *mc_charge;
  std::vector<std::vector<int> > *mc_parent_index;
  std::vector<std::vector<int> > *mc_child_index;
  
  int jet_AntiKt4Truth_n;
  std::vector<float>   *jet_AntiKt4Truth_E;
  std::vector<float>   *jet_AntiKt4Truth_pt;
  std::vector<float>   *jet_AntiKt4Truth_eta;
  std::vector<float>   *jet_AntiKt4Truth_phi;
  
};

#endif //MasterShef_DataMembersHF_H
