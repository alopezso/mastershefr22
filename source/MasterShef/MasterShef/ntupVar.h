#ifndef MasterShef_ntupVar_H
#define MasterShef_ntupVar_H


// This is the list of variables that are used to be 
// stored in the ntuple
// Make a separate file to keep things tidy

float nt_cputime; //!
int nt_runNumber; //!
unsigned long long int nt_eventNumber; //!
int nt_lbn; //!
int nt_bcid; //!

// global number of tracks 
 
int nt_ntrk_global;

float nt_nominalWeight;//!
float nt_muR10_muF20Weight;//!
float nt_muR10_muF05Weight;//!
float nt_muR20_muF10Weight;//!
float nt_muR05_muF10Weight;//!
float nt_muR05_muF05Weight;//!
float nt_muR20_muF20Weight;//!
float nt_Var3cUpWeight; //!
float nt_Var3cDownWeight; //!
float nt_ISRmuRfac10_FSRmuRfac20Weight; //!
float nt_ISRmuRfac10_FSRmuRfac05Weight; //!
std::vector<float> nt_PDFUncertainty; //!
 

float nt_analysisWeight; //!
float nt_beamspotWeight; //!
float nt_pileupweight; //!
float nt_pileupweightUP; //!
float nt_pileupweightDOWN; //!
ULong64_t nt_pileupweightHash; //!
float nt_TriggerWeight,nt_TriggerWeight2; //!
float nt_TriggerSF; //!
float nt_jvtweight; //!
float nt_jvtweightUP; //!
float nt_jvtweightDOWN; //!
float nt_btagweight; //!
float nt_btagweightBUP; //!
float nt_btagweightBDOWN; //!
float nt_btagweightCUP; //!
float nt_btagweightCDOWN; //!
float nt_btagweightLUP; //!
float nt_btagweightLDOWN; //!
float nt_btagweightExUP; //!
float nt_btagweightExDOWN; //!
float nt_btagweightExCUP; //!
float nt_btagweightExCDOWN; //!

float nt_btagweight_B0_UP; //!
float nt_btagweight_B0_DOWN; //!
float nt_btagweight_B1_UP; //!
float nt_btagweight_B1_DOWN; //!
float nt_btagweight_B2_UP; //!
float nt_btagweight_B2_DOWN; //!
float nt_btagweight_B3_UP; //!
float nt_btagweight_B3_DOWN; //!
float nt_btagweight_B4_UP; //!
float nt_btagweight_B4_DOWN; //!
float nt_btagweight_B5_UP; //!
float nt_btagweight_B5_DOWN; //!
float nt_btagweight_B6_UP; //!
float nt_btagweight_B6_DOWN; //!
float nt_btagweight_B7_UP; //!
float nt_btagweight_B7_DOWN; //!
float nt_btagweight_B8_UP; //!
float nt_btagweight_B8_DOWN; //!
float nt_btagweight_C0_UP; //!
float nt_btagweight_C0_DOWN; //!
float nt_btagweight_C1_UP; //!
float nt_btagweight_C1_DOWN; //!
float nt_btagweight_C2_UP; //!
float nt_btagweight_C2_DOWN; //!
float nt_btagweight_C3_UP; //!
float nt_btagweight_C3_DOWN; //!
float nt_btagweight_L0_UP; //!
float nt_btagweight_L0_DOWN; //!
float nt_btagweight_L1_UP; //!
float nt_btagweight_L1_DOWN; //!
float nt_btagweight_L2_UP; //!
float nt_btagweight_L2_DOWN; //!
float nt_btagweight_L3_UP; //!
float nt_btagweight_L3_DOWN; //!
float nt_btagweight_L4_UP; //!
float nt_btagweight_L4_DOWN; //!



float nt_btagtrkweight; //!
float nt_btagtrkweightBUP; //!
float nt_btagtrkweightBDOWN; //!
float nt_btagtrkweightCUP; //!
float nt_btagtrkweightCDOWN; //!
float nt_btagtrkweightLUP; //!
float nt_btagtrkweightLDOWN; //!
float nt_btagtrkweightExUP; //!
float nt_btagtrkweightExDOWN; //!
float nt_btagtrkweightExCUP; //!
float nt_btagtrkweightExCDOWN; //!


float nt_topjetweight; //!


float nt_SherpaWeight; //!

float nt_muonsTrigSF; //!
float nt_electronsTrigSF; //!
float nt_muonsRecoSF; //!
float nt_electronsRecoSF; //!
float nt_photonSF; //!


float nt_leptonsGlobalTrigSF; //
float nt_leptonsGlobalTrigSF_TRIGUP; //
float nt_leptonsGlobalTrigSF_TRIGDOWN; //
float nt_leptonsGlobalTrigSF_TRIGEFFUP; //
float nt_leptonsGlobalTrigSF_TRIGEFFDOWN; //
float nt_leptonsGlobalTrigSF_IDUP ; //!
float nt_leptonsGlobalTrigSF_IDDOWN ; //!
float nt_leptonsGlobalTrigSF_ISOUP ; //!
float nt_leptonsGlobalTrigSF_ISODOWN ; //!
float nt_leptonsGlobalTrigSF_RECOUP ; //!
float nt_leptonsGlobalTrigSF_RECODOWN ; //!
float nt_leptonsGlobalTrigSF_CHARGEIDSELUP; //!
float nt_leptonsGlobalTrigSF_CHARGEIDSELDOWN; //!

float nt_leptonsGlobalTrigSF_MUONTRIGSYSUP; //
float nt_leptonsGlobalTrigSF_MUONTRIGSYSDOWN; //
float nt_leptonsGlobalTrigSF_MUONTRIGSTATUP; //
float nt_leptonsGlobalTrigSF_MUONTRIGSTATDOWN; //
float nt_leptonsGlobalTrigMuonSF_TTVASYSTUP; //!
float nt_leptonsGlobalTrigMuonSF_TTVASYSTDOWN; //!
float nt_leptonsGlobalTrigMuonSF_TTVASTATUP; //!
float nt_leptonsGlobalTrigMuonSF_TTVASTATDOWN; //!
float nt_leptonsGlobalTrigMuonSF_ISOSYSTUP; //!
float nt_leptonsGlobalTrigMuonSF_ISOSYSTDOWN; //!
float nt_leptonsGlobalTrigMuonSF_ISOSTATUP; //!
float nt_leptonsGlobalTrigMuonSF_ISOSTATDOWN; //!
float nt_leptonsGlobalTrigMuonSF_BADMUONSYSUP; //!
float nt_leptonsGlobalTrigMuonSF_BADMUONSYSDOWN; //!
float nt_leptonsGlobalTrigMuonSF_BADMUONSTATUP; //!
float nt_leptonsGlobalTrigMuonSF_BADMUONSTATDOWN; //!
float nt_leptonsGlobalTrigMuonSF_RECOSYSUP; //!
float nt_leptonsGlobalTrigMuonSF_RECOSYSDOWN; //!
float nt_leptonsGlobalTrigMuonSF_RECOSTATUP; //!
float nt_leptonsGlobalTrigMuonSF_RECOSTATDOWN; //!
float nt_leptonsGlobalTrigMuonSF_RECOSYSLOWPTUP; //!
float nt_leptonsGlobalTrigMuonSF_RECOSYSLOWPTDOWN; //!
float nt_leptonsGlobalTrigMuonSF_RECOSTATLOWPTUP; //!
float nt_leptonsGlobalTrigMuonSF_RECOSTATLOWPTDOWN; //!


// Tau systematics 
float nt_tauSF; //!
float nt_tauSF_TRUEELECTRON_ELEOLR_TOTAL__1down; //!
float nt_tauSF_TRUEELECTRON_ELEOLR_TOTAL__1up; //!
float nt_tauSF_TRUEHADTAU_ELEOLR_TOTAL__1down; //!
float nt_tauSF_TRUEHADTAU_ELEOLR_TOTAL__1up; //!
float nt_tauSF_JETID_1PRONGSTATSYSTUNCORR2025__1down; //!
float nt_tauSF_JETID_1PRONGSTATSYSTUNCORR2025__1up; //!
float nt_tauSF_JETID_1PRONGSTATSYSTUNCORR2530__1down; //!
float nt_tauSF_JETID_1PRONGSTATSYSTUNCORR2530__1up; //!
float nt_tauSF_JETID_1PRONGSTATSYSTUNCORR3040__1down; //!
float nt_tauSF_JETID_1PRONGSTATSYSTUNCORR3040__1up; //!
float nt_tauSF_JETID_1PRONGSTATSYSTUNCORR40__1down; //!
float nt_tauSF_JETID_1PRONGSTATSYSTUNCORR40__1up; //!
float nt_tauSF_JETID_3PRONGSTATSYSTUNCORR2030__1down; //!
float nt_tauSF_JETID_3PRONGSTATSYSTUNCORR2030__1up; //!
float nt_tauSF_JETID_3PRONGSTATSYSTUNCORR30__1down; //!
float nt_tauSF_JETID_3PRONGSTATSYSTUNCORR30__1up; //!
float nt_tauSF_JETID_HIGHPT__1down; //!
float nt_tauSF_JETID_HIGHPT__1up; //!
float nt_tauSF_JETID_SYST__1down; //!
float nt_tauSF_JETID_SYST__1up; //!
float nt_tauSF_RECO_HIGHPT__1down; //!
float nt_tauSF_RECO_HIGHPT__1up; //!
float nt_tauSF_RECO_TOTAL__1down; //!
float nt_tauSF_RECO_TOTAL__1up; //!

// Trigger systematics : Remove them unless some tau trigger is applied 
// float nt_tauSF_TRIGGER_STATDATA2015__1down; //!
// float nt_tauSF_TRIGGER_STATDATA2015__1up; //!
// float nt_tauSF_TRIGGER_STATDATA2016__1down; //!
// float nt_tauSF_TRIGGER_STATDATA2016__1up; //!
// float nt_tauSF_TRIGGER_STATDATA2017__1down; //!
// float nt_tauSF_TRIGGER_STATDATA2017__1up; //!
// float nt_tauSF_TRIGGER_STATMC2015__1down; //!
// float nt_tauSF_TRIGGER_STATMC2015__1up; //!
// float nt_tauSF_TRIGGER_STATMC2016__1down; //!
// float nt_tauSF_TRIGGER_STATMC2016__1up; //!
// float nt_tauSF_TRIGGER_STATMC2017__1down; //!
// float nt_tauSF_TRIGGER_STATMC2017__1up; //!
// float nt_tauSF_TRIGGER_SYST2015__1down; //!
// float nt_tauSF_TRIGGER_SYST2015__1up; //!
// float nt_tauSF_TRIGGER_SYST2016__1down; //!
// float nt_tauSF_TRIGGER_SYST2016__1up; //!
// float nt_tauSF_TRIGGER_SYST2017__1down; //!
// float nt_tauSF_TRIGGER_SYST2017__1up; //! 


float nt_passTSTCleaning; //!
int nt_year; //!  
std::string nt_anaflag; //!
std::string nt_btagType; //!
std::string nt_btaggerName; //!
int nt_anatruth; //!

int nt_passtauveto; //!
int nt_hasMEphoton; //!
int nt_hasMEphoton80; //!
int nt_hasMEphotonMCTC; //!



double nt_GenFiltMET; //!
double nt_GenFiltHT; //!

int nt_TruthChLeptsFromLQ;//!
int nt_tcMeTcategory; //!
int nt_topDecay; //!
std::string nt_topISR; //!

int nt_TopHeavyFlavorFilterFlag; //!
int nt_ttbar_class; //!
int nt_ttbar_class_ext; //!
int nt_ttbar_class_prompt; //!

float nt_muonsTrigSF_TRIGSYSTUP; //!
float nt_muonsTrigSF_TRIGSYSTDOWN; //!
float nt_muonsTrigSF_TRIGSTATUP; //!
float nt_muonsTrigSF_TRIGSTATDOWN; //!
float nt_muonsTrigSF_TTVASYSTUP; //!
float nt_muonsTrigSF_TTVASYSTDOWN; //!
float nt_muonsTrigSF_TTVASTATUP; //!
float nt_muonsTrigSF_TTVASTATDOWN; //!
float nt_muonsTrigSF_ISOSYSTUP; //!
float nt_muonsTrigSF_ISOSYSTDOWN; //!
float nt_muonsTrigSF_ISOSTATUP; //!
float nt_muonsTrigSF_ISOSTATDOWN; //!
float nt_muonsTrigSF_BADMUONSYSUP; //!
float nt_muonsTrigSF_BADMUONSYSDOWN; //!
float nt_muonsTrigSF_BADMUONSTATUP; //!
float nt_muonsTrigSF_BADMUONSTATDOWN; //!
float nt_muonsTrigSF_RECOSYSUP; //!
float nt_muonsTrigSF_RECOSYSDOWN; //!
float nt_muonsTrigSF_RECOSTATUP; //!
float nt_muonsTrigSF_RECOSTATDOWN; //!
float nt_muonsTrigSF_RECOSYSLOWPTUP; //!
float nt_muonsTrigSF_RECOSYSLOWPTDOWN; //!
float nt_muonsTrigSF_RECOSTATLOWPTUP; //!
float nt_muonsTrigSF_RECOSTATLOWPTDOWN; //!

float nt_electronsTrigSF_IDUP ; //!
float nt_electronsTrigSF_IDDOWN ; //!
float nt_electronsTrigSF_ISOUP ; //!
float nt_electronsTrigSF_ISODOWN ; //!
float nt_electronsTrigSF_RECOUP ; //!
float nt_electronsTrigSF_RECODOWN ; //!
float nt_electronsTrigSF_TRIGUP ; //!
float nt_electronsTrigSF_TRIGDOWN ; //!
float nt_electronsTrigSF_TRIGEFFUP ; //!
float nt_electronsTrigSF_TRIGEFFDOWN ; //!
float nt_electronsTrigSF_CHARGEIDSELUP; //!
float nt_electronsTrigSF_CHARGEIDSELDOWN; //!

float nt_muonsSF_TTVASYSTUP; //!
float nt_muonsSF_TTVASYSTDOWN; //!
float nt_muonsSF_TTVASTATUP; //!
float nt_muonsSF_TTVASTATDOWN; //!
float nt_muonsSF_ISOSYSTUP; //!
float nt_muonsSF_ISOSYSTDOWN; //!
float nt_muonsSF_ISOSTATUP; //!
float nt_muonsSF_ISOSTATDOWN; //!
float nt_muonsSF_BADMUONSYSUP; //!
float nt_muonsSF_BADMUONSYSDOWN; //!
float nt_muonsSF_BADMUONSTATUP; //!
float nt_muonsSF_BADMUONSTATDOWN; //!
float nt_muonsSF_RECOSYSUP; //!
float nt_muonsSF_RECOSYSDOWN; //!
float nt_muonsSF_RECOSTATUP; //!
float nt_muonsSF_RECOSTATDOWN; //!
float nt_muonsSF_RECOSYSLOWPTUP; //!
float nt_muonsSF_RECOSYSLOWPTDOWN; //!
float nt_muonsSF_RECOSTATLOWPTUP; //!
float nt_muonsSF_RECOSTATLOWPTDOWN; //!
float nt_electronsSF_RECOUP; //!
float nt_electronsSF_RECODOWN; //!
float nt_electronsSF_ISOUP; //!
float nt_electronsSF_ISODOWN; //!
float nt_electronsSF_IDUP; //!
float nt_electronsSF_IDDOWN; //!
float nt_electronsSF_CHARGEIDSELUP; //!
float nt_electronsSF_CHARGEIDSELDOWN; //!

float nt_photonSF_IDUP ; //!
float nt_photonSF_IDDOWN ; //!
float nt_photonSF_TRKISOUP ; //!
float nt_photonSF_TRKISODOWN ; //!

float nt_jetfJvtEfficiencyUP; //!
float nt_jetfJvtEfficiencyDOWN; //!
float nt_jetJvtEfficiencyUP; //!
float nt_jetJvtEfficiencyDOWN; //!

float nt_actualIntPerXing; //!	
float nt_averageIntPerXing; //!	
float nt_averageIntPerXingCorr; //!	

float nt_nVertices; //!
float nt_nVertices_PriVtr; //!

//truth B-Hadron
std::vector<float> nt_truth_bhadron_pt; //!
std::vector<float> nt_truth_bhadron_eta; //!
std::vector<float> nt_truth_bhadron_phi; //!
std::vector<float> nt_truth_bhadron_m; //!
//truth C-Hadron
std::vector<float> nt_truth_chadron_pt; //!
std::vector<float> nt_truth_chadron_eta; //!
std::vector<float> nt_truth_chadron_phi; //!
std::vector<float> nt_truth_chadron_m; //!

// truth vector boson

float nt_VbosonPt; //!
float nt_VbosonEta; //!
float nt_VbosonPhi; //!
float nt_VbosonE; //!
float nt_VbosonPDGID; //!

std::vector<float> nt_nuMET; //!

float nt_TruthNonInt_pt; //!
float nt_TruthNonInt_phi; //!


// Variables: JETS
int nt_nJets; 
double nt_hlt_jet; //!
std::vector<float> nt_jet_pt; //!
std::vector<float> nt_jet_eta; //!
std::vector<float> nt_jet_phi; //!
std::vector<float> nt_jet_e;  //!
std::vector<float> nt_jet_ghost_pt; //!
std::vector<float> nt_jet_ghost_eta; //!
std::vector<float> nt_jet_ghost_phi; //!
std::vector<float> nt_jet_ghost_e;  //!
std::vector<float> nt_jet_MV2c10;//!
std::vector<float> nt_jet_DL1;//!
std::vector<float> nt_jet_MV2c20;//!
std::vector<float> nt_jet_btagweight;//!
std::vector<int>   nt_jet_IsBJet; //! 
std::vector<int>   nt_jet_IsCJet; //! 
std::vector<float> nt_jet_DL1_pu;//!
std::vector<float> nt_jet_DL1_pc;//!
std::vector<float> nt_jet_DL1_pb;//!
std::vector<float> nt_jet_DL1r_pu;//!
std::vector<float> nt_jet_DL1r_pc;//!
std::vector<float> nt_jet_DL1r_pb;//!
std::vector<float> nt_jet_DL1r_cscore;//!
std::vector<float> nt_jet_chf;//!
std::vector<float> nt_jet_BCH_CORR_CELL; //!
std::vector<float> nt_jet_emfrac; //!
std::vector<float> nt_jet_jvtxf;  //!
std::vector<int>   nt_jet_flav; //! 
std::vector<int>   nt_jet_flavFlat; //!
std::vector<int>   nt_jet_fmax; //!
std::vector<int>   nt_jet_isbad; //!
//Index of truth b-hadron that is closest in dR space
std::vector<int> nt_jet_index_truthbhadron;//!

std::vector<int> nt_jet_isSimpleTau;//!
std::vector<int> nt_jet_passTightClean;//!
std::vector<int> nt_jet_passTightCleanDFFlag;//!

int nt_passDFCommonJets_eventClean_LooseBad; //!


std::vector<int> nt_jet_truthflav;//!
//  std::vector<int> nt_jet_isHiggs;//!
//  std::vector<int> nt_jet_isTop;//!
std::vector<std::string> nt_jet_type;//!
std::vector<std::string> nt_jet_origin;//!
std::vector<float> nt_jet_PullMag;//!
std::vector<float> nt_jet_PullPhi;//!

// Varaibles : Jet constituents
std::vector<std::vector<float> > nt_jet_tracks_pt;//!
std::vector<std::vector<float> > nt_jet_tracks_eta;//!
std::vector<std::vector<float> > nt_jet_tracks_phi;//!
std::vector<std::vector<float> > nt_jet_tracks_e;//!

 

std::vector<float> nt_sbv_tight_Lxy; //!
std::vector<float> nt_sbv_tight_X; //!
std::vector<float> nt_sbv_tight_Y; //!
std::vector<float> nt_sbv_tight_Z; //!

std::vector<float> nt_sbv_tight_m; //!
std::vector<float> nt_sbv_tight_pt; //!
std::vector<float> nt_sbv_tight_eta; //!
std::vector<float> nt_sbv_tight_phi; //!

std::vector<int> nt_sbv_tight_nTracks; //!
std::vector<float> nt_sbv_tight_Chi2Reduced; //!
int nt_nsbv_tight; //!

std::vector<int> nt_sbv_index_truthbhadron; //!
std::vector<int> nt_sbv_index_truthchadron; //!
std::vector<int> nt_sbv_tight_index_truthbhadron; //!//mario
std::vector<int> nt_sbv_tight_index_truthchadron; //!   

std::vector<int> nt_sbv_medium_index_truthbhadron; //!
std::vector<int> nt_sbv_medium_index_truthchadron; //!
std::vector<int> nt_sbv_loose_index_truthbhadron; //!
std::vector<int> nt_sbv_loose_index_truthchadron; //!  

std::vector<float> nt_sbv_medium_Lxy; //!
std::vector<float> nt_sbv_medium_X; //!
std::vector<float> nt_sbv_medium_Y; //!
std::vector<float> nt_sbv_medium_Z; //!

std::vector<float> nt_sbv_medium_m; //!
std::vector<float> nt_sbv_medium_pt; //!
std::vector<float> nt_sbv_medium_eta; //!
std::vector<float> nt_sbv_medium_phi; //!

std::vector<int> nt_sbv_medium_nTracks; //!
std::vector<float> nt_sbv_medium_Chi2Reduced; //!
int nt_nsbv_medium; //!

std::vector<float> nt_sbv_loose_Lxy; //!
std::vector<float> nt_sbv_loose_X; //!
std::vector<float> nt_sbv_loose_Y; //!
std::vector<float> nt_sbv_loose_Z; //!

std::vector<float> nt_sbv_loose_m; //!
std::vector<float> nt_sbv_loose_pt; //!
std::vector<float> nt_sbv_loose_eta; //!
std::vector<float> nt_sbv_loose_phi; //!

std::vector<int> nt_sbv_loose_nTracks; //!
std::vector<float> nt_sbv_loose_Chi2Reduced; //!
int nt_nsbv_loose; //!

// Varaibles : Track Jet constituents
std::vector<std::vector<float> > nt_trackjet_tracks_pt;//!
std::vector<std::vector<float> > nt_trackjet_tracks_eta;//!
std::vector<std::vector<float> > nt_trackjet_tracks_phi;//!
std::vector<std::vector<float> > nt_trackjet_tracks_e;//!
std::vector<int> nt_jet_ntracks;//!
std::vector<int> nt_jet_npartons;//!
std::vector<double> nt_jet_wpartons;//!
std::vector<int> nt_jet_truthmatched;    //!
//This corresponds to HadronConeExclTruthLabelID
std::vector<int> nt_jet_truthflavhadcone; //!
std::vector<int> nt_jet_truthflavhadexcl; //!
std::vector<int>   nt_isMV2c10H85; //!
std::vector<int>   nt_isMV2c10H77; //!
std::vector<int>   nt_isMV2c10H70; //!
std::vector<int>   nt_isDL1H85; //!
std::vector<int>   nt_isDL1H77; //!
std::vector<int>   nt_isDL1H70; //!


// Varialbes: track jets
std::vector<float> nt_trackjet_pt; //!
std::vector<float> nt_trackjet_eta; //!
std::vector<float> nt_trackjet_phi; //!
std::vector<float> nt_trackjet_e;  //!
int nt_trackjet_hasVROverlap; //!
std::vector<int> nt_trackjet_isbjet;  //!
std::vector<int> nt_trackjet_orISR;  //!
std::vector<int> nt_trackjet_ntrk;  //!
std::vector<float> nt_trackjet_MV2c10;//!
//This corresponds to if susytools marks this as a b-track-jet or not
std::vector<int> nt_trackjet_flav;//!
//susytools flag for VR trackjet DR OR
std::vector<int> nt_trackjet_passDR;//!
//This corresponds to ConeTruthLabelID
std::vector<int> nt_trackjet_truthflav;//!
//This corresponds to HadronConeExclTruthLabelID and HadronConeExclExtendedTruthLabelID
std::vector<int> nt_trackjet_truthflavhadcone; //!
std::vector<int> nt_trackjet_truthflavhadexcl; //!
//These, origin and type, give information on truth flavor via MCTruthClassifier
std::vector<std::string> nt_trackjet_origin; //!
std::vector<std::string> nt_trackjet_type; //!
//Index of truth b-hadron that is closest in dR space
std::vector<int> nt_trackjet_index_truthbhadron;//!
//variables to classify the truth HF composition of the event
bool nt_hasTruthB; //!
bool nt_hasTruthC; //!
bool nt_hasNoTruth; //!


// Variables: smrJETS
std::vector<int> nt_nsmrJets; //!
std::vector<int> nt_smrjet_index; //!
std::vector<float> nt_smrjet_pt; //!
//std::vector<TLorentzVector> nt_smrjet_p4; //!
std::vector<float> nt_smrjet_R; //!
std::vector<float> nt_smrjet_eta; //!
std::vector<float> nt_smrjet_phi; //!
std::vector<float> nt_smrjet_e;  //!
std::vector<float> nt_smrjet_MV2;//!
std::vector<int>   nt_smrjet_flav; //!

std::vector<float> nt_tailWeights; //!

std::vector<int> nt_smrrcjet_kt12_index; //!
std::vector<float> nt_smrrcjet_kt12_pt; //!
std::vector<float> nt_smrrcjet_kt12_eta; //!
std::vector<float> nt_smrrcjet_kt12_phi; //!
std::vector<float> nt_smrrcjet_kt12_e;  //!

std::vector<int> nt_smrrcjet_kt8_index; //!
std::vector<float> nt_smrrcjet_kt8_pt; //!
std::vector<float> nt_smrrcjet_kt8_eta; //!
std::vector<float> nt_smrrcjet_kt8_phi; //!
std::vector<float> nt_smrrcjet_kt8_e;  //!





// Variables: smrMET
std::vector<float> nt_smrMET_pt; //!
std::vector<float> nt_smrMET_phi; //!
std::vector<float> nt_smrsumet; //!
std::vector<float> nt_smrmetsig; //!


// Variables: FatJets kt8
int nt_nFatJetsKt8; 
std::vector<float> nt_rcjet_kt8_pt; //!
std::vector<float> nt_rcjet_kt8_eta; //!
std::vector<float> nt_rcjet_kt8_phi; //!
std::vector<float> nt_rcjet_kt8_e;  //!
std::vector<float> nt_rcjet_kt8_MV2;//!
std::vector<int>   nt_rcjet_kt8_flav; //!
std::vector<int>   nt_rcjet_kt8_nconst; //!

std::vector<float>   nt_rcjet_kt8_pflowMass; //!
std::vector<float>   nt_rcjet_kt8_trkMass; //!
std::vector<float>   nt_rcjet_kt8_massConst; //!
std::vector<float>   nt_rcjet_kt8_Tau1; //!
std::vector<float>   nt_rcjet_kt8_Tau2; //!
std::vector<float>   nt_rcjet_kt8_Tau3; //!
std::vector<float>   nt_rcjet_kt8_Tau21; //!
std::vector<float>   nt_rcjet_kt8_Tau32; //!
std::vector<float>   nt_rcjet_kt8_Split12; //!
std::vector<float>   nt_rcjet_kt8_Split23; //!
std::vector<float>   nt_rcjet_kt8_Split34; //!
std::vector<float>   nt_rcjet_kt8_Dip12; //!
std::vector<float>   nt_rcjet_kt8_Dip13; //!
std::vector<float>   nt_rcjet_kt8_Dip23; //!
std::vector<float>   nt_rcjet_kt8_Qw; //!
std::vector<float>   nt_rcjet_kt8_Charge; //!!
std::vector<float>   nt_rcjet_kt8_ECF1; //!
std::vector<float>   nt_rcjet_kt8_ECF2; //!
std::vector<float>   nt_rcjet_kt8_ECF3; //!
std::vector<float>   nt_rcjet_kt8_C2; //!
std::vector<float>   nt_rcjet_kt8_D2; //!

// Variables: FatJets kt12
int nt_nFatJetsKt12; 
std::vector<float> nt_rcjet_kt12_pt; //!
std::vector<float> nt_rcjet_kt12_eta; //!
std::vector<float> nt_rcjet_kt12_phi; //!
std::vector<float> nt_rcjet_kt12_e;  //!
std::vector<float> nt_rcjet_kt12_MV2;//!
std::vector<int>   nt_rcjet_kt12_flav; //!
std::vector<int>   nt_rcjet_kt12_nconst; //!

std::vector<float>   nt_rcjet_kt12_pflowMass; //!
std::vector<float>   nt_rcjet_kt12_trkMass; //!
std::vector<float>   nt_rcjet_kt12_massConst; //!
std::vector<float>   nt_rcjet_kt12_Tau1; //!
std::vector<float>   nt_rcjet_kt12_Tau2; //!
std::vector<float>   nt_rcjet_kt12_Tau3; //!
std::vector<float>   nt_rcjet_kt12_Tau21; //!
std::vector<float>   nt_rcjet_kt12_Tau32; //!
std::vector<float>   nt_rcjet_kt12_Split12; //!
std::vector<float>   nt_rcjet_kt12_Split23; //!
std::vector<float>   nt_rcjet_kt12_Split34; //!
std::vector<float>   nt_rcjet_kt12_Dip12; //!
std::vector<float>   nt_rcjet_kt12_Dip13; //!
std::vector<float>   nt_rcjet_kt12_Dip23; //!
std::vector<float>   nt_rcjet_kt12_Qw; //!
std::vector<float>   nt_rcjet_kt12_Charge; //!!

std::vector<float>   nt_rcjet_kt12_ECF1; //!
std::vector<float>   nt_rcjet_kt12_ECF2; //!
std::vector<float>   nt_rcjet_kt12_ECF3; //!
std::vector<float>   nt_rcjet_kt12_C2; //!
std::vector<float>   nt_rcjet_kt12_D2; //!


// Variables: FatJets kt10
int nt_nFatJetsKt10_RC; 
std::vector<float> nt_rcjet_kt10_pt; //!
std::vector<float> nt_rcjet_kt10_eta; //!
std::vector<float> nt_rcjet_kt10_phi; //!
std::vector<float> nt_rcjet_kt10_e;  //!
std::vector<float> nt_rcjet_kt10_MV2;//!
std::vector<int>   nt_rcjet_kt10_flav; //!
std::vector<int>   nt_rcjet_kt10_nconst; //!


// Variables: FatJets kt15
int nt_nFatJetsKt15; 
std::vector<float> nt_rcjet_kt15_pt; //!
std::vector<float> nt_rcjet_kt15_eta; //!
std::vector<float> nt_rcjet_kt15_phi; //!
std::vector<float> nt_rcjet_kt15_e;  //!
std::vector<float> nt_rcjet_kt15_MV2;//!
std::vector<int>   nt_rcjet_kt15_flav; //!
std::vector<int>   nt_rcjet_kt15_nconst; //!

 
// Variables: FatJets from susytools

int nt_nFatJetsSt;
std::vector<float> nt_fatjet_kt10_pt; //!
std::vector<float> nt_fatjet_kt10_eta; //!
std::vector<float> nt_fatjet_kt10_phi; //!
std::vector<float> nt_fatjet_kt10_e;  //!
std::vector<int> nt_fatjet_kt10_ntrkjets;  //!
std::vector<int> nt_fatjet_kt10_nghostbhad;  //!
std::vector<float> nt_fatjet_kt10_Split12;  //!
std::vector<float> nt_fatjet_kt10_Split23;  //!
std::vector<float> nt_fatjet_kt10_Split34;  //!
std::vector<float> nt_fatjet_kt10_Qw;  //!
std::vector<float> nt_fatjet_kt10_Tau1;  //!
std::vector<float> nt_fatjet_kt10_Tau2;  //!
std::vector<float> nt_fatjet_kt10_Tau3;  //!
std::vector<float> nt_fatjet_kt10_Tau32;  //!
std::vector<int>   nt_fatjet_kt10_ztag; //!
std::vector<int>   nt_fatjet_kt10_wtag; //!
std::vector<bool>   nt_fatjet_kt10_ztag50; //!
std::vector<bool>   nt_fatjet_kt10_ztag80; //!
std::vector<bool>   nt_fatjet_kt10_wtag50; //!
std::vector<bool>   nt_fatjet_kt10_wtag80; //!

std::vector<int> nt_fatjet_kt10_W50res;
std::vector<int> nt_fatjet_kt10_W80res;
std::vector<int> nt_fatjet_kt10_Z50res;
std::vector<int> nt_fatjet_kt10_Z80res;
std::vector<float> nt_fatjet_kt10_WLowWMassCut50;
std::vector<float> nt_fatjet_kt10_WLowWMassCut80;
std::vector<float> nt_fatjet_kt10_ZLowWMassCut50;
std::vector<float> nt_fatjet_kt10_ZLowWMassCut80;
std::vector<float> nt_fatjet_kt10_WHighWMassCut50;
std::vector<float> nt_fatjet_kt10_WHighWMassCut80;
std::vector<float> nt_fatjet_kt10_ZHighWMassCut50;
std::vector<float> nt_fatjet_kt10_ZHighWMassCut80;
std::vector<float> nt_fatjet_kt10_WD2Cut50;
std::vector<float> nt_fatjet_kt10_WD2Cut80;
std::vector<float> nt_fatjet_kt10_ZD2Cut50;
std::vector<float> nt_fatjet_kt10_ZD2Cut80;

std::vector<bool> nt_fatjet_kt10_toptag50;
std::vector<bool> nt_fatjet_kt10_toptag80;
std::vector<int> nt_fatjet_kt10_top50res;
std::vector<int> nt_fatjet_kt10_top80res;
std::vector<float> nt_fatjet_kt10_TopTagTau32Cut50;
std::vector<float> nt_fatjet_kt10_TopTagTau32Cut80;
std::vector<float> nt_fatjet_kt10_TopTagSplit23Cut50;
std::vector<float> nt_fatjet_kt10_TopTagSplit23Cut80;
std::vector<float> nt_fatjet_kt10_ECF1;
std::vector<float> nt_fatjet_kt10_ECF2;
std::vector<float> nt_fatjet_kt10_ECF3;
std::vector<float> nt_fatjet_kt10_C2;
std::vector<float> nt_fatjet_kt10_D2;
std::vector<int> nt_fatjet_kt10_dnntoptag80;
std::vector<int> nt_fatjet_kt10_dnntoptag50;
std::vector<int> nt_fatjet_kt10_dnntoptag;
std::vector<int> nt_fatjet_kt10_nconstituents;
std::vector<std::string> nt_fatjet_kt10_origin;

std::vector<float> nt_fatjet_kt10_BetaG;

// Variables: Electrons
int nt_nEl; //!
int nt_nbaselineEl_beforeOR; //!
int nt_nbaselineEl; //!
int nt_nbaselineEl_lowPt; //!
int nt_isElTrigMatched; //!
std::vector<float> nt_el_pt; //!
std::vector<float> nt_el_eta; //!
std::vector<float> nt_el_phi; //!
std::vector<float> nt_el_e;  //!
std::vector<float> nt_el_SF; //!
std::vector<float> nt_el_charge; //!


// Variables: Muons
int nt_nMu; //!
int nt_nbaselineMu_beforeOR; //!
int nt_nbaselineMu; //!
int nt_nbaselineMu_lowPt; //!

int nt_ncosmicMu; //!
int nt_isMuTrigMatched; //!
std::vector<float> nt_mu_pt; //!
std::vector<float> nt_mu_eta; //!
std::vector<float> nt_mu_phi; //! 
std::vector<float> nt_mu_e;  //!
std::vector<float> nt_mu_SF; //!
std::vector<float> nt_mu_charge; //!

// Variables: Photon
int nt_nPh; //!
int nt_nbaselinePh; //!

std::vector<float> nt_ph_pt; //!
std::vector<float> nt_ph_eta; //!
std::vector<float> nt_ph_phi; //! 
std::vector<float> nt_ph_e;  //!
std::vector<float> nt_ph_topoetcone20; //! 
std::vector<float> nt_ph_topoetcone40; //!
std::vector<float> nt_ph_ptcone20; //!   
std::vector<float> nt_ph_ptvarcone20; //!

//Variables: taus
int nt_nbaselineTau; //!
int nt_nTau; //!
std::vector<float> nt_tau_pt; //!
std::vector<float> nt_tau_eta; //!
std::vector<float> nt_tau_phi; //! 
std::vector<float> nt_tau_e;  //!
std::vector<float> nt_tau_nPi0; //!
std::vector<float> nt_tau_nH; //!
std::vector<float> nt_tau_nWTracks; //!
std::vector<float> nt_tau_nTracks; //!
std::vector<float> nt_tau_nCharged; //!
std::vector<float> nt_tau_nNeut; //!
std::vector<float> nt_tau_bdtJet; //!
std::vector<float> nt_tau_bdtEl; //!
std::vector<int> nt_tau_truthflav; //!
std::vector<int> nt_tau_passOR; //!
std::vector<int> nt_tau_isVeryLooseTau; //!
std::vector<int> nt_tau_isLooseTau; //!
std::vector<int> nt_tau_isTruthMatched; //!
std::vector<int> nt_tau_isMediumTau; //!
std::vector<std::string> nt_tau_TruthParent; //!
std::vector<int> nt_tau_nTracks_Iso; //!
std::vector<float> nt_tau_TruthMatchedTauPartnerPt; //!
std::vector<float> nt_tau_TruthMatchedTauPartnerEta; //!
std::vector<float> nt_tau_TruthMatchedTauPartnerPhi; //!
std::vector<float> nt_tau_TruthMatchedTauPartnerE; //!
std::vector<int> nt_tau_TruthMatchedTauPartnerNTracks; //!
std::vector<int> nt_tau_TruthMatchedTauPartnerNNeutralPions; //!
std::vector<int> nt_tau_TruthMatchedTauPartnerNChargedPions; //!
std::vector<int> nt_tau_TruthMatchedTauPartnerNCharged; //!
std::vector<int> nt_tau_TruthMatchedTauPartnerNNeutral; //!

//Variables: truth taus
std::vector<float> nt_truthtau_pt; //!
std::vector<float> nt_truthtau_eta; //!
std::vector<float> nt_truthtau_phi; //!
std::vector<float> nt_truthtau_e; //!
std::vector<int> nt_truthtau_NTracks; //!   
std::vector<float> nt_truthtau_status; //!
std::vector<bool> nt_truthtau_ishadronic; //!
std::vector<std::string>nt_truthtau_origin; //!
std::vector<std::string>nt_truthtau_type; //!

//Variables: TruthMuons
std::vector<float> nt_truthmu_pt; //!
std::vector<float> nt_truthmu_eta; //!
std::vector<float> nt_truthmu_phi; //!
std::vector<float> nt_truthmu_e; //!                                                                              
std::vector<float> nt_truthmu_status; //!
std::vector<std::string> nt_truthmu_type; //!
std::vector<std::string> nt_truthmu_origin;//!

//Variables: TruthElectrons
std::vector<float> nt_truthel_pt; //!
std::vector<float> nt_truthel_eta; //!
std::vector<float> nt_truthel_phi; //!
std::vector<float> nt_truthel_e; //!                                                                              
std::vector<int> nt_truthel_NTracks; //!                                                                          
std::vector<float> nt_truthel_status; //!
std::vector<std::string> nt_truthel_type; //!
std::vector<std::string> nt_truthel_origin; //!



// Variables: truth jets
std::vector<float> nt_truthJet_pt; //!
std::vector<float> nt_truthJet_eta; //!
std::vector<float> nt_truthJet_phi; //! 
std::vector<float> nt_truthJet_e;  //!
std::vector<float> nt_truthJet_flav;  //!
std::vector<int> nt_truthJet_calib_isgood;  //!
std::vector<int>nt_truthJet_matched_tau; //!
std::vector<std::string>nt_truthJet_origin; //!
std::vector<float>nt_truthJet_TaudR; //!
std::vector<float>nt_truthJet_CdR;//!
std::vector<float>nt_truthJet_BdR; //!
std::vector<float>nt_truthJet_status; //!
std::vector<std::string> nt_truthJet_type; //!


// Variables: reco jets + reco muons for RMaps
std::vector<float> nt_RMapRecoJet_pt; //!
std::vector<float> nt_RMapRecoJet_eta; //!
std::vector<float> nt_RMapRecoJet_phi; //! 
std::vector<float> nt_RMapRecoJet_e;  //!
std::vector<float> nt_RMapRecoJet_flav;  //!
std::vector<float> nt_RMapRecoJet_truthindex;  //!
std::vector<int> nt_RMapRecoJet_baseline;  //!
std::vector<int> nt_RMapRecoJet_signal;  //!
std::vector<int> nt_RMapRecoJet_passOR;  //!



//
// Variables: truth neutrinos
std::vector<float> nt_truthNu_pt; //!
std::vector<float> nt_truthNu_eta; //!
std::vector<float> nt_truthNu_phi; //! 
std::vector<float> nt_truthNu_e;  //!
// Variables: truth electrons
std::vector<float> nt_truthEl_pt; //!
std::vector<float> nt_truthEl_eta; //!
std::vector<float> nt_truthEl_phi; //! 
std::vector<float> nt_truthEl_e;  //!
// Variables: truth muons
std::vector<float> nt_truthMu_pt; //!
std::vector<float> nt_truthMu_eta; //!
std::vector<float> nt_truthMu_phi; //! 
std::vector<float> nt_truthMu_e;  //!
std::vector<bool> nt_truthMu_signal_matched; //!
std::vector<bool> nt_truthMu_baseline_matched; //!
std::vector<bool> nt_truthMu_preOR_matched; //!
std::vector<bool> nt_truthMu_inA; //!

int nt_ntruthMu; //!



// Variables: MET
float nt_MET_pt; //!
float nt_MET_phi; //!
                                                                                                                                                                                     
float nt_MET_LepJet_pt; //!
float nt_MET_LepJet_phi; //!  

float nt_MET_jet_pt; //!
float nt_MET_jet_phi; //!

float nt_MET_mu_pt; //!
float nt_MET_mu_phi; //!

float nt_MET_el_pt; //!
float nt_MET_el_phi; //!
float nt_MET_y_pt; //!
float nt_MET_y_phi; //!

float nt_MET_softTrk_pt; //!
float nt_MET_softTrk_phi; //!

float nt_MET_NonInt_pt; //!
float nt_MET_NonInt_phi; //!

float nt_trackMET_pt; //!
float nt_trackMET_phi; //!

float nt_MET_pt_inv; //!
float nt_MET_phi_inv; //!

float nt_MET_pt_prime; //!
float nt_MET_phi_prime; //!

float nt_MET_tau_pt; //!
float nt_MET_tau_phi; //!


float nt_metsigET; //!
float nt_metsigHT; //!
float nt_metsig; //!
float nt_metsig_default; //!
float nt_metsig_binflate; //!
float nt_metsig_run1JER; //!

float nt_metsigST; //!
float nt_metsigLepJetST; //!
float nt_metsigLepJetResol; //!

float nt_sumet; //!
float nt_sumet_jet;   //!
float nt_sumet_el;   //!
float nt_sumet_tau; //!
float nt_sumet_y;   //!
float nt_sumet_mu;   //!
float nt_sumet_softTrk;   //!
float nt_sumet_softClu;   //!
float nt_sumet_NonInt;   //!

float nt_cellMET;   //!
float nt_mhtMET;   //!
float nt_IsMETTrigPassed; //!


float nt_simpleMET_pt; //!
float nt_simpleMET_phi; //!

float nt_simpleMETCST_pt; //!
float nt_simpleMETCST_phi; //!

float nt_simpleMET_jet_pt; //!
float nt_simpleMET_jet_phi; //!

float nt_simpleMETCST_jet_pt; //!
float nt_simpleMETCST_jet_phi; //!

float nt_simpleMET_mu_pt; //!
float nt_simpleMET_mu_phi; //!

float nt_simpleMETCST_mu_pt; //!
float nt_simpleMETCST_mu_phi; //!

float nt_simpleMET_softTrk_pt; //!
float nt_simpleMET_softTrk_phi; //!

float nt_simpleMETCST_softClus_pt; //!
float nt_simpleMETCST_softClus_phi; //!

float nt_sumet_simpleTST; //!
float nt_sumet_simpleTST_jet; //!
float nt_sumet_simpleTST_mu; //!
float nt_sumet_simpleTST_softTrk; //!

float nt_sumet_simpleCST; //!
float nt_sumet_simpleCST_jet; //!
float nt_sumet_simpleCST_mu; //!
float nt_sumet_simpleCST_softClus; //!


float nt_passSeedSelection; //!
float nt_passHTSeedSelection; //!
float nt_passETSeedSelection; //!
float nt_passETSeedSelectionv2; //!
float nt_passSeedSelectionQRT; //!
float nt_passSeedSelectionCU; //!

float nt_MET_pt_seed; //!
float nt_sumet_seed; //!
float nt_sumet_jet_seed; //!
float nt_sumet_soft_seed; //!
float nt_HT_seed; //!
bool nt_smrFilter; //!


// Trigger variables
bool nt_trigPass[2000];
bool nt_trigMatch[2000];

#endif
