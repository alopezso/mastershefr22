#ifndef MasterShef_HFSystDataMembers_H
#define MasterShef_HFSystDataMembers_H


#include <vector>
#include <string>

/** @class DataMemberHF
 ** to be used with HF Classification Tool
 ** independantly of the framework used
 */

class HFSystDataMembers{
  
 public:

  HFSystDataMembers();
  HFSystDataMembers( const HFSystDataMembers &dm );
  virtual ~HFSystDataMembers();

  //Declaration of needed variables as public data members
  int HF_Classification;

  float q1_pt;
  float q1_eta;
  float qq_pt;
  float qq_dr;
  float top_pt;
  float ttbar_pt;
  
};

#endif //MasterShef_HFSystDataMembers_H
