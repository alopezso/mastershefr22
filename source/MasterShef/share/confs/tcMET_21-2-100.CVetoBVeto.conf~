##################################################
# SUSYTools configuration file
##################################################
Slices.Ele: 1
Slices.Pho: 0
Slices.Mu: 1
Slices.Tau: 0
Slices.Jet: 1
Slices.BJet: 1
Slices.FJet: 1
Slices.TJet: 0
Slices.MET: 1
#  
EleBaseline.Pt: 4500.
#EleBaseline.Pt: 6000.
EleBaseline.Eta: 2.47
EleBaseline.Id: LooseAndBLayerLLH
EleBaseline.CrackVeto: false
EleBaseline.z0: 0.5
#
Ele.Et: 10000.
Ele.Eta: 2.47
Ele.CrackVeto: false
Ele.Iso: Loose_VarRad #FCHighPtCaloOnly #PLVLoose #FCLoose
Ele.IsoHighPt: Loose_VarRad # FCHighPtCaloOnly # tight iso required for electrons pt > 200 GeV
Ele.Id: TightLLH
Ele.d0sig: 5.
Ele.z0: 0.5
# ChargeIDSelector WP
Ele.CFT: None
#
MuonBaseline.Pt: 4000.
MuonBaseline.Eta: 2.7
MuonBaseline.Id: 1 # Medium
MuonBaseline.z0: 0.5
#
Muon.Pt: 10000.
Muon.Eta: 2.7
Muon.Id: 1 # Medium
Muon.Iso: Loose_VarRad #Loose_VarRad #FCLoose
Muon.IsoHighPt: Loose_VarRad #Loose_VarRad #FCLoose # change WP if you want
Muon.d0sig: 3.
Muon.z0: 0.5
#
MuonCosmic.z0: 1.
MuonCosmic.d0: 0.2
#
BadMuon.qoverp: 0.4
#
# PhotonBaseline.Pt: 25000.
# PhotonBaseline.Eta: 2.37
# PhotonBaseline.Id: Tight
# #
# Photon.Pt: 130000.
# Photon.Eta: 2.37
# Photon.Id: Tight
# Photon.Iso: FixedCutTight
# #
# Tau.Pt: 20000.
# Tau.Eta: 2.5
# Tau.Id: Medium
#Tau.DoTruthMatching: false
#
Jet.Pt: 20000.
Jet.Eta: 2.8
Jet.InputType: 9 # EMTopo 1, PFlow: 9 
Jet.JVT_WP: Default
Jet.JvtPtMax: 60.0e3
#Jet.UncertConfig: rel21/Summer2019/R4_CategoryReduction_FullJER.config
#Jet.UncertConfig: rel21/Summer2019/R4_CategoryReduction_SimpleJER.config
Jet.UncertConfig: rel21/Summer2019/R4_SR_Scenario1_SimpleJER.config
Jet.JMSCalib: false
Jet.UncertPDsmearing: false
#
FwdJet.doJVT: false
FwdJet.JvtEtaMin: 2.5
FwdJet.JvtPtMax: 50e3
#FwdJet.JvtUseTightOP: false
#
Jet.LargeRcollection: AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets # set to None to turn this off
Jet.LargeRuncConfig: rel21/Winter2021/R10_GlobalJES_SimpleJER_FullJMS.config
#rel21/Summer2019/R10_GlobalReduction.config # set to None to turn this off
#Jet.LargeRuncVars: pT,mass,D2Beta1     # W/Z Taggers
#Jet.LargeRuncVars: pT,Split23,Tau32WTA # Top taggers
#
Jet.WtaggerConfig: SmoothedContainedWTagger_AntiKt10LCTopoTrimmed_FixedSignalEfficiency80_MC16_20201216.dat
Jet.ZtaggerConfig: SmoothedContainedZTagger_AntiKt10LCTopoTrimmed_FixedSignalEfficiency80_MC16_20201216.dat
Jet.ToptaggerConfig: JSSDNNTagger_AntiKt10LCTopoTrimmed_TopQuarkInclusive_MC16_20201216_80Eff.dat
#Jet.WtaggerConfig: SmoothedContainedWTagger_AntiKt10TrackCaloClusterTrimmed_MaxSignificance_3Var_MC16d_20190410.dat # set to None to turn this off
#Jet.ZtaggerConfig: SmoothedContainedZTagger_AntiKt10TrackCaloClusterTrimmed_MaxSignificance_3Var_MC16d_20190410.dat # set to None to turn this off
#Jet.ToptaggerConfig: JSSDNNTagger_AntiKt10LCTopoTrimmed_TopQuarkInclusive_MC16d_20190405_80Eff.dat # set to None to turn this off
#
BadJet.Cut: LooseBad
#
#master switch for btagging use in ST. If false, btagging is not used neither for jets decorations nor for OR (regardless of the options below)
Btag.enable: true
#
Btag.Tagger: DL1r # DL1, DL1mu, DL1rnn, MV2c10mu, MV2c10rnn, MC2cl100_MV2c100
Btag.WP: FixedCutBEff_77
Btag.TimeStamp:	201903
Btag.CalibPath: xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2021-04-16_v1.root
Btag.MinPt: 20000.
Btag.SystStrategy: SFEigen
#
TrackJet.Coll: AntiKtVR30Rmax4Rmin02TrackJets # AntiKt2PV0TrackJets
TrackJet.Pt: 10000.
TrackJet.Eta: 2.8
BtagTrkJet.Tagger: DL1r
BtagTrkJet.WP: FixedCutBEff_77
BtagTrkJet.TimeStamp:  201903
BtagTrkJet.MinPt: 10000.
#
# set the -999. to positive number to override default
OR.DoBoostedElectron: true
OR.BoostedElectronC1: -999.
OR.BoostedElectronC2: -999.
OR.BoostedElectronMaxConeSize: -999.
OR.DoBoostedMuon: true
OR.BoostedMuonC1: -999.
OR.BoostedMuonC2: -999.
OR.BoostedMuonMaxConeSize: -999.
OR.DoMuonJetGhostAssociation: true
OR.DoTau: false
OR.DoPhoton: false
OR.Bjet: true
OR.ElBjet: true 
OR.MuBjet: false 
OR.TauBjet: false
OR.MuJetApplyRelPt: false
OR.MuJetPtRatio: -999.
OR.MuJetTrkPtRatio: -999.
OR.RemoveCaloMuons: true
OR.MuJetInnerDR: -999.
OR.BtagWP: FixedCutBEff_85
OR.BJetPtUpperThres: 100e3
#
OR.DoFatJets: false
OR.EleFatJetDR: -999.
OR.JetFatJetDR: -999.
#
SigLep.RequireIso: true
#
MET.EleTerm: RefEle
MET.GammaTerm: #RefGamma
MET.TauTerm: #RefTau
MET.JetTerm: RefJet
MET.MuonTerm: Muons
MET.OutputTerm: Final
MET.JetSelection: Tight # Loose, Tight, Tighter, Tenacious
MET.RemoveOverlappingCaloTaggedMuons: true
MET.DoRemoveMuonJets: true
MET.UseGhostMuons: false
MET.DoMuonEloss: false
#Met.DoSetMuonJetEMScale:	true
#
#METSig.TreatPUJets: true
## Trigger configuration
#Trig.Multi2015: e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose || mu20_iloose_L1MU15_OR_mu50
#Trig.Multi2016: e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50
#Trig.Multi2017: e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50
#Trig.Multi2018: e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50
## Trigger SFs configuration
#Ele.TriggerSFStringSingle: SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0
#Trig.Multi2018: e26_lhmedium_lrtloose_L1EM22VHI_OR_e26_lhtight_ivarloose_OR_e60_lhmedium_L1EM22VHI_OR_e80_lhmedium_L1EM22VHI || mu26_ivarmedium_L1MU20_OR_mu50_L1MU20
#Trig.Multi2018: e26_lhmedium_lrtloose_L1EM22VHI || e26_lhtight_ivarloose || e60_lhmedium_L1EM22VHI || e80_lhmedium_L1EM22VHI || mu26_ivarmedium_L1MU20 || mu50_L1MU20
# Trigger SFs configuration
#Ele.TriggerSFStringSingle: SINGLE_E_2015_2018_e26_lhmedium_lrtloose_L1EM22VHI_OR_e26_lhtight_ivarloose_OR_e60_lhmedium_L1EM22VHI_OR_e80_lhmedium_L1EM22VHI

#
Trigger.UpstreamMatching: true
Trigger.MatchingPrefix: TrigMatch_
#                                                                                                                                                                                                        # actual Mu files have to be set in SUSYTools                                                                                                                                                            
PRW.autoconfigPRWHFFilter: CVetoBVeto
PRW.autoconfigPRWRtags:mc16a:r9364_r11505_r11285,mc16c:r9781,mc16d:r10201_r11506_r11279,mc16e:r10724_r11507_r11249_r12020_r12034_r12405_d1616_r12400_r12400_r12627_r12627_r12627_r12627_d1639_r12597_p4505_d1663_r12711_r12711_d1729_r13201_r13208,mc16ans:r10740_r10832_r10847_r11008_r11036,mc16dns:r10739_r10833_r10848_r11009_r11037,mc16ens:r10790_r11038_r11265,mc20e:r13145_r13145_r13145_r13145_r13146_p5057,mc20d:r13144_r13144_r13144_r13144_r13146_p5057
#mc20:d1722_r13135_r13147_r13144_r13144_r13144_r13144_r13146_p4926_r13286_p4926_r13145_p4926_r13167_p4926_r13145_p5057,
#
StrictConfigCheck: true
# actual Mu files have to be set in SUSYTools
PRW.ActualMu2017File: GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
PRW.ActualMu2018File: GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
# default to None but do set to BFilter (366010-366017), CFilterBVeto (366019-366026), or CVetoBVeto (366028-366035) to remap MC16e Znunu dsid
#
StrictConfigCheck:	true
#
Truth.UseTRUTH3: true