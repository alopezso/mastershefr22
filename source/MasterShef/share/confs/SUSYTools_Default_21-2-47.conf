##################################################
# SUSYTools configuration file
##################################################
EleBaseline.Pt: 10000.
EleBaseline.Eta: 2.47
EleBaseline.Id: LooseAndBLayerLLH
EleBaseline.CrackVeto: false
#
Ele.Et: 25000.
Ele.Eta: 2.47
Ele.CrackVeto: false
Ele.Iso: GradientLoose
Ele.IsoHighPt: FixedCutHighPtCaloOnly # tight iso required for electrons pt > 400 GeV
Ele.Id: TightLLH
Ele.d0sig: 5.
Ele.z0: 0.5
# ChargeIDSelector WP
Ele.CFT: None
#
MuonBaseline.Pt: 10000.
MuonBaseline.Eta: 2.7
MuonBaseline.Id: 1 # Medium
#
Muon.Pt: 25000.
Muon.Eta: 2.7
Muon.Id: 1 # Medium
Muon.Iso: GradientLoose
Muon.d0sig: 3.
Muon.z0: 0.5
#
MuonCosmic.z0: 1.
MuonCosmic.d0: 0.2
#
BadMuon.qoverp: 0.2
#
PhotonBaseline.Pt: 25000.
PhotonBaseline.Eta: 2.37
PhotonBaseline.Id: Tight
#
Photon.Pt: 130000.
Photon.Eta: 2.37
Photon.Id: Tight
Photon.Iso: FixedCutTight
#
Tau.Pt: 20000.
Tau.Eta: 2.5
Tau.Id: Medium
#Tau.DoTruthMatching: false
Tau.IDRedecorate: False
#
Jet.Pt: 20000.
Jet.Eta: 2.8
Jet.InputType: 1 # EMTopo 1, PFlow: 9 
Jet.JVT_WP: Medium
Jet.UncertConfig: rel21/Summer2018/R4_StrongReduction_Scenario1_SimpleJER.config
#
FwdJet.doJVT: false 
FwdJet.JvtEtaMin: 2.5
FwdJet.JvtPtMax: 50e3
FwdJet.JvtUseTightOP: false
#
Jet.LargeRcollection: AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets
Jet.LargeRuncConfig: rel21/Moriond2018/R10_CombMass_medium.config
#Jet.LargeRuncVars: pT,Tau21WTA,Split12
#
Jet.WtaggerConfig: SmoothedWZTaggers/SmoothedContainedWTagger_AntiKt10LCTopoTrimmed_FixedSignalEfficiency80_MC15c_20161215.dat # 80% eff WP
Jet.ZtaggerConfig: SmoothedWZTaggers/SmoothedContainedZTagger_AntiKt10LCTopoTrimmed_FixedSignalEfficiency80_MC15c_20161215.dat # 80% eff WP
#
Jet.JMSCalib: None # Non, Extrap, Frozen
#
BadJet.Cut: LooseBad
#
#master switch for btagging use in ST. If false, btagging is not used neither for jets decorations nor for OR (regardless of the options below)
Btag.enable: true
#
Btag.Tagger: MV2c10 # DL1, DL1mu, DL1rnn, MV2c10mu, MV2c10rnn, MC2cl100_MV2c100
Btag.WP: FixedCutBEff_77
Btag.CalibPath: xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-06-29_v1.root
#
TrackJet.Coll: AntiKtVR30Rmax4Rmin02TrackJets # AntiKt2PV0TrackJets
TrackJet.Pt: 20000.
TrackJet.Eta: 2.8
BtagTrkJet.Tagger: MV2c10
BtagTrkJet.WP: FixedCutBEff_77
#
# set the -999. to positive number to override default
OR.DoBoostedElectron: true
OR.BoostedElectronC1: -999.
OR.BoostedElectronC2: -999.
OR.BoostedElectronMaxConeSize: -999.
OR.DoBoostedMuon: true
OR.BoostedMuonC1: -999.
OR.BoostedMuonC2: -999.
OR.BoostedMuonMaxConeSize: -999.
OR.DoMuonJetGhostAssociation: true
OR.DoTau: false
OR.DoPhoton: false
OR.Bjet: false
OR.ElBjet: false 
OR.MuBjet: false 
OR.TauBjet: false
OR.MuJetApplyRelPt: false
OR.MuJetPtRatio: -999.
OR.MuJetTrkPtRatio: -999.
OR.RemoveCaloMuons: true
OR.MuJetInnerDR: -999.
OR.BtagWP: FixedCutBEff_85
#
OR.DoFatJets: false
OR.EleFatJetDR: -999.
OR.JetFatJetDR: -999.
#OR.InputLabel: selected
#
SigLep.RequireIso: true
SigLepPh.IsoCloseByOR: false
#
MET.EleTerm: RefEle
MET.GammaTerm: RefGamma
MET.TauTerm: RefTau
MET.JetTerm: RefJet
MET.MuonTerm: Muons
MET.OutputTerm: Final
MET.JetSelection: Tight # Loose, Tight, Tighter, Tenacious
MET.RemoveOverlappingCaloTaggedMuons: true 
MET.DoRemoveMuonJets: true
MET.UseGhostMuons: false
MET.DoMuonEloss: false
#
PRW.MuUncertainty: 0.2
#
# Trigger SFs configuration
Ele.TriggerSFStringSingle: SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2017_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0
#
StrictConfigCheck: true
