import sys,os


samples_mc16a=".samples_mc16a.list"
if not os.path.isfile(samples_mc16a):
    os.system("rucio list-dids mc16_13TeV:mc16_13TeV.*merge.AOD.*r9364* --filter type=container --short | tee %s"%(samples_mc16a))


samples_mc16c=".samples_mc16c.list"
if not os.path.isfile(samples_mc16c):
    os.system("rucio list-dids mc16_13TeV:mc16_13TeV.*megre.AOD.*r9781* --filter type=container --short | tee %s"%(samples_mc16c))


samples_mc16d=".samples_mc16d.list"
if not os.path.isfile(samples_mc16d):
    os.system("rucio list-dids mc16_13TeV:mc16_13TeV.*merge.AOD.*r10201* --filter type=container --short | tee %s"%(samples_mc16d))


def getSamples(f):
    check=open(f)
    retval=[]
    for line in check:
        line=line.rstrip()
        retval.append(line)
    return list(sorted(retval))


all_samples={}
all_samples["mc16a"]=getSamples(samples_mc16a)
all_samples["mc16c"]=getSamples(samples_mc16c)
all_samples["mc16d"]=getSamples(samples_mc16d)


rtags={"r9364":"mc16a","r9781":"mc16c","r10201":"mc16d"}

def getTag(f):
    for tag in rtags:
        if tag in f: 
            return tag,rtags[tag]


def getName(f):
    return f.split(".")[2]

def findSamples(f,the_campaign):

    all_found={}
    for tag,campaign in sorted(rtags.iteritems()):
        if campaign == the_campaign:continue
        check=all_samples[campaign]
        
        name=getName(f)
        
        samples_found=[]
        for line in check:
            line=line.rstrip()
            if name in line:
                samples_found.append(line)

        

        all_found[campaign]=samples_found
    return all_found

for f in sys.argv[1:]:
    for sample in open(f):
        if '#' in sample:continue
        sample=sample.rstrip()
        tag,campaign=getTag(sample)
        print sample
        otherSamples=findSamples(sample,campaign)
        for sam in otherSamples["mc16c"]:print sam
        for sam in otherSamples["mc16d"]:print sam
        print 
    print 
