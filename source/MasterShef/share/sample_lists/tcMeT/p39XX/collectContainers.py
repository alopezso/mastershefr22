import argparse

parser = argparse.ArgumentParser(description='DxAOD file checker')

parser.add_argument('-i', action='store', default=None,
                    dest='files',
                    required=True,
                    nargs='+',
                    help="input files from old lists")

opts=parser.parse_args()

badFiles=[]
theMap={}
for f in opts.files:
    with open(f,"r") as fin:
        for line in fin.readlines():
            line=line.rstrip()
            if 'data' in line:continue
            if '#' in line:continue
            if line.rstrip() =="": continue
            temp=line.rstrip().split(".")
            
            dsid=temp[1]
            
            tags=temp[-1].split("_")
            if len(tags)>4: 
                print "errr what is this...",line
                badFiles.append(line)
                continue


            etag,stag,rtag,ptag=tags           

            if dsid not in theMap:
                theMap[dsid]={}
            
            if rtag not in theMap[dsid]:
                theMap[dsid][rtag]={'sample':line.rstrip(),'file':f}
            else:
                print line
                print "rtag already present!"
                continue

       # break

#load all p3990
all_p3990=[ line.rstrip() for line in open("all/all_p3990.txt","r") ]
all_p3965=[ line.rstrip() for line in open("all/all_p3965.txt","r") ]


#complete=open("complete.txt","w")
missing=open("missing.txt","w")

outFileNameMap={}
print "==========================================="
for dsid,m in theMap.items():
    rtags=['r10724', 'r10201', 'r9364']
   
    for rtag in rtags:
        try:
            sample=m[rtag]['sample']
        except KeyError:
            #print 'Cannot find ', rtag, ' in ',dsid
            continue
        
        try:
            sam=all_p3990[all_p3990.index(sample.replace("p3895","p3990"))]
            #print sam
            fname=m[rtag]['file'].split("/")[-1]
            if fname not in outFileNameMap:
                outFileNameMap[fname]=[]
            outFileNameMap[fname].append(sam)
           
        except ValueError:
            #try:
            #    print all_p3965[all_p3965.index(sample.replace("p3895","p3965"))]
            #except ValueError:
            print "Warning: missing",sample.replace("p3895","p3990")
            #if 'DM' in sample:
            #    sam=all_p3990[all_p3990.index(sample.replace("p3895","p3990"))]
            #    print sam
            missing.write(sample.replace("p3895","p3990")+"\n")
            continue
        
        #complete.write(sam+"\n")
#complete.close()
missing.close()
#exit(0)

print "==============================="
for key,samples in outFileNameMap.items():
    outFile=open(key,"w")
    print key,len(samples)
    for sam in samples:
        outFile.write(sam+"\n")
    outFile.close()
        


