import SUSYCommonTriggerList
import SUSY7TriggerList


def getListFromFile(modname):
    retval={}
    for item in dir(modname):
        temp=getattr(modname,item)
        if type(temp).__name__ != "list": continue
        retval[item]=list(sorted(temp))
    
    return retval

all_triggers={}
all_triggers.update(getListFromFile(SUSY7TriggerList))


printed_items=[]
for subset in sorted(all_triggers.iterkeys()):
    #skip lines already printed!
    #if item in printed_items:continue
  
    print
    print "# ",subset
    for item in all_triggers[subset]:
        
        if item in printed_items:continue

        print item

        printed_items.append(item)
