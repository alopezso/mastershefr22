#include "JetSmearing/SmearData.h"
#include "xAODBase/IParticleHelpers.h"

#include <utility>

SmearData::SmearData(const xAOD::JetContainer& theJets){
  std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_shallowCopy = xAOD::shallowCopyContainer( theJets );
  jetContainer = jets_shallowCopy.first;
  jetAuxContainer = jets_shallowCopy.second;
  bool setLinks = xAOD::setOriginalObjectLink(theJets,*jetContainer);
  if (false == setLinks) std::cout << "WARNING: In SmearData::SmearData(const xAOD::JetContainer& theJets) Could not update jet links to match original jets " << std::endl;
}

SmearData::~SmearData(){
  //if (jetAuxContainer) delete jetAuxContainer;
  //if (jetContainer) delete jetContainer;
}
