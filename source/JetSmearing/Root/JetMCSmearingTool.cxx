#include "JetSmearing/JetMCSmearingParameters.h"
#include "JetSmearing/JetMCSmearingTool.h"

#include "xAODBTagging/BTagging.h"

#include "TRandom.h"

#include <sstream>

namespace JetSmearing {

  JetMCSmearingTool::JetMCSmearingTool(const std::string& name) : asg::AsgTool( name ) {

    //default setting of 1000 smears per seed event
    declareProperty("NumberOfSmearedEvents",m_nSmear = 1000);
    //By default we do want to do b-jet smearing
    declareProperty("DoBJetSmearing", m_doBJetSmearing = true);
    m_ptBinning = 20*1000;
    //use tail weights as default method for systematic uncertainties
    declareProperty("UseTailWeights", m_doTailWeights = false);
    //by default want to smear Gaussian core without systematic variation    
    declareProperty("DoGaussianCoreSmearing",m_doSecondarySmearing = false);
    declareProperty("GaussianCoreSmearingType", m_gaussianCoreSmearingTypeString = "optimal");
    //by default don't adjust the means of jet responses
    declareProperty("ShiftMeanType",m_meanShiftTypeString = "none");
    //by default we smear the phi component
    declareProperty("DoPhiSmearing",m_doPhiSmearing = false);
  }

  JetMCSmearingTool::~JetMCSmearingTool() {

    if (m_Rvector.size()) {
      for (unsigned int i=0; i<m_Rvector.size(); i++){
	if (m_Rvector[i].size())
	  for (unsigned int j=0; j<m_Rvector[i].size(); j++) 
	    delete m_Rvector[i][j];
	m_Rvector[i].clear();
      }
      m_Rvector.clear();
    }
    
    if (m_bJet_Rvector.size()) {
      for (unsigned int i=0; i<m_bJet_Rvector.size(); i++){
	if (m_bJet_Rvector[i].size())
	  for (unsigned int j=0; j<m_bJet_Rvector[i].size(); j++) 
	    delete m_bJet_Rvector[i][j];
	m_bJet_Rvector[i].clear();
      }
      m_bJet_Rvector.clear();
    }
    
    if (m_Rmap.size()) {
      for (unsigned int i=0; i<m_Rmap.size(); i++) 
	delete m_Rmap[i];
      m_Rmap.clear();
    }
    
    if (m_bJet_Rmap.size()) {
      for (unsigned int i=0; i<m_bJet_Rmap.size(); i++) 
	delete m_bJet_Rmap[i];
      m_bJet_Rmap.clear();
    }
  }

  StatusCode JetMCSmearingTool::initialize() {

    //Here we map the strings the user chooses to enums (which are safer at compile time)
    //This is needed because Gaudi (in athena) does not allow properties of arbitrary type (like SmearingType)
    m_gaussianCoreSmearingType = mapStringToSmearingType(m_gaussianCoreSmearingTypeString);
    m_meanShiftType = mapStringToSmearingType(m_meanShiftTypeString);

    // Cout list of tool parameters
    std::cout << std::endl << std::endl;
    std::cout << "Setting up Jet Smearing Tool with parameters :" << std::endl;
    std::cout << "NumberOfSmearedEvents = " << m_nSmear << " ||  ";
    std::cout << "DoBJetSmearing = "<< m_doBJetSmearing <<" ||  ";
    std::cout << "Jet Response Map true jet pt binning = " << m_ptBinning << " ||  ";
    std::cout << "UseTailWeights = " << m_doTailWeights <<" ||  "; 
    std::cout << "DoGaussianCoreSmearing = " << m_doSecondarySmearing <<" ||  ";
    std::cout << "GaussianCoreSmearingType = " << m_gaussianCoreSmearingType <<" ||  ";
    std::cout << "ShiftMeanType = " << m_meanShiftType <<" ||  ";
    std::cout << "DoPhiSmearing = " << m_doPhiSmearing << std::endl;
    return StatusCode::SUCCESS;
  }

  void JetMCSmearingTool::AddPhiCorrections(const TF1* data , const TF1* pseudo ){
    //old notation that should be fixed one day... confusing..
    //setParameters(N,S,C)
    //N/jetPt + S/sqrt(jetPt) + C
    //[0] + [1]/x + [2]/sqrt(x)
    //[0] == C ; [1] == N ; [2] == S 
    double N=data->GetParameter(1);
    double S=data->GetParameter(2);
    double C=data->GetParameter(0);
    m_thePhiParameters.setParametersData(N,S,C);
    
    N=pseudo->GetParameter(1);
    S=pseudo->GetParameter(2);
    C=pseudo->GetParameter(0);
    

    m_thePhiParameters.setParametersPseudo(N,S,C);
    
  }
  


  void JetMCSmearingTool::AddResponseMaps(const TH2F* Rmap_lj , const TH2F* Rmap_bj ){
    
    if (Rmap_lj) {
      TH2F* rMapClone = (TH2F*)Rmap_lj->Clone("JetMCSmearingTool::rMapClone");
      m_Rmap.push_back(rMapClone);
      std::vector<TH1F*> vector_1DHistograms ; // Creating a buffer of TH1F's
      
      const unsigned int rMapBinNumber = m_Rmap.size() - 1; // Get the Rmap bin number to loop over ,last added.
      for (int i=1; i<=m_Rmap[rMapBinNumber]->GetNbinsX(); i++) {
	
	std::stringstream ss;
	ss << i;
	std::string name = "response_sys" + ss.str();
	const TH1F* h_temp = (TH1F*)m_Rmap[rMapBinNumber]->ProjectionY(name.c_str(), i, i);
	name = "JetMCSmearingTool::projectionHist_" + ss.str();
	TH1F* h = (TH1F*)h_temp->Clone(name.c_str());
	for (int j=41; j<=52; ++j) h->SetBinContent(j,h->GetBinContent(j)); 
	vector_1DHistograms.push_back(h); /// Getting the vector of TH1F's   
      }
      m_Rvector.push_back(vector_1DHistograms); 
    }
    
    //if you ever had a NULL map, we disable all b-jet smearing.
    if (Rmap_bj) {
      TH2F* bJet_RmapClone = (TH2F*)Rmap_bj->Clone("JetMCSmearingTool::rMap_BJet_Clone");
      m_bJet_Rmap.push_back(bJet_RmapClone);
      const unsigned int rMapBinNumber = m_bJet_Rmap.size() - 1; // Get the Rmap bin number to loop over ,last added.
      std::vector<TH1F*> vector_1DHistograms ; // Creating a buffer of TH1F's
      for (int i=1; i<=m_bJet_Rmap[rMapBinNumber]->GetNbinsX(); i++) {
	std::stringstream ss;
	ss << i;
	std::string name = "response_sys" + ss.str();
	const TH1F* h_temp = (TH1F*)m_bJet_Rmap[rMapBinNumber]->ProjectionY(name.c_str(), i, i);
	name = "JetMCSmearingTool::projectionBJetHist_" + ss.str();
	TH1F* h = (TH1F*)h_temp->Clone(name.c_str());
	for (int j=41; j<=52; ++j) h->SetBinContent(j,h->GetBinContent(j)); 
	vector_1DHistograms.push_back(h); /// Getting the vector of TH1F's   
      }
      m_bJet_Rvector.push_back(vector_1DHistograms);// pushing the vector of TH1F's to the vector of vectors
    }
    else m_doBJetSmearing = false;
    
  }

  const TH1F* JetMCSmearingTool::MakeSmearingHist(const double& pt, const unsigned int& i){
    //This is only valid for pt binning = 20 GeV
    const int ptBin = this->getPtBin(pt);
    return m_Rvector[i][ptBin];
  }

  const TH1F* JetMCSmearingTool::MakeBJetSmearingHist(const double& pt, const unsigned int& i){
    //This is only valid for pt binning = 20 GeV
    const int ptBin = this->getPtBin(pt);
    return m_bJet_Rvector[i][ptBin];
  }


  void JetMCSmearingTool::DoSmearing(std::vector<std::unique_ptr<SmearData> >& smrMc, const xAOD::JetContainer& jets) {


    const unsigned int nJets = jets.size();

    smrMc.clear();

    for (unsigned int x = 0; x < m_nSmear; ++x){
       std::unique_ptr<SmearData> smearDataPtr( new SmearData(jets));
       smrMc.push_back( std::move(smearDataPtr));
    }

    std::vector<const TH1F*> vectorOfResponseHistograms(nJets);
    for (unsigned int i = 0; i < nJets; i++) {
      if(m_doBJetSmearing){
	if ( this->isJetBTagged(*jets[i]) ) vectorOfResponseHistograms[i] = MakeBJetSmearingHist(jets[i]->pt(), 0);
	else vectorOfResponseHistograms[i] = MakeSmearingHist(jets[i]->pt(), 0);
      }//doBJetSmearing
      else {
	vectorOfResponseHistograms[i] = MakeSmearingHist(jets[i]->pt(), 0);
      }

    }//jets loop

    for (unsigned int i = 0; i < m_nSmear; i++) {

      SmearData& smrData = *smrMc[i];
      smrData.TailWeights.clear(); // clear the vector
      smrData.TailWeights.resize(m_Rmap.size()-1,1.0); // initialise the weight to 1.
      smrData.TailWeightsbtag.clear(); // clear the vector
      smrData.TailWeightsbtag.resize(m_Rmap.size()-1,1.0); // initialise the weight to 1.
      smrData.TailWeightsbveto.clear(); // clear the vector
      smrData.TailWeightsbveto.resize(m_Rmap.size()-1,1.0); // initialise the weight to 1.

      for (unsigned int j = 0; j < nJets; j++) {

	const xAOD::Jet* thisJet = jets[j];
	
	//Only smear jets that pass kinematic selection
	if (false == this->passSelection(*thisJet)) continue;

	double Random = vectorOfResponseHistograms[j]->GetRandom();

	double pt=thisJet->pt();

	bool isBJet = false;
	if (true == m_doBJetSmearing) isBJet=this->isJetBTagged(*thisJet); // test if its a b-jet if b-jet smearing is being applied

	if(m_doTailWeights){  // We pick R first before the Additional Gaus Smearing, therefore Weights should be calculated before it.

	  int ptbin = this->getPtBin(pt);

	  for (unsigned int z=1 ; z < m_Rmap.size() ; z++ ){   // running over Systematic maps only
	    if (isBJet){
	      int Rbin = m_bJet_Rvector[0][ptbin]->FindBin(Random); // FindsRbin
	      
	      TH1F* Sys = const_cast<TH1F*> (m_bJet_Rvector[z][ptbin]);
	      double TempWeight = 1.;
	      if (vectorOfResponseHistograms[j]->GetBinContent(Rbin) && Sys->GetBinContent(Rbin) ) TempWeight = (Sys->GetBinContent(Rbin) / Sys->Integral() ) / ( vectorOfResponseHistograms[j]->GetBinContent(Rbin) / vectorOfResponseHistograms[j]->Integral() );
	      
	      smrData.TailWeights[z-1] *= TempWeight; 
	      smrData.TailWeightsbtag[z-1] *= TempWeight; 
	      
	    }//if b-tagged
	    else{

	      int Rbin = m_Rvector[0][ptbin]->FindBin(Random); // FindsRbin
	      TH1F* Sys = const_cast<TH1F*> (m_Rvector[z][ptbin]);
	      double TempWeight = 1.;
	      if (vectorOfResponseHistograms[j]->GetBinContent(Rbin) && Sys->GetBinContent(Rbin) ) TempWeight = (Sys->GetBinContent(Rbin) / Sys->Integral() ) / ( vectorOfResponseHistograms[j]->GetBinContent(Rbin) / vectorOfResponseHistograms[j]->Integral() );
	      
	      smrData.TailWeights[z-1] *= TempWeight; 
	      smrData.TailWeightsbveto[z-1] *= TempWeight; 

	    } // if light jet
	  } // end of loop over Rmaps
	} // end of dotailweights

	if (true == m_doSecondarySmearing){
	  double gaussianCoreSmearingValue = this->calculateGaussianCoreSmearingValue(m_theCoreParameters,pt,1.0 , false);
	  if (-1.0 == gaussianCoreSmearingValue) gaussianCoreSmearingValue = 1.0;
	  Random *= gaussianCoreSmearingValue;

	}//smearing of Gaussian core

	if (m_meanShiftType == high) Random += 0.05;
	else if (m_meanShiftType == low) Random -= 0.05;

	TLorentzVector theLorentzVector(thisJet->px()*Random, thisJet->py()*Random, thisJet->pz()*Random, thisJet->e()*Random );
	xAOD::JetContainer* smearedJetContainer = smrData.jetContainer;
	xAOD::Jet* smearedJet = smearedJetContainer->at(j);
	this->setJet4Vector(*smearedJet,theLorentzVector);
	if (m_doPhiSmearing){
	  double gaussianCoreSmearingPhiValue = this->calculateGaussianCoreSmearingValue(m_thePhiParameters,pt,0.0 ,true);
	  if (-1.0 != gaussianCoreSmearingPhiValue) this->rotateZ(*smearedJet,gaussianCoreSmearingPhiValue, theLorentzVector);
	}

      }//jets loop

    }//smearing loop

  }//doSmearing

  
  //void JetMCSmearingTool::DecorateSmeared(const xAOD::Jet& theJet){
  //  theJet.auxdata< char >("smearjet") == true ;
  // }

  bool JetMCSmearingTool::isJetBTagged(const xAOD::Jet& theJet){
    if ( theJet.auxdata< char >("bjet") == true ) return true;                       
    else return false;
  }

  double JetMCSmearingTool::calculateGaussianCoreSmearingValue(const JetMCSmearingParameters& theParameters, const double& jetPt, const double& meanValue , const bool& isPhiSmearing){

    const double N_data = theParameters.getN_Data();
    const double S_data = theParameters.getS_Data();
    const double C_data = theParameters.getC_Data();
    const double N_smr = theParameters.getN_Smr();
    const double S_smr = theParameters.getS_Smr();
    const double C_smr = theParameters.getC_Smr();

    const double jetPt_GeV = jetPt/m_GeV;

    double sigma_data = N_data/jetPt_GeV+S_data/sqrt(jetPt_GeV)+C_data;
    double sigma_smr = N_smr/jetPt_GeV+S_smr/sqrt(jetPt_GeV)+C_smr;
    double sigma_cor;
    
    if (sigma_smr > sigma_data) sigma_cor=0.0;
    else if(isPhiSmearing)  {
      //sigma_cor =  (1./sqrt(2)) * sqrt ( sigma_data*sigma_data - sigma_smr*sigma_smr );
      sigma_cor =  sqrt ( sigma_data*sigma_data - sigma_smr*sigma_smr );
    }
    else sigma_cor = sqrt(2) * sqrt ( sigma_data*sigma_data - sigma_smr*sigma_smr );

    if(!isPhiSmearing){
      if (m_gaussianCoreSmearingType == high) sigma_cor+=0.05;
      if (m_gaussianCoreSmearingType == low) sigma_cor-=0.05;
    }

    double gaussianCoreSmearingValue = -1.0;
    if (sigma_cor > 0.0) gaussianCoreSmearingValue = gRandom->Gaus(meanValue, sigma_cor);
       
    return gaussianCoreSmearingValue;
  }


  void JetMCSmearingTool::rotateZ(xAOD::Jet& theJet, const double& angle, TLorentzVector& theLorentzVector){

    theLorentzVector.RotateZ(angle);
    this->setJet4Vector(theJet,theLorentzVector);
  }

  void JetMCSmearingTool::setJet4Vector(xAOD::Jet& theJet, const TLorentzVector& theLorentzVector){
    xAOD::JetFourMom_t newP4( theLorentzVector.Pt(), theLorentzVector.Eta(), theLorentzVector.Phi(), theLorentzVector.M() );
    theJet.setJetP4(xAOD::JetAssignedScaleMomentum,newP4);
  }

  bool JetMCSmearingTool::passSelection(const xAOD::Jet& theJet){
    
    if ( theJet.auxdata< char >("smearjet") == false ) return false;    
    return true;

  }
      
  SmearingType JetMCSmearingTool::mapStringToSmearingType(const std::string& theSmearingTypeString){

    if ("optimal" == theSmearingTypeString) return optimal;       
    else if ("high" == theSmearingTypeString) return high;
    else if ("low" == theSmearingTypeString) return low;
    else if ("none" == theSmearingTypeString) return none;
    else return none;
    
  }


}
