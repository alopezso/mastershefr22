
#include "JetSmearing/PreScaleTool.h"

using namespace JetSmearing;

PreScaleTool::PreScaleTool(const std::string& name) : asg::AsgTool( name ) {
 
  declareProperty("HistoPath",m_histoPath="");//gSystem->ExpandPathName("$ROOTCOREBIN/data/JetSmearing/PreScales/prescale_histos_combined_2015-2016.root"));
 
}


StatusCode PreScaleTool::initialize(){
    
  infile = new TFile(m_histoPath.c_str());
  
  if(!infile->IsOpen()){
    ATH_MSG_WARNING(" Infile with name ' " << m_histoPath << " ' cannot be opened! check your declaration of property HistoPath ");
    return StatusCode::FAILURE;
  }
    
  // fill map with histograms
  TIter next(infile->GetListOfKeys());
  TKey *key;
  while ( (key=(TKey*)next()) )
    {
      if ( key->GetClassName() != std::string("TH1F") )
	continue;
      _hmap[key->GetName()] = (TH1F*) infile->Get(key->GetName());
    }

  ATH_MSG_INFO("Loaded " << _hmap.size() << " prescale histograms");

  return StatusCode::SUCCESS;
}

PreScaleTool::~PreScaleTool(){

  delete infile;
}


double PreScaleTool::getTriggerPrescale(std::vector<std::pair<std::string,bool > >  trigPass , xAOD::Jet theJet, int runnumber){
  double pT_1jet = theJet.pt();
  //check leading jet pT and runnumber
  if(pT_1jet==0)
    return 0;
  if(runnumber==0)
    return 0;
  double GeV=1000.;
  std::string RunNumber = std::to_string(runnumber);
  return getTriggerPrescale(trigPass ,pT_1jet/GeV, RunNumber);
}


double PreScaleTool::getTriggerPrescale(std::vector<std::pair<std::string,bool > >  trigPass , double pT_1jet, std::string runnumber){
  /*
  if(!infile){
    ATH_MSG_WARNING(" No input file found! Did you forget to call prescaletool->initialize() ?? ");
    return StatusCode::FAILURE;
  }
  */

  //open the histogram for the selected runnumber
  std::string name =  "h_"+runnumber;

  auto map_it = _hmap.find(name);
  if (map_it == _hmap.end()) {
    ATH_MSG_WARNING("Cannot find histogram " << name);
    return 0;
  }

  TH1F* theHisto = map_it->second;

  //return 0 prescale if runnumber cannot be found
  if (!theHisto) {
    ATH_MSG_WARNING("Cannot load histogram " << name);
    return 0;
  }
  

  //loop over the triggers to see which fired
  for(auto & it: trigPass)
    {

      //skip if the trigger didn't fire 
      if (it.second == false)
	continue;


      //first check if the trigger is in the histogram file
      bool isTriggerOn = false;
      double lower = 0.;
      double upper = 0.;
      double prescale = 0.;
      for(int i=1; i<theHisto->GetNbinsX();i++){
	bool ismatched = it.first == theHisto->GetXaxis()->GetBinLabel(i);
	
	if(ismatched){
	  isTriggerOn=true;
	  lower = theHisto->GetXaxis()->GetBinLowEdge(i);
	  upper = theHisto->GetXaxis()->GetBinUpEdge(i);
	  //if the last bin aka unprescaled, then the upper limit is infinite
	  if(i==(theHisto->GetNbinsX()-1))
	    upper=10E8;
	  prescale = theHisto->GetBinContent(i);
	  break;
	}
      }
     
      //skip if the trigger was turned off during the run
      if(!isTriggerOn)
	continue;
   
      //check if HLT jet is within threshold
      if(pT_1jet>=lower && pT_1jet<upper){
	return prescale;
      }
      else{
	ATH_MSG_DEBUG("outside of threshold " <<pT_1jet << " " << lower << "," << upper  );
	return 0.0;
      }    
    }
  ATH_MSG_DEBUG("Nothing fired!");
  return 0.0;

}

