#include "JetSmearing/JetMCSmearingParameters.h"
#include <iostream>

JetMCSmearingParameters::~JetMCSmearingParameters() {}


JetMCSmearingParameters::JetMCSmearingParameters(){
  N_data=0.0;
  S_data=0.0;
  C_data=0.0;
  N_smr=0.0;
  S_smr=0.0;
  C_smr=0.0;
}


void JetMCSmearingParameters::setParametersData(double N, double S, double C){
  N_data=N;
  S_data=S;
  C_data=C;
}

void JetMCSmearingParameters::setParametersPseudo(double N, double S, double C){
  N_smr=N;
  S_smr=S;
  C_smr=C;
}



