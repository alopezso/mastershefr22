#ifndef SMEARDATA_H
#define SMEARDATA_H

#include "TVector2.h"
#include "xAODJet/JetContainer.h"
#include "xAODCore/ShallowCopy.h"

#include <utility>

class SmearData
{
 public:
  
  SmearData(const xAOD::JetContainer& theJets);
  ~SmearData() ;

  xAOD::JetContainer* jetContainer;
  xAOD::ShallowAuxContainer* jetAuxContainer;
  TVector2 etMissCellOut;
  std::vector<double> TailWeights;
  std::vector<double> TailWeightsbtag;
  std::vector<double> TailWeightsbveto;

 private:
  std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* >* myPair;

};

#endif
