#ifndef IPRESCALETOOL_H
#define IPRESCALETOOL_H


#include "AsgTools/AsgTool.h"

namespace JetSmearing{
  class IPreScaleTool : public virtual asg::IAsgTool   {
    ASG_TOOL_INTERFACE( IPreScaleTool )
     

  public:
    virtual double getTriggerPrescale(std::vector<std::pair<std::string,bool > >  trigPass , double pT_1jet, std::string runnumber) = 0;
    virtual  double getTriggerPrescale(std::vector<std::pair<std::string,bool > >  trigPass , xAOD::Jet theJet, int runnumber) = 0;
    
    virtual StatusCode initialize() = 0;
	};
}
#endif
 
