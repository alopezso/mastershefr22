#ifndef IJETMCSMEARINGTOOL_H
#define IJETMCSMEARINGTOOL_H

/** This is the interface of the xAOD version of the Jet Smearing tool, to be used for data-driven multijet background estimations 
Author: Mark Hodgkinson
Based on the Jet Smearing tool developed by R.Duxfield, S. Owen, M. Hodgkinson, G. Fletcher
*/

#include <TH2F.h>
#include "SmearData.h"
#include "AsgTools/IAsgTool.h"
#include <memory>

namespace JetSmearing {

  class IJetMCSmearingTool : public virtual asg::IAsgTool {

    /** Declare the interface that the class provides */
    ASG_TOOL_INTERFACE( JetSmearing::IJetMCSmearingTool )
    
  public:

    virtual void DoSmearing(std::vector<std::unique_ptr<SmearData >>& smrMc, const xAOD::JetContainer& jets) = 0;
      
    virtual StatusCode initialize() = 0;

    /** Set pointers to response maps */
    virtual void SetResponseMaps(const TH2F* Rmap_lj , const TH2F* Rmap_bj) = 0;

    /** Add systematic response map or pair of maps */
    virtual void AddSystematicResponseMaps(const TH2F* Rmap_lj , const TH2F* Rmap_bj ) = 0;

   
  };

}
#endif
