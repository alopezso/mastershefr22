import os,sys,glob,re
import ROOT

triggers=["HLT_g35_loose_L1EM15","HLT_g40_loose_L1EM15","HLT_g45_loose_L1EM15","HLT_g50_loose_L1EM15","HLT_j400","HLT_j360","HLT_j320","HLT_j260","HLT_j200","HLT_j175","HLT_j150","HLT_j110","HLT_j85","HLT_j60","HLT_j25","HLT_j15"]


outfile = ROOT.TFile("prescale_histos.root","recreate")
for trigger in triggers:
    # logs from iLumiCalc.exe saved in lumiFiles_<trigger>/lumi-<runnumber>
    for fin in glob.glob("lumiFiles_"+trigger+"/*"):
        
        runnumber=fin.split("lumiFiles_"+trigger+"/lumi-")[1].split(".")[0]

        larlumi=0
        prescaleCorLumi=0
        for line in open(fin):
            line=re.sub('\x1b[^m]*m', '', line)
            bits = re.split("\s+",line)
          
            
            if "Total" in line and "LAr" in line:
                larlumi= float(bits[9])
                print bits
 
            if "Total" in line and "IntL" in line and "recorded" in line:
                prescaleCorLumi=float(bits[8])
                print bits
          

        
        prescale=larlumi/prescaleCorLumi    
       
        

        h = ROOT.TH1F(runnumber,trigger,1,0,2)
        h.Fill(1,prescale)

        print trigger,runnumber,h.GetBinContent(1),larlumi,prescaleCorLumi
        outfile.cd()
        
        cdtof = outfile.GetDirectory(trigger)
        if cdtof == None:
            cdtof = outfile.mkdir(trigger)
        cdtof.cd()
        h.Write("h_"+runnumber)


        del h
