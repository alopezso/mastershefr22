# Twiki page

For help in running the JetSmearing package, please refer to our twiki page:
https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/JetSmearing

# RootCore Setup

Firstly checkout the JetSmearing repo:
```bash
#clone the repo
git clone https://:@gitlab.cern.ch:8443/atlas-phys-susy-wg/JetSmearing.git
cd JetSmearing
#git fetch #optional if you need to fetch additional tags that might have been pushed since your original git clone
# Recommeded tag Moriond2017 (01-00-26-02)
git checkout JetSmearing-01-00-26-02
```
